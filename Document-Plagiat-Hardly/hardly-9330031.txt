(Dalam Nenden Yusnita, 1999: 53-55).
Seperti yang tefah dijelaskan sebefumnya, kontrol diri dapat memberikan
pengaruh dalam dunia sosialisasi seseorang. Salah satunya adalah untuk
meningkatkan kemampuan penyesuaian diri pad a seseorang khususnya remaja,
karena interaksi remaja dengan fingkungan ternan sebayanya juga dipengaruhi
kemampuan kendafi dirinya. Dengan memifiki kontrof diri, seseorang dapat
mengefektifkan pikiran, emosi dan kebutuhan-kebutuhannya dalam rangka
pengambilan keputusan dan mampu berpikir jernih yang pad a gilirannya dapat
membantu mengarahkan perilakunya saat berinteraksi dengan lingkungan
teman-temannya. Sebafiknya, dengan tidak dimifikinya kontrol diri yang memadai
seseorang tidak dapat mengendafikan dan mengefektifkan pikiran, emosi dan
kebutuhan-kebutuhannya yang membuat mereka sulit untuk memfokuskan dan
memusatkan perhatian untuk mengarahkan perilakunya dalam berinteraksi
dengan lingkungan yang dihadapi (Lazarus, 1976).
Individu yang memiliki penyesuaian diri yang baik (well-adjusted)
adalah seorang yang respon-responnya matang, efisien bisa memuaskan, sehat
dan tanpa banyak mengeluarkan energi, tanpa membuang-buang waktu dan
tanpa banyak kesalahan (Schneiders, 1964). Secara ringkas, dapat dikatakan
bahwa seorang yang well-adjusted adalah seorang yang dalam batas-batas
kapasitasnya sendiri dan sesuai dengan kepribadiannya, belajar untuk bereaksi
terhadap dirinya sendiri dan lingkungannya secara matang, sehat, efisien dan
memuaskan dan dapat menyelesaikan konflik mental, frustrasi dan bereaksi
terhadap permasalahan personal dan sosial tanpa memunculkan tingkah laku
simptomatik.
Seorang yang well-adjusted relatif bebas dari simptom-simptom
ketidakmampuan seperti kecemasan yang kronis, obsesi, fobia, keragu-raguan
dan gangguan psikosornatis yang mengacaukan moral, sosial,
keagamaan/pekerjannya. la adalah seorang yang hidup dalam/membantu untuk
mewujudkan sebuah dunia re/asi interpersonal dan kegembiraan timba/ balik
yang berperan da/am kelanjutan realisasi dan pertumbuhan kejadian (Dalam
Maria Eminingisih, 1999: 42-43).
 Istilah "adolescence" atau remaja berasal dari kata latin "adolescence" 
 (kata bendanya "adoloscentia" yang berarti remaja) yang berarti 'tumbuh' atau 
 'tumbuh ke arah kematangan'. Kematangan disini mempunyai arti yang luas, 
 mencakup kematangan secara fisiologis, psikologis dan sosial. Masa remaja 
 merupakan suatu periode transisi, dimana individu mengalami perubahan baik 
 secara fisik maupun psikis dari masa anak-anak menuju ke masa dewasa 
 (Hurlock, 1994). 
 Masa remaja dimulai ketika seorang individu mulai mencapai kematangan 
 seksual dan berakhir pada saat dimana ia sudah tidak tergantung lagi pad a 
 otoritas orang dewasa. Hurlock (1994), membedakan masa remaja menjadi 2 
 periode, yaitu early adolescence dan late adolescence. Garis pemisah antara 
 early adolescence dengan late adolescence terletak kira-kira di sekitar usia tujuh 
 belas tahun; usia sa at mana rata-rata setiap remaja memasuki dunia sekolah 
 menengah. Apabila dibagi berdasarkan periode awal dan akhir, maka early 
 adolescence berada pada rentang 12/13 -17/18 tahun, sedangkan late 
 adolescence berada pada usia 17/18 - 21 tahun (Hurlock, 1994). 
 Masa remaja memiliki ciri-ciri tertentu yang membedakannya dari 
 masa sebelumnya dan sesudahnya : 
 . Pada masa remaja terjadi perkembangan fisik dan mental yang cepat 
 sehingga menimbulkan kebutuhan akan penyesuaian mental dan 
 pembentukan sikap, nilai dan minat yang baru. 
 . Pada masa ini terjadi perubahan minat, emosi yang meninggi, adanya 
 sikap ambivalensi terhadap perubahan. 
 . Pada masa ini inividu mengalami banyak masalah yang kadang-kadang 
 terasa sufit untuk diatasi, karena pengalaman dalam mengatasi masalah 
 kurang dan menolak bantuan orangtua untuk mengatasi masalah yang 
 timbul. 
 . Pada masa remaja, individu belum memiliki status identitas yang jelas, 
 sehingga timbul keraguan akan peran apa yang harus diinginkannya 
 . Pada masa ini remaja yang sifatnya negatif dari orang dewasa, 
 menimbulkan banyak pertentangan sehingga menghalangi remaja untuk 
 meminta bantuan dalam menyelesaikan berbagai masalah 
 . Pada masa ini timbul harapan-harapan yang kurang realistik terhadap diri 
 sendiri, keluarga , ternan-ternan dan kehidupan pad a umumnya, namun 
 sesuai dengan bertambahnya pengalaman dan meningkatnya 
 kemampuan untuk berpikir rasional, sikap ini akan berubah. 
 . Pada masa ini remaja menganggap perilaku tertentu seperti merokok, 
 minum-minuman keras, penggunaan obat dan seks adalah perilaku yang 
 menggambarkan kedewasaan (Hurlock, 1980 : 207-209) 
 Seperti periode perkembangan yang lain, masa remaja memiliki tugas- 
 tugas pekembangan tertentu yang harus dikuasai oleh individu. Tugas 
 perkembangan pada masa ini lebih terarah bagi penyesuaian terhadap 
 perubahan-perubahan yang te~adi : 
 1. Menyesuaikan diri terhadap perubahan fisik yang te~adi 
 2. Belajar bergaul dengan lawan jenis 
 3. Belajar mandiri secara emosional dari orangtua dan orang dewasa lain. 
 Sedangkan kemandirian secara ekonomis belum dapat dicapai sebelum 
 remaja beke~a. 
 4. Belajar menguasai kecakapan intelektual konsep yang penting bagi 
 5. Mempelajari nilai-nilai yang berlaku di lingkungan orang dewasa, hal ini 
 seringkali bertentangan dengan nilai yang berlaku di lingkungan teman 
 sebaya. 
 6. Belajar mengembangkan perilaku sosial yang bertanggung jawab (Hurlock, 
 1994 : 209-210) 
 Kemampuan dasar sosialisasi berlangsung selama masa kanak-kanak 
 dan remaja, mereka diharapkan dapat membangun pala tingkah laku (attitude) 
 yang sesuai bagi mereka pada dunia orang dewasa melalui kemampuan dasar 
 ini. Kelompok masyarakat mengharapkan remaja menguasai tugas 
 perkemabangan yaitu : membentuk hubungan baru yang lebih matang dengan 
 kelompok sebaya dari kedua jenis kelamin, pencapaian tingkahlaku sosial yang 
 bertanggung jawab, mengembangkan kemampuan intelektual dan konsep yang 
 penting untuk kompetensi; dan mencapai tingkat kemandirian mel~lui pencapaian 
 sebaya yang selanjutnya, diasumsikan meningkatkan pentingnya dalam 
 mempengaruhi attitude dan tingkah laku. Remaja menghabiskan lebih banyak 
 waktu dengan ternan sebaya, orang dewasa sering khawatir ternan sebaya atau 
 kelompok sebaya akan mengambil alih kontrol kekuasaan terhadap anak 
 mereka. Fuhrman, (1990) (Dalam Irma, 1999: 60 - 61). 
 Ternan sebaya memainkan peranan penting dalam perkembangan 
 psikologis pada kebanyakan remaja. Hubungan ternan sebaya memberikan lebih 
 banyak fungsi yang sarna dalam masa remaja seperti saat masa kanak-kanak. 
 Mereka memberi kesempatan belajar sosial dan mengembangkan ketrampilan 
 dan minat yang sesuai dengan usianya dan berbagi masalah dan perasaan yang 
 sarna (Conger, 1977). Meskipun demikian, untuk sejumlah alasan peran ternan 
 sebaya memainkan peran yang lebih penting pada masa remaja daripada masa 
 kanak-kanak. 
 Ada faktor khusus yang tak terbantah berperan selama masa remaja 
 yang menempatkan kelompok sebaya pad a posisi penting yang tidak biasa. 
 Faktor ini merupakan proses alamiah pada masa remaja. Terdapat 3 aspek 
 dalam proses transisi yang semuanya memajukan pentingnya kelompok sebaya 
 Adelson, (1980) yaitu : 
 1) Selama masa remaja, remaja menghadapi perkembangan fisik yang 
 memerlukan reorganisasi sosial dan emosional. Perubahan fundamental itu 
 memaksa individu beradaptasi dengan pengalaman baru yang tidak 
 diketahui, dan menimbulkan tantangan pada identitas dan self esteem. Hal ini 
 biasanya menimbulkan keterikatan yang besar terhadap dukungan dari orang 
 lain khususnya yang mengalami hal yang sama dalam hidup mereka sendiri. 
 2) Ciri integral remaja adalah pemutusan perlahan-lahan ikatan emosional 
 dengan orangtua. Ini merupakan proses yang kompleks, yang dialami tidak 
 hanya antara pria dan wanita, dimana remaja akhirnya mencapai kebebasan 
 dengan latar belakang lingkungan yang mendukung. Dapat dikatakan bahwa 
 pada beberapa tingkat semua remaja terlibat dalam proses dimana standar 
 orang dewasa dipertanyakan dan otoritas dewasa ditantang serta keterikatan 
 emosional pada orangtua selama masa kanak-kanak lama kelamaan 
 melemah. Pada saat ketidakpastian dan perdebatan makin besar dan saat 
 dukungan paling banyak diperlukan, banyak remaja merasa sulit bahkan 
 tidak mungkin mengungkapkan emosinya pada orangtua. Dalam keadaan ini 
 tidak mengejutkan bahwa teman sebaya memainkan peranan penting. 
 3) Sangat alamiah bahwa transisi melibatkan eksperimentasi. Ciri umum 
 perubahan dari suatu tahap ke tahap lain adalah pentingnya mencoba bentuk 
 tingkah laku baru. Hal ini berlaku pula pada remaja. Remaja harus belajar 
 bagaimana tingkah laku sosial mereka dikendalikan, mereka menemukan 
 bahwa beberapa tingkah laku diterima dan lainnya tidak, yang mana dari 
 kepribadiannya yang disukai dan yang mana ditolak. Mereka menemukan 
 kebutuhan dan motivasinya untuk berinteraksi dengan lingkungan sosial 
 peran apa yang sesuai dengan perkembangan identitasnya. Proses 
 penemuan ini kadang mendapat dukungan, kadang menyakitkan dan 
 memalukan, tergantung keterlibatan kelompok teman sebaya. (Dalam 
 Irma,1999 :60 - 61) 
 o Tidak bertanggung jawab, tampak dalam perilaku mengabaikan pelajaran, 
 misalnya, untuk bersenang-senang dan mendapatkan dukungan sosial. 
 o Sikap yang sangat agresif dan sangat yakin pada diri sendiri 
 o Perasaan tidak aman, yang menyebabkan remaja patuh mengikuti standar- 
 o Merasa ingin pulang, bila berada jauh dari lingkungan yang dikenal 
 o Terlalu banyak berkhayal untuk mengimbangi ketidakpuasan yang diperoleh 
 dari kehidupan sehari-hari 
 o Mundur ke tingkat perilaku yang sebelumnya agar disenangi dan diperhatikan 
 o Menggunakan mekanisme pertahanan seperti rasionalis, proyeksi, berkhayal 
 dan memindahkan (Hurlock, 1994 : 239). 
 1. Sindroma Penerimaan 
 . Kesan pertama yang menyenangkan sebagai akibat dari penampilan yang 
 menarik perhatian, sikap yang tenang, dan gembira 
 . Reputasi sebagai seorang yang sportif dan menyenangkan 
 . Penampilan diri yang sesuai dengan penampilan ternan sebaya 
 . Perilaku sosial yang ditandai oleh kerjasama, tanggung jawab, panjang akal, 
 kesenangan bersama orang-orang lain, bijaksana dan sopan 
 . Matang, terutama dalam hal pengendalian emosi serta kemauan untuk 
 mengikuti peraturan-peraturan 
 . Status sosial ekonomi yang sama atau sedikit diatas anggota-anggota lain 
 dalam kelompoknya ada anggota-anggota lain dalam kelompoknya dan 
 hubungan yang baik dengan anggota-anggota keluarga 
 . Tempat tinggal yang dekat dengan kelompok sehingga mempermudah 
 hubungan dan partisipasi dalam pelbagai kegiatan kelompok. 
 2. Sistem Alienasi 
 . Kesan pertama yang kurang baik karena penampilan diri yang kurang 
 menarik atau sikap menjauhkan diri, yang mementingkan diri sendiri 
 . T erkenal sebagai seorang yang tidak sportif 
 . Penampilan yang tidak sesuai dengan standar kelompok dalam hal daya tarik 
 . Perilaku sosial yang ditandai oleh perilaku menonjolkan diri, mengganggu 
 dan menggertak orang lain, senang memerintah, tidak dapat bekerja sama 
 dan kurang bijaksana. 
 . Kurangnya kematangan, terutama kelihatan dalam hat pengendalian emosi, 
 ketenangan, kepercayaan-diri dan kebijaksanaan. 
 . Sifat-sifat kepribadian yang mengganggu orang lain seperti mementingkan 
 diri sendiri, keras kepala, gelisah, dan mudah marah. 
 . Status sosioekonomis berada di bawah status sosioekonomis kelompok dan 
 hubungan yang buruk dengan anggota-anggota keluarga. 
 . Tempat tinggal yang terpencil dari kelompok atau tidak kemampuan untuk 
 berpartisipasi dalam kegiatan kelompok karena tanggung jawab keluarga 
 atau karena kerja sambilan (Hurlock, 1994 : 217). 
 Menurut Calhoun (1990) kontrol diri merupakan suatu pengaruh dan 
 pengaturan seluruh proses-proses fisik, tingkahlaku dan proses-proses 
 psikologis, atau merupakan rangkaian proses-proses yang menyusun diri 
 (Calhoun, 1990 :116-117). Secara konkrit, Calhoun (1990) menggambarkan 
 penjelasan tentang kontrol fisik, psikologis dan tingkah laku, yaitu : 
 . Kontrol fisik menyangkut kemampuan individu dalam mempengaruhi dan 
 mengatur keadaan diri secara fisik, yaitu kontrol terhadap penampilan diri, 
 gerakan motorik dan perubahan faktor-faktor di dalam tubuhnya. 
 .- Kontrol psikologis, merupakan kemampuan individu dalam mempengaruhi 
 dan mengatur perasaan dan persepsi dalam arti kebutuhan, pikiran dan 
 emosi dalam berhubungan dengan lingkungannya. 
 . Kontrol tingkah laku, bagian ini merupkan gabungan dari kontrol fisik dan 
 kontrol psikologis, menyangkut kemampuan individu dalam mempengaruhi 
 dan mengatur hal-hal yang ingin dilakukan, dimana akibatnya akan 
 mempengaruhi apakah suatu tingkah laku akan ditampilkan atau tidak dalam 
 berhubungan dengan lingkungannya. 
 Self Control (Kontrol Diri) diperlukan, karena : 
 Pertama, individu merupakan suatu anggota dari masyarakat. Setiap individu 
 memiliki kebutuhan masing-masing dan untuk menyalurkannya, diperlukan suatu 
 tidak mengganggu atau merugikan pihak lain (orang-orang atau masyarakat 
 sekitarnya). 
 Kedua, setiap individu belajar dari budaya dengan tujuan kompetensi tertentu, 
 untuk mencapai kebaikan-kebaikan bagi dirinya dan keinginan-keinginan yang 
 lain. Untuk itu, kontrol diri diperlukan terutama pada masyarakat yang 
 berorientasi pada prestasi. Masyarakat kita mendorong secara konstan untuk 
 mengeset I menyusun standar yang tinggi dan lebih tinggi bagi kita sendiri. 
 Dengan demikian, individu harus membuat standarnya sendiri, harus belajar 
 terus menerus untuk mengontrol impuls-impulsnya dan memilih tujuan jangka 
 panjang untuk mendapatkan rewrdnya. Misal : Jika seseorang ingin lulus sekolah 
 dalam 2 tahun, maka ia tidak dapat pergi main bowling pada malam sebelum 
 ujian akhir, seperti yang ia inginkan. Atau jika seseorang ingin merawatl 
 mengasuh keluarga, sebaiknya ia tinggal di rumah dengan merawat adik yang 
 sakit walaupun punya tiket pergi ke konser (Calhoun, 1990 : 116-117). 
 Self control (Kontrol diri) dianggap sebagai lawan dari kontrol eksternal. 
 Dalam kontrol diri, individu menentukan standar performancenya sendiri dan 
 memberi reward atau menghukum dirinya sendiri untuk mencapai atau tidak 
 mencapai standard yang ditentukan oleh individu. Sedangkan dalam kontrol 
 eksternal, orang lain yang menata dirinya, menentukan standard dan 
 memberikan (atau menahan) reward. 
 Mengapa ada perbedaan antara kontrol diri dengan kontrol eksternal, karena 
 kebanyakan dari kita punya kesempatan untuk dipertimbangkan yaitu memilih 
 antara diri dan lingkungan. Seseorang yang memahami kapan kontrol diri 
 digunakan, maka semakin sedikit lingkungan yang melakukan kontrol pada 
 dirinya. Selama seseorang bergantung lingkungan (kontrol eksternal), hidupnya 
 hampir seluruhnya dikendalikan orang lain. Dengan kontrol diri yang kuat, akan 
 tampil menjadi orang yang mudah bergaul dan dapat mengarahkan perilaku 
 sesuai dengan keinginannya. Tapi disisi lain, individu yang terlalu sering 
 menggunakan mekanisme kontrol, akan tamoil sebagai orang yang kaku dan 
 tidakluwesdalam bergaul. (Calhoun, 1990 :117-118). 
 a. Proses 
 Proses-proses belajar merupakan pusat perkembangan kontrol diri. 
 Melalui pengkondisian responden, kita belajar asosiasi dengan stimulus 
 menyenangkan dan menyakitkan, yang karenanya melatih diri kita sendiri untuk 
 menunda -kepuasan. Selain itu, kita belajar mengontrol diri kita sendiri yang 
 bertujuan untuk meningkatkan konsekwensi-konsekwensi yang memuaskan. 
 Tingkah laku kita mungkin diperkuat oleh reinforcement positif (stimulus 
 menyenangkan) atau reinforcement negatif (menghilangkan stimulus tidak 
 menyenangkan) dan tingkah laku mungkin pula dapat 'melemah' melalui 
 punishment atau extinction (pemunahan). 
 Ketika seseorang belajar pada suatu situasi atau suatu respon tertentu, 
 maka pada saat berada pada situasi atau respon lain ia akan memberikan 
 respon yang serupa, disini proses generalisasi terjadi adanya pemindahan 
 prisnsip-prinsip umum yang dipelajari oleh individu pada bidang lain. Ketika 
 seseorang berada pada situasi lain maka kemampuan untuk memberikan 
 respons yang berbeda adalah berdasarkan kemampuan untuk menangkap 
 perbedaan dalam situasi yang dihadapi. Dalam proses belajar diskriminasi ini, 
 seseorang belajar menangkap/memahami hubungan-hubungan yang ada antara 
 stimulus. Selain itu, respon-respon kompleks dapat juga dipelajari melalui 
 shaping (pembentukan). melalui reinforcement ketepatan kesuksesan suatu 
 respon, atau melalui modeling dengan mengobservasi orang lain (Calhoun, 
 1990: 120-131). 
 b. Hasil 
 Secara mendasar. kita belajar tiga hal : 
 Bagaimana mengontrol tubuh, bagaimana mengontrol tingkah laku impulsif dan 
 bag~imana bereaksi pada diri kita sendiri. 
 1. Kontrol Tubuh 
 Saat Jahir. individu dalam pengaruh lingkungan (kontrol eksternal), atau 
 dengan kata lain individu tidak mempuanyai kontrol dalam diri. Individu lakukan 
 hanyalah refleks-refleks bawaan lahir yang otomatis seperti : bernafas. 
 menghisap, mengedipkan mata, buang air kecil dan besar, tertidur dan bangun. 
 Satu tahun pertama setelah tahir, individu akan belajar lebih banyak mengenai 
 kontrol diri dibandingkan yang akan dipelajari seumur hidupnya. Untuk itu dalam 
 awal perkembangan individu belajr ketrampilan dasar mengontrol tubuhnya. 
 Secara berangsur-angsur, dalam perkembangannya individu melewati 
 tahapan perkembangan kontrol diri fisiknya, individu belajar memfokuskan mata 
 dan memperhatikan objek yang bergerak, belajar berbalik dan duduk, serta 
 menguasai ketrampilan yang penting dari koordinasi mata dan tangan : 
 kemampuan meraih dan mengambil sesuatu yang dia lihat, dan di akhir tahun 
 pertama individu tetah mulai mengembangkan dua ketrampilan : bicara dan 
 berjalan. Semua ketrampilan ini merupakan hasil perkembangan fisik sederhana. 
 Ketrampilan awal kontrol diri seperti berjalan, bicara, koordinasi mata dan 
 tangan mudah diterima sebagai hal yang wajar. Anak-anak belajar, sesuai 
 dengan apa yang diinginkan oleh orangtua mereka. Ketika mereka menguasai 
 ketrampilan ini, mereka mendapat reward menjadi bebas dari 
 hukuman/punisment orangtua, dan dengan perasaan berhasil. Pengalaman ini 
 ditetapkan dalam pikiran anak hingga menjelang remaja, sebagai model untuk 
 usaha -di masa mendatang terhadap kemampuan kontrol dirinya yaitu : 
 keengganan awal, penguasaan bertahap, dan rewards terhadap self 
 determination dan persetujuan sosial (Calhoun, 1990: 131-132). 
 2. Kontrol atas tingkah laku impulsif 
 Toilet training merupakan contoh yang paling baik dari perkembangan 
 kontrol diri : Setelah ~ndividu menguasai ketrampilan fungsi fisik, individu juga 
 berupaya untuk mengendalikan tingkah laku impulsif yaitu ketika individu 
 melakukan tindakan cepat untuk kepuasan yang cepat. Pengendalian tingkah 
 laku impulsif melibatkan dua kemampuan : kemampuan menunggu sebelum 
 bertindak dan kemampuan untuk tidak jadi mendapatkan kepuasan yang segera 
 demi reward di masa mendatang yang lebih besar. 
 Mengajarkan kontrol tingkah laku impulsif, terutama terhadap variasi 
 seksual dan agresif, merupakan usaha utama membina sosialisasi masa kanak- 
 kanak. (Aronfread,1968). Salah satu contoh, jika seorang anak telah dipuji untuk 
 perencanaan, menunggu, dan bekerja dalam mencapai tujuan dan jika janji-janji 
 di masa lalu dari rewards pada kenyataannya telah ditepati/diterima, maka anak 
 itu lebih mungkin menunda rewards kecil yang didapat segera untuk kepentingan 
 rewards di masa mendatang yang lebih besar (Mischel, 1966, 1968,1974,1983). 
 Lebih jauh lagi, jika ia harus bekerja dan mencapai sesuatu dengan tujuan 
 memperoleh reward, keyakinannya pada kemampuannya untuk melaksanakan 
 pekerjaan yang diharuskan akan mempengaruhi keputusannya (Mischel, 
 1974;Mischel dan Mischel, 1976, 1983). Dengan kata lain, faktor-faktor kritis 
 yang ditemui, diyakini memiliki orientasi tujuan dan akan mempengaruhi 
 kepercayaan diri (self-confidence), keadaan tersebut ditentukan oleh proses 
 belajar yang dialami sebelumnya. (Calhoun, 1990: 132-133). 
 3. Reaksi-reaksi pada diri 
 Dalam mekanisme belajar kontrol diri, hal yang utama adalah 
 reinforcement ekstemal. Satu dan sekian banyak pembentuk kontrol diri yang 
 kuat adalah reinforcement yang diikuti bersama : reaksi-reaksi individu pada 
 dirinya sendiri. Pada proses terakhir pembentukan kontrol diri ini individu 
 mengevaluasi performance mereka sandin (Bandura & Whalen, 1964, 
 Harter1983). Dalam mengevaluasi performancenya Self- reinforcement akan 
 memilih wujud reward konkrit yang aktual, seperti ketika individu mengancam 
 dirinya sendiri pada saat makan malam yang menyenangkan bahwa ia akan 
 bekerja keras untuk tes. Yang sangat umum, bagaimanapun dan mungkin yang 
 paling kuat - yaitu reinforcement yang individu suplai dalam bentuk perasaan 
 sederhana tentang dirinya yaitu : bangga akan performance yang baik dan malu 
 karena performance yang buruk. 
 Eksperimen-eksperimen telah menunjukkan bahwa pola-pola self 
 reinforcement yang konknt secara cepat dipelajan melalui modelling (Bandura & 
 Whalen,1964; Mischel & Liebert,1966). Untuk reinforcer-reinforcer yang tidak 
 jelas dari self appraise dan self accusation, dapat dipelajari melalui satu 
 kombinasi modelling dan pengkondisian responden. Salah satu contoh, anak 
 yang telah dimarahi oleh ibunya karena melempar anjing dengan balok 
 mengasosiasikan tindakan agresif ini dengan penarikan cinta ibu. Dan akhirnya, 
 ia menggabungkan, melalui belajar, baik standard ibunya (agresi itu buruk) dan 
 hukuman (menarik kasih sayang). Dengan demikian, ketika ia bertingkah laku 
 agresif di masa depan, atau tergoda untuk melakukannya, ia menarik kasih 
 sayangnya sendiri terhadap dirinya sendiri (Sears dkk,1957; Aronfreed,1968). 
 Dengan kata lain, ia merasa bersalah. Seperti yang telah diketahui, rasa salah 
 merupakan stimulus menyakitkan secara ekstrim, satu yang akan membuat 
 individu berusaha untuk menghindari atau membuangnya. Oleh karena itu, 
 kontrol diri berupaya menanggulangi atau mengatasi perasaan salah yang 
 secara jelas ditandai oleh adanya reinforcement negatif, dengan menghilangkan 
 stimulus yang negatif (Calhoun, 1990: 133-134). 
 Jika pengkondisian self control sempurna, maka kontrol tubuh individu, 
 kontrol impuls individu dan rekasi diri individu akan mampu memberi efisiensi 
 yang dapat membuat individu mampu mnengarahkan diri sesuai dengan 
 keinginannya dan masyarakat. Dalam kehidupan nyata, bagaimaanpun kontrol 
 diri individu bervariasi, dimana self defeating dan rasa salah menjadi satu, tidak 
 menghasilkan pemecahan. Salah satu contoh, ketika individu mengalami 
 masalah menjadi banyak merokok, banyak minum, banyak makan, atau menjadi 
 hilang kendali . 
 Menurut teori behavioral, kontrol diri yang buruk dikembangkan dengan 
 cara yang sarna seperti kontrol diri yang baik yaitu melalui proses belajar. 
 sudah tidak sesuai atau tidak memadai lagi. Dalam kasus belajar yang tidak 
 memadai, salah satu contoh: seorang anak kecil yang beke~a keras di sekolah 
 namun tidak menerima feedback (reinforcement) dari orangtuanya, tidak ada 
 dorongan semangat, sebagai hasilnya, respon belajarnya semakin 
 melemah/padam, maka saat ia mencapai sekolah menengah, dia tidak akan 
 dapat melatih self control untuk belajar demi ujian. 
 Sedangkan dalam kasus belajar yang tidak sesuai, salah satu contoh 
 yang tidak baik yaitu seseorang yang tumbuh dengan kedua orangtuanya yang 
 secara konstan terpaksa minum alkohol dalam situasi yang menekan, dimana 
 minum-minuman keras menjadi komponen dasar kelompok untuk bersenang- 
 senang. Dengan model-model dan reinforcement semacam itu, individu tersebut 
 menjadi mengasosiasikan minum dengan terlepas dari ketegangan, penerimaan 
 sosial dan waktu menyenangkan. Sehingga ia menjadi seseorang alkoholik 
 (Calhoun, 1990: 134-135). 
 Banyak penelitian yang dilakukan mencari faktor yang mempengaruhi 
 kontrol diri, Skinner (1953) mengemukakan bahwa kontrol diri dipengaruhi oleh 
 tiga faktor dasar, yaitu : 
 1) Pilihan disengaja 
 2) Pilhan antara dua tingkah laku yang membuat konflik, satu menawarkan 
 kepuasan segera dan yang lain menawarkan suatu reward jangka panjang 
 3) Memanipulasi stimulus dengan maksud membuat suatu tingkah laku kurang 
 mempunyai kemungkinan dan tingkah laku yang lain lebih mempunyai 
 kemungkinan (Calhoun, 1990: 141-142) 
 Istilah penyesuaian diri merupakan perluasan dari istilah adaptasi yang 
 digunakan dalam ilmu biologi untuk menunjukkan kemampuan mempertahankan 
 diri dari spesies-spesies. 
 Konsep dari penyesuaian berasal dari biologi, dalam biologi istilah yang 
 digunakan adaJah adaptasi, suatu konsep yang menjadi bagian penting dari teori 
 evolusi oJeh Darwin (1859), Darwin mengatakan bahwa hanya spesies (dan 
 struktur dan proses biologi secara umum) yang paling pantas untuk beradaptasi 
 yang bertahan terhadap bahaya dari dunia fisiko 8iologi dan fisiologi tetap 
 menaruh perhatian terhadap adaptasi dan menganggap bahwa penyakit-penyakit 
 manusia adalah hasH dari proses fisiologi adaptasi terhadap tekanan (stress) 
 dalam kehidupan (Selye, 1956). 
 Konsep biologi dari adaptasi telah dipinjam oleh psikolog dan dinamai 
 ulang sebagai penyesuaian (adjustment). Penyesuaian dan adaptasi secara 
 bersama-sama mewakili perspektif fungsional untuk memandang dan memahami 
 tingkah laku manusia dan binatang. Tingkah laku ini dipandang sebagai 
 mempunyai fungsi lebih dalam menghadapi atau mengatasi tingkah laku yang 
 diberikan lingkungan kepada individu. Sebagai contoh, pakaian yang dikenakan 
 manusia berbeda-beda sesuai dengan iklim dimana dia tinggaf atau paling tidak 
 secara sebagian adalah adaptasi terhadap cuaca. Pakaian itu menpunyai fungsi 
 untuk menjaga suhu badan agar tetap konstan. 8entuk-bentuk arsitektur juga 
 menunjukkan kecerdasan yang luar biasa dalam menghadapai bahan-bahan 
 mentah dari lingkungan sekitarnya bagi kebutuhan tempat berteduh dan 
 kehangatan. Hal ini diilustrasikan oleh orang eskimo yang membangun rumah 
 dari es dan salju dalam rangka beradaptasi terhadap kejamnya kehidupan kutub 
 utara. Ringkasnya, tingkah laku menggambarkannya sebagai adaptasi terhadap 
 bermacam-macam tuntutan fisik dan penyesuaian kepada tuntutan-tuntutan 
 psikologis. Psikolog lebih menaruh perhatian terhadap apa yang disebut 
 ketahanan psikologi (psychological survival) atau penyesuaian. Dibandingkan 
 terhadap ketahanan fisiologi (physicological survival) atau adaptasi. Paralel 
 dengan konsep biologi dari adaptasi dalam psikologi, tingkah laku diartikan 
 sebagai upaya penyesuaian terhadap tuntutan fisik dan penyesuaian kepada 
 tuntutan - tuntutan psikologis. Penyesuaian atau adjusment terdiri atas proses- 
 proses psikologi yang digunakan oleh individu untuk menghadapi dan mengatur 
 berbagai macam tuntutan dan tekanan. Lazarus mengatakan, dalam melakukan 
 proses penyesuaian sebenarnya ada 2 bentuk proses : Pertama, individu 
 mencocokkan dirinya/menyesuaikan tuntutan dirinya dengan tuntutan yang ada 
 di lingkungannya atau individu di lain pihak mengadakan penyesuaian diri 
 dengan keadaan. Kedua, individu merubah kondisi lingkungan agar sesuai/cocok 
 dengan kebutuhan dirinya atau di lain pihak merubah keadaan untuk disesuaikan 
 dengan kebutuhan (Lazarus, 1976: 15 -18). 
 Dalam analisa psikologi tuntutan ini terdiri atas dua macam, yang pertama 
 adalah secara umum berbentuk sosial atau interpersonal dan hasil-hasil yang 
 didapat dengan hidup saling bergantung dengan orang lain. Orang tua mulai 
 membuat tuntutan terhadap keturunan bahkan pada saat masih bayi untuk 
 menjadi dewasa yang lain melanjutkan untuk mempunyai harapan-harapan 
 terhadapnya tentang bagaimana dia bertingkah laku. Contohnya yang 
 berhubungan dengan perkawinan, karir dan dimana ia tinggal. tstri-istri 
 mempunyai harapan-harapan terhadap suami-suaminya, atasan terhadap 
 bawahannya, orangtua kepada anaknya dan anak terhadap orangtuanya. 
 Harapan-harapan ini mempunyai tekanan yang besar terhadap individu. 
 Tuntuan yang kedua secara umumnya adalah internal yang timbul 
 sebagian dari penyempurnaan biologis manusia yang menentukan kondisi- 
 kondisi fisik tertentu seperti makanan, air, dan kehangatan untuk kenyamanan 
 dan bertahan hidup dan sebagian lagi apa yang telah dipelajari pada masa 
 lalunya yang menginginkan kondisi-kondisi sosial tertentu seperti pembenaran 
 dan pencapaian hasil-hasil tertentu. (Lazarus, 1976: 15 -18). 
 Berdasarkan hat ini, Lazarus menyatakan penyesuaian diri adalah 
 kemampuan seseorang dalam bereaksi atau bertingkah laku secara bertanggung 
 jawab baik dalam bersikap atau ter1ibat dalam kegiatan saat menghadapi 
 berbagai tuntutan lingkungan yang dihadapi. Penyesuaian diri menunjukkan hasil 
 performance yang terjadi dari fungsi tingkah laku, performance dari seseorang 
 saat berinteraksi dengan lingkungannya menunjukkan seberapa jauh individu 
 dapat meyelaraskan keinginan diri dengan keinginan lingkungan dengan 
 berhasil. 
 Setiap kejadian-kejadian psikologi dalam manusia dipengaruhi oleh 
 setting-setting sosial. Pada saat adanya kehadiran orang lain maka pikiran- 
 pikiran, perasaan-perasaan dan tindakan-tindakan kita dalam takaran tertentu 
 diarahkan oleh orang-orang tersebut, meskipun pada saat tertentu kita hanya 
 sendirian tetapi pengalaman-pengalaman sosial pada masa lalu memainkan 
 peranan penting dalam menentukan tindakan kita. 
 Lazarus menyatakan, tidak mungkin membicarakan penyesuaian diri 
 tanpa kepribadian orang yang menjalaninya, Adjustment dan Personality dibatasi 
 dua cara dengan : pertama, sejarah sosial dari suatu individu membentuk 
 kepribadian tertentu, dan yang kedua fungsi-fungsi kepribadian dalam setting 
 sosial saat sekarang, yang dapat membuat individu bereaksi dalam cara yang 
 unik terhadap stimuli yang sarna. Sejarah sosial pad a masa lalu seseorang 
 mempengaruhi perkembangan kepribadiannya. Kepribadian dikendalikan oleh 
 pemilihan yang mana keadaan situasi dan kondisi sosial menyita perhatian dan 
 diterjemahkan keadaannya oleh antar pribadi individu (Lazarus, 1976 : 439). 
 a) Pengaruh sosial alamiah 
 Dari berbagai fa kto r yang mempengaruhi penyesuaian diri dan 
 kepribadian, salah satu faktor penting adalah keluarga. Ini adalah karena dari 
 semua kelompok sosial, keluarga merupakan yang paling alamiah dan individu 
 itu sendiri bagian integral di dalamnya. Supaya anak mencapai penyesuaian 
 yang sehat dan stabilitas psikologis, maka ia harus mengembangkan rasa aman 
 yang mendalam dan merasa dimiliki. Biasanya keluarga mampu memberi rasa 
 aman. Hal-hal dalam keluarga yang mempengaruhi penyesuaian diri adalah 
 keterlibatan hubungan anak - orangtua, adanya variasi kebuadayaan dan sistim 
 sosial dalam menyampaikan dan mengirimkannya melalui pola-pola ketika anak- 
 anak masih sangat muda, seperti hukuman dan disiplin, terlalu memanjakan 
 atau terlalu melindungi. Tipe kebiasaan parental terhadap anak akan mendukung 
 pembentukan peran sosial anak dalam keluarga, penerimaan atau penolakan 
 dan identifikasi. 
 Dilibatkannya kebiasaan-kebiasaan orangtua yang telah diterapkan dalam 
 melakukan interaksi, menentukan keberhasilan individu dalam menerima, 
 merasakan, mempercayai, bersikap terhadap keadaan yang memliki tujuan yang 
 panjang dan pendek; serta untuk berhubungan secara verbal dan non-verbal. 
 (Lazarus, 1976 : 439-440). 
 b) Mekanisme Pengaruh sosial - Penguasaan Moral 
 Moral berkenaan dengan bagaimana penilaian dan perasaan seseorang 
 mengenai diri dan kondisi lingkungan, berkaitan dengan standar-standar sosial 
 yang telah diterapkan yang berupa peraturan-peraturan yang harus dipatuhi. 
 Penilaian mencakup hal-hat yang positif dan negatif. individu yang memiliki 
 pandangan yang positif terhadap perilaku dan lingkungannya akan merasa 
 dirinya disukai, berharga dan diterima. la akan lebih percaya diri dan lebih 
 mampu mengekspresikan potensinya. Hal ini membantunya untuk memudahkan 
 menyesuaikan diri dengan peraturan-peraturan yang telah ditetapkan. Sebaliknya 
 individu dengan penilaian negatif akan lebih banyak menghayati emosi-emosi 
 negatif, disertai oleh rendahnya rasa penyesuaian diri yang rendah. Hal ini 
 seakan mengarahkan individu untuk merasa senang atau tidak terhadap kondisi 
 lingkungan sehingga mempengaruhi tingkah laku moral positif, tergantung 
 kecenderungan konsistens untuk menilai suatu situasi sebagai suatu tantangan 
 atau menilai suatu kerugian dan ancaman sebagai sesuatu yang bisa diatur dan 
 diatasi (Lazarus, 1976: 472-474). 
 c) Pengaruh sosial sebaya 
 Pada pengertian proses penyesuaian diri, perlu rnerniliki inforrnasi tentang 
 kelornpok sosial yang rnernpengaruhi kelornpok individu itu sendiri. Kelornpok- 
 kelornpok sosial yang ada, rnerniliki aturan yang harus diterapkan pada rnasing- 
 secara urn urn tanpa rnenghiraukan tipe-tipe 
 kepribadian. 
 Teori psikologi sosial dan penelitian lain rnenyetujui adanya pengaruh 
 lingkungan sebaya terhadap individu dari kelornpok-kelornpok sosial yang ada. 
 Sebagai contoh, dalarn interaksi sosial seseorang dapat rnerasa dengan siapa ia 
 dapat rnelakukan kontak sosial dan rnernbuat penilaian tentang lingkungan sosial 
 dari reaksi rnereka. Berdasarkan ketrarnpilan berelasi yang dirniliki seseorang 
 dapat rnerasakan dan rnenginterpretasikan, orang-orang yang dihadapinya, 
 aturan - aturan dan norma sosial yang digunakan oleh orang lain, serta 
 kepribadian rnasing-rnasing individu yang dihadapi. 
 Sernua faktor interaksi ini berhubungan dengan aturan persepsi so sial 
 lingkungannya, karena pada situasi sosial, seseorang dapat rnerasa rnenjadi 
 bagian dari kelornpok atau rnerasa ditolak oleh kelornpok lainnya. Terutarna 
 individu yang rnernililki standar lebih tinggi atau kuat daripada individu lainnya. 
 T erdapat rnacarn-rnacarn kelornpok sosial yang berdasar pad a standar dan nilai- 
 nilai rnereka, berdasar pada besar kecilnya kelornpok dan berdasar pada pola 
 pernirnpin yang beragarn. 
 Pada beberapa situasi sosial, aturan individu dibedakan ada yang 
 yang rnerniliki aturan tidak jelas atau berubah-ubah. Kadang-kadang aturan 
 sosial yang diterapkan oleh kelornpok rnerupakan hal yang harmonis sejalan 
 dengan konsep diri individu sendiri, sementara pada keadaan lainnya, aturan 
 terse but membuat konf1ik pada diri individu. Demikian pula, dengan perilaku yang 
 ditampilkan individu kadang dapat diterima, dan pada saat lainnya tidak dapat 
 diterima oleh kelompok atau individu lain. 

