Seiring berkembangnya zaman dan gaya hidup, setiap orang dituntut dapat 
bekerja untuk memenuhi semua kebutuhan. Kesempatan meraih pendidikan tinggi 
dan kesempatan bekerja yang terbuka untuk semua individu membuat setiap orang 
akan berusaha agar menjadi yang terbaik, maka setiap individu akan berlomba-
lomba untuk meraih jenjang pendidikan dan prestasi yang baik demi mendapatkan 
pekerjaan dan posisi yang tinggi, sehingga mendapatkan penghasilan yang dapat 
memenuhi semua kebutuhan. 
Di samping pentingnya pekerjaan, tidak dipungkiri bahwa kehidupan 
keluarga juga penting. Dalam sistem sosial budaya di Indonesia, peran dan 
tanggung jawab bagi kelancaran dan keselamatan rumah tangga ada di tangan 
wanita, sedangkan peran ayah atau bapak lebih dikaitkan sebagai penghasil dan 
penyangga pendapatan rumah tangga. Seiring dengan terbukanya kesempatan 
untuk menempuh pendidikan dan memiliki pekerjaan bagi wanita, peran ibu 
rumah tangga berpengaruh dalam mendukung perekonomian. Dengan demikian, 
struktur keluarga tradisional mengalami pergeseran. 
Wanita yang bekerja memiliki kebutuhan-kebutuhan yang ingin mereka 
inginkan, misalnya kebutuhan untuk relasi sosial atau kebutuhan untuk aktualisasi 
diri di masyarakat serta keinginan menyejahterakan keluarganya. Kebutuhan 
finansial terkadang menjadi hal yang membuat ibu rumah tangga ikut serta dalam 
dunia pekerjaan. Tidak terpenuhinya semua kebutuhan rumah tangga dari 
penghasilan suami, membuat istri ikut berperan aktif untuk bekerja agar 
memenuhi kebutuhan rumah tangganya. 
Meningkatnya jumlah wanita yang bekerja terjadi di kota-kota besar di 
Indonesia, salah satunya adalah Jakarta. Jakarta yang menjadi ibu kota, banyaknya 
perusahaan berdiri di Jakarta. Perusahaan pun tidak membatasi hanya pada pria 
saja, tetapi wanita pun diberi kesempatan yang luas untuk bekerja. Banyaknya 
wanita yang bekerja di Jakarta mengalami peningkatan dari tahun ke tahun. Pada 
tahun 2013 ini, jumlah wanita yang bekerja sebanyak 51,73 ribu orang 
(http://jakarta.bps.go.id/fileupload/brs/2013_05_06_16_04_08.pdf). 
Di Jakarta, peningkatan presentase perempuan yang bekerja dari 37.03 % 
tahun 2005 menjadi 44. 86 % tahun 2010; sedangkan presentase perempuan yang 
tinggal dan mengurus rumah tangga menurun yaitu dari 43. 32 % tahun 2005 
menjadi 38.77 % tahun 2010 (Badan Pemberdayaan Masyarakat Dan Perempuan 
Dan Keluarga Berencana Provinsi DKI Jakarta 2011:19). Data ini menunjukkan 
bahwa semakin banyak perempuan yang  keluar rumah  untuk bekerja memenuhi 
kebutuhan hidup yang semakin mahal di Jakarta 
(http://www.politik.lipi.go.id/in/kolom/jender-and-politik/794-sistem-pendukung-
perempuan-pekerja.html). 
Seluruh sektor industri di Jakarta memberikan peluang bagi wanita untuk 
bekerja, salah satunya adalah PT  X  yang merupakan perusahaan yang bergerak 
dalam Hutan Tanam Industri, yang memiliki visi untuk menjadi perusahaan hutan 
berkelas dunia, dengan pengelolaan hutan secara lestari dan sinergi dengan 
kehidupan sosial masyarakat, ekonomis dan ramah lingkungan. Selain visi, PT. 
 X  Jakarta tersebut memiliki misi yaitu untuk profesional dalam mengelola dan 
mengembangkan sumber daya hutan dalam rangka menciptakan nilai bagi para 
stakeholders dengan mengembangkan hutan tanaman berkualitas tinggi yang 
berkelanjutan dengan biaya terbaik dan risiko terendah untuk memasok bahan 
baku kertas. Menyediakan kesempatan kerja dan bisnis untuk industri umum  
terkait dan juga meningkatkan kesejahteraan masyarakat sekitar. Melestarikan 
hutan alam dan meningkatkan kesejahteraan di sekitar lingkungan hutan. 
Berkontribusi untuk pendapatan pajak pemerintah dan menghasilkan laba yang 
memuaskan. 
PT.  X  yang berdomisili di Jakarta, dapat menyerap tenaga kerja 
sebanyak 320 orang karyawan. Sebanyak 212 orang yaitu karyawan pria dan 
sebanyak 108 orang yaitu karyawan wanita. Sebanyak 66 orang karyawan wanita 
yaitu karyawati yang belum menikah dan sebanyak 42 orang karyawan wanita 
yaitu karyawati yang sudah menikah dan memiliki anak. PT.  X  Jakarta 
memberikan kesempatan bagi siapa saja yang ingin berkontribusi di perusahaan 
tersebut, kesempatan tersebut terbuka juga bagi karyawati yang sudah 
berkeluarga. 
PT.  X  Jakarta menuntut kepada setiap karyawannya untuk dapat bekerja 
secara optimal, tanpa terkecuali karyawati yang sudah berkeluarga. Mereka 
dituntut untuk bekerja dengan baik untuk mencapai visi dan misi dari perusahaan. 
memiliki performance kerja yang baik berkewajiban untuk dapat berkontribusi 
dalam pekerjaan. Mereka diharuskan untuk dapat berperan aktif dalam tim 
kerjanya agar dapat menyelesaikan pekerjaannya. Mereka memiliki kewajiban 
untuk dapat menyelesaikan tugas-tugas sesuai target, terkadang untuk 
menyelesaikan tugas karyawati di PT.  X  Jakarta diharuskan untuk lembur agar 
dapat menyelesaikan pekerjaannya sehingga mereka pulang tidak tepat waktu. 
Karyawati PT.  X  Jakarta pun diharuskan melakukan perjalanan dinas ke Riau, 
Palembang, Jambi, Kalimantan Timur di Sebulu dan Kalimantan Barat Pontianak 
untuk mengontrol perkerjaan sehingga mereka harus meninggalkan keluarga. 
Disisi lain, istri juga berperan sebagai istri dan juga ibu rumah tangga, 
mereka memiliki kewajiban untuk dapat berkontribusi dalam keluarga 
menjalankan perannya sebagai seorang istri dan juga ibu. Mereka memiliki 
kewajiban untuk dapat melayani kebutuhan suami, seperti menyiapkan makan, 
pakaian dan mendengarkan keluh kesah suami dalam pekerjaan. Tugasnya sebagai 
ibu adalah mengasuh anak, membimbing anak dalam belajar, serta memenuhi 
kebutuhan keluarga. Karyawati memiliki anak berusia balita mereka dituntut 
untuk memberikan perhatian yang intensif kepada anaknya, sehingga mereka 
dituntut untuk memiliki waktu yang lebih banyak untuk keluarga. 
Karyawati yang sudah berkeluarga memiliki dua peran, yaitu peran di 
pekerjaan dan peran di keluarga. Mereka dituntut untuk dapat berkontribusi 
dengan baik dalam dua perannya tersebut. Jika mereka tidak dapat berkontribusi 
dengan baik, maka akan terjadi ketidakseimbangan antara tuntutan di pekerjaan 
dan tuntutan di keluarga. Karyawati yang sudah bekeluarga diharuskan untuk 
bekerja secara profesional, mereka diharuskan datang dan pulang kerja sesuai 
dengan aturan yang telah ditetapkan oleh perusahaan, bekerja dengan penuh 
konsentrasi. Selain itu perannya dalam keluarga, menuntut untuk dapat memiliki 
waktu yang lebih banyak untuk keluarga.  Terjadinya ketidakseimbangan antara 
pekerjaan dan keluarga maka akan terjadi konflik antar peran (interrole conflict). 
Interrole conflict yaitu seseorang yang menjalani dua peran atau lebih 
secara bersamaan saat pemenuhan tuntutan dari suatu peran bertentangan dengan 
pemenuhan tuntutan dari peran yang lain. Konflik antar peran tersebut dapat 
mengakibatkan suatu konflik antara pekerjaan dan keluarga atau disebut dengan 
work family conflict. Work family conflict adalah sebuah bentuk interrole conflict 
dimana tekanan peran yang berasal dari pekerjaan dan keluarga saling mengalami 
ketidakcocokan dalam beberapa karakter. Dengan demikian, partisipasi untuk 
berperan dalam pekerjaan (keluarga) menjadi lebih sulit dengan adanya partisipasi 
untuk berperan di dalam keluarga (pekerjaan). (Khan et al. dalam  Greenhaus dan 
Beutell (1985)). 
Work family conflict dapat terjadi karena tuntutan peran di pekerjaan 
mempengaruhi pemenuhan tuntutan peran di keluarga atau tuntutan  di keluarga 
mempengaruhi pemenuhan tuntutan di pekerjaan. Tuntutan peran di pekerjaan 
seperti waktu kerja yang padat, tidak teratur, perjalanan kerja yang padat, 
pekerjaan yang berlebihan dan bentuk-bentuk lain dari stress kerja, adanya konflik 
interpersonal di tempat kerja, career transition, serta supervisor atau organisasi 
yang tidak mendukung. Selain itu tuntutan peran di keluarga seperti kehadiran 
anak, masih mempunyai tanggungjawab utama pada anak usia balita dan remaja, 
mempunyai konflik dengan anggota keluarga dan keberadaan anggota keluarga 
yang tidak mendukung. (Greenhaus, 1985). 
Berdasarkan hasil survey yang dilakukan pada 4 orang karyawati PT.  X  
Jakarta. Sebanyak 80% atau 3 orang menyatakan bahwa mereka mengalami 
konflik  waktu pada perannya dipekerjaan yang mempengaruhi perannya di 
keluarga. Pekerjaan yang mana mereka dituntut untuk mengerjakan pekerjaan 
dengan tepat waktu dan apabila diharuskan untuk lembur maka untuk menikmati 
waktu bersama suami dan anakpun berkurang. Terkadang mereka harus lebih awal 
untuk pergi karena jarak tempuh yang jauh dan kemacetan pada jam berangkat 
kerja karena mereka memasuki kawasan perkantoran. Selain itu saat pulang 
terkadang anak sudah tidur karena pulang larut malam sehinga menyebabkan 
mereka tidak memiliki kesempatan untuk menjalankan perannya sebagai ibu. 
Sebanyak 20% atau 1 orang menyatakan bahwa mereka mengalami 
konflik pada perannya di pekerjaan yang mempengaruhi perannya di keluarga. 
Kelelahannya pada perannya sebagai pekerja membuatnya tidak dapat berperan 
secara maksimal dalam keluarga.setelah seharian bekerja sesampainya dirumah 
mereka sudah kelelahan karena pekerjaan dan situasi macet dijalanan. Sehingga 
perannya sebagai istri ataupun ibu kurang maksimal mereka tidak dapat 
memantau perkembangan anak, bercerita dengan anak sharing bersama suami 
yang telah mereka lalui, begitu juga suami dan mereka terkadang sulit untuk 
menentukan waktu berlibur bersama karena suami juga bekerja dan memiliki 
aturan berlibur yang berbeda. 
Selain itu karyawan PT  X  Jakarta pun mengalami konflik pada waktu 
untuk keluarga, mereka ketika pulang kerja harus menjemput anak dirumah 
orangtuanya pada malam hari bahkan larut malam sepulang bekerja, anak sudah 
dalam keadaan tidur, sehingga mereka tidak dapat menjalankan peranya sebagai 
ibu karena waktu yang mereka miliki sudah dihabiskan untuk pekerjaan. 
Hal-hal yang telah dijelaskan diatas memiliki ketidakcocokan dengan 
tuntutan yang dihasilkan dari suatu peran dapat menghambat pemenuhan tuntutan 
dalam peran lainnya. Hal ini akan memicu terjadinya konflik antar peran 
(interrole conflict). Konflik antar peran yang dialami oleh karyawati berkaitan 
dengan peran mereka di pekerjaan dan di keluarga. Dari fenomena yang 
dipaparkan terlihat bahwa usaha karyawati yang sudah berkeluarga untuk 
memenuhi peran di pekerjaan menghambat peran di keluarga, begitu pula 
sebaliknya. Berdasarkan fenomena yang telah dipaparkan diatas peneliti tertarik 
untuk meneliti dimensi work family conflict pada karyawati yang sudah 
berkeluarga di PT  X  Jakarta. 
Dari penelitian ini ingin diketahui bagaimana work family conflict pada 
karyawati yang sudah berkeluarga di PT.  X  Jakarta. 
Penelitian ini memiliki maksud untuk memperoleh gambaran mengenai 
work family conflict pada karyawati yang sudah berkeluarga di PT.  X  Jakarta. 
Penelitian ini memiliki tujuan untuk melihat dimensi work family conflict 
pada karyawati yang sudah berkeluarga di PT  X  Jakarta, yang muncul berupa 
dimensi mana yang hasilnya berada pada derajat yang tinggi, sedang, dan rendah 
dari work family conflict, yaitu Time Based WIF, Time Based FIW, Strain Based 
WIF, Strain Based FIW, Behavior WIF, dan Behavior FIW. 
1. Menjadi bahan masukan bagi ilmu Psikologi khususnya dalam bidang 
Psikologi Industri dan Organisasi juga Psikologi Keluarga mengenai 
work family conflict ( pada karyawati yang sudah berkeluarga di PT. 
 X  Jakarta). 
2. Memberikan sumbangan informasi kepada peneliti lain yang tertarik 
untuk meneliti mengenai work family conflict dan mendorong 
dikembangkannya penelitian-penelitian lain yang berhubungan dengan 
topik tersebut. 
1. Memberikan informasi kepada karyawati yang sudah berkeluarga di 
PT.  X  Jakarta mengenai konflik yang dialami pada perannya sebagai 
pekerja dan sebagai istri, sehingga senantiasa dapat mengantisipasi 
masalah-masalah yang akan timbul yang diakibatkan karena work 
family conflict. 
2. Memberikan informasi kepada karyawati yang sudah berkeluarga di 
PT  X  Jakarta mengenai konflik yang dialami pada perannya sebagai 
pekerja dan ibu rumah tangga, sehingga dapat mengantisipasi masalah-
masalah yang dapat timbul dari akibat work family conflict. 
Wanita yang sudah bekeluarga pada umumnya memiliki tanggung jawab 
penuh sebagai ibu rumah tangga yang memiliki tugas untuk berkontribusi penuh 
dalam keluarga tetapi sejalan dengan kesempatan menempuh pendidikan yang 
tinggi bagi wanita, membuat wanita antusias untuk memperoleh pendidikan yang 
tinggi. Setelah memperoleh pendidikan yang tinggi, wanita memiliki keinginan 
untuk mengaplikasikan ilmu yang diperolehnya di bangku pendidikan di dunia 
pekerjaan. Hal ini membuat wanita berlomba-lomba mengejar karir mereka sesuai 
dengan bidang yang mereka peroleh di bangku pendidikan. 

