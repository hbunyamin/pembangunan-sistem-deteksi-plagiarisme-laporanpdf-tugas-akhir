Dari definisi tersebut dapat disimpulkan bahwa pengambilan keputusan karir 
merupakan hal yang penting dalam perencanaan karir, karena untuk merencanakan karir perlu 
dilakukan pengambilan keputusan karir terlebih dahulu untuk mengetahui apa yang menjadi 
tujuannya. Hal itu juga diperkuat dengan pendapat Hodkinson (1998, dalam Greenbank dan 
Hepworth, 2008) yang mengatakan pengambilan keputusan karir yang dilakukan melalui 
pendekatan rasional sering dianjurkan sebagai sarana memaksimalkan peluang karir. Menurut 
Gati, Krausz dan Osipow (1996) individu bisa dikatakan telah melakukan pengambilan 
keputusan karir apabila ia telah mengambil keputusan, menentukan sejumlah alternatif yang 
dipersiapkan atas karirnya, dan mempertimbangkan beberapa   aspek   dalam   alternatif   yang   
ditentukan   karirnya.   Pengambilan keputusan karir bagi para individu merupakan suatu hal 
yang penting dan dapat berdampak besar dalam penentuan karir masa depan.  
Dengan pengambilan keputusan karir, individu dapat melakukan setiap tindakannya ke 
suatu arah karir yang jelas dan tidak melenceng. Dengan melakukan pengambilan keputusan 
karir  individu  dapat  lebih  termotivasi  dalam  melakukan  kegiatan perkuliahannya karena 
sudah mengambil keputusan mengenai karirnya. Ketika  melakukan  pengambilan  keputusan  
karir,  setiap  orang  dituntut untuk dapat berpikir kritis dan melakukan evaluasi diri (Brown, 
2002).  
Banyak individu yang akhirnya merasa kesulitan atau kebingungan saat dihadapkan 
dengan pengambilan keputusan untuk karirnya. Beberapa ahli mencoba untuk mendefinisikan 
kesulitan mengambil keputusan karier, menurut Osipow (1999) kesulitan mengambil keputusan 
karier merupakan keadan yang akan datang dan pergi setiap keputusan dibuat, dilaksanakan, 
bertumbuh, dan akhirnya mengarah kepada kebutuhan untuk membuat keputusan yang baru 
yakni menghasilkan kesulitan yang baru. 
Penekanan tersebut akan lebih terasa terutama pada mahasiswa, karena para mahasiswa 
merupakan orang-orang yang terbiasa untuk berpikir analitis serta mampu berpikir secara   lebih   
mendalam,   hal   itu   juga   sesuai   dengan   masa   perkembangan kognitifnya dimana pada 
usia dewasa awal kemampuan berpikir abstrak dan berpikir menggunakan alasan yang logis 
sedang berkembang (Papalia, Olds, dan Feldman,2009). Seorang mahasiswa harus bisa 
mengenal apa yang menjadi kelebihan dirinya dan  kekurangan  dirinya,  serta  apa saja  minat  
yang ia miliki. Apabila mahasiswa tersebut telah dapat mengenali dirinya tentu mahasiswa 
tersebut harus mengetahui lebih jauh mengenai karir apa yang diminatinya, dan setelah 
melakukan  pengamatan  tersebut  maka  mahasiswa  harus  menyesuaikan  antara minatnya  
dengan  apa  yang  ada  pada  dirinya  sebagai  modal  dalam  berkarir. Setelah mahasiswa dapat 
menentukan karir apa yang akan dijalaninya nanti, seorang mahasiswa juga perlu melakukan 
pemikiran mendalam mengenai perencanaan bagaimana cara mencapai targetnya tersebut. 
Seseorang yang berkuliah atau biasa disebut dengan istilah mahasiswa di tuntut untuk 
menyelesaikan kuliahnya tepat waktu, karena ini merupakan salah satu syarat untuk bisa 
diterima bekerja (http://www.id.karir.com). Mahasiswa yang siap untuk lulus untuk tahun ini 
adalah mahasiswa yang masuk kedalam tahun ajaran 2011. Mahasiswa  semester  akhir  
program  sarjana  menjadi  sangat  penting karena mahasiswa merupakan manusia yang sudah 
dipersiapkan untuk terjun di dunia pekerjaan. Mahasiswa tingkat akhir yang dimaksud dalam 
hal ini adalah mereka yang berada pada semester 8 keatas. Sebagian besar mahasiswa yang 
lulus dari perguruan tinggi akan bekerja, sedangkan hanya sebagian kecil yang akan 
melanjutkan ke jenjang pendidikan selanjutnya. Dipilihnya mahasiswa semester akhir program 
sarjana juga karena mereka sudah sangat dekat dengan dunia pekerjaan. 
Seorang mahasiswa harus bisa mengenal apa yang menjadi kelebihan dirinya dan 
kekurangan dirinya, serta apa saja minat yang ia miliki. Apabila mahasiswa tersebut telah dapat 
mengenali dirinya tentu mahasiswa tersebut harus mengetahui lebih jauh mengenai karier apa 
yang diminatinya, dan setelah melakukan pengamatan tersebut maka mahasiswa harus 
menyesuaikan antara minatnya dengan apa yang ada pada dirinya sebagai modal dalam 
berkarier. Setelah mahasiswa dapat menentukan karier apa yang akan dijalaninya nanti, seorang 
mahasiswa juga perlu melakukan pemikiran mendalam mengenai perencanaan bagaimana cara 
mencapai targetnya tersebut.  
Untuk mencapai target para mahasiswa ini dibekali keilmuan atau soft skills sesuai 
bidang yang dipilih yang ada di Fakultas atau jurusan kuliah. Asumsi peneliti hampir seluruh 
mahasiswa memiliki pemikiran yang ideal akan gambaran tentang karirnya. Jelas seperti 
jurusan kedokteran akan mencetak dokter, walaupun harus melalui proses yang panjang. 
Mahasiswa psikologi akan menjadi seorang psikolog. Namun tidak seluruh mahasiswa 
berujung dengan karir yang ideal seperti yang sudah dipikirkan, ada saja para mahasiswa yang 
akhirnya berbelok arah tidak sesuai dengan tujuan di awal masuk perkuliahan. Disini lah peran 
Univeritas sangat lah penting sebagai Institusi yang memberikan skills untuk tetap menjaga 
konsistensi mahasiswa untuk tetap berada di area karirnya. 
Salah satu perguruan tinggi yang mencetak sarjana siap kerja/berkarier adalah 
Universitas  X  yang ada di kota Bandung. Universitas  X  di kota Bandung ini merupakan 
salah satu Universitas swasta yang favorit di kota Bandung ini terlihat dengan terus 
meningkatnya jumlah mahasiswa baru dari tahun ke tahun. Ada berbagai macam fakultas di 
Univeristas  X , diantaranya adalah Fakultas Psikologi, Fakultas Kedokteran, Fakultas Teknik, 
Fakultas Ekonomi, fakultas Hukum, Faklutas Sasatra, Fakultas Seni Rupa dan Desain, Fakultas, 
dan Teknik Informatika.  
Terkait dengan karier dalam dunia kerja, pihak Univeritas  X  sudah berupaya untuk 
memfasilitasi bantuan karier. Hal ini dilakukan dengan cara selalu mengadakan jobs fair, 
pemasangan informasi seputar lowongan kerja perusahaan yang membutuhkan lulusan sarjana, 
yang dipasang di setiap mading bertempat di Tatausaha setiap Fakultas maupun di mading-
mading ruang dosen Fakultas. Pemasangan info mading ini dimaksudkan untuk memberikan 
informasi tentang dunia pekerjaan kepada mahasiswa agar mahasiswa mampu 
mempertimbangkan pilihan kariernya di masa depan melalui keilmuannya dan bimbingan karir 
yang dibuka oleh pihak Universitas untuk bisa membantu para lulusannya untuk bisa bekerja. 
Informasi tersebut harusnya membantu mahasiswa untuk mengetahui peluang karier yang ada 
di dunia kerja nanti. Diharapkan nanti mahasiswa memiliki rencana akan berkarier kemana 
setelah lulus kuliah. 
Terkait dengan fenomena di atas peneliti melakukan wawancara terhadap mahasiswa 
angkatan 2011 Universitas  X  terkait Career Decision Making Difficulties. Dari wawancara 
kepada 20 orang yang terdiri dari 3 mahasiswa Fakultas Kedokteran angkatan 2011, 3 
mahasiswa Fakultas Sastra angkatan 2011, 2 mahasiswa Fakultas Teknik angkatan 2011, 2 
mahasiswa Fakultas Teknik angkatan 2011, dan 4 mahasiswa Fakultas Psikologi angaktan 
2011, 2 mahasiswa Fakultas Hukum angkatan 2011, 2 mahasiswa Fakultas Ekonomi angkatan 
2011, dan 2 mahasiswa Fakultas Teknik Informatika angkatan 2011,  diperoleh data bahwa 11 
mahasiswa (59%) masih bingung akan berkarier kemana, sementara 9 (41%) mahasiswa 
mengatakan akan mencari pekerjaan dalam bidang keilmuanya. 11 Mahasiswa (58%) telah 
mengetahui karakteristik kariernya dan menyebutkan belem mengetahui Jobdesk dari 
pekerjaannya, sementara sisanya yaitu 9 (42%) mahasiswa sudah mengetahui mngenai 
karakteristik kariernya dengan detail dan kurang mengetahui Jobdesk dari pekerjaannya. 
Kemudian terkait dengan kesiapan, sebanyak 13 orang (66%) belum siap untuk berkarier, dan 
7 orang (34%) sudah siap untuk memilih pekerjaan. Kemudian terkait dengan konsistensi 
informasi, sebanyak 17 orang (85%) masih belum terlalu paham dengan informasi di pekerjaan 
yang mereka minati, dan 3 orang (15%) sudah mantap dengan informasi yang mereka miliki.  
Dari data hasil wawancara diatas dapat dikatakan bahwa sebagian besar mahasiswa 
sudah tahu informasi tentang jalur karier di dunia pekerjaan, namun belum memiliki tujuan 
karier yang akan di tuju dan belum memiliki kesiapan dan belum paham tentang karier yang 
mereka inginkan, namun beberapa mahasiswa juga ditemukan sudah memiliki tujuan karier. 
Sementara pengambilan keputusan kariernya masih dirasakan belum mantap. Dari pemaparan 
dan fenomena di atas menarik minat peneliti melakukan penelitian mengenai kesulitan 
pengambilan keputusan karier (career decision making difficulties) yang akan dilakukan 
terhadap mahasiswa, terutama terhadap mahasiswa angkatan 2011 di Universitas  X  di kota 
Bandung. 
Berdasarkan latar belakang masalah yang dikemukakan di atas, dari penelitian ini ingin 
mengetahui tingkat kesulitan pengambilan keputusan karier (career decision making 
difficulties) pada mahasiswa angkatan 2011 di Universitas  X  di Kota Bandung 
Maksud penelitian ini adalah untuk memperoleh gambaran tingkat kesulitan pengambilan 
keputusan karier (career decision making difficulties) pada mahasiswa angkatan 2011 di 
Universitas  X  di Kota Bandung 
Adapun tujuan dari penelitian ini adalah : 
1. Memperoleh data empiris mengenai tingkat kesulitan pengambilan keputusan karier 
(career decision making difficulties) pada mahasiswa angkatan 2011 di Universitas  X  di 
2. Memperoleh informasi di Universitas  X  di kota Bandung mengenai keterkaitan antara 
tingkat kesulitan mahasiswa angkatan 2011 dalam pengambilan keputusan karier (career 
decision making difficulties), dengan faktor-faktor yang mempengaruhi.  
1. Hasil penelitian ini diharapkan dampat menjadi sumber informasi mengenai gambaran 
secara kasar mahasiswa di Indonesia yang dapat digunakan untuk penelitian 
berikutnya.  
2. Selain itu hasil penelitian ini diharapkan dapat menjadi rujukan bagi pembentuk 
kurikulum di Universitas  X  agar mahasiswa lebih siap menghadapi karirnya. 
3. Memberikan informasi bagi penelitian selanjutnya, terutama yang berhubungan 
dengan Career dalam setting pendidikan. 
1. Hasil penelitian ini diharapkan dapat berguna bagi Universitas  X  di Kota Bandung, 
untuk memahami kesulitan mahasiswa dalam pengambilan keputusan karier  
2. Penelitian ini juga diharapkan dapat  menambah pemahaman mahasiswa Universitas 
 X  Kota Bandung, mengenai karier dan kaitannya dengan berbagai aspek dalam 
pekerjaan dengan intervensi dari setiap fakultas atau jurusan. 
Pengambilan keputusan mengenai pilihan karir diperguruan tinggi merupakan tugas 
yang harus dilalui oleh setiap mahasiswa untuk mempersiapkan diri menempuh jenjang karir 
di dalam dunia pekerjaan, hal ini juga sesuai dengan perkembangan teori karir (e.g., 
Gottfredson, 1981; Super, 1957). Ini merupakan tahap yang penting yang harus diperhatikan 
oleh para mahasiswa. Proses pengambilan keputusan mengenai pilihan jurusan merupakan 
salah satu aktivitas dari proses pengambilan keputusan karir dan pekerjaan yang mengarah pada 
tingkatan perkembangan karir yang dikemukakan oleh  papalia (2009) bahwa dewasa awal yang 
duduk di perguruan tinggi (usia 20   25) diharapkan sudah mampu membedakan mana pilihan 
kegiatan yang dihargai oleh masyarakat dan sudah mampu memikirkan karir mereka 
berdasarkan minat, kemampuan dan nilai-nilai melalui pertimbangan-pertimbangan yang 
realistis, sehingga pada akhirnya seorang mahasiswa dapat melakukan pengambilan keputusan 
pilihan karir di dunia pekerjaan. 

