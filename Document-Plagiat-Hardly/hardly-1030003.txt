Pendidikan adalah usaha sadar dan terencana untuk mewujudkan 
suasana belajar dan proses pembelajaran agar peserta didik secara aktif 
mengembangkan potensi dirinya untuk memiliki kekuatan spiritual 
keagamaan, pengendalian diri, kepribadian, kecerdasan, akhlak mulia, serta 
keterampilan yang diperlukan dirinya, masyarakat, bangsa dan negara (UU 
No. 20 Tahun 2003 tentang SISDIKNAS). Menurut UU No. 20 Tahun 2013 
tentang sistem pendidikan di Indonesia salah satu jenis pendidikan di 
Indonesia adalah pendidikan khusus. Pendidikan khusus merupakan 
pendidikan bagi peserta didik yang memiliki tingkat kesulitan dalam 
mengikuti proses pembelajaran karena kelainan fisik, emosional, mental, 
sosial, dan/atau memiliki potensi kecerdasan dan bakat istimewa (UU No. 20  
Tahun 2003 tentang SISDIKNAS). 
Pendidikan tidak lepas dari guru sebagai salah satu komponennya agar 
siswa bisa belajar dengan baik. Dengan kata lain guru adalah pengelola 
proses belajar siswa (Winkel, 1983). Guru-guru yang mengajar pada jalur 
pendidikan khusus disebut guru pendidikan khusus. Guru pendidikan khusus 
adalah tenaga pendidik yang memenuhi kualifikasi akademik, kompetensi, 
dan sertifikat pendidik bagi peserta didik berkebutuhan khusus karena 
kelainan fisik, emosional, mental, intelektual, sosial, dan/atau memiliki 
potensi kecerdasan dan bakat istimewa pada satuan pendidikan khusus, satuan 
pendidikan umum, dan/atau satuan pendidikan kejuruan 
(Permendiknas,2008). Guru pendidikan khusus memiliki karakteristik dan 
kompetensi kemampuan khusus yaitu memahami ABK (Anak Berkebutuhan 
Khusus), mampu mengidentifikasi ABK, mampu berempati, memiliki 
kemampuan yang diperlukan untuk mendidik peserta didik luar biasa jenis 
tertentu (spesialis), dan memilih keahlian sesuai dengan minat masing-masing 
tenaga kependidikan (anakluarbiasa.com). 
Peserta didik pendidikan khusus (ABK) memiliki perbedaan 
karakteristik dan hambatan, untuk itu ABK memerlukan bentuk pelayanan 
pendidikan khusus yang disesuaikan dengan kemampuan mereka. Di 
Indonesia terdapat 6 jenis Sekolah Luar Biasa yaitu SLB-A (tunanetra), SLB-
B (tunarungu), SLB-C(tunagrahita), SLB-D (tunadaksa), SLB-E (tunalaras), 
dan SLB-F (tunaganda) (edukasi.kompasiana.com). Setiap karakteristik ABK 
memiliki kesulitan yang berbeda, namun pada anak tunagrahita terdapat 
karakteristik tersendiri yang membedakannya dengan karakteristik ketunaan 
lainnya yaitu hambatan kognitif. Hambatan kognitif membuat anak 
tunagrahita sulit sekali untuk menerima pelajaran. Hal tersebut sejalan dengan 
informasi yang diperoleh melalui wawancara kepada seorang kepala sekolah 
SLB-C dan seorang guru SLB-B yang juga mengajar berdampingan di 
sekolah SLB-C. Guru SLB-B tersebut menyatakan bahwa pada ABK 
tunanetra, tunadaksa, dan tunalaras, guru mendidik dan melatih anak yang 
secara kognitif normal hanya saja memiliki kecacatan dalam organ fisiknya, 
sehingga anak lebih mudah untuk diajar. Beliau juga mengatakan bahwa 
beliau tidak tahan dengan bau tubuh anak tunagrahita yang menyengat. 
Alasan ini yang membuat beliau sejak awal tidak bersedia mengajar anak 
tunagrahita. Hal yang serupa juga diutarakan oleh kepala sekolah di salah satu 
SLB-C di Kota Bandung. Beliau menyatakan bahwa dari hasil diskusi dengan 
beberapa kepala sekolah berbagai SLB di kota Bandung, banyak yang 
berpendapat bahwa kesulitan terbesar adalah mengajar siswa tunagrahita yang 
disebabkan oleh hambatan kognitif yang dimiliki oleh siswa tunagrahita. 
Tunagrahita adalah istilah yang digunakan untuk menyebut anak yang 
mempunyai kecerdasan dibawah rata-rata dan ditandai oleh keterbatasan 
intelegensi dan ketidakcakapan dalam interaksi sosial. Anak tunagrahita 
memiliki keterbatasan dalam hal kemampuan menyesuaikan dengan tugas 
baru, belajar dari pengalaman, berpikir abstrak, kreatif, kemampuan menilaim 
secara kritis, menghindari kesalahan, dan mengatasi kesulitan. Kapasitas 
belajar anak tunagrahita sangat terbatas khususnya pada hal-hal yang abstrak 
sehingga anak tunagrahita lebih banyak belajar dengan meniru apa yang 
dikatakan gurunya dari pada dengan pengertian. Anak tunagrahita tidak dapat 
mengurus diri, memelihara dan memimpin diri (Somantri,2006). 
Demi memenuhi kebutuhan pendidikannya anak tunagrahita 
dianjurkan untuk bersekolah di SLB-C. Dengan segala keterbatasan yang 
dimiliki anak tunagrahita, guru SLB-C menghadapi kesulitan yang besar 
dalam mendidik anak yang sulit sekali menangkap pelajaran. Kesulitan-
kesulitan yang dialami oleh guru SLB-C terungkap melalui hasil survei awal 
melalui wawancara pada 11 orang guru dari 4 sekolah SLB-C di Kota 
Bandung dan melalui observasi pada 3 sekolah SLB-C di Kota Bandung. 
Berdasarkan hasil wawancara pada 11 orang guru dari empat SLB-C 
di Kota Bandung diperoleh hasil sebagai berikut. Seluruh responden guru 
SLB-C (100%) menyatakan bahwa mereka perlu kesabaran lebih untuk 
mengajar anak tunagrahita yang terhambat secara kognitif. Meskipun dalam 
kondisi lelah ataupun sedang menghadapi masalah pribadi, guru tetap 
bersabar mengajarkan materi pelajaran secara terus-menerus, berulang-ulang 
sampai anak mengalami perubahan sedikit demi sedikit, dan menunggu 
berjam-jam dengan satu materi tugas karena tempo bekerja anak tunagrahita 
sangat lambat.  
Seluruh responden guru SLB-C (100%) menyatakan bahwa mereka 
berpikir keras dan sekreatif mungkin untuk mengetahui bagaimana cara 
mengkonkritkan semua materi pelajaran yang harus diberikan di tengah 
kondisi kurangnya sarana dan prasarana mengajar seperti alat peraga. Ada 
kalanya materi pelajaran seperti pendidikan kewarganegaraan sulit sekali 
untuk dikonkritkan dan  sulit untuk membuat anak mengerti materi pelajaran 
yang sifatnya abstrak. 
Seluruh responden guru SLB-C (100%) menyatakan setiap hari guru 
bersiaga untuk mengamati perubahan ekspresi wajah anak dan peka terhadap 
perubahan perilaku anak untuk mengetahui bagaimana kondisi perasaan anak 
dan menentukan metode belajar seperti apa yang saat itu paling tepat untuk 
anak apakah dengan menyanyi, menari, duduk tenang dan mendengarkan, 
atau belajar sambil peregangan diluar kelas.  
Seluruh responden guru SLB-C (100%) menyatakan mereka berusaha 
belajar cara mendekati dan memengaruhi anak secara personal karena setiap 
anak tunagrahita memiliki kebutuhan dan cara belajar yang berbeda-beda 
sampai mereka tahu bagaimana cara menenangkan anak ketika sedang 
tantrum. Guru SLB-C juga berusaha mempertahankan situasi kondusif dalam 
kelas dengan berpikir cara tetap menarik perhatian anak tunagrahita karena 
anak tunagrahita memiliki rentang waktu konsentrasi yang pendek.  
Sebanyak 9 (82%) dari 11 orang guru menyatakan mendapat kesulitan 
dari orangtua murid yang tidak paham dengan kondisi anaknya dan memiliki 
harapan terlalu tinggi dengan kemampuan anaknya. Sehingga, orangtua murid 
ini sering mengeluh karena tingkat perkembangan anak yang lambat dan 
menyalahkan pada kurangnya kinerja guru dan mengatakan bahwa guru SLB-
C tidak memberikan manfaat bagi perkembangan anak tunagrahita. Di sisi 
lain terdapat 2 (18%) dari 11 orang yang sudah mampu membangun 
hubungan baik dengan orangtua murid agar lebih berpartisipasi aktif untuk 
membantu pendidikan anak tunagrahita dan tidak segan mendatangi rumah 
muridnya satu per satu ketika orangtuanya tidak bersikap kooperatif dan aktif 
dengan pendidikan anak. 
Sebanyak 10 (91%) orang dari 11 guru menyatakan sering disakiti 
oleh siswanya saat proses mengajar. Mereka disakiti dalam bentuk dipukul, 
dicakar, digigit, ditendang, dicengkram dengan kuat hingga menimbulkan 
bekas pada kulit, dan ditusuk dengan pensil runcing hingga ada seorang guru 
yang memiliki lubang di tangannya. Ada kalanya guru merasa jenuh dan kesal 
dengan tingkah laku tunagrahita, sehingga ada kalanya guru menyalurkan 
emosi negatif mereka dengan memukul anak, membentak, meninggikan 
suara, dan mencubit anak dengan tujuan untuk menegur anak. Hingga 
terdapat seorang guru yang dikenal galak pernah mendapat kasus karena ia 
mencubit anak sampai memar karena menegur anak yang akan menyolok 
mata temannya dengan pensil tajam. Orangtua anak tersebut protes hingga 
memutuskan untuk pindah sekolah.  Di sisi lain terdapat seorang guru (9%) 
yang sama-sama memiliki pengalaman disakiti oleh siswa, tetapi mampu 
mengelola emosi negatifnya dengan tetap tenang, sabar, dan menegur anak 
dengan lembut. Beliau juga menyatakan bahwa dirinya tidak pernah merasa 
kesal dengan anak tunagrahita.  
Seluruh responden (100%) menyatakan jumlah guru pria jauh lebih 
sedikit dari wanita. Dalam proses belajar pun siswa jauh lebih senang berada 
didekat guru wanita dibandingkan guru pria. Biasanya guru pria berperan 
sebagai guru olahraga dan membantu guru wanita ketika ada siswa yang 
tantrum dan tidak bisa ditangani lagi oleh guru wanita. Sebanyak 2 (18%) 
dari 11 orang guru menyatakan bahwa mereka sendiri yang memilih bekerja 
sebagai guru SLB-C, tetapi dengan pilihan bekerja sebagai guru SLB-C 
mereka mendapat kecaman dari pihak keluarga, sehingga mereka harus 
meyakinkan keluarga dengan pilihan kerjanya. Disisi lain, sebanyak 9 (82%) 
dari 11 orang guru SLB-C menyatakan mendapat dukungan dari keluarganya 
untuk bekerja sebagai guru SLB-C. Sebanyak 3 (27%) dari 11 orang guru 
menyatakan bahwa pada awalnya mereka tidak berminat pada materi yang 
diajarkan pada pendidikan khusus sehingga mereka lebih merasakan kesulitan 
ketika mulai mengajar. Sebanyak 8 (73%) dari 11 orang guru menyatakan 
memang memilih jurusan Pendidikan Luar Biasa karena ketertarikan mereka.  
Berdasarkan hasil observasi pada 3 sekolah SLB-C di Kota Bandung 
didapatkan hasil bahwa guru tetap menunggu selama berjam-jam sampai anak 
tunagrahita selesai mengerjakan tugas yang diberikan, guru-guru pun begitu 
ekspresif dan ramah dalam mengajar. Guru SLB-C seringkali mengumbar 
senyum pada muridnya untuk membuat anak tetap dalam kondisi kondusif 
untuk belajar. Guru SLB-C yang berusaha menenangkan muridnya yang 
tantrum, hingga terdorong jatuh oleh anak tunagrahita. Sebagian besar guru 
SLB-C memiliki bekas luka pada bagian tangannya. Berdasarkan hasil 
wawancara dan observasi, diperoleh gambaran bahwa guru SLB-C 
menghadapi kesulitan yang besar dalam mengajar.  
Kesulitan-kesulitan yang tampak melalui wawancara dan observasi 
menimbulkan penghayatan bahwa guru perlu mengelola emosinya agar tetap 
dapat bertahan memenuhi tugas dan tanggung jawab mendidik anak 
tunagrahita, kemampuan mengelola emosi merupakan salah satu bagian dari 
kecerdasan emosional. Kecerdasan emosional adalah kemampuan mengenali 
perasaan kita sendiri dan perasaan orang lain, kemampuan memotivasi diri 
sendiri, dan kemampuan mengelola emosi dengan baik pada diri sendiri dan 
dalam hubungan dengan orang lain (Goleman, 2000). 

