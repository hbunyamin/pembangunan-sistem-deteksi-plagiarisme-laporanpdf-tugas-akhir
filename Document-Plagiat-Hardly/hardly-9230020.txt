Remaja sebagai generasi penerus bangsa sangat dibutuhkan perannya
dalam mewujudkan pembangunan nasionaI. Dengan demikian remaja diharapkan
sudah mulai memikirkan dan merencanakan masa depannya. Masa remaja itu
sendiri merupakan masa transisi dari masa anak menuju masa dewasa dan seperti
tahap perkembangan lainnya, individu yang berada pada masa remaja mulai
dituntut untuk dapat menyelesaikan tugas-tugas perkembangannya.
Semua tugas-tugas perkembangan pada masa remaja dipusatkan pada
perslapan remaJa untuk menghadapi masa depan, artinya tugas-tugas
perkembangan remaja secara tidak langsung menunjukkan suatu orientasi masa
depan. Pada dasarnya orientasi masa depan merupakan cara pandang individu
terhadap masa depannya. Dengan adanya orientasi masa depan berarti remaja
telah melakukan antisipasi terhadap kehidupan yang akan dihadapinya dimasa
yang akan datang (Nurmi, 1991). Remaja yang sudah memikirkan dan
merencanakan masa depannya akan lebih mudah mengarahkan tingkah lakunya,
sehingga apa yang dilakukannya adalah untuk mewujudkan masa depannya.
Ada beberapa bidang kehidupan yang menjadi pusat perhatian orientasi
masa de pan remaja, salah satu yang menarik minat dan menjadi pemikiran
orientasi masa depan remaja adalah mengenai bidang pendidikan (Nurmi, 1991).
Hal ini disebabkan karena remaja menyadari bahwa pendidikan yang lebih tinggi
merupakan batu loncatan bagi karier mereka kelak dan persiapan untuk
menunjang kehidupan mereka, karena karier yang nanti dimiliki oleh remaja tidak
terlepas dari pendidikan yang dilakukan remaja tersebut.
Bila sejak awal remaja telah mampu menetapkan tujuan dan mempunyai
persiapan serta perencanaan dalam bidang pendidikan, menunjukkan bahwa
mereka mempunyai orientasi masa depan pendidikan yang jelas. Orientasi masa
depan pendidikan yang jelas tersebut ditandai dengan adanya motivasi tinggi,
yang mendorong remaja dalam menetapkan tujuan pendidikan yang ingin dicapai.
Agar tujuan pendidikan tersebut dapat tercapai, maka remaja harus mempunyai
minat dan harapan yang tinggi. Perencanaan yang dilakukan remaja untuk
mencapaI tujuan yang telah ditetapkan juga terarah, sehingga mereka mampu
membuat penilaian mengenai langkah yang paling memungkinkan untuk
tercapainya tujuan tersebut (Nurmi, 1991).
Seperti diungkapkan oleh D pelajar putri berusia 18 tahun duduk di bangku
SMU negeri kelas III, mengatakan bahwa ia berminat untuk terus melanjutkan
pendidikannya ke perguruan tinggi negeri pacta Fakultas Ekonomi. Karena 0
menyadari bahwa persaingan ke perguruan tinggi negeri sangat ketat, maka untuk
mewujudkan keinginannya ia makin giat belajar. D berharap dengan. makin giat ia
belajar maka makin terbuka kesempatan baginya untuk diterima di perguruan
tinggi negen tersebut, sehingga menurutnya nanti ia akan lebih mudah
mendapatkan pekerjaan dan hidup mandiri sesuai dengan cita-citanya.
I pelajar putri berusia 18 tahun duduk di bangku SMU negeri kelas 1lI,
mengungkapkan bahwa sebenarnya ia juga ingin melanjutkan pendidikannya ke
perguruan tinggi negeri dan menjadi sarjana ekonomi. Namun orang tuanya hanya
mampu membiayai pendidikannya sampai ke jenjang D I. Menurut I nanti setelah
ia mendapat pekerjaan dan mampu membiayai segala kebutuhan hidupnya sendiri,
pasti ia akan. tetap melanjutkan pendidikannya sesuai dengan cita-cita yang
diinginkannya.
Namun demikian tidak semua pelajar putri mempunyal orientasi masa
depan pendidikan yang jelas. Kita sering menemukan banyak remaja mengalami
kebingungan untuk memilih jurusan atau program pendidikan yang akan mereka
tempuh setelah lulus SMU ataU bahkan mereka diliputi keragu-raguan apakah
akan melanjutkan sekolah atau tidak. Seperti yang diungkapkan Y pelajar putri
berusia 19 tahun duduk di bangku SMU Swasta kelas III, mengatakan bahwa ia
masih belum tahu apa yang akan dilakukan setelah lulus SMU nanti. Belum
terpikir mau apa, apakah akan sekolah lagi atau tidak, bagaimana nanti saja.
Y juga rnengatakan untuk apa sekolah tinggi-tinggi kalau nantinya susah
rnendapatkan pekerjaan. Lebihlanjut ia juga rnengungkapkan bahwa yang sekolah
tinggi juga banyak yang rnenganggur.
Pelajar putri yang rnernpunyai orientasi rnasa depan pendidikan tidak jelas,
seringkali rnengalarni harnbatan dalarn rnenyalurkan rninat dan rnenyusun
perencanaan dalam menentukan tujuan yang ingin dicapainya. Ketidakjelasan
orientasi rnasa depan pendidikan pada pelajar putri ini dipengaruhi juga oleh
lingkungan sosialnya terutarna pengaruh kelornpok ternan-ternan sebaya.
Trornrnsdorf (1983), rnengungkapkan bahwa bila rernaja rnasuk pada kelornpok
ternan-ternan yang rnernpunyai orientasi rnasa depan tidak jelas rnaka hal tersebut
akan berpengaruh pada pernbiearaan, penarnpilan, perilaku, rninat serta pandangan
rnereka terhadap rnasa depannya.
Y. Barnbang Mulyono juga berpendapat bahwa kehadiran ternan dan
keterlibatannya dalarn suatu kelornpok sosial akan rnernbawa pengaruh tertentu
baik dalarn arti positif rnaupun dalam arti negatif. Seseorang yang telah
rnerasa coeok dengan ternan atau kelornpoknya tentu cenderung untuk rnengikuti
gaya teman atau kelornpoknya tersebut. Walaupun sebenarnya perilaku yang
rnereka tarnpilkan tersebut salah dan rnenyirnpang atau bahkan menj urus pada
bentuk kenakalan (Mengatasi Kenakalan Rernaja, 1993).
Da!am kehidupan sehari-hari kita banyak rnenjurnpai bermacam bentuk
kenakalan yang dilakukan remaJa. Membolos sekolah, membaca buku-buku
porno, membawa senjata tajam , mabuk-mabukan, penyalahgunaan obat terlarang
dan salah satu bentuk kenakalan yang cukup memprihatinkan pada remaja adalah
kenakalan seksual.
Menurut Ketua Majelis Ulama Indonesia (MUl) Jawa Barat, Dr. lbin
Kutibin, Spkj, mengungkapkan bahwa perilaku kenakalan seksua] yang telah
banyak dilakukan remaja tersebut, bukan merupakan hal mengejutkan lagi
terutama di kota-kota besar seperti Bandung. Berbagai jenis sarana hiburan yang
ada terutama dengan makin banyaknya hiburan malam, ditambah lagi dengan
makin tersedianya berbagai sarana kontrasepsi, maka akan semakin
mempermudah para remaja untuk melakukan kenakalan seksual. Bentuk
kenakalan seksual yang dilakukan para remaja tersebut ialah mulai dari berkencan,
bercumbu, bersenggama, seks bebas dan gejala yang tampak nyata adalah makin
banyaknya remaja yang menjadi pekeIja seks. Remaja pekerja seks ini, di kota
Bandung lebih terkenal dengan sebutan "bayur" (Tabloid Berita Mingguan
Indonesia-Indonesia, 1999).
Mengutip hasil survey yang dilakukan Yayasan Bahtera, yaitu yayasan
yang menangam masalah kenakalan remaja dan anak-anak jalanan pada tahun
1988 di kota Bandung, menyebutkan bahwa dari total 1.545 pekerja seks baik yang
berada di jalanan, di tempat hiburan atau di tempat lokalisasi, 30% dari mereka
adalah remaja yang masih berstatus sebagai pelajar dan mahasiswa. (Tabloid
Berita Mingguan Fokus, 1998).
kebanyakan dari pekerja seks yang masih bcrstatus pelajar dan mahasiswa
itu seringkali mempunyai masalah disekolah. Seperti diungkapkan oleh Dr.
SucherJy, S.E, M.S, kepala pusat Litbang bisnis Unpad mengemukakan bahwa
berdasarkan hasil riset yang dilakukan terhadap beberapa orang pelajar putri
pekerja seks di Kotamadya Bandung, beberapa diantaranya mempunyai prestasi
yang rendah di sekolah, sering membolos sekolah dan mempunyai minat yang
rendah terhadap sekolah. (Tabloid Berita Mingguan Fokus, 1998).
Rendahnya minat terhadap sekolah pada pelajar putri pekelja seks tersebut
membuat orientasi masa depan pendidikan mereka menjadi tidak jelas Hal ini
sesuai dcngan apa yang dikatakan Nurmi (1991), bahwa remaja yang mempunyai
orientasi masa depan pendidikan tidak jelas, mempunyai motivasi yang rendah
dalam menentukan tujuan pendidikannya. Penyusunan perencanaan mereka
kemasa depan juga tidak terarah, sehingga mereka menjadi kurang mampu
melakukan penilaian untuk terealisasinya tujuan yang telah ditetapkan.
Seperti yang diungkapkan S pelajar putri pckerja seks, berusia 18 tahun
duduk di bangku SMU Swasta kelas III. S mengatakan bahwa setelah lulus SMU
nanti, ia tidak tidak tahu apakah akan melanjutkan sekolah sampai ke jenjang
pendidikan yang lebih tinggi atau tidak. Menurut S dengan bersekolahpun belum
tentu ia dapat membiayai segala kebutuhan hidupnya sendiri seperti sekarang ini.
la juga mengatakan bahwa ia tidak terlalu memikirkan masa depan. Biarlah masa
depannya hancur sekalian, ia bekerja sebagai pekerja seks juga karena merasa
hidupnya telah hancur setelah dinodai pacar.
Namun berdasarkan wawancara peneliti dengan pelajar putri pekerja seks,
diperoleh fakta tidak semua pelajar pUlri pekerja seks mempunyai orientasi masa
depan pendidikan yang tidak jelas. Seperti yang diungkapkan oleh N berusia 19
tahun duduk di bangku SMU Swasta kelas III, ia bekerja sebagai pekerja seks
karena terpaksa untuk membiayai sekolahnya setelah perusahaan orang twmya
bangkrut karena krisis moneter. Walaupun N menyadari bahwa langkah yang ia
lakukan untuk dapat terus melanjutkan pendidikan sampai jenjang D3 (Sekretaris)
tersebut salah, namun N berharap semoga dengan terus bersekolah ia dapat
mencapai cita-citanya dan memperbaiki hidupnya serta keluarganya.
Berdasarkan uraian diatas terlihat bahwa terdapat perbedaan antara pelajar
putri pckerja seks dengan pelajar putri bukan pekerja seks dalam menetapkan
tujuan yang ingin dicapai dimasa depaIl, terutama mengenai kelarljutan pendidikan
yang akan mereka tempuh di masa depan. Atas dasar kondisi tersebut peneliti
tertarik untuk mengadakan studi perbandingan mengenai orientasi masa depan
pendidikan pada pelajar putri pekerja seks dan pelajar putri bukan pekerja seks
usia 17-19 tahun di Kotamadya Bandung.
Apakah ada perbedaan orientasi rnasa depan pendidikan pada pelajar putri
pekerja seks dan pelajar putri bukan pekerja seks usia J7-J9 tahun di Kotarnadya
Bandung ?
Maksud dari penelitian ini adalah untuk rnernbandingkan orientasi rnasa
depan pendidikan pada pelajar putri pekerja seks dan pelajar putri bukan pekerja
seks usia 17-19 tahun di kotarnadya Bandung.
Tujuan dari penelitian ini adalah untuk rnernperoJeh garnbaran tentang
perbedaan orientasi rnasa depan pendidikan pada pelajar putri pekerja seks dan
pelajar Putri bukan pekerja seks usia 17-19 tahun di Kotarnadya Bandung.
Kegunaan ilrniah dari penelitian ini ialah untuk pengembangan ilrnu
psikologi khususnya psikologi perkernbangan. Hasil penelitian ini juga diharapkan
dapat rnemberikan informasi guna rnenarnbah pengetahuan dan pernaharnan
tentang pelajar, khususnya pelajar yang bekerja sehagai pekerja seks, berkaitan
dengan orientasi rnasa depan pendidikannya. Hal ini dapat rncnjadi bahan
pertimbangan untuk penelitian selanjutnya terutama guna pengembangan
penelitian dalam bidang orientasi masa depan.
Hasi! peneJitian ini dapat menjadi bahan masukan dan pertimbangan bagi
peJajar putri, khususnya pc/ajar putri pekerja seks dalam menetapkan orientasi
masa depannya terutama orientasi masa depan pendidikannya. Oiharapkan mereka
dapat merencanakan dan memandang masa depannya secara Jebih positif,
sehingga mampu mencapai tujuan masa depannya sesuai dengan harapan yang
diinginkannya.
Masa remaja adalah suatu periode dalam kehidupan manusia yang ditandai
dengan adanya suatu proses peralihan dari masa anak menuju masa dewasa. Masa
remaJa 1m dapat dikatakan sebagai fase terakhir masa kanak-kanak sebelum
memasuki masa dewasa. Dalam masa peraIihan ini, remaja dianggap sudah
mampu menguasai dan meninggalkan masa kanak-kanak dan siap memasuki masa
dewasa.
Remaja Juga dianggap telah mencapai kematangan kognitif yaitu
merupakan suatu tahap dimana remaja mampu berpikir formal operasionai. Tahap
berpikir formal operasional ini merupakan suatu tahap dimana remaja memiIiki
kemampuan merumuskan hipotesis-hipotesis dan dapat menyusun strategi
pemecahan masalah. Mereka dapat melihat segala sesuatu dari sejumJah sudut
pandang ketika menemui suatu permasalahan pada saat mencapai tujuan
(Trommsdrof, 1983). Tahap berpikir ini juga membuat remaja bukan saja mampu
memahami keadaan yang sedang te~iadi tetapi juga memahami keadaan yang
diduga akan terjadi. Antisipasi yang dilakukan remaja ini, menunjukkan bahwa
mereka mempunyai orientasi masa depan.
Orientasi masa de pan itu sendiri ialah cara pandang seseorang terhadap
kehidupan yang akan dihadapinya di masa yang akan datang. Orientasi masa
depan tersebut dapat digambarkan melalui tiga tingkatan yang merupakan suatu
proses yang saling berinteraksi. Ketiga proses tersebut ada!ah motivasi,
perencanaan dan evaluasi (Nurmi, 199 I).
Beberapa bidang kehidupan di masa depan yang seringkali menjadi pusat
perhatian remaja, diantaranya adalah masalah yang berkaitan dengan kelanjutan
pendidikan yang akan mereka tempuh di masa depan (Nurmi,1991). Pada dasamya
setiap remaja mempunyai pandangan positif terhadap pendidikan. Hal ini
disebabkan karena remaja menyadari bahwa pendidikan merupakan jalan untuk
mencapai keberhasilan dalam kehidupan mereka. Sehingga minat, perencanaan
dan aspirasinya adalah melanjutkan pendidikan ke jenjang yang lebih tinggi di
masa depan (Trommsdrof, 1983).
Orientasi masa depan ini juga berhubungan dengan struktur kehidupan
sosial. Dimana segala sesuatu yang diterima remaja dari lingkungan akan
mempengaruhi wawasan dan pandangan mereka akan masa depannya.
Pengalaman belajar yang remaja alami dalam lingkungan keluarga, lingkungan
ternan sebaya akan bcrpengaruh terhadap pembentukan orientasi masa depannya.
Menurut Syaiful Harahap, lingkungan ternan sebaya selain membawa
pengaruh positif juga membawa pengaruh negatif terhadap kehidupan remaja.
Terkadang pengaruh negatif dari teman sebaya ini menjurus pada pcnyimpangan
periJaku, seperti periJaku kenakalan seksual yang menjurus pada pelacuran (Jika
Remaja Putri Lepas Kontrol, Tabloid Mutiara, 1988).
Leila C.H. Budiman juga mengatakan bahwa pengalaman akan kegagalan,
kurangnya pengendalian diri dan kurangnya penerimaan sosial juga dapat menjadi
penyebab pelacuran pada remaja. Akibatnya remaja menjadi tidak percaya diri dan
menimbulkan sikap pesimis. Sikap pesimis, kecewa, putus asa inilah yang
memunculkan keputusan untuk menjadi pekerja seks (Liku-Liku Pergaulan
Pranikah, 1991).
Menurut Gilbert & 1. Reinda Lumoindong, pekerja seks adalah seseorang
yang melakukan kebebasan relasi seksual dalam bentuk penyerahan diri kepada
banyak laki-laki untuk pemuasan seksual dan mendapatkan imbalan jasa bagi
pelayanannya (Pelacuran di Balik Seragam Sekolah, 1996).
Masalah kenakalan seksual yang dilakukan remaJa tersebut dapat
menghambat orientasi masa depan mereka. Nurmi (1991) Mengungkapkan bahwa
bila remaja mengalami hambatan dalam menetapkan orientasi masa depan maka
oricntasi masa depan mcrcka menjadi tidak jelas
Pada umumnya pejaJar putri peke~ja seks mempunyai orientasi masa depan
pendidikan yang tidak jelas. ketidakje!asan tersebut disebabkan karena pelajar
putri pekerja seks ini mempunyai motivasi yang rendah, yang menyebabkan
mereka tidak terdorong untuk menctapkan tujuan pcndidikan masa depannya
Lemahnya dorongan pada peJajar putri pekerja seks ini menyebabkan rendahnya
minat mereka, sehingga mereka tidak mempunyai keinginan untuk melanjutkan
pendidikannya ke jenjang yang lebih tinggi.
Perencanaan yang dilakukan pelajar putri pekerja seks ini juga tidak
terarah. Dimana menurut Nurmi (1991), penyusunan rencana mengenai langkah-
langkah yang akan dilakukan untuk merealisasikan tujuan yang ingin dicapai di
masa depan, didasari oleh knowledge, plans dan realization. Knowledge berkaitan
dengan pembentukan sub-sub tujuan. Pembentukan sub-sub tujuan adalah
merupakan suatu usaha yang dilakukan pelajar putri pekerja seks dalam
mewujudkan tujuan yang telah direncanakan. Untuk membentuk sub-sub tujuan
dibutuhkan pengetahuan yang berhubungan dengan tujuan tersebut. Seberapa
banyak pengetahuan yang dimiliki akan mempengaruhi perencanaan yang dibuat.
Dalam tahap ini pelajar putri pekerja seks kurang mempunyai usaha untuk
mewujudkan tujuan pendidikannya, sehingga perencanaan yang dibuat menjadi
tidak terarah. Plans berkaitan dengan keragaman dari rencana atau strategi yang
dilakukan. Pada tahap ini juga pelajar putri pekerja seks kurang dapat menyusun
perencanaan dan strategi secara baik. Sedangkan reuli:;alion berkaitan dengan apa
saja yang telah dan akan dilakukan individu daJam mewujudkan tujuan. Pada
tahap ini pelajar putri pekerja seks kurang dapat memperkirakan hambatan-
hambatan yang mungkin ada, sehingga mereka mengalami kesulitan dalam
melakukan penilaian untuk merealisasikan tujuan pendidikan yang ingin dicapai,
Sejauhmana tujuan yang teJah ditetapkan dan perencanaan yang telah
dibuat mengenai langkah yang paling memungkinkan dapat direalisasikan disebut
sebagai tahap evaluasi. Oalam proses ini pelajar putri pekerja seks kurang dapat
memperkirakan faktor-faktor apa saja yang dapat mendukung dan menghambat
pencapaian tujuan. Selain itu penghayatan mereka terhadap kesuksesan dan
kegagalan yang pernah dialami akan mempengaruhi keyakinan atau optimisme
mereka terhadap kemungkinan tercapai tidaknya tujuan.
Pelajar putri bukan pekerja seks umumnya mempunyai orientasi masa
depan pendidikanjelas. Menurut Nurmi (1991), remaja yang mempunyai orientasi
masa depan pendidikan jelas akan mempunyai motivasi serta mempunyai minat
yang tinggi dalam menentukan tujuan pendidikannya. Penyusunan perencanaan
mereka kemasa depan juga akan terarah. Mereka menyadari potensi yang mereka
miliki, mereka juga memiliki antisipasi terhadap hambatan yang mungkin
dihadapi di masa depan. Akibatnya penilaian mereka untuk terwujudnya tujuan
dimasa depan terhadap faktor-faktor apa saja yang sekiranya dapat mendukung
atau menghambat terwujudnya tujuan di masa depan menJadi jelas dan realistis.
Secara lebih jelas kerangka pemikiran di atas dapat digambarkan dalam
bagansebagaiberikut:
usia] 7-] 9 tahun I
P'kT
S,b
,
l Pelajar Putri . MotivasiI. Perencanaan
. Evaluasi
Berdasarkan kerangka pemikiran di atas dapat diturunkan asumsi sebagai
berikut :
1. Remaja pada umumnya sudah mampu menetapkan tujuan masa depannya.
Dimana motivasi, perencanaan dan evaluasi yang mereka lakukan adalah
untuk mencapai tujuan, terutama mengenai kelanjutan pendidikan yang akan
mereka tempuh di masa depan.
2. Pada umumnya pelajar putri pekerja seks mempunyai orientasi masa depan
pendidikan tidak jelas. Mereka mempunyai motivasi yang rendah dalam
menetapkan tujuan pendidikannya. Penyusunan perencanaan mereka kemasa
depan juga tidak terarah, sehingga mereka menjadi kurang mampu me!akukan
penilaian untuk terealisasinya tujuan.
3. Pada umumnya pelajar putri bukan pekerja seks mempunyai orientasi masa
depan pendidikan jelas. Mereka mempunyai motivasi yang tinggi dalam
menetapkan tujuan pendidikan yang ingin dicapai. Perencanaan yang
dilakukan juga terarah, sehingga penilaian yang dilakukan lebih realistis sesuai
dengan tujuan.
Hipotesis yang diajukan dalam penelitian ini adalah : "Terdapat
Perbedaan Orientasi Masa Depan Pendidikan Pada Pelajar Putri Pekerja
Seks Dan PeIajar Putri Bukan Pekerja Seks Usia 17-19 Tahun Di Kotamadya
Bandung ".
Dalam penelitian ini akan diukur perbandingan Orientasi Masa Depan
pendidikan pada pelajar putri peicerja seks dan pelajar putri bukan pekerja seks
berusia 17-19 tahun di Kotamadya Bandung. Penelitian ini bersifat komparatif
yaitu membal!dingkan antara satu variabei dengan variabel Iainnya.
Data penelitian ini diperoleh dengan menggunakan kuesioner yang
diberikan pada pelajar putri pekerja seks dan pelajar putri bukan pekerja seks
untuk memperoleh gambaran tentang Orientasi Masa Depan Pendidikan
berdasarkan pada konsep Orientasi Masa Depan Nurmi.
SampeI yang digunakan dalam penelitian 1m diperoleh dengan
menggunakan teknik lnsidental Sampling, yaitu cara penarikan sampel dimana
sampel diperoleh semata-mata dari keadaan-keadaan yang insidental atau secara
kebetulan. Untuk pengolahan dan analisa data digunakan perhitungan statistik
dari U-Mann Whitney.
Penelitian ini dilakukan di sekitar jalan Alkateri (Plaza Asia Afrika), di
jalan Merdeka (Bandung Indah Plaza), di Alun-alun (Palaguna Plaza), di jalan
Malabar (dekat SMU Muslimin 1 dan 2), di Jalan Cihampelas (dekat SMU
Pasundan 2).
Waktu penelitian dilakukan pada bulan Mci 2000 dan untuk pengambilan
data kurang lebih tiga bulan.
 Remaja sebagai generasi penerus bangsa sangat dibutuhkan perannya 
 dalam mewujudkan pembangunan nasionaI. Dengan demikian remaja diharapkan 
 sudah mulai memikirkan dan merencanakan masa depannya. Masa remaja itu 
 sendiri merupakan masa transisi dari masa anak menuju masa dewasa dan seperti 
 tahap perkembangan lainnya, individu yang berada pada masa remaja mulai 
 dituntut untuk dapat menyelesaikan tugas-tugas perkembangannya. 
 Semua tugas-tugas perkembangan pada masa remaja dipusatkan pada 
 perslapan remaJa untuk menghadapi masa depan, artinya tugas-tugas 
 perkembangan remaja secara tidak langsung menunjukkan suatu orientasi masa 
 depan. Pada dasarnya orientasi masa depan merupakan cara pandang individu 
 terhadap masa depannya. Dengan adanya orientasi masa depan berarti remaja 
 telah melakukan antisipasi terhadap kehidupan yang akan dihadapinya dimasa 
 yang akan datang (Nurmi, 1991). Remaja yang sudah memikirkan dan 
 merencanakan masa depannya akan lebih mudah mengarahkan tingkah lakunya, 
 sehingga apa yang dilakukannya adalah untuk mewujudkan masa depannya. 
 Ada beberapa bidang kehidupan yang menjadi pusat perhatian orientasi 
 masa de pan remaja, salah satu yang menarik minat dan menjadi pemikiran 
 orientasi masa depan remaja adalah mengenai bidang pendidikan (Nurmi, 1991). 
 Hal ini disebabkan karena remaja menyadari bahwa pendidikan yang lebih tinggi 
 merupakan batu loncatan bagi karier mereka kelak dan persiapan untuk 
 menunjang kehidupan mereka, karena karier yang nanti dimiliki oleh remaja tidak 
 terlepas dari pendidikan yang dilakukan remaja tersebut. 
 Bila sejak awal remaja telah mampu menetapkan tujuan dan mempunyai 
 persiapan serta perencanaan dalam bidang pendidikan, menunjukkan bahwa 
 mereka mempunyai orientasi masa depan pendidikan yang jelas. Orientasi masa 
 depan pendidikan yang jelas tersebut ditandai dengan adanya motivasi tinggi, 
 yang mendorong remaja dalam menetapkan tujuan pendidikan yang ingin dicapai. 
 Agar tujuan pendidikan tersebut dapat tercapai, maka remaja harus mempunyai 
 minat dan harapan yang tinggi. Perencanaan yang dilakukan remaja untuk 
 mencapaI tujuan yang telah ditetapkan juga terarah, sehingga mereka mampu 
 membuat penilaian mengenai langkah yang paling memungkinkan untuk 
 tercapainya tujuan tersebut (Nurmi, 1991). 
 Seperti diungkapkan oleh D pelajar putri berusia 18 tahun duduk di bangku 
 SMU negeri kelas III, mengatakan bahwa ia berminat untuk terus melanjutkan 
 pendidikannya ke perguruan tinggi negeri pacta Fakultas Ekonomi. Karena 0 
 menyadari bahwa persaingan ke perguruan tinggi negeri sangat ketat, maka untuk 
 mewujudkan keinginannya ia makin giat belajar. D berharap dengan. makin giat ia 
 belajar maka makin terbuka kesempatan baginya untuk diterima di perguruan 
 tinggi negen tersebut, sehingga menurutnya nanti ia akan lebih mudah 
 mendapatkan pekerjaan dan hidup mandiri sesuai dengan cita-citanya. 
 I pelajar putri berusia 18 tahun duduk di bangku SMU negeri kelas 1lI, 
 mengungkapkan bahwa sebenarnya ia juga ingin melanjutkan pendidikannya ke 
 perguruan tinggi negeri dan menjadi sarjana ekonomi. Namun orang tuanya hanya 
 mampu membiayai pendidikannya sampai ke jenjang D I. Menurut I nanti setelah 
 ia mendapat pekerjaan dan mampu membiayai segala kebutuhan hidupnya sendiri, 
 pasti ia akan. tetap melanjutkan pendidikannya sesuai dengan cita-cita yang 
 di inginkannya. 
 Namun demikian tidak semua pelajar putri mempunyal orientasi masa 
 depan pendidikan yang jelas. Kita sering menemukan banyak remaja mengalami 
 kebingungan untuk memilih jurusan atau program pendidikan yang akan mereka 
 tempuh setelah lulus SMU ataU bahkan mereka diliputi keragu-raguan apakah 
 akan melanjutkan sekolah atau tidak. Seperti yang diungkapkan Y pelajar putri 
 berusia 19 tahun duduk di bangku SMU Swasta kelas III, mengatakan bahwa ia 
 masih belum tahu apa yang akan dilakukan setelah lulus SMU nanti. Belum 
 terpikir mau apa, apakah akan sekolah lagi at au tidak, bagaimana nanti saja. 
 Y juga rnengatakan untuk apa sekolah tinggi-tinggi kalau nantinya susah 
 rnendapatkan pekerjaan. Lebihlanjut ia juga rnengungkapkan bahwa yang sekolah 
 tinggi juga banyak yang rnenganggur. 
 Pelajar putri yang rnernpunyai orientasi rnasa depan pendidikan tidak jelas, 
 seringkali rnengalarni harnbatan dalarn rnenyalurkan rninat dan rnenyusun 
 perencanaan dalam menentukan tujuan yang ingin dicapainya. Ketidakjelasan 
 orientasi rnasa depan pendidikan pada pelajar putri ini dipengaruhi juga oleh 
 lingkungan sosialnya terutarna pengaruh kelornpok ternan-ternan sebaya. 
 Trornrnsdorf (1983), rnengungkapkan bahwa bila rernaja rnasuk pada kelornpok 
 ternan-ternan yang rnernpunyai orientasi rnasa depan tidak jelas rnaka hal tersebut 
 akan berpengaruh pada pernbiearaan, penarnpilan, perilaku, rninat serta pandangan 
 rnereka terhadap rnasa depannya. 
 Y. Barnbang Mulyono juga berpendapat bahwa kehadiran ternan dan 
 keterlibatannya dalarn suatu kelornpok sosial akan rnernbawa pengaruh tertentu 
 baik dalarn arti positif rnaupun dalam arti negatif. Seseorang yang telah 
 rnerasa coeok dengan ternan atau kelornpoknya tentu cenderung untuk rnengikuti 
 gaya teman atau kelornpoknya tersebut. Walaupun sebenarnya perilaku yang 
 rnereka tarnpilkan tersebut salah dan rnenyirnpang atau bahkan menj urus pada 
 bentuk kenakalan (Mengatasi Kenakalan Rernaja, 1993). 
 Da!am kehidupan sehari-hari kita banyak rnenjurnpai bermacam bentuk 
 kenakalan yang dilakukan remaJa. Membolos sekolah, membaca buku-buku 
 porno, membawa senjata tajam , mabuk-mabukan, penyalahgunaan obat terlarang 
 dan salah satu bentuk kenakalan yang cukup memprihatinkan pada remaja adalah 
 kenakalan seksual. 
 Menurut Ketua Majelis Ulama Indonesia (MUl) Jawa Barat, Dr. lbin 
 Kutibin, Spkj, mengungkapkan bahwa perilaku kenakalan seksua] yang telah 
 banyak dilakukan remaja tersebut, bukan merupakan hal mengejutkan lagi 
 terutama di kota-kota besar seperti Bandung. Berbagai jenis sarana hiburan yang 
 ada terutama dengan makin banyaknya hiburan malam, ditambah lagi dengan 
 makin tersedianya berbagai sarana kontrasepsi, maka akan semakin 
 mempermudah para remaja untuk melakukan kenakalan seksual. Bentuk 
 kenakalan seksual yang dilakukan para remaja tersebut ialah mulai dari berkencan, 
 bercumbu, bersenggama, seks bebas dan gejala yang tampak nyata adalah makin 
 banyaknya remaja yang menjadi pekeIja seks. Remaja pekerja seks ini, di kota 
 Bandung lebih terkenal dengan sebutan "bayur" (Tabloid Berita Mingguan 
 Indonesia-Indonesia, 1999). 
 Mengutip hasil survey yang dilakukan Yayasan Bahtera, yaitu yayasan 
 yang menangam masalah kenakalan remaja dan anak-anak jalanan pada tahun 
 1988 di kota Bandung, menyebutkan bahwa dari total 1.545 pekerja seks baik yang 
 berada di jalanan, di tempat hiburan atau di tempat lokalisasi, 30% dari mereka 
 adalah remaja yang masih berstatus sebagai pelajar dan mahasiswa. (Tabloid 
 Berita Mingguan Fokus, 1998). 
 kebanyakan dari pekerja seks yang masih bcrstatus pelajar dan mahasiswa 
 itu seringkali mempunyai masalah disekolah. Seperti diungkapkan oleh Dr. 
 SucherJy, S.E, M.S, kepala pusat Litbang bisnis Unpad mengemukakan bahwa 
 berdasarkan hasil riset yang dilakukan terhadap beberapa orang pelajar putri 
 pekerja seks di Kotamadya Bandung, beberapa diantaranya mempunyai prestasi 
 yang rendah di sekolah, sering membolos sekolah dan mempunyai minat yang 
 rendah terhadap sekolah. (Tabloid Berita Mingguan Fokus, 1998). 
 Rendahnya minat terhadap sekolah pada pelajar putri pekelja seks tersebut 
 membuat orientasi masa depan pendidikan mereka menjadi tidak jelas Hal ini 
 sesuai dcngan apa yang dikatakan Nurmi (1991), bahwa remaja yang mempunyai 
 orientasi masa depan pendidikan tidak jelas, mempunyai motivasi yang rendah 
 dalam menentukan tujuan pendidikannya. Penyusunan perencanaan mereka 
 kemasa depan juga tidak terarah, sehingga mereka menjadi kurang mampu 
 melakukan penilaian untuk terealisasinya tujuan yang telah ditetapkan. 
 Seperti yang di ungkapkan S pelajar putri pckerja seks, berusia 18 tahun 
 duduk di bangku SMU Swasta kelas III. S mengatakan bahwa setelah lulus SMU 
 nanti, ia tidak tidak tahu apakah akan melanjutkan sekolah sampai ke jenjang 
 pendidikan yang lebih tinggi atau tidak. Menurut S dengan bersekolahpun belum 
 tentu ia dapat membiayai segala kebutuhan hidupnya sendiri seperti sekarang ini. 
 la juga mengatakan bahwa ia tidak terlalu memikirkan masa depan. Biarlah masa 
 depannya hancur sekalian, ia bekerja sebagai pekerja seks juga karena merasa 
 hidupnya telah hancur setelah dinodai pacar. 
 Namun berdasarkan wawancara peneliti dengan pelajar putri pekerja seks, 
 diperoleh fakta tidak semua pelajar pUlri pekerja seks mempunyai orientasi masa 
 depan pendidikan yang tidak jelas. Seperti yang diungkapkan oleh N berusia 19 
 tahun duduk di bangku SMU Swasta kelas III, ia bekerja sebagai pekerja seks 
 karena terpaksa untuk membiayai sekolahnya setelah perusahaan orang twmya 
 bangkrut karena krisis moneter. Walaupun N menyadari bahwa langkah yang ia 
 lakukan untuk dapat terus melanjutkan pendidikan sampai jenjang D3 (Sekretaris) 
 tersebut salah, namun N berharap semoga dengan terus bersekolah ia dapat 
 mencapai cita-citanya dan memperbaiki hidupnya serta keluarganya. 
 Berdasarkan uraian diatas terlihat bahwa terdapat perbedaan antara pelajar 
 putri pckerja seks dengan pelajar putri bukan pekerja seks dalam menetapkan 
 tujuan yang ingin dicapai dimasa depaIl, terutama mengenai kelarljutan pendidikan 
 yang akan mereka tempuh di masa depan. Atas dasar kondisi terse but peneliti 
 tertarik untuk mengadakan studi perbandingan mengenai orientasi masa depan 
 pendidikan pada pelajar putri pekerja seks dan pelajar putri bukan pekerja seks 
 usia 17-19 tahun di Kotamadya Bandung. 
 Apakah ada perbedaan orientasi rnasa depan pendidikan pada pelajar putri 
 pekerja seks dan pelajar putri bukan pekerja seks usia J 7- J 9 tahun di Kotarnadya 
 Bandung ? 
 Maksud dari penelitian ini adalah untuk rnernbandingkan orientasi rnasa 
 depan pendidikan pada pelajar putri pekerja seks dan pelajar putri bukan pekerja 
 seks usia 17-19 tahun di kotarnadya Bandung. 
 Tujuan dari penelitian ini adalah untuk rnernperoJeh garnbaran tentang 
 perbedaan orientasi rnasa depan pendidikan pad a pelajar putri pekerja seks dan 
 pelajar Putri bukan pekerja seks usia 17-19 tahun di Kotarnadya Bandung. 
 Kegunaan ilrniah dari penelitian ini ialah untuk pengembangan ilrnu 
 psikologi khususnya psikologi perkernbangan. Hasil penelitian ini juga diharapkan 
 dapat rnemberikan informasi guna rnenarnbah pengetahuan dan pernaharnan 
 tentang pelajar, khususnya pelajar yang bekerja sehagai pekerja seks, berkaitan 
 dengan orientasi rnasa depan pendidikannya. Hal ini dapat rncnjadi bahan 
 pertimbangan untuk penelitian selanjutnya terutama guna pengembangan 
 penelitian dalam bidang orientasi masa depan. 
 Hasi! peneJitian ini dapat menjadi bahan masukan dan pertimbangan bagi 
 peJajar putri, khususnya pc/ajar putri pekerja seks dalam menetapkan orientasi 
 masa depannya terutama orientasi masa depan pendidikannya. Oiharapkan mereka 
 dapat merencanakan dan memandang masa depannya secara Jebih positif, 
 sehingga mampu mencapai tujuan masa depannya sesuai dengan harapan yang 
 diinginkannya. 
 Masa remaja adalah suatu periode dalam kehidupan manusia yang ditandai 
 dengan adanya suatu proses peralihan dari masa anak menuju masa dewasa. Masa 
 remaJa 1m dapat dikatakan sebagai fase terakhir masa kanak-kanak sebelum 
 memasuki masa dewasa. Dalam masa peraIihan ini, remaja dianggap sudah 
 mampu menguasai dan meninggalkan masa kanak-kanak dan siap memasuki masa 
 dewasa. 
 Remaja Juga dianggap telah mencapai kematangan kognitif yaitu 
 merupakan suatu tahap dimana remaja mampu berpikir formal operasionai. Tahap 
 berpikir formal operasional ini merupakan suatu tahap dimana remaja memiIiki 
 kemampuan merumuskan hipotesis-hipotesis dan dapat menyusun strategi 
 pemecahan masalah. Mereka dapat melihat segal a sesuatu dari sejumJah sudut 
 pandang ketika menemui suatu permasalahan pada saat mencapai tujuan 
 (Trommsdrof, 1983). Tahap berpikir ini juga membuat remaja bukan saja mampu 
 memahami keadaan yang sedang te~iadi tetapi juga memahami keadaan yang 
 diduga akan terjadi. Antisipasi yang dilakukan remaja ini, menunjukkan bahwa 
 mereka mempunyai orientasi masa depan. 
 Orientasi masa de pan itu sendiri ialah cara pandang seseorang terhadap 
 kehidupan yang akan dihadapinya di masa yang akan datang. Orientasi masa 
 depan tersebut dapat digambarkan melalui tiga tingkatan yang merupakan suatu 
 proses yang saling berinteraksi. Ketiga proses tersebut ada!ah motivasi, 
 perencanaan dan evaluasi (Nurmi, 199 I). 
 Beberapa bidang kehidupan di masa depan yang seringkali menjadi pusat 
 perhatian remaja, diantaranya adalah masalah yang berkaitan dengan kelanjutan 
 pendidikan yang akan mereka tempuh di masa depan (Nurmi,1991). Pada dasamya 
 setiap remaja mempunyai pandangan positif terhadap pendidikan. Hal ini 
 disebabkan karena remaja menyadari bahwa pendidikan merupakan jalan untuk 
 mencapai keberhasilan dalam kehidupan mereka. Sehingga minat, perencanaan 
 dan aspirasinya adalah melanjutkan pendidikan ke jenjang yang lebih tinggi di 
 masa depan (Trommsdrof, 1983). 
 Orientasi masa depan ini juga berhubungan dengan struktur kehidupan 
 sosial. Dimana segala sesuatu yang diterima remaja dari lingkungan akan 
 mempengaruhi wawasan dan pandangan mereka akan masa depannya. 
 Pengalaman belajar yang remaja alami dalam lingkungan keluarga, lingkungan 
 ternan sebaya akan bcrpengaruh terhadap pembentukan orientasi masa depannya. 
 Menurut Syaiful Harahap, lingkungan ternan sebaya selain membawa 
 pengaruh positif juga membawa pengaruh negatif terhadap kehidupan remaja. 
 Terkadang pengaruh negatif dari teman sebaya ini menjurus pada pcnyimpangan 
 periJaku, seperti periJaku kenakalan seksual yang menjurus pada pelacuran (Jika 
 Remaja Putri Lepas Kontrol, Tabloid Mutiara, 1988). 
 Leila C.H. Budiman juga mengatakan bahwa pengalaman akan kegagalan, 
 kurangnya pengendalian diri dan kurangnya penerimaan sosial juga dapat menjadi 
 penyebab pelacuran pada remaja. Akibatnya remaja menjadi tidak percaya diri dan 
 menimbulkan sikap pesimis. Sikap pesimis, kecewa, putus asa inilah yang 
 memunculkan keputusan untuk menjadi pekerja seks (Liku-Liku Pergaulan 
 Pranikah, 1991). 
 Menurut Gilbert & 1. Reinda Lumoindong, pekerja seks adalah seseorang 
 yang melakukan kebebasan relasi seksual dalam bentuk penyerahan diri kepada 
 banyak laki-laki untuk pemuasan seksual dan mendapatkan imbalan jasa bagi 
 pelayanannya (Pelacuran di Balik Seragam Sekolah, 1996). 
 Masalah kenakalan seksual yang dilakukan remaJa tersebut dapat 
 menghambat orientasi masa depan mereka. Nurmi (1991) Mengungkapkan bahwa 
 bila remaja mengalami hambatan dalam menetapkan orientasi masa depan maka 
 oricntasi masa depan mcrcka menjadi tidak jelas 
 Pada umumnya pejaJar putri peke~ja seks mempunyai orientasi masa depan 
 pendidikan yang tidak jelas. ketidakje!asan tersebut disebabkan karena pelajar 
 putri pekerja seks ini mempunyai motivasi yang rendah, yang menyebabkan 
 mereka tidak terdorong untuk menctapkan tujuan pcndidikan masa depannya 
 Lemahnya dorongan pada peJajar putri pekerja seks ini menyebabkan rendahnya 
 minat mereka, sehingga mereka tidak mempunyai keinginan untuk melanjutkan 
 pendidikannya ke jenjang yang lebih tinggi. 
 Perencanaan yang dilakukan pelajar putri pekerja seks ini juga tidak 
 terarah. Dimana menurut Nurmi (1991), penyusunan rencana mengenai langkah- 
 langkah yang akan dilakukan untuk merealisasikan tujuan yang ingin dicapai di 
 masa depan, didasari oleh knowledge, plans dan realization. Knowledge berkaitan 
 dengan pembentukan sub-sub tujuan. 

