Perkembangan dunia industri dewasa ini, termasuk industri konstruksi 
meningkat sangat pesat disertai persaingan antar pelakunya yang begitu ketat. 
Tidak mengherankan bila mereka berlomba-lomba menciptakan strategi. 
Persaingan tidak hanya terjadi di antara perusahaan dalam negeri, tetapi juga 
dengan perusahaan di luar negeri. 
Penciptaan strategi tersebut dapat dilihat dari usaha setiap perusahaan 
untuk mengolah sebaik mungkin sumber daya yang dimilikinya (Man, Money, 
Machine, Material and Method) untuk menghasilkan produk yang terbaik. 
Perusahaan menyadari sepenuhnya bahwa salah satu kunci keberhasilan dalam 
menghadapi tantangan dan meraih peluang di masa mendatang serta dalam rangka 
mencapai visinya akan sangat tergantung dari kemampuan sumber daya manusia 
yang dimilikinya. 
Manusia atau sumber daya manusia (SDM)   yang dalam dunia industri 
biasa disebut dengan istilah karyawan - senantiasa menjadi faktor utama dalam 
pencapaian tujuan suatu perusahaan. Perusahaan tidak mungkin berjalan tanpa 
adanya karyawan. Ia akan dapat bertahan dan terus berkembang jika didukung 
oleh karyawan yang berkualitas. Salah satu dukungan yang diberikan oleh para 
karyawan diwujudkan melalui kinerja atau performance.  
Setiap perusahaan menghendaki performance terbaik dari setiap 
karyawannya, oleh karena itu perusahaan tersebut perlu menerapkan sistem 
manajemen yang tepat dalam mengelola SDM yang ada maupun dalam 
melakukan pencarian karyawan baru yang berkualitas. Setiap perusahaan dituntut 
untuk melakukan seleksi ketat terhadap calon-calon karyawan yang akan direkrut. 
Berbagai macam syarat dan kualifikasi ditetapkan bagi setiap pelamar. Hal ini 
dilakukan agar perusahaan mendapatkan karyawan dengan kualitas terbaik. Begitu 
pula dalam mengelola karyawan yang sudah ada. Perusahaan dituntut untuk 
memiliki suatu standar yang dijadikan acuan untuk selalu meningkatkan 
performance karyawan agar lebih berkualitas. Kualitas karyawan inilah yang 
menjadi salah satu faktor penting dalam keberhasilan perusahaan. 
Dalam memenuhi kebutuhan karyawan yang dapat mendorong tercapainya 
visi, misi dan sasaran perusahaan, saat ini telah banyak perusahaan yang 
menerapkan suatu sistem bernama kompetensi dalam mengelola SDM-nya. 
Kompetensi merupakan karakteristik dasar individu yang berhubungan dengan 
kriteria efektif dan atau performansi terbaik dalam menjalankan suatu tugas atau 
menghadapi suatu situasi (Spencer & Spencer, 1993).  
Sistem kompetensi inilah yang digunakan oleh PT  X , sebuah perusahaan 
yang bergerak di bidang jasa konstruksi, berdiri di Jakarta pada tanggal 11 Maret 
1960. PT  X  merupakan perusahaan konstruksi berskala nasional. Mereka telah 
menangani banyak proyek besar seperti pembangunan jalan tol Cipularang, 
pembangunan komplek perumahan Kota Baru Parahyangan, pembangunan 
Breakwater Pertamina, dan lain sebagainya. PT  X  memiliki visi menjadi juara 
sejati di bisnis jasa konstruksi dan mitra pilihan dalam bisnis jasa perekayasaan 
dan investasi infrastruktur di Indonesia dan beberapa negara terpilih. Misinya 
adalah menciptakan nilai yang berkesinambungan kepada pelanggan, karyawan, 
pemegang saham, dan berbagai pihak lain yang berkepentingan; memperkokoh 
kompetensi inti, memperluas kapabilitas dalam jasa perekayasaan, serta 
mengembangkan kapabilitas dalam jasa investasi secara selektif; berkecimpung 
aktif dalam program Public Private Partnership (PPP) untuk mendukung 
pertumbuhan ekonomi, menjalankan inisiatif-inisiatif Corporate Social 
Responsibility (CSR) dalam rangka pengembangan kemanusiaan. PT  X  
memiliki komitmen dalam memenuhi kepuasan pelanggan, yaitu dengan filosofi 
Teamwork, Innovation, Integrity, Excellence. Target kualitas PT  X  adalah 
memberikan produk dan pelayanan yang sesuai dengan peraturan dan spesifikasi 
yang telah dijanjikan dan untuk mencapai target perusahaan dengan tingkat 
kecelakaan nol (zero accident).  
PT  X  saat ini memiliki 1.325 karyawan, terdiri atas 415 karyawan tetap 
dan 910 karyawan kontrak. Sumber daya manusia adalah aset utama dari sebuah 
perusahaan dalam menjalankan bisnisnya. Kompetisi terberat dari bisnis 
konstruksi adalah pengembangan sumber daya manusia secara konstan dengan 
cara kerja yang sistematis dan terencana dengan baik. Dalam industri konstruksi, 
pengalaman dan dedikasi tinggi karyawan merupakan faktor mendasar yang 
menentukan kesuksesan sebuah perusahaan (www.pt x .com).  
Menyadari bahwa PT  X  merupakan sebuah perusahaan besar dan telah 
beberapa kali menerima penghargaan seperti ISO 9001:2000 dan SMK3L, 
berbagai usaha untuk meningkatkan kepuasan pelanggan sebagai pengguna jasa 
terus dilakukan. Perbaikan sistem manajemen, pengembangan performa kerja 
karyawan, penambahan karyawan yang berkualitas seakan menjadi syarat utama 
atas keberhasilan perusahaan, karena itulah mereka selalu berusaha untuk 
mewujudkan sistem manajemen terbaik dan meningkatkan kualitas karyawan. 
Dalam menjalankan bisnisnya, PT  X  mengacu pada sebuah manual 
perusahaan, yaitu detail pelaksanaan proses bisnis PT  X  secara menyeluruh. Isi 
dari manual perusahaan tersebut antara lain adalah Detail Proses Bisnis Proyek 
yang berisi aturan tentang Management Responsibility (Tanggung Jawab 
Manajemen), Resources Management (Pengelolaan Sumber Daya), Project 
Operation (Operasional Proyek), dan Measurement, Analysis and Improvement 
(Pengukuran, Analisa dan peningkatan). Manual perusahaan ini menjadi panduan 
bagi para karyawan PT  X  dalam menjalankan kegiatan bisnis secara 
Dalam mengelola sumber daya manusianya, PT  X  sejak tahun 1999 
telah menyusun dan menerapkan sistem kompetensi dari Hay Management guna 
mewujudkan harapan perusahaan untuk memiliki sumber daya manusia yang 
berkualitas. Kompetensi inilah yang dijadikan acuan dalam mengelola SDM-nya. 
Diharapkan pengaplikasian sistem kompetensi dapat membawa dampak positif 
bagi perusahaan, seperti dalam seleksi karyawan dan mewujudkan karyawan 
dengan kinerja terbaik. Karyawan diharapkan mampu menjalankan tugas dan 
perannya dengan baik sesuai visi, misi, job description serta orientasi proses 
bisnis yang ditetapkan oleh perusahaan. Karyawan juga diharapkan mampu 
mengatasi berbagai tantangan yang muncul, seiring dengan perkembangan zaman 
dan ketatnya persaingan dengan perusahaan-perusahaan sejenis. Berdasarkan 
wawancara dengan Kepala Bagian SDM, penggunaan sistem kompetensi 
membawa pengaruh yang sangat baik dalam pengelolaan SDM seperti 
recruitment, selection, placement, promotion, coaching dan conselling. 
Kompetensi yang telah ada dibuat dengan mengacu pada visi, misi, dan 
orientasi proses bisnis perusahaan. Proses bisnis adalah suatu rangkaian proses 
atau aktifitas yang satu sama lain memiliki hubungan sehingga menjadi suatu 
sistem yang terpadu dan terintegrasi untuk menjamin pencapaian output (Manual 
Perusahaan PT  X  Edisi ke-6). Proses bisnis PT.  X  dirumuskan secara rinci 
untuk setiap bagiannya, misalnya Divisi Operasi & Cabang atau Bisnis Proyek. 
Proses bisnis ini disusun dan disepakati oleh seluruh jajaran Direksi perusahaan 
berdasarkan core business PT  X  yaitu melakukan bisnis konstruksi. Selama 
beberapa tahun terakhir, penggunaan kompetensi yang telah disusun dirasa sudah 
tepat bagi perusahaan ini, namun karena adanya perubahan tuntutan pasar 
terhadap perusahaan, maka terdapat beberapa hal yang menjadi tidak sesuai 
terhadap proses bisnis yang telah ditetapkan sebelumnya. Perubahan tuntutan 
pasar ini membuat PT  X  harus merevisi dan mengembangkan lingkup proses 
bisnis mereka. Salah satu pengembangan Proses Bisnis PT  X  ini adalah 
perluasan scope bisnis mereka ke luar negeri. PT  X  telah mendapatkan 
kepercayaan dari beberapa negara di Asia untuk mengerjakan proyek-proyek 
mereka. Ini merupakan suatu kemajuan yang sangat baik bagi perusahaan, maka 
PT  X  ingin lebih mengingkatkan kualitas kerja mereka agar dapat menunjukkan 
performa terbaik bagi dunia internasional. 
Revisi dan pengembangan salah satu proses bisnis tersebut menimbulkan 
berbagai dampak, mencakup ketidaksesuaian bagi kompetensi yang telah disusun. 
Salah satunya adalah kompetensi pada Kepala Proyek (Project Manager), yang 
termasuk ke dalam kelompok jabatan Production Management. Sebagai sebuah 
perusahaan kontraktor, PT  X  memfokuskan diri pada keberhasilan mendapatkan 
proyek dan cara untuk menjalankannya dengan hasil terbaik. Keberhasilan suatu 
proyek berada di tangan orang-orang yang terjun langsung pada setiap detil 
pekerjaan proyek tersebut, terutama Kepala Proyek. Sebagai orang yang 
memimpin jalannya pengerjaan suatu proyek, Kepala Proyek dituntut untuk 
mampu memahami cara terbaik dalam mengerjakan proyek tersebut. Tujuan 
umum jabatan Kepala Proyek adalah memimpin dan mengkoordinasi pelaksanaan 
proyek agar dapat berjalan sesuai dengan rencana, baik menyangkut biaya, waktu, 
mutu, serta membantu kelancaran proses penagihan. 
Dalam hubungan struktural, Kepala Proyek berada tepat di bawah Kepala 
Cabang. Mereka bertanggung jawab langsung terhadap Kepala Cabang. 
Sedangkan secara fungsional, Kepala Proyek merupakan jabatan tertinggi di 
setiap struktur organisasi proyek. Mereka membawahi seluruh personil yang 
bekerja untuk proyek tersebut, seperti Site Manager, Human Resources, 
Secretary, Administration and Finance, Site Office Engineer, Planning and 
Controling, General Superintendent, Logistic/ Proccurement, dan masing-masing 
Supervisor ini membawahi lagi sejumlah karyawan. Karena itulah jabatan Kepala 
Proyek merupakan jabatan kunci dari keberhasilan sebuah proyek. 
PT  X  sebenarnya telah menyusun suatu kompetensi untuk seluruh 
jabatan manajerial, termasuk Kepala Proyek. Dikarenakan adanya revisi dan 
pengembangan proses bisnis, maka kompetensi yang telah ada sebelumnya 
dianggap tidak lagi sesuai dengan kebutuhan PT  X  saat ini. Salah satunya 
adalah dengan dibuatnya Divisi Luar Negeri. PT  X  merasa bahwa mereka perlu 
menyusun ulang kompetensi khususnya untuk jabatan Kepala Proyek. Setiap 
Kepala Proyek PT  X  memiliki kesempatan untuk masuk ke dalam Divisi Luar 
Negeri. Mengingat tuntutan dan tanggung jawab dalam mengerjakan proyek luar 
negeri lebih besar, maka perusahaan merasa perlu untuk menyusun ulang 
kompetensi agar lebih tepat dan sesuai dengan kebutuhan perusahaan saat ini. Hal 
ini dilakukan untuk meningkatkan kualitas dan performa setiap Kepala Proyek 
sesuai dengan tuntutan dan kualifikasi yang dibutuhkan. Dengan adanya 
kompetensi baru untuk Kepala Proyek, khususnya yang akan ditempatkan di 
Divisi Luar Negeri, maka diharapkan setiap Kepala Proyek dapat menjalankan 
perannya dengan baik sesuai visi, misi, dan orientasi proses bisnis perusahaan, 
serta dapat mengatasi setiap tantangan yang muncul dan dapat meningkatkan 
kualitas performa mereka.  
Menurut hasil wawancara dengan tiga orang Kepala Proyek, menurut 
mereka kompetensi yang telah ada sebenarnya sudah cukup efektif dan sesuai 
dengan pekerjaan mereka, juga cukup membantu dalam melihat penilaian kerja 
mereka sendiri. 

