. Hilangnya pesan non verbal pada komunikasi elektronik ini menggiring 
pada perbedaan persepsi atau kesalah pahaman dalam komunikasi diantara 
mahasiswa yang mengirim dan menerima pesan. Sebagai contoh, saat 
mengirimkan pesan melalui chat jejaring sosial. Mahasiswa pengirim pesan 
mengirimkan beberapa pertanyaan pada mahasiswa penerima pesan. Mahasiswa 
yang menerima pesan tidak menjawab seluruh pertanyaan dan menjawab pesan 
tersebut dengan beberapa kata. Hal ini seringkali menimbulkan asumsi negatif 
dari salah satu pihak serta menduga-duga secara subjektif mengapa pesan tidak 
dijawab secara keseluruhan.  
 Kesalahan persepsi oleh kedua belak pihak yang melakukan komunikasi 
biasanya juga disebabkan oleh kurang matangnya pihak pengirim pesan dalam 
berpikir. Hal ini dikarenakan oleh komunikasi online yang dapat mengirim pesan 
dengan instan dan cepat sehingga pihak penerima tertekan untuk membalas 
dengan segera. Perasaan tertekan untuk membalas pesan dengan cepat ini 
terkadang membuat seseorang tidak mampu berpikir dengan matang dan 
seringkali pesan yang dikirim tidak sesuai dengan harapan pihak penerima 
sehingga kerapkali menimbulkan kesalahan persepsi. 
 Sedangkan pada Mahasiswa yang berada di kategori Communication 
technostress rendah, mereka dapat mengendalikan penggunaan komunikasi online 
pada smartphone yang mereka miliki. Sehingga mereka masih mampu 
menyeimbangkan penggunaan komunikasi online dengan komunikasi face to face 
dan terhindar dari munculnya berbagai kesalah pahaman tersebut. 
 Tipe technostress yang keempat adalah time technostress. Time 
technostress merupakan dampak negatif pada sikap, pikiran dan tingkah laku yang 
dialami oleh mahasiswa pengguna smartphone terkait dengan waktu yang 
dimiliki. Smartphone merupakan suatu bentuk teknologi yang memiliki kecepatan 
akses baik untuk perangkat itu sendiri maupun dalam mengakses informasi.  
 Sistem operasi dari smartphone didesain berbeda dengan telepon genggam 
sehingga kemampuan akses pada perangkat ini tergolong cepat. Pada Mahasiswa 
yang berada di kategori Time Technostress yang tinggi, kecepatan akses pada 
smartphone seringkali berpengaruh pada pribadi diri mereka. Mereka terbiasa dan 
beranggapan bahwa segala sesuatunya harus dilakukan dengan cepat. Hal ini 
berdampak pada diri mereka yang menjadi tidak sabar pada peangkat smartphone. 
Selain ketidaksabaran pada perangkat itu sendiri, dampak negatif tersebut juga 
turut terbawa pada kehidupan mahasiswa. Mahasiswa menginginkan segala hal di 
sekitarnya bergerak dengan cepat sehingga menjadi tidak sabar pada diri dan 
orang lain. Ketidaksabaran pada diri seringkali ditunjukkan dengan keinginan 
untuk menyelesaikan segala aktivitas dengan cepat. Sedangkan ketidaksabaran 
pada orang lain umumnya ditunjukkan dengan ketidaknyamanan mahasiswa 
ketika menunggu orang lain beraktivitas. 
 Ketika Mahasiswa terbiasa untuk mengakses segala sesuatu dengan cepat 
maka diri mereka pun turut beranggapan bahwa aktivitas yang mereka lakukan 
harus dilakukan dengan sesegera mungkin. Hal ini juga berdampak pada 
pemikiran mereka yang mudah terpecah ketika mengerjakan satu hal. Mereka 
ingin semua bergerak dengan cepat bahkan seringkali berdampak pada perilaku 
multitasking atau mengerjakan beberapa pekerjaan dalam satu waktu. 
 Sedangkan pada Mahasiswa yang berada di kategori Time Technostress 
rendah, mereka justru dapat memanfaatkan perangkat smartphone untuk 
menghemat waktu. Sebagai contoh yaitu, mengirim data perkuliahan melalui 
email sehingga tidak perlu mencopy banyak kertas dalam waktu yang lama. Hal 
tersebut dapat menghemat banyak waktu mereka, sehingga dapat memiliki banyak 
waktu luang untuk fokus ke dalam pekerjaan yang lebih penting.  
 Tipe technostress yang kelima adalah family technostress. Family 
technostress adalah dampak negatif pada sikap, pikiran dan tingkah laku yang 
dialami oleh mahasiswa pengguna smartphone karena kurangnya kualitas 
interaksi dalam keluarga. Hal ini disebabkan karena kehadiran smartphone yang 
ada dalam lingkup keluarga akan membentuk techno cocoon, yaitu individu 
pengguna smartphone akan menghabiskan waktunya dengan perangkat tersebut 
sehingga terisolasi dan tidak dapat berkomunikasi secara intens dengan orang lain.  
Keunggulan smartphone yang mampu dengan instan mengakses dunia 
maya terkadang menjadi perangkap tersendiri bagi mahasiswa pengguna 
smartphone. Pada mahasiswa yang berada di kategori Family Technostress tinggi, 
hal tersebut membuat mereka kurang mampu mengontrol penggunaan smartphone 
di lingkup keluarga. Mahasiswa pada kategori ini dapat menggunakan smartphone 
untuk mengakses dunia maya secara berlebihan atau bermain smartphone di 
berbagai situasi termasuk situasi keluarga.  
 Di saat keluarga berkumpul untuk menghabiskan waktu bersama, 
mahasiswa pengguna smartphone terus memandangi layar perangkatnya. Hal ini 
dapat membatasi komunikasi mereka dengan anggota keluarga dan menyebabkan 
dirinya menjadi terisolasi dari interaksi keluarga. Kualitas dari waktu bersama 
keluarga semakin berkurang karena kehadiran smartphone yang mengambil alih 
perhatian mahasiswa sehingga seringkali menghiraukan anggota keluarga saat 
sedang berkumpul dan berbincang satu sama lain. Sedangkan pada Mahasiswa 
yang berada di Family Technostress yang rendah, mereka mampu mengontrol 
penggunaan smartphone bahkan di dalam lingkup keluarga sehingga kehadiran 
smartphone tidak memberikan dampak yang negatif pada berkurangnya interaksi 
keluarga.  
 Tipe technostress yang terakhir adalah society technostress. Society 
technostress adalah dampak negatif pada sikap, pikiran dan tingkah laku yang 
dirasakan oleh mahasiswa pengguna smartphone terkait dengn cepatnya 
perkembangan teknologi. Smartphone memberikan kemudahan pada mahasiswa 
untuk mengakses informasi dari berbagai sumber. Akan tetapi pertumbuhan dari 
informasi di dunia maya sangat pesat dan dapat ditemukan dari berbagai sumber. 
 Saat mahasiswa ditugaskan untuk memilah data mengenai isu yang sedang 
berkembang di masyarakat saat ini, maka mahasiswa akan lebih memilih untuk 
memperoleh informasi melalui internet dibandingkan harus membaca buku. Hal 
ini dilakukan karena melalui internet dapat diperoleh berbagai sumber dengan 
mudah dan sesuai keinginan pengguna. Aplikasi pada smartphone pun telah 
banyak yang terhubung pada situs google atau berita aktual. 
 Pada Mahasiswa yang berada di society technostress yang tinggi, 
seringkali mereka merasakan information overload. Hal ini dikarenakan Informasi 
yang diperoleh dari dunia maya seringkali berbeda-beda satu sama lain dan tidak 
jelas sumbernya maupun keakuratannya karena situs informasi tersebut dapat 
dibuat oleh banyak orang yang belum tentu memiliki kompetensi di bidangnya. 
Hal ini dapat menyebabkan kebingungan pada mahasiswa karena merasa 
informasi yang diperoleh tidak jelas sumber dan keakuratannya.  
 Selain hal tersebut, interaksi yang terlalu sering dengan smartphone dapat 
menyebabkan Mahasiswa terisolasi dari lingkup sosialnya. Mahasiswa yang 
berada di kategori ini kerapkali mengakses smartphone mereka bahkan di dalam 
lingkungan sosialnya sehingga tanpa sadar mengabaikan sekitarnya. 
 Sedangkan pada Mahasiswa yang berada pada kategori Society 
Technostress yang rendah. Mereka mampu membatasi penggunaan smartphone 
yang berdampak pada pembatasan akses informasi yang diterima dari internet dan 
juga mampu berinteraksi dengan baik di lingkup sosialnya tanpa pengaruh 
smartphone.  
 Semakin sering mahasiswa merasakan berbagai dampak dari keenam tipe 
technostress, maka derajat technostress pada mahasiswa akan semakin tinggi. 
Berdasarkan uraian tersebut, dapat dilihat bagan kerangka pemikiran mengenai 
Derajat Technostress pada Mahasiswa Universitas  X  di Bandung yang 
menggunakan Smartphone, sebagai berikut :  
Rendah     Bagan 1.1. Bagan Kerangka Pemikiran 
1. Derajat Technostress pada Mahasiswa Universitas  X  di Bandung 
yang menggunakan Smartphone ditentukan oleh tipe technostress yaitu 
Learning Technostress, Boundary Technostress, Time Technostress, 
Communication Technostress, Family Technostress dan Society 
2. Semakin sering Mahasiswa Universitas  X  di Bandung yang 
menggunakan Smartphone merasakan dampak negatif yang tergolong 
ke dalam keenam tipe technostress, maka semakin tinggi derajat 
 1. Hasil penelitian ini diharapkan dapat memberikan informasi pada Universitas  X  Bandung mengenai derajat technostress pada Mahasiswa yang menggunakan Smartphone.
 Definisi technostress pertama kali diungkapkan oleh seorang psikolog 
klinis Dr. Craig Brod pada tahun 1984. Dr. Craig Brod menyatakan bahwa 
technostress merupakan "a modern disease of adaptation caused by an inability to 
cope with the new computer technologies". Namun pada tahun 1997, Weil dan 
Rosen mengungkapkan kembali definisi dari technostress yaitu sebagai dampak 
negatif pada sikap, pikiran, tingkah laku atau fisiologis tubuh, yang disebabkan 
baik secara langsung maupun tidak langsung oleh teknologi. Technostress yang 
dialami langsung ketika berhubungan dengan teknologi menimbulkan perasaan 
keterasingan, meningkatnya level stress, dan perasaan ketergantungan. Dibalik itu, 
Technostress juga dapat dikarenakan ketidakmampuan saat menghadapi teknologi 
sehingga timbul perasaan terisolasi, cemas dan ketakutan.  
 Menurut Weil dan Rosen (1997), technostress memiliki 7 tipe. Akan tetapi 
sejalan dengan kerelevanan teori dalam penelitian ini, hanya digunakan 6 tipe dari 
7 tipe yang tersedia. Berikut ini adalah uraian dari 6 tipe technostress :  
Reaksi akan kemunculan teknologi pada setiap orang berbeda. Karakteristik reaksi 
tersebut terbagi menjadi 3 yaitu : eager adopters, hesitant "prove its" dan 
resisters. 
 Eager adopters merupakan tipe individu yang menyukai teknologi. 
Teknologi dianggap sebagai hal yang menyenangkan dan menantang bagi eager 
adopters. Saat teknologi mengalami gangguan, eager adopters berupaya mencari 
solusi atas permasalahan tersebut atau mencari orang lain yang ia angap lebih 
mampu menyelesaikannya. 
Eager Adopters berharap bahwa teknologi yang ia miliki mengalami 
gangguan karena mereka yakin bahwa mereka mampu menyelesaikan 
permasalahan tersebut. Bagi mereka, permasalahan pada teknologi merupakan hal 
yang lumrah. Akan tetapi saat mereka dapat menyelesaikan permasalahan pada 
teknologi, mereka akan merasa puas dan lengkap. 
  Hesitant "prove its" 
 Individu dengan karakteristik ini menganggap teknologi bukan sebagai 
suatu hal yang menyenangkan. Mereka lebih suka menunggu untuk 
menggunakannya saat teknologi tersebut terbukti bermanfaat. Jika seseorang 
mampu menunjukkan pada hesitant "prove its" bagaimana teknologi telah terbukti 
membuat hidupnya menjadi lebih mudah, ia akan berusaha 
mempertimbangkannya.  
 Hesitant "prove its" mempersonalisasi setiap permasalahan pada teknologi 
dan mengira bahwa mereka yang telah membuat permasalahan tersebut. Ketika 
permasalahan pada teknologi muncul, mereka cenderung memilih untuk meminta 
bantuan orang yang lebih mampu memperbaikinya karena mereka takut untuk 
bereksperimen.  
 Individu dengan tipe Resister cenderung menghindari teknologi. Tipe ini 
tidak tahu apa yang harus mereka lakukan dengan teknologi. Mereka tidak peduli 
apa yang orang lain katakan atau buktikan padanya bahwa teknologi berguna. 
Teknologi sama sekali tidak menyenangkan untuk mereka. Mereka yakin bahwa 
mereka akan merusak mesin atau gadget apapun yang mereka sentuh. Hal tersebut 
membuat mereka terintimidasi, malu, atau merasa sangat bodoh ketika harus 
berhubungan dengan teknologi. Ketika permasalahan pada teknologi muncul. 
Resisters merasa bahwa hal tersebut merupakan kesalahannya. Permasalahan pada 
teknologi seringkali menjatuhkan kepercayaan diri mereka. Setiap permasalahan 
membuat mereka semakin menjauh dari teknologi.  
 Dari ketiga karakteristik learning technostress yang telah dijelaskan di 
atas, eager adopters menampilkan perasaan positif lebih banyak pada teknologi 
namun mereka juga merasa frustasi. Frustasi merupakan bagian dari pengalaman 
teknologi walaupun pada individu yang menikmatinya. Hesitant "prove its" 
memiliki perasaan yang cenderung ambivalen. Di satu sisi ia merasa tidak 
nyaman, tidak pasti dan canggung, tetapi mereka juga dapat merasa bersemangat 
dan tertarik. Sedangkan Resisters cenderung merasa frustasi, gugup dan 
menyedihkan. 
Ketakutan dan penolakan pada teknologi dirasakan oleh banyak orang, 
namun melalui proses conditioning mereka dapat menjadi ketergantungan pada 
benda tersebut. Ketergantungan ini berkembang dan memberikan dampak negatif 
pada diri kita. Individu berubah menjadi machine-oriented dan kurang sensitif 
akan kebutuhan diri serta orang lain. Bahkan beberapa orang beresiko kehilangan 
identitas. 

