Ketika figur attachment cukup dekat dan tanggap, bayi biasanya merasa lebih aman dan 
memperlihatkan perilaku yang lucu dan eksploratif. Ketika figur attachment tidak dekat dan 
tanggap, bayi biasanya mengalami ketakutan dan kecemasan yang dapat mengakibatkan 
perilaku mencari kedekatan dengan figur attachment. Dinamika dari attachment berinteraksi 
dengan lingkungan pengasuhan yang berbeda-beda untuk menghasilkan banyak variasi dalam 
kepribadian dan perilaku. Sebagai contoh banyak bayi yang memperlihatkan gaya attachment 
secure, avoidant, atau anxious-ambivalent ketika ditempatkan pada situasi dengan mainan 
yang tidak dikenal, orang asing dan pemisahan sementara dari ibu (Kirkpatrick, 2002). 
Pada awalnya Bowlby bersama dengan Mary Ainsworth dan yang lainnya 
mengklasifikasikan attachment styles pada bayi dan orang dewasa menjadi tiga model yaitu 
(Cooper, 2009): 
1. Secure dan insecure 
2. Insecure/ avoidant  
3. Insecure/ ambivalent   
Kemudian penelitian ini diikuti  oleh Bartholomew (1990) dan Brennan, Clark, dan 
Shaver (1998). Mereka mengklasifikasikan  attachment styles  menjadi empat, yaitu: 
1. Secure 
2. Preoccupied (Anxious/Ambivalent) 
3. Dismissing (Avoidant) 
4. Fearful (Disoriented/ Disorganized) 
Pengertian tingkah laku lekat (attachment behavior) adalah beberapa bentuk perilaku 
yang dihasilkan dari usaha seseorang untuk mempertahankan kedekatan dengan seseorang 
yang dianggap mampu memberikan perlindungan dari ancaman lingkungan terutama saat 
seseorang merasa takut, sakit dan terancam. Berkaitan dengan tingkah laku lekat, Ainsworth 
(Papalia dan Old 1986) menyebutkan ada mekanisme yang disebut dengan  working model  
atau istilah Bowlby disebut dengan  internal working model . 
Dalam Eliasa (2011), konsep working model selanjutnya dikembangkan oleh Collins 
dan Read (dalam Pramana, 1996) yang terdiri dari empat komponen yang saling berhubungan, 
yaitu: 
1. Memori tentang kelekatan yang dihubungkan dengan pengalaman 
2. Kepercayaan, sikap dan harapan mengenai diri dan orang lain yang dihubungkan 
3. Kelekatan dihubungkan dengan tujuan dan kebutuhan (goal and needs) 
4. Strategi dan rencana yang disosiasikan dengan pencapaian tujuan kelekatan. 
Mc Cartney dan Dearing (2002) dalam Eliasa (2011) menyatakan bahwa pengalaman 
awal akan menggiring dan menentukan perilaku dan perasaan melalui internal working 
moadel. 
Adapun penjelasan mengenai konsep ini adalah,  Internal : karena disimpan dalam 
pikiran,  working : karena membimbing persepsi dan perilaku, dan  model : karena 
mencerminkan representasi kognitif dari pengalaman dalam membina hubungan. Anak akan 
menyimpan pengetahuannya mengenai suatu hubungan, khususnya pengetahuan mengenai 
keamanan dan bahaya. Model ini selanjutnya akan menggiring mereka dalam interaksi di 
masa yang akan datang. Interaksi interpersonal dihasilkan dan diinterpretasikan berdasarkan 
gambaran mental yang dimiliki seorang anak (Ervika, 2005, dalam Eliasa, 2011). 
Model ini diasumsikan bekerja di luar pengalaman sadar. Pengetahuan anak 
didapatkannya dari interaksi dengan pengasuh, khususnya ibu. Anak yang memiliki orang tua 
yang mencintai dan dapat memenuhi kebutuhannya akan mengembangkan model hubungan 
yang positif yang didasarkan pada rasa percaya (trust). Selanjutnya secara simultan anak akan 
mengembangkan model yang parallel dalam dirinya. 
Anak dengan orang tua yang mencintai akan memandang dirinya  berharga . Model ini 
selanjutnya akan digeneralisasikan anak dari orang tua pada orang lain, misalnya pada guru 
dan teman sebaya. Anak akan berpendapat bahwa guru dan teman adalah orang yang dapat 
dipercaya. Sebaliknya anak yang memiliki pengasuh yang tidak menyenangkan akan 
mengembangkan kecurigaan (mistrust) dan tumbuh sebagai anak yang pencemas dan kurang 
mampu menjalin hubungan sosial. 
Menurut Bowlby (Ervika, 2005, dalam Eliasa, 2011) internal working model dan figur 
attachment saling melengkapi serta saling menggambarkan dua sisi hubungan tersebut. Bayi 
yang diasuh dengan kehangatan, sensitifitas dan responsifitas akan mengembangkan internal 
working model yang positif pada orang tua dan diri sendiri. Internal working model 
merupakan hasil interpretasi pengalaman secara terus-menerus dan interaksinya dengan figur 
lekat. Ada dua faktor yang dapat meningkatkan kestabilan internal working model, yaitu:  
1) Familiar, yaitu pola interaksi yang berulang, cenderung akan menjadi kebiasaan 
2) Dyadic Pattern, yaitu pola yang timbal balik cenderung akan mengubah pola 
individual karena harapan yang timbal balik memerintahkan masing-masing pasangan untuk 
mengartikan perilaku pihak lainnya. 
Kirkpatrick bukan orang pertama yang mencetuskan mengenai persamaan antara 
attachment dengan agama. Analogi mengenai attachment pada umat Kristiani pertama 
dicetuskan oleh Reed (1978).  Setiap bentuk perilaku attachment, dan perilaku figur 
attachment, yang diidentifikasi oleh Bowlby, mempunyai pembanding yang dekat dengan 
gambaran hubungan berdasarkan Worshiper dengan Tuhan yang dapat ditemukan di kitab 
Mazmur.  (Kirkpatrick, 2005). 
Melihat bahwa hubungan orang dewasa dapat mencukupi banyak fungsi yang sama 
dengan attachment ibu dengan anak, Kirkpatrick (2002) menyatakan bahwa hubungan 
pribadi seseorang dengan Tuhan juga dapat mencukupi fungsi attachment. Secara jelas 
terdapat perbedaan yang penting antara Tuhan dan figur attachment manusia. Perbedaan yang 
paling mendasar adalah bahwa Tuhan adalah sosok yang non-physical. Attachment biasanya 
berkembang pada bayi berdasarkan interaksi fisik dengan manusia, hal ini terlihat tidak 
masuk akal bahwa hubungan attachment dapat terbentuk dengan ketuhanan yang non-
observable. Menanggapi hal ini, Kirkpatrick (1999) mengutip dari diskusi Bretherton (1987) 
mengenai bagaimana anak mengembangkan kapasitas kognitif untuk mempertahankan a 
sense of attachment  dalam ketidakhadiran dari interaksi yang konkrit (Jenay, 2010). 
Peneliti attachment telah mendeskripsikan bagaimana manusia membentuk internal 
mental representation dari figur attachment yang membantu dalam meregulasi distress 
walaupun figur tersebut tidak hadir secara fisik (Mikulincer & Shaver, 2004). Simbol mental 
atau image ini membantu manusia untuk mempertahankan rasa kedekatan ketika figur 
attachment tidak hadir, dan memotivasi penggunaan dari perilaku attachment untuk 
mempertahankan hubungan (Cicirelli, 2004). Proses kognitif dan kapasitas kognitif ini dapat 
digunakan pula untuk mengembangkan dan mempertahankan attachment to God (Kirkpatrick, 
1998, 1999; Schvaneveldt, 2008; Kirkpatrick, 1998; Miner, 2007; Miner et.al, 2009; Olthuis, 
2006, dalam Jenay, 2010). Attachment to God  menurut Okozi (2010) adalah ikatan 
afeksional yang nyata antara manusia dengan Tuhan sebagai sosok attachment.  
Metode taxometric mengindikasikan bahwa model dimensi lebih konsisten untuk 
menggambarkan perbedaan gaya attachment pada orang dewasa daripada model kategori. 
Brennan, Clark, dan Shaver (1998) dalam Jenay (2010) membuat lebih dari 320 item yang 
mewakili setiap attachment yang ada pada orang dewasa terhadap sampel dalam jumlah besar. 
Analisis faktor mengindikasi bahwa perbedaan dalam attachment individual lebih baik dilihat 
dalam two-dimensional space. Kedua dimensi tersebut adalah Anxiety about Abandonment 
dan Avoidance of Intimacy.  
Dimensi anxiety adalah adanya rasa takut dan pikiran berlebihan individu mengenai 
penolakan atau pengabaian dari figur attachment yaitu Tuhan, dan distress atau protes saat 
Tuhan dianggap tidak responsif atau tidak dapat dijangkau (Fraley et al, dalam Calvert, 2010). 
Sedangkan dimensi avoidance adalah rasa tidak nyaman (dan kecenderungan untuk 
menghindar) individu dari ketergantungan dan kedekatan dalam hubungan dengan Tuhan, 
dan self-reliance yang berlebihan (Brennan et al, dalam Calvert, 2010). Dengan adanya dua 
dimensi yang saling berkaitan tersebut, akan mendasari terbentuknya empat model 
attachment to God yaitu secure, preoccupied, dismissing, dan fearful yang akan berbeda pada 
masing-masing individu dan dipengaruhi oleh faktor-faktor yang dapat mempengaruhi 
attachment to God.  
Model attachment to God didapat melalui dimensi anxiety dan avoidance. Dalam two-
dimensional space, bagian anxiety dan avoidance yang rendah menunjukkan secure 
attachment, bagian anxiety tinggi dan avoidance rendah menunjukkan preoccupied 
attachment, bagian anxiety rendah dan avoidance tinggi menunjukkan dismissing attachment, 
bagian anxiety dan avoidance tinggi menunjukkan fearful attachment. Hal ini ditunjukkan 
pada Bagan 2.1. Pandangan dimensional terlihat pada attachment style pada anak-anak dan 
juga orang dewasa (Brennan et.al, 1998), dan istilah untuk kedua attachment style tersebut 
diperlihatkan dalam diagram. 
attachment style pada anak-anak adalah kata pertama, 
sedangkan istilah untuk attachment style adalah kata yang ada 
dalam kurung. 
Model attachment to God juga dapat dilihat melalui quad model yang diadaptasi dari 
Bartholowes dan Horowitz (1991) dan Griffin dan Bartholomew (1994) dengan mengganti 
 Model of Other  dengan  Model of God . Pada bagan ini mencakup model secure, 
preoccupied, fearful, dan dismissing attachment style to God. Untuk kedua dimensi (self dan 
other), skor yang rendah mengindikasi model yang positif dan skor yang tinggi mengindikasi 
model yang negatif, seperti yang dapat dilihat dalam Bagan 2.2. 
Bartholomew dan Horwitz (1991) dan Griffin dan 
Bartholomew (1994) dalam Okozi, 2010. 
Horwitz (1991) dan Griffin dan Bartholomew (1994) dalam Okozi (2010) 
mengkonseptualisasikan empat model attachment to God yaitu: 
1. Secure attachment (pandangan yang sehat terhadap diri sendiri dan Tuhan). Orang 
dengan attachment to God yang secure tidak akan khawatir akan ditinggalkan oleh Than. 
Mereka melihat Tuhan sebagai pribadi yang dapat diandalkan dan dapat dipercaya. Mereka 
ingin memiliki hubungan yang intim atau dekat dengan Tuhan. 
2. Preoccupied attachment (pandangan yang negatif terhadap diri sendiri dan 
pandangan positif terhadap Tuhan). Orang dengan preoccupied attachment style sering 
merasa bersalah dan malu secara berlebihan. Mereka sangat khawatir akan ditinggalkan atau 
 Positive (Low) Negative (High) 
Positive (Low) Secure 
Negative (High) Dismissing 
Menolak keintiman, 
keintiman, 
(Dependence/Anxiety) 
(Dependence/Anxiety) 
ditolak oleh Tuhan. Walaupun mereka sangat membutuhkan hubungan yang intim dengan 
Tuhan, mereka merasa bahwa mereka tidak cukup baik, dan dengan demikian mereka 
dipenuhi rasa iri hati dan kemarahan dan mereka sering mengeluh. 
3. Fearful attachment (pandangan yang negatif terhadap diri sendiri dan Tuhan).orang 
dengan fearful attachment style dipenuhi oleh ketakutan akan ditinggalkan Tuhan, namun 
mereka menolak untuk mengembangkan hubungan yang intim dengan Tuhan. Mereka dapat 
memiliki pandangan yang pesimis dalam hidup secara umum dan mengenai hal-hal di sekitar 
mereka. 
4. Dismissing attachment (pandangan yang positif terhadap diri sendiri dan pandangan 
yang negatif terhadap Tuhan). Orang dengan dismissing attachment style akan lebih 
mengandalkan diri sendiri dan kemampuan mereka dalam mencapai sesuatu dengan sedikit 
bantuan dari Tuhan. Mereka merasa bahwa mereka tidak dapat mengandalkan Tuhan karena 
Tuhan itu  unpredictable . Mereka juga cenderung lebih sedikit terlibat dalam aktivitas 
keagamaan, dan tidak peduli dengan banyaknya kehadiran saat pertemuan doa atau ibadah 
hari Minggu. Mereka pergi ke gereja, misalnya, sekitar satu kali dalam setahun, dan 
cenderung menghindar dalam membuat komitmen keagamaan. 
Attachment to God dapat terbentuk dalam dua cara yang berbeda, yaitu melalui model 
kompensasi dan model korespondensi (Kirkpatrick, dalam Okozi 2010). Hipotesis 
korespondensi mengatakan bahwa attachment style yang dialami oleh individu akan 
konsisten melintasi berbagai macam tipe ikatan, seperti misalnya hubungan dengan figur 
attachment utama, pasangan, dan Tuhan (Okozi, 2010). 
Dalam Kirkpatrick (2005), jika Tuhan berfungsi sebagai figur attachment secara 
psikologis dengan cara yang sama seperti figur attachment utama anak-anak dan pasangan 
orang dewasa, hal ini yang mengarahkan prediksi hipotesis korespondensi. Contohnya adalah 
orang-orang yang memiliki secure attachment style akan diharapkan melihat gambar Allah 
seperti relasi dengan manusia, yaitu sebagai sosok yang tersedia, figur attachment yang mau 
mendengarkan yang mengasihi dan peduli, dimana orang-orang yang avoidant melihat Tuhan 
sebagai sosok yang jauh, sulit dijangkau, atau dingin dan menolak, atau secara sederhana 
menganggap Tuhan tidak ada. Anak-anak memperoleh keyakinan mereka dari orang tua 
serupa dengan orang-orang mendapatkan pengaruh dari orang lain. 
Hipotesis kompensasi mengatakan bahwa hubungan dengan Tuhan dapat 
 menggantikan  kekurangan figur attachment utama dan atau ikatan cinta dewasa. Hal ini 
tidak menunjukkan adanya attachment to God yang tidak sehat, namun tidak berarti bahwa 
hal ini adalah awal dari attachment to God yang tidak sehat. Hipotesis kompensasi 
memberikan kerangka pikir untuk dapat memahami beberapa aspek dari keyakinan 
keagamaan, seperti misalnya resiliensi dan meningkatan spiritualitas saat mengalami distres 
atau kehilangan orang yang dicintai. (Okozi, 2010). 
Hipotesis ini muncul dari pertimbangan dimana kondisi dalam sistem attachment 
individu akan aktif ketika berada di bawah beberapa tingkat kriteria, seperti individu mulai 
merasa khawatir atau merasakan sosok attachment tersebut tidak cukup ada dan responsif. 
Perilaku attachment ini dimulai untuk mengembalikan kedekatan individu dengan figur 
attachment tersebut (Kirkpatrick, 2005). 
Poin dalam hipotesis kompensasi adalah pengamatan umum oleh para peneliti 
attachment bahwa anak-anak yang gagal untuk membangun attachment yang secure dari 
orang tua cenderung untuk mencari "pengganti" atau tokoh attachment pengganti, termasuk 
guru, kakak, saudara lain, atau, secara umum atau sesuatu yang lebih kuat, bijaksana yang 
andal dan terbukti dapat diakses dan responsif terhadap kebutuhan attachmentnya. 
(Ainsworth, 1985 dalam Kirkpatrick, 2005).  
Dari perspektif ini, Tuhan sebagai figur attachment dianggap paling penting 
dibandingkan individu yang lainnya. Dalam situasi seperti itu, keterikatan manusia dianggap 
tidak tersedia atau tidak memadai. Artinya, kontras dengan hipotesis korespondensi, 
kurangnya attachment manusia yang memadai mungkin diharapkan untuk motivasi atau 
mengaktifkan kepercayaan pada Tuhan yang ada, dalam cara yang penting, tidak seperti 
tokoh-tokoh attachment seorang manusia (Kirkpatrick, 2005).  
Konstruk lain yang telah mendapat perhatian dalam psikologi literatur agama adalah 
religious coping. Coping mengacu pada upaya aktif oleh seseorang untuk menemukan basis 
yang aman dalam menghadapi ancaman (Cooper, 2009). Coping sebenarnya dapat dilihat 
sebagai unsur dalam motivasi di balik mencari attachment figure.  
Murphy (1974, dalam Cooper, 2009) menggambarkan urutan dalam adaptasi anak 
terhadap ancaman, seperti petir. Ia melihat proses bergerak dari ekspresi rasa takut dan tidak 
berdaya, untuk mencari kenyamanan dari orang yang mendukung, untuk internalisasi sumber 
kenyamanan, dan kemudian ke simbolisasi dari sumber kenyamanan dan melalui 
pembentukan reaksi menjadi sumber penghiburan kepada orang lain. Murphy berfokus pada 
hasil yang positif. Dari literatur attachment terdapat hasil yang tidak positif (Cassidy & 
Shaver, 1999 dalam Cooper, 2009). Tentu saja, mencari kenyamanan dari orang yang 
mendukung dan menginternalisasi orang itu adalah inti dari attachment. Simbol-simbol 
agama bisa dilihat sebagai yang muncul keluar dari proses asli dengan perpindahan dan 
proyeksi benda asli (primary care giver) dengan Allah, gereja, atau simbol-simbol 
keagamaan lainnya. Hal ini juga didokumentasikan dalam literatur bahwa orang-orang 
menggunakan simbol-simbol agama untuk mencari kenyamanan ketika menghadapi stres 
kehidupan (misalnya, Granqvist, 2005; Pargament , 1997; Pargament et al, 1990 dalam 
Cooper, 2009). 
Coping kemudian dapat dilihat sebagai proses yang berkaitan dengan pengembangan 
attachments. Coping juga dapat dilihat sebagai hasil dari attachment style seseorang. 
Belavich dan Pargament (2002 dalam Cooper, 2009) telah menunjukkan bahwa skor 
attachment to God adalah prediksi coping spiritual dan coping spiritual adalah prediksi dari 
penyesuaian. Orang akan berharap bahwa secure attachment to God akan menyebabkan 
coping positif dan adaptasi yang lebih baik untuk situasi stres. Religious coping telah 
ditunjukkan untuk memainkan peran besar dalam kesehatan mental dan fisik seseorang dan 
telah dikaitkan dengan hasil berbagai situasi yang menantang (Pargament, Smith, Koenig & 
Perez, 1998).  
Pargament dan rekan-rekannya (dalam Cooper, 2009) mengidentifikasi beberapa pola 
religious coping: 
1. Metode koping positif cenderung terkait untuk mengamankan hubungan dengan 
Allah dan rasa hubungan spiritual dengan orang lain.  
2. Pola koping negatif mencerminkan hubungan kurang aman dengan Allah, 
pandangan tidak aman dan menjengkelkan dari dunia, dan konflik agama dalam 
mencari tujuan dan makna. Pola negatif ini ditemukan terkait dengan gangguan 
emosi yang lebih besar, kualitas hidup yang lebih buruk, lebih banyak gejala 
psikologis, dan ketidakpedulian terhadap orang lain.  
Penelitian Pargament menunjukkan adanya perbedaan dalam attachment to God 
seseorang akan  membuat individu tersebut melakukan religious coping dalam berbagai cara 
yang berbeda. 
sumber kekuatan dalam menghadapi permasalahan hidup, menggunakan religious coping 
yang positif. Mereka biasanya tidak merasa kurang puas dibandingkan  model attachment 
to God yang lainnya. Saat mengalami stres, mereka cenderung mengalihkan pikiran 
mereka terhadap hal-hal dan kegiatan yang bersifat keagamaan dibandingkan fokus pada  
masalah  mereka.  Individu ini cenderung mencari emotional support dibandingkan 
dengan dismissing atau fearful attachment to God. 
coping yang sama dengan  secure attachment to God. Namun individu dengan 
preoccupied attachment to God cenderung terus menerus membutuhkan jaminan cinta 
Tuhan dan cemburu saat Tuhan terlihat lebih mengasihi orang lain. Individu ini 
cenderung mencari emotional support dibandingkan dengan dismissing atau fearful 
attachment to God. 

