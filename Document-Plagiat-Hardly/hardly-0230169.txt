Bila berbicara mengenai gaya kepemimpinan, maka tidak akan terlepas dari 
satu hal yang juga menjadi perhatian, yaitu persepsi orang lain; salah satunya adalah 
persepsi bawahan atau juniornya. Dalam persepsi aturan dasarnya adalah kita melihat 
apa yang ingin kita lihat. Maka dalam hal ini bawahan atau junior akan melihat apa 
yang ingin mereka lihat dari section headnya.  
Menurut Hamner dan Organ, orang secara selektif mempersepsi hal-hal yang 
akan memuaskan kebutuhan mereka. Individu tidak melihat orang lain sebagaimana 
adanya mereka, namun apa arti mereka untuk individu itu sendiri  we do not see 
people as they are, we see them for what they mean to us.  (W. Clay Organ & Dennis 
W. Organ, 1978: 89). Hal senada juga disampaikan oleh Leavit (1972) bahwa orang 
mempersepsi apa yang mereka pikir akan membantu memuaskan kebutuhan mereka, 
mengabaikan hal yang sedikit mengganggu, bahkan memperhatikan hal-hal yang 
mereka ketahui atau rasakan membahayakan mereka. Selain itu nilai-nilai pribadi, 
harapan, sikap, pengalaman masa lalu, dan kepribadian setiap orang berbeda.  
Seorang section head memiliki karakteristik-karakteristik tertentu yang 
melekat pada dirinya, misalnya tampilan fisik, sikap, jenis kelamin, bahkan ras. 
Karakteristik yang melekat ini akan dipersepsi secara berbeda oleh bawahan atau 
juniornya karena dalam diri setiap bawahan atau junior terdapat harapan, nilai-nilai 
tertentu, sikap, dan pengalaman masa lalu terhadap karakteristik-karakteristik yang 
melekat pada section head tersebut. Sehingga perbedaan persepsi antara section head 
dan bawahan atau juniornya dapat terjadi 
Situasi saat persepsi itu dilakukan juga turut berperan. Seorang bawahan atau 
junior yang bertemu dengan section headnya pada situasi yang kooperatif, maka 
kemungkinan section head tersebut dipersepsi sebagai seorang yang mudah diajak 
bekerja sama dibandingkan bila mereka bertemu section headnya pada situasi yang 
tidak kooperatif. 
Dari penjelasan tersebut maka dapat terjadi perbedaan persepsi antara section 
head dan bawahan atau juniornya karena memang setiap orang akan memandang 
berbeda suatu hal meskipun yang dilihat sama. Section head dapat mempersepsi 
dirinya sedemikian rupa namun belum tentu apa yang ia persepsikan sama dengan 
bawahan atau juniornya.  
Secara singkat kerangka berpikir peneliti dapat dilihat pada bagan berikut: 
a. Para section head divisi H1, H2, dan H3 PT. Daya Adira Mustika Bandung 
akan menunjukkan gaya kepemimpinan initiating structure dan consideration 
dengan derajat yang berbeda. 
b. Beberapa section head divisi H1, H2, dan H3 PT. Daya Adira Mustika 
Bandung akan menunjukkan gaya kepemimpinan initiating structure dan 
consideration dengan derajat yang tinggi. 
Divisi H1, H2, H3 
PT. Daya Adira 
Initiating Structure: 
tertentu/deadlines 
Consideration: 
kepercayaan, dan kenyamanan 
dalam pekerjaan,  
c. Beberapa section head divisi H1, H2, dan H3 PT. Daya Adira Mustika 
Bandung akan menunjukkan gaya kepemimpinan initiating structure dengan 
derajat yang tinggi dan consideration dengan derajat yang rendah. 
d. Beberapa section head divisi H1, H2, dan H3 PT. Daya Adira Mustika 
Bandung akan menunjukkan gaya kepemimpinan initiating structure dengan 
derajat yang rendah dan consideration dengan derajat yang tinggi. 
e. Beberapa section head divisi H1, H2, dan H3 PT. Daya Adira Mustika 
Bandung akan menunjukkan gaya kepemimpinan initiating structure dan 
consideration dengan derajat yang rendah. 
Sesuai dengan permasalahan yang akan diteliti, maka pada bab ini peneliti 
akan menjelaskan beberapa definisi kepemimpinan menurut para ahli dan konsep 
kepemimpinan menurut teori perilaku. Beberapa definisi kepemimpinan tersebut 
adalah sebagai berikut: 
Oxford English Dictionary mencatat awal kemunculan kata leader, pemimpin 
dalam bahasa Inggris di sekitar tahun 1300-an. Sedangkan kata bentukan leadership, 
kepemimpinan baru muncul pada tahun 1800-an. Tentang definisi kepemimpinan 
yang berbobot ilmiah, diungkapkan secara bervariasi oleh para ahli dalam 
pengkajiannya tentang konsep tersebut. Walau berbeda-beda sejumlah kesamaan 
dapat ditarik dari keragaman pandangan tersebut. 
Harold Koontz dan Cyrill O. Donnell (1976) menjabarkan kepemimpinan 
sebagai seni membujuk bawahan untuk menyelesaikan pekerjaan-pekerjaan mereka 
dengan semangat keyakinan.  
Menurut Daniel Goleman kepemimpinan adalah suatu kegiatan yang 
dilakukan seseorang untuk mempengaruhi individu/kelompok melalui proses 
komunikasi terarah, dalam suatu hubungan interpersonal, agar individu/kelompok 
mau berusaha mencapai tujuan yang sudah ditetapkan. Selain itu, kepemimpinan juga 
berarti proses membangkitkan kemauan pada orang lain untuk mencapai suatu 
sasaran atau membuat orang lain ingin melakukan sesuatu seperti yang diinginkan. 
Dalam The Art of Motivating&Leading People, diungkapkan bahwa 
kepemimpinan atau leadership adalah kemampuan yang membantu anda untuk 
membimbing suatu organisasi atau kelompok orang dalam arahan yang 
menguntungkan atau menuju tujuan yang berharga.  ability that helps you guide an 
organization or a group of people in a beneficial direction or to a valuable 
destination.  (Walter J. Wadsworth, 2001)  
Atas dasar itu dapatlah disimpulkan bahwa kepemimpinan adalah rangkaian 
kegiatan penataan berupa kemampuan mempengaruhi perilaku orang lain agar 
bersedia bekerja sama untuk mencapai tujuan yang telah ditetapkan dengan cara 
membimbing dan mengarahkan.  
Pendekatan perilaku memfokuskan diri secara eksklusif pada apa yang 
dilakukan pemimpin dan bagaimana mereka bertindak. Berbagai penelitian membagi 
perilaku pemimpin menjadi dua dimensi mendasar yaitu perilaku tugas dan perilaku 
hubungan. Perilaku tugas mengarah pada penekanan tugas atau pekerjaan dalam 
memimpin, sedangkan perilaku hubungan lebih mengarah pada penekanan jalinan 
hubungan dalam pekerjaan sehingga menimbulkan rasa nyaman antara pemimpin dan 
bawahan.  
Banyak penelitian telah dilakukan untuk meneliti gaya kepemimpinan. 
Beberapa di antaranya adalah penelitian yang dilakukan oleh Ohio State University 
dan University of Michigan  pada tahun 1940-an. Pada awal tahun 1960-an, Blake dan 
Mouton mengadakan penelitian yang menguak tentang bagaimana manajer 
menggunakan perilaku tugas dan hubungan dalam seting organisasi. Penelitian beliau 
dikenal dengan nama Managerial Grid. 
Dilatarbelakangi oleh ketidakpuasan atas penelitian kepemimpinan 
berdasarkan ciri-ciri tertentu (traits), maka sejumlah peneliti di Ohio State University 
mulai menganalisis bagaimana individu bertindak saat mereka memimpin kelompok 
atau organisasi. Penelitian ini dilakukan dengan melibatkan bawahan untuk 
melengkapi kuesioner tentang pemimpin mereka. Di dalam kuesioner tersebut 
bawahan diminta untuk mengidentifikasi seberapa sering pemimpin mereka 
berperilaku tertentu.  
Kuesioner asli yang digunakan dalam penelitian tersebut didasarkan pada 
lebih dari 1800 item yang menggambarkan aspek yang berbeda dari perilaku seorang 
pemimpin. Dari daftar panjang ini, sebuah kuesioner berhasil diformulasikan menjadi 
150 pertanyaan yang pada akhirnya alat ukur tersebut dikenal dengan nama Leader 
Behavior Description Questionnaire (LBDQ; Hemphil & Coons, 1957). LBDQ telah 
diberikan pada ratusan individu dalam seting pendidikan, militer, dan industri, dan 
hasilnya menunjukkan beberapa perilaku mencerminkan perilaku pemimpin. 
Peneliti peneliti tersebut menemukan bahwa respon bawahan pada kuesioner 
tersebut dikelompokkan menjadi dua tipe umum perilaku pemimpin yaitu initiating 
structure dan consideration (Stogdill, 1974). Gaya initiating structure dapat 
dikatakan sebagai perilaku tugas, yang termasuk di dalamnya perilaku seperti 
mengorganisir pekerjaan, memberikan struktur pada seting pekerjaan, mendefinisikan 
tanggung jawab peran, dan menjadwalkan kegiatan dalam pekerjaan. Sedangkan 
perilaku consideration merupakan perilaku hubungan yang termasuk di dalamnya 
perilaku seperti membangun kepercayaan, penghargaan, keyakinan, dan saling 
menyukai antara pemimpin dan pengikutnya. 
Penelitian Ohio State memaparkan bahwa dua tipe perilaku atau gaya ini 
sebagai dua hal yang berbeda dan independen. Kedua perilaku ini tidak dilihat 
sebagai satu kontinum tetapi sebagai dua kontinua yang berbeda. Sebagai contoh, 
seorang pemimpin tinggi pada gaya initiating structure dan tinggi atau rendah pada 
gaya consideration. Begitu pula sebaliknya, seorang pemimpin dapat rendah pada 
gaya initiating structure dan rendah atau tinggi pada gaya consideration. Dengan 
singkat dapat dikatakan bahwa derajat gaya yang satu tidak berhubungan dengan 
derajat gaya yang lain. 
Banyak penelitian yang telah dilakukan untuk menentukan gaya manakah 
yang paling efektif dalam situasi tertentu. Dalam beberapa konteks pekerjaan, 
consideration yang tinggi sangat efektif. Namun dalam kondisi pekerjaan yang lain 
initiating structure yang tinggi lebih efektif. Banyak penelitian juga mengungkapkan 
bahwa memiliki initiating structure dan consideration yang tinggi adalah yang 
terbaik bagi kepemimpinan (Peter G. Northouse, 2004). 
Menurut Clifford T. Morgan persepsi adalah suatu proses membedakan 
rangsang (stimulus) yang satu dari yang lain dengan melakukan interpretasi terhadap 
rangsang tersebut. 
Edgar F. Huse & James L. Bowdith menjelaskan bahwa persepsi merupakan 
proses pengorganisasian data yang masuk ke sistem sensorik. Leavit (1972) 
mengungkapkan bahwa persepsi didefinisikan sebagai proses dimana individu 
mengorganisasi, menginterpretasi, mengalami, dan memroses isyarat atau material 
(input-input) yang diterima dari lingkungan luar.  Perception is defined as the 
process by which people organize, interpret, experience, and process cues or 
material (inputs) received from the external environment .  
Dari definisi-definisi tersebut maka dapat ditarik kesimpulan bahwa persepsi 
adalah suatu proses penyadaran rangsang-rangsang yang hadir di sekitar kita sehingga 
terjadi penilaian atau pengertian tentang rangsang tersebut atau memberikan 
makna/arti pada suatu objek berdasarkan stimulus indrawi. 
Menurut Hamner dan Organ dalam Organizational Behavior: An Applied 
Psychological Approach, orang secara selektif mempersepsi hal-hal yang akan 
memuaskan kebutuhan mereka. Kita tidak melihat orang lain sebagaimana adanya 
mereka, namun apa arti mereka untuk kita.  

