Dalam konsep kepemimpinan terdapat sikap dan perilaku untuk
mempengaruhi pekerja-pekerja yang menjadi bawahannya agar mampu
berperilaku kerja yang diharapkan, yaitu bekerjasama dalam pencapaian tujuan
organisasi. Dalam hal ini kepemimpinan dapat berpengaruh terhadap aktivitas
bawahan tergantung pada peran pimpinan tersebut didalam mendorong dan
mengarahkan bawahan untuk mencapai tujuan perusahaan serta persepsi bawahan
terhadap kepemimpinan pimpinannya, karena persepsi merupakan proses
pemaknaan terhadap stimulus yang ada di lingkungan yang selanjutnya dapat
mempengaruhi tingkah laku individu selanjutnya.
Gaya kepemimpinan yang akan dibahas dalam penelitian ini adalah gaya
kepemimpinan situasional yang merupakan gaya kepemimpinan yang cenderung
berbeda-beda dan satu situasi ke situasi yang lain. Dalam kepemimpinan
situasional sangat penting bagi setiap pimpinan untuk mengadakan diagnosa yang
baik mengenai kematangan (kemampuan dan kemauan) bawahan, karena salah
satu kelebihan dari gaya kepemimpinan situasional tersebut adalah dapat membuat
hubungan antara pimpinan dan bawahan menjadi lebih tleksibel karena dapat
disesuaikan dengan keadaan ataupun situasi yang sedang dihadapi bawahan.
Fleksibilitas sangat diperlukan bagi seorang pimpinan, karena didalam bekerja
pimpinan akan menghadapi kematangan bawahan yang berbeda-beda, dan hal ini
menuntut gaya kepemimpinan yang sebaiknya disesuaikan dengan kematangan
bawahan. Hal ini merupakan kunci sukses perusahaan yang selanjutnya dapat
meningkatkan produktivitas kerja (Hersey & Blanchard, 1994). Selain gaya
kepemimpinan, terdapat faktor lain yang dapat menunjang produktivitas kerja
bawahan, antara lain fasilitas kerja yang diberikan perusahaan, volume kerja yang
dihadapi oleh bawahan, keterampilan kerja bawahan dalam menyelesaikan
pekerjaan atau permasalahan yang dihadapi bawahan, kerjasama kelompok dalam
bekerja, penghargaan yang diberikan perusahaan atau pimpinan kepada bawahan
serta tujuan dan sasaran kerja yang harus dilakukan bawahan (Timpe, Seri
Manajemen SDM 2000).
Untuk mengukur produktivitas kerja bawahan, PT. X melakukan penilaian
terhadap produktivitas kerja bawahan dalam bentuk nilai kerja individu (NKI)
berdasarkan pencapaian target atau sasaran yang telah ditetapkan dalam sasaran
kerja individu (SKI) dalam jangka waktu satu tahun. nilai kerja individu (NKI)
bawahan kemudian dikategorikan kedalam lima tingkat pencapaian produktivitas
kerja, sebagai berikut:
PI: (pencapaian target < 85%)
P2 : (pencapaian target 85% - 97%)
P3 : (pencapaian target 97% - 103%)
P4 : (pencapaian target 103% - 110%)
P5 : (pencapaian target> 110%)
Berdasarkan hasil penilaian terakhir perusahaan terhadap bawahan yang
menduduki posisi sebagai Officer Sirkit di Divisi Terresterial terjadi penurunan
produktivitas kurang lebih sebesar 15% setelah terjadi pergantian kepemimpinan
di Divisi Terresterial pada tahun 2002 lalu. Penurunan ini dapat dilihat karena di
bawah kepernirnpinan yang berbeda pada tahun 2001, pencapaian target dapat
rnencapai persentase 100% - 110% atau berada dalarn kategori P3 (pencapaian
target 97% - 103%) dan P4 (pencapaian target 103% - 110%) yang ditetapkan
oleh perusahaan. Hal ini berarti pencapaian target atau sasaran yang telah
ditetapkan dalarn Sasaran Kerja Individu (SKI) tercapai. Sedangkan pada tahun
2002, pencapaian target berada pada persentase 95% - 100%, atau berada dalarn
kategori P2 (pencapaian target 85% - 97%) dan P3 (pencapaian target 97% -
103%). Hal ini berarti pencapaian target atau sasaran yang telah ditetapkan dalarn
Sasaran Kerja Individu (SKI) kurang tercapai, karena perusahaan rnenetapkan
pencapaian target sarna dengan atau lebih baik dari P3.
Sebagian besar bawahan tidak dapat rnencapai target yang telah ditentukan
oleh perusahaan dengan persentase kurang dari 100%, antara lain tidak siapnya
sarnbungan transrnisi tepat pada waktunya sehingga rnengganggu sinyal transrnisi
telekornunikasi. Dengan penurunan produktivitas yang terjadi tersebut, pelayanan
terhadap jasa telekornunikasi rnenjadi rnenurun, sehingga rnernunculkan keluhan
dari para penggunajasa atau pelanggan.
Berdasarkan hasil wawancara penulis terhadap 20 bawahan yang
rnenduduki posisi Officer Sirkit, diperoleh informasi bahwa plrnpman yang
diharapkan oleh bawahan adalah yang dapat rnernberikan perhatian penuh
terhadap produksi barang dan jasa, rnernberi sernangat dan rnotivasi kerja
bawahan rnelalui penggunaan pendekatan yang baik, serta rnerniliki fleksibilitas
dalarn rnenghadapi setiap perrnasalahan yang rnuncul di lingkungan kerjanya.
Selain itu, sebanyak 12 orang bawahan (60%) rnernaknakan pirnpinannya lebih
mengutamakan pencapaian hasil kerja dan target tanpa menjalin komunikasi dua
arah yang baik dengan bawahan serta tidak lagi menjelaskan apa dan bagaimana
cara penyelesaian tugas. Berdasarkan Kepemimpinan Situasional, pemimpin di
atas menerapkan gaya kepemimpinan delegating, dimana pimpinan lebih menaruh
kepercayaan kepada bawahan untuk dapat menyelesaikan tugasnya sendiri tanpa
menjalin komunikasi dua arah yang baik dengan bawahan atau menjelaskan lagi
apa, kapan dan bagaimana menyelesaikan tugasnya. Menurut 10 orang (83,3%)
dari 12 orang bawahan tersebut, gaya kepemimpinan seperti di atas membuat
bawahan menjadi kurang tennotivasi dan kurang dapat mengerjakan tugasnya
dengan baik karena sebenamya bawahan masih memerlukan bimbingan dari
pimpinannya dalam menyelesaikan tugas, dan hal tersebut dapat mempengaruhi
pencapaian sasaran atau target bawahan. Namun 2 orang lainnya (16,7%)
meskipun memaknakan hal yang sama tetap dapat menyelesaikan tugasnya
sehingga pencapaian sasaran atau target dapat tercapai.
Sedangkan 8 orang (40%) dari 20 orang bawahan tersebut memaknakan
pimpinannya lebih mengutamakan pencapaian terhadap sasaran dan hasil kerja,
dengan cara memberikan penjelasan yang terperinci mengenai apa, kapan dan
bagaimana menyelesaian tugas, namun tidak melakukan komunikasi dua arah
yang baik dengan bawahan. Berdasarkan Kepemimpinan Situasional, pimpinan di
atas menerapkan gaya kepemimpinan telling, dimana pimpinan lebih menekankan
pada pemberian pengarahan dan pengawasan yang spesifik dan terperinci
mengenai apa, kapan dan bagaimana menyelesaikan tugas. Menurut 5 orang
(62,5%) dari 8 orang bawahan tersebut, mereka dapat lebih tennotivasi untuk
menyelesaikan tugas karena dapat lebih menguasai tugas yang dilakukan
berdasarkan pengarahan yang jelas dan spesifik yang diberikan oleh pimpinannya.
Meskipun pimpinan kurang atau tidak melakukan komunikasi dua arah yang baik,
namun pencapaian sasaran atau target dapat tercapai. Sedangkan 3 orang lainnya
(37,5%) merasakan pimpinannya terlalu kaku karena kurang melakukan
komunikasi dua arah yang baik dan terlalu memfokuskan pada tugas membuat
mereka jadi kurang termotivasi dan merasa kurang dipercaya dalam
menyelesaikan tugas sehingga pencapaian sasaran atau target tidak dapat tercapai
dengan baik.
Berdasarkan hal di atas, menunjukkan bahwa bawahan yang menduduki
posisi sebagai Officer Sirkit memiliki persepsi yang berbeda-beda terhadap gaya
kepemimpinan yang diterapkan pimpinannya. Bawahan yang mempersepsi
pimpinannya menerapkan gaya kepemimpinan delegating, ada yang menunjukkan
pencapaIan sasaran atau target yang baik namun ada juga yang menunjukkan
pencapaIan sasaran atau target yang kurang baik. Begitu pula halnya dengan
bawahan yang mempersepsi pimpinannya menerapkan gaya kepemimpinan
telling, ada yang menunjukkan pencapaian sasaran atau target yang baik namun
adajuga yang menunjukkan pencapaian sasaranatau target yang kurang baik.
Berdasarkan permasalahan diatas, penulis tertarik untuk meneliti
Hubungan Efektivitas Gaya Kepemimpinan Atasan dengan Produktivitas Kerja
Bawahan yang Menduduki Posisi sebagai Officer Sirkit Divisi Terresterial PT.
X Bandung.
Dalam penelitian ini, penyusun meneliti sejauh mana hubungan efektivitas
gaya kepemimpinan atasan dengan produktivitas kerja Officer Sirkit Divisi
Terresterial PT. X Bandung.
Maksud diadakannya penelitian ini adalah:
Untuk mengetahui hubungan efektivitas gaya kepemimpinan atasan dengan
produktivitas kerja Officer Sirkit Divisi Terresterial PT. X Bandung.
Tujuan diadakannya penelitian ini adalah:
Untuk mendapatkan gambaran mengenai seberapa erat hubungan efektivitas gaya
kepemimpinan atasan dengan produktivitas kerja Officer Sirkit Divisi Terresterial
PT. X Bandung.
,
Penelitian ini diharapkan dapat berguna untuk menambah informasi
pengetahuan, terutama yang berkaitan gaya kepemimpinan dan produktivitas.
Bagi peneliti lain, hasil penelitian ini dapat dimanfaatkan sebagai referensi
atau rujukan.
Hasil penelitian ini diharapkan dapat memberikan sumbangan pemikiran
(masukan) bagi perusahaan untuk meningkatkan produktivitas kerja bawahan,
salah satunya melalui peranan gaya kepemimpinan.
Kepemimpinan dalam suatu organisasi merupakan suatu faktor yang
menentukan berhasil tidaknya suatu organisasi. Kepemimpinan adalah proses
mempengaruhi aktivitas seseorang atau sekelompok orang untuk mencapai tujuan
dalam situasi tertentu (Hersey & Blanchard, 1982). Oleh karena itu,
kepemimpinan yang baik akan menunjukkan pula pengelolaan suatu organisasi
yang berhasil. Efektivitas kerja seorang pimpinan ditentukan oleh kematangan
kerja bawahannya. Kematangan (maturity) merupakan kemampuan dan kemauan
(ability dan willingness) orang-orang untuk memikul tanggungjawab untuk
mengarahkan perilaku mereka sendiri (Hersey & Blanchard, 1982). Oleh karena
itu, para pimpinan berkewajiban untuk memberikan bantuan dan dorongan agar
bawahan dapat mencapai prestasi yang optimal dan tepat waktu dalam
melaksanakan pekerjaan atau tugas mereka.
.
Gaya kepemimpinan yang diterapkan oleh pimpinan selanjutnya ditangkap
oleh bawahan melalui persepsi mereka dalam situasi kerja sehari-hari. Persepsi
merupakan proses penerimaan stimulus, mengorganisasikan, memberikan makna,
menguji sampai memberikan respon terhadap stimulus. Proses persepsi
dipengaruhi oleh bermacam faktor, antara lain proses belajar, pengalaman masa
lalu, suasana hati dan kebutuhan individu yang bersangkutan (Schiffman &
Kanuk, 1987). Jika bawahan mempersepsi gaya kepemimpinan yang diterapkan
oleh pimpinannya sesuai dengan kematangan yang dimilikinya, maka bawahan
akan merasa pimpinannya mampu memberikan bantuan atau dukungan. Hal
tersebut membuat bawahan menjadi tennotivasi, sehingga bawahan dapat
melaksanakan atau menyelesaikan pekerjaannya sesuai dengan tujuan yang telah
ditetapkan, yang pada akhimya dapat meningkatkan produktivitas kerja bawahan.
Produktivitas merupakan tingkat pencapaian sasaran atau target secara
efektif dalam hal kualitas dan kuantitas yang telah ditentukan (Ivancevich, 1980).
Berdasarkan pemyataan tersebut dapat dikatakan bahwa produktivitas kerja
adalah seberapa b~myak jumlah barang atau jasa yang dihasilkan berdasarkan
target yang telah ditentukan. Begitu pula halnya dengan bawahan yang menduduki
posisi sebagai Officer Sirkit Divisi Terresterial PT. X yang memproduksi jasa
pelayanan telekomunikasi yang membutuhkan pekerjaan dengan produktivitas
yang tinggi. Dengan kata lain, bawahan harus mampu memenuhi desain produk
transmisi telekomunikasi yang diinginkan oleh pelanggan, serta selanjutnya dapat
mengoperasikannya sesuai dengan waktu. yang telah ditetapkan. Agar
produktivitas yang telah ditetapkan oleh perusahaan dapat dicapai, para bawahan
yang menduduki posisi sebagai Officer Sirkit Divisi Terresterial PT. X harus
selalu fokus dalam setiap proses pemecahan masalah, mampu merespon
perubahan yang sangat dinamis dalam bidang bisnis telekomunikasi ke dalam
program kerjanya, dan mampu memenuhi kebutuhan pelanggan sesuai dengan
kualitas dan waktu yang telah ditetapkan.
Pemasangan transmisi telekomunikasi serta penanganan gangguan
transmisi telekomunikasi yang dihadapi oleh bawahan yang menduduki posisi
sebagai Officer Sirkit Divisi Terresterial PT. X, harus dapat diselesaikan secara
efektif dan efisien sesuai dengan waktu atau target yang telah ditetapkan. Efektif,
jika alat komunikasi yang dipasang dapat berfungsi dengan baik atau gangguan
yang ada dapat diselesaikan dengan baik sehingga tidak terjadi lagi pada masa
yang akan datang. Sementara dikatakan efisien, jika pemasangan alat komunikasi
tersebut sesuai dengan sasaran atau target yang telah ditentukan atau
menyelesaikan masalah utama yang menimbulkan gangguan dalam transmisi
telekomunikasi.
Untuk mewujudkan hal tersebut seorang plmpman harus dapat
menerapkan gaya kepemimpinan yang sesuai dengan kematangan bawahan agar
bawahan dapat mempergunakan seluruh potensi yang ada pada dirinya sehingga
hal tersebut dapat memotivasi bawahan untuk dapat melaksanakan atau
menyelesaikan pekeIjaannya sesuai dengan target yang telah ditetapkan sehingga
dapat menghasilkan produktivitas yang tinggi. Apabila hal di atas dapat dicapai,
maka gaya kepemimpinan yang diterapkan oleh pimpinan menjadi efektif
Bawahan yang tidak mampu untuk menyelesaikan tugas atau pekeIjaannya
(kematangan rendah), akan membutuhkan pimpinan yang menerapkan gaya
kepemimpinan telling (memberitahukan) atau selling (menjajakan), karena gaya
kepemimpinan tersebut memiliki efektivitas paling tinggi bagi bawahan dengan
tingkat kematangan ini.
Pimpinan lebih memberikan pengarahan serta pengawasan yang spesifik
pada bawahan dalam melaksanakan pekerjaannya, sehingga akan timbul
keselarasan antara gaya kepemimpinan yang diterapkan oleh pimpinan dengan
kematangan yang dimiliki oleh bawahan. Bawahan akan merasa nyaman dalam
bekerja, merasa yakin serta termotivasi dalam melaksanakan dan menyelesaikan
pekerjaan atau tugasnya, sehingga dapat meningkatkan produktivitas keIjanya.
Namun, apabila pimpinan menerapkan gaya kepemimpinan participating
(mengikutsertakan) atau delegating (mendelegasikan) yang lebih memberikan
kepercayaan penuh kepada bawahan untuk menentukan bagaimana, kapan serta
dimana melaksanakan pekeIjaan, terhadap bawahan yang memiliki kematangan
yang rendah, maka gaya kepemimpinan yang diterapkan oleh pimpinan menjadi
tidak efektif, karena gaya kepemimpinan yang diterapkan oleh atasan tidak sesuai
dengan apa yang diharapkan atau dibutuhkan oleh bawahan.
Hal tersebut akan menimbulkan ketidakselarasan antara gaya
kepemimpinan yang diterapkan oleh pimpinan dengan kematangan yang dimiliki
oleh bawahannya. Bawahan merasa tidak nyaman dalam bekerja, bingung serta
menjadi kurang termotivasi dalam melaksanakan dan menyelesaikan pekerjaan
atau tugasnya, sehingga dapat membuat produktivitas kerja bawahan menurun.
Sementara, bagi bawahan yang mampu untuk menyelesaikan tugas atau
pekerjaannya (kematangan tinggi). Gaya kepemimpinan participating atau
delegating yang lebih memberikan kepercayaan kepada bawahan untuk
menentukan bagaimana, kapan serta dimana melaksanakan dan menyelesaikan
pekerjaan atau tugasnya, akan memiliki efektivitas paling tinggi bagi bawahan
dengan tingkat kematangan ini.
Bawahan akan merasa diberi kepercayaan penuh terhadap pelaksanaan
pekerjaan atau tugasnya, sehingga bawahan menjadi termotivasi dan pada
akhirnya dapat meningkatkan produktivitas kerja bawahan. Namun, apabila
bawahan yang telah memiliki kematangan yang tinggi dipimpin oleh pimpinan
yang menerapkan gaya kepemimpinan telling atau selling, maka akan muncul
perasaan kurang nyaman dalam diri bawahan pada saat bekerja. Hal tersebut dapat
terjadi karena gaya kepemimpinan yang diterapkan oleh pimpinan tidak efektif,
karena gaya kepemimpinan yang diterapkan oleh atasan tidak sesuai dengan apa
yang diharapkan atau dibutuhkan oleh bawahan.
Pimpinan lebih menekankan pada pemberian pengawasan, penjelasan dan
pengarahan yang spesifik terhadap pekeIjaan atau tugas yang dihadapi bawahan
serta tidak memberikan kepercayaan penuh terhadap bawahan untuk menentukan
sendiri bagaimana, kapan serta dimana melaksanakan atau menyelesaiakan
pekerjaan atau tugasnya yang sebenarnya telah dimengerti dan dikuasai oleh
bawahannya. Akibatnya, bawahan akan menjadi kurang termotivasi dalam
menyelesaikan pekeIjaannya karena bawahan merasa kurang dipercaya dan
dianggap kurang mampu untuk menyelesaikan pekerjaannya sendiri, dan hal ini
dapat mempengaruhi pencapaian produktivitas bawahan yang bersang~utan.
Berdasarkan hal diatas, persepsi bawahan terhadap gaya kepemimpinan
pimpinannya dapat mempengaruhi motivasi bawahan dalam melaksanakan atau
menyelesaikan pekeIjaannya, tergantung pada efektivitas gaya kepemimpinan
yang diterapkan oleh pimpinannya.
Apabila gaya kepemimpinan yang diterapkan oleh pimpinan sesuai dengan
kematangan yang dimiliki oleh bawahan, maka gaya kepemimpinan yang
diterapkan oleh pimpinan menjadi efektif sehingga bawahan menjadi termotivasi
dan dapat menyelesaikan pekerjaannya sesuai dengan target yang telah ditetapkan
sehingga produktivitas kerja bawahan dapat meningkatkan. Sebaliknya, apabila
gaya kepemimpinan yang diterapkan oleh pimpinan tidak sesuai dengan
kematangan yang dimiliki oleh bawahan, maka gaya kepemimpinan yang
diterapkan oleh pimpinan menjadi kurang atau tidak efektif Hal tersebut dapat
menyebabkan bawahan menjadi kurang tennotivasi serta tidak dapat
menyelesaikan pekeIjaannya sesuai dengan target yang telah ditetapkan, sehingga
produktivitas kerja bawahan menjadi menurun.
Namun, selain gaya kepemimpinan terdapat faktor-faktor yang berkaitan
dengan situasi dan kondisi keIja bawahan, yang dapat mendukung bawahan untuk
dapat menghasilkan produktivitas kerja yang baik. Faktor-faktor tersebut, yaitu
fasilitas kerja, volume kerja, keterampilan kerja, kerjasama kelompok dalam
bekerja, penghargaan yang diterima dalam bekerja serta sasaran dan tujuan keIja.
Seberapa besar pengaruh faktor-faktor tersebut, juga tergantung kepada persepsi
masing-masing bawahan. Apabila bawahan mempersepsi faktor-faktor tersebut
sangat mendukung bawahan dalam bekeIja, maka faktor-faktor tersebut dapat
mendukllng bawahan untuk dapat menghasilkan produktivitas keIja yang tinggi.
Sebaliknya, apabila bawahan mempersepsi faktor-faktor tersebut kllrang
---
mendukung bawahan dalam bekerja, maka faktor-faktor tersebut dapat menjadi
penghambat bagi bawahan untuk dapat menghasilkan produktivitas yang tinggi
(Timpe, Seri Manajemen SDM: 2000).
.
-------
,- - - - - - - - - --I
, ,
~,
s:::
.9I tIS , s:::
s::: VJ 0..I cI:I , 's., cI:I I,
s:::
I VJ 0.. '-
~0..0.. I
'"~, 0.. tIS S I tIS bb~ ~~Q, Q).s::: Q) , ;;.-.
~I-<0.. , tIS
~,Q) Q) Q) I C)
I p.,f-<~ , I I I II I U
C'!L.__________~
------------
-----------
~---------------I
r i ~
,
,
,
,
,
,
,
,
,
,
,
,
,
,
,
"----------------
"r;) I
cI:I '50:
.:::
01),
+-' s::: I
0'''''
,
:;8 f-< :
,
~
,
',S::: I
, cI:I s:::
'I
01)
, S:::.s::: I
I .5 tIS I
tIS ::::
: S tIS:
,Q)"c,
,~ I
, I
---.---
Dari uraian kerangka pikir diatas, peneliti mempunyai asumsi yaitu:
Bawahan yang menduduki posisi Officer Sirkit memiliki persepsi yang
berbeda-beda tentang efektivitas gaya kepemimpinan yang diterapkan oleh
plmpmannya.
Pimpinan yang menerapkan gaya kepemimpinan sesuai dengan situasi dan
kondisi bawahan, maka akan menimbulkan keselarasan antara gaya
kepemimpinan yang diterapkan pimpinan dengan kematangan bawahan. Hal
tersebut membuat gaya kepemimpinan menjadi efektif sehingga akan
memotivasi bawahan dalam menyelesaikan pekeIjaannya.
Pimpinan yang menerapkan gaya kepemimpinan tidak sesuai dengan situasi
dan kondisi bawahan, maka akan menimbulkan ketidakselarasan antara gaya
kepemimpinan yang diterapkan pimpinan dengan kematangan bawahan,
membuat gaya kepemimpinan menjadi tidak efektif sehingga akan membuat
bawahan kurang termotivasi dalam menyelesaikan pekerjaannya.
Motivasi yang tinggi dapat mendukung bawahan untuk menghasilkan
produktivitas kerja yang tinggi.
Motivasi yang rendah kurang dapat mendukung bawahan untuk menghasilkan
produktivitas kerja yang tinggi.
Berdasarkan kerangka pemikiran diatas, maka hipotesis dari penelitian ini
adalah terdapat "hubungan antara efektivitas gaya kepemimpinan atasan dengan
produktivitas kerja bawahan yang menduduki posisi Officer Sirkit Divisi
Terresterial PT. X Bandung".
 Pada saat ini banyak perusahaan yang telah mendapatkan pengakuan di 
 dunia intemasional. Hasil tersebut tidak jarang didapatkan dalam jangka waktu 
 yang panjang dan disertai dengan usaha, kerja keras dan pengabdian dari para 
 bawahannya. Salah satunya adalah PT. X yang telah lama bergerak dalam bidang 
 telekomunikasi non seluler domestik. Pada tahun 2001 yang lalu, PT. X telah 
 memperoleh sertifikat World Class Operator (WCO) atau dengan kata lain PT. X 
 telah diakui oleh dunia intemasional sebagai operator kelas dunia. Hasil tersebut 
 diperoleh melalui perubahan didalam mekanisme kerja PT. X serta perbaikan 
 dalam melayani pelanggan. 
 Perubahan didalam mekanisme kerja diantaranya perubahan bentuk 
 perusahaan dari PERUM (Perusahaan Umum) menjadi PT (Perseroan Terbatas) 
 yang juga mengubah kebijakan yang berlaku di dalam perusahaan. Hal ini 
 merupakan konsekuensi yang diambil oleh PT. X untuk menembus pasar 
 intemasional dan supaya dapat memenangkan kompetisi dengan perusahaan lain 
 yang juga bergerak dalam bidang telekomunikasi. 
 Selain PT. X, saat ini terdapat 7 operator swasta yang beroperasi dalam 
 bidang telekomunikasi, yaitu Ratelindo, Excelcomindo, Komselindo, Satelindo, 
 Indosat, Mobisel dan Metrosel. Hal ini berdampak pada timbulnya persaingan 
 yang semakin ketat untuk menarik calon konsumen, sehingga hal ini membuat 
 konsumen semakin kritis didalam memilih dan menuntut pelayanan yang lebih 
 baik. 
 Selain terjadi perubahan didalam mekanisme kerja dan dalam cara 
 melayani pelanggan, pada tahun 1995 terjadi restrukturisasi intern pada PT. X 
 yang diantaranya terjadi perubahan dalam status hukum, pembentukan perusahaan 
 patungan dan penjualan saham. Selain struktur perusahaan menjadi lebih 
 sederhana, juga dibentuk divisi-divisi baru yang berfungsi untuk lebih menunjang 
 kerja PT. X, antara lain Oivisi Sistem Informasi, Oivisi Pelatihan, Oivisi Properti, 
 Oivisi Terresterial dan divisi pendukung lainnya. 
 Salah satu divisi yang menunjang kerja PT. X, yaitu Oivisi Terresterial 
 yang merupakan divisi yang langsung berhubungan dengan jasa pelayanan 
 telekomunikasi, karena Oivisi Terresterial memiliki usaha utama antara lain 
 menyediakan jasa Saluran Langsung Jarak Jauh (SLJJ), jasa telepon berbasis 
 network (jasa Inteligent Network/JAPATI), jasa sirkit (UPNW) dan jasa yang 
 berkaitan dengan penggunaan transponder satelit untuk kawasan Asia Pasifik. 
 Oivisi Terresterial ini terdiri dari Bidang Prasarana dan Penyedia Barang, Bidang 
 Sumber Oaya Manusia, Bidang Keuangan, Bidang Niaga, Bidang Provlog, Bidang 
 Pengendali Network, Bidang Harian serta Bidang Pengembangan dan Operasi. 
 Oivisi Terresterial ini memiliki 500 bawahan yang tersebar diberbagai kota, antara 
 lain Jakarta, Cibinong, Bandung, Tangerang dan Medan. 
 Berkaitan dengan usaha utama Oivisi Terresterial tersebut, terdapat suatu 
 posisi pekerjaan yang menunjang kerja Oivisi Terresterial, yaitu Officer Sirkit, 
 yang bergerak di Bidang Pengembangan dan Operasi dengan plmpman 
 langsungnya, yaitu Kepala Bagian Pengembangan dan Operasi. Bawahan yang 
 menduduki posisi sebagai Officer Sirkit ini memiliki sasaran utama pekerjaan 
 untuk merencanakan, melaksanakan dan mengendalikan kegiatan operasional 
 transmisi telekomunikasi secara efektif dan efisien agar mampu memberikan 
 pelayanan transmisi telekomunikasi yang cepat dan tepat dalam upaya meraih 
 kepercayaan dan kepuasan pelanggan yang bergerak dalam bidang 
 telekomunikasi. Berdasarkan sasaran utama yang hams dicapai didalam 
 pekerjaannya, terlihat jelas bahwa posisi Officer Sirkit tersebut memiliki peranan 
 yang penting dalam Divisi Terresterial. Bawahan hams dapat menciptakan 
 pelayanan yang semakin baik, antara lain dapat menyediakan peralatan yang 
 bermutu baik, mampu mengoperasikan transmisi telekomunikasi dengan baik 
 serta mampu menangani segal a bentuk gangguan atau keluhan yang dirasakan 
 oleh pengguna jasa dengan baik, sebagai competitive advantage dalam 
berkompetisi dengan penyelenggara jasa telekomunikasi lain. 

