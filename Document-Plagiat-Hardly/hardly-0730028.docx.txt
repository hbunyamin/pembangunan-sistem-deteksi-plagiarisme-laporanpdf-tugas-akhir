Perkembangan moral para remaja bertitik tolak dari rasa berdosa dan 
usaha untuk mencari proteksi. Tipe moral yang juga terlihat pada para remaja juga 
mencakupi: 
1) Self   directive, taat terhadap agama atau moral berdasarkan pertimbangan 
pribadi. 
2) Adaptive, mengikuti situasi lingkungan tanpa mengadakan kritik. 
3) Submissive, merasakan adanya keraguan terhadap ajaran moral dan agama. 
4) Unadjusted, belum meyakini akan kebenaran ajaran agama dan moral. 
5) Deviant, menolak dasar dan hukum keagamaan serta tatanan moral 
masyarakat. 
e. Sikap dan Minat 
Sikap dan minat remaja terhadap masalah keagamaan boleh dikatakan 
sangat kecil dan hal ini tergantung dari kebiasaan masa kecil serta lingkungan 
agama yang mempengaruhi mereka (besar kecil minatnya). 
Kelompok sosial merupakan kesatuan sosial yang terdiri dari kumpulan 
individu   individu yang hidup bersama dengan mengadakan hubungan timbale 
balik yang cukup intensif dan teratur, sehingga daripadanya diharapkan adanya 
pembagian tugas, struktur, serta norma   norma tertentu  yang berlaku bagi 
mereka.  
Berdasarkan besar kecilnya jumlah anggota kelompok, maka Charles 
Horton Cooley membedakan antara kelompok primer (primary group) dan 
kelompok sekunder (secondary group). Menurut Cooley, kelompok ditandai 
dengan adanya hubungan yang erat di mana anggota   anggotanya saling 
mengenal dan sering kali berkomunikasi secara langsung berhadapan muka (face 
to face) serta terdapat kerja sama yang bersifat pribadi atau adanya ikatan 
psychologis yang erat. Dari ikatan   ikatan psychologis dan hubungan yang 
bersifat pribadi inilah, maka akan terjadi  peleburan   peleburan daripada individu 
tujuan kelompoknya. 
Cooley menerangkan kelompok primer berdasarkan atas tiga tinjauan, 
yaitu: 
a. Kondisi   kondisi fisik kelompok primer 
1) Tidak cukup hanya hubungan saling mengenal saja, akan tetapi yang 
terpenting adalah bahwa anggota   anggotanya secara fisik harus saling 
berdekatan. 
2) Jumlah anggotanya harus kecil, agar supaya mereka dapat saling kenal 
dan saling bertemu muka. 
3) Hubungan antara anggota   anggotanya harus permanen. 
b. Sifat   sifat hubungan primer 
1) Sifat utama hubungan primer, yaitu adanya kesamaan tujuan di antara 
para anggotanya yang berarti bahwa masing   msing individu 
mempunyai keinginan dan sikap yang sama dalam usahanya untuk 
mencapai tujuan, serta salah satu pihak harus rela berkorban demi untuk 
kepentingan pihak lainnya. 
2) Hubungan primer ini harus secara sukarela, sehingga pihak   pihak yang 
bersangkutan tidak merasakan adanya penekanan   penekanan, 
melainkan semua anggota akan merasakan adanya kebebasan. 
3) Hubungan primer bersifat dan juga inklusif, artinya hubungan yang 
diadakan itu harus melekat pada kepribadian seseorang dan tidak 
digantikan oleh orang lain, dan bagi mereka yang mengadakan hubungan 
harus menyangkut segala kepribadiannya, misalnya perasaannya, sifat   
sifatnya, dan sebagainya. 
c. Kelompok   kelompok yang konkret dan hubungan primer 
Kelompok primer seperti yang digambarkan di atas kenyataan tidak terdapat 
pada msyarakat, artinya tidak terdapat kelompok primer yang sempurna sesuai 
dengan syarat   syarat yang telah disebutkan di atas. 
Kelompok primer ini dapat menguntungkan terhadap individu dan juga dapat 
membantu perkembangan individu. Adapun hal   hal yang menguntungkan 
terhadap individu sebagai berikut: 
1) dapat menunjang sifat   sifat baik manusia serta memberikan kekuatan dan 
dorongan kepada individu, sehingga dapat mengurangi sifat   sifat individu 
yang lemah. 
2) Dapat mempertebal ketergantungan individu terhadap kelompoknya. 
3) Semua hal didasarkan pada perasaan, artinya reaksi   reaksi yang 
diperlihatkan oleh masing   masing individu dalam kelompok didasarkan 
atas perasaan. 
Sedangkan hal   hal yang dapat membantu kelompok sosial ini terdapat 
individu antara lain: 
1) Dapat memperbesar rasa loyalitas. 
2) Dapat memberikan pegangan pada individu, agar supaya tidak mengalami 
kebingungan dan frustrasi. 
Jadi, kelompok primer ini sangar berguna sekali bagi individu, baik dalam hal 
kepentingan maupun keamanan individu sehubungan dengan adanya hubungan 
yang erat di antara para anggotanya.  
Penelitian metode komparatif adalah penelitian yang membandingkan 
keberadaan satu variable atau lebih pada dua atau lebih sample yang berbeda, atau 
pada waktu yang berbeda (Sugiyono,2009). 
Penelitian ini digunakan untuk mengetahui derajat dimensi-dimensi  
religiusitas pada mahasiswa yang mengikuti Kristen yang mengikuti PMK dengan 
mahasiswa Kristen yang tidak mengikuti PMK di Universitas  X  Bandung 
dengan menggunakan teknik survei, yaitu penelitian yang mengambil sampel dari 
suatu populasi dengan menggunakan kuesioner sebagai alat pengumpul data yang 
pokok (Masri Singarimbun, 1995). Secara skematis penelitian ini dapat 
digambarkan sebagai berikut:  
3. 2 Bagan Rancangan Penelitian 
dimensi-
dimensi-
 Variabel dalam penelitian ini adalah, yaitu derajat dimensi religiusitas 
pada mahasiswa Kristen yang mengikuti PMK dengan mahasiswa Kristen yang 
tidak mengikuti PMK di Universitas  X  Bandung. 
 Derajat dimensi religiusitas adalah tinggih rendahnya pemahaman dan 
penghayatan mahasiswa Kristen yang mengikuti PMK dengan mahasiswa Kristen 
yang tidak mengikuti PMK di universitas  X  Bandung mengenai ajaran agama 
yang dianutnya serta pengaplikasiaannya dalam aktivitas sehari   hari, yang 
terwujud melalui lima dimensi, yaitu ; 
a. Dimensi Ideologis (religious belief), yaitu seberapa besar keyakinan 
mahasiswa Kristen yang mengikuti PMK dengan mahasiswa Kristen yang 
tidak mengikuti PMK  di Universitas  X  Bandung terhadap ajaran-ajaran 
agama yang bersifat fundamental dan dogmatis, seperti mahasiswa 
memiliki keyakinan mengenai doktrin Allah, dosa, kasih, dan keselamatan. 
b. Dimensi Praktik Agama (religious practice), yaitu seberapa besar  
keyakinan mahasiswa Kristen yang mengikuti PMK dengan mahasiswa 
Kristen yang tidak mengikuti PMK  di Universitas  X  Bandung untuk 
mematuhi dan mengerjakan kegiatan-kegiatan ritual sebagaimana yang 
dianjurkan oleh agamanya, seperti melakukan saat teduh, mengikuti 
persekutuan, berdoa syafaat, membaca Alkitab dan beribadah. 
c. Dimensi Pengalaman dan Penghayatan (religious feeling), yaitu seberapa 
besar keyakinan mahasiswa Kristen yang mengikuti PMK dengan 
mahasiswa Kristen yang tidak mengikuti PMK  di Universitas  X  
Bandung untuk menghayati ajaran agamanya dan berelasi dengan 
Tuhannya, seperti merasakan sukacita saat melakukan praktik agama, 
merasakan dekat dengan Allah, merasakan ketenangan dalam menjalani 
kehidupan. 
d. Dimensi Pengetahuan Agama (religious knowledge), yaitu seberapa besar 
keyakinan mahasiswa Kristen yang mengikuti PMK dengan mahasiswa 
Kristen yang tidak mengikuti PMK  di Universitas  X  Bandung terhadap 
doktrin ajaran agama, seperti pengetahuan akan isi alkitab baik perjanjian 
lama dan perjanjian baru. 
e. Dimensi Pengamalan (religious effect), yaitu seberapa besar keyakinan 
mahasiswa Kristen yang mengikuti PMK dengan mahasiswa Kristen yang 
tidak mengikuti PMK  di Universitas  X  Bandung untuk melakukan 
ajaran agama dalam kehidupan sehari-hari, seperti membantu sesama, 
melakukan kebenaran.  
Alat ukur yang digunakan adalah alat ukur hasil modifikasi yang dibuat 
oleh Rocky Haryanto. Alat ukur ini terdiri dari 74 item dengan perincian 9 item 
untuk dimensi ideologis, 16 item untuk dimensi pengalaman, 6 item untuk 
dimensi pengamalan, 10 item untuk dimensi praktik agama dengan 4 pilihan 
jawaban, 25 item untuk dimensi pengetahuan dengan pilihan benar atau salah. 
Alat ukur yang digunakan adalah berupa kuesioner yang berisi pernyataan   
pernyataan untuk menjaring profil dimensi-dimensi religiusitas dengan dasar teori 
religiusitas dari Glock dan Stark, bahwa religiusitas meliputi lima dimensi yaitu 
dimensi ideologis (religious belief), dimensi praktik agama (religious practice), 
dimensi pengalaman dan penghayatan (religious feeling), dimensi pengetahuan 
agama (religious knowledge), dan dimensi pengamalan/konsekuensi (religious 
effect). 
Item-item untuk dimensi ideologis, dimensi pengalaman dan penghayatan, 
dan dimensi pengalaman/konsekuensi disatukan dalam satu kuesioner karena 
semuanya memiliki sifat yang sama yakni mengukur sikap dan perilaku 
religiusitas. Kuesioner untuk keempat dimensi ini dirumuskan dalam item positif 
dan item negatif. Kuesioner ini menggunakan skala Likert. Dalam kuesioner ini 
disediakan 4 pilihan jawaban, yaitu  Sangat Sesuai (SS) ,  Sesuai (S) ,  Tidak 
Sesuai (TS) , dan  Sangat Tidak Sesuai (STS) . 
Item-item untuk dimensi praktik agama disusun dalam satu kuesioner dan  
disusun dalam bentuk pertanyaan-pertanyaan yang masing-masing memiliki 4 
alterntif jawaban yang disusun berdasarkan frekuensi responden dalam melakukan 
praktik ritual keagamaannya. 
Item-item untuk dimensi pengetahuan agama disusun dalam satu kuesioner 
karena item-item tersebut bersifat mengukur keluasan pengetahuan agama 
responden. Item untuk dimensi pengetahuan agama disusun dalam bentuk 
pernyataan-pernyataan dan responden diminta untuk memberikan huruf  B 
(Benar)  pada pernyataan yang dianggap benar dan huruf  S (Salah)  pada 
pernyataan yang dianggap salah. 
Dimensi Indikator Item + Item - 
Memiliki keyakinan mengenai doktrin Allah 1 39 
Memiliki keyakinan mengenai doktrin Manusia 
Memiliki keyakinan mengenai doktrin Kristus  14 
30 19,22,33 
Merasa sukacita saat melakukan ritual keagamaan 
(berdoa, Saat Teduh, membaca Alkitab, beribadah, 
dan bersekutu) 
37,12 26,29,36 
Merasa dekat dengan Allah dalam situasi apa pun 6,9 27 
Merasa ketenangan dalam menghadapi berbagai 
 3,8,13,18 
Merasa sukacita dan bersyukur ketika 
mendengarkan kesaksian/pengalaman rohani 
orang lain dan menceritakan kesaksian rohani diri 
17 5,11,32 
Dimensi Membantu sesama (mengasihi, menolong teman 34 23 
Pengamalan yang membutuhkan, mendoakan teman) 
Menegakkan kebenaran dan keadilan (berani 
menegur teman yang salah, tidak melakukan hal 
yang merugikan orang lain) 
 16,20 
Menjaga lingkungan (tidak membuang sampah 
sembarangan) 
Ambisi (Tujuan hidup untuk berkarya buat Tuhan) 24,25 2,7 
Sikap mengenai harta benda, waktu, dan bakat 
(tidak menunda-nunda waktu, mengatur keuangan, 
menggunakan bakat) 
Penilaian yang benar mengenai pacaran (prinsip 
yang benar mengenai seks, prinsip dalam mencari 
dan menemukan pasangan hidup yang sesuai 
dengan kehendak Allah) 
15,31 28 
Penilaian yang benar terhadap Bangsa dan Negara 
1. Secara pribadi 
1. Saat Teduh 
2. Berdoa syafaat untuk orang lain 
3. Membaca Alkitab 
4. Persembahan persepuluhan 
5. Memberikan Persembahan 
6. Memberitakan Injil 
7. Bersaksi 
2. Secara bersama-sama 
1. Ibadah minggu di gereja 
2. Mengikuti Persekutuan doa 
3. Ikut Melayani 
Pengetahuan mengenai isi Alkitab: 
1. Perjanjian Lama 
2. Perjanjian Baru 
1,4,7,8,11,14,18,20,23,25 
2,3,5,9,12,15,17,19,22 
umat Kristen (Natal, Jumat Agung, 
Paskah, hari kenaikan Yesus, 
Pentakosta) 
10,13,16.24 
Kristen (Baptisan, Perjamuan Kudus) 
6,21 
Penghayatan, dan Dimensi Pengamalan/Konsekuensi) 
Pilihan Jawaban Item positif Item negatif 
Sangat Sesuai (SS) 4 1 
Sesuai (S) 3 2 
Tidak Sesuai (TS) 2 3 
Sangat Tidak Sesuai (STS) 1 4 
Nilai responden dihitung berdasarkan penjumlahan skor masing-masing 
dimensi religiusitas. Skor yang telah diperoleh diubah ke dalam bentuk persentase 
kemudian menghitung skor maksimum dan skor minimum setelah itu dihitung 
selisihnya. Pengukuran profil dimensi-dimensi religiusitas pada masing-masing 
dimensi diperoleh dengan cara menggunakan median, sehingga didapatkan 2 
kategori, yaitu skor yang berada di atas median tergolong ke dalam kategori 
tinggi. Sementara untuk skor yang berada di bawah median tergolong ke dalam 
kategori rendah. Masing-masing dimensi religiusitas memiliki skor profil yang 
berbeda-beda. 
 Alat Ukur ini didukung juga oleh data pribadi responden yaitu umur 
responden. Selain itu terdapat data penunjang berupa faktor-faktor yang 
mempengaruhi terhadap derajat dimensi-dimensi religiusitas yang meliputi faktor 
kepribadian, faktor lingkungan, faktor institusional, dan faktor masyarakat.  
Uji validitas dimasukkan untuk mengetahui taraf ketepatan suatu alat ukur 
dalam mengukur, validitas alat ukur ini bersifat Construct Validity, yaitu alat ukur 
yang dipergunakan adalah skala yang telah disusun berdasarkan teori yang sudah 
valid. Bila alat ukur telah memiliki Construct Validity berarti semua item yang 
ada pada alat ukur itu mengukur apa yang ingin diukur.  
Pengujian validitas pada alat ukur ini bersifat uji pakai dalam artian 
sample yang digunakan untuk mengambil data juga digunkan untuk uji coba alat 
ukur. Pengujian validitas dihitung menggunakan bantuan SPSS 17 for windows 
dengan menggunakan rumus Rank Spearman yang berskala Ordinal dan Pearson 
untuk skala interval.  
Kriteria yang digunakan untuk menentukan validitas sesuai dengan kriteria 
Lisa Freidenberg (Anastasi, 1997) dengan ketentuan sebagai berikut : 
r < 0,3 = item tidak valid, item tidak dapat dipakai. 
r ? 0,3 = item valid, item dapat dipakai 
Pengujian reliabiltas dilakukan untuk mengetahui sejauhmana hasil 
pengukuran itu relatif konsisten jika pengukuran dilakukan dua kali atau lebih 
pada waktu yang berbeda. 
Untuk memperoleh reliabilitas dari kuesioner derajat dimensi-dimensi 
religiusitas peneliti terlebih dahulu membuang item-item yang tidak valid. Peneliti 
kemudian menggunakan Split Half Methode dimana alat ukur dibagi menjadi dua 
bagian, yaitu item bernomor ganjil dan genap. Kemudian nilai item ganjil 
dikorelasikan dengan rangking nilai item genap. Pengujian reliabilitas dihitung 
menggunakan bantuan SPSS 17 for windows dengan menggunakan Rank 
Kriteria yang digunakan  untuk menyeleksi item didasarkan atas norma 
dari Alpha Gronbach, yaitu: 
0,00-0,59 : Reliabilitas rendah (item tidak dapat digunakan) 
0,60-1,00 : Reliabilitas tinggi (item dapat digunakan). 
Populasi sasaran dalam penelitian ini adalah mahasiswa Kristen baik yang 
mengikuti PMK dan yang tidak mengikuti PMK di Universitas  X  Bandung. 
Karakteristik sampel dari penelitian ini adalah: 
Karakteristik sampel dari penelitian ini adalah: 
Teknik penarikan sampel dilakukan dengan purposive sampling, yaitu 
pengambilan sampel berdasarkan karakteristik sampel yang telah ditentukan oleh 
peneliti dari populasi sasaran (Sudjana, 1984 dan Singarimbun, 1982). 
Teknik analisis data yang digunakan dalam penelitian ini adalah 
menggunakan: 
1) Distribusi frekuensi dari data primer, yaitu skor tinggi, sedang, 
rendahnya dimensi   dimensi religiusitas berdasarkan lima dimensi 
religiusitasnya dengan menghitung persentase tiap   tiap dimensi. 
2) Setelah itu dibuat tabulasi silang antara skor tinggi rendahnya masing   
masing dimensi religiusitas dengan data penunjang. 

