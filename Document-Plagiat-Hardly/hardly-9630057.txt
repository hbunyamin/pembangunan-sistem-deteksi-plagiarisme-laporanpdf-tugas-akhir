Pada era globalisasi saat ini, selaras dengan kemajuan teknologi, manusia
semakin dihadapkan pada berbagai pennasalahan yang rumit. Kecenderungan
yang dihadapi manusia di dunia bersumber dari kebutuhan manusia akan
kesejahteraan dan keamanan, baik individu, kelompok, bangsa, hingga negara.
Manusia jaman sekarang dituntut untuk selalu mengembangkan dirinya baik dalam
ilmu pengetahuan maupun teknologi, sebab hal ini dapat membantu individu untuk
menghadapi masa depan.
Salah satu kemajuan teknologi adalah diciptakannya pesawat udara yang
pada awalnya sangat sederhana. Sampai dengan empat dekade terakhir ini, telah
hadir pesawat-pesawat canggih yang merupakan perkembangan dunia penerbangan
yang dapat digunakan sebagai sarana transportasi ke luar negeri. Selain sebagai
sarana transportasi, pesawat udara juga digunakan sebagai alat untuk
mempertahankan dan menjaga kedaulatan negara di udara terhadap ancaman yang
berasal dari negara lain. Namun, kondisi ini tidak diimbangi dengan kemajuan fisik
manusia. Toleransi tubuh manusia sejak dulu hingga sekarang belum menunjukkan
perubahan yang berarti sehingga menimbulkan kesenjangan antara teknologi
pesawat terbang dan kemampuan manusia.
TNI-AU selaku penegak kedaulatan negara di udara, mempunyai tugas
pokok anw.ra lain mempertahankan keutuhan wilayah dirgantara nasional dan
menegakkan hukum di udara. Kesiapan dan kemampuan tempur TNI Angkatan
Udara sangat dipengaruhi oleh kualitas alat utama sistem senjata (alut sista) serta
kualitas sumber daya manusia yang mengawakinya. Kualitas alat utama sistem
senjata tergantung perkembangan teknologi yang telah diaplikasikan dalam wujud
hasil produk dari negara pembuat mesin perang khususnya alat utama sistem
senjata udara. Semakin antisipatif negara produsen terhadap tantangan yang
berkembang, maka aplikasinya dalam menciptakan alat utama sistem senjata akan
semakin berbobot teknologi yang paling mutakhir serta dapat diaplikasikan di
lapangan. Semakin mutakhimya alut sista menuntut sumber daya manusia yang
profesional di bidangnya, sehingga personel yang mengawaki alat utama yang
sangat sarat teknologi dapat secara profesional mengawaki alat utama tersebut.
Peran penerbang militer berbeda dengan penerbang sipil, penerbang militer
memiliki beban dan tanggung jawab yang lebih besar, karena mereka dituntut
untuk selalu siap selama 24 jam terutama pada tugas yang berbahaya seperti
perang, sehingga secara fisik maupun psikis harus selalu siap menghadapi berbagai
kondisi yang berat dan bahaya. Sebagai personil TNI, seorang penerbang militer
tentunya dituntut untuk semaksimal mungkin mempertahankan eksistensi
kedaulatan dan kemerdekaan negara. Upaya untuk memperoleh penerbang yang
tangguh dan memiliki kesiapsiagaan yang tinggi dilakukan melalui proses
pendidikan dan latihan yang teratur, mantap dan penuh disiplin.
Meskipun demikian, faktor manusia dengan segala keterbatasannya sebagai
makhluk bumi tetap merupakan titik lemah dalam sistem keamanan terbang.
lumlah kecelakaan penerbangan akhir-akhir ini yang terjadi di luar negeri dan
khususnya yang terjadi di Indonesia baik di lingkungan TNI atau TNI-AU maupun
non TNI tampak semakin meningkat. Setiap kecelakaan pesawat terbang selalu
membawa kerugian bagi negara, baik kerugian materiil dan terutama kerugianjiwa
manusia yang terlatih dan tidak dapat dinilai harganya. Berdasarkan laporan Civil
Aviation Authority sejak tahun enam puluhan sampai tahun delapan puluhan, 60%
kecelakaan terbang disebabkan pilot error (human factor) dan hanya 40%
disebabkan faktor lain di luar human factor. Kemudian suatu penelitian dari USAF
(United State Air Force) selama dekade 1960 - 1970 menyimpulkan bahwa
accident rate cenderung menurun tetapi persentase pilot error tetap tinggi. Nada
yang sarna juga dikatakan oleh Angkatan Udara Kerajaan Inggeris-Royal Air
Force (RAF) 1971, bahwa pada kecelakaan terbang sekitar 87% disebabkan pilot
error.
Kecelakaan dalam penerbangan yang diakibatkan oleh kelalaian manusia
tetap tidak dapat dihindarkan, kira-kira 75% dari kecelakaan pesawat terbang
diakibatkan oleh kesalahan manusia, baik penerbang, teknisi, maupun A TC
(pengatur lalu-lintas udara) dalam monitoring, managing, dan mengoperasikan
sistem pesawat (angka tersebut diambil secara keseluruhan dari kecelakaan pesawat
sipil maupun militer, Sheril L. Chappel, dalam Jensen, 1989). Hal ini dapat
dikaitkan dengan penjelasan Kepala Dinas Keselamatan Terbang dan Kerja
Angkatan Udara (KADISLAMBANGJAAU), Marsekal Pertama TNI
Suyanto, yang menyatakan bahwa dalam periode 10 tahun (1993 sid 2002) di
lingkungan TNI-AU, 17,14 % kecelakaan dalam penerbangan yang diakibatkan
oleh kesalahan penerbang, 74,46% akibat faktor mesin pesawat dan 8,24% akibat
faktor media (landasan, cuaca dan sebagainya.). Faktor manusia di sini, dibatasi
pada lingkup kesehatan jiwa, yaitu berkaitan dengan masalah proses pikir, emosi
dan perilaku manusia, yang dapat menjadi latar belakang suatu tindakan yang
merugikan keamanan terbang dengan akibat timbulnya kecelakaan.
Keberhasilan maupun kecelakaan terbang bergantung pada interaksi antara
tiga faktor utama yang juga dikenal dengan 3M, yaitu Mesin (material), Media
(cuaca dan fasilitas penerbangan), Manusia (fisik, mental dan sosial). Bila media
dan mesin pesawastnya dalam keadaan baik, maka faktor manusia, dalam hal ini
penerbang, sangat menentukan keberhasilan maupun kegagalan penerbangan.
Untuk mampu mengendalikan pesawat dengan baik, seorang penerbang harus
memiliki penalaran yang baik, keterampilan yang memadai serta pertimbangan
(judgement) yang tepat. Kesemuanya itu merupakan hasil akhir dari suatu latihan
dan proses belajar yang terus menerus dan tak kenaI lelah. Setiap kesalahan yang
kecil sekalipun dapat mengakibatkan timbulnya kecelakaan dengan akibat cacat
(mutilasi) atau bahkan kematian.
Ancaman mutilasi bahkan kematian merupakan stresor yang secara
potensial dapat menimbulkan kecemasan yang bila tidak ditanggulangi dengan
benar dapat menimbulkan berbagai tingkah laku yang muncul dalam perilaku
sehari-hari penerbang tersebut, misalnya menghindari kegiatan penerbangan.
Bahwa penerbangan militer itu berbahaya memang adalah kenyataan. Bahaya itu
secara sadar dialami sebagai rasa cemas. Pada penerbang rasa cemas tersebut
timbul karena risiko yang akan dihadapi pada saat terbang, bahwa "one can be
killed while flying" apakah itu karena kondisi pesawat yang kurang baik atau pada
diri penerbangnya, misalnya pada saat itll mereka merasa belum siap untuk terbang,
takut apabila teIjadi kecelakaan tidak dapat mengambil keputusan untuk berbuat
sesuatu, atau apabila menghadapi musuh yang peralatannya lebih canggih, timbul
perasaan takut mati tertembak. Oleh karena itu, seorang penerbang yang secara
fisik dan psikis sehat serta berkemampuan, dapat saja merasa cemas. Rasa cemas
ini terutama dialami sebelum atau pada waktu akan terbang, dan juga selama
terbang. Yang menjadi masalah adalah bagaimana seorang penerbang mengolah
rasa cemasnya pada saat rasa cemas itu timbul. Spielberger (1966 : 28)
menyimpulkan bahwa kecemasan merupakan suatu perasaan yang tidak enak
dikarenakan adanya antisipasi terhadap suatu situasi yang akan datang dianggap
sebagai bahaya dan ancaman.
Menurut Kol. Kes. Drs. Christian Hareva salah seorang psikolog di TNI-
AU, ada berbagai kemungkinan langkah yang diambil seorang penerbang untuk
mengolah rasa cemasnya. Dalam beberapa kasus misalnya, beberapa penerbang
menyadari bahwa menerbangkan pesawat itu bagi mereka sangat sukar. Mereka
memberikan reaksi dengan cara introspeksi secara realistik dan obyektif terhadap
dirinya. Kekurangan-kekurangan yang ditemukan pada diri penerbang, berusaha
diatasi dengan langkah-Iangkah positif yaitu dengan belajar, berlatih dan
sebagainya. Dengan demikian kemampuan mereka meningkat, kepercayaan diri
tetap terpelihara, dan jadi lebih dewasa dalam profesinya. Ini semua dilakukan
demi keselamatan terbang. Reaksi seperti ini dinilai adekuat.
Dalam beberapa kasus lain, penerbang yang bersangkutan memberikan
reaksi dengan cara "escape forward" atau "dashingforward", dalam arti perasaan
cemas yang dirasakan penerbang dialihkan dengan memperbanyak jam terbang,
sehingga perasaan cemas yang timbul sebelum terbang menjadi berkurang atau
hilang. Yang terjadi di sini adalah suatu kompensasi mental yang bersangkutan
secara sadar, dimana rasa cemas ditransformasikan menjadi energi agresi. Reaksi
ini juga disebut "dare-devil reaction". Apabila rasa cemas tidak diproses dan tidak
ditransformasikan seperti diutarakan di atas, akan dapat mengakibatkan kondisi-
kondisi gangguan pada psikomotor, seperti konsentrasi yang berlebihan,
ketegangan, tremor muskuler, serta ketagihan alkohol, nikotin dan obat-obat.
Akibat lain yang tak kurang pentingnya adalah menurunnya motivasi untuk
terbang. Bila penerbang secara sadar atau tidak, menghindar dari pembicaraan
mengenai kecemasannya, maka penurunan motivasi akan muncul dalam bentuk
menghindari tugas-tugas terbang, seperti berpura-pura sakit atau mengundurkan
diri dari status penerbang.
Wawancara yang dilakukan oleh peneliti terhadap 12 orang penerbang (5
orang penerbang yang belum pemah mengalami kecelakaan dan 7 orang penerbang
yang pemah mengalami kecelakaan), menunjukkan bahwa 4 penerbang yang belum
pemah mengalami kecelakaan, merasakan kecemasan pada saat sebelum terbang,
saat akan terbang (lepas landas atau take ojj) atau dalam perjalanan dan pada saat
mendarat (landing), dalam intensitas yang tidak berlebihan (masih dalam batas
wajar). Mereka melakukan latihan terbang sesuai dengan prosedur yang telah
ditentukan, namun terkadang ada perasaan ingin lebih sempuma, karena tidak ingin
mengalami hal serupa yang telah dialami oleh rekan sekerja. Namun 1 dari
penerbang yang belum pemah mengalami kecelakaan merasa takut dan cemas
untuk terbang pada saat mendengar temannya mengalami kecelakaan. Sedangkan
pada penerbang yang pemah mengalami kecelakaan, kecemasan yang timbul
sangat berlebihan dari biasanya, karena mereka tidak ingin hal serupa teIjadi lagi.
Pada saat akan terbang mereka akan selalu melakukan check dan recheck sampai
berulang-ulang, karena adanya rasa kurang percaya diri. Namun mereka
mengatakan kecemasan tersebut biasanya timbul tergantung pada kapan mereka
mengalami kecelakaan. Misalnya wawancara pada salah satu penerbang yang
pemah mengalami kecelakaan pada saat mendarat (landing), penerbang tersebut
s~lalu merasa cemas apabila tiba saatnya untuk mendaratkan pesawat. Ia mengakui
bahwa dirinya selalu melakukan check dan recheck apakah roda pesawat sudah
turun dan itu ia lakukan sampai berulang kali. Berbeda pula dengan penerbang
yang mengalami kecelakaan pada saat lepas landas (take off), sewaktu akan terbang
ia merasa selalu ingin buang air kecil terkadang juga sampai sakit perut. Namun
karena terbang adalah suatu keharusan, ia mencoba untuk mengalihkannya dengan
lebih berkonsentrasi dan berdoa. Sedangkan pada penerbang yang mengalami
kecelakaan dalam peIjalanan, kecemasan yang sangat berlebihan ia rasakan pada
saat di udara atau dalam perjalanan. Ia selalu melakukan pengecekan yang terkesan
over check terhadap penunjukan kondisi instrumen mesin (temperatur mesin, bahan
bakar, tekanan oli, jumlah oli, dan sebagainya) maupun instrumen penunjukan
penerbangan (kecepatan, mesin, artificial horizon, arah/kompas, radio komunikasi).
Tetapi 4 orang di antaranya menyatakan tidak terlalu khawatir dan cemas dengan
kejadian yang pemah dialami, mereka lebih percaya bahwa semua itu sudah diatur
oleh Yang Maha Kuasa, sehingga mereka memandang kejadian tersebut sebagai
suatu hal yang biasa dan sudah menjadi risiko sebagai seorang penerbang.
Stres emosional yang berkepanjangan atau kambuhnya stres emosi mungkin
juga berperan dalam berbagai kecelakaan. Tak ada alasan untuk berkata bahwa
penerbang lebih kebal dalam menghadapi stresor yang membahayakan. Sekalipun
terpilihnya seseorang sebagai penerbang boleh jadi tingkat emosinya stabil dan
lebih tahan uji. Menurut Kepala Lembaga Psikologi Angkatan Udara
(KALAPSIAU), untuk menjadi seorang penerbang tidaklah mudah, karena harus
mengikuti saringan yang ketat, salah satunya adalah melalui tes psikologi. Para
penerbang diharuskan memiliki emosi yang stabiI, kecerdasan di atas rata-rata,
mampu menghadapi kehidupan, sedangkan bila menghadapi stress atau kecemasan
cenderung untuk berbuat sesuatu dengan tidak berlebihan dan mampu untuk
mengintrospeksi diri. Namun demikian adalah hal yang tidak dapat dipungkiri
bahwa penerbang akan mengalami stres dan kecemasan yang tidak dapat
ditanggulangi secara tepat oleh mereka. Banyak sekali reaksi terhadap situasi
seperti itu dan dapat terlibat dalam berbagai kecelakaan. Reaksi itu mungkin
beragam, dari beralihnya perhatian, kebingungan, cemas, dan lain-lain (Komando
Pendidikan,1989).
Hasil wawancara peneliti dengan Inspektur Jenderal dan
Perbendaharaan Angkatan Udara (IRJENAU), Marsekal Muda TNI Tjutju
Djuanda, menunjukkan bahwa tingkat kecemasan yang timbul pada penerbang
yang pemah mengalami kecelakaan tergantung juga dari status penerbang yang
bersangkutan. Pada penerbangfighter (tempur) dibedakan dengan kategori sebagai
wingman (penerbang pemula/baru lulus konversi), flight leader (::I:500 - 800 jam
terbang) dan instruktur (::I: 1500 jam terbang). Apabila kecelakaan terjadi pada
wingman maka pada awal penerbangan setelah diijinkan kembali untuk terbang,
kecemasan yang timbul akan lama bahkan dapat lebih dari 10jam, sedangkan pada
flight leader kecemasan yang timbul kurang dari 10 jam, dan untuk yang sudah
menjadi instruktur kecemasan yang muncul lebih singkat lagi. Begitu juga halnya
pada penerbang transport ({"IXwing) dan helikopter (rotary wing), dengan
kategori yaitu penerbang dua (co-pilot), penerbang satu (captain pilot), dan
instruktur. untuk penerbang yang pemah mengalami kecelakaan, apabila mereka
diberi hukuman tidak terbang dalam periode waktu tertentu (grounded), hal ini
juga dapat membuat para penerbang menjadi kurang percaya diri dan merasa lebih
rendah kemampuannya dari yang lainnya.
Pada usia sekitar 24 - 28 tahun, yaitu usia yang termasuk pada kategori
wingman dan flight leader (penerbang fighter) atau co-pilot dan captain pilot
(penerbang transport dan helikopter) dikatakan oleh Reinhardt (dalam Untung
Kahar, 1987) bahwa di usia tersebut penerbang mulai menyadari batas-batas
kemampuan dirinya, demikian juga dengan pesawatnya. Penerbang mulai
menyadari kesempatan yang sempit untuk menghindar dari adanya bahaya
kecelakaan serta ditambah pengalaman kehilangan ternan seprofesi.
Berdasarkan fenomena-fenomena di atas dapat disimpulkan bahwa cara
penerbang dalam menghadapi kecemasan terhadap kecelakaan pesawat berbeda-
beda, untuk itulah penulis ingin mengetahui apakah tingkat kecemasan sesaat
terhadap kecelakaan pesawat pada penerbang TNI-AU yang belurn pernah
mengalami kecelakaan dengan penerbang yang pemah mengalami kecelakaan itu
terdapat perbedaan.
Berdasarkan latar belakang masalah yang telah diuraikan sebelumnya, maka
dapat dirumuskan identifikasi masalah sebagai berikut "Apakah ada perbedaan
tingkat kecemasan sesaat terhadap kecelakaan pesawat pada penerbang TNI-AU
yang pemah mengalami kecelakaan dengan penerbang TNI-AU yang belum pemah
mengalami kecelakaan ?"
Maksud penelitian ini adalah untuk memperoleh gambaran
mengenai tingkat kecemasan sesaat terhadap kecelakaan pesawat pada
penerbang TNI-AU yang pemah mengalami kecelakaan dengan penerbang
TNI-AU yang belum pemah mengalami kecelakaan.
Tujuan penelitian ini adalah untuk memperoleh gambaran tentang
perbedaan tingkat kecemasan sesaat terhadap kecelakaan pesawat pada
penerbang TNI-AU yang pemah mengalami kecelakaan dengan penerbang
TNI-AU yang belum pemah mengalami kecelakaan.
Hasil penelitian ini dapat memberikan infonnasi bagi pihak instansi
TNI-AU khususnya Dinas Keselamatan Terbang dan KeIja Angkatan Udara
(DISLAMBANGJAAU) dalam upaya mengintensifkan upaya-upaya
preventif dalam rangka mendukung perwujudan "Zero Accident" di TNI-
AU. Sedangkan bagi para penerbang TNI-AU guna mengetahui kondisi
kecemasan yang sebenamya, sehingga dapat mengantisipasi hal-hal yang
berkaitan dengan keselamatan penerbangan.
Hasil penelitian ini diharapkan dapat memperkaya wacana dalam
bidang psikologi penerbangan, khususnya pemahaman mengenai tingkat
kecemasan terhadap kecelakaan pesawat pada penerbang TNI-AU yang
pemah mengalami kecelakaan dengan penerbang TNI-AU yang belum
pemah mengalami kecelakaan. Selain itu hasil penelitian ini dapat menjadi
infonnasi bagi para peneliti yang akan melakukan penelitian lebih lanjut.
Menjadi seorang penerbang dituntut untuk bisa bekerja dengan baik di
wahana udara yang mempunyai risiko sangat tinggi. Banyak pennasalahan yang
dihadapi seorang penerbang dalam menjalankan tugasnya di angkasa, yaitu harns
mampu mengatasi beban aerofisiologi akibat faktor ketinggian, kecepatan,
manuverabilitas, serta daya jelajah pesawatnya. Pada saat terbang para penerbang
senantiasa berada dalam kondisi berbeda yang secara langsung maupun tidak
langsung berpotensi membahayakan jiwa penerbang. Hal ini yang dapat
menimbulkan stres bagi seorang penerbang. Stres ini muncul apabila adanya
rangsang-rangsang yang membahayakan (stresor) yang menggugah reaksi-reaksi
kecemasan. Stres juga diartikan sebagai adanya perubahan-perubahan afektif,
perubahan tingkah laku dan perubahan fisiologis yang terjadi karena adanya
rangsang yang dianggap membahayakan.
Berbicara mengenai stres yang mungkin dialami oleh sel)rang penerbang,
maka harus dilihat dari sudut lingkungan dimana seorang penerbang itu berada.
Lingkungan pada penerbang ada tiga, yaitu home environment, yang terdiri atas
hubungan penerbang dengan keluarga (isteri, anak-anak, orang tua, teman dekat),
kemudian ground environment, yang terdiri atas hubungan penerbang dengan
lingkungan pekerjaan (atasan, senior, rekan sejawat, bawahan) dan yang terakhir
adalah flight environment, yang terdiri atas angkasa (aerospace), lingkungan
dimana pesawat bergerak, serta cockpit, cabin, gun turret dari pesawat, lingkungan
dimana awak pesawat bergerak, yang juga disebut flight deck. Ada flight
environment berarti terdapat juga flight stress. Flight stress di sini terdiri atas tiga
macam, yaitu angkasa seperti risiko jatuh, kehilangan posisi, kemudian pesawat
seperti kontinuitas gerakan, high velocity, G-Forces, dan yang terakhir adalah
pengalaman penerbang dalam kedua lingkungan tersebut (Komando Pendidikan,
1989).
Jadi stress adalah stimulus obyektif yang mengandung bahaya baik fisik
maupun psikologis pada derajat keadaan tertentu. Adanya pandangan terhadap
stress maka timbul suatu perasaan terancam pada diri penerbang, dan hal ini akan
meningkatkan atau menggugah kecemasan sesaat penerbang (state anxiety).
Kecemasan menurut Spielberger (1966 : 13) merupakan suatu proses dari
beberapa kejadian yang berurutan, yaitu proses kognitif, afektif, fisiologis dan
tingkah laku. Masih menurut Spielberger, kecemasan terdiri atas dua jenis, yaitu
kecemasan sesaat (State Anxiety) dan kecemasan dasar (Trait anxiety). Kedua
kecemasan ini sangat berperan dalam tergugahnya penghayatan terhadap
kecemasan. Kecemasan sesaat adalah suatu penghayatan yang subyektif dari
adanya perasaan tegang, terancam, serta perubahan sistem syaraf otonom yang
dialami penerbang pada situasi sesaat. Contohnya takut terbang. Keadaan ini
ditentukan oleh perasaan ketegangan yang subyektif. Lamanya suatu kecemasan
sesaat sangat tergantung pada besamya stres penerbang terhadap suasana yang
dianggapnya mengancam. Sedangkan kecemasan dasar adalah ciri atau sifat
penerbang yang stabil atau menetap. Hal ini juga sebagai suatu perbedaan
individual yang relatif menetap yang mengarahkan penerbang untuk
menginterpretasikan suatu keadaan sebagai ancaman. Penerbang yang memiliki
kecemasan dasar tinggi, maka cenderung lebih kuat untuk mempersepsikan dunia,
lingkungan di sekitamya sebagai suatu keadaan yang membahayakan daripada
penerbang yang memiliki kecemasan dasar rendah, sehingga dapat diartikan
penerbang dengan kecemasan dasar tinggi lebih mudah terkena stress dan
cenderung untuk lebih tinggi dalam menghayati kecemasan sesaatnya.
Namun menurut Spielberger, adanya keadaan yang membahayakan atau
tidak pada diri penerbang ditentukan juga oleh cognitive appraisal atau penilaian
kognitif Penilaian kognitif (cognitive appraisal) sangat dipengaruhi juga oleh dua
fak'tor, yaitu commitment dan belief Apabila dihubungkan dengan profesi
penerbang, maka bisa dijelaskan bahwa kecemasan yang timbul pada penerbang
sangat ditentukan oleh penilaian kognitifnya. Seorang penerbang yang pernah
mengalami kecelakaan memiliki belief bahwa kegiatan penerbangan merupakan
situasi yang dirasakan kurang nyaman seperti terbang setelah grounded dalam
kurun waktu tertentu, maka apabila kejadian sebelumnya sangat berkeslln dan
bahkan dianggap sebagai suatu pukulan, pada saat pertama kali terbang kembali
(setelah grounded) akan muncul perasaan khawatir, takut, dan cemas yang
berlebihan. Sehingga kecemasan sesaat yang dirasakan akan tinggi. Hal ini dapat
mengganggu konsentrasi penerbang selama bertugas, dan mungkin juga akan
muncul tingkah laku seperti pura-pura sakit, ingin buang air kecil terus-terusan,
ingin muntah. Sedangkan bagi penerbang yang belum pernah mengalami
kecelakaan belief yang dirasakan terhadap kegiatan penerbangan adalah suatu tugas
yang rutin dan wajib untuk dilaksanakan. Pada penerbang ini, flight stress yang
dirasakan cukup mengancam, hanya karena belurn adanya penghayatan terhadap
kecelakaan pesawat maka kecemasan sesaat yang dirasakan relatif rendah.
Dari uraian di atas dapat tergambar bahwa perasaan cemas yang dirasakan
oleh penerbang tidak terlepas dari penilaian kognitif (cognitive appraisal) dimana
di dalamnya mencakup belief dan commitment serta stresor yang dianggap
membahayakan.
A.
Skema kerangka pikir dapat digambarkan sebagai berikut :
-HOME STRESS
-GROUND STRESS
-FLIGHT STRESS
-BELIEF
A.
Berdasarkan kerangka pemikiran yang telah diuraikan diatas, maka diajukan
asumsi-asumsi sebagai berikut :
. Kecemasan yang timbul dalam diri seorang penerbang sangat dipengaruhi
oleh stressor yaitu pengalaman kecelakaan pesawat yang dianggap membahayakan
dan cognitive appraisal dari pengalaman tersebut, dimana didalamnya tercakup
belief; dimana belief pada penerbang yang pernah mengalami kecelakaan adalah
negatif dn beliefbagi penerbang yang belum pemah mengalami kecelakaan adalah
positif dan commitment dari masing-masing penerbang.
. Penerbang yang pemah mengalami kecelakaan memiliki state anxiety
(kecemasan sesaat) pada kategori tinggi, sedangkan penerbang yang belum pemah
mengalami kecelakaan memiliki state anxiety (kecemasan sesaat pada kategori
rendah.
Berdasarkan kerangka pikir dan asumsi yang dibuat di atas, maka
peneliti mengajukan hipotesis penelitian, yaitu :
. Terdapat perbedaan tingkat kecemasan sesaat terhadap kecelakaan pesawat
pada penerbang TNI-AU yang pernah mengalami kecelakaan dengan
penerbang TNT-AUyang belum pemah mengalami kecelakaan.
 Pada era globalisasi saat ini, selaras dengan kemajuan teknologi, manusia 
 semakin dihadapkan pada berbagai pennasalahan yang rumit. Kecenderungan 
 yang dihadapi manusia di dunia bersumber dari kebutuhan manusia akan 
 kesejahteraan dan keamanan, baik individu, kelompok, bangsa, hingga negara. 
 Manusia jaman sekarang dituntut untuk selalu mengembangkan dirinya baik dalam 
 ilmu pengetahuan maupun teknologi, sebab hal ini dapat membantu individu untuk 
 menghadapi mas a depan. 
 Salah satu kemajuan teknologi adalah diciptakannya pesawat udara yang 
 pada awalnya sangat sederhana. Sampai dengan empat dekade terakhir ini, telah 
 hadir pesawat-pesawat canggih yang merupakan perkembangan dunia penerbangan 
 yang dapat digunakan sebagai sarana transportasi ke luar negeri. Selain sebagai 
 sarana transportasi, pesawat udara juga digunakan sebagai alat untuk 
 mempertahankan dan menjaga kedaulatan negara di udara terhadap ancaman yang 
 berasal dari negara lain. Namun, kondisi ini tidak diimbangi dengan kemajuan fisik 
 manusia. Toleransi tubuh manusia sejak dulu hingga sekarang belum menunjukkan 
 perubahan yang berarti sehingga menimbulkan kesenjangan antara teknologi 
 pesawat terbang dan kemampuan manusia. 
 TNI-AU selaku penegak kedaulatan negara di udara, mempunyai tugas 
 menegakkan hukum di udara. Kesiapan dan kemampuan tempur TNI Angkatan 
 Udara sangat dipengaruhi oleh kualitas alat utama sistem senjata (alut sista) serta 
 kualitas sumber daya manusia yang mengawakinya. Kualitas alat utama sistem 
 senjata tergantung perkembangan teknologi yang telah diaplikasikan dalam wujud 
 hasil produk dari negara pembuat mesin perang khususnya alat utama sistem 
 senjata udara. Semakin antisipatif negara produsen terhadap tantangan yang 
 berkembang, maka aplikasinya dalam menciptakan alat utama sistem senjata akan 
 semakin berbobot teknologi yang paling mutakhir serta dapat diaplikasikan di 
 lapangan. Semakin mutakhimya alut sista menuntut sumber daya manusia yang 
 profesional di bidangnya, sehingga personel yang mengawaki alat utama yang 
 sangat sarat teknologi dapat secara profesional mengawaki alat utama tersebut. 
 Peran penerbang militer berbeda dengan penerbang sipil, penerbang militer 
 memiliki beban dan tanggung jawab yang lebih besar, karena mereka dituntut 
 untuk selalu siap selama 24 jam terutama pada tugas yang berbahaya seperti 
 perang, sehingga secara fisik maupun psikis harus selalu siap menghadapi berbagai 
 kondisi yang berat dan bahaya. Sebagai personil TNI, seorang penerbang militer 
 tentunya dituntut untuk semaksimal mungkin mempertahankan eksistensi 
 kedaulatan dan kemerdekaan negara. Upaya untuk memperoleh penerbang yang 
 tangguh dan memiliki kesiapsiagaan yang tinggi dilakukan melalui proses 
 pendidikan dan latihan yang teratur, mantap dan penuh disiplin. 
 Meskipun demikian, faktor manusia dengan segala keterbatasannya sebagai 
 makhluk bumi tetap merupakan titik lemah dalam sistem keamanan terbang. 
 lumlah kecelakaan penerbangan akhir-akhir ini yang terjadi di luar negeri dan 
 khususnya yang terjadi di Indonesia baik di lingkungan TNI atau TNI-AU maupun 
 non TNI tampak semakin meningkat. Setiap kecelakaan pesawat terbang selalu 
 membawa kerugian bagi negara, baik kerugian materiil dan terutama kerugianjiwa 
 manusia yang terlatih dan tidak dapat dinilai harganya. Berdasarkan laporan Civil 
 Aviation Authority sejak tahun enam puluhan sampai tahun delapan puluhan, 60% 
 kecelakaan terbang disebabkan pilot error (human factor) dan hanya 40% 
 disebabkan faktor lain di luar human factor. Kemudian suatu penelitian dari USAF 
 (United State Air Force) selama dekade 1960 - 1970 menyimpulkan bahwa 
 accident rate cenderung menurun tetapi persentase pilot error tetap tinggi. Nada 
 yang sarna juga dikatakan oleh Angkatan Udara Kerajaan Inggeris-Royal Air 
 Force (RAF) 1971, bahwa pada kecelakaan terbang sekitar 87% disebabkan pilot 
 error. 
 Kecelakaan dalam penerbangan yang diakibatkan oleh kelalaian manusia 
 tetap tidak dapat dihindarkan, kira-kira 75% dari kecelakaan pesawat terbang 
 diakibatkan oleh kesalahan manusia, baik penerbang, teknisi, maupun A TC 
 (pengatur lalu-lintas udara) dalam monitoring, managing, dan mengoperasikan 
 sistem pesawat (angka tersebut diambil secara keseluruhan dari kecelakaan pesawat 
 sipil maupun militer, Sheril L. Chappel, dalam Jensen, 1989). Hal ini dapat 
 dikaitkan dengan penjelasan Kepala Dinas Keselamatan Terbang dan Kerja 
 Angkatan Udara (KADISLAMBANGJAAU), Marsekal Pertama TNI 
 Suyanto, yang menyatakan bahwa dalam periode 10 tahun (1993 sid 2002) di 
 lingkungan TNI-AU, 17,14 % kecelakaan dalam penerbangan yang diakibatkan 
 oleh kesalahan penerbang, 74,46% akibat faktor mesin pesawat dan 8,24% akibat 
 faktor media (landasan, cuaca dan sebagainya.). Faktor manusia di sini, dibatasi 
 pada lingkup kesehatan jiwa, yaitu berkaitan dengan masalah proses pikir, emosi 
 dan perilaku manusia, yang dapat menjadi latar belakang suatu tindakan yang 
 merugikan keamanan terbang dengan akibat timbulnya kecelakaan. 
 Keberhasilan maupun kecelakaan terbang bergantung pada interaksi antara 
 tiga faktor utama yang juga dikenal dengan 3M, yaitu Mesin (material), Media 
 (cuaca dan fasilitas penerbangan), Manusia (fisik, mental dan sosial). Bila media 
 dan mesin pesawastnya dalam keadaan baik, maka faktor manusia, dalam hal ini 
 penerbang, sangat menentukan keberhasilan maupun kegagalan penerbangan. 
 Untuk mampu mengendalikan pesawat dengan baik, seorang penerbang harus 
 memiliki penalaran yang baik, keterampilan yang memadai serta pertimbangan 
 (judgement) yang tepat. Kesemuanya itu merupakan hasil akhir dari suatu latihan 
 dan proses belajar yang terus menerus dan tak kenaI lelah. Setiap kesalahan yang 
 kecil sekalipun dapat mengakibatkan timbulnya kecelakaan dengan akibat cacat 
 (mutilasi) atau bahkan kematian. 
 Ancaman mutilasi bahkan kematian merupakan stresor yang secara 
 potensial dapat menimbulkan kecemasan yang bila tidak ditanggulangi dengan 
 benar dapat menimbulkan berbagai tingkah laku yang muncul dalam perilaku 
 sehari-hari penerbang tersebut, misalnya menghindari kegiatan penerbangan. 
 Bahwa penerbangan militer itu berbahaya memang adalah kenyataan. Bahaya itu 
 secara sadar dialami sebagai rasa cemas. Pada penerbang rasa cemas tersebut 
 timbul karena risiko yang akan dihadapi pada saat terbang, bahwa "one can be 
 killed while flying" apakah itu karena kondisi pesawat yang kurang baik atau pada 
 diri penerbangnya, misalnya pada saat itll mereka merasa belum siap untuk terbang, 
 takut apabila teIjadi kecelakaan tidak dapat mengambil keputusan untuk berbuat 
 sesuatu, atau apabila menghadapi musuh yang peralatannya lebih canggih, timbul 
 perasaan takut mati tertembak. 

