o Properties kognisi dari suatu subsistem dipengaruhi oleh sistem 
keseluruhan dimana kognisi itu merupakan bagiannya. Kerangka 
kognisi akan dibentuk berdasarkan olahan dari semua masukan 
yang pernah diterima dan masih melekat pada kognisi individu 
sehingga dapat berperan sebagai acuan dalam persepsinya. 
Kerangka ini disebut sebagai kerangka acuan (frame of reference). 
Kerangka acuan ini diperoleh dari olahan pengalaman, karenanya 
selain tergantung dari masukan juga tergantung dari olahan 
terhadap masukan tersebut.  
o Perubahan kognitif secara tipikal digerakkan oleh perubahan 
informasi dan keinginan-keinginan individu. Informasi baru dapat 
mengubah cara berpikir individu. Meskipun informasi yang sama 
dapat menyebabkan perubahan yang berbeda pada individu dengan 
kognisi yang serupa. Selain itu, informasi baru tidak selalu 
menyebabkan perubahan kognitif. Perubahan kognitif lebih 
disebabkan oleh adanya perubahan keinginan individu daripada 
masuknya suatu informasi baru.  
o Perubahan kognitif untuk sebagian ditentukan oleh karakteristik 
sistem kognitif yang telah dimiliki. Derajat dan cara bagaimana 
perubahan keinginan dan informasi menghasilkan perubahan 
dalam kognisi tergantung pada karakteristik sistem kognitif yang 
dimiliki (multipleksitas, konsonansi dan kesalingberjalinan).  
o Perubahan kognitif untuk sebagian ditentukan oleh faktor 
kepribadian. Perubahan kognitif tidak hanya merupakan fungsi dari 
hakikat sistem kognitif yang dimiliki melainkan juga merupakan 
fungsi dari karakteristik pribadi individu, yaitu : kemampuan 
intelektual, derajat toleransi terhadap ketaksaan (ambiguitas) dan 
disonansi kognitif, keterbukaan jiwa (open mindedness) atau 
ketertutupan jiwa (closed mindedness), dan cara tipikal dalam 
menghadapi rintangan pemuasan keinginan.  
Kepribadian menurut A.A Roback adalah integrasi dari aspek 
kognitif, afektif, konatif, dan kecenderungan fisik masing-masing 
individu. Kepribadian juga bisa diartikan sebagai pola pikir, emosi, dan 
perilaku yang berbeda dan berkaitan dengan karakteristik yang akan 
menentukan gaya personal individu dan mempengaruhi gaya interaksinya 
dengan lingkungan ( Atkinson, Atkinson, Smith & Bern ).  
Kepribadian menurut Wolman adalah sifat-sifat yang menjadi karakter 
psikologikal dari seorang individu. Dalam hal ini mencakup perbedaan 
tiap individu dalam menerima situasi yang sama dan bereaksi secara 
konsisten dalam menghadapi perubahan-perubahan yang meliputi 
perubahan kondisi stimulus, nilai, kemampuan, motivasi, temperamen, 
identitas dan gaya pribadi.  
Maka, secara garis besar dapat disimpulkan bahwa kepribadian 
adalah integrasi yang bersifat individual, menetap, dan konsisten meliputi 
aspek kognitif, afektif dan konatif. Kepribadian ini akan berpengaruh pada 
cara berpikir individu, merasa, bertingkah laku dan menentukan gaya 
personal dan interaksi individu dengan lingkungannya melalui karakter 
psikologiknya yang unik.  
Kepribadian terbentuk oleh 2 aspek, yaitu : genetik dan pengaruh 
lingkungan. Ada korelasi yang erat diantara keduanya 
1. Aspek genetik 
Secara genetik, kepribadian terbentuk dari elemen-elemen yang 
diwariskan dari kedua orangtua individu. Dalam hal ini akan berkaitan 
dengan penentuan warna kulit, rambut, ukuran tubuh, jenis kelamin, 
kemampuan intelektual, dan temperamen. 
2. Aspek pengaruh dari lingkungan. 
Pengaruh dari lingkungan berkaitan dengan kebudayaan, kelompok sosial, 
dan lingkungan keluarga. Pengaruh ini didapatkan individu dalam 
interaksinya dengan lingkungan selama pertumbuhan yang juga akan 
menentukan perkembangan individu selanjutnya.  
Selain berinteraksi dengan lingkungan, karakteristik bawaan/genetik 
individu juga membentuk lingkungannya sendiri. Menurut Plomin, 
DeFries dan Loehlin (1977) juga Scarr dan McCartney (1983) bentuk 
interaksi tersebut terdiri atas : 
Adalah interaksi dimana individu bereaksi terhadap lingkungan sesuai 
dengan kepribadian dirinya. Individu berbeda yang mengalama 
lingkungan akan mengalami hal yang sama, menginterpretasikannya dan 
bereaksi terhadapnya secara berlainan. Dengan kata lain, tiap kepribadian 
individu akan menggali lingkungan psikologis subjektif dari sekelilingnya 
yang objektif. Lingkungan yang dipandang secara subjektif itulah yang 
membentuk perkembangan kepribadian ke tahapan selanjutnya. Interaksi 
reaktif ini akan terjadi sepanjang rentang hidup individu. 
Adalah interaksi yang terjadi dimana llingkungan akan memberikan 
respon sesuai dengan kepribadian yang ditampilkan individu. Dengan kata 
lain, setiap kepribadian individu akan menimbulkan respon yang 
berlainan. Interaksi evokatif juga terjadi sepanjang rentang hidup individu. 
Misalnya, individu ramah akan membangkitkan situasi lingkungan yang 
ramah. Sebaliknya, individu yang menimbulkan situasi permusuhan akan 
membangkitkan situasi lingkungan yang bermusuhan pula.  
Adalah interaksi yang terjadi dalam lingkungan yang dipilih individu 
sesuai dengan kepribadiannya. Ketika individu tumbuh dan berkembang 
seiring pertambahan usia, individu dapat bergerak diluar lingkungan yang 
ditetapkan oleh orangtua dan memilih serta membentuk lingkungannya 
sendiri. Selanjutnya, lingkungan tersebut akan berperan membentuk 
kepribadian individu. Interaksi proaktif merupakan proses dimana individu 
menjadi pelaku aktif dalam perkembangan kepribadian individu secara 
aktif. 
Sejumlah studi mendukung pandangan bahwa struktur dasar 
kepribadian ditentukan pada masa kanak-kanak awal (Freud, 1959 ; 
Kagan & Moss, 1962 ; Mischel, 1969 ; Scott, 1962 ). Hal tersebut 
dikarenakan adanya hubungan timbal balik antara karakteristik bawaan 
individu secara genetik dengan lingkungan yang berperan besar pada diri 
individu pada masa kanak-kanak dimana lingkungan tersebut ditetapkan 
oleh orangtua.Tetapi dengan mempertimbangkan perubahan pada masa 
pubertas, remaja, dewasa awal, terbuka kemungkinan bahwa kepribadian 
tidak hanya ditentukan pada masa kanak-kanak saja. Perkembangan dan 
perubahan kepribadian akan terus berlanjut sepanjang rentang hidup 
individu. Dengan kata lain, korelasi awal antara karakteristik bawaan dan 
pengaruh lingkungan akan menunjukkan penurunan arti sementara 
pengaruh interaksi proaktif mengalami peningkatan arti (Atkinson, 
Atkinson, Smith & Bem  edisi 11, jilid 2, halaman 224-227). 
Dapat dikatakan bahwa kepribadian sesungguhnya bukanlah 
sebagai susunan statis dari komposisi karakteristik. Ataupun sebagai 
proses diferensiasi sepanjang kehidupan yang dipengaruhi oleh faktor 
bawaan dan pengalaman masa lampau yang kemudian secara terus 
menerus mempengaruhi perkembangan kepribadian individu selanjutnya. 
Kepribadian memiliki kapasitas dan kualitasnya secara individual. 
Kapasitas kepribadian adalah mencakup faktor inteligensi, sedangkan 
kualitas dari kepribadian itu sendiri akan lebih berkaitan dengan karakter 
individu. (A.A.Roback). 
Kepribadian menyatakan manusia secara umum yang mencakup 
kapasitas dan kualitas kepribadian. Sementara karakter dalam arti luas 
dapat dipahami sebagai kualitas kepribadian yang menjadikan individu 
unik. ( A.A.Roback, halaman 6 ). Karakter menurut A.A.Roback adalah 
bagian kepribadian yang memiliki aspek kognitif, afektif, dan kognitif. 
Karakter juga didefinisikan sebagai disposisi psikofisikal yang relatif 
menetap, menghambat pengekspresian dorongan dan mengalami 
penyesuaian dengan aturan-aturan umum yang ada di lingkungan. 
Menurut Benjamin B.Wolman, karakter merupakan aspek yang 
menetap dan konsisten dari kepribadian individu.  Karakter adalah pola 
pikir, perasaan, dan tingkah laku yang menetap yang menjadi karakteristik 
kepribadian dan membedakan individu satu dengan lainnya (Marlon 
Perlmutter & Elizabeth Hall, 1985 : 4). Morton Prince berpendapat 
bahwa karakter adalah wujud, atau manisfestasi dari kepribadian. 
Berdasarkan definisi diatas, karakter dapat disimpulkan sebagai wujud 
atau manisfestasi dari kualitas kepribadian yang memiliki disposisi aspek 
kognitif, afektif, dan konatif yang menetap. Karakter juga merupakan 
karakteristik dari kepribadian yang disesuaikan dengan lingkungan melalui 
aturan-aturan umum yang berlaku.  
Karakter dikatakan sebagai sesuatu yang menentap, ternyata 
karakter juga dapat berubah atau mengalami modifikasi sesuai dengan 
situasi dan kondisi yang dihadapi individu. Mengenai modifikasi karakter 
ini, A.A Roback menuliskan dua pandangan yang berbeda. Pandangan 
yang pertama mengacu pada para transedentalis yang percaya bahwa inti 
dari karakter tetap tidak dapat diubah dalam berbagai situasi. Sedangkan 
pandangan lainnya yang mengacu pada kaum enviromentalis berpendapat 
bahwa karakter dapat diubah melalui perubahan-perubahan dalam hidup, 
meskipun perubahan tersebut dikaitkan dengan banyak peristiwa yang 
tidak menyenangkan. 
Dalam buku-buku Jerman dituliskan mengenai kompromi 
mengenai dua pandangan diatas. Kemudian, dimunculkan istilah karakter 
endogen dan karakter shicksal. Dimana karakter shicksal dipengaruhi 
berbagai situasi dan menghasilkan reaksi yang berbeda dari reaksi karakter 
endogen. Berdasarkan hal tersebut, dapat ditarik suatu asumsi bahwa 
didalam diri tiap individu ada bagian yang dapat mengalami perubahan, 
namun ada juga yang tidak dapat mengalami perubahan dalam proporsi 
yang berbeda-beda.  
Berdasarkan pandangan-pandangan diatas, dapat dikatakan 
karakter bawaan sebagai potensi kepribadian yang dipengaruhi oleh faktor 
genetik dan juga mengalami modifikasi hasil interaksi dengan lingkungan. 
Faktor lingkungan memiliki peran penting dalam  pembentukan dan 
pengembangan karakter, termasuk didalamnya lingkungan sosial seperti : 
guru, pemimpin agama, pendidikan, status sosial, dan lingkungan tempat 
tinggal individu dibesarkan.  
Berdasarkan pengaruh genetik dan pengaruh lingkungan pada 
karakteristik bawaan, individu mendapatkan suatu karakteristik tambahan 
yang disebut sebagai karakter yang didapatkan (acquired characteristic). 
Benjamin Wolman mendefinisikan karakter yang didapatkan ini 
(acquired characteristic) merupakan perubahan struktur organisme 
sebagai akibat dari pengaruh lingkungan atau dikarenakan aktivitas 
organisme itu sendiri.  
Menurut Bulliot karakter mempunyai dua dimensi, yaitu : 
Dimensi ini ditandai dengan adanya satu atau lebih fungsi kepribadian 
yang mendominasi. 
Dimensi ini berkaitan dengan pembentukan karakter oleh beberapa faktor 
individual. Termasuk didalamnya temperamen, struktur tulang, dll. 
Dimana faktor tersebut akan berpengaruh pada fungsi kepribadian yang 
dominan.  
Ilmu astrologi merupakan salah satu seni kuno dari negeri Cina. 
Astrologi merupakan suatu kajian yang orisinal berkaitan dengan hal di 
luar diri manusia dan merupakan hal terpenting sebagai bagian lima aliran 
seni dari takdir. Selama ribuan tahun seni kembar astrologi dan astronomi 
adalah hal yang sama, tidak ada hal yang dapat membedakan keduanya. 
Seorang astronom mengamati bintang dalam usahanya untuk melihat apa 
yang penguasa langit rencanakan untuk bumi dan tentu saja untuk manusia 
sebagai bagiannya. Berawal dari hari tersebut, pada masa sesudahnya 
hingga saat ini astronom dan ahli astrologi menjadi bagian dari istana 
kekaisaran.  
Dalam catatan terpenting dunia tersebut, tercantum suatu catatan 
yang tidak ada cacat untuk kurun waktu lebih dari tiga ribu tahun. Hingga 
saat ini, astrologi cina menjadi bagian besar dari kehidupan penduduk 
Cina dari hari ke hari. Setiap harinya mereka menggunakan alamanak Cina 
yang disebut T  ung Shu. T  ung Shu mencatat data astrologi dan 
astronomi setiap hari dari tiap tahun Cina. Berisikan dan berdasar pada 
data mengenai hari yang menguntungkan dan hari yang kurang baik 
peruntungannya. Sangat sedikit, orang Cina memulai suatu bisnis atau 
perjalanan dalam hidupnya ketika T  ung Shu memberikan arahan bahwa 
hari tersebut kurang baik untuk melakukan suatu kegiatan. 
Berkaitan dengan alamanak, atau kalender perhitungan sebagai 
bagian dari alamanak sebaiknya dimulai dengan ilmu astronomi dan 
astrologi Cina. Berdasarkan buku tertua Cina yang berjudul  Shu Ching  
atau  Book of Historical Documents , akan dapat ditemukan Cerita 
tentang Kaisar Yao dan saudara laki-lakinya Hsis dan Ho. Cerita ini 
dibuat pada tahun 2256 SM. Pada masa itu Kaisar Yao memiliki 
keinginan untuk membuat kalender tahunan sehingga warganya dapat 
mengetahui waktu kapan suatu musim berlangsung, kapan waktu yang 
tepat untuk menanam dan kapan waktu untuk menuai. Oleh karena itu 
Kaisar Yao memerintahkan kedua saudara laki-lakinya untuk mengamati 
jagat raya yang luas, memperhitungkan, serta menggambarkan pergerakan 
matahari, bulan dan bintang. Lalu, diharapkan berdasar hasil 
pengamatannya dapat dibuat sebuah kalendar. (Shu Ching,  The Canon of 
Yao , bagian 1, buku kedua). Berdasarkan hal tersebut sejarah astronomi 
dan astrologi dimulai ribuan tahun yang lalu dan seperti sudah menjadi 
suatu kebiasaan ilmu astronomi dipergunakan sejak waktu yang lalu. 
Ilmu astrologi yang diterapkan secara individual pertama baru 
ditemukan setelah masa agama kristen mulai menyebar yaitu pada tahun 
100 M. Setelah masa tersebut, sejumlah catatan astrologi individual baru 
berkembang hingga catatan ensiklopedia disusun secara berseni pada masa 
dinasti Tang (618   907 M). Sejak masa tersebut, astrologi menjadi bagian 
dari hidup warga Cina. Meskipun begitu pernyataan dan peringatan 
terhadap nasib dari negeri Cina masih dapat ditemukan pada alamanak 
masa kini.  
Horoskop Cina menggunakan bulan sebagai benda angkasa yang 
penting dalam menentukan karakter individu. Bulan, memiliki empat fase 
pergerakan yang bertepatan juga dengan perubahan empat musim. 
Keempat fase tersebut terbagi atas fase bulan baru (new moon) bertepatan 
dengan berlangsungnya musim semi, seperempat awal bulan (first quarter) 
bertepatan dengan musim panas, bulan utuh/penuh (full moon) bertepatan 
dengan musim gugur, dan seperempat bulan akhir (last quarter) bertepatan 
dengan musim dingin. Masing-masing fase menurut horoskop Cina 
memiliki pengaruh terhadap keunikan karakter individu yang dilahirkan 
pada fase tersebut. Berikut penjabaran untuk setiap fasenya : 
Pada fase pergerakan bulan yang pertama (new moon) bertepatan 
dengan berlangsungnya musim semi. Individu yang dilahirkan bertepatan 
dengan berlangsungnya musim semi, berdasarkan pengamatan memiliki 
sifat-sifat yang menggambarkan musim semi. Misalnya memiliki 
pemikiran yang inovatif, menyukai kebebasan, serta kreatif. 
Fase pergerakan bulan berikutnya (first quarter), bertepatan 
dengan berlangsungnya musim panas. Berdasarkan pengamatan, individu 
yang dilahirkan bertepatan dengan berlangsungnya musim panas ini, 
memiliki sifat-sifat yang menggambarkan musim panas. Misalnya: 
memiliki pemikiran yang jernih, seorang pejuang sejati, energik dan 
menguasai banyak kemampuan.  
Fase bulan utuh (full moon) melambangkan keteraturan, kesuburan, 
dan penyimpanan. Individu yang dilahirkan pada tahun kelahiran 
bertepatan dengan berlangsungnya musim gugur memiliki sifat yang 
berkaitan dengan sifat musim gugur. Misalnya: menyukai keteraturan, 
memiliki keinginan untuk memecahkan masalah secepatnya, dan 
memperbaiki kesalahan secara efektif. 
Fase yang terakhir (last quarter) bertepatan dengan 
berlangsungnya musim dingin. Individu yang dilahirkan bertepatan 
dengan berlangsungnya musim ini, berdasarkan pengamatan memiliki sifat 
berkaitan dengan sifat musim dingin. Misalnya : pemikir, membuat 
perhitungan secara rinci dan kurang percaya diri.  
Berdasarkan pengamatan bahwa terdapat pengaruh kosmis bulan 
terhadap karakter dan sifat manusia tersebut, karakter manusia yang unik 
dan berlainan dikelompokkan secara umum. Karakter tersebut dapat 
diketahui secara menyeluruh dengan mengetahui komponen horoskop 
Cina lainnya, seperti : siklus enam puluh tahun, sepuluh tangkai langit, 
dan dua belas cabang bumi.   

