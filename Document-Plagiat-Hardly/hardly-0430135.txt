Lalu pada dimensi competence, 5 orang responden (50%) mengungkapkan 
bahwa Kawasaki Ninja 250R sukses meraih pasar dalam hal kemampuan untuk 
menunjukan keberadaannya di pasar dan pandai dalam memenuhi keinginan 
konsumen dan juga mereka yakin Kawasaki Ninja 250R mampu mempertahankan 
kesuksesannya tersebut. Sedangkan 5 responden (50%) lainnya menyatakan 
bahwa Kawasaki Ninja 250R akan tergerus oleh para pesaingnya, hal tersebut 
dikarenakan harga yang kurang mampu bersaing dengan para pesaingnya. 
Pada dimensi sophisticating, 7 responden (70%) mengatakan bahwa 
Kawasaki Ninja 250R merupakan sepeda motor kelas atas, dan memiliki 
penampilan yang sangat menarik yang mampu membuat mereka merasa menjadi 
seorang pribadi yang menarik ketika menggunakannya. Sedangkan 3 responden 
(30%) lain mengungkapkan bahwa walaupun memiliki penampilan menarik 
namun sebenarnya Kawasaki Ninja 250R ialah sepeda motor untuk pemula, dan 
mereka mengungkapkan bahwa masih banyak sepeda motor yang berada jauh 
diatas kelas Kawasaki Ninja 250R. 
Pada dimensi rugedness, 5 responden (50%) mengungkapkan bahwa 
Kawasaki Ninja 250R merupakan sosok yang maskulin, kokoh, dan  macho  yang 
mampu mewakili diri mereka, hal tersebut diwakili oleh bentuk body yang besar 
dan berotot, deru mesin khas motor besar, dan tenaga yang besar. Namun tidak 
demikian 5 responden (50%) lainnya yang menyatakan bahwa Kawasaki Ninja 
hanya memiliki body yang besar dan berotot tetapi memiliki tenaga mesin yang 
kecil. Dari gambaran diatas, maka peneliti tertarik untuk meneliti mengenai Brand 
Personality Kawasaki Ninja 250R pada anggota klub sepeda motor  X  di 
Bandung. 
Bagaimana Brand Personality Kawasaki Ninja 250R pada anggota klub sepeda 
motor  X  di Bandung. 
Tujuan dari penelitian ini adalah untuk memperoleh gambaran secara mendalam 
mengenai Brand Personality Kawasaki Ninja 250R pada anggota klub sepeda 
motor  X  di Bandung. 
Kegunaan teoritis dalam penelitian ini adalah: 
1. Memperkaya sudut pandang psikologi konsumen dalam hal temuan empirik 
mengenai Brand Personality. 
2. Memberi masukan bagi peneliti lain yang ingin mengetahui lebih lanjut 
mengenai Brand Personality Kawasaki Ninja 250R sebagai dasar untuk 
3. Menambah wawasan pengetahuan penulis mengenai Brand Personality. 
Kegunaan praktis dalam penelitian ini adalah: 
1. Sebagai bahan masukan dan dan informasi bagi para pembaca mengenai 
brand personality Kawasaki Ninja 250R pada anggota klub sepeda motor  X  
di Bandung. 
2. Sebagai masukan pada para produsen khususnya dibidang pemasaran sepeda 
motor untuk  menjaga dan mengembangkan Brand Personality produknya 
guna mempererat hubungan dengan konsumen dan dapat menciptakan brand 
personality yang sesuai dengan konsumen. 
PT Kawasaki Motor Indonesia (PT KMI) terkenal dengan produk sepeda 
motor yang memiliki kecepatan tinggi dan terkenal di arena balap sepeda motor 
sejak era tahun 1980, khususnya pada kategori motor sport. Namun dalam 
memasarkan produknya, PT KMI tidak bisa hanya memproduksi sebuah motor 
sport saja tanpa memberi identitas pada produk tersebut, karena produk hanya 
sebatas apa yang dihasilkan dari proses produksi. Produk hanya merupakan atribut 
fisik yang tidak lebih hanya sebagai komoditi yang dapat diperjualbelikan, oleh 
karena itu produsen harus melakukan hal yang penting yaitu penciptaan suatu 
 brand atau merek  pada produknya, merek dapat menjelaskan emosi dan 
hubungan yang khusus dengan pelanggannya (Aaker & Joachimstahler, 2000: 51) 
 Hubungan emosional yang erat antara merek (Kawasaki Ninja 250R) 
dengan konsumen tercermin dari banyak didirikannya klub sepeda motor 
Kawasaki Ninja, salah satunya ialah Klub sepeda motor  X . Hubungan 
emosional yang erat tersebut tercipta karena anggota klub sepeda motor  X  
mengasosiasikan Kawasaki Ninja 250R dengan sebuah karakteristik kepribadian 
manusia (brand personality) yang sesuai dengan kepribadian dirinya. Hal tersebut 
menunjukan bahwa Kawasaki Ninja 250R mampu mencerminkan kepribadian 
anggota klub sepeda motor  X . Konsumen cenderung memilih produk atau 
merek sebagai cerminan dari kepribadian mereka (Schiffman & Kanuk, 2004: 
123).  
 Brand personality adalah suatu gabungan dari sifat manusia yang dapat 
diterapkan pada suatu merek (Kotler & Amstrong, 2006 :140). Jennifer Aaker  
(Journal Brand Management, 2003:148) mengembangkan suatu kerangka untuk 
menggambarkan dan mengukur  personality/ kepribadian  brand dalam lima 
dimensi inti. Untuk melihat asosiasi kepribadian Kawasaki Ninja 250R  yang 
melekat di benak anggota klub sepeda motor  X , dan juga untuk mengukur 
sejauh mana keterkaitan brand personality Kawasaki Ninja 250R dengan 
kepribadian anggota klub sepeda motor  X  maka digunakanlah dimensi brand 
personality (Aaker, 1996: 352). Brand Personality dikelompokan ke dalam lima 
dimensi yaitu sincerity (ketulusan), excitement (semangat), competence 
(kemampuan), sophistication (kecangihan), dan ruggedness (kasar). Anggota klub 
sepeda motor  X  akan mengasosiasikan Kawasaki Ninja 250 R dengan salah satu 
dimensi brand personality tersebut, namun sebenarnya brand personality tersebut 
terdiri dari gabungan lima dimensi namun dengan proporsi yang berbeda. 
 Sincerity (tulus), adalah karakter yang jujur, rendah hati, dan sederhana. 
Sincerity tertuang dalam kejujuran dalam kualitas, keaslian produk, dan 
keidentikan merek dengan karakter yang sederhana, ceria dan berjiwa muda. 
Dimensi sincerity ini memiliki sifat down to earth (membumi/ merakyat), small 
town (kuno, picik, berpikiran sempit, tidak berminat pada ide baru atau apa yang 
terjadi diluar lingkungannya), honest (jujur), cheerful (riang, gembira). Pada 
dimensi ini pihak Kawasaki pun dengan sungguh- sungguh menciptakan produk 
yang dapat memenuhi keinginan konsumen dalam hal kualitas dan mampu 
menunjukan jati diri pemakainya dengan apa adanya. Namun semua hal tersebut 
hanya berlaku pada para penggemar motorsport, Kawasaki Ninja 250R memiliki 
pangsa pasar yang sempit. 
Excitement (gembira) berarti karakter unik yang penuh semangat dan 
imajinasi yang tinggi dalam melakukan perbedaan dan inovasi. Dimensi ini 
memiliki sifat up to date (mengikuti perkembangan jaman), daring (berani), 
spirited (enerjik, penuh semangat),. Kawasaki Ninja 250R menawarkan sensasi 
mengendara berbeda yang tidak dapat diberikan oleh pesaingnya, yaitu mesin 2 
silindernya. Sepeda motor 2 silinder memiliki suara mesinnya yang berat dan 
bergetar sehingga terdengar seperti sepeda motor kompetisi, tentu saja hal tersebut 
membuatnya menyenangkan untuk dipacu. Tidak hanya suaranya yang besar, 
namun juga tenaga yang dihasilkan cukup besar dan merata, hal yang sangat 
disukai oleh konsumen berjiwa muda. Meskipun semua hal tersebut harus ditebus 
dengan biaya yang relatif mahal dibandingkan sepeda motor pada umumnya.  
 Competence (cakap), yaitu keamanan, kemudahan, kemampuan untuk 
dapat diandalkan dan dipercaya oleh pelanggan. Dimensi ini memiliki sifat 
reliable (dapat dihandalkan), intelegent (cerdas), succesful (sukses, berhasil). 
Pihak Kawasaki dengan cerdas mampu mendesain Kawasaki Ninja 250R yang 
terbukti sukses meraih penjualan yang tinggi. Dibalik kesuksesannya, Kawasaki 
Ninja 250R memiliki resale value (harga jual kembali) yang anjlok, akan tetapi 
hal tersebut masih terbilang wajar karena Kawasaki Ninja 250R dianggap sebagai 
sepeda motor hobi. 
Sophistication (canggih), yaitu karakteristik yang berkaitan dengan 
eksklusifitas yang dibentuk oleh keunggulan prestise, citra merek, maupun tingkat 
daya tarik yang ditawarkan pada pelanggan. Dimensi ini memiliki sifat, upper 
class (kelas atas), charming (memikat, menarik).  Dengan harga jual sebesar 46,5 
juta rupiah terbilang mahal dibandingkan dengan sepeda motor sport lainnya yang 
memiliki rentang harga antara 17   30 juta rupiah, hal ini tentu saja memberikan 
kesan bahwa Kawasaki Ninja 250R ialah sepeda motor ekslusif yang ditujukan 
untuk kalangan ekonomi menengah keatas, ditunjang dengan penampilan yang 
tergolong menarik dan mempesona. Walaupun sebenarnya Kawasaki Ninja 250R 
yang beredar di Indonesia masih menggunakan sistem bahan bakar karburator, 
tidak seperti di Eropa yang sudah lebih canggih dan modern dengan menggunakan 
system bahan bakar injeksi.   
Ruggedness (tangguh), yaitu karakteristik merek yang dikaitkan dengan 
kemampuan suatu merek dalam menunjang kegiatan luar rumah dan kekuatan 
atau daya tahan produk. Dimensi ini memiliki memiliki sifat outdoorsy (senang 
dengan kegiatan outdoor), tough (tangguh, ulet, tidak mudah menyerah). Dimensi 
ini tercermin pada iklan Kawasaki Ninja 250R di media elektronik dimana pada 
iklan tersebut digambarkan seorang pengendara sedang mengendarai Kawasaki 
Ninja 250 R seorang diri dengan kecepatan tinggi di sebuah gurun gersang, hal 
tersebut mengesankan Kawasaki Ninja 250R adalah sepeda motor yang tangguh, 
mandiri, maskulin, dan cocok untuk kegiatan outdoor. Namun pada kenyataannya 
ukuran sepeda motor ini kurang cocok untuk postur tubuh mayoritas masyarakat 
Indonesia dengan rata-rata tinggi badan 170 cm. 
  Brand personality dipengaruhi oleh dua faktor yaitu, product-related 
characteristic dan non-product related characteristic (Aaker, 1996: 146). 
Product-related characteristic merupakan faktor yang secara langsung 
mempengaruhi pembentukan brand personality meliputi product category 
(kategori produk) yaitu perbedaan kategori produk dapat membedakan kelas dari 
sebuah produk. PT KMI memproduksi beberapa kategori sepeda motor. 

