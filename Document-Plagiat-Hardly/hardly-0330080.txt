Proses ini, seperti disebutkan didepan menyangkut pengintegrasian 
pengalaman-pengalaman sebelumnya pada masa kanak-kanak, pengalaman 
yang tengah dialami sekarang dan harapan-harapannya di masa yang akan 
datang. Kemampuan remaja dalam memadukan berbagai pengalaman dan 
berbagai harapan merupakan suatu tuntutan yang niscaya perlu 
disempurnakan terus menerus. Sebab proses ini pada akhirnya akan berujung 
pada keberhasilan atau kegagalan remaja membentuk identitasnya. Bila 
berhasil maka remaja akan meraih idetitas yang baru dan mantap. Sedangkan 
kalau gagal, remaja akan sampai kondisi kebingungan identitas. Kegagalan 
meraih identitas yang jelas biasanya merupakan akibat dari pemaksaan yang 
terlalu dini pada remaja untuk memilih suatu karier, pasangan hidup atau 
keyakinan ideologis tertentu, padahal ia belum mencoba langsung dan 
mempertimbangkan secara masak akibat dari pilihannya itu (Cremers, 1989). 
 Pembentukan identitas diri meupakan bagian terpenting dari suatu 
perkembangan kepribadian seseorang. Proses pembentukan identitas yang 
terutama berlangsung selama masa remaja dilakukan dengan 
mengkonsolidasikan beberapa cirri identitas yang telah diraihnya pada akhir 
masa kanak-kanak untuk menuju ke masa dewasanya yang sehat. Karena 
kompleksitasnya proses tersebut, individu dituntut mampu mensistesiskan 
keterampilan, kepercayaan dan identifikasi yang telah diraihnya pada masa 
kanak-kanak itu kearah yang lebih padu, atau malah kurang padu, pada masa 
dewasanya. Dengan demikian, dapat dimaknai bahwa pembentukan identitas 
merupakan proses unik yang dilakukan individu remaja dalam memadukan 
perasaan dan kemantapan pribadinyaatas pengalamannya pada masa kanak-
kanak dan harapannya pada masa dewasa (Marcia, 1993). 
 Remaja awal dapat dilihat sebagai suatu periode destrukturisasi, 
dimana taraf kognitif, psikoseksual dan fisiologis yang telah dicapai 
sebelumnya dirubah untuk kemudian dialihkan ke dalam nuansa yang lebih 
dewasa. Masa pertengahan remaja ditafsirkan sebagai suatu fase 
restrukturisasi, yaitu saat individu melakukan penataan yang lebih baru atas 
keterampilan lama dan keterampilan baru yang telah dimilikinya. Adapun 
masa remaja akhir berbeda dengan dua periode keremajaan sebelumnya. 
Masa tersebut tampak sebagai suatu peiode konsolidasi susunan identitas 
yang ada dan mengujicobakan ke dalam lingkungannya. Dengan demikian, 
masa remaja akhir merupakan suatu periode dalam rentang kehidupannya saat 
untuk pertama kalinya seseorang mencapai kematangan atas banyak identitas 
(Marcia, 2003). 
 Berkaitan dengan ini, Waterman (dalam Marcia, 1993) menyatakan 
hipotesis dasar perkembangan identitas dengan rumusan:  Transisi dari masa 
remaja menjadi dewasa melibatkan menguatnya pemahaman tentang identitas 
secara prograsif.  Masa transisi ini berlangsung dalam proses eksplorasi atau 
pencarian identitas-identitas dan berujung pada komitmen atau  tanggung 
jawab  terhadap pilihan identitasnya tersebut. 
Perubahan dalam cara seseorang memandang dan merasakan dirinya 
terjadi sepanjang siklus hidup manusia. Pembentukan konsepsi diri dan citra 
diri terjadi sepanjang masa anak-anak. Bila diminta untuk menguraikan 
mengenai diri mereka, anak-anak yang lebih besar menunjukan pandangan 
yang jauh lebih kompleks mengenai diri mereka dibandingkan dengan anak-
anak yang lebih kecil. Anak-anak yang lebih kecil membatasi uraian pada 
daftar barang yang mereka miliki atau hal-hal yang mereka suka lakukan, 
anak-anak yang lebih besar cenderung mengatakan juga mengenai 
kepribadian, sifat-sifat diri mereka. 
 Dalam kenyataan perubahan identitas terjadi sepanjang siklus 
kehidupan, namun perubahan yang terjadi pada masa remaja melibatkan 
reorganisasi dan restrukturisasi yang mendasar dalam penghayatan individu 
mengenai dirinya. Pada saat ini dia mempunyai kemampuan intelektual untuk 
memahami arti perubahan tersebut. Walaupun perubahan yang penting terjadi 
pada masa kanak-kanak, remaja lebih menyadari dan menghayati perubahan 
ini dengan lebih sungguh. 
 Pada masa remaja individu menunjukan perhatian terhadap perubahan 
yang mendasar dalam segi biologis, kognitif dan sosial yang merupakan cirri 
masa tersebut. Pada masa pubertas terjadi perubahan yang dramatis dalam 
penampilan fisik individu dan hal ini mengubah konsepsi remaja tentang 
dirinya dan juga mengubah hubungan dirinya dengan orang lain. Pubertas 
memegang peranan penting dalam perkembangan selama masa remaja, 
mengubah potongan rambut, mengurangi berat badan, membeli kacamata 
baru, kadang-kadang remaja juga merasa seakan-akan kepribadian remaja 
juga berubah. Selama masa pubertas, ketika remaja menunjukan perubahan 
dramatis dalam penampilan luar, sebenarnya di dalam diri mereka pun terjadi 
perubahan. Bagi remaja mengalami perubahan fisik meningkatkan fluktuasi 
dalam citra diri mereka dan reevaluasi mengenai siapa diri mereka 
sesungguhnya. 
 Kemampuan intelektual yang meningkat pada masa awal remaja 
memungkinkan cara berpikir baru dalam mengahadapi masalah, nilai-nilai 
dan relasi interpersonal, juga memungkinkan remaja memandang dirinya 
dengan cara baru. Pada masa remaja individu mulai dapat berpikir secara 
sistematis mengenai kejadian-kejadian di masa yang akan datang yang 
bersifat hipotesis. Pada masa ini individu mulai bertanya:  Akan menjadi apa 
saya nanti? ,  seperti apakah identitas diri saya? , atau  Orang seperti apakah 
saya ini?  karena cara berpikir individu pada masa pra-remaja bersifat 
kongkret dia tidak mungkin berpikir unutk menjadi orang yang berbeda. 
Perubahan cara berpikir yang terjadi pada masa remaja membukakan dunia 
baru yang penuh altenatif mengenai kemungkinan identitas dirinya. 
 Perubahan dalam peran sosial juga membukakan serangkaian pilihan 
dan keputusan orang muda yang tidak pernah dipikirkan sebelumnya. Dalam 
keadaan masyarakat seperti dewasa ini, masa remaja merupakan masa 
penting karena pada masa ini individu memikirkan dengan lebih sungguh-
sungguh untuk membuat keputusan lebih penting mengenai pendidikan 
formal, pekerjaan, perkawinan dan masa depan. Dalam menghadapi 
keputusan seperti itu posisi seorang individu di dalam masyarakat tidak hanya 
memunculkan pertanyaan mengenai siapa diri remaja dan kemana remaja 
menuju, lebih dari itu merupakan kebutuhan yang harus dijawab. Pada masa 
ini individu harus membuat keputusan yang penting mengenai karir, 
komitmen terhadap orang lain dan pemikiran mengenai ini mendorong 
mereka untuk mempertanyakan lebih lanjut mengenai diri mereka:  Apakah 
yang saya sungguh-sungguh inginkan dari hidup ini? ,  barang-barang 
apakah yang penting bagi saya? ,  menjadi orang yang seperti apakah yang 
benar-benar saya inginkan?  pertanyaan mengenai masa yang akan datang 
muncul karena remaja mempersiapkan diri menghadapi masa dewasa dan 
untuk tujuan itu dia mengajukan pertanyaan mengenai identitas. 
 Perkembangan identitas bersifat kompleks dan mengandung banyak 
segi. Perkembangan ini lebih mudah dipahami sebagai serangkaian 
perkembangan yang berhubungan satu dengan yang lainnya dari pada 
memandangnya sebagai satu perkembangan yang bersifat tunggal. 
Perkembangan ini melibatkan perubahan cara individu memandang dirinya 
sendiri dalam hubungannya dengan orang lain dalam hubungannya dengan 
masyarakat luas di mana mereka hidup. 
Domain ego identity  dapat dibedakan menjadi domain-domain utama 
dan domain-domain penunjang. Domain-domain utama terdiri dari vocational 
choice, religious belief, Political ideology, Gender role attitudes, belief about 
sexual expression. 
1. Vocational choice 
Domain ini membahas mengenai keputusan seseorang mengenai 
pekerjaanya. Vocation tidak diartikan sebatas pekerjaan yang dibayar, 
tetapi meliputi pekerjaan yang di bayar dan juga mengurus rumah tangga, 
menjalankan tanggung jawab orang tua, kegiatan sukarela atau kegiatan 
lain yang dikerjakan seseorang dengan jumlah waktu tertentu walaupun 
tidak memperoleh penghasilan dalam arti ekonomis, misalnya kegiatan 
kesenian, olah raga amatir. Isu-isu yang merupakan bagian dari domain 
ini adalah: 
  Apakah sebaiknya seseorang berusaha untuk mendapat pekerjaan 
yang memberinya pengahsilan atau melakukan pekerjaan rumah 
tangga dan tanggung jawab sebagai orang tua? 
  Apakah pilihan yang terbaik dalam bidang karier ? 
  Sejauh manakah pentingnya imbalan intrinsic dan ekstrinsik 
  Sejauh manakah pentingnya arti mengambil resiko untuk 
pengembangan karier dan mempertahankan stabilitas dan kemapanan 
dalam arti ekonomis? 
  Apakah yang dapat dilakukan sehubungan dengan antisipasi mengenai 
kejenuhan menjalankan pekerjaan dan bagaimanakah cara 
menghadapinya? 
2. Religious Beliefs 
Domain ini membahas mengenai pandangan seseorang mengenai 
persoalan yang biasanya termasuk dalam pembicaraan mengenai 
agama. Bila seseorang tidak menganut suatu agama tertentu dalam arti 
konvensional, maka dapat diartikan sebagai filsafat hidup, Khususnya 
yang berhubungan dengan etika dan tanggung jawab social. Isu-isu 
yang merupakan bagian dari domain ini adalah : 
  Apakah seseorang percaya akan adanya tuhan? 
  Apakah bentuk dan berapa banyakkah kegiatan keagamaanyang harus 
dilakukan seseorang? 
  Apakah seseorang perlu mempertahankan keterlibatan dengan suatu 
organisasi keagamaan tertentu atau seseorang harus mempunyai 
orientasi kegamaan yang bersifat pribadi, tanpa melibatkan diri pada 
kelompok tertentu? 
  Sikap apakah yang perlu diambil seseorang dalam berhadapan dengan 
doktrin yang ada? 
  Dalam keadaan apakah seseorang perlu berpindah agama? 
  Bila seseorang tidak menganut agama tertentu, dasar apakah yang 
digunakannya untuk penilaian etis? 
  Bagaimanakah pandangan seseorang mengenai perkawinan antar 
agama? 
  Pendidikan agama seperti apakah yang harus diberikan kepada anak-
anak? 
3. Political Ideology 
Domain ini membahas mengenai filsafat politik seseorang, Hubungan 
antara individu dengan masyarakat , tidak terbatas pada persoalan politik. 
Isu-isu yang merupakan bagian dari domain ini adalah : 
  Apakah seseorang perlu masuk dalam paratai politik tertentu dan 
dukungan dalam bentuk apakah yang sebaiknya dia berikan kepada 
partai tersebut? 
  Dimanakah dia menempatkan diri dalam kontinum liberal, Moderat 
atau konservatif, dalam arti politik? 
  Sikap apakah yang sebaiknya diambil oleh seseorang dalam 
menghadapi isu sosial dan politik seperti misalnya pemerintahan yang 
besar atau kecil, perang dan damai, kebijakan ekonomi, kebijakan 
hubungan luar negeri, perlindungan lingkungan hidup, hubungan antar 
suku dan keadilan social? 
4. Gender Role Attitudes 
Domain membahas mengenai apakah artinya bagi seseorang untuk 
menjadi perempuan atau laki-laki. Isu-isu yang merupakan bagian dari 
domain ini adalah : 
  Sampai sejauh apakah seseorang mengikuti pola-pola peran gender 
yang diartikan sebagai maskulin, feminine atau androgin? 
  Bagaimanakah pandangan seseorang mengenai minat yang khusus 
untuk jenis kelamin tertentu dan/atau minat yang tidak terikat pada 
jenis kelamin tertentu? 
  Apakah pertimbangan mengenai peran gendr mempengaruhi 
keputusan untuk memilih teman? 
  Apakah sikap yang harus diambil seeorang sehubungan dengan isu 
social, politik sehubungan dengan emansipasi perempuan 
5. Belief about sexual expression 
Domain ini membahas mengenai arti kegiatan seksual dalam 
kehidupan seseorang. Isu-isu yang merupakan bagian dari domain ini 
adalah : 
  Sejauh manakah ekspresi seksual mempengaruhi arti diri seseorang? 
  Orientasi seksual seperti apakah (heteroseksual, Homoseksual, 
biseksual) yang secara pribadi ditunjukan oleh seseorang? 
  Apakah hubungan antara perasaan cinta dengan kegiatan seksual? 
  Bagaimanakah pendapat seseorang mengenai kegiatan seksual pra 
nikah? 
  Bagaimanakah pandangan seseorang mengenai kegiatan seksual di 
luar nikah? 
  Keyakinan apakah yang dimiliki seseorang sehubungan dengan 
penggunaan alat-alat kontrasepsi? 
Domain-domain penunjang adalah avocational interest, relationship 
with friends, relationship with dates, role of spouse, role of parents, 
priorities assigned to family and career goal. 
1. Avocational Interest 
Domain ini membahas mengenai pemaknaan diri seseorang 
sehubungan dengan kegiatan waktu luang mereka. Isu-isu yang 
merupakan bagian dari domain ini adalah: 
  Sejauhmanakah pentingnya arti kegiatan waktu luang bagi kehidupan 
seseorang? 
  Kegiatan di luar kerja/ belajar apakah yang dirasakan paling 
memberikan kepuasan? 
2. Relationship with friends 
Domain ini membahas mengenai bagaimana seseorang memberi arti 
pada dirinya dalam hubungannya dnegan teman-teman. Domain ini 
kadang-kadang dianggap merupakan bagian dari intimacy, Intimacy 
membahas mengenai bagaimana seseorang benar-benar berelasi 
dengan orang lain. Isu-isu yang merupakan bagian dari domain ini 
adalah : 
  Dalam hal apakah seseorang harus memberi atau berbagi dengan 
teman? 
  Apakah yang dianggap wajar dapat diharapkan seseorang dari 
temannya? 
  Tanggapan apakah yang dianggap wajar dapat diharapkan oleh 
teman? 
  Pertimbangan apakah yang seharusnya digunakan seseorang dalam 
memilih teman? 
3. Relationship with dates 
Domain ini membahas mengenai hal yang sama seperti dalam 
friendship namun disertai komponen romantis yang potensial, actual, 
bagaimana seseorang mengartikan dirinya dalam hubungan dengan 
pacar. Domain ini juga sering dianggap tumpang tindih dengan 
intimacy. Isu-isu yang merupakan bagian dari domain ini adalah: 
  Dalam hal apakah seseorang harus memberi atau berbagai dengan 
pacar? 
  Apakah yang dianggap wajar dapat diharapkan seseorang dari 
pacarnya? 
  Tanggapan apakah yang dianggap wajar dapat diharapkan oleh pacar? 
  Pertimbangan apakah yang seharusnya digunakan seseorang dalam 
memilih pacar? 
4. Role of Spouse 
Domain ini membahas bagaimana seseorang mengartikan dirinya 
dalam hubungan dengan pasangan hidup, suami atau istri. Domain ini 
juga sering dianggap tumpang tindih dengan intimacy. Isu-isu yang 
merupakan bagian dari domain ini adalah: 
  Apakah alasan seseorang menikah atau tidak menikah? 
  Apakah artinya menjadi seorang istri atau suami? 
  Apakah seorang bersedia untuk menerima batasan-batasan yang 
ditentukan oleh pasangan hidupnya melalui kehidupan perkawinan? 
  Dalam hal apa seseorang harus memberi atau berbagi dengan 
pasangan  hidupnya? 
  Apakah yang dianggap wajar dapat diharapkan seseorang menjadi 
pasangan hidupnya dalam kehidupan perkawian? 
  Dalam keadaan apakah suatu perkawinan dapat diakhiri dengan 
perceraian? 
5. Role of parent 
Domain ini membahas mengenai pilihan  seseorang untuk 
mengartikan dirinya dalam kegiatan tanggung jawab sebagai orang 
tua. Domain ini juga sering dianggap tumpang tindih dengan 
generativity. Isu-isu yang merupakan bagian dari domain ini adalah: 
  Mengapa seseorang ingin menjadi orang tua atau tidak ingin menjadi 
orang tua? 
  Apa yang ingin diberikan seseorang kepada anak dalam perannya 
sebagai orang tua? 
  Menjadi orang tua seperti apakah yang diinginkan seseorang, Gaya 
apakah yang paling menonjol ditunjukan seseorang sebagai orang tua? 
  Apakah yang dianggap wajar dapat diharapkan seseorang dari anaknya? 
6. Priorities assigned to family and career goals 
Domain ini membahas mengenai bagaimana seseorang 
mengutamakan salah satu dari dua bidang kehidupan yaitu kehidupan 
keluarga dan kegiatan bekerja. Isu-isu yang merupakan bagian dari 
domain ini adalah: 
  Bagaimana seharusnya seseorang membagi waktu untuk memenuhi 
tanggung jawab pekerjaan dan tanggung jawab keluarga? 
  Bila ada konflik antara kepentingan karier dan peran sebagai pasang 
hidup, Bagaimana cara menyelesaikannya? 
  Bila ada konflik antara kepentingan karier dan peran sebagai peran 
orang tua bagaimanakah cara menyelesaikannya? 
Dalam membahas status identitas dalam hubungannya dengan 
berbagai domain, yang perlu diperhatikan adalah berapa banyak isu yang 
muncul dari satu domain, pentingnya arti individu dan juga pentingnya arti 
isu yang tidak menjadi pokok pembahasan. Dalam menetukan status identitas 
Marcia menekankan pentingnya proses dan bukan isi, variabel. Keputusan 
mengenai status seseorang dibuat atas dasar pemahaman mengenai 
bagaimana proses individu berusaha untuk memenuhi tugas untuk 
berkembang. 
The identity statuses are difined interms of process, not content, 
variables. What is needed to arrive at a decision on the assignment of a 
status is an understanding of how the individual went about handling the 
task developing whatever content may be discussed in the interview 
(Marcia, et al, 1993) 
 Eksplorasi adalah proses yang mengarah pada upaya mencari, 
menjajaki dan mengidentifikasi bebagai aspek kehidupan. Marcia (1993) 
memberikan batasan mengeni eksplorasi sebagai suatu periode berjuang, 
tanya-jawab aktif untuk mencapai keputusan-keputusan mengenai tujuan-
tujuan, nilai-nilai dan keyakinan. Marcia menegaskan ada tiga kondisi 
eksplorasi, yaitu: 
a. Past condition: eksplorasi telah terjadi dan berakhir. 
b. In condition: eksplorasi sedang berlangsung. 
c. Absence condition: eksplorasi dianggap tidak diperlukan. 
Berdasarkan posisi ketiga kondisi eksplorasi tadidapat disimpulkan bahwa 
eksplorasi tidak hanya berupa proses pencarian dan pengidentifikasian 
alternatif dari berbagai aspek kehidupan, melainkan juga berupa hasil dari 
proses tersebut. Kriteria ada atau tidak adanya eksplorasi dapat dijelaskan 
sebagai berikut: 
1. Knowledgeabilty  
Kemampuan untuk membuat penilaian berdasarkan kebutuhan dan 
kemampuan individu secara akurat mengenai suatu hal. Untuk membuat 
penilaian yang akurat tersebut diperlukan pengetahuan yang mendetail 
dan adanya gambaran realistic tentang hal itu. 
2. Activity Directed Toward the Gathering of Information 
Untuk menunjang keluasan dan kedalaman pengetahuan, diperlukan 
informasi mengenai hal tersebut. Perlu adanya aktifitas pengumpulan 
informasi seperti membaca buku, diskusi dengan orang lain dan inisiatif 
untuk melakukan pencarian. 
3. Considering Alternative Potential Identity Elements 
Sejalan dengan perkembangannya, individu akan menyadari aspek-aspek 
pada dirinya yang berbeda. Untuk itu remaja perlu mempertimbangkan 
alternatif dari berbagai informasi untuk menentukan atau memilih 
identitas yang potensial dan mencurahkan perhatian terhadap pilihan-
pilihan dan konsekuensi dari setiap pilihan. 

