Sejarah sepakbola sendiri berasal dari bangsa Romawi membawa 
permainan tersebut ke Inggris dengan nama  Harpascum . Saat pertama kali 
dimainkan, sepakbola belum memiliki aturan permainan, satu bola diperebutkan 
oleh dua kelompok sehingga sering timbul banyak korban dikedua pihak 
(http:/www.kabarindonesia.com/sejarahsepakbola.html). Kebrutalan dan 
banyaknya korban yang terjadi dalam permainan sepakbola primitif sehingga 
King Edward III pada tahun 1331 mengeluarkan atura untuk menghentikan 
permainan sepakbola di Inggris. 
Seiring dengan majunya peradaban manusia, berkembanglah sepakbola 
menjadi suatu olahraga yang aman untuk dimainkan. Hal tersebut ditandai dengan 
diciptakannya peraturan baku yang bersifat internasonal mengenai administrasi 
penyelenggaraan pertandingan sepakbola. 
Berbicara mengenai sepakbola bearti berbicara mengenai bayak orang 
yang terlibat di dalamnya, termasuk suporter sepakbola itu sendiri, sepakbola 
memiliki kaitan yag sangat erat dengan pendukung atau suporter. Setiap klub bola 
profesional memiliki kelompok pendukung tertentu. Bahkan, kelompok 
pendukung tersebut mempunyai nama   nama tertentu untuk menunjukkan 
identitas mereka ( Roversi dalam Giulianotti dan Williams, 1994 ). Seperti 
contohnya contohnya   The KOP   kelompok suporter Liverpool FC dari Inggris, 
  Juventini   kelompok suporter Juventus FC dari  Italia. Di Indonesia sendiri, 
terdapat beberapa kelompok suporter seperti   The Jakmania   suporter Persija 
dari Jakarta,   Viking   suporter Persib dari Bandung dan masih banyak yang lain. 
Berbalut seragam tim kebanggaan para suporter pun bernyanyi, bersorak, atau 
berteriak mendukung pemain tim kesayangan yang sedang bertanding di 
lapangan. Dapat diketahui bahwa pertandingan sepakbola resmi yang melibatkan 
masa dalam jumlah banyak bisa menghasilkan crowd. 
Menurut Hinca (2007), Suporter atau fans club adalah sebuah organisasi 
yang terdiri dari sejumlah orang yang bertujuan untuk mendukung sebuah klub 
sepak bola. Suporter harus berafiliasi dengan klub sepak bola yang didukungnya, 
sehingga perbuatan suporter akan berpengaruh terhadap klub yang didukungnya. 
Klub dapat diberikan sanksi apabila suporter baik perorangan maupun grup 
melakukan tindakan yang merusak atau tindakan anarki. Namun, klub juga harus 
menyediakan fasilitas dalam bentuk subsidi finansial, infrastruktur dan pendidikan 
kepada suporter. Klub juga harus memberikan penjelasan kepada suporter 
mengenai peraturan permainan, dan peraturan perwasitan yang bertujuan agar 
suporter dapat lebih mengerti peraturan yang berlaku. Suporter harus berlaku 
sopan dan memberikan dukungan, sehingga akan memberi respons positif dari 
penonton atau suporter yang lain sehingga tingkat kerusuhan dapat di minimalisir. 
Ajiwibowo (2007) menyatakan bahwa suporter saat ini mengambil dua peran 
sekaligus yaitu sebagai penampil (performer) dan penonton (audience).  
Sebagai penampil (performer) yang ikut menentukan jalannya 
pertandingan sepakbola, suporter kemudian menetapkan identitas yang 
membedakannya dengan penonton biasa. Suporter jauh lebih banyak bergerak, 
bersuara dan berkreasi di dalam stadion dibanding penonton yang terkadang 
hanya ingin menikmati pertandingan sepak bola dari kedua tim yang bertanding. 
Suporter dengan peran penyulut motivasi dan penghibur itu biasanya membentuk 
kerumunan dan menempati area atau tribun tertentu di dalam stadion. Para 
suporter ini menemukan kebahagiaan dengan cara mendukung secara penuh tim 
kesayangannya, sekaligus memenuhi kebutuhan mereka akan kepuasan yang tidak 
dapat dilakukan sendirian. 
Russell ( 1993 ) mengatakan  bahwa  diluar  peperangan,   olah  raga 
merupakan salah satu wahana bagi tindakan agresi yang ditoleransi oleh sebagian 
besar masyarakat. Perilaku agresi tidak hanya terjadi pada pemain tetapi juga 
terjadi pada penonton. Selanjutnya Arm, dkk (1979) dalam penelitiannya 
menyatakan bahwa responden yang menonton pertandingan gulat atau 
pertandingan hoki menunjukkan sikap bermusuhan yang lebih tinggi 
dibandingkan penonton lomba renang (kondisi kontrol non agresif). Pada 
pertandingan olah raga beregu dan profesional, kekerasan juga terjadi pada 
penonton, seperti kerusuhan antara suporter sepak bola atau kasus hooliganisme. 
Kata hooligan sendiri berasal dari suporter Inggris yang pertama kali melakukan 
kekerasan dalam skala besar pada saat penyelenggaraan piala  dunia tahun 1966 di 
Inggris. Roversi ( Giulanotti dan Williams, 1944 ) mendefinisikan football 
hooliganisme sebagai aksi pengrusakan yang sistematis. Terkadang terjadi agresi 
berdarah, yang dilakukan oleh kelompok suporter tertentu melawan suporter 
lainnya baik didalam maupun diluar arena pertandingan sepak bola. 
Perilaku suporter Indonesia dewasa ini menunjukkan sikap fanatisme yang 
berlebihan yang dimanifestasikan dalam perilaku agresif seperti kerusuhan antar 
suporter, pengerusakan fasilitas stadion dan di luar stadion, cacian, cemohan, dan 
lain-lain ketika tim kesayangannya kalah atau tidak puas dengan hasil 
pertandingan. Besarnya dukungan suporter tidak saja memberikan konsekuensi 
positif terhadap tim, melainkan juga memberikan dampak negatif pada tim, 
terutama akibat tindakan agresi atau kebrutalan yang ditimbulkannya. 
 Seperti kerusuhan yang terjadi yang dilakukan pendukung pada saat 
pertandingan antara Persija Jakarta melawan Persikab di Bogor dan melawan 
Persita di Tangerang dalam pertandingan Liga Djarum Indonesia, sehingga 
Komisi Disiplin PSSI. 
(Persatuan Sepak Bola Seluruh Indonesia) menjatuhkan sanksi kepada tim 
Persija denda sebesar 25 (dua puluh lima) juta Rupiah. (Media Indonesia, 2008) 
Di Kota Bandung sendiri terdapat suporter bola yang bernama Viking yang telah 
berdiri sejak 1993 yang mendukung klub Persib.  
Bermula saat kelompok fanatik yang biasa menempati tribun selatan 
mencetuskan ide untuk menjawab totalitas idola mereka yaitu Persib Bandung di 
lapangan dengan sebuah totalitas dalam memberi dukungan, maka setelah melalui 
beberapa kali pertemuan, akhirnya terbentuklah sebuah kesepakatan bersama. 
Tepatnya pada Tanggal 17 Juli 1993, disebuah rumah jalan Kancra no. 34. 
Akhirnya lahir lah sebuah kelompok bobotoh dengan nama VIKING PERSIB 
CLUB. Adapun pelopor dari pendiriannya antara lain Ayi Beutik, Heru Joko, 
Dodi  Pesa  Rokhdian, Hendra Bule, dan Aris Primat dengan dihadiri oleh 
beberapa Pioner Viking Persib Club lainnya, yang hingga kini masih tetap aktif 
dalam kepengurusan Viking Persib Club.  
Nama Viking diambil dari nama sebuah suku bangsa yang mendiami 
kawasan skandinavia di Eropa Utara. Suku bangsa tersebut dikenal dengan sifat 
yang keras, berani, gigih, solid, patriotis, berjiwa penakluk, pantang menyerah, 
serta senang menjelajah. Karakter dan semangat itulah yang mendasari nama 
Viking kedalam nama kelompok yang telah dibentuk. Secara demonstratif, Viking 
Persib Club pertama kali mulai menunjukan eksistensinya pada Liga Indonesia I 
  tahun 1993, yang digemborkan sebagai kompetisi semi professional pertama di 
Tanah Air. 
Acara pertandingan bola di Bandung pun tidak jauh berbeda dengan 
pertandingan bola di negara lain, identik dengan tertawa, menari   nari jika 
timnya menang, bahkan jika tim yang didukung kalah para suporter berekspresi 
yang di tunjukkan adalah seperti kekecewaan seperti diam dan kadang-kadang 
mencomooh pemain baik lawan ataupun pemain yang didukung dan tidak 
terdorong untuk memotivasi, bahkan dapat melempar botol atau benda   benda 
lainnya kepada lawan tim saingannya ataupun terhadap suporter bola saingannya 
yang berada di stadion.  
Tindakan para suporter Viking di stadion terkadang membuat suasana 
pertandingan bola  menjadi kacau dan bisa berujung pada perkelahian dan tawuran 
antar suporter di pertandingan tersebut. 
Dari kejadian pertandingan bola yang berpotensi kerusuhan dan 
penampilan yang terkesan agresif dari kebanyakan suporter Viking membuat 
adanya judgement negatif dari kalangan masyarakat di kota Bandung terhadap 
suporter   suporter bola khusunya di Bandung seperti Viking. 
Burhanuddin (1997) mengindikasikan bahwa tindak kerusuhan pada 
suporter sepak bola dan agresivitas massa muncul dari arus sosial yang 
menghanyutkan emosi mereka ke luar kontrol kesadaran dirinya sendiri. Tindakan 
tersebut merupakan gejala sosial yang tidak memiliki bentuk yang jelas dan bisa 
saja terjadi pada setiap orang. Seperti yang terjadi pada Stadion Si Jalak Harupat 
para bobotoh menyerang da merusak mobil yang berpelat B, tidk sampai disitu 
penumpang mobil juga dikeroyok sehingga mengakibatkan luka ringan dan luka 
berat, hal itu mengusik keamanan dan kenyamanan warga sekitar Stadion Si Jalak 
Harupat. ( Kompas 18 Februari 2014 ) 
Berdasarkan pengamatan peneliti dalam menonton pertandingan bola di 
Bandung, penonton yang menghadiri pertandingan didominasi oleh laki-laki yang 
tergolong masih remaja menuju dewasa. Pada masa remaja yang dikenal dengan 
masa mencari jati diri dan masa memberontak dirasa cukup sulit untuk menjaga 
kestabilan emosi dan cukup mudahnya untuk terpengaruh. Sebagai contoh dengan 
menonton aksi para suporter Viking dalam mengapresiasi mendukung timnya 
yang sedang bertanding di lapangan tersebut dengan ekspresi fisik/kekerasan 
membuat para remaja meniru tingkah laku tersebut.  
Selain itu agresi yang dimiliki oleh remaja pun dapat terpicu baik oleh hal-
hal yang ditontonnya dari penampilan suporter bola lainnya jika dalam 
mendukung yang berisi kekerasan seperti mengolok dengan kata   kata kasar 
terhadap tim lawan ataupun suporter bola tim lawan, melemparkan benda   benda 
yang berbahaya, merusak fasilitas   fasilitas umum yang ada di Bandung, 
tindakan vandalism, bahkan tidak segan untuk menghancurkan mobil   mobil 
pendatang dari luar Bandung dan lainnya.  
Serta penelitian terdahulu yang dianggap oleh peneliti relevan dengan 
penelitian kali ini adalah adanya ikatan emosional terhadap tim sepak bola dan 
sikap fanatisme supporter, hasil dari penelitian ini menunjukkan bahwa ada  
hubungan positif yang signifikan ( Jurnal penelitian oleh Suroso, Dyan Evita 
Santi, dan Aditya Permana Fakultas Psikologi Universitas Surabaya ). Dan 
pandangan masyarakat tentang pandangan suporter Bonek yang kerap membuat 
kerusuhan dan tindakan anarkis ( Jurnal penelitian oleh Tri Novyan Setyawan 
UPN Jawa Timur ). 
Hal itu menjadi awal dari minat peneliti untuk meneliti apakah perilaku 
agresi tersebut dimunculkan juga oleh para suporter Viking dalam kehidupan 
sehari-hari atau tidak. Peneliti ingin mengetahui apakah suporter Viking tersebut 
melakukan tindakan agresi dalam kehidupan sehari-hari sesuai ketika dia berada 
di dalam sebuah pertandingan bola. 
Suporter Viking di Bandung yang dikenal keras dan ekstrim memang 
terkadang  diidentikan dengan tindakan agresi dari para suporter Viking. Menurut 
Zillmann (1978), agresi adalah segala bentuk tingkah laku yang bertujuan untuk 
menyerang atau melukai individu lain baik secara fisik maupun psikis yang 
membuat individu tersebut termotivasi untuk menghindar. Dengan begitu 
banyaknya konsep tindakan agresi yang dapat muncul dalam perilaku manusia, 
Buss (1961) mengajukan suatu pemikiran yang membagi agresi kedalam tiga 
dimensi: fisik-verbal, aktif-pasif, dan langsung-tidak langsung. Perbedaan dimensi 
fisik-verbal terletak pada perbedaan antara menyakiti tubuh (fisik) orang lain dan 
menyerang orang lain dengan kata-kata. Perbedaan dimensi aktif-pasif terletak 
pada perbedaan antara tindakan nyata dan kegagalan untuk bertindak, sedangkan 
agresi langsung berarti kontak face to face dengan orang yang diserang dan agresi 
tidak langsung terjadi tanpa kontak dengan orang yang diserang. 
Agresi pada suporter Viking akan dapat dilihat jelas dalam cara para 
suporter melakukan aksi ketika berada di pertandingan untuk mendukung timnya. 
Bentuk perilaku tipe agresi fisik seperti mengolok dengan kata   kata kasar 
terhadap tim lawan ataupun suporter tim lawan, melemparkan benda   benda yang 
berbahaya, merusak fasilitas   fasilitas umum yang ada di Bandung, tindakan 
vandalism, bahkan tidak segan untuk menghancurkan mobil   mobil pendatang 
dari luar Bandung dan lainnya. Hal tersebut berbeda ketika mereka berada pada 
kehidupan sehari-hari. 
Berdasarkan survei awal yang dilakukan peneliti terhadap sepuluh 
mahasiswa laki-laki yang menjadi anggota suporter Viking di Kota Bandung, 
sebanyak 20 orang (100%) mahasiswa yang menjadi anggota suporter Viking 
tersebut mengaku cukup sering mengucapkan kata-kata kasar ketika berbicara 
dengan teman-temannya, selain itu mereka sering mengejek temannya untuk 
dijadikan lelucon ketika mereka berkumpul. Sebanyak 18 orang dari mahasiswa 
suporter yang diwawancara mengaku cukup mudah untuk terlibat dalam 
perkelahian dan tidak ragu untuk memulai perkelahian dengan orang lain baik 
melakukanya sendiri dan secara berkelompok, alasannya mereka melakukan hal 
tersebut adalah karena membela tim yang mereka dukung, dan tidak membiarkan 
seorang-pun mengejek atau menghina tim yang mereka dukung.  
Dari fenomena yang terjadi di Kota Bandung ketika terdapat fakta bahwa 
pertandingan bola kerap kali berpotensi menimbulkan kerusuhan, serta didukung 
dengan fakta bahwa suporter sendiri pada saat pertandingan bola bernuansa 
kekerasan dan cara suporter Viking mendukung serta membela tim yang tidak 
segan bertindak dalam bentuk kekerasan/agresi, membuat peneliti tertarik untuk 
melihat agresi yang terdapat pada suporter Viking di Kota Bandung dalam 
kesehariannya.
Berdasarkan latar belakang masalah yang dikemukakan di atas, dari penelitian 
ini ingin diketahui bagaimana gambaran tipe perilaku agresi pada suporter Viking 
di Kota Bandung. 
Maksud penelitian ini adalah untuk memperoleh gambaran mengenai tipe 
agresi pada suporter Viking di Kota Bandung. 
Tujuan penelitian ini adalah untuk mengetahui gambaran tentang tipe agresi 
berdasarkan dimensi fisik-verbal, aktif-pasif, dan langsung-tidak langsung pada 
suporter Viking di Kota Bandung. 
Memberikan sumbangan bagi ilmu Psikologi Sosial tentang tipe agresi pada 
suporter Persib Viking di Kota Bandung. 
berhubungan dengan agresi dalam setting sosial. 
Memberikan informasi bagi anggota suporter Viking di Kota Bandung. 
usia remaja akhir. Informasi ini diharapkan dapat dimanfaatkan oleh 
anggota suporter Viking di Kota Bandung sebagai sumber pengetahuan 
mengenai tipe agresi mereka. 
dari anak mereka yang menjadi anggota suporter Viking di Kota Bandung 
sebagai bahan pertimbangan menyikapi kegemaran anaknya menonton 
serta mendukung tim favorit dan tipe agresi yang anak mereka munculkan 
sebagai anggota suporter Viking dalam kehidupan sehari-hari. 
Suporter adalah sebuah organisasi yang terdiri dari sejumlah orang yang 
bertujuan untuk mendukung sebuah klub sepak bola. Suporter harus berafiliasi 
dengan klub sepak bola dimana mereka memberikan dukungan, sokongan dalam 
berbagai bentuk disuatu situasi. Suporter biasanya memiliki cara-cara dalam 
mendukung tim kesukaannya, seperti bernyanyi-nyanyi menyatakan 
dukungannya., sehingga perbuatan suporter akan berpengaruh terhadap klub yang 
         Suporter bola terdiri dari berbagai latar belakang budaya, jenis kelamin, usia 
dan status lainnya yang menjadi ciri khas persamaan mereka adalah mereka dalam 
kegiatan mendukung tim kesayangannya atau bahkan dalam kehidupan sehari-
hari. Melihat usia dari suporter dalam suatu organisasi bisa saja memiliki usia 
yang berbeda dari muda sampai tua tidak terkecuali remaja. Berdasarkan 
observasi peneliti dalam suatu pertandingan bola, suporter bola pada umumnya 
masih berusia remaja menuju dewasa. Para remaja yang masih aktif dalam 
mencari identitas diri. 
Menurut Santrock (1998), masa remaja adalah masa perkembangan 
transisi antara masa anak-anak dan masa dewasa yang mencakup perubahan 
biologis, kognitif, dan sosio-emosional. Santrock (1998) melakukan 
pengelompokkan pada remaja berdasarkan usia dari individu. Remaja akhir 
menunjuk pada usia kira-kira setelah 15 tahun, pada fase ini remaja memfokuskan 
kepada karir dan eksplorasi identitas diri. Hal tersebut mendorong remaja akhir 
mencari role model yang dirasa ideal bagi peran mereka dalam kehidupan 
sosialnya. 
Menurut Santrock (1998), remaja akhir memiliki pertumbuhan biologis 
dalam bentuk perkembangan fisik yang diiringi perkembangan psikologis. 
suporter bola yang berada pada fase remaja akhir memiliki taraf kesehatan dan 
daya tahan tubuh yang prima sehingga dalam melakukan berbagai kegiatan 
tampak inisiatif, kreatif, energik, cepat dan proaktif. Hal ini bisa dimanfaatkan 
oleh para suporter bola dalam melakukan aksi saat mereka mensuporter tim 
kesukaan. Suporter bola fase remaja akhir yang pada umumnya sedang 
menempuh pendidikan di SMA atau sebagai mahasiswa juga mengalami 
perkembangan dalam kognisinya, mereka mampu membayangkan situasi rekaan, 
kejadian yang semata-mata berupa kemungkinan, hipotesis, ataupun proposisi 
abstrak dan mencoba mengolahnya dengan pemikiran yang logis. Bahkan 
mahasiswa juga lebih idealistis sehingga mulai menentukan pilihannya sendiri 
dan bertanggungjawab akan pilihan yang dibuatnya.  
Mahasiswa pada masa perkembangan remaja akhir juga mengalami 
peralihan dari bentuk sosial yang bersifat kekanakan bebas menjadi bentuk sosial 
yang matang dan bertanggung jawab. Lingkungan sosial yang bergeser dari 
lingkungan keluarga menjadi lingkungan teman sebaya.
