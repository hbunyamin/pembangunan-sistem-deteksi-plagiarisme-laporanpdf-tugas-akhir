pertama yang muncul adalah individu tersebut  sering 
merasa tidak bahagia  dan dengan demikian menunjukkan perasaan kesepian 
dalam diri individu (feelings of loneliness), asumsi yang kedua individu tersebut 
 sering melakukan segala sesuatu seorang diri  yang menunjukkan adanya 
perasaan isolasi sosial (feelings of social isolation). Terlihat bahwa dalam setiap 
pernyataan UCLA lonelinesss scale memuat indikator lonelinesss yang mengukur 
perasaan kesepian individu (feelings of loneliness) dan perasaan isolasi sosial 
(feelings of social isolation). Indikator tersebut yang selanjutnya digunakan oleh 
para ahli untuk mengukur derajat kesepian yang dialami individu (Peplau, 1998). 
Faktor-faktor demografis berikut ini menunjukkan individu mana yang 
memiliki resiko lebih besar untuk mengalami loneliness, diantaranya sebagai 
berikut: 
A. Usia 
Orang yang berusia tua memiliki stereotipe tertentu di dalam masyarakat. 
Banyak orang yang menganggap semakin tua seseorang semakin merasa 
loneliness. Tetapi banyak penelitian yang telah membuktikan bahwa stereotipe ini 
keliru, salah satunya adalah penelitian yang dilakukan oleh Rubenstein, Shaver, 
dan Peplau (1979) yang menemukan bahwa loneliness paling tinggi terjadi pada 
remaja dan dewasa awal kemudian menurun seiring dengan bertambahnya usia. 
Hal tersebut disebabkan karena individu yang masih muda (remaja dan dewasa 
awal) akan memasuki dunia sosial yang baru seperti di perkuliahan atau pekerjaan 
yang baru, di mana mereka akan membutuhkan relasi yang baru. Pada masa 
remaja individu juga akan menghadapi tugas perkembangan yaitu menetapkan 
identitas diri yang memungkinkan timbulnya perasaan loneliness sehingga 
individu yang lebih muda memiliki kemungkinan mengalami loneliness yang 
lebih besar. 
Survei yang dilakukan oleh Parlee (1979 dalam Perlman & Peplau, 1984) 
juga menunjukkan bahwa 79% responden berusia di bawah 18 tahun mengatakan 
bahwa mereka merasa terkadang atau seringkali merasa kesepian, dibandingkan 
dengan hanya 53% responden berusia 45-54, dan 37% responden berusia 55 tahun 
ke atas. Orang tua sebenarnya lebih merasa puas dengan relasi sosial mereka 
dibanding individu dewasa awal. Meskipun anak muda biasanya memiliki lebih 
banyak kesempatan untuk menjalin relasi sosial, mereka juga memiliki harapan 
yang tinggi dan tidak realistis mengenai relasi sosial mereka. Seiring 
bertambahnya usia, individu dapat menetapkan harapan dan standar yang lebih 
masuk akal bagi relasi sosial mereka. 
B. Gender 
Secara umum, tidak ada perbedaan gender ketika menggunakan 20 item 
UCLA Loneliness Scale, namun perbedaan gender muncul ketika responden 
diminta untuk menjawab pertanyaan langsung tentang apakah mereka merasa 
kesepian atau tidak. Pada pertanyaan tunggal tersebut, wanita lebih mampu 
menjelaskan dirinya sebagai individu yang kesepian dibandingkan pria. Borys dan 
Perlman (1981) menyatakan bahwa para pria lebih sulit untuk menyatakan secara 
langsung atau mengakui bahwa mereka mengalami loneliness. Ketika mengakui 
loneliness pria akan mengevaluasi lebih negatif daripada wanita karena adanya 
harga diri sosial yang lebih tinggi pada pria dibanding wanita, selain itu dampak 
negatif karena mengakui dirinya kesepian lebih banyak diterima oleh pria 
daripada wanita. Pria yang kesepian dipandang kurang dapat menyesuaikan diri, 
kurang diterima secara sosial, dan kurang efektif dalam memainkan berbagai 
peran dibanding wanita. Data menunjukkan bahwa masyarakat kurang dapat 
menoleransi jika pria yang kesepian dibanding jika itu wanita. 
Teori tentang attachment dikembangkan oleh psikiater berkebangsaan 
Inggris, Bowlby (1979 dalam Ju-Ping Chiao Yeo, 2010) yang mengemukakan 
bahwa individu membentuk ikatan emosional yang kuat dengan pengasuh utama 
mereka. Bowlby (1979 dalam Ju-Ping Chiao Yeo, 2010) menyebut ikatan yang 
demikian sebagai ikatan kelekatan atau attachment, dan pengasuh utama sebagai 
figur attachment. Individu menggunakan figur attachment sebagai dasar yang 
kokoh untuk mengeksplorasi dunia dan menganggap figur attachment sebagai 
tempat berlindung yang aman pada waktu bahaya dan kesukaran, selain itu 
individu mencoba untuk menjaga kedekatan dengan figur attachment dan 
menunjukkan tanda-tanda kecemasan ketika mereka terpisah dengan figur 
attachment (Ainsworth, 1985 dalam Ju-Ping Chiao Yeo, 2010). 
Rasa tidak aman (insecurity) akan muncul dalam diri individu ketika figur 
attachment tidak berada di dekat individu pada saat individu mengalami bahaya, 
dalam kondisi sakit atau kelelahan. Sebaliknya, dalam situasi yang tidak 
membahayakan dan dengan adanya figur attachment di dekat individu akan 
membuat individu merasa aman (secure), sehingga individu merasa termotivasi 
untuk secara berani menjelajahi lingkungan di sekitarnya. Bowlby (1973 dalam 
Kirkpatrick, 2005) mengemukakan ketika seorang individu merasa yakin bahwa 
figur attachment akan selalu ada di sisinya kapanpun ia membutuhkan, maka rasa 
takut dalam diri individu tersebut akan cenderung berkurang. 
Ainsworth (1985 dalam Kirkpatrick, 2005) menjelaskan ada 4 kriteria 
yang digunakan individu sebagai acuan untuk membentuk ikatan attachment 
dengan pengasuh: (1) individu akan mencari figur attachment dalam situasi yang 
mengancam, (2) individu menjadikan figur attachment sebagai dasar yang kokoh 
untuk menjelajahi lingkungan sekitarnya, (3) individu menjadikan pengasuh 
sebagai tempat berlindung yang aman, dan (4) individu menunjukkan kecemasan 
ketika mengalami perpisahan dengan figur attachment. 
Kirkpatrick (1999 dalam Kirkpatrick, 2005) mengungkapkan bahwa relasi 
individu dengan Tuhan juga dapat dikatakan sebagai ikatan attachment karena 
memenuhi 4 kriteria tersebut: (1) individu ingin membangun kedekatan dengan 
Tuhan atau berusaha mendekatkan diri kepada Tuhan, misalnya melalui doa dan 
aktivitas keagamaan lainnya (2) individu menjadikan Tuhan sebagai dasar yang 
teguh dalam menghadapi tantangan dunia karena Tuhan dipercaya Mahahadir dan 
Mahakuasa sehingga Tuhan dapat memberi pertolongan dan perlindungan, (3) 
individu menjadikan Tuhan sebagai tempat berlindung yang aman, khususnya 
dalam situasi yang mengancam dan penuh tekanan, dan (4) individu yang merasa 
jauh atau terpisah dari Tuhan dapat merasa sedih dan menderita. Dengan 
demikian, Tuhan juga berperan sebagai figur attachment dan relasi yang terjalin 
antara individu dengan Tuhan merupakan ikatan attachment yang oleh 
Kirkpatrick (2005) disebut sebagai attachment to God. 
 Correspondence hypothesis menyatakan bahwa attachment to God pada 
dasarnya merupakan cerminan dari tipe attachment terhadap orang tua atau 
pasangan (Kirkpatrick, 2005). Secara rinci dijelaskan bahwa individu yang 
memiliki relasi yang positif dengan orang tua atau pengasuh juga akan 
mempersepsi Tuhan secara positif sebagai pribadi yang penuh kasih dan 
memelihara. Sebaliknya, individu yang memiliki relasi yang negatif dengan orang 
tua atau pengasuh juga akan mempersepsi Tuhan secara negatif sebagai pribadi 
yang lebih banyak menuntut dan otoriter.  
 Sejumlah penelitian dilakukan pada remaja untuk menggali hubungan 
antara gambaran remaja tentang Tuhan dengan persepsi remaja terhadap orang 
tua. Spilka, Addison, dan Rosensohn (1975 dalam Kirkpatrick, 2005) melakukan 
penelitian pada para siswa SMA Katolik berusia 16 tahun dan menemukan bahwa 
persepsi terhadap orang tua sebagai penyayang dan pemelihara berkorelasi tinggi 
dengan gambaran tentang Tuhan sebagai penuh kasih, memelihara, dan memberi 
rasa nyaman. Penelitian serupa dilakukan oleh Potvin (1977 dalam Kirkpatrick, 
2005) pada remaja berusia 13-18 tahun dalam penelitian berskala nasional dan 
ditemukan bahwa persepsi remaja mengenai kasih sayang dan aturan yang 
diberikan orang tua berkorelasi secara signifikan dengan gambaran remaja tentang 
kasih dan hukuman yang diberikan Tuhan. 
Bowlby (1979 dalam Ju-Ping Chiao Yeo, 2010) menemukan bahwa tipe 
attachment yang berkembang di masa kanak-kanak akan terinternalisasi dan 
menjadi representasi mental, yang disebut internal working models. Individu 
cenderung menggunakan internal working models yang sama atau mirip dalam 
menjalin hubungan dekat dengan orang lain, seperti hubungan romantis, di 
sepanjang rentang hidup mereka, maupun ketika individu berelasi dengan Tuhan. 
Hal ini sejalan dengan asumsi correspondence hypothesis bahwa pola attachment 
yang terbentuk dengan Tuhan merupakan kelanjutan dari pola attachment yang 
terbentuk dengan orang tua di masa kanak-kanak, sebagaimana yang digambarkan 
dalam bagan 2.2 sebagai berikut: 
Compensation hypothesis menyatakan bahwa attachment to God 
merupakan relasi pengganti (substitute attachment) bagi individu yang kurang 
memiliki ikatan dengan orang tua atau pengasuh karena Tuhan dipandang mampu 
mengisi kekosongan/ kehampaan akibat tidak adanya attachment dengan orang 
tua atau pengasuh (Kirkpatrick, 2005). Compensation hypothesis memprediksi 
bahwa individu dengan attachment masa kanak-kanak yang insecure 
kemungkinan besar nantinya akan berpaling kepada Tuhan agar memperoleh rasa 
aman dan dasar yang kokoh yang selama ini dirasa tidak ditemukan dalam relasi 
attachment masa kanak-kanak dengan orang tua atau pengasuh. Anak-anak yang 
gagal membentuk secure attachment dengan orang tua kemungkinan besar akan 
mencari  surrogates  atau figur attachment penganti, dalam hal ini Tuhan 
merupakan sosok yang potensial untuk menjadi figur pengganti.  
 Keterpisahan dan kehilangan figur attachment dapat menjadi pengalaman 
yang paling berdampak secara emosional dalam kehidupan seseorang. 
Berdasarkan teori attachment, perpisahan dan kehilangan akan mengaktifkan 
sistem attachment sebagai usaha untuk memulihkan hubungan dengan figur 
attachment. Ketika figur attachment utama, seperti orang tua atau pengasuh 
meninggal, atau ketika berada dalam situasi keterpisahan yang lama dengan figur 
attachment maka Tuhan dipandang sebagai figur pengganti attachment yang 
paling menarik dan berharga. Namun demikian, ketika relasi antara individu 
dengan Tuhan sebagai figur pengganti attachment mulai terbentuk, internal 
working models yang dimiliki individu dari interaksi dengan pengasuh 
sebelumnya mulai mempengaruhi relasi dengan Tuhan sehingga kemungkinan 
besar attachment yang terbentuk dengan Tuhan akan mengikuti internal working 
models dengan pengasuh sebelumnya. 
Attachment to God merujuk pada situasi ketika seseorang membentuk 
suatu relasi kedekatan dengan Tuhan dan menganggap Tuhan sebagai figur 
pemberi kasih sayang atau attachment figure (Kirkpatrick, 2005). Namun 
demikian, dibuktikan bahwa seseorang dapat membentuk suatu attachment 
dengan Tuhan hanya jika Tuhan dipertimbangkan sebagai sosok yang relasional 
dan Tuhan pada dasarnya adalah suatu pribadi (Beck & McDonald, 2004). 
Kirkpatrick (1992 dalam Kirkpatrick, 2005) berpendapat bahwa Tuhan seringkali 
digambarkan sebagai figur ayah dalam Alkitab dan dideskripsikan sebagai 
pelindung yang kuat bagi orang percaya.  
Menurut Brennan, Clark, dan Shaver (1998 dalam Beck & McDonald, 
2004), attachment to God dibentuk oleh dua dimensi utama yakni: (1) Anxiety 
about Abandonment dan (2) Avoidance of Intimacy. Secara spesifik, Anxiety about 
Abandonment melibatkan tema-tema hubungan seperti ketakutan akan 
ditinggalkan oleh Tuhan, protes kemarahan (kebencian atau frustrasi atas 
kurangnya perhatian yang diberikan Tuhan), cemburu ketika melihat Tuhan 
nampaknya lebih dekat dengan orang lain, merasa cemas tentang keberhargaan 
diri di mata Tuhan, dan terakhir individu merasa terpaku atau khawatir mengenai 
relasinya dengan Tuhan. Sebaliknya, Avoidance of Intimacy with God melibatkan 
tema-tema hubungan seperti kebutuhan untuk bebas (mandiri), merasa sulit untuk 
bergantung pada Tuhan, dan ketidakmauan untuk dekat secara emosional dengan 
Tuhan. Kombinasi kedua dimensi tersebut akan menentukan derajat kedekatan 
yang terbentuk antara remaja dengan Tuhan, yakni attachment to God yang 
tergolong tinggi atau attachment to God yang tergolong rendah.   
Individu yang memiliki attachment to God yang tinggi adalah individu 
yang meyakini bahwa Tuhan senantiasa hadir, selalu ada dan responsif, khususnya 
ketika menghadapi situasi yang buruk dan mengancam. Ketika individu secara 
konsisten mengalami bahwa Tuhan responsif padanya, maka seiring berjalannya 
waktu individu tersebut akan secara yakin melibatkan Tuhan dalam perjalanan 
hidupnya, menyadari bahwa Tuhan selalu ada dalam situasi di mana dukungan, 
rasa nyaman, pertolongan dan perlindungan dibutuhkan. 

