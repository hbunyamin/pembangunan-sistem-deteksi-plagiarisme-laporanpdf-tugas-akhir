, dengan menggunakan tolok ukur ketidakberhasilan 
 memenuhi target penjuaian yang telah ditetapkart PT 'P' sebagai salah satu distributor 
 yang mernasarkan wadah penyimpanan yang terbuat dari plastik dengan sistim 
 pemasaran mUltilevel juga mengaiarni keadaan demikian. Agar para wiraniaga 
 terdorong rnelakukan penjuaian sebesar-besarnya, pihak perusahaan menetapkan target 
 penjualan dalarn kurun waktu tertentu, disertai pemberian persentasi, hadiah, bahkan 
 peningkatan jenjang karir sebagai penghargaan atas pre stasi ke~janya. Wiraniaga pun 
 dlbekah pengetahuan-pengetahuan tertentu yang didapatkan rnelalui pelatihan-pelatihan 
 yang rutin dilakukan, agar mereka marnpu melakukan pekerjaannya. Di satu sisi PT 'P' 
 memiliki wiraniaga yang rata-rata marnpu menjual produk melebihi target penjualan, 
 narnun di sisi lain ada wiraniaga yang kurang berhasil menjual produk melebihi target 
 penjualan tersebut. 
 Berdasarkan wawancara dengan perwakilan dari PT 'P' diketahui bahwa gaya 
 menjual setiap wiraniaga reiatif berbeda satu sarna lain, sesuai dengan kebiasaan 
 mereka masing-masing dan keadaan konsumen yang mereka hadapi. Kemudian hasil 
 wawancara penetiti dengan beberapa wiraniaga yang berprestasi kerja tinggi dan 
 rendah di PT 'P' tentang proses penjualan yang mereka lakukan menunjukkan bahwa 
 urnumnya wiraniaga yang berprestasi kerja tinggi merasa tidak menemui kesulitan yang 
 WtranHlga yang betprestasi kerja rendah cenderung mengalami kesulitan ketika 
 berinteraksl dengan konsumen, yang akhimya membuat mereka jarang melakukan 
 Dengan demikian peneiiti memperkirakan bahwa gaya menjual yang dilakukan 
 oieh wiraniagaiah yang berperan dalam menunJang keberhasilan ataupun 
 ketidakberhasiian penjuaian produk tersebut Bagaimana gaya menjual wiraniaga yang 
 berprestasi kerja tinggi dan bagaimana gaya metYual wiraniaga yang berprestasi kerja 
 t"endah mi akhimya menarik perhatian peneliti untuk melakukan penelitian lebih lanjut 
 terutama untuk mendapatkan data empiris apakah terdapat perbedaan dalam gaya 
 menjual antara kedua kelompok wiraniaga. 
 Masalah yang akan diteliti adalah: 
 Bagaimana gaya menjual dari wiraniaga yang berprestasi kerja tinggi? 
 Bagaimana gay a menjual dari wiraniaga yang berprestasi kerja rendah? 
 Apakah terdapat perbedaan gaya menjuat antara wiraniaga yang berprestasi kerja 
 tinggi dengan wiraniaga yang berprestasi kerja rendah? 
 Makfmd dari penelitian ini adalah untuk memperoleh gambaran mengenal 
 adakah perbedaan gaya menjuai antara wiraniaga yang betprestasi kerja tinggi dengan 
 wiraniaga yang betprestasi ket:.ia rendal1. 
 Tuiuan dari peneiitian ini adaiah untuk mendapatkan gambaran mengenai gaya 
 melljuai pada \-viraniaga yang betpre&1asi kerja tinggi dan gaya menjual pada wiraniaga 
 yang berprestasi kerja rendah di PT. 'P' - Bandung, serta signifikansi perbedaan gaya 
 menjuai antara kedua keiompok wiraniaga tersebut 
 Kegunaan penelitian ini terbagi menjadi dua: 
 Hasit peneiitian ini diharapkan dapat memberi infbnnasi bagi pihak PT. 'P' 
 mengenai bagaimana kecenderungan gaya menjual dari wiraniaganya, baik yang 
 berprestasi kerja tinggi maupWl yang betprestasi kerja rendah. 
 Int1mnasi mengenai gaya menjual yang paling banyak digunakan oleh wiraniaga 
 yang betprestasi kerja tinggi diharapkan dapat dimanfaatkan oleh bagian pelatihan 
 pelatihan yang selama ini sudah diberikan kepada para wiraniaga. 
 Kegunaan Ilmiah: 
 Penelitian ini menjadi sarana bagi peneliti 1U1tuk menerapkan teori tentang 
 salesmanship terhadap wiraniaga yang melakukan penjualan perorangan ootuk 
 memasarkan produk. 
 Hasil penelitian ini diharapkan dapat menjadi masukan ootuk penelitian sejenis dan 
 Ciri utama dari sarana promosi penjualan perorangan (personal selling) adalah 
 adanya interaksi pribadi secara langsung antara wiraniaga (salesperson) clan pembeli 
 potensial (konsumen). Menurut Richard R. Still (1981), sit1lt Interaksi ini menentukan 
 bagaimana reaksi konsumen, baik reaksi yang sesuai dengan keinginan wiraniaga 
 ataupoo tidak. Interaksi ini selalu melibatkan komunikasi, yang dimulai oleh wiraniaga 
 sebagai komunikator dan konsumen sebagai komunikm Melalui pembicaraan dan 
 tingkah laklUlya wiraniaga mengkomunikasikan pesan yang ingin disampaikarmya 
 kepada konsumen. 
 Mouton (i970), tingkah lai..'U wiraniaga saat menghadapi konsumen tersebut dinamakan 
 gaya menjual. Dasar terjadinya gaya me~iual adalah adanya dua hal yang dipikirkan 
 oieh wiraniaga yaitu perhatian terhadap penjualan dan perhatian terhadap konSUtnen. 
 Kedua perhatian ini akan menentukan cara berinteraksi dan mendorong tingkah laku 
 wiraniaga seiama proses penjualan. Blake dan Mouton menunjukkan interaksi yang 
 terjadi antara kedua perhatian itu meialui diagram gaya m~jual (diagram gaya menjual 
 dapat diiihat pada bagan 2.2). 
 Pada diagnw ditu~iukkan dua jenis petnatian tadi dan cara interaksi antara 
 keduanya. Smnbu horisontal menunjukkan perbatian terbadap penjualan sedangkan 
 sumbu vertikal menunjukkan perhatian temadap konsumen. Masing-masing diungkapkan 
 daiam 9 skala. Angka 1 menunjukkan perbatian minimmn sedangkan angka 9 
 menunjukkan petnatian maksirnum. Angka 5 menunjukkan tingkat menengah. 
 Perpotongan skala pacia sumbu horisontal dan vertikal menggambarkan gaya menjual. 
 Dengan demikian terdapat 81 gaya menjual wiraniaga. Blake dan Mouton 
 mengelompokkannya kedalam 5 gaya menjual utama wiraniaga yaitu gaya menjual (9,1), 
 (1,9), (1,1), (5,5), clan (9,9). 
 Gaya menjual (9,1), merupakan perpaduan antara perbatian yang maksimum 
 terhadap penjnaian dan perhatian yang minimum terhadap konsumen. Tingkah iaku 
 keras serta agresif Wiraniaga dengan gaya menjual ini mempunyai keyakinan yang kuat 
 dan siap mempertahankan gagasan, sikap dan opininya, bahkan ia tidak segan-segan 
 memaksakannya agar orang lain mau menerima. Ia bertekad untuk berhasil, memaksa 
 penJualan produk-sehingga ia melupakan pikiran dan kebutuhan konsumetl., bagaimana 
 perasaan, reaksi, keberatan-keberatan dan keragu-raguan konsumen. 
 Pada gaya menjual (1,9), perhatian minimum terhadap penjualan bergabung 
 secara bersahabat, menjaga perasaan konsumen, bercerita tentang hal yang 
 menyenangkan yang membuat wiraniaga diterima oleh konsumen. Ia ingin mengerti diri 
 konsumen, menanggapi perasaan clan minat konsumen sehingga konsumen akan 
 menyukai dirinya. Melalui gaya menjual seperti ini, dengan sedikit persuasif wiraniaga 
 secara langsung membawa konsumen kearah pembelian. Harapan penjualan lebih 
 dipandang sebagai suatu hasil dari keramahtamahan daripada sebagai akibat tangsung 
 dari inisiatifpenjualan. 
 Pada gaya menjual (1,1), perhatian wiraniaga terhadap penjualan dan perhatian 
 terhadap konsumen sarna-sama rendah. Wiraniaga bertingkah laku pasif: dan 
 mempunyai pengaruh yang minimal setama wawancara penjualan. La tidak menekankan 
 untuk membangun hubmlgan yang lebih baik dengan konsumen juga tidak mencoba agar 
 terlebih dabulu kepada konsumen dan terjuatnya produk itu tergantung produk itu sendiri 
 dan mau tidaknya konsumen melakukan tindakan pembetian.. 
 Pacia gaya menjual (5,5), yang menjadi pus at, berisi jwnlah menengah dari 
 kedua macam perhatian. Wiraniaga menu~jukkan perhatian terhadap konsumen dengan 
 mencoba menambahkan suatu cita rasa sosial yang menyenangkan dalam cara ketjanya. 
 Dalarn prak1eknya., ia cenderung tidak ingin mencapai prestasi menjual yang luar biasa., 
 dan tidak ingin menjalin hubungan yang tedalu akrab dengan konsumen. Menurut 
 wiraniaga (.5,5), bagaimanapun kuatnya seseorang berusaha mencapai prestasi (volwne 
 pef~ualan) yang tinggi, ia tidak akan dapat memuaskan sernua orang. Ia menetapkan 
 sasaran penjualan yang menumtnya dapat tercapai dengan menggunakan teknik menjual 
 yang sudah terbukti kebenarannya. 
 Yang terakhir, gaya menjual (9,9), merupakan perpaduan dari perbatian yang 
 sarna-sarna tinggi baik terhadap penjualan dan konsumen. Wiraniaga mengungkapkan 
 perhatiannya terhadap konsumen melalui beberapa pertimbangan seperti: Dapatkah 
 produknya itu betui-betui berguna untuk konsumen? Keuntungan apa yang mungkin 
 diperoleh oleh konsumen? Akankah konswnen mengurangi pengeluarannya dengan 
 menggunakan produk ini? Apakah produk itu mempooyai beberapa kekurangan dari 
 sudut pandang konsumen ? Dengan kata lain, wiraniaga berkonsultasi dengan konsumen 
 situasinya sehingga produk yang ditawarkan dapat memuaskan kebutuhan konsumen. 
 Us aha wiraniaga mengarah pada suatu keputusan pembelian dari konsumen, keputusan 
 pembelian yang memberi keuntungan bagi konsumen seperti yang diharapkan oleh 
 konsumen itu sendiri_ Perhatian yang tinggi terhadap penjualan yang dimiliki oleh 
 wiraniaga (9,9) tedihat jelas pada pengetahuan produk yang seksama yang dimilikinya, 
 dimana ia mampu berhubungan secara meyakinkan dengan kebutuhan konsumen selama 
 Penggunaan suatu tipe gaya menjual tergantung pada derajat perhatian yang 
 dimitiki tiap wiraniaga. Derajat perhatian dipengaruhi oleh faktor internal clan faktor 
 eksternal wiraniaga. Faktot- internal adalah taktor yang ada pada diri wiraniaga 
 meiiputi kebutuhan yang ingin dipenuhi, pengetahuan akan produk, ketrampilan dalam 
 menjual produk, pengalaman terdahulu dalam menjual produk, usia, jenis kelamin, 
 pendidikan, masa kerja, tingkatan atan level wiraniaga clan status marital. Faktor 
 eksternal adalah faktor yang berasal dari luar diri wiraniaga meliputi target penjualan 
 yang harns dicapai, strategi penjualan yang ditetapkan, kebijakan dalam hat insentif: 
 karalderistik konsumen, kebutuhan yang ingin dicapai konsumen, situasi pada saat 
 proses penjualan berlangsung dan ikiim persaingan dengan perusahaan lain yang 
 wiraniaga dan konsumen. Masing-masing wiraniaga dan konsumen mengembangkan 
 suatu strategi yang bet~juan untuk merundingkan pertukaran yang menguntungkan. 
 St.rategi ini didasar-kan pacia kebutuharl (needs) mereka masing-masing. Wiraniaga 
 menginginkarl agar- konsunlen meiai-..ukan tindakan pembelian terhadap produk yang 
 ditavvar-kannya, seciarlgkan konsumen memiliki berbagai kebutuhall yang mungkin dapat 
 dipenuhi meiaiui proses pembeiian tersebut. Menurut Schiffman dan Kanuk (1991), 
 salah sam alasan utarna vano mendorong konsumen untuk melaJ.mkan tindakan 
 adaiah adanya kebutuhan yang ingin dipenuhi. Beberapa kebutuharl merupakan bawaan 
 (znnate) dan bersifat t1sioiogis; kebutuhan lain adalah kebutuhan yang dipelajari ketika 
 berespon terhadap kebudayaan atau lingkungall dan bersifat psikologis, seperti 
 kebutuhan akan harga diri, prestise, ateksi, kekuasaan, clan lain-lain. Kebutuhan- 
 kebutuhan ini diharapkan akan terpenuhi melaIui produk yang ditawarkan dan melalui 
 interaksi yang terjadi antara konsumen dengan wiraniaga selama proses penjualan. 
 Wiraniaga yang betperan sebagai komunikator dalam proses penjualan, adalah 
 pihak yang berinisiatif memenuhi baik kebutuhan dirinya dan kebutuhan konsumen. 
 Wiraniaga berusaha memenuhi kebutuhannya melalui aktivitas penjualan, sedangkan 
 kebutuhan konsumen diharapkan dapat terpenuhi melalui produk yang ditawarkan clan 
 relasi yang terjaiin antara wiraniaga dan konswnen. Berdasarkan teori gaya menjual, 
 pet \'1JJudan ak1:ivitas penjuatan dan pemenuhan kebutuhan konsumen tersebut didasari 
 oteh adanya perhatian terhadap penjuatan dan perhatian terhadap konsumen. Dengan 
 demikian dapat disimputkan bahwa keberhasilan pel~ualan akan diperoleh oleh 
 vviraniaga yang memiiiki baik perhatian terhadap penjuatan maupun perhatian terhadap 
 konsumen_ Kedua perhatian ini dimiliki oteh wiraniaga dengan gaya menjual (9,9) atau 
 0), sehing.ga diperkirakan bahwa wiraniaga dengan gaya menjual tersebut akan lebih 
 bet-hasil met~ual dibandinoakan dengan wiraniaga dengan gaya mel~iuallain. 
 Kesimpulan diatas juga sejalan dengan hasil penetitian Blake dan Moutoll 
 (970)- Berdasarkan hasil penetitian tersebut didapatkan bahwa 61 persen dari 
 kesuksesan penjualan diiakukan oleh wiraniaga yang dipandang oleh konsumen 
 memihki gaya menjual (9,9). Gaya menjual yang paling eteldifberikutnya adatah gaya 
 menjuat (5,5). Ketika sam dari dua wiraniaga yang bersaing dipandang memiliki gaya 
 m~juat (9,9) sedangkan yang lain dipandang tidak memiliki gaya menjual tersebut, 
 dalam hal ini ternyata wiraniaga bertipe gaya menjuat (9,9) melakukan penjualan 
 sebesar 94 persen. Hat ini memberi kesan bahwa gaya menjuat - gaya menjual lain 
 ditoiak ketika mereka dibandingkan dengan solusi penjualan, yaitu gaya menjual (9,9). 
 Keberhasilan pekerja dan pengukuran terhadap keberhasilan berhubungan 
 dengan jenis pekerjaan itu sendiri. Berdasarkan pembagian jenis pekerjaan dari Schultz 
 (19731, maka pekerjaan wiraniaga diklasitlkasikan kedalanl jenis production job. 
 Keberhasilan production Job didasarkan atas segi kuantitas yang mempakan standard 
 yang obyek"tlf dari hasil pekerjaan. Hal ini berarti keberhasilan wiraniaga dapat diukur 
 melalui kuantitas penjualan yang dilat..llkannya. 
 Kebethasilan seseonll1g dalam melaksanakan pekel:.iaannya diartikan oleh 
 Maler (1965) sebagaiJob peljormrJnce. Pengertian prestasi kerja berkaitan denganjob 
 menjelaskan bahwa dalam mashab ,!;-ehaviorat, yang dimaksud dellgan prestasi adalah : 
 Segata tingkah 1at..1l yang diarahkan ulltuk pellcapaian pellerimaan atau penghilldaran 
 keunggulan diberlaimkan. Berdasarkan pengertian prestasi diatas, maka penilaian 
 prestasi ket:.ia seiaiu berhubungan dengan adanya standard keunggulan yang akan 
 dibandingkan dengan hasil kerja seorang pekerja. Pada penilaian prestasi kerja seorang 
 vviraniaga, hasil pe~iualan wiraniaga akan dibandingkan dengan standard jumlah 
 penjualan tertentu. Bila wiraniaga mampu menjual produk melebihi standard tersebut 
 maka wiraniaga itu dinilai berprestasi. 
 Blake dan Mouton (1970) mengemukakan faktor-faktor lain selain gaya 
 menjuai yang menentukan keberhasilan penjualan seperti produk itu sendiri, harga 
 prociuk, produk pesaing, kondisi ekonomi yang berlaku, dan keadaan finansial 
 Konsnmen. 
 r ------- 
 ;" 
 ! : .a ~ : 
 ~~ : ::I g ; 
 , 
 Asumsi: 
   Keberhaslian penjuatan ditentukan oleh gaya menjual tertentu. 
 Gaya menjual (9,9) dan (5,5) adalah gaya menjuai yang paling efektifuntuk mencapai 
 keherhasitan penjuatan. 
 Berdasarkan kerangka pikir di atas maka dapat dikemukakan hipotesis penelitian 
 Terdapat perbedaan gaya menjuai antara wiraniaga yang berprestasi kerja tinggi dengan 
 wirani3ga yang berprestasi kerja rendah di PT. 'P' - Bandung. 
 1. 7 Me[ode PeneJitian 
 Rancangan peneiitian yang digunakan adalah rancangan penelitian jenis non- 
 experirnental ex-post facto two-group designed (comparative studies) (Woodworth, 
 1976). 
 9430049-2.pdf
 Alat uk'Ut' yang digunakan untuk mengul..'Ut' gaya menjual adalah berasal dari Robert 
 R. Blake clan Jane Srygley Mouton, sedangkan pengelompokkan terhadap wiraniaga yang 
 berprestasi ke~ja tinggi dengan winmiaga yang berprestasi kerja rendah ditentukan 
 berdasarkan pada kriteria tingkat pre stasi wiraniaga sesuai ketentuan dari pihak 
 Berdasarkan kar-ald:enstik yang telah ditentukan sebelumnya oleh peneliti maka 
 diperoieh sejumiah subyek yang terbagi kedaiam kelompok wiraniaga yang betprestasi 
 ketja tinggi dan keiompok wiraniaga yang berprestasi kerja rendah. Kedua kelompok 
 Data yang diperoleh nanti akan diolah dan dianalisis didasarkan pada uji statistik 
 chi-kuadrat Metode penelitian yang digunakan adalah metode kompar"atif untuk melihat 
 bagaimana perbandingan gaya menjual antar"a kedua kelompok populasi sasaran, daD 
 metode deskriptifuntuk menggambarkan hasil penelitian mi. . 
 i.8 Lokasi dan Waktu Penelitian 
 Penelitian ini dilat..LJkan di PT. 'P' - Bandung. Waktu yang dibutuhkan untuk 
 penelitian ini adalah l..'Ut'ang lebih 12 bulan. 
2.:it' en~ei'ti3u P eniu3hm
'='
Penjualan diat1ikan oleh Prof. DR. Willardi, SE. (1989) sebagai sebuah proses
dlm~ma kebntuhan pembeli clan penjual dipenuhi melalui pertukaran intormasi dan
kepentingan Pengettian tersebut didasari detlnisi pe~iualan yang dikemukakan oleh
Paul Preston clanRalph Nelson dalam buklJl1yayang berjudul "'Salesmanship" sebagai
"PenJ1Jaianberat1i berkumpulnya seorang pembeli dan seorang penjual dengan ~iuan
melaksanakan tukar menukar barang dan jasa berdasarkan pertimbangan yang berharga
sepet1i pertimban,gan uang_"
Selanjutnya ditambahkan oleh Prof. DR. Wmardi, SE. (1989) bahwa penjualan
juga merupakan proses identitlkasi dan pemuasan kebutuhan dengan produk barang atau
jasa. Pet~ualan berat1i pula membantu seseorang baik secara langsung maupun tidak
langsung untuk bertindak secara positif terhadap sebuah ide, saran atau konsep.
meyakinkan orang lain untuk melak'llkansuatu tindakan yang disarankannya., berarti ia
telah melalmkanupaya penjualan.
Carlton A. Pederson C.S. (1981), mendetlnisikan penjualan sebagai :
";::;eUzn'i? shouLd be de j lned as the lJrocess whereby the seLLer ascertains , activates,U
and satisfies the needs or wants of the buyer to the mutual, continuous benefit of
tJoth the buver and the seLLer_ "
Pe~jualan dianikan sebagai proses dimana penjual mengetahui dengan pasti,
meneak1:ifkan. clan. memuaskan keinginan atau kebutuhan dari oembeli melalui
W.' __~
.I.
keuntunganyang sating berkesinanlbungan baik bagi pembeIi maupUtlpenjual.
Dalarn memahami arti penJualan, orang sering terpaku pada istilah personal
selling (penjuaian perorangan) dan istilah salesmanship (seW menjual), dan seringkali
keduanva dianeeao memiliki oeneertian vane sarna. Pada dasamva kedua keeiatan
J",-,
tersebut mempakan bidang studi yang tercak-updahlin penjualan.
Menumt I.E. Stroh (1966) salesmanship merupakan upaya yang muncul dari
pihak penjuai bempa penyediaan informasi bagi para calon pembeli prospektif yang
memotivasi atau mempersuasi mereka untuk mengambil keputusan-keputusan pembelian
yang menguntungkanbagi produk barang ataujasa pihak penjual.
PersonaL seihng atau pe~iuaian perorangan merupakan salah satu bagian
pentmg daiam proses penjuaian. Penjualan perorangan merupakan salah satu metode
promosi, seiain pet~uaian massal dan promosi p~iualan. Berikut ini dikenmkakan
beberapa uraian dan teori mengenai pet~ualan perorangan agar dapat diperoleh konsep
yang jelas mengenai bidang pemasaran produk yang dilaJ..-ukansecara "direct"
(langsung) daiam arti seorang tenaga pemasaran atau p~iual berhadapan langSUltg
dengan kom;umen~'~l:)ersonto person" atau "face to face" pada saat memasarkan
oroduk keoada konSUlnen.
. .
Penjuaian perorangan oieh T.F. Stroh (1966) didefinisikan sebagai :
"jt is direct, face to jace, seLLerto buyer influence which can communicate the facts
necessary for making a buying decision; or it can uhLize the psychology of
persuasion to encourage the jormahon ofa buying decision."
Komite van~ memmuskan dettnisi-detlnisi dari The American MarketingJ
'-'
Association merumuskan p~iualan perseorangan sebagai :
..- the personaL or imJ.:tJersonaLprocess in assisting and / or persuading a
prospectlvP- custorner to buy a commodity or a service or to act favorably upon an
idea that lias conunercwL Si9;II ,tlcance to the seLLer."
'-"
.
Dari kedua detlnisi diatas, dapat ditarik kesimpulan bahwa pe~iualan
perorangan merupakan salah satu cara yang digunakan oleh penjual untuk
menyampalkan pesan komersiai suatu produk barang atau jasa kepada konsumen secara
iangsung meiaiui kegiatan tatap muka dengan tujuan mempengaruhi pandangall dan sikap
konsumen sehingga memenuhi kebutuhan penjual dan konsumen, dimana melalui
penjuaian tersebut dapat ditarik keuntunganbagi penjual maupun kOtlSumen.
Untuk melakukan penjualan perorangan, seorang penjual memerlukan sejumlah
persyaratan tenentu. Pendapat P.J. MicaH (1972) berikut ini menegaskan persyaratan
vang dibutuhkan :
J _~
"8alesman must aa)ust their personalities on every caLL,making sure that what they
.
-
. . ..
'".
"
say and do IS compatibLe with the ..Dersonaiity oJ each proSPect.
Prof. DR. Winardi, SE (1989) mengemukakan bahwa dalam kegiatan
pemasaran produk, dijalankan "Ramuan Promosi" (The PromotionaL BLend) yang
terdiri dari pengiklanan (advertising), penjualan perorangan (personal selling) dan
pubiisitas (publicity). Penjualan perorangan merupakan elemen yang paling efektif
dalam kegiatan pemasaran bila dibandingkan dengan elemen lainnya karena pe~ualan
perorangan mempunyai tiga keuntunganyaitu :
1. Konfi-ontasi perorangan
Penjuaian perorangan meiiputi hubungan yang hidup, langsung serta interaktif antara
dua orang / iebih, dimana masing-masing pihak dapat mengamati kebutuhan dengan
eiri masing-masing serta mengadakan penyesuaian dengan segera.
Pet~uaian perorangan memungkinkan tumbulmya segala macam hubungan yang
berkisar pada hubunganpenjualan hingga kadang terbentuk ikatan pribadi yang lebih
daiam.
3. Reaksi
P~iuaian perorangan menyebabkan calon pembeli merasa telah mendengar uraian-
uraian dari pihak penjuaL Dengan demikian kebutuhan calon pembeli untuk bereaksi
akan lebih besar, sekalipun reaksinya hanya berupa ucapan terima kasih.
Telah disebutkan bahwa sering terjadi penyamaan antara istilah "personal
~eWng" (penjuaian perorangan) dengan "salesmanship" (seni me~ual)_ Pada sub bab
sebetumnya telah diuraikan pengertian salesmanship dan pengertian personaL seLLing,
setanjutnya pada sub bab ini akan dijeiaskan hubungan antara kedua pengertian tersebut.
Menurut Richard R. Still (1981), personaL seLLing merupakan konsep yang
teblh tuas dibandingkan dengan konsep salesmanship_ PersonaL seLLing secara luas
ditaksanakan metaiui saLesmanship_ Behau mengemukakan pengertian mengenai
r>ersona{ seiling dan saiennanshiv sebagai berik1Jt :
.i.. ~ :<
..i. L~
"Salennanshw IS one a.meet o(versonal seUing- it is never aU ont."
"' ~
'"
Daiam rangka usaha member-ikan kejelasan, selanjutnya dikenmkakan oleh
beliau: "Bersama-sama dengan etemen-elemen pokok pemasaran lainnya seperti
misatnya penetapan harga, periklanan, pengembangan serta riset produk, saluran
pemasaran, dan distribusi tlsik produk, personaL seLLing mempakan sarana yang
digunakan untuk melaksanakan program-program pemasaran. Secara lebih tepat,
.5'.alesmans.hipadalah salah satu keahlian yang digunakan dalam personaL seLLing;
seperti yang didetlnisikan oieh I. J. Shapiro, saLesmanship adalah seni agar berhasil
meyakinkan calon konsumen atau konsumen untuk membeli produk atau jasa dimana
konsumen dapat memperoleh keuntungan yang sesuai dari produk tersebut, dengan
demikian meningkatkan kepuasan total merek3-" Suatu sam, penekanan saLesmanship
hampir seturuhnya pada meyakinkan (persuasion); saat ini, ketika mengenali arti
membujuk, penekanan berpindah pada keuntungan yang menarik bagi calon konsumen
dan konsumen.
Prof. DR. Willardi~ SE. (1989) menjelaskan bahwa salesmanship merupakan
saiah satu keterampiian yang digunakan dalam bidang pel~ualan perorangan.
2..2 KOllsep Penjualall dan Konsep Pemasaran
Konsep penjuaian dan konsep pemasaran seringkali dicampuradukkan.
Theodore Levitt (i960) membedakan kedua konsep tersebut sebagai berikut :
"Konsep pe~uaian menitikberatkan pada kebutuhan penjual~ konsep pemasaran
menitikberatkan pada kebutuhan pembeli."
Untuk memberikan kejelasan mengenai pengertian kedua konsep diatas, Levitt
mefiJelaskan perbedaan antara konsep penjualan dan konsep pemasaran berdasarkan
tiga hal, yaitu : tof.."Us,media dan sasaran.
Fokus perhatian penjual pada konsep penjualan adalah produk, sedangkan pada konsep
pemasaran adalah kebutuhan konsumen. Media yang digunakan untuk mencapai sasaran
pada konsep penjualan adalah penjualan perorangan dan promosi, sedangkan pada
konsep pemasaran digunakan marketing mix (bauran pemasaran). Sasaran penjual pada
konsep penjualan adalah mencari laba melalui volume penjualan, sedangkan pada
konsep pemasaran adalah mencari iaba metatui kepuasan konsumen.
2.:tl PengernaD PemasaraD Aftdtil~'~l
Pemasat.an rnuiriieveL (AluitiLevel Al(:1rketing atau Neh1'ork lvkuketing) adalah
suatu cara atau metode me~iual baratlg secara langsung kepada pelanggatl melalui
jarmgan yang dikembangkan oleh para distributor tepas yang memperkenalkatl para
distnbutor beril1Jtnya; pendapatan dihasitkan terdiri dari laba eceran dan laba grosir
ditambah dengan pembayaran-pembayaratl berdasarkan penjualan total kelompok yang
dibentuk oleh sebuah di&1ributor.
Pemasaran multilevel adatah salah satu dari banyak metode memindahkan
produk dari pabrik/pembuat ke petanggan eceran. Tapi prinsip dasar pemasaran
multilevel iaiah bahwa armada penjuai selengkapnya dikembangkan oleh tenaga penjual
itu sendiri. Mereka yang berusahapaling keras dalam kegiatan ini akan mencapai
tingkat paling tinggi dan dengan demikian menerima imbalan paling besar.
Pemasaran multilevel memanfaatkan apa yang dikenal sebagai "penghasilan
multipieks". In; mempakatl salah satu bentuk penghasilatl residual, karena penghasilan
tersebut memberikan penghasilan-penghasilan sekarang yang berdasarkan pada usaha-
usaha masa 1ampau. Selain itu, penghasilan diberikan dalam hubungannya dengan
us aha-us aha orang lain.
2A Teon ten tang Penjuai
Kunci keberhasilan suatu produk terletak pada kemampuan pe~iual dalam
menJelaskan sifat serta keuntungan yang diberikan produk, serta kepekaan pe~iua1 akan
kebutuhan konsumen dan menyesuaikatUlyadengan sifat-sifat produk.
Pada masa sekarang, pel~iuai dianggap sebagai seorang wakil perusahaan yang
bertanggungjawab untukmemberikan keterangan lengkap kepada para caton koru,--umen,
bukan hanva sekedar memindahkan oroduk ke tan!!an konsumen. Bukti oerubahan
tersebut adalah dengan munculnya istilah lain dari penjua1 yang sudah sering digunakan
seperti field manager, rnarket specialist, sales representative (sales person), saLes
agent, dan sales engIneer. Tugas mereka tidak hanya sebatas mengkomunikasikan
segaia sesuatu yang berkaitan dengan produk perusahaan kepada para konswnen, tetapi
mereka diharapkan dapat memberi masukan (feed back) mengenai reaksi para pembeli
kepada perusahaan.
Kadang mereka diharapkan menjadi manager bidang pemasaran pada wilayah
kerJa mereka. Daiam kenyataan beberapa diantara dapat menjadi marketing manager
oieh karena manager mereka atau top management telah memberikan petunjuk-petunJuk
strategis yang iengkap kepada mereka. Daiam kondisi ini mereka harns mengisi kegiatan
pet~iuaian dengan mengembangkan ma rketing mix -nya sendiri dan bahkan kadang-
kadang menerapkan strateginya sendiri.
Saiah sam kesuiitan yang dijumpai adalah bahwa setiap situasi penjualan, dalam
hai ini situasi oeniuaian oerorangan. adaiah berbeda. Untuk memahami betul sitat dan
.i. _I ..
"_'.
tingkat perbedaan tersebut, adaiah sangat membantu untuk membedakan antar-a dua
macam pe~ualan-service seilmg (pet~iuaian pelayanan) dan deveLopmental selling
(penjuaian berkei~iutan)_ Kedua macam penjualan ini memiliki tujuan yang berbeda.
Service selling be~iuan untuk memperoleh penjualan dari pelanggan yang ada yang
memiliki kebiasaan dan poia pemikiran yang sudah menghasilkan penjualan seper1i itu.
Developmental selling tidak bertujuan untuk membuat penjualan yang segera sebanyak
seperti tujuannya untuk merubah calon konswnen menjadi pelanggan. Dengan kata lain,
deveLopmental selling mencoba menciptakan pelanggan dari orang-orang yang sekarang
ini memandang perusahaan wiraniaga sebagai perusahaan yang tidak baik, clan yang
mungkin bertahan untukmerubah sumbet.persediaan saat ini.
McMUJTy dan Arnold ( da1am Richard R. Still, 1981) menjelaskan bahwa
perbedaan posisi penjuat memerlukan jumlah dan macam service selling dan
developmental seLling yang berbeda, dan mengklasifikasikan posisi pada suatu
speh1rum yang berkisar dari posisi yang sangat sederhana hingga posisi yang sangat
kompleks. IvIerekamengkategorikan posisi penjual kedalam tiga kelompok, yaitu :
Kelompok A (dasarnya adatah service selling)
1. 1nSIde Order Taker-terutama "melayani" pelanggan; contohnya, pramuniaga yang
berada di belakan~ meia t)aian~an dasi.
.~. _. J. _8 ~.
2.. DeUver.y Saie~uerson-terutama sibuk mengantarkan produk; contohnya, pengantar
susu, roti, atau bahan bakar.
3. Route atau ivferchandising SCliesperson-terutama adalah seorang pengambil
pesanan (order taker) tetapi bekerja di iapangan-khususnya seperti wiraniaga yang
memasarkan sabun, yang pekerjaannya menghubungipenjual eceran
4. The MIssionary .saLesperson-posisi dimana wiraniaga tidak diharapkan atau tidak
diijinkan untuk mengambil pesanan tetapi tujuannya hanya membangun goodwill
atau mendidik pengguna aktual atau pengguna potensial, contolUlya,"missionary"
5. Jhe Tedmical Salesperson-penekanan temtama pada pengetahuan teknik,
contoimya wiraniaga keahlian teknik yang temtama menjadi konsultan bagi "klien"
.
Kelompok B (dasarnya adaiah developmental seiling)
0_ Creatiw ;Xlies..verson of Tangibles-contoimya, wiraniaga yang menjual alat
I. C'r>zatr\;',zSca,z;,7: Derson of Intangibles-contohnya wlranlaga yang menjual polis
asunm~;i j3~;3 periklanan, dan program pendidikan.
Kejompok C (dasarnya adalah developrnental selLing. tetapi memerlukan tingkat
kt-eativitas yang tmggi)
8. The "PoliticaL" Indirect, atau "Back-door!} Salesperson-melibatkan penjualan
produk yang tuntutannya tinggi, khususnya barang yang benar -benar tidak memiliki
pesaing. Pe~1ua1an terutama dilaksanakan mela1ui pemberian pelayanan yang
bersifat sangat pribadi (yang memiliki sedikit atau tidak memiliki hubungan dengan
produk) kepada kunci pengambil keputusan pada organisasi pelanggan. Contohnya
\viraniaga yang mendapat pesanan tepoog yang besar dari pemsahaan roti dengan
memenuhi minat pembeli terhadap kegiatan memancing dan main golf
Y. The Salesperson Engaged in Multiple Sales-melibatkan penjualan produk yang
tuntutannya tinggi dimana wiraniaga hams mengadak:anpresentasi ootuk beberapa
individu pada organisasi pe1anggan, biasanya suatu komite, hanya seseorang yang
dapat mengatakan "ya" , tetapi semuanya dapat mengatakan "tidak". Contohnya,
ekse1anif keuangan dari suatu agen perik1anan yang mengadakan presentasi untuk
"komite penseleksi agen" dari pihak pengik1an.Bahkan sete1ah pertanggungjawaban
diperoleh, wiraniaga umumnya hams bekerja berke1anjutan untuk memelihara hal
Semakin dipedukannya develo..Dmentalseiling dalam suatu posisi penjual yang
khlli.LJSclan semakin kompieks developmental seiling tersebut, semakin sulit penjual
tersehut melakukan penjualan. Jumlah dan macam deveLopmentaL seiling yang
cliperlukantergantung pada sifat ca10nkonsumen dan pelanggan pada satu sisi, dan sifat
produk, pada sisi lain. Pel~uaian yang termudah adalah pet~ualan seLf-service, yang
mana McMmTy clan Arnold mengabaikannya dari spel..1rummereka. Pel~ualan yang
paling sullt adaiah penjuaian yang me1ibatkan kombinasi dat.i deveLopmentaL seiling
dan suatu tingkat kreativitas yang tinggi-ketika kadang-kadang p~jualan hams
diiakukan pada beberapa dasar lain selain keuntungan produk, atau penjualan
""mulhpie" harns cliiakukan untuk mendapatkan pesanan, dan dimana usaha yang
berke1anjutan dibutuhkan untuk menyimpan pertanggungjawaban.
Wiramaga adatah orang yang melakukan promosi melalui sarana pt.omosl
pel~uatan perorangan (personaL seLLing).Sarana promosi adalah sarana yang dilakukan
organisasi untuk mengkomunikasikan tentang segala sesuatu terutama produk organisasi
Kepada masyarakat konsurnen. Promosi ditat.mkan dengan harapan akan membuat
masyarakat konsumen tertarik dan yakin terhadap produk yang ditawarkan kepadanya,
:.:ehingga mereka met<ik"Ukantindakan pembelian. Demikian pula halnya dengan
penjuatan perorangan wiraniaga, hanya saja bentuk komunikasi yang tetjadi adalah
komunikasi personal yang dit<ik"Dkansecara tangsung orang per orang, clan tatap muka,
sedangkan komunikasi sarana promosi lainnya bersifht impersonal dan massal. Setiap
organisasi biasanya memiiih sarana promosi pel~ualan perorangan sebagai pilihan
utama daiam menjaiankan strategi pemasarannya, atau paling tidak menyertakan
penjuaian perorangan sebagai salah satu sarana promosi, Pertimbangan dipilihnya
pe~1uatan perorangan karena sarana promosi tersebut memiliki beberapa kelebihan
dibandingkan dengan sarana promosi lainnya, Pertama, hubungan yang ada lebih
"hidup", iangsung dan interaktif Artinya, karena hubungan yang terjadi dengan
konsumennya bersifat iangsung clan tatap muka, maka wiraniaga dapat menciptakan
interaksi yang "hidup" clan interaktif melalui pertanyaan, pemyataan dan humor yang
dilontat'kan wiraniaga selama berinteraksi. Kedua, dapat terciptanya keakraban, artinya
wiraniaga dapat membina hubungan yang lebih erat, bersahabat dan menyenangkan
sphingga konsumen tidak akan beralih ke pesaing. Ketiga, adanya tanggapan yang lebih
bes3r dat.i kom:umen, yang diperoleh dengan memperhatikan reaksi-reaksi yang
diberikan konsumen selama interaksi. Dan kelebihanllya adalah penjualan pet.orangan
tangsung di~iukan kepada calon yang paling prospektif sehingga memperkecil waktu
dan tenaga yang terbuang. Pe~iualan perorangan di~iukan pada penutupan pe~iualan.
Adanya kelebihan-kelebihan ini akan membuat wiraniaga lebih mampu mengetahui
- -
kebutuhan dan karakieristik konsumen, sehingga ia bisa melakukan penyesuaian
penjualannya. Jadi ~iuan akhir dari promosi yang dilakukan seorang wiraniaga adalah
terjadmya tindakan pembelian oleh konsumen terhadap produk organisasi yang
ditawat-kannV3.
.;
Unmkmenjalankan fungsi promosinya dengan baik, seorang wiraniaga biasanya
melak-ukansam atau beberapa aktivitas seperti di bawah ini :
. Mempresentasikan dan mendemonstrasikan produk serta menutup penjualan.
. Mel~elaskan kebijaksanaan pemsahaan
. Melaporkan keadaan pasar dan mencari pasar bam
. Mempertahankan hubungan baik dengan konsumen
. Menan!?anikeluhan konsumen
'.J
. Memperkenalkan produk bam
Dari uralan di atas, jetaslah bahwa tugas seorang wlranlaga memiliki kekhasan
tersendiri dibandingkan dengan pekerjaan yang lain, sehingga tuntutan terhadap
karah'teristik vviraniaga pun memiliki kekhasan. KekhasatUlya yang pertama adalah
wlraniaga me\vakiii perusahaannya untuk berhubungan dengan lingkungannya, sehingga
opmJ mas'Y3.t"akatkonsumen tentang perusahaatI datI produknya seringkali terbentuk oleh
kef~atl van~ diting!!aikan vvirania!!a seiatna berinteraksi den!!an konswnen. Kedua.
-".~
pengawaf~3t1terhadap togas YatIgdiiakukan WiratIiaga sangat keeiI, sehingga wiraniaga
(htuntutmemiiiki kreativitas dan inisiatifyatIg ting,gi.Ketiga, WiratIiaga harns memiliki
ketrampiiatI bersosiahsasi untuk berinteraksi dengan konswnen. Dan keernpat, stress
mental clan tuntutan peketjaan mempersyaratkatI wiraniaga dengan stamina fisik yang
tinggi" Ditinjau dari produk yang dijual wiraniaga, maka dapat dibedakan menjadi
wiraniaga barang dan wirat1iagajasa. Wiraniaga barang adalah seorang wiraniaga yang
rnenjuai suatu produk nyata (bervv1Jjud), dapat dilihat atau disentuh, sedangkan
wiraniaga jasa adalah seorang yang menjual suatu produk yang abstrak (tidak
be~jud), tidak dapat dilihat atau disentIJh,tetapi dapat dirasakan.
Britt 0966) dalam bukunva "Consumer Behavior & BehavioraL Sciences"
'"
mennltskafJ lJahwa daianI memutuskan barang atau jasa ekonomi apa yang akan dibe1i
atau digunakan oleh seseorang, dipengaruhi oleh f3ktor-fak10r tertentu. Kotler (1997)
Juga mengungkapkan bahwa 1atar belakang dan karakteristik seseorang akan mewamai
tingkah 13k'llmembelinya. Latar belakang dan karakteristik seseorang tersebut mencakup
fah1or-fah1:or seba!?ai berikut :
"-~
Fai..1:orbudaya~ Y=1ituk'Ultur, subkuttur, dan kelas sosia1.
  Fai..1:orso~~iai:yaitu kelompok acuan, ketuarga, peran dan status.
Fai.'1orkepribadian; yaitu usia dan tahap siktus hidup, peketjaan, keadaan ekonol"l:li,
gaya hidup, kepribadian dan konsep diri.
  Fai.'torkejiwaan / psikotogis~ yaitu motivasi, persepsi, pembelajaran, keyakinan dan
sikao.
.
Daiam buklJnya 'Cbnsumer Behavior', Schiffman dan Kanuk menyatakan
bahwa setiap individu memitiki kebutuhan : bebempa mempakan bawaan (innate),
yang lain adalah yang dipero1eh (acquired). Kebutuhan yang merupakan bawaan
bersifat t1siotogis (sepetti kebutuhan biologis); termasuk didalamnya kebutuhan akan
makanan, air, udara, pakaian, tempat berlindung, dan seks. Karena kebutuhan ini
dipertukan untuk memungkinkan kehidupan biologis, maka kebutuhan ini dianggap
merupakan kebutuhan atau motif primer.
Kebutuhan yang diperoteh (acquired) adalah kebutuhan yang kita pelajari dalam
resoon ter-harlankebudavaan atau tin!?lmn!?ankita. Kebutuhan van~ termasuk didalamnva
.I. .I. _, W W
-"
U .;
seperti kebutuhan akan har-ga diri, martabat (prestise), afeksi, kekuasaan, dan belajar.
Karena kebutuhan ini umumnya bersiiat psikologis, kebutuhan ini dianggap sebagai
kebutnhan atau motif sekunder_Kebutuhan ini merupakan hasil dari keadaan psikologis
subyek1:ifindiviclu clandari relasi dengan orang lain.
l.6 leori mengellai bueraksi all tara \Viraniaga dall Konsumen
Sitat dari interaksi yang terjadi antara konsumen dan wiraniaga menentukan
bagaimana reaksi konsumen, baik reaksi yang sesuai dengan keinginan wiraniaga
ataupun tidak. Richard R. Still (1981) menunjukkan model konseptual hubungan
"wiraniaga-konsumen"_ Model ini, dikembangkan melalui suatu penelitian literatur yang
iuas, memandang proses pe~iualan dipengaruhi baik oleh wiraniaga dan konsumen, tiap
wlraruaga dan konsumen dipengaruhi oleh personal characteristics (karakteristik
pribadi) dan roie requirements (persyaratan peran). Personal characteristics
mencaknp kepribadian, sistim nilai, sikap, pengalaman masa lampau, dan lain-lain.
Kumpulan role requirernents (sebagai contoh, otoritas formal dan otonomi organisasi)
berinteraksi dengan personal characteristics Wltukmembentuk needs (kebutuhan) dan
 -q>02ctations(harapan). Persepsi baik terhadap kebutuhan diri sendiri maupun orang
lain dapat mengarah pada penyes,'uatan kebutuhan mereka masing-masing (lihat
mekanisme}?edback yang diwaki1i oleh garis pums-pums).
Berdasatkan needs dan expectations mereka masing-masing, setiap wiraniaga
clan konsumen mengembangkan suam str-ategi yang bertujuan untuk menmdingkan
pertukaran yang menguntungkan. 

