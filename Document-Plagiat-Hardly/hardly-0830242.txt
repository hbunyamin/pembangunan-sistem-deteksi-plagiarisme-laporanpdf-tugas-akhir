Alat ukur ini memiliki nilai validitas berkisar antara 0,323-0,750 dan nilai reliabilitas 
pada dimensi Amount 0,645, Valency 0,698, Accuracy / Honesty 0,722, Intention 0,631 dan 
Intimacy 0,732. Pengolahan data menggunakan distribusi frekuensi program SPSS 20.0 for 
Windows. 
Berdasarkan hasil pengolahan data secara statistik, didapatkan sebanyak 89,7% 
responden memiliki Amount yang terbuka dan 10,3% memiliki Amount yang tidak terbuka. 
90,8% responden memiliki Valency yang terbuka dan 9,2% memiliki Valency yang tidak 
terbuka. 97,1% responden memiliki Accuracy / Honesty yang terbuka dan 2,9% memiliki 
Accuracy / Honesty yang tidak terbuka. 97,7% responden memiliki Intention yang terbuka 
dan 2,3% memiliki Intention yang tidak terbuka. 89,1% responden memiliki Intimacy yang 
terbuka dan 10,9% memiliki Intimacy yang tidak terbuka. 
Bagi peneliti selanjutnya, peneliti menyarankan agar memperluas penelitian dengan 
menambahkan faktor-faktor yang mempengaruhi Self Disclosure seperti kepribadian. Bagi 
peneliti selanjutnya peneliti menyarankan untuk memperluas cangkupan penelitian dengan 
meneliti sampel penelitian pada jurusan yang lebih variatif, sehingga dalam penelitian 
selanjutnya hasil yang diperoleh dapat dilihat lebih signifikan mengenai gambaran self 
Disclosure. 
The research was conducted to determine of Self Disclosure the image Self Disclosure 
in first-year student in Psychology Faculty of University "X" Bandung. The design used is 
descriptive research design. Sample in this study was 174 people. 
Measuring instrument used was a questionnaire Self Disclosure that has been 
modified by the researchers based on the theory of Self Disclosure (Wheeless & Grotz, 1976) 
then modified by Tery Setiawan M.Sc. The questionnaire consists of 40 items based on the 
dimensions Self Disclosure which are Amount, Valence, Accuracy / Honesty, Intention and 
Intimacy. 
This instrument has a validity value ranged from 0.323 to 0.750 and the value of 
reliability on the dimensions Amount 0.645, 0.698 Valency, Accuracy / 0.722 Honesty, 
Intimacy Intention 0.631 and 0.732. Processing data using the frequency distribution of SPSS 
20.0 for Windows. 
Based on the results of statistical data processing, obtained as much as 89.7% of 
respondents have an open Amount and 10.3% have Amount that do not open. 90.8% of 
respondents have an open Valency and 9.2% have Valency that do not open. 97.1% of 
respondents have Accuracy / Honesty open and 2.9% have Accuracy / Honesty is not open. 
97.7% of respondents have an open Intention and 2.3% have Intention is not open. 89.1% of 
respondents have an open Intimacy and 10.9% have Intimacy is not open. 
For further research, the researchers suggested that expanding the research by 
adding factors that affect Self Disclosure as personality. For further research to expand 
cangkupan researchers suggest the study by examining the sample at the majors are more 
varied, so that in future research results obtained can be seen more significant about the 
picture of self Disclosure. 
 GAMBARAN RESPONDEN DAN DIMENSI SELF  
Kehidupan manusia tidak bisa lepas dari interaksi dengan manusia lainnya. Setiap 
manusia berinteraksi membutuhkan bantuan dalam menjalankan aktifitasnya karena manusia 
merupakan makhluk sosial.  Dalam pemenuhan kebutuhannya, manusia memerlukan adanya 
suatu interaksi dengan lingkungan sekitarnya. Proses interaksi salah satunya dengan adanya 
komunikasi, proses interaksi maupun komunikasi ini juga secara langsung menunjukkan 
eksistensi manusia. Informasi yang disampaikan dalam komunikasi dapat berupa identitas diri, 
pikiran, perasaan, penilaian terhadap keadaan sekitar, pengalaman masa lalu dan rencana 
masa depan yang sifatnya rahasia maupun yang tidak.  
Komunikasi adalah proses pertukaran informasi dengan menyampaikan gagasan atau 
perasaan agar mendapat tanggapan dari orang lain dan dapat mengekspresikan dirinya yang 
unik (Devito, 2012). Komunikasi digunakan untuk menciptakan hubungan yang harmonis, 
untuk menciptakan hubungan yang harmonis tersebut individu memerlukan kemampuan 
untuk menyesuaikan diri. Penyesuaian diri yaitu bagaimana individu dapat berada pada 
lingkungan sosial dan berinteraksi secara harmonis. Penyesuaian diri perlu dikembangkan 
dalam diri individu, untuk dapat mengembangkannya diperlukan keterampilan sosial sehingga 
dapat menunjang keberhasilan individu dalam berinteraksi  
Keterampilan sosial diharapkan dimiliki oleh individu yang berada pada tingkat 
perguruan tinggi. Perguruan tinggi merupakan jenjang lanjutan dari Sekolah Menengah Atas, 
ketika siswa mengalami transisi dari sekolah menengah atas menuju perguruan tinggi, siswa 
dihadapkan pada berbagai perubahan, siswa menghadapi fenomena yang teratas ke bawah 
(top-dog phenomenon), yaitu keadaan dimana siswa bergerak dari posisi yang paling atas (di 
sekolah menengah atas menjadi yang tertua, terbesar, dan yang paling berkuasa) menuju 
posisi yang paling rendah (di perguruan tinggi sekolah menjadi yang paling muda, dan paling 
tidak berkuasa). Oleh karena itu tahun pertama dapat dikatakan menjadi tahun yang sangat 
sulit bagi kebanyakan mahasiswa (Santrock, 2007 : 352).  
Sesuai dengan perkembangannya emerging adulthood (beranjak dewasa), pada masa 
ini mahasiswa dituntut lebih belajar menyesuaikan diri dengan lingkungan sosial yang lebih 
luas. Siswa yang baru saja menyelesaikan pendidikannya di tingkat sekolah menengah atas 
dan mulai memasuki jenjang pendidikan yang lebih tinggi di perguruan tinggi akan 
mengalami berbagai masalah yang dihadapi. Siswa dihadapkan pula pada perubahan sistem 
pengajaran, kurikulum serta hubungan antar mahasiswa dan dosen. Didalam lingkungan 
perkuliahan banyak dijumpai adanya komunikasi yang kurang efektif antara mahasiswa 
dengan dosen, dan mahasiswa dengan mahasiswa lainnya. Hal ini dapat dilihat dari gejala-
gejala seperti tidak dapat mengeluarkan pendapat, tidak mampu mengemukakan ide atau 
gagasan yang ada pada dirinya, merasa was-was atau takut jika hendak mengemukakan 
sesuatu (Gainau, 2009: 2).  
Mahasiswa tahun pertama diharapkan memiliki keterampilan sosial seperti 
keterampilan berinteraksi dengan orang lain dan keterampilan menyampaikan pendapat 
sehingga mampu berkomunikasi dengan tepat untuk menganalisa dan menciptakan sesuatu 
yang positif dan membangun hubungan dengan baik satu sama lain. Dengan adanya 
keragaman asal latar belakang yang ada diantara mahasiswa pada tahun pertama di perguruan 
tinggi menyebabkan mahasiswa belum saling mengenal dengan baik, sehingga perlu 
membuka diri agar dapat membina hubungan dengan teman baru. Diantara mahasiswa  yang 
dituntut untuk berhubungan dan berhadapan dengan orang banyak dalam pendidikan, 
kehidupan sehari-hari maupun pekerjaannya nanti adalah mahasiswa Fakultas Psikologi. 
Mahasiswa pada Fakultas Psikologi diharapkan memiliki keterampilan sosial untuk dapat 
menyesuaikan diri tidak dalam hal akademik saja namun dengan lingkungan sosialnya yang 
baru yang akan mendukung untuk kehidupan dan pekerjaannya nanti. Mahasiswa tahun 
pertama Fakultas Psikologi dituntut untuk dapat berkomunikasi dan mengekspresikan yang 
terdapat dalam dirinya maupun pendapat yang akan diutarakannya kepada sesama mahasiswa 
atau dosen, sehingga dengan tepat dapat menganalisa, mengevaluasi dan menciptakan sesuatu 
yang positif. Dunia perguruan tinggi menuntut mahasiswa untuk mampu melakukan 
komunikasi yang baik agar diterima secara sosial dengan membangun interaksi dengan 
lingkungan sosial tersebut sebagai modal untuk mencapai kesuksesan di masa perkuliahan dan 
lapangan kerja kelak.  
Kesulitan dalam keterampilan sosial dan penyesuaian diri yang perlu diatasi oleh 
mahasiswa tahun pertama dapat dibantu dengan adanya suatu pengungkapan diri. 
Pengungkapan diri atau Self Disclosure menurut Wheeless & Grotz (1976) adalah  pesan 
apapun tentang diri yang dikomunikasikan kepada orang lain. Self Disclosure sering 
digunakan dalam bidang yang berkaitan dengan manusia seperti komunikasi, sosiologi, 
psikologi, konseling dan psikoterapis (Darlega & Berg, 1987). 
Pengungkapan diri ini dapat berupa berbagai topik seperti informasi, perilaku, sikap, 
perasaan, keinginan, motivasi dan ide yang sesuai dan terdapat di dalam diri orang yang 
bersangkutan untuk membangun hubungan sosial dilingkungan yang baru (Barrett & 
Pietremonaco dalam Wei, Russel & Zakalik, 2005 h. 603-604). Pengungkapan diri seseorang 
tergantung pada situasi dan orang yang diajak untuk berinteraksi. Jika orang berinteraksi 
dengan menyenangkan dan membuat merasa aman serta dapat membangkitkan semangat 
maka kemungkinan bagi individu untuk lebih membuka diri amatlah besar, sebaliknya pada 
beberapa orang tertentu yang dapat saja menutup diri karena merasa kurang percaya kepada 
orang lain (Devito, 1997).  
 Self disclosure merupakan aspek penting dalam komunikasi interpersonal pada 
mahasiswa. Self Disclosure merupakan cara untuk mendapatkan dukungan dari orang lain 
dalam melewati masa penyesuaian diri, baik dengan lingkungan maupun penyesuaian dengan 
perubahan internal sebagai akibat perubahan dan perkembangan selama menjadi mahasiswa 
pada tahun pertama dalam masa perkuliahan. Pengungkapan diri pun membantu memberikan 
pemahaman yang lebih mendalam mengenai perilaku diri sendiri, penerimaan diri, dan 
mempererat hubungan dengan orang lain.     
Berdasarkan survey awal yang telah peneliti lakukan kepada 10 mahasiswa tahun 
pertama Fakultas Psikologi Universitas  X  Bandung dengan menggunanakan metode 
wawancara, diperoleh data 10 mahasiswa tahun pertama                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
(100%) mengatakan bahwa mereka mengalami kesulitan untuk menyesuaikan diri pada awal 
memasuki perkuliahan. Kesulitan tesebut didapat ketika terjadi perubahan dan perbedaan 
kurikulum maupun sistem pengajaran, sistem berbeda saat mereka masih di bangku SMA, 
melaksanakan dan menyelesaikan tugas perkuliahan yang mereka anggap begitu banyak dan 
memiliki waktu yang singkat dalam menyelesaikannya. Dalam menyesuaikan diri dengan 
teman baru dan lingkungan sosial pada tahun pertama ini 9 mahasiswa tahun pertama (90%) 
mengatakan tidak merasa sulit untuk menyesuaikan diri dengan teman-teman baru untuk 
dapat berkomunikasi dan bekerja sama dalam suatu kelompok untuk menyelesaikan tugas dan 
mampu mengeluarkan pendapat mereka pada saat melakukan diskusi. Mereka mengatakan 
bahwa mereka adalah orang yang cepat akrab. Sedangkan satu mahasiswa tahun pertama 
(10%) mengatakan sulit untuk menyesuiakan diri dengan teman-teman baru terutama saat 
awal memasuki masa perkuliahan dikarenakan mahasiswa baru yang beraneka ragam budaya 
sehingga ia tidak dapat menyesuikan diri dengan cepat dan merasa terhambat apabila sedang 
berdiskusi dan kurang mampu untuk dapat mengeluarkan pendapatnya. 
 Saat pertama kali memasuki lingkungan perkuliahan responden menyatakan sering 
melakukan pengungkapan diri mereka, mengenai perasaan, ide, pikiran dan informasi 
mengenai dirinya atau hal-hal yang belum diketahui orang lain sebelumnya. Diperoleh 8 
mahasiswa tahun pertama (80%)  mengatakan jarang untuk melakukan pengungkapan diri 
menceritakan mengenai diri sendiri kepada orang lain, membicarakan diri sendiri apabila 
sedang kesal, sedang mengalami masalah atau sedang kecewa maupun pengalaman-
pengalaman pribadi, sedangkan 2 mahasiswa tahun pertama (20%) mengatakan banyak 
melakukan pengungkapan menceritakan megenai diri sendiri menceritakan masalahnya dalam 
hal apapun dan memakan waktu yang cukup lama kepada teman-temannya dikarenakan 
mahasiswa tersebut senang berkumpul dengan teman-temannya, ia melakukan pengungkapan 
termasuk teman yang baru ia kenal bahkan dilingkungan yang baru ia jumpai.  
 Berdasarkan survey diperoleh 8 mahasiswa tahun pertama (80%) yang menceritakan 
dan melakukan pengungkapan diri bahwa mereka menceritakan hal-hal yang positif mengenai 
dirinya, ia menceritakan mengenai kegemarannya apa yang ia sukai, kejadian-kejadian yang 
menurutnya menyenangkan, sedangkan 2 mahasiswa tahun pertama (20%) tidak menceritakan 
hal yang positif mengenai dirinya namun mengenai hal yang biasa atau bersifat netral dan hal 
yang negatif terhadap orang lain, dalam hal ini misalnya mahasiswa membicarakan mengenai 
temannya. 
  

