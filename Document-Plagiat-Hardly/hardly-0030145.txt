Keempat, kebingungan mengenai peran, harapan terhadap peran
tersebut, nilai yag dianut, dan identitas diri. Mahasiswa bam merasa bingung
dengan peran yang hams ia lakukan dalam lingkungan masyarakat Bandung.
Kelima, tidak menyukai adanya perbedaan bahasa, kebiasaan, nilai/norma, sopan-
santun di daerah yang bam. Mahasiswa tidak menyukai adanya perbedaan dalam
tata cara pergaulan antara kota Bandung dengan daerah asalnya. Keenam,
perasaan tidak berdaya yang disebabkan oleh ketidakmampuan menyesuaikan diri
dengan lingkungan bam. Pada aspek keenam ini mempakan aspek dimana
dampak culture shock benar-benar dialami mahasiswa (Jurnal Ps;kolog;, Vol- 5,
No.1, Maret 2000).
Dampak culture shock yang dialami mahasiswa bam pada aspek
pertama diantaraya insomnia; orientasi yang berlebihan terhdaap kesehatan;
perasaan tidak aman akan keselamatan dirinya; makan berlebihan atau sulit
makan; sedih; maupun terlalu mengutamakan kebersihan. Pada aspek kedua yaitu
kerinduan terhadap keluarga, ternan, dan orang-orang yang dekat dengannya;
perasaan ingin pulang ke mmah; dan merasa berada dalam kesendirian. Pada
aspek ketiga yaitu tidak memiliki keinginan untuk berintraksi dengan orang lain;
menghabiskan waktu dengan melakukan kegiatan sendiri; maupun merasaakan
diri tidak diperhatikan oleh orang lain. Pada aspek keempat yaitu kehilangan rasa
percaya diri; keinginan yang berlebihan untuk melakukan identifikasi dengan
masyarakat setempat; maupun kehilangan identitas. Pada aspek kelima yaitu
mengembangkan stereotip negatif tentang budaya yang baru; kurang menyukai
nilai-nilai yang diyakini masyaraakt setempat; maupun merasa dirinya sangat
loyal dengan budaya daerah asalnya. Pada aspek yang keenam yaitu sensitif;
kehilangan kemampuan untuk melakukan aktivitas secara efektif; dan
psikosomatis.
Dampak culture shock yang dialami mahasiswa dapat dikurangi dengan
melakukan interaksi dengan teman akrab setiap hari misalnya dengan melakukan
berbagai macam kegiatan, mengirimkan berita pada keluarga, maupun kedatangan
orangtua untuk menjenguk mahasiswa ketika berada di kota Bandung. Menurut
adelman (1988) dan Fontaine (1986) dalam Ward, Bochner, dan Furnham
(2001 : 85), bahwa dukungan sosial merupakan faktor signifikan dalam
adjustment psikologis. Dukungan sosial dapat diperoleh dari berbagai sumber,
termasuk keluarga, teman, dan kenalan (Ward, Bochner, dan Furnham, 2001
: 85).
Relasi dengan co-national, dalam hal ini mahasiswa yang berasal dari
Bandung merupakan sumber yang paling menonjol dan mengandung kekuatan
untuk mendorong dan mendukung sojourner yaitu mahasiswa yang berasal dari
luar propinsi Jawa Barat untuk menanggulangi dampak culture shock yang
dialaminya (Sykes dan Eden, 1987, dalam Ward, Bochner, dan Furnham,
2001 : 86). Hubungan dengan co-national bisa sangat menolong, penuh
kehangatan, yang berwujud pada dukungan individual dan kelompok.
Demikian juga frekuensi kontak sosial mahasiswa yang berasal dari luar
propinsi Jawa Barat dengan mahasiswa yang berasal dari Bandung, dapat
memfasilitasi kemampuan mempelajari budaya spesifik untuk kehidupan di
lingkungan budaya yang barn yaitu budaya masyarakat Jawa Barat (Ward,
Bochner, dan Furnham, 2001 : 87). Dengan memiliki kamampuan untuk
mempelajari budaya masyarakat Jawa Barat diharapkan mahasiswa tersebut tidak
mengalami culture shock dalam tarafyang berat.
Mahasiswa dari luar propinsi Jawa Barat yang sering mengirim berita pada
keluarganya, maupun yang pemah dijenguk orang tua bisa mengurangi dampak
culture shock yang dialaminya. Menurnt Judit Kivits dan Anne Herman, 2001,
dalam www.edu.oulu.fi.culture.htm. dengan seringnya melakukan kontak
dengan ternan dan keluarga bisa mengurangi dampak culture shock yang dialami
mahasiswa. Melalui kontak yang dilakukan ini mahasiswa tersebut bisa
menceritakan pengalaman yang dialami mahasiswa di daerah yang barn dalam hal
ini di Bandung. Mahasiswa yang frekuensi kontak dengan keluarga dan temannya
jarang lebih merasakan kerinduan daan kesendirian.
Lamanya mahasiswa tinggal di daerah asal dapat meningkatkan taraf
culture shock yang dialami. Semakin lama mahasiswa tinggal di darah asalnya
maka identitas kuIturalnya akan semakin kuat. Identitas kultural merupakan
penegasan, kebanggaan, dan evaluasi positif dari suatu kelompok yang
berhubungan dengan perilaku etnis budaya, nilai-nilai, dan tradisi (Phinney,
1992, dalam Ward, Bochner, dan Furnham, 2001 : 99). Identitas kuItural
merujuk pada keikutsertaan, yaitu seberapa besar mahasiswa tersebut merasa
dirinya sebagai bagian dari masyarakat di daerah asalnya. Sentralitas yaitu
seberapa penting suatu kelompok yaitu masyarakat daerah asalnya untuk identitas
pribadinya. Evaluasi yaitu penilaian positif atau negatif tentang suatu kelompok,
dalam hal ini penilaian mahasiswa tersebut terhadap budaya daerah asalnya
apakah bersifat positif atau negatif Identitas kuItural juga merujuk pada tradisi,
yaitu praktek pelaksanaan budaya dan penerimaan norma-norma dan nilai-nilai
tradisional. Hal ini merujuk pada penerapan nilai-nilai budaya daerah asal oleh
mahasiswa tersebut dalam kehidupan sehari-harinya. (Ward, Bochner, dan
Furnham, 2001 : 99-] 00).
Lamanya mahasiswa tinggal di Bandungjuga turut mempengaruhi dampak
culture shock yang dialami mahasiswa. Dalam hal ini lamanya mahasiswa tinggal
di Bandung dapat menyebabkan terjadinya akulturasi pada mahasiswa sebagai
hasil dari kontak yang terjadi dengan masyarakat Bandung (Redfield, Linton,
dan Herskovits, ]936, dalam Ward, Bochner, dan Furnham, 2001 : 43).
AkuIturasi merupakan upaya lebih lanjut yang dilakukan mahasiswa dalam
menanggulangi dampak culture shock yang dialaminya. Dengan melakukan
akulturasi mahasiswa dapat menyesuaikan diri dengan budaya masyarakat Jawa
Barat. Namun, pada mahasiswa yang merasa ditolak oleh budaya masyarakat
ditempat yang bam ia diami, pada umumnya belum terlalu berhasil menyesuaikan
diri di lingkungan yang bam karena ia menghayati tempat yang bam tidak seperti
daerah asalnya (Judith Kivits dan Anne Hermanns, 2001, dalam
www.edu.oulu.fi.culture.htm).
'"~
=;:::
'"
. .
. .
. .
; .:::
'-+-
~c:
~ :g.Vi .
fU~:bfJO~i..
ft ~.~ ~ ~ ~ ~
p~~
. .
. .
Berdasarkan uraian di atas dapat diasumsikan bahwa :
1. Situasi budaya yang barn di kota Bandung mempakan shock bagi
mahasiswa semester dua Universitas Kristen Maranatha yang berasal dari
luar propinsi Jawa Barat.
2. Perbedaan antara budaya di kota Bandung dengan dengan di daerah asal
dapat menimbulkan dampak culture shock pada mahasiswa semester dua
Universitas kristen Maranatha yang berasal dari luar propinsi Jawa Barat.
 Oi Indonesia pendidikan tinggi dirasakan sangat penting dalam rangka 
 meningkatkan kualitas sumber daya manusia agar bisa menghadapi persaingan 
 dengan negara lain yang semakin ketat. Namun, dibandingkan dengan jumlah 
 penduduk dan luasnya wilayah Indonesia jumlah perguruan tinggi di Indonesia 
 masih kurang memadai. Banyak provinsi terutama di luar pulau Jawa yang belum 
 memiliki cukup perguruan tinggi dalam hal kuantitas. Misalnya dari total 
 keseluruhan perguruan tinggi yang ada di Indonesia yaitu sebanyak 273 , di luar 
 pulau Jawa hanya memiliki 112 perguruan tinggi atau 41.03 % yang masing- 
 masing terse bar di berbagai wilayah yaitu di Aceh, Sumatera Utara, Sumatera 
 Selatan dan Lampung yaitu sebanyak 39 perguruan tinggi atau 34.82 %, Nusa 
 Tenggara Barat, Nusa Tenggara Timur, dan Bali sebanyak 18 perguruan tinggi 
 atau 16.07 %, di Sulawesi sebanyak 24 perguruan tinggi atau 21.43 %, Sumatera 
 Barat dan Riau sebanyak 14 perguruan tinggi atau 12.5 %, Kalimantan sebanyak 
 12 perguruan tinggi atau 10.71 % dan Irian Jaya sebanyak 5 perguruan tinggi atau 
 4.46 %. Jumlah perguruan tinggi yang ada di pulau Jawa sebanyak 16] perguruan 
 tinggi atau 59.97 % yang masing-masing tersebar di berbagai wi]ayah yaitu di 
 Jakarta sebanyak 29 perguruan tinggi atau ]8.0] %, Jawa barat sebanyak 35 
 perguruan tinggi atau 21.74 %, Yogyakarta sebanyak 2] perguruan tinggi atau 
 13.04 %, Jawa tengah sebanyak 21 perguruan tinggi atau 13.04 %, dan Jawa tirnur 
 sebanyak 55 perguruan tinggi atau 34.16 %. ( www. Direktorat Jenderal 
 Pendidikan Tinggi (Dikti). org, 1999). 
 Sebagian besar siswa lulusan Sekolah Menengah Urnurn yang ingin 
 rnelanjutkan ke perguruan tinggi rnernilih pulau Jawa sebagai tujuan utarna. 
 Salah satu kota yang rnenjadi tujuan utarna bagi pelajar yang berasal dari berbagai 
 daerah di seluruh Indonesia yang ingin rneneruskan studi ke perguruan tinggi 
 adalah kota Bandung. Berdasarkan data pada www.jabar.com bahwa propinsi 
 Jawa Barat rnerniliki jurnlah penduduk terbesar di Indonesia dan rnernpunyai 
 proporsi penduduk dengan tingkat pendidikan, jurnlah lulusan strata 1, strata 2, 
 dan strata 3 terbanyak dibandingkan dengan propinsi lain. Hasil wawancara 
 dengan seorang rnahasiswa lnstitut Teknologi Nasional (ITENAS) angkatan 
 2002, A yang berasal dari Riau rnengatakan "Perguruan tinggi yang ada di 
 Bandung rnernpunyai kualitas yang baik dan rnarnpu rnenghasilkan lulusan yang 
 cukup berkualitas, sehingga wajar jika sebagian besar lulusan Sekolah Menengah 
 Urnurn (SMU) rnernilih kota ini sebagai tujuan untuk rnelanjutkan ke jenjang 
 perguruan tinggi". Dernikian juga diungkapkan oleh C, rnahasiswa Universitas 
 Padjajaran (UNP AD) angkatan 2000 yang berasal dari Banjarmasin "Kota 
 Bandung rnernang terkenal dengan sebutan kota kern bang, tapi lebih dari itu 
 perguruan tinggi yang ada di Bandung cukup baik dan berkualitas". 
 Mahasiswa yang berasal dari luar propinsi Jawa Barat yang meneruskan 
 pendidikan tinggi di Bandung tersebar di berbagai perguruan tinggi. Universitas 
 Kristen Maranatha (UKM) merupakan salah satu universitas di Bandung yang 
 mahasiswanya sebagian besar berasal dari luar propinsi Jawa Barat. Hal ini 
 diketahui dari jumlah mahasiswa yang diterima tiga tahun terakhir. Dari 
 keseluruhan mahasiswa baru di Universitas Kristen Maranatha (UKM) pada tahun 
 2001 yang terdaftar dan aktif di seluruh fakultas yang berasal dari luar Jawa Barat 
 yaitu 829 orang (49.02 %) dari total keseluruhan 1691 orang, sedangkan 
 mahasiswa yang berasal dari Jawa Barat sendiri 822 orang (50.98 %). Demikian 
 pada tahun 2002 jumlah mahasiswa baru di Universitas Kristen Maranatha 
 (UKM) yang terdaftar dan aktif di seluruh fakultas yang berasal dari luar Jawa 
 Barat 1036 orang (48.52%) dari total keseluruhan 2135 orang, sedangkan 
 mahasiswa yang berasal dari Jawa Barat sendiri 1099 orang (51.48 %), serta pada 
 tahun 2003 jumlah mahasiswa baru yang terdaftar di Universitas Kristen 
 Maranatha (UKM) berasal dari luar Jawa Barat yang terdaftar dan aktif di 
 seluruh fakultas 585 orang (32.66 %) dari total keseluruhan 1791 orang 
 sedangkan mahasiswa yang berasal dari Jawa Barat sendiri 1206 orang (67.34%). 
 (Humas Universitas Kristen Maranatha, Daftar Mahasiswa Baru, 2003). 
 Mahasiswa yang meneruskan pendidikan tinggi di Universitas Kristen 
 Maranatha Bandung berasal dari berbagai daerah di Indonesia. Salah satu 
 keadaan yang dihadapi oleh mahasiswa baru adalah proses penyesuaian diri 
 terhadap lingkungan belajar yang baru. Dalam penyesuaian diri dengan sistem 
 pengaJaran baru di perguruan tinggi, dunia mahasiswa sangat berbeda dengan 
 dunia SMU. Sistem belajar yang mengadopsi dari Amerika yaitu Sistem Kredit 
 Semester (SKS), cara dosen mengajar yang sungguh berbeda kadang membuat 
 mahasiswa baru menjadi bingung (www.detik.com). Di samping itu, mereka juga 
 perlu melakukan penyesuaian diri terhadap kebiasaan sehari-hari masyarakat 
 Bandung dalam menjalin interaksi dengan masyarakat. 
 Proses penyesuian diri ini seringkali menimbulkan "culture shock", yang 
 mengacu pada keadaan stres dan ketegangan yang terkumulasi dengan adanya 
 tekanan saat bertemu dengan kebutuhan sehari-hari pada cara yang tidak biasa, 
 seperti bahasa, iklim, makanan, kebersihan, serta persahabatan atau cara 
 berinteraksi (www.mville.edu). Culture shock menggambarkan suatu keadaan 
 yang negatif, reaksi pasif untuk mengatur lingkungan yang berbahaya, dimana 
 respon seseorang terhadap lingkungan budaya yang tidak biasa tersebut 
 merupakan suatu proses yang berlangsung terus menerus dalam berhubungan 
 dengan perubahan yang terjadi (Oberg, 1960 dalam Collen Ward, Bochner, dan 
 Furnham, 2001 :270). Pada umumnya culture shock dialami oleh pendatang 
 selama enam bulan sampai satu tahun pertama kedatangannya (Jurnal Psikologi, 
 Vol- 5. No.1, Maret 2000). 
 Perbedaan antara kondisi di daerah asal dengan di daerah yang baru dapat 
 memunculkan dampak yang tidak menyenangkan bagi seorang mahasiswa 
 pendatang seperti merasa tidak percaya diri, mengalami kesulitan tidur atau 
 insomnia, berburuk sangka terhadap masyarakat setempat, maupun kesulitan 
 menjalin relasi. Dampak yang tidak menyenangkan ini mempakan dampak dari 
 Culture Shock yang dialami individu yang pindah ke daerah bam (Furnham dan 
 Bochner, 1986, dalam Jurnal Psikologi, Maret 2000). 
 Berdasarkan hasil penelitian sebelumnya oleh Vivi Hidajat dan Tutty I. 
 Sodjakusumah (2000) terhadap 74 orang mahasiswa tingkat pertama Universitas 
 Padjajaran (UNPAD) yang berasal dari luar daerah Jawa Barat, tampak sebanyak 
 31 orang atau 41.9 % mengalami Culture Shock dalam derajat yang tergolong 
 ringan atau di bawah rata-rata kelompoknya. Sebanyak 33 orang atau 44.6 % 
 mengalami Culture Shock dalam derajat yang tergolong berat atau di atas rata-rata 
 kelompoknya. Mahasiswa yang mengalami culture shock dalam derajat di atas 
 rata-rata kelompoknya mengalami gangguan psikologis seperti kesulitan 
 konsentrasi, perasaan yang sensitif, dan kurangnya kesiapan mental dalam 
 menempuh pendidikan di perguman tinggi(Jurnal Psikologi, Vol - 5. No 1, 
 Maret 2000). 
 Berdasarkan hasil survey terhadap 30 orang mahasiswa semester dua 
 Universitas Kristen Maranatha (UKM) yang berasal dari luar propinsi Jawa Barat 
 pada tanggal 5 mei 2004 tentang penghayatan mahasiswa terhadap kebiasaan 
 sehari-hari daerah asal mahasiswa , 25 orang atau 83.33 % menjelaskan bahwa 
 mereka menghayati kebiasaan sehari-hari masyarakat daerah asal mereka yaitu 
 melakukan kerjasama. Mereka juga saling membantu antara satu dengan yang 
 lain , perilaku hams sesuai dengan adat istiadat yang berlaku dan umumnya 
 mereka saling mengenal antara satu dengan yang lain. Mereka juga menggunakan 
 bahasa daerah dalarn berkornunikasi dan rnenyukai rnakanan khas daerah rnereka. 
 Sedangkan 5 orang atau 16.67 % rnenje1askan bahwa hubungan yang terjadi 
 dalarn rnasyarakat bersifat dangkal dan seperlunya saja ketika rnereka berada di 
 daerah asal rnereka. Mereka saling rnengenal hanya dengan pihak tertentu saja dan 
 tidak dengan sernua anggota rnasyarakat. Mereka juga tidak rnenggunakan bahasa 
 daerah dalarn berkornunikasi, dan jarang rnernilih rnakanan khas daerah rnereka. 
 Mereka juga rnenghayati kebiasaan sehari-hari ternan-ternan kuliah yang 
 berasal dari Jawa Sarat di karnpus UKM, 57.25 % dari rnahasiswa rnengatakan 
 bahwa ternan kuliah yang berasal dari Jawa Sarat rnenggunakan bahasa Sunda 
 yang kadang-kadang sulit rnereka rnengerti, 78.25 % rnengatakan bahwa relasi 
 yang terjadi antara rnahasiswa dengan ternan kuliah yang berasal dari Jawa Sarat 
 seperlunya saja dan bersikap kurang peduli, 65.25 % rnenyatakan bahwa 
 rnahasiswa yang berasal dari Jawa Sarat rnenerapkan gaya hidup rnewah. 
 Sebanyak 60.75 % rnenyatakan rnereka agak sulit rnengikuti cara berpakaian 
 ternan-ternan di karnpus. Sebanyak 57.95 % rnenyatakan cukup sulit untuk 
 beradaptasi dengan kondisi lalu lintas kota Sandung karena belurn rnengetahui 
 sepenuhnya kondisi jalan raya di kota Sandung, dan 55.75 % rnenyatakan agak 
 sulit rnenyesuaikan diri dengan menu rnakanan sehari-hari. 
 Proses adaptasi yang dilakukan rnahasiswa tidak berlangsung rnulus 
 rnisalnya yang diungkapkan dalarn wawancara oleh P rnahasiswa asal Riau yang 
 kuliah di Fakultas Teknik lndustri Universitas Kristen Maranatha bahwa "Sebagai 
 anak Riau, saya rnerasa bahwa relasi yang terjadi di rnasyarakat Sandung rnernang 
 cuek kurang peduli antara satu dengan yang lain. Saya enam bulan pertama 
 merasa terasing dan sendirian di sini, sulit membaur dengan relasi masyarakat di 
 sini". Demikian juga dengan hasil survey terhadap 30 orang mahasiswa 
 Universitas Kristen Maranatha yang berasal dari luar propinsi Jawa Barat, 23 
 orang di antaranya atau 76.67 % menjelaskan bahwa mereka sulit beradaptasi 
 dengan kebiasaan sehari-hari masyarakat Bandung, mereka merasa tegang ketika 
 berada di Bandung, mengalami diare, demam, flu, susah tidur, migrain, me rasa 
 bingung tentang bagaimana hams bersikap terhadap masyarakat di Bandung dan 
 agak sulit berkonsentrasi ketika mengikuti perkuliahan. Mereka juga merasa agak 
 sulit untuk menyesuaikan diri dalam menyelesaikan tugas yang diberikan oleh 
 dosen, membutuhkan waktu yang lebih untuk memahami materi kuliah. Sebagian 
 lagi yaitu 7 orang mahasiswa atau 23.33 % menjelaskan mereka sulit beradaptasi 
 hanya dalam waktu tiga bulan pertama, kemudian mereka bisa menyesuaikan diri 
 dengan kebiasaan-kebiasaan masyarakat Bandung, maupun dalam sistem belajar 
 di kampus. 
 Berdasarkan alasan tersebut peneliti tertarik untuk meneliti tentang 
 dampak culture shock yang dialami mahasiswa semester dua (yang berasal dari 
 luar propinsi Jawa Barat) Universitas Kristen Maranatha Bandung. 
 Berdasarkan latar belakang masalah yang telah diuraikan sebelumnya, maka 
 ingin diketahui bagaimanakah dampak culture shock pada mahasiswa semester 
 dua Universitas Kristen Maranatha Bandung yang berasal dari luar propinsi Jawa 
 Barat. 
 1. 3 Maksud dan Tujuan Penelitian 
 Memperoleh gambaran tentang dampak culture shock pada mahasiswa 
 semester dua Universitas Kristen Maranatha Bandung yang berasal dari 
 luar propinsi Jawa Barat. 
 Memberikan paparan yang lebih rinci mengenai dampak culture shock 
 pada mahasiswa semester dua Universits Kristen Maranatha Bandung 
 yang berasal dari luar propinsi Jawa Barat. 
 Diharapkan hasil penelitian ini dapat digunakan antara lain untuk : 
 a. Memberikan informasi bagi bidang Psikologi Lintas Budaya secara 
 khusus tentang dampak culture shock pada mahasiswa semester dua. 
 b. Sebagai landasan informatif untuk penelitian selanjutnya yang 
 berhubungan dengan dampak culture shock pada mahasiswa semester 
 dua. 
 Diharapkan penelitian ini dapat bermanfaat bagi berbagai pihak, antara 
 lain: 
 a. Memberikan informasi bagi mahasiswali semester dua Universitas 
 Kristen Maranatha Bandung yang berasal dari luar propinsi Jawa 
 Barat tentang dampak culture shock. 
 b. Sebagai masukan bagi pihak Universitas Kristen Maranatha Bandung 
 khususnya bagian MSDCC (Maranatha Student Development and 
 Counselling Center) mengenai gambaran dampak culture shock pada 
 mahasiswa semester dua yang berasal dari luar propinsi Jawa Barat 
 dalam upaya memberikan bimbingan serta bantuan psikologis 
 sehingga memudahkan mahasiswa untuk melakukan penyesuian diri 
 dengan lingkungan yang bam. 
 c. Bagi masing-masing fakultas yang ada di Universitas Kristen 
 Maranatha Bandung, penelitian ini diharapkan dapat memberikan 
 masukan bagi dosen wali dan membantu dosen wali untuk memahami 
 mahasiswa, sehingga dosen wali dapat membantu mahasiswa 
 khususnya yang berasal dari luar propinsi Jawa Barat untuk 
 mengatasi dampak culture shock yang dialaminya. 
 Calon mahasiswa yang berasal dari berbagai wilayah di Indonesia 
 melakukan perpindahan tempat studi dari daerah asal ke kota Bandung untuk 
 meneruskan studi ke jenjang pendidikan yang lebih tinggi (Bochner, 1982, dalam 
 Ward, Bochner, dan Furnham, 2001 : 5). Perpindahan tempat studi tersebut 
 menciptakan bertemunya dua atau lebih budaya di tempat yang baru tersebut. 
 Menurut Koentjaraningrat (1990), budaya merupakan keseluruhan sistem 
 gagasan, tindakan dan hasil karya manusia dalam rangka kehidupan masyarakat 
 yang dijadikan milik diri dengan belajar. Hal ini berarti bahwa hampir seluruh 
 tindakan manusia adalah budaya, karena amat sedikit tindakan manusia dalam 
 rangka kehidupan masyarakat yang tak perlu dibiasakan dengan belajar, yaitu 
 hanya beberapa tindakan naluri, retleks, maupun tindakan akibat proses fisiologis 
 (Koentjaraninngrat, 1990 : 180). Budaya memiliki tiga wujud yaitu, pertama 
 budaya sebagai suatu kompleks dari ide-ide, gagasan, nilai-nilai, norma-norma, 
 peraturan-peraturan. Kedua, budaya merupakan sistem sosial dalam aktivitas di 
 masyarakat. Ketiga, budaya dalam bentuk fisik yang merupakan hasil karya 
 manusla. 
 Budaya dalam hal ini merujuk pada kebiasaan sehari-hari yang terjadi di 
 dalam suatu masyarakat seperti cara berpakaian, perilaku laki-Iaki dan perempuan, 
 sikap terhadap kebersihan, sikap terhadap agama, maupun relasi interpersonal. 
 Menurut Bochner (1982) dalam Collen Ward, Bochner, Furnham (2001 : 5), 
 kontak antar kebudayaan dapat terjadi diantara penduduk dalam satu bangsa yang 
 memiliki budaya berbeda. Kontak terjadi ketika individu dari suatu komunitas 
 mengunjungi daerah lain dengan beberapa tujuan seperti bekerja, bermain, belajar, 
 pindah ataupun dalarn rangka rnemberikan bantuan. Dalam hal ini rnahasiswa 
 yang berasal dari berbagai wilayah dan beranekaragam budaya di Indonesia yang 
 datang ke Bandung rnengalami kontak antar berbagai budaya dalarn satu bangsa. 
 Mereka saling berinteraksi dan menyesuaikan diri satu sarna lain. Mereka disebut 
 juga sebagai sojourner, yakni kelornpok yang rnelakukan perpindahan dan tinggal 
 untuk sernentara waktu di Bandung dan kernbali lagi ke kota asal mereka ketika 
 tujuan rnereka telah tercapai yaitu menempuh pendidikan tinggi. 
 Kontak sosial yang terjadi pada budaya yang berbeda yang dialarni oleh 
 rnahasiswa baru seringkali menyulitkan dan penuh dengan stres (Collen Ward, 
 Bochner, Furnham, 2001 : 9). Keadaan dernikian sering diungkapkan dengan 
 istilah culture shock. Menurut Oberg (1960, dalam Collen Ward, Bochner, 
 Furnham, 2001 : 270) culture shock menggambarkan sesuatu yang negatif, reaksi 
 pasif dari individu untuk mengatur lingkungan yang berbahaya. Dalam hal ini, 
 respon yang diberikan terhadap lingkungan budaya yang tidak biasa tersebut 
 rnerupakan suatu proses yang berlangsung terus-rnenerus sehubungan dengan 
 perubahan budaya yang terjadi. Menurut Robert.L.Kohls, culture Shock 
 rnenggarnbarkan kondisi individu yang rnenemukan dirinya berada pada budaya 
 lain dan merasakan homesick, malas atau menarik diri. Mereka menghabiskan 
 seluruh waktunya untuk menghindari masyarakat setempat. 
 Hal-hal berbeda yang dapat menimbulkan terjadinya shock pada 
 mahasiswa semester dua yang berasal dari luar Bandung adalah makanan, tipe 
 pakaian, perilaku laki-Iaki dan perempuan, sikap terhadap kebersihan, pengaturan 
 keuangan, penggunaan waktu, relasi interpersonal, maupun transportasi umum 
 (J.P. Spradley dan M.Phillips, 1972 dalam Collen Ward, Bochner, Furnham, 
 2001 : 74). 
 Proses yang terjadi saat mahasiswa semester dua mengalami culture 
 shock melibatkan tiga komponen dalam dirinya, yaitu afeksi (Affect), perilaku 
 (Behavior), dan kognisi (Cognition), yaitu bagaimana mahasiswa merasa, 
 berperilaku, serta berpikir saat menampakkan pengaruh budaya di tempat yang 
 baru. Komponen afektif meliputi kebingungan, kecemasan, disorientasi, curiga, 
 dan harapan terus-menerus untuk berada di tempat lain. Komponen tingkah laku 
 berhubungan dengan pembelajaran budaya, yang merupakan perluasan dari 
 kemampuan sosial, meliputi aturan-aturan, perjanjian, serta asumsi yang mengatur 
 interaksi interpersonal, termasuk komunikasi verbal dan nonverbal. Komponen 
 kognisi menekankan bahwa budaya merupakan "shared meaning" (berbagi arti) 
 diantara masyarakat. Dalam hal ini, apakah mahasiswa akan mempertahankan 
 atau mengubah identitas budayanya dengan adanya stereotip dari masyarakat, 
 serta bagaimana mahasiswa mempertahankan harga dirinya dengan adanya 
 tekanan, akulturasi, atau prasangka (Oberg dalam Ward, Bochner, dan 
 Furanham, 2001 : 48,270-272). 
 Mahasiswa semester dua yang berasal dari luar Jawa Barat yang berusia 
 18 sampai dengan 21 tahun berada pada kategori remaja akhir (Santrock, 2002). 
 Sejalan dengan tugas perkembangan remaja akhir yang sedang dijalani 
 mahasiswa, ditandai dengan tahap berpikir formal operational. Dalam hal ini 
 mahasiswa belajar mengintegrasikan pemahamannya mengenal budaya 
 masyarakat Bandung yang diperoleh melalui interaksi secara langsung dengan 
 masyarakat Bandung, maupun melalui media massa ketika mahasiswa tersebut 
 berusaha melakukan penyesuaian diri. Menurut Erikson (1968), kelompok- 
 kelompok minoritas, akan beIjuang mempertahankan identitas kebudayaan 
 mereka saat berbaur ke dalam kebudayaan yang dominan (Erikson, 1968 dalam 
 Santrock, 2002 : 60). Dalam hal ini mahasiswa yang berasal dari luar propinsi 
 Jawa Barat akan berjuang untuk mmepertahankan identitas etnik mereka ketika 
 mereka berbaur dengan masyarakat Bandung. Semakin mahasiswa tersebut 
 berusaha mempertahankan identitas kebudayaannya akan memperbesar dampak 
 culture shock yang dialaminya. 
 Menurut Oberg (1960), terdapat enam aspek yang melandasi 
 munculnya dampak culture shock, yaitu pertama, ketegangan karena adanya 
 usaha untuk beradaptasi secara psikis. Dalam hal ini mahasiswa yang berasal dari 
 luar propinsi Jawa Barat harus menerima bahwa antara dirinya dan masyarakat 
 Bandung memiliki perbedaan budaya sehari-hari. Kedua, perasaan kehilangan 
 dan kekurangan keluarga dan teman. Keberadaannya sebagai bagian dari anggota 
 masyarakat tidak begitu diperhatikan karena sebagian besar masyarakat 
 mementingkan diri sendiri dan keluarga terdekat. Dengan demikian mahasiswa 
 tersebut hams mengerjakan semuanya sendiri tanpa bantuan dari orang lain. 
 Ketiga, penolakan terhadap dan dari orang-orang di lingkungan yang bam. Dalam 
 hal ini mahasiswa tidak memiliki keinginan untuk berinteraksi dengan masyarakat 
 Bandung. Keempat, kebingungan mengenai peran, harapan terhadap peran 
 tersebut, nilai yag dianut, dan identitas diri. Mahasiswa bam merasa bingung 
 dengan peran yang hams ia lakukan dalam lingkungan masyarakat Bandung. 
 Kelima, tidak menyukai adanya perbedaan bahasa, kebiasaan, nil ai/norma, sopan- 
 santun di daerah yang bam. Mahasiswa tidak menyukai adanya perbedaan dalam 
 tata cara pergaulan antara kota Bandung dengan daerah asalnya. Keenam, 
 perasaan tidak berdaya yang disebabkan oleh ketidakmampuan menyesuaikan diri 
 dengan lingkungan bam. Pada aspek keenam ini mempakan aspek dimana 
 dampak culture shock benar-benar dialami mahasiswa (Jurnal Ps;kolog;, Vol- 5, 
 No.1, Maret 2000). 
 Dampak culture shock yang dialami mahasiswa bam pada aspek 
 pertama diantaraya insomnia; orientasi yang berlebihan terhdaap kesehatan; 
 perasaan tidak aman akan keselamatan dirinya; makan berlebihan atau sulit 
 makan; sedih; maupun terlalu mengutamakan kebersihan. Pada aspek kedua yaitu 
 kerinduan terhadap keluarga, ternan, dan orang-orang yang dekat dengannya; 
 perasaan ingin pulang ke mmah; dan merasa berada dalam kesendirian. Pada 
 aspek ketiga yaitu tidak memiliki keinginan untuk berintraksi dengan orang lain; 
 menghabiskan waktu dengan melakukan kegiatan sendiri; maupun merasaakan 
 diri tidak diperhatikan oleh orang lain. Pada aspek keempat yaitu kehilangan rasa 
 percaya diri; keinginan yang berlebihan untuk melakukan identifikasi dengan 
 masyarakat setempat; maupun kehilangan identitas. Pada aspek kelima yaitu 
 mengembangkan stereotip negatif tentang budaya yang baru; kurang menyukai 
 nilai-nilai yang diyakini masyaraakt setempat; maupun merasa dirinya sangat 
 loyal dengan budaya daerah asalnya. Pada aspek yang keenam yaitu sensitif; 
 kehilangan kemampuan untuk melakukan aktivitas secara efektif; dan 
 psikosomatis. 
 Dampak culture shock yang dialami mahasiswa dapat dikurangi dengan 
 melakukan interaksi dengan teman akrab setiap hari misalnya dengan melakukan 
 berbagai macam kegiatan, mengirimkan berita pada keluarga, maupun kedatangan 
 orangtua untuk menjenguk mahasiswa ketika berada di kota Bandung. Menurut 
 adelman (1988) dan Fontaine (1986) dalam Ward, Bochner, dan Furnham 
 (2001 : 85), bahwa dukungan sosial merupakan faktor signifikan dalam 
 adjustment psikologis. Dukungan sosial dapat diperoleh dari berbagai sumber, 
 termasuk keluarga, teman, dan kenalan (Ward, Bochner, dan Furnham, 2001 
 : 85). 
 Relasi dengan co-national, dalam hal ini mahasiswa yang berasal dari 
 Bandung merupakan sumber yang paling menonjol dan mengandung kekuatan 
 untuk mendorong dan mendukung sojourner yaitu mahasiswa yang berasal dari 
 luar propinsi Jawa Barat untuk menanggulangi dampak culture shock yang 
 dialaminya (Sykes dan Eden, 1987, dalam Ward, Bochner, dan Furnham, 
 2001 : 86). Hubungan dengan co-national bisa sangat menolong, penuh 
 kehangatan, yang berwujud pada dukungan individual dan kelompok. 
 Demikian juga frekuensi kontak sosial mahasiswa yang berasal dari luar 
 propinsi Jawa Barat dengan mahasiswa yang berasal dari Bandung, dapat 
 memfasilitasi kemampuan mempelajari budaya spesifik untuk kehidupan di 
 lingkungan budaya yang barn yaitu budaya masyarakat Jawa Barat (Ward, 
 Bochner, dan Furnham, 2001 : 87). 

