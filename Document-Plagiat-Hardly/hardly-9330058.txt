Remaja merupakan aset bangsa yang memegang peranan penting
pada masa pembangunan yang akan datang. Sebagai penerus
pembangunan bangsa, seyogianya remaja memilikikualitas fisik dan
mental yang dapat diandalkan agar dapat mengatasi tantangan yang
dihadapinya. Memang cukup banyak harapan masyarakat terhadap
sosok remaja sebagai generasi penerus, namun demikian remaja tetaplah
sebagai individuyang tengah berada pada masa peralihan antara masa
anak-anak dengan masa dewasa yang mengalami banyak perubahan,
baik pada dirinya maupun lingkungannya.
Seiring dengan harapan-harapan masyarakat yang menuntut
remaja untuk berperilaku sesuai dengan nilai-nilai yang ada dalam
masyarakat, nam un masyarakat sendiri yang merupakan contoh model
bagi perilaku remaja malah memperlihatkan perilaku yang jauh dari nilai-
nilai etika dan moral. Hal ini tentu membuat remaja menjadi semakin
mudah meniru apa yang dllakukan oleh masyarakat.
Seperti banyak diberitakan dalam media massa tentang perilaku
masyarakat yang dapat menjadi contoh bagi remaja, berupa terjadinya
tawuran antar masyarakat, perilaku tidak disiplin, ketidakadilan, mencuri
dan sebagainya, tentunya akan menjadi dilema bagi perilaku remaja itu
sendiri. Oi satu sisi remaja dituntut untuk berlaku jujur, adil, taat hukum
dan sebagainya, nam un di sisi lain lingkungan masyarakat secara tidak
langsung memberi contoh yang buruk.
Remaja yang memasuki masa peralihan dari masa anak menuju
masa dewasa mengalami perubahan, di antaranya perubahan kognitif dan
peranan di dalam masyarakat. Sejalan dengan perkembangan remaja
yang memasuki cara berpikir operasional formal, remaja m ulai
membutuhkan pertimbangan-pertimbangan dan argumen yang mendasari
tentang mengapa hal tersebut baik dan mengapa hal tersebut buruk.
Karena berdasarkan kenyataan yang kerapkali terjadi pada remaja,
seperti memberi contekan pada teman ketika ulangan, mencontek,
membolos dari sekolah, berkelahi, mencoret-coret atau merusak fasilitas
umum, menonton film-film porno, merokok atau penyalahgunaan obat-
obat terlarang (Plklran Rakyat, 2002), tentu menimbulkan pertanyaan,
apa yang sebenarnya menjadi pertimbangan remaja dalam melakukan
hal tersebut?
Untuk dapat mempertimbangkan apa yang dianggap balk atau
buruk, benar atau salah, diperlukan kematangan moral dari remaja itu
sendiri. Pertimbangan-pertimbangan inilah yang menjadi indikator dari
tahap kematangan moral. Sepertl diungkapkan oleh Kohlberg (1995)
bahwa, pertimbangan moral merupakan suatu proses berpikir di dalam diri
seseorang dalam memutuskan masalah 80sial yang menyangkut moral,
yaitu mengapa sesuatu hal dianggap benar atau salah. Dengan demikian
pertimbangan mengapa sesuatu hal dianggap baik atau buruk, benar atau
salah, diperlukan remaja untuk mendasari pengambilan keputusan
moralnya.
Menurut Dr. Sugarda T. SpA(k) (Plklran Rakyat, 2000),
peralihan moralitas dari moralitas anak ke moralitas remaja, meliputi
perubahfln sikap dan nilai-nilai yang mendasari pembentukan konsep
moralnya sehingga sesuai dengan moralitas dewasa dan mampu
mengendalikan tingkah lakunya sendiri. Dengan demikian apabila rem aja
melakukan suatu aktivitas yang cenderung negatif bukanlah semata-mata
ditentukan oleh diri remaja itu 8endiri melainkan dipengaruhi pula oleh
interaksi antara remaja dengan komponen-komponen lingkungannya yang
salah satunya adalah orang tua yang menempati posisi sangat
berpengaruh dalam perkembangan remaja.
Hal ini berarti bahwa, orang tua memlllki kewajlban untuk sedapal
mungkin mengusahakan remaja menjadi individu yang sesuai dengan
harapan orang tua dan masyarakat. Blmblngan dan pengarahan orang tua
sangat penting, salah satunya adalah dalam menanamkan aarti' serla nilai
moral. Karena arti dan nilai moral itu sendiri tidak hanya menyangkut diri
sendiri letapi menyangkut pula dlri orang lain ataupun masyarakat luas.
Oleh karena itu melalui caranya sendiri orang tua menanamkan standar
nUai, mem perkuat sikap dan tlngkah laku yang dianggap positif dan
memperlemah sikap dan tingkah laku yang dianggap negatif agar rem aja
mampu melakukan banyak hal demi kemandiriannya.
Pada kenyataannya tidak sem ua orang tua atau orang dewasa
dapat membimbing anaknya menuju tahap perkembangan pertimbangan
moral yang lebih matang. Tidak sedikit orang tua atau orang dewasa
yang memberi contoh buruk secara tidak langsung pada anaknya, seperti
fenomena yang terjadi pad a orang tua yang melarang anak untuk
berbohong, tetapi orang tua sendiri melakukan hal itu. Dengan demikian
dalam keluarga diperlukan usaha orang tua untuk mengajarkan norma-
norma melalui perlakuan yang ditampilkan oleh orang tua dalam
kehidupan sehari-hari.
Disamping itu, orang tua juga mendorong terciptanya dialog-dialog
mengenai nilai-nilai yang harus dianut oleh remaja. Nilai-nilai tersebut
perlu disesuaikan dengan konsep-konsep moral yang berlaku umum yang
berhubungan dengan Jenis dislplin yang dlterapkan dl rum~,h, karena
pengetahuan remaja mengenai apa yang baik atau yang buruk, benar
atau salah secara moral diperoleh dart IIngkungan keluarga khususnya
dart orang tUB. Berdasarkan pengamatan secara umum pada beberapa
orang tua dalam membimbing dan mendidik remaja, ada berbagai cara
yang ditunjukkan, seperti apabila anaknya melakukan hal yang tldak
sesuai dengan kehendak orang tua, maka ada orang tua yang mengajak
anaknya untuk berdiskusi, ada yang tidak mempedulikan dengan
membiarkan anak melakukan tindakan yang tidak sesuai dengan
kehendak orang tua, ada yang langsung marah dan menghukum
anaknya, dan sebagainya.
Berkaitan dengan hal tersebut di atas, Hoffman (1994)
mengklasifikasikan teknik disiplin ini menjadi tiga, pertama teknik disiplin
Power Assertion yang mengutamakan kekuasaan orang tua dengan
melibatkan kekuatan fisik apabila remaja tidak patuh kepada orang tua.
Kedua teknik disiplin Love withdrawal yang secara langsung
mengungkapkan ketidaksenangan orang tua terhadap remaja dengan
tidak mengacuhkan atau menolak berblcara atau mendengarkan ketika
remaja melakukan kesalahan atau tidak menuruti kehendak orang tua.
Terakhir teknik disiplin Induction yang mengutamakan penjelasan
mengenai akibat yang dapat timbul dari perilakunya yang baik atau buruk
untuk dirinya sendiri atau untuk orang lain.
Pada intinya cara yang dnunjukkan orang tua tersebut bertujuan
untuk mendidik remaja agar menampilkan tingkah laku yang sesuai
dengan norma-norma masyarakat. Norma-norma masyarakat ini
diajarkan oleh orang tua melalui penerapan aturan dalam keluarga, dalam
hal ini pelaksanaan aturan dikendalikan melalui teknik penerapan disiplin
yang dapat dilihat dari fenomena yang ada pada remaja di SLTP .X".
Berdasarkan hasil wawancara (studi awaQ dengan 27 orang rem aja
di SLTP "X", diketahui bahwa sebanyak 44% responden menolak untuk
mangkir dari jam pelajaran di sekolah, dan sebanyak 56% tidak menolak
untuk mangkir dari jam pelajaran di sekolah. Dari 44%, 33% menolak
untuk mangkir dari jam pelajaran di sekolah karena takut mendapat
hukuman dari orang tuanya. Kemudian sebanyak 67% responden
menganggap bahwa mangkir dari jam pelajaran di sekolah merupakan
perbuatan tidak baik dan juga dikarenakan oleh pertimbangan bahwa,
mereka selalu mendapat penjelasan dari orang tua bahwa perbuatan itu
dapat merugikan diri sendiri.
Dari 56% responden yang tidak menolak untuk mangkir dari jam
pelajaran di sekolah, 53%nya memberikan alasan bahwa, apabila tidak
iku1mangkir dari sekolah maka tidak akan disukai oleh teman-temannya.
Kemudian sebanyak 13% memilih alasan karena tidak ingin ternan-
temannya terganggu di kelas dengan kehadirannya. Dan sebanyak 33%
memberi alasan, asalkan orang tua tidak mengetahui perbuatannya.
Atas dasar tersebut di atas, maka peneliti tertarik untuk meneliti
apakah ada hubungan antara penerapan teknik disiplin orang tua dengan
pertimbangan moral pada remaja.
Dari latar belakang masalah tersebut ingin diteliti :
"Apakah ada hubungan antara teknik penerapan disiplin yang diterapkan
orang tua dengan pertimbangan moral pada remaja di SL TP "X" (13-15
tahun) Kota Bandung?"
Penelitian ini dimaksudkan untuk memperoleh gambaran mengenai
teknik penerapan disiplin orang tua dan pertimbangan moral pada rem aja
di SL TP "X" (13-15 tahun) Kota Bandung.
Tujuan penelitian ini adafah untuk mengetahui hubungan antara
teknik penerapan disiplin orang tua dengan pertimbangan moral pada
remaja di Sl TP "X" (13-15 tahun) Kota Bandung.
Sebagai masukan bagi ilmu psikologi perkembangan mengenai
peran orang tua dalam menerapkan disiplin pada remaja, dikaitkan
dengan pertimbangan moral remaja.
Penelitian ini juga dapat memberikan masukan bagi penelitian-
penelitian lebih lanjut tentang penerapan teknik disiplin dan
pertimbangan moral.
Penelitian ini diharapkan dapat dijadikan informasi yang berg una
bagi orang tua untuk mengetahui Janis teknik penerapan disiplin
yang dapat membantu remaja dalam mengembangkan
kemampuan dalam mempertimbangkan masalah-masalah moral.
Penelitian ini diharapkan dapat memberikan informasi bagi guru di
sekolah mengenai tahap pertlmbangan moral remaja dalam
kaitannya dengan teknik penerapan disiplin yang diberikan orang
tua, sehingga rem aja dapat melakukan pertimbangan moral untuk
melakukan suatu tindakan.
lingkungan keluarga adalah tempat pertama bagi remaja untuk
membentuk pola plklrnya dalam memahami dan mempelajari suatu
permasalahan dan penyelesaiannya. Orang tua sebagai lingkungan
sosial yang pertama kali ditemui remaja perlu memberikan bimbingan
pada remaja untuk belajar memahaml harapan-harapan masyarakat
terhadap m ereka.
Bila melihat aspek moral pada remaja, tampak bahwa pada aspek
ini mengalami perubahanlperkembangan. Perkembangan moral ini
nampak jelas dalam lingkup teorl Kohlberg. Adanya perkembangan
dalam berpikir pada masa remaja, yang pada umumnya berada pada
tahap operasional formal, mem ungkinkan remaja untuk menyelesaikan
suatu masalah dan mempertanggung jawabkannya berdasarkan suatu
hipotesis, yaitu dengan memandang masalah yang dihadapi dengan
berbagai sudut pandang dan menyelesaikannya dengan mengambil
banyak faldor sebagai bahan pertimbangan. Kohlberg (dalam Llckona,
1976:32) menjelaskkan eiri dari pendekatan kognisi terhadap moralitas,
yaitu :
"'The most obvioU3 characteri3tic of cognlive developmental theories i3
their U3e of 30me type of stage concept, of some motion of age Hnked
sequential reorganization in the development of moralattlude-.
Yang artinya bahwa, perkembangan sikap moral itu terjadi m elalui
reorganisasi seeara berurutan, yang berhubungan dengan bertambahnya
usia.
Seiring dengan perkembangan moral remaja yang diharapkan telah
dapat menggantikan konsep moral anak-anak yang spesiflk dengan
prinsip-prinsip moral yang lebih umum. Remaja diharapkan telah dapat
memutuskan apa yang seharusnya dilakukan berdasarkan nilai-nilai dan
keputusannya sendiri. Pada umumnya remaja lelah dapat mencapai
pertimbangan moral pada tingkat Conventional, yang menunjukkan bahwa
kelompok dianggap sebagai sesuatu yang baik bagi dirinya sendiri. Pada
tingkat ini mereka tidak hanya ingin menyesuaikan diri dengan harapao-
harapan kelompok dalam lingkungannya tetapi juga menunjukkan sikap
ingin loyal, sikap ingin menjaga/menunjang ketertiban sosial.
Perkembangan remaja yang diharapkan dapat mencapai tingkat
conventional, yaltu moralitas yang berkembang dari kontrol eksternal ke
kontrol internal. Artinya remaja dapat memutuskan apa yang dilakukan
berdasarkan nilai-nllai dan keputusannya sendiri dengan bisa
membedakan mana yang baik dan mana yang buruk. Kemampuan
memilah mengapa hal tersebut baik atau buruk disebut dengan
pertimbangan moral (Kohlberg, 1995).
Dalam pertimbangan moral, pencapaian tahap berpikir
(perkembangan kognisi) memang diperlukan, tetapi itu saja belum
mencukupi terjadinya peningkatan tahap perkembangan pertimbangan
moral, karena kemampuan tersebut tidak secara langsung diikuti oleh
pertimbangan moral yang matang. Dengan dem ikian diperlukan fakfor
lain bagi perkembangan moral, yaitu keaneka ragaman pengalaman sosial
yang diperoleh dari orang tua melalui interaksinya dalam kehidupan
sehari- hari.
Orang dewasa atau orang tua akan merangsang anaknya untuk
berusaha menentukan apa yang harus dilakukan dengan mengarahkan
remaja untuk menyelesaikan tugas-tugas yang dihadapinya. Tugas-tugas
tersebut dipengaruhi oleh proses kognitif yang mengantarai perubahan
tingkah laku dan orang tua sebagai lingkungan sosial yang paling dulu
ditemui rem aja dapat memberikan bimbingan pada remaja untuk belajar
memahami nilai-nilai moral yang sesuai dengan harapan masyarakat
terhadap remaja melalui pembentukan pola pikirnya dalam memahami
dan mempelajari suatu permasalahan dan penyelesaiannya (Dr. Singgih
G., 2001).
Kohlberg membagi tahapan pertimbangan moral menjadi tiga
tingkatan dan tiap tingkatan terdiri atas dua tahap, yaitu :
1. Tingkat Pre Conventional, pada tingkat ini, individu peka terhadap
peraturan-peraturan yang berlatar belakang budaya dan terhadap
penilaian baik atau buruk, benar atau salah, yang diartikan dari akibat
fisik suatu tindakan dari enak tidaknya akibat itu, atau dari kekuasaan
fisik yang memberikan peraturan penilaian baik atau buruk. Pada
tingkat Pre Conventional ini terdapat dua tahapan, yaitu:
Heteronomous moralty (orlentasl hukuman dan kepatuhan) dan
Individualism, instrumental hedonistik (orientasi relativis instrumental)
2. Tlngkat Conventional, pada tingkat Int harapan-harapan keluarga,
kelompok atau bangsa dianggap sebagai sesuatu yang berharga pada
dirinya. tidak peduli terhadap akibat-akibat yang langsung dan yang
kelihatan. Pada tingkat Conventional Inl terdapat dua tahap, yaitu
Mutual interpersonal expectation relstionship and interpersonal
conform'y (orientasi ke kelompok "anak baik" atau "anak manis'') dan
Social Bnd conscience (orientasi hukum dan ketertiban).
3. Tingkat Post Conventional, pada tingkat inl individu terlepas dari
otoritas kelompok karena sudah memegang prinsip itu sendiri dari
kelompok itu atau tidak. Pada tingkat Post Conventional ini terdapat
dua tahap, yaitu Social contract and utilty and individual right
(orientasi kontrak sosial legalitas) dan Universal ethical principle
(orientasi azas eUka universaQ.
Pada umumnya pertimbangan moral remaja berada pada tingkat
conventional, namun ada pula remaja yang memiliki pertimbangan moral
pada tingkat pre conventiona/atau post conventional. Menurut Kohlberg
(1995), seorang individu menyukai pertimbangan moral yang berada satu
tahap di atas tahap perkembangannya sendiri dan menolak pertimbangan-
pertimbangan yang berasal dari tingkat di bawah tiogkat
perkembangannya. Walaupun demikian, tahap tertinggi yang m ungkin
dlcapai oleh remaja atau orang dewasa pad a umumnya, hanya pada
tahap 5 (orientasi kontrak sosial dan legalitas).
Menurut Parke, 1977, terbentuknya kemampuan pertlmbangan
moral individu tidak terlepas dari peran orang tua dalam membentuk
kepribadian dan perilaku remaja. Dengan demikian remaja dipengaruhi
oleh perlakuan yang diterlma melalui hubungan interaktlf antars orang tua
dan remaja yang dianggap sangat penting dalam proses sosialisasi, yang
tergantung pada sltuasl yang mempengaruhi pilihan orang tua pada jenls
dan kerasnya pelaksanaan disiplin (Levenson, 1973 dalam James W. V.
Zanden, 1985). Jadi bagalmanapun remaja dapat menyesualkan diri
dalam lingkungan sosial yang diperoleh melalui proses sosialisasinya
yang pertama kali diterima dari lingkungan keluarga.
Pada lingkungan ini yang menjadi figur utama dalam mengarahkan
dan membimbing remaja adalah orang tua, salah satunya adalah melalui
teknik disiplin yang diterapkan. Perlakuan yang ditampilkan orang tua
terhadap remaja di antaranya ada yang menggunakan hukuman fisik, ada
yang memperlihatkan ekspresi marah dan ada juga yang menasehati
untuk tidak mengulangi perbuatannya dengan menjelaskan alasannya bila
remaja melakukan kesalahan atau melakukan tindakan yang tidak sesuai
dengan kehendak orang tua.
Pada umumnya orang tua mempunyai satu teknik disiplin yang
lebih dominan sehingga memberi kekhasan tersendiri terhadap interaksi
dengan anak. Adanya atribusl yang dlperoleh balk darl dlrl remaja Itu
sendiri atau dari lingkungan dapat ditinjau dari corak interaksinya ini.
Menurut Hoffman, 1998 (Hoffman, Paris, Hall; 1994), ada tlga bentuk
teknik disiplin, yaitu :
1. Teknik disiplin Power Assertion
2. Teknik dlsiplin Love Withdrawal
3. Teknik disiplin Induction
Oalam teknik disiplin Power Assertion, orang tua yang cenderung
menggunakan kekerasan secara fisik, mengancam remaja dan mencabut
hak-hak remaja pada saat melakukan kesalahan atau perilaku yang tidak
sesuai dengan kehendak orang tua. Akibatnya tingkah laku remaja dalam
berinteraksi hanya ditujukan untuk menghindari hukuman (atribusi
eksternaQ, bukan timbul karena memahami kebutuhan dan perasaan
oranglain. Hal ini berarti, tingkah laku lebih dikendalikan oleh persepsi
remaja tentang faktor eksternal dalam dirinya (Hoffman, dalam Shaffer,
1994). Remaja cenderung hanya akan memperhatikan konsekuensi yang
akan diterimanya apabila kurang memperhatikan kebutuhan orang lain.
Tingkah laku ini dalam pertimbangan moral yang dlungkapkan oleh
Kohlberg (1995) berada pada tahap 1 dan 2, yaitu orientasi hukum dan
kepatuhan dan orientasi relatlvis instrumental.
Teknik disiplin Love Withdrawal dilakukan orang tua untuk
mengontrol tingkah laku remaja secara non fisik pada saat melakukan
kesalahan atau perilaku yang tldak sesuai dengan kehendak orang tua,
yaitu dengan mengabaikan, mengancam untuk meninggalkan remaja,
menolak untuk berbicara dan mendengarkan rem aja , serta menyatakan
ketidaksukaannya kepada anak. Teknik disiplin ini dapat menimbulkan
kecemasan pada remaja, dalam hal ini anak merasa cemas apabila
kehilangan dukungan, afeksi dan perlakuan dari orang tua (Hoffman,
1988). Akibatnya, remaja akan bertingkah laku sesuai dengan apa yang
dia.iarkan orang tua hanya apabila orang tua berada di sekitar remaja
(atribusi eksternaQ. Teknik disiplin ini juga menyebabkan terhambatnya
kom unikasi antara orang tua dan remaja, di mana remaja tidak
memperoleh penjelasan mengapa orang tua mengancam atau
mengabaikannya. Juga mengakibatkan remaja tidak mengetahui letak
kesalahannya terutam a akibat tingkah lakunya terhadap diri dan orang
lain. Dengan demikian dalam berinteraksi, remaja tidak memiliki
pengetahuan dan pemahaman tentang tingkah lakunya yang menyangkut
kebutuhan orang lain sehingga kemungkinan remaja dapat mengulangi
lingkah laku tersebut. Karena orang tua tidak memberikan informasi atas
kesalahan yang sudah dilakukan, remaja tidak mengetahui apa
kesalahannya dan merasa ditolak, sehingga remaja mencoba untuk tidak
mengulangi lagi. Teknik disiplin Love Withdrawal dalam pertimbangan
moral yang diungkapkan oleh Kohlberg (1995) berada pada tahap 2 dan
3, yaitu orientasi relativis instrumental dan orientasi ke kelompok " anak
balk" dan "anak manis".
Sedangkan dalam teknik disiplin Induction, komunikasi antara
orang tua dan remaja terjalin dengan baik, orang tua mengarahkan
tingkah laku remaja dengan cara memberikan penjelasan-penjelasan
mengenai konsekuensi dari tingkah laku baik atau buruk. Melalui
penjelasan yang diberikan orang tua, remaja dapat memahami letak
kesalahannya sehingga pengetahuan mengenai kesalahannya ini akan
dislmpan dalam Ingatannya dan tidak akan diulangi lagl. Penjelasan yang
diberikan orang tua membuat remaja memahami konsekuensi dari tingkah
lakunya baik terhadap orang lain maupun terhadap dirinya. Dalam diri
remaja akan timbul kesadaran mempertimbangkan kepentingan orang lain
selain kepentingan dirinya dalam berinteraksi, termasuk dalam
menyelesaikan masalah interpersonal dan memelihara relasi dengan
orang lain. Dengan demikian teknik disiplin ini dapat mengembangkan
kontrol pada remaja dalam jangka waktu yang lama dan tidak tergantung
pada sanksi eksternal. Berdasarkan teari atribusi (social information
processing) dari Hoffman (dalam Shaffer, 1994), bahwa penjelasan yang
diberikan orang tua mengenai tingkah laku remaja dapat meningkatkan
pemahaman remaja mengenai informasi, khususnya menyangkut alasan
mengapa tingkah laku remaja dikatakan salah dan bahwa rem aja
seharusnya merasa bersalah dan malu apabila mengulangi tingkah
lakunya. Melalui penjelasan yang diberikan orang tua, maka remaja akan
mengembangkan atrlbusl Internal dalam dirinya, dalam hal inl rem aja
merasa bersalah dan m alu apabila melakukan tingkah laku yang tidak
sesual dengan norm a soslal yang ada dan dalam tahap pertimbangan
moral berada pada tahap 4 (orientasi hukum dan ketertiban) dan tahap 5
(orientasi kontrak sosiallegalitas).
Pada umumnya orang tua memlilki satu teknik disiplln yang
dominan dipergunakan dalam membimbing dan mendidik anaknya.
dominasi masing-masing teknik disiplin berpengaruh terhadap
perkembangan kemampuan mempertimbangkan moral. Dengan demikian
hal di atas secara skematis dapat digambarkan sebagai berikut :
T em}; Disip1in
,
(13-15 tah~
- -
"
Proses '.
'-
"
. -
"~ ,---"'.I"
.
.
~.----------------------
_'
~
Socid IIff(N!lMti.ort.Proce.s&itte - - -,
: -Itftllrtlal t.tibuiat
f - - -, , . _
-
&ternm attrihttimt ~ ~- -
: -~---------------------
.
.
.
.
.
.
. \
/
I.Power~
2. Love Ifttkdrawal
3. I Mucti .ft
J. Pre Comllmtiond
-
HlilterorlGJOltO<t&Mora1io/
-lrrdi\lUiua1i.&1II, IP!8truItwItaI
Hed.odUtic
1. COfU'~
E~ ReI~ &;
IIIt4IPp4II'.-urlC~
-
Social & Corr.JCillftCe
3. Po&t COftPtKiarraJ
-
Social Contr/&'t and Utility
&; lrrdillidual a,kt
-
Unr:tlersaI Etktcal Prirrc.ple
Dari uraian di atas, dapat ditarik asumsi sebagai berikut :
. Tahap pertimbangan moral remaja bervariasi.
. Perkembangan tahap pertim bangan moral remajadipengaruhi oleh
bimbingan yang diberikan orang tua masing-masing m elalui teknik
disiplin yang diterapkan orang tua.
. Teknik disiplin yang diterapkan orang tua pada remaja juga dapat
bervariasi, ada yang menggunakan teknik penerapan disiplin Power
Assertion, Love Wlhdrawal atau Induction.
. Perkembangan kognitif menjadi dasar dalam mengembangkan
daya nalar dalam pertimbangan moral.
Berdasarkan asumsi di atas, maka hipotesis penelitian ini adalah :
"Terdapat hubungan antara teknlk penerapan dlsiplin orang tua dengan
pertimbangan moral remaja".
 Remaja merupakan aset bangsa yang memegang peranan penting 
 pada masa pembangunan yang akan datang. Sebagai penerus 
 pembangunan bangsa, seyogianya remaja memiliki kualitas fisik dan 
 mental yang dapat diandalkan agar dapat mengatasi tantangan yang 
 dihadapinya. Memang cukup banyak harapan masyarakat terhadap 
 sosok remaja sebagai generasi penerus, namun demikian remaja tetaplah 
 sebagai individu yang tengah berada pada masa peralihan antara masa 
 anak-anak dengan masa dewasa yang mengalami banyak perubahan, 
 baik pada dirinya maupun lingkungannya. 
 Seiring dengan harapan-harapan masyarakat yang m enuntut 
 remaja untuk berperilaku sesuai dengan nilai-nilai yang ada dalam 
 masyarakat, nam un masyarakat sendiri yang merupakan contoh model 
 bagi perilaku remaja malah memperlihatkan perilaku yang jauh dari nilai- 
 nilai etika dan moral. Hal ini tentu membuat remaja menjadi semakin 
 m udah meniru apa yang dllakukan oleh masyarakat. 
 Seperti banyak diberitakan dalam media massa tentang perilaku 
 masyarakat yang dapat menjadi contoh bagi remaja, berupa terjadinya 
 tawuran antar masyarakat, perilaku tidak disiplin, ketidakadilan, mencuri 
 dan sebagainya, tentunya akan menjadi dilema bagi perilaku remaja itu 
 sendiri. Oi satu sisi remaja dituntut untuk berlaku jujur, adil, taat hukum 
 dan sebagainya, nam un di sisi lain lingkungan masyarakat secara tidak 
 langsung memberi contoh yang buruk. 
 Remaja yang memasuki masa peralihan dari masa anak menuju 
 masa dewasa mengalami perubahan, di antaranya perubahan kognitif dan 
 peranan di dalam masyarakat. Sejalan dengan perkembangan remaja 
 yang memasuki cara berpikir operasional formal, remaja m ulai 
 membutuhkan pertimbangan-pertimbangan dan argumen yang mendasari 
 tentang mengapa hal tersebut baik dan mengapa hal tersebut buruk. 
 Karena berdasarkan kenyataan yang kerapkali terjadi pada remaja, 
 seperti memberi contekan pada teman ketika ulangan, mencontek, 
 membolos dari sekolah, berkelahi, mencoret-coret atau merusak fasilitas 
 umum, menonton film-film porno, merokok atau penyalahgunaan obat- 
 obat terlarang (Plklran Rakyat, 2002), tentu menimbulkan pertanyaan, 
 apa yang sebenarnya menjadi pertimbangan remaja dalam melakukan 
 hal tersebut? 
 Untuk dapat mempertimbangkan apa yang dianggap balk atau 
 buruk, benar atau salah, diperlukan kematangan moral dari remaja itu 
 sendiri. Pertimbangan-pertimbangan inilah yang menjadi indikator dari 
 tahap kematangan moral. Sepertl diungkapkan oleh Kohlberg (1995) 
 bahwa, pertimbangan moral merupakan suatu proses berpikir di dalam diri 
 seseorang dalam memutuskan masalah 80sial yang menyangkut moral, 
 yaitu mengapa sesuatu hal dianggap benar atau salah. Dengan demikian 
 pertimbangan mengapa sesuatu hal dianggap baik atau buruk, benar atau 
 salah, diperlukan remaja untuk mendasari pengambilan keputusan 
 moralnya. 
 Menurut Dr. Sugarda T. SpA(k) (Plklran Rakyat, 2000), 
 peralihan moralitas dari moralitas anak ke moralitas remaja, meliputi 
 perubahfln sikap dan nilai-nilai yang mendasari pembentukan konsep 
 moralnya sehingga sesuai dengan moralitas dewasa dan mampu 
 mengendalikan tingkah lakunya sendiri. Dengan demikian apabila rem aja 
 melakukan suatu aktivitas yang cenderung negatif bukanlah semata-mata 
 ditentukan oleh diri remaja itu 8endiri melainkan dipengaruhi pula oleh 
 interaksi antara remaja dengan komponen-komponen lingkungannya yang 
 salah satunya adalah orang tua yang menempati posisi sangat 
 berpengaruh dalam perkembangan remaja. 
 Hal ini berarti bahwa, orang tua memlllki kewajlban untuk sedapal 
 mungkin mengusahakan remaja menjadi individu yang sesuai dengan 
 moral. Karena arti dan nilai moral itu sendiri tidak hanya menyangkut diri 
 sendiri letapi menyangkut pula dlri orang lain ataupun masyarakat luas. 
 Oleh karena itu melalui caranya sendiri orang tua menanamkan standar 
 nUai, mem perkuat sikap dan tlngkah laku yang dianggap positif dan 
 memperlemah sikap dan tingkah laku yang dianggap negatif agar rem aja 
 mampu melakukan banyak hal demi kemandiriannya. 
 Pada kenyataannya tidak sem ua orang tua atau orang dewasa 
 dapat membimbing anaknya menuju tahap perkembangan pertimbangan 
 moral yang lebih matang. Tidak sedikit orang tua atau orang dewasa 
 yang memberi contoh buruk secara tidak langsung pada anaknya, seperti 
 fenomena yang terjadi pad a orang tua yang melarang anak untuk 
 berbohong, tetapi orang tua sendiri melakukan hal itu. Dengan demikian 
 dalam keluarga diperlukan usaha orang tua untuk mengajarkan norma- 
 norma melalui perlakuan yang ditampilkan oleh orang tua dalam 
 kehidupan sehari-hari. 
 Disamping itu, orang tua juga mendorong terciptanya dialog-dialog 
 mengenai nilai-nilai yang harus dianut oleh remaja. Nilai-nilai tersebut 
 perlu disesuaikan dengan konsep-konsep moral yang berlaku umum yang 
 atau salah secara moral diperoleh dart IIngkungan keluarga khususnya 
 dart orang tUB. Berdasarkan pengamatan secara umum pada beberapa 
 orang tua dalam membimbing dan mendidik remaja, ada berbagai cara 
 yang ditunjukkan, seperti apabila anaknya melakukan hal yang tldak 
 sesuai dengan kehendak orang tua, maka ada orang tua yang mengajak 
 anaknya untuk berdiskusi, ada yang tidak mempedulikan dengan 
 membiarkan anak melakukan tindakan yang tidak sesuai dengan 
 kehendak orang tua, ada yang langsung marah dan menghukum 
 anaknya, dan sebagainya. 
 Berkaitan dengan hal tersebut di atas, Hoffman (1994) 
 mengklasifikasikan teknik disiplin ini menjadi tiga, pertama teknik disiplin 
 Power Assertion yang mengutamakan kekuasaan orang tua dengan 
 melibatkan kekuatan fisik apabila remaja tidak patuh kepada orang tua. 
 Kedua teknik disiplin Love withdrawal yang secara langsung 
 mengungkapkan ketidaksenangan orang tua terhadap remaja dengan 
 tidak mengacuhkan atau menolak berblcara atau mendengarkan ketika 
 remaja melakukan kesalahan atau tidak menuruti kehendak orang tua. 
 Terakhir teknik disiplin Induction yang mengutamakan penjelasan 
 mengenai akibat yang dapat timbul dari perilakunya yang baik atau buruk 
 untuk dirinya sendiri atau untuk orang lain. 

