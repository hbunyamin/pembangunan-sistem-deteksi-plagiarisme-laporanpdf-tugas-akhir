Indonesia sebagai negara berkembang sedang giat melaksanakan 
 pembangunan di segala bidang. Pembangunan yang dilakukan tidak terlepas dari 
 globalisasi yang sedang melanda dunia, secara langsung maupun tidak langsung 
 membawa masyarakat Indonesia ke era kehidupan yang lebih maju. 
 Dalam melakukan usaha-usaha pembangunan, sering dikemukakan bahwa 
 tenaga kerja yang berkualitas merupakan salah satu unsur penting, seperti yang 
 tertulis dalam GBHN mengenai jumlah penduduk yang sangat besar apabila 
 benar-benar dibina dan dikerahkan sebagai tenaga kerja yang efektif merupakan 
 modal pembangunan yang besar dan sangat menguntungkan bagi usaha-usaha 
 pembangunan di segala bidang. Penjelasan di dalam GBHN tersebut secara 
 implisit mengungkapkan pentingnya manusia sebagai tenaga kerja, karena 
 manusia merupakan pengelola sumber-sumber daya lain yang ada. 
 Tenaga kerja yang berkualitas menentukan hasil akhir yang ingin dicapai, oleh 
 karena itu setiap organisasi diharapkan mengetahui faktor-faktor yang dapat 
 memberi kemungkinan untuk berhasil. Untuk mencapai tujuan dan sasaran 
 tersebut, organisasi memiliki berbagai sumber daya seperti yang utama sumber 
 daya manusia dan selain itu yaitu sumber daya material, dana, informasi, waktu. 
 Dapat dikatakan sumber daya manusia memegang peranan penting dan 
 merupakan modal utama dalam menunjang keberhasilan organisasi, karena 
 manusia mempunyai kecakapan pengetahuan dan kemampuan yang dapat 
 dikembangkan oleh organisasi. Sumber daya manusia dalam organisasi ini 
 tentunya tidak terbatas pada pria saja, wan ita pun merupakan sumber daya yang 
 patut diperhatikan. 
 Dalam era modemisasi, terbuka kesempatan bagi kaum wanita untuk 
 mengembangkan diri, diantaranya kesempatan mengenyam pendidikan tinggi dan 
 berperan serta secara konkrit dalam kegiatan pembangunan bangsa. Dengan 
 demikian, wan ita mempunyai hak dan kewajiban yang sama dengan pria, begitu 
 pula wan ita dapat bekerja secara aktif di luar rumah. Saat ini, wan ita bekerja tidak 
 hanya untuk membantu perekonomian keluarga melainkan juga ingin 
 mengaktualisasikan dirinya. 
 Seperti yang diungkapkan oleh Peter & Hamsen (Ibnu Umar, 1982), 
 "Dengan bekerja seseorang dapat mencapai identitas diri, mencapai tingkat sosial 
 tertentu dalam masyarakat, memperoleh kemungkinan untuk mengadakan kontak 
 sosial, merasa senang dan lepas dari rasa bosan, melakukan sesuatu yang 
 konstruktif dan kreatif serta dapat menyumbangkan ide-ide." 
 Meniti karir bukanlah monopoli kaum pria, tetapi juga menjadi aspirasi dan 
 kegiatan para wan ita di berbagai bidang pekerjaan, misalnya di bidang-bidang 
 manajemen, politik, kehutanan, pertambangan, perdagangan dan lain sebagainya. 
 Di lain pihak, wan ita menjalankan peran feminin yang berorientasi memelihara 
 dan menjaga keluarga, sedangkan pria sebagai kepala keluarga, berprestasi bagi 
 kaum wanita di dunia kerja bukan merupakan hal yang feminin. 
 Sebagaimana yang dikatakan oleh DR. Parwati Soepangat M.A (Disertasi, 
 1987), "Wan ita jarang dilihat dari segi aktualisasi diri dan kebutuhannya. Wanita 
 lebih sering dikaitkan dengan peran yang harus dilakukan sebagai manusia. 
 Wanita lebih sering dikaitkan dengan peran yang harus dilakukan sebagai peran 
 feminin. Selain itu wan ita jarang "menampilkan" dirinya. Hal ini bukan saja 
 menyangkut aktualisasi diri yang belum terlatih, namun dapat saja karena masalah 
 yang serba takut, takut ini, takut itu. Dalam masalah budaya pun wan ita sudah 
 dibentuk harus seperti ini, seperti itu, apabila melanggar maka hal itu tidak baik 
 bagi wan ita". 
 Di Indonesia, data yang didapat dari Departemen Tenaga Kerja (Depnaker), 
 menunjukkan pada tahun 1995, pekerja wanita sebanyak 185.676 (0.103%) orang 
 dan laki-Iaki sebanyak 212.624 (0.118%) orang. Namun pada tahun berikutnya 
 (1996) hingga tahun 2000, pekerja wan ita menjadi lebih banyak bila dibandingkan 
 dengan pekerja pria. Pada tahun 2000, pekerja wanita sebanyak 183.310 (0.087%) 
 orang dan laki-laki sebanyak 137.448 orang (0.065%). Data terse but menunjukkan 
 proporsi peningkatan jumlah tenaga kerja wan ita yang terlihat cukup signifikan. 
 Sejalan dengan peningkatan jumlah pekerja wan ita tersebut, masih terdapat 
 kendala dalam pencapaian kedudukan yang lebih tinggi, yaitu pandangan yang 
 ada pada masyarakat Indonesia. Membuat wan ita tetap dibayangi peran tradisional 
 yaitu menjaga dan memelihara rumah tangga, di luar itu perannya dianggap tidak 
 wajar, dengan kata lain di luar pekerjaan rumah tangga adalah melanggar 
 kodratnya sebagai wanita. Bekerja bagi wan ita semata-mata hanya sebatas 
 membantu perekonomian keluarga bukan meniti karir. 
 Selain itu ada pula pandangan mengenai wan ita karir, yaitu apabila berhasil 
 berprestasi di dunia kerja dan berpenghasilan lebih tinggi dari suami maka akan 
 bersikap tidak etis dalam rumah tangga, kurang menghargai sampai terlalu 
 dominan terhadap pasangannya (Femina, 24/XXII, 2003). Menurut Atkinson & 
 Boles (1984), suami yang memiliki penghasilan lebih rendah dari istrinya akan 
 merasa sebagai kelompok dominan yang takut kehilangan kekuasaannya, merasa 
 harga dirinya menjadi berkurang karena istri berpenghasilan lebih tinggi. Persepsi 
 akan pandangan masyarakat, ditambah dengan adanya perasaan yang kurang 
 feminin karena ketertarikan kuat terhadap kerja, memunculkan kecemasan pada 
 wan ita jika dikaitkan dengan keinginannya untuk mengaktualisasikan dirinya 
 dalam dunia kerja, mengingat setiap kesuksesan memunculkan konsekuensi lain 
 berupa pandangan negatif dari masyarakat mengenai jati diri kewanitaannya 
 (Walker, 1970). 
 Kecemasan yang timbul pada diri wanita karir tersebut muncul jika memiliki 
 aspirasi yang cukup tinggi, seperti yang diungkapkan oleh Martina Horner 
 (Shaw & Costanzo, 1 972), fear of success lebih banyak terdapat pada wan ita 
 yang berorientasi pada karir, dimana lebih merasakan konflik akibat keinginan 
 dan kesempatan sukses dalam karir, dengan tuntutan serta harapan masyarakat 
 pad a mereka sesuai dengan perannya sebagai wanita. 
 Secara tidak langsung, fear of success terjadi akibat dari identiflkasi wan ita 
 dengan peran jenis kelamin yang diajarkan oleh orang tua dan lingkungan. Orang 
 tua berperan besar dalam pembentukan peran gender anak, hal ini dikarenakan 
 interaksi mereka yang meliputi setuju atau tidak setuju dengan tingkah laku anak 
 serta menjadi model bagi tingkah laku anak selanjutnya. Perkataan dan 
 perlakukan orang tua, seperti anak laki-Iaki tidak boleh menangis, pemberian 
 mainan masak-masakan, boneka serta rumah-rumahan bagi anak perempuan, 
 memungkinkan terjadinya proses pengidentifikasian gender terse but. Di sekolah 
 pun, guru berperan dalam pembentukan stereotip gender. Umumnya, para guru 
 mengharapkan bidang-bidang yang berbeda bagi anak laki-Iaki dan perempuan, 
 sehingga perlakukannya pun menjadi berbeda. Tingkah laku yang diharuskan di 
 sekolah juga menjadi pendorong pembedaan. Anak laki-Iaki biasanya diberi 
 kegiatan mengangkat meja sedangkan wan ita diharapkan mampu mengurus tugas- 
 tugas kesekretariatan. 
 Dengan adanya pembedaan perlakukan yang diberikan pada masa kanak- 
 kanak hingga dewasa, menjadikan seorang wan ita karir mengalami konflik, di 
 satu sisi ingin meraih sukses dalam dunia kerja namun disisi lain lingkungan akan 
 menilai negatif ataskeberhasilannya. Disamping itu, dalam dunia kerja kompetisi 
 tidak dapat dielakkan, terlebih jika menghadapi rekan kerja yang berlainan jenis. 
 Hurlock (1980), mengatakan wanita cenderung memandang kemampuan dirinya 
 lebih rendah dibandingkan pria sehingga perbandingan kemampuan dengan pria 
 akan menimbulkan kecemasan pada dirinya karena apabila temyata dinilai 
 memiliki kemampuan yang lebih baik dari pria, dan hal ini dinilai menyimpang 
 dari peran jenis kelaminnya. Pemyataan tersebut didukung oleh pendapat Friez 
 (1978), wanita cenderung kurang percaya diri dalam menghadapi situasi 
 kompetitif, dimana wanita selalu merasa dirinya kurang mampujika dibandingkan 
 dengan pria. Keadaan ini menimbulkan kecemasan pada diri wanita yang dapat 
 mempengaruhi hasil kerja dalam mencapai prestasi yang diinginkan yaitu 
 mencapaikesuksesan. 
 Secara umum, kesuksesan pada wan ita karir dalam suatu organisasi dapat 
 dilihat dari segi posisi atau jabatan. Semakin tinggi jabatan yang dicapai sejauh itu 
 pula kesukesan diraih. Jabatan manajer dalam organisasi dapat dikatakan sebagai 
 jabatan yang tinggi dimana manajer terbagi dalam tiga level manajer yaitu 
 manajer level atas, menengah dan bawah, untuk setiap level memiliki tugas dan 
 tanggung jawab yang berbeda-beda. Makin tinggi level yang dicapai maka makin 
 tinggi pula tugas dan tanggungjawab, misalnya level manajer atas memiliki tugas 
 dan tanggung jawab yang lebih berat bila dibandingkan dengan level manajer 
 menengah. Karena tugas dan tanggungjawab tersebut makin tinggi maka konflik 
 yang dirasakan oleh wan ita karir yang telah berkeluarga pun semakin meningkat. 
 Kontlik antara keinginan untuk tetap eksis di dunia kerja yaitu dengan memenuhi 
 tuntutan perusahaan dengan melaksanakan tugas dan tanggung jawab dan tugas 
 sebagai ibu dan istri dalam dunia rumah tangga, kedua dunia ini menuntut waktu 
 dan perhatian yang sarna besar. Disinilah fenomena fear of success terjadi dan 
 akan mempengaruhi hasil kerja para wan ita karir. Di satu sisi, perusahaan 
 mengharapkan hasil kerja yang optimal, di sisi lain keluarga mengharapkan 
 perhatian yang selanjutnya diiringi dengan konsekuensi negatifyang didapat dari 
 masyarakat yaitu berupa penolakan so sial karena dianggap tidak feminin karena 
 lebih mementingkan karir daripada keluarga. Terlebih pada wan ita karir yang 
 menduduki posisi manajer dimana pada posisi ini menuntut waktu dan tenaga 
 yang lebih. 
 Pada penelitian ini sam pel yang diambil adalah wanita karir yang berada pada 
 posisi manajer level menengah dan menejer level bawah, dimana tugas dan 
 tanggung jawab yang dimiliki berat namun tetap pada kedua level ini terdapat 
 perbedaan yang signifIkan, secara umum tugas dan tanggungjawab level manajer 
 menengah lebih berat dibandingkan level manajer bawah.Peneliti telah 
 melakukan survey awal di perusahaan, survey dilakukan dengan melakukan 
 wawancara singkat kepada delapan responden yaitu empat wanita karir yang telah 
 berkeluarga yang berada pada posisi level manajer menengah dan empat yang 
 berada pada posisi manajer level bawah. Para responden mengungkapkan 
 ketakutannya akan sukses, salah satu alasan yang diungkapkan adalah karena 
 kemungkinan posisi suami akan lebih rendah darinya dan oleh lingkungan 
 keluarga maupun lingkungan disekitar tempat tinggal akan dinilai tidak feminin 
 karena dianggap ingin melebihi suami serta tidak menjalankan peran sebagai ibu 
 dan istri dengan baik karena lebih mementingkan karir, waktu yang dihabiskan 
 lebih banyak diluar rumah (di kantor) dari pada di rumah. Namun disamping itu 
 pekerjaan menuntut untuk menghasilkan kinerja yang optimal, yaitu 
 melaksanakan tugas dan tanggung jawab sesuai dengan jabatan yang diduduki. 
 Terlebih terdapat perbedaan kecemasan yang dialami oleh manajer menengah dan 
 manajer bawah, para manajer menengah merasakan konflik yang lebih berat 
 karena tugas dan tanggung jawab yang lebih dibandingkan manajer bawah. 
 Berdasarkan wawancara, tiga dari empat wanita karir manajer level menengah 
 mengatakan merasakan kecemasan dalam meraih jenjang karir. Sedangkan pada 
 wanita karir manajer level bawah, dua dari empat wanita mengatakan mengalami 
 kecemasan. Para wanita karir tersebut merasakan konflik dalam dirinya, pekerjaan 
 dan keluarga menuntut perhatian yang sarna besamya. 
 Fenomena ini dijelaskan kembali oleh pemyataan Yati Utoyo Lubis (Femina 
 36/XXV, 2002), wanita takut bila 'terbang tinggi' dalam karir, itu sebabnya ada 
 wanita yang karimya meningkat menjadi merasa takut merasa bersalah. Majalah 
 Femina (15/XIX, 2001) menuliskan, keberhasilan seorang wanita dalam pekerjaan 
 dianggap tidak ada artinya bila kehidupan keluarga atau rumah tangga tidak 
 terurus dengan baik. Hasil angket wan ita bekerja dari majalah Femina (15/XIX, 
 2001), menyebutkan bahwa wan ita karir yang sudah menikah menunjukkan 
 kecemasan yang berhubungan dengan perannya sebagai ibu rumah tangga. 
 Mereka merasa cemas bila tidak dapat hadir saat dibutuhkan oleh anak dan suami 
 dan penilaian bahwa mereka lebih mementingkan pekerjaan dari pada 
 keluarganya. 
 Berdasarkan fenomena terse but, menunjukkan adanya kecemasan pada wanita 
 dalam meniti karir. Kecemasan yang memungkinkan terhambatnya pemanfaatan 
 potensi yang ada pada diri wan ita untuk berkarir yang sedikit banyak membawa 
 konsekuensi terhadap hasil kerja. Oleh karenanya, peneliti tertarik untuk 
 melakukan suatu penelitian mengenai perbedaan antara fear of success yang 
 dialami oleh wan ita karir yang telah berkeluarga yang berada pada level manajer 
 menengah dan manajer level bawah. 
 Apakah terdapat perbedaan antara fear of success pada wan ita karir 
 berkeluarga yang berada pada manajer level menengah dan manajer level bawah 
 di P.T 'X' kota Bandung? 
 Maksud penelitian ini adalah untuk mendapatkan data mengenai gambaran 
 fear of success pada wan ita karir bekeluarga yang berada pada manajer level 
 menengah dan manajer level bawah. 
 Sedangkan tujuan dari penelitian ini untuk mengetahui perbedaan antara}ear 
 of success pada wan ita karir berkeluarga yang berada pada manajer level 
 menengah dan manajer level bawah. 
 1. Kegunaan Teoretik 
 a. Melalui penelitian ini diharapkan dapat memberi pengetahuan khususnya 
 bidang psikologi wan ita, mengenai }ear of success pada wan ita karir, 
 khususnya perbedaan antarafear of success pada wanita karir berkeluarga 
 yang berada pada manajer level menengah dan manajer level bawah. 
 b. HasH penelitian ini diharapkan menjadi informasi tambahan bagi peneliti 
 lain yang tertarik untuk mengadakan penelitian dalam topik yang sarna. 
 2. Kegunaan Praktis 
 a. Memberikan sumbangan pemikiran kepada pihak perusahaan mengenai 
 konsep psikologi tentang fenomena }ear of success yang terdapat pada 
 wanita karir, juga diharapkan wanita karir tersebut dapat introspeksi diri 
 sampai sejauh mana takut akan sukses mempengaruhi kehidupan kerjanya. 
 b. Memberikan sumbangan pengetahuan dalam menangani masalah-masalah 
 wanita dalam kehidupan sehari-hari. Khususnya terhadap fenomena}ear of 
 success yang muncul pada wan ita karir yang telah berkeluarga maupun 
 belum. 
 Masyarakat pada umumnya menghargai prestasi tinggi yang berhasil dicapai. 
 Bagi individu, motif prestasi tinggi dipersepsi sebagai kondisi yang 
 memungkinkan peraihan prestasi setinggi mungkin serta membangkitkan harapan 
 besar akan terwujudnya keinginan karena prestasi tersebut sesuai dengan tuntutan 
 masyarakat atau lingkungan sosial. 
 Dukungan masyarakat atau lingkungan sosial terhadap motif berprestasi pria 
 lebih memicu untuk menunjukkan hasil kerja seoptimal mungkin, tetapi tidak 
 demikian halnya pada wan ita. Wanita dalam usahanya berkiprah dalam dunia 
 kerja masih dibayangi nilai-nilai masyarakat yaitu wan ita diharapkan berada di 
 rumah mengurus dan memelihara rumah tangga dan menjalankan peran sebagai 
 ibu dan istri dengan baik, yang diturunkan dari generasi ke generasi dan 
 senantiasa bertahan dan dianggap benar oleh masyarakat (Allport, 1964). 
 Pembedaan peran telah diajarkan oleh lingkungan, terutama lingkungan 
 terdekat yaitu orang tua, sejak seorang anak dilahirkan melalui proses belajar yang 
 diperolehnya, seorang anak diajarkan berprilaku sesuai dengan jenis kelaminnya. 
 Pengajaran tersebut mengarahkan anak untuk bertingkah laku sesuai dengan jenis 
 kelamin dan sesuai dengan harapan lingkungan. Menurut Williams dan Best 
 (1990), wan ita diterima sebagai individu yang sentimental, penurut dan 
 mempercayai ramalan (superstitious). Sedangkan pria, digambarkan sebagai 
 individu yang memiliki jiwa petualang, bersemangat dan mandiri. 
 Apabila telah memasuki dunia kerja, baik pria maupun wanita akan menemui 
 situasi kerja yang kompetitif, jika seseorang terjun ke dalamnya mau tidak mau 
 harus berkompetisi dengan rekan sekerjanya. Sementara di sisi lain, wanita 
 cenderung menurunkan motif berprestasinya dalam situasi kompetitif, karena 
 mendapatkan konsekuensi negatif seperti kehilangan harkat kewanitaan, lebih 
 lanjut lagi, situasi prestasi merupakan situasi kompetitif, dimana kompetisi dapat 
 dilihat sebagai suatu bentuk sublimasi agresi (Horner, 1972). Setiap tingkah laku 
 agresi yang dilakukan oleh wan ita, diberi konsekuensi negatif oleh masyarakat 
 sebagai sesuatu yang tidak feminin. Oleh karena itu, wanita yang berhasil dalam 
 situasi kerja khususnya prestasi kerja non rumah tangga, terutama dalam 
 persaingannya dengan pria, dirasakan tidak feminin. 
 Adanya harapan terhadap wan ita akan peran jenis tersebut memunculkan 
 perasaan ditinggal oleh teman-temannya apabila berhasil dalam kerja. Pada 
 wan ita, dukungan masyarakat dan lingkungan merupakan hal penting dalam 
 berprestasi, sementara pada pria lebih dipengaruhi oleh keinginan diri sendiri 
 (Verrof, 1969). Di lingkungan kerja, seorang wanita dihadapkan pada realita 
 kewanitaannya. Dalam berpretasi tetap membutuhkan dukungan emosional dan 
 penerimaan dari lingkungan sosialnya. Bagi pekerja wanita yang berkeluarga, 
 suami dan anak erat dikaitkan dengan pengakuan pre stasi dan dukungan 
 emosional. 
 Variabel yang akan diteliti dalam penelitian ini adalahftar of success. Adapun 
 teori utama yang mendukung adalah teori dari Martina Horner (1972), yang 
 mendefmisikanfear of success yaitu kecemasan wanita akan konsekuensi negatif 
 yang menyertai kesuksesannya, yang berupa hilangnya sifat kewanitaan, 
 kemudian mulai beranggapan subyektif pada diri sendiri mengenai kurangnya 
 penghargaan masyarakat yang akan diikuti dengan penolakan sosial dari 
 lingkungan. Adapun aspek-aspek dalam fear of success ada tiga, yang pertama 
 adalah loss of jeminity, yaitu perasaan berkurangnya nilai kewanitaan yang 
 diartikan kurang dapatnya seorang wanita tampil dengan sifat-sifat feminin. 
 Adapun aspek kedua adalah, loss of social self esteem yaitu adanya anggapan 
 subyektif pada diri wanita yang bersangkutan akan kurangnya penghargaan 
 masyarakat terhadap dirinya akibat kesuksesan dalam karir sehingga dipandang 
 tidak lagi feminin. Sedangkan aspek yang ketiga adalah social rejection, dalam 
 hal ini penolakan sosial diartikan sebagai suatu bentuk sikap atau tingkah laku 
 dari lingkungan yang menolak kehadiran, menjauhkan diri atau berupa cemoohan 
 dan penilaian negatif terhadap seorang wan ita akibat kesuksesan yang diraih. 
 Horner (1972) juga mengemukakan: 
 Dikatakan bahwa wanita senantiasa mempertimbangkan penilaian akan 
 feminitas dalam pencapaian sukses. Dan keberhasilan pada wan ita selalu 
 dihubungkan dengan kehilangan feminitas, kehilangan penghargaan dari 
 masyarakat hingga penolakan sosial atau konbinasi dari hal-hal tersebut. 
 Fear of success merupakan hasil dari sosialisasi khusus dimana kondisi ini 
 mengarah pada antisipasi wanita akan kehilangan feminitas yang kemudian akan 
 mengarahkan wanita untuk merasa takut meraih sukses dan memperkirakan 
 kehilangan perhargaan so sial serta penolakan so sial. Lebih spesiftk lagi, Horner 
 (1972), mengatakan bahwafear of success lebih banyak dialami oleh wanita yang 
 lebih berorientasi pada karir (dari pada wanita yang kurang termotivasi) yang 
 lebih merasakan konflik-akibat keinginan dan kesempatan untuk sukses dalam 
 karir, dengan tuntutan serta harapan masyarakat sesuai dengan perannya sebagai 
 wan ita. Sesungguhnya, wanita memiliki kecenderungan yang lebih besar untuk 
 mengalamiftar of success dibandingkan dengan pria (Middle Brooks, 1980). 
 Fear of success dipengaruhi oleh beberapa faktor yaitu faktor individu dan 
 faktor lingkungan (Horner, 1972). Untuk penjelasan faktor individu, ftar of 
 success lebih banyak terdapat pada wanita yang memiliki orientasi terhadap dunia 
 kerja (pencapaian prestasi kerja). Sedangkan pada wanita kurang termotivasi 
 dalam dunia kerja, sukses bukanlah hal yang mudah untuk diraih dan bukan 
 tujuan utama. Oleh karena itu mereka tidak terlalu mempermasalahkan 
 kesuksesan tersebut. Sebaliknya, pada wan ita yang berorientasi pada karir, sukses 
 merupakan suatu kemungkinan untuk diperoleh dan menjadi tujuan utamanya. 
 Oleh karena itu, mereka lebih merasakan kontlik yang muncul antara keinginan 
 dan kesempatan sukses meniti karir, dengan tuntutan serta harapan dari 
 masyarakat sesuai dengan perannya sebagai wan ita. Akibatnya, wan ita 
 menyesuaikan diri dengan peran jenis kelaminnya dengan cara memilih pekerjaan 
 feminin serta tetap berada di posisi yang lebih rendah dibandingkan pria di dalam 
 pekerjaannya (Stein & Bailey, 1973). 
 Adapun faktor lingkungan dibagi menjadi tiga bagian yaitu situasi, sikap 
 pasangan dan sikap lingkungan. Untuk penjelasan mengenai situasi sebagai faktor 
 yang pertama, Horner (Atkinson, 1972), mengatakanfearofsuccessmunculjika 
 wanita dihadapkan pada situasi kompetisi, dimana kemampuannya dinilai 
 berdasarkan standar tertentu dan dibandingkan dengan orang lain. Fear of success 
 akan semakin tampak jika wanita harus berkompetisi dengan pria. Wan ita 
 cenderung kurang percaya diri dalam menghadapi situasi kompetitif dim ana 
 wanita selalu merasa dirinya kurang mampu jika dibandingkan dengan pria 
 (Friez, 1971). Perasaan bahwa berhasil mengungguli pria akan munculkan 
 konsekuensi negatif yang menyertai keunggulan tersebut. Hurlock (1980), 
 mengatakan wan ita dengan konsep tradisional cenderung memandang 
 kemampuan dirinya lebih rendah dibanding dengan pria sehingga perbandingan 
 kemampuan tersebut akan menimbulkan kecemasan pada dirinya karena ternyata 
 dinilai memiliki kemampuan yang lebih bila dibandingkan dengan pria, sehingga 
 dikatakan menyimpang dari peran jenis kelaminnya. 
 Adapun faktor yang kedua yaitu sikap pasangan, sikap pria terhadap pasangan 
 wanitanya yang bekerja atau wanita yang berprestasi memperbesar kecenderungan 
 muncuInya fear of success pada dirinya, terlebih jika harus bersaing bersama. 
 Disamping itu, ada tidaknya pasangan pada wanita juga bepengaruh pada 
 muncuInya fear of success. Belum adanya pasangan tetap akan mempengaruhi 
 penilaian negatif dari lingkungan yang menyertai kesuksesan, karena penilaian 
 negatif selalu diikuti dengan penolakan sosial yang akan membuatnya menjadi 
 lebih sulit untuk mencari pasangan (Bardwick, 1971). Adanya pasangan tetap 
 bagi wan ita akan memberikan perasaan aman karena telah memenuhi tuntutan 
 mendapatkan pasangan, sehingga penilaian negatif dari lingkungan tidak terlalu 
 berpengaruh pada dirinya, kecuali penilaian negatif datang dari pasangannya. 
 Sedangkan faktor yang ketiga yaitu sikap lingkungan, masyarakat tidak 
 mengharapkan wan ita meraih sukses diluar rumah terutama di bidang-bidang 
 maskulin. Jika wanita sukses, masyarakat memberikan sangsi negatif. Frieze 
 (1978), mengemukakan akibat dari sikap masyarakat, wanita cenderung untuk 
 tidak menampilkan pre stasi yang optimal karena tidak ingin menyalahkan harapan 
 peran jenisnya. Bardwick (1971) mengatakan, pada sebagian besar wan ita 
 kesuksesan dianggap mengancam hubungan sosial dengan lingkungan, karena 
 akan diikuti oleh penolakan sosial. 
 Teori lain yang mendukung fenomena fear of success adalah teori dari Shaw 
 & Constanzo (1982), yang mengatakan bahwa fear of success dapat dilihat 
 sebagai anxiety yang berasal dari kontlik peran. Oleh karena itu, tuntutan dari 
 situasi prestasi yang kompetitif pada wan ita dengan tuntutan peran jenis kelamin 
 membawanya agar selalu mengalah dan tidak kompetitif. Untuk mencapai karir 
 kerja yang optimal, fear of success tentu akan mengganggu. 
 Oalam dunia kerja, ketakutan atau kecemasan tersebut akan terlihat pada 
 prilaku kerja, hal ini didukung oleh pendapat dari Freud (Calvin hall, 1995), 
 kecemasan sinonim dengan perasaan takut terhadap sesuatu dari dunia luar (dalam 
 hal ini ketakutan akan kehilangan sisi feminitas yang diikuti oleh penolakan 
 sosial), dan kecemasan tersebut bagi wanita karir akan mempengaruhi hasil 
 kerjanya. Keinginan untuk berprestasi dalam dunia kerja pasti ada terutama pada 
 wan ita yang memiliki orientasi karir, indikator bahwa prestasi telah dicapai salah 
 satunya adalah dengan melihat posisi atau jabatan di dalam perusahaan. Semakin 
 tinggi jabatan yang dicapai maka diasumsikan semakin tinggi pula prestasi yang 
 dicapai dalam pekerjaan, namun semakin tinggi jabatan tentunya semakin berat 
 pula tugas dan tanggung jawab yang diharus dilaksanakan. Pada mulanya 
 sebagian besar wan ita bekerja pada perkerjaan yang dianggap sesuai dengan 
 "kodrat" perempuan misalnya perawat, guru, buruh pabrik, pembantu rumah 
 tangga, dan lain-lain. Namun akhir-akhir ini tidak sedikit wan ita yang bekerja di 
 bidang maskulin, jabatan mereka tidak terbatas pada fungsi staf seperti manajer 
 personalia, tetapi mulai bergerak ke fungsi lini seperti manager pabrik, manajer 
 pemasaran atau kepala operasional. Masyarakat biasanya memberikan sebutan 
 "wan ita karir" bagi wanita manajer tersebut. 
 Definisi dari manajer adalah orang yang mengatur proses kerja samadan 
 memanfaatkan kemampuan orang lain untuk mencapai sasaran atau tujuan 
 organisasi. Jadi manajer berwenang dan bertanggung jawab membuat rencana, 
 mengatur, memimpin, dan mengendalikan pelaksanaannya untuk mencapai 
 sasaran tersebut. Mengingat luasnya tugas dan fungsi manajemen, maka proses 
 kegiatan dibagi pula sesuai dengan tugas serta tingkat posisinya, sehingga 
 terbentuk apa yang dimaksudkan hirarki manajemen dalam suatu organisasi. 
 Tingkat manajer secara umum dibagi tiga, yang pertama adalah manajer level 
 pertama (first line or first level manager), yaitu tingkat paling rendah dalam 
 sebuah organisasi dimana orang bertanggung jawab atas pekerjaan orang lain 
 disebut manajemen level pertama. Manajer level pertama mengerahkan karyawan 
 non-manajemen, mereka tidak mengawasi manajer yang lain. Contoh dari manajer 
 level pertama adalahforeman atau supervisor produksi dalam pabrik, supervisor 
 teknik dalam depertemen penelitian, demikian juga manajer tim liga sepak bola. 
 Yang kedua adalah manajer level menengah (middle manager), istilah manajer 
 level menengah dapat mencakup lebih dari satu tingkat dalam sebuah organisasi. 
 Manajer level menengah mengarahkan kegiatan manajer dari tingkat yang lebih 
 rendah dan kadang-kadang karyawan operasional juga. Prinsip tanggung jawab 
 manajer level menengah adalah mengarahkan aktivitas yang meng- 
 implementasikan kebijakan organisasi dan menyeimbangkan permintaan dari 
 manajer dengan kapasitas karyawan. Adapun yang ketiga adalah manajer level 
 atas (top manager), yaitu terdiri dari kelompok yang relatif sedikit, manajer level 
 atas bertanggung jawab untuk manajemen keseluruhan dari sebuah organisasi. 
 Orang-orang ini disebut eksekutif, mereka menetapkan kebijakan opreasional dan 
 pedoman interaksi dengan lingkungan. Biasanya nama bagijabatan manajer level 
 atas adalah "chief executif officer", "presiden" dan "wakil presiden". 
 Telah dijelaskan diatas mengenai level-level manajer. Pada penelitian ini 
 sam pel yang diambil adalah wan ita karir yang telah berkeluarga dan berada hanya 
 pada manajer level menengah dan manajer level bawah. Tugas dan tanggung 
 jawab pada masing-masing level manajer tersebut berbeda-beda, makin tinggi 
 level manajer maka makin berat pula tugas dan tanggung jawabnya. Lebih 
 spesifIk lagi, seorang wanita karir yang telah berkeluarga yang menduduki posisi 
 manajer level menengah dan manajer level bawah akan mengalami konflik, yaitu 
 harus menjalankan peran sebagai istri dan ibu yaitu menjaga dan memelihara 
 rumah tangga dan juga harus tetap memenuhi tuntutan perusahaan yaitu 
 menjalankan tugas dan tanggung jawabnya sebagai karyawan di perusahaan, 
 disamping itu tanggapan masyarakat terhadap wanita karir pun akan menjadi 
 negatif (konsekuensi negatif) karena dianggap mementingkan karir daripada 
 keluarga. Oleh karena itu, kecemasan yang dialami oleh wanita karir tersebut akan 
 meningkat karena dihadapkan pada dua dunia yang sarna penting. Namun, apabila 
 dilihat dari segi tugas dan tanggung jawab, manajer level menengah akan lebih 
 menuntut waktu dan tenaga yang lebih berat apabila dibandingkan dengan 
 manajer level bawah. Sedangkan konflik akan lebih meningkat apabila kondisi 
 yang melatarbelakanginya menuntut perhatian yang lebih. Konflik ini akan 
 menimbulkan fenomena fear of success, tetapi apakah terdapat perberbedaan 
 antara fear of success yang dialami oleh wanita karir yang telah berkeluarga yang 
 berada pada manajer level menengah dengan fear of success yang dialami oleh 
 wan ita karir yang berada pada manajer level bawah. 
 Secara singkat kerangka pikir dapat digambarkan sebagai berikut: 
   Fear of success merupakan kecemasan pada wan ita akan konsekuensi- 
 konsekuensi negatif dari kesuksesan yang diraihnya, sehingga menimbulkan 
 kecemasan yang sedikit ban yak akan berpengaruh pada karir kerja yang telah 
 dicapai. 
   Fear of success akan meningkat derajatnya dalam situasi kompetisi terutama 
 pada level kerja yang tinggi, yaitu posisi manajer baik level menengah 
 ataupun level bawah. 
 Berdasarkan uraian di atas maka dapat diajukan hipotesis yang akan diuji 
 kebenarannya yaitu: "Terdapat perbedaan fear of success pada wan ita karir 
 berkeluarga yang berda pada manajer level menengah dan manajer level bawah 
 P.T 'X' di Bandung". 
Perkembangan dan kemajuan jaman telah membuka kesempatan tidak
hanya bagi kaum pria namunjuga bagi kaum wanita. Wanita melalui prestasinya
dapat mengambil partisipasi dalam dunia pendidikan dan kerja, namun dalam
lingkungan sosial hingga saat ini masih terdapat pandangan-pandangan serta nilai-
nilai yang berlaku mengenai peran jenis kelamin pria dan wanita. Meskipun
batasan peran tersebut semakin tidak jelas akibat derasnya arus globalisasi masa
kini, aturan-aturan dasar yang dihayati khusus turun temurun masih melekat.
Kondisi ini tergambar adanya keengganan pekerja wan ita untuk
mengaktualisasikan dirinya setinggi mungkin dalam dunia kerja.
Pada bab ini akan diuraikan teori-teori yang berkenaan dengan fear of
success pada wanita karir dan teori-teori yang berhubungan dengan kondisi sosial
masyarakat.
Peran gender dan peran jenis kelamin mempunyai arti yang berbeda. Sex
menunjukkan sistem biologis atau fisiologis individu. Terdapat dua aspek pada
biological sex yaitu genetic sex yang secara genetik akan menentukan jenis
kelamin individu dan anatomical sex yang secara fisiologis merupakan segi fisik
yang membedakan pria dan wanita. Gender merupakan suatu konsep yang
meliputi arti psiko-sosial sebagai tambahan dari aspek biologis. Gender
merupakan aspek sosial dari sex, yang disebutjuga maskulinitasdan feminitas.
Menurut Crooks (Kohlberg, 1966), gender identity (identitasjenis) adalah
sesuatu yang digunakan untuk menggambarkan diri, pandangan subyektif
mengenai diri pria atau wanita. Identitas jenis mencakup kondisi biologis dan
faktor social learning. Gender role (peran jenis) merupakan seperangkat sikap
dan tingkah laku yang sedapat mungkin normal dan dapat diterima dalam suatu
budaya spesifik bagi individu yang berjenis kelamin khusus. Tingkah laku yang
diterima oleh lingkungan sosial untuk pria itu disebut maskulin dan untuk wanita
disebut feminin. Peran jenis membentuk harapan yang sesuai dengan jenis
kelamin yang harus dipenuhi oleh individu.
Douglas C. Kimmel (1974), menyebutkan hal yang sarna, menurutnya
dalam banyak hal jenis kelamin berfungsi sebagai label yang digunakan seseorang
serta merupakan aspek diri. Secara umum dibagi dua bentuk yang dapat
menunjukkan diri individu, yaitu:
1. Jenis kelamin merupakan bagian dari koleksi "diri" yaitu sebagai laki-lakiatau
perempuan. Individu menerima dan mengalami hal ini serta berespon seperti
yang diharapkan.
2. Jenis kelamin merupakan koleksi "diri" serta dilabelkan dalam kualitas yang
bertipe maskulin atau feminin. Jenis kelamin ini sering dikenal dengan istilah
gender.
Dikatakan bahwa gender merupakan atribut sosial, oleh karena itu nilai
dan norma masyarakat yang diturunkan. dari generasi ke generasi sangat
mempengaruhi penilaian gender individu.Nilai ini diyakinioleh masyarakat tanpa
perubahan yang berarti. Lerner & Hulstch (1989), mengungkapkan norma dari
nilai pembentukkan gender tersebut sebagai stereotipe sosial. Stereotipe sosial
merupakan suatu keyakinan yang berlaku umum, seperti halnya kombinasi
kognisi dan perasaan maupun beberapa sikap yang memberikan karakteristik
suatu stimulus obyek tanpa kecuali.
Stereotip sosial mencakup pengertian sex role, sex role behavior dan sex
role stereotype. Sex role merupakan ketentuan tingkah laku individu yang
dihasilkan secara sosial oleh masyarakat dan harus dilakukan kelompok jenis
kelamin tertentu. Sex role behavior adalah pemfungsian tingkah laku sesuai
dengan ketentuan yang ada berdasarkan pembagian sex role. Sex role stereotype
adalah keyakinan yang berlaku umum menyatakan bahwa sejumlah tingkah laku
tertentu merupakan karakteristik dari tingkah laku kelompok jenis kelamin
lainnya.
Keberadaan stereotipe tersebut memberikan masukan bagi masyarakat
bahwa pria dan wanita berbeda dalam berbagai aspek, namun menurut Lerner &
Hulstch (1989), hal tersebut secara langsung berarti bahwa pria dan wanita
sungguh-sungguh menampilkan tingkah laku yang berbeda dalam dimensi jenis
kelaminnya. Menurut Allport (1964), pemaknaan stereotipe sosial berlaku kaku,
sehingga menyebabkan kondisi tersebut cenderung bertahan terhadap perubahan-
perubahan lingkungan dan senantiasa dianggap benar oleh masyarakat.
Menurut Rosenkrantz, Vogel Bee & Broverman (1968):
"The stereotype differences between men and women describe above
appear to be accepted by a large segment of our society. Thus college
students portray the ideal women as less competent than the ideal man,
and mental health professionals tend to see mature healthy women as
more submissive, less independent, etc., than either mature healthy men or
adults, sex unspecified. To the extend that this results reflect societal
standards of sex role behavior, women are clearly put in a double bind by
the fact they different standards exist for women than for adults, they risk
censure for their failure to be appropriately feminine; they are necessary
deficient with respect to the general standards for adult behavior"
(Kimmel, 1974).
"Perbedaan stereotipe antara pria dan wanita menggambarkan keadaan
yang diterima oleh masyarakat luas. Mahasiswa menggambarkan wanita
ideal sebagai individu yang kurang kompeten dibandingkan pria ideal
yang digambarkan lebih submisif, kurang bebas dan lain-lain. Kondisi
akibat dari terdapatnya standar sosial mengenai tingkah laku sex role,
wanita terperangkap dalam "double bin,!' dimana fakta mengenai standar
yang berbeda berlaku bagi wanita dibandingkan orang dewasa lainnya.
Resiko mereka gagal diterima kefeminannya".
Seorang anak akan belajar dan mengetahuijenis kelaminnya ketika berusia
18 bulan, maka tingkah laku yang ditampilkan ,akan sesuai dengan peran jenis
kelaminnya (Kohlberg, 1960). Sosial budaya mendorong proses belajar mengenal
identitasjenis kelamin tersebut.
Menurut Williams (1983), terdapat tiga konsep belajar bagi seorang anak
sehingga bertingkah laku sesuai dengan peran jenisnya, yaitu:
1. Reinforcement
Tingkah laku, termasuk tingkah laku sex type dapat dibentuk melalui
pemberian hadiah atau hukuman, misalnya seorang anak memakai baju rapi
dan bersih, sang ibu akan berkata "kamu kelihatan manis". Kalimat yang
diungkapkan ibu tersebut menjadi pendorong bagi anak untuk mengulangi
tingkah lakunya. Sebaliknya, seorang anak belajar untuk tidak mengucapkan
kata-kata tertentu karena sang ibu akan menegumya. Pujian serta teguran dari
seorang ibu mengarahkan tingkah laku anak untuk mempelajari sesuatu.
Seorang anak akan mengulangi tingkah lakunya jika sang ibu memberikan
konsekuensi positif berupa pujian atau hadiah, sementara itu seorang anak
tidak akan mengulangi tingkah lakunya jika sang ibu memberikan
konsekuensi negatif yang berupa teguran atau hukuman. Demikian pula
halnya tingkah laku yang sesuai dengan jenis kelamin. Seorang anak akan
belajar untuk bertingkah laku yang sesuai dengan jenis kelaminnya. Ketika
seorang anak bertingkah laku sesuai dengan jenis kelaminnya maka akan
mendapat hadiah sedangkan jika tidak akan mendapat hukuman. Kondisi ini
akan membuat anak mempelajari tingkah laku yang pantas maupun yang tidak
sesuai denganjenis kelaminnya.
2. Modeling
Modeling merupakan jawaban pula bagi proses belajar anak dalam
bertingkah laku. Belajar melalui model meliputi observasi dan proses kognitif,
hal tersebut terjadi tanpa ada reinforcement langsung. Menurut Mischel
(1970), individu belajar mengenai peran jenis kelaminnya melalui mata dan
telinga serta mengobservasi tingkah laku orang lain atau kejadian dan tidak
hanya langsung atas tingkah laku yang tampak.
Konsep modeling merupakan proses utama dalam mempelajari tingkah
laku yang bersifat sex type yang mengimplikasikan anak perempuan
mempelajari peran wanita melalui observasi dan pengamatan dari jenis
kelamin yang sejenis dibandingkan lain jenis, terutama jika model tersebut
bertingkah laku sesuai dengan sex type-nya. 
Melalui observasi dan
berbagai kondisi pendorong, anak akan mengetahui tingkah laku apa yang
pantas untuk ditampilkan sesuai dengan jenis kelaminnya.
. 

