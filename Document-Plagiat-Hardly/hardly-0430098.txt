Di Indonesia jumlah anak berkebutuhan khusus semakin mengalami 
peningkatan, beberapa tahun belakangan ini istilah anak berkebutuhan khusus 
semakin sering terdengar dan banyak dibahas melalui media cetak maupun 
elektronik. Saat ini jumlah anak berkebutuhan khusus yang perlu mendapat 
perhatian serius mencapai 1,2 juta orang atau dua setengah persen dari 
populasi anak-anak usia sekolah, dinyatakan oleh Dirjen Pendidikan Luar 
Sekolah dan Pemuda Departemen Pendidikan Indonesia, Fasli Jalal (Harian 
Umum Pelita,15 februari 2008). 
Dengan peningkatan jumlah anak berkebutuhan khusus tersebut 
pemerintah berupaya untuk membantu dan memenuhi kebutuhan anak 
berkebutuhan khusus, dalam hal ini pendidikan. Seperti yang tercantum dalam 
Undang Undang Dasar 1945 pasal 31 ayat 1 dan Undang-Undang Nomor 2 
tahun 1989 tentang Sistem Pendidikan Nasional bab III ayat 5 dinyatakan 
bahwa setiap warga negara mempunyai kesempatan yang sama memperoleh 
pendidikan. Hal ini menunjukkan bahwa anak berkebutuhan khusus berhak 
pula memperoleh kesempatan yang sama dengan anak normal di dalam 
pendidikan. 
Anak dengan kebutuhan khusus dapat memperoleh pendidikan yang 
layak dan salah satunya dengan bersekolah di sekolah reguler. Oleh karena itu 
pemerintah membuat kebijakan mengenai program pendidikan inklusi. Di 
Indonesia, penerapan pendidikan inklusi dijamin oleh Undang-Undang Nomor 
20 Tahun 2003 tentang Sistem Pendidikan Nasional, yang dalam 
penjelasannya menyebutkan bahwa penyelenggaraan pendidikan untuk peserta 
didik berkelainan atau memiliki kecerdasan luar biasa diselenggarakan secara 
inklusi. Pendidikan inklusi adalah kebersamaan untuk memperoleh pelayanan 
pendidikan dalam satu kelompok secara utuh bagi seluruh anak berkebutuhan 
khusus usia sekolah, mulai dari jenjang TK, SD, SLTP sampai dengan SMU. 
Sekolah inklusi juga membantu agar anak berkebutuhan khusus mampu 
bersosialisasi dan berinteraksi dengan anak sebayanya disekolah reguler 
(Dikdasmen Depdiknas, Akhir tahun 2004). 
Dalam program pendidikan inklusi, anak berkebutuhan khusus belajar 
bersama dengan anak normal lainnya dalam satu kelas yang sama di sekolah 
reguler. Menurut Direktorat Pembinaan Sekolah Luar Biasa, karakteristik anak 
untuk program pendidikan inklusi adalah anak yang sudah mampu 
mengendalikan perilakunya sehingga tampak berperilaku normal, 
berkomunikasi dan berbicara normal, serta mempunyai wawasan akademik 
yang cukup sesuai dengan anak seusianya (www.Direktorat Pembinaan 
Sekolah Luar Biasa.com. Jakarta, 13 April 2006). 
SD  X  Bandung ini merupakan salah satu sekolah yang menerapkan 
program belajar bersama antara anak berkebutuhan khusus dan anak yang 
normal, jauh sebelum pemerintah membuat kebijakan mengenai program 
pendidikan inklusi. Program inklusi ini bertujuan agar anak-anak dengan 
kebutuhan khusus dilayani untuk lebih memaksimalkan potensi dari segi 
sosial, emosional, fisik, kognitif maupun kemandiriannya dalam lingkungan 
anak-anak yang beragam.  
SD  X  Bandung memiliki visi yaitu mewujudkan sebuah lembaga 
pendidikan yang berwawasan global sehingga menghasilkan anak-anak yang 
siap menghadapi berbagai tantangan hidup di era globalisasi dan bermanfaat 
bagi masyarakat. Sedangkan misinya adalah mengembangkan semaksimal 
mungkin potensi yang ada pada setiap individu dengan segala kelebihan dan 
kekurangannya, membantu menyiapkan anak-anak dalam menghadapi era 
globalisasi dengan dasar kepribadian yang baik dan memberikan lingkungan 
yang beragam bagi anak-anak agar lebih peka terhadap lingkungannya yang 
penuh keberagaman. 
Dalam SD  X  ini anak berkebutuhan khusus terdiri dari beragam 
kasus atau gangguan antara lain, autis, Mental Retardasi (MR), gangguan 
konsentrasi dan kesulitan belajar, sehingga selain guru yang mengajar dikelas 
juga dibutuhkan guru pendamping yang khusus mendamping anak 
berkebutuhan khusus selama berada disekolah. Dalam sekolah inklusi guru 
kelas mempunyai wewenang penuh akan kelasnya serta bertanggung jawab 
atas terlaksananya peraturan yang berlaku, sedangkan guru pendamping 
adalah seorang yang dapat membantu guru kelas dalam mendampingi anak 
yang berkebutuhan khusus pada saat diperlukan, sehingga proses belajar 
mengajar dapat berjalan lancar tanpa gangguan (www.Direktorat Pembinaan 
Sekolah Luar Biasa.com). Berdasarkan hasil wawancara dengan koordinator 
guru pendamping di SD  X  Bandung guru pendamping memiliki tugas pokok 
antara lain membimbing anak berkebutuhan khusus selama proses 
pembelajaran, menjaga keselamatan anak berkebutuhan khusus selama di 
sekolah, melakukan kegiatan administrasi penilaian anak berkebutuhan 
khusus, bekerjasama dengan orangtua anak berkebutuhan khusus. 
Berdasarkan hasil observasi dan wawancara kepada 8 orang guru 
pendamping, dalam proses pembelajaran guru pendamping mengalami 
hambatan terutama ketika anak berkebutuhan khusus tidak dapat untuk 
memfokuskan perhatian terhadap pelajaran yang diajarkan oleh guru dikelas, 
pada saat itu guru pendamping perlu menenangkan anak tersebut dengan 
berusaha mengetahui apa yang menjadi masalah dan membujuk anak 
berkebutuhan khusus agar kembali memperhatikan guru di depan. Walaupun 
demikian masih ada anak yang sulit ditangani dan tiba-tiba bersikap agresif 
dengan menyerang guru pendamping, seperti mencubit, menggigit atau 
memukul. Perilaku yang tiba-tiba menjadi agresif ini biasanya menimbulkan 
kekhawatiran bagi guru pendamping terkait keselamatan anak berkebutuhan 
khusus selama berada di sekolah, karena dapat melukai diri anak 
berkebutuhan khusus sendiri. 
Guru pendamping mendampingi anak berkebutuhan khusus tidak 
hanya di dalam kelas saja, di SD  X  bagi anak berkebutuhan khusus terdapat 
jadwal dimana mereka datang ke suatu ruangan khusus yang disebut Unit 
Stimulus Anak (USA), disini guru pendamping membantu ortopedagog dalam 
memberikan stimulasi agar anak berkebutuhan khusus dapat mencapai 
kemajuan dalam perkembangannya baik secara akademis maupun 
kemandirian. Guru pendamping secara khusus mengajar dan melatih anak 
keterampilan-keterampilan khusus yang berkaitan dengan kemandirian, yang 
disesuaikan dengan kebutuhan anak. Salah satunya seperti melatih 
kemampuan menulis anak atau cara memasangkan kancing baju dengan benar. 
Setiap harinya guru pendamping di SD  X  diwajibkan membuat 
catatan mengenai kegiatan yang telah diberikan kepada anak kemudian respon 
apa saja yang ditampilkan oleh anak, namun terkadang guru pendamping 
merasa tidak ada waktu untuk dapat mencatat setiap kegiatan dan respon anak 
yang didampinginya secara detail karena harus selalu mengawasi anak yang 
didampinginya. Catatan yang dibuat ini nantinya berguna untuk proses 
penilaian anak berkebutuhan khusus, berdasarkan penilaian tersebut guru 
pendamping dapat memberikan informasi kepada orangtua mengenai cara-
cara yang perlu diterapkan orangtua untuk dapat membantu anak selama 
berada di luar sekolah. Perkembangan yang ditunjukkan anak berkebutuhan 
khusus baik secara akademis maupun kemandirian menjadi tolak ukur guru 
pendamping. Melihat uraian di atas maka dari itu guru pendamping 
membutuhkan keyakinan diri yang tinggi dalam mendampingi anak 
berkebutuhan khusus karena selama mendampingi anak berkebutuhan khusus 
guru pendamping mengalami banyak tantangan dan kesulitan.  
Keyakinan mengenai kemampuan diri dikenal dengan self efficacy. 
Self-efficacy merupakan keyakinan seseorang mengenai kemampuan dirinya 
dalam mengatur dan melaksanakan serangkaian tindakan yang dibutuhkan 
untuk memperoleh hasil yang diinginkan (Bandura, 2002). Keyakinan akan 
kemampuannya guru pendamping dalam menjalani pekerjaan mendampingi 
anak berkebutuhan khusus dapat dilihat melalui, seberapa yakin guru 
pendamping mampu membuat pilihan yang tepat dalam melakukan tugasnya 
saat mendampingi anak berkebutuhan khusus, seberapa besar keyakinan untuk 
mampu mengerahkan usaha dalam melakukan tugasnya dalam menghadapi 
anak berkebutuhan khusus, seberapa besar keyakinan guru pendamping untuk 
mampu bertahan saat dihadapkan pada kesulitan-kesulitan, serta seberapa 
yakin guru pendamping mampu mengelolah emosi yang dimiliki terhadap 
pilihan, usaha dan ketahanan yang dilakukannya. 
Berdasarkan hasil wawancara awal yang dilakukan kepada 8 guru 
pendamping, terdapat 37,5% guru pendamping yang mengatakan merasa 
dirinya yakin mampu melaksanakan setiap tugas dan kewajiban yang 
diberikan meskipun mereka dihadapkan pada kesulitan karena adanya tuntutan 
kurikulum yang disesuaikan dengan anak normal lainnya, anak berkebutuhan 
khusus yang sulit diatur, dan mengalami kesulitan bersosialisasi karena tidak 
semua teman sebayanya dapat menerima keberadaan anak berkebutuhan 
khusus. 
Agar dapat mendampingi anak berkebutuhan khusus secara optimal 
mereka yakin mampu menentukan sendiri metode untuk membantu anak 
berkebutuhan khusus, salah satu contoh dalam mengerjakan tugas bahasa 
Inggris, berusaha untuk membantu anak dengan cara memberi stimulus, 
seperti mengucapkan satu kata dalam bahasa inggris kemudian anak 
berkebutuhan khusus diminta untuk mengembangkan kata tersebut hingga 
menjadi satu rangkaian kalimat. Ketika para guru pendamping mengalami 
kesulitan, mereka tetap bertahan untuk mendampingi anak berkebutuhan 
khusus. Begitu juga dalam keadaan lelah, mereka tetap semangat dalam 
mendampingi anak berkebutuhan khusus. Menurut Bandura (2002), 
seseorang yang memiliki self-efficacy yang tinggi akan menentukan langkah 
dan cara yang tepat untuk dilakukan dalam mencapai tujuannya serta akan 
tetap bertahan dan berusaha mempertahankannya. Demikian juga mereka 
menganggap setiap hambatan dan kesulitan yang dihadapinya sebagai sesuatu 
yang dapat diselesaikan. 
Terdapat 62,5% guru pendamping lainnya yang merasa kurang yakin 
akan kemampuannya, terkadang mereka kurang yakin apakah mampu 
menghadapi kesulitan ketika membantu anak berkebutuhan khusus dan 
mampu bertahan mendampingi anak berkebutuhan khusus tersebut. 
Berdasarkan hasil wawancara kepada guru pendamping dalam mendampingi 
anak berkebutuhan khusus mereka memilih menggunakan metode 
pendampingan seadanya sesuai dengan apa yang telah diberikan oleh 
ortopedagog. Guru pendamping tidak berusaha untuk mencari alternatif 
metode pengajaran yang lain ketika menghadapi hambatan. Melalui situasi 
tersebut guru pendamping anak berkebutuhan khusus akan mudah menyerah 
untuk mendampingi anak berkebutuhan khusus. Tidak jarang pula saat guru 
pendamping merasa lelah dan bosan, guru pendamping merasa tidak yakin 
mampu untuk mendampingi anak berkebutuhan khusus sehingga pada 
akhirnya perkembangan anak menjadi kurang optimal. 
Menurut Bandura (2002), seseorang yang memiliki self-efficacy 
rendah akan merasa kurang yakin dalam menentukan pilihan langkah atau cara 
yang tepat untuk dilakukan dalam mencapai tujuan dan kurang dapat bertahan 
lama dalam melakukan usaha dan akan lebih mudah untuk menyerah serta 
cenderung mempunyai penghayatan negatif terhadap setiap hambatan dan 
tuntutan yang dihadapinya. Dalam hal ini, guru pendamping menganggap 
kesulitan yang dihadapinya sebagai hambatan untuk mencapai tujuannya. 
Berdasarkan hasil survei awal yang dilakukan peneliti mengenai 
keyakinan akan kemampuan pada guru pendamping anak berkebutuhan 
khusus di SD  X  Bandung, menggambarkan bahwa guru pendamping lebih 
memiliki tugas yang khas serta mengalami kesulitan dalam mendampingi anak 
berkebutuhan khusus sehingga dibutuhkan keyakinan diri. Oleh karena itu, 
peneliti tertarik untuk melakukan penelitian mengenai self-efficacy pada guru 
pendamping di sekolah inklusi SD  X  Bandung. 
1. 2 Identifikasi Masalah 
Bagaimana derajat self-efficacy pada guru pendamping dalam 
menjalankan tugas-tugasnya mendampingi anak berkebutuhan khusus di SD 
 X  Bandung. 
1. 3 Maksud dan Tujuan Penelitian 
1. 3. 1  Maksud Penelitian 
  Untuk memperoleh gambaran umum mengenai derajat self-efficacy 
pada guru pendamping dalam menjalankan tugas-tugasnya selama 
mendampingi di SD  X  Bandung. 
1. 3. 2 Tujuan penelitian 
  Tujuan penelitian ini adalah untuk mengetahui derajat self-efficacy 
berdasarkan keyakinan akan kemampuan menentukan pilihan, kemampuan 
mengerahkan usaha dalam mencapai tujuan, kemampuan bertahan, dan 
keyakinan akan kemampuan dalam mengolah emosi yang dimiliki pada guru 
pendamping di SD  X  Bandung. 
1. 4  Kegunaan Penelitian 
1. 4. 1 Kegunaan Teoritis 
1. Memberi masukan bagi bidang ilmu psikologi pendidikan mengenai 
self-efficacy pada guru pendamping di SD  X  Bandung.  
2. Memberikan tambahan informasi bagi peneliti lain yang ingin 
melakukan penelitian lanjutan mengenai derajat self-efficacy. 
1. 4. 2 Kegunaan Praktis 
1. Memberikan informasi kepada guru pendamping di SD  X  Bandung 
mengenai self-efficacy mereka dalam menghadapi pekerjaan, agar 
lebih memahami dirinya untuk pengembangan diri lebih lanjut. 
2. Memberi informasi kepada pihak sekolah mengenai self-efficacy guru 
pendamping dalam menghadapi pekerjaan sehingga dapat menjadi 
masukan dalam proses pemberian pengarahan kepada guru 
pendamping agar dapat berhasil saat mendampingi anak berkebutuhan. 
1. 5 Kerangka Penelitian 
SD  X  merupakan salah satu sekolah yang menerapkan program 
inklusi, dimana dalam sekolah tersebut anak berkebutuhan khusus belajar 
bersama-sama dalam satu kelas dengan anak normal lainnya dalam kelas 
reguler. 

