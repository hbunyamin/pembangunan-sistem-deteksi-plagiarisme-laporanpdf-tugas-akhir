Pada masa sekarang ini dunia dihadapkan pada kemajuan ilmu pengetahuan dan 
teknologi untuk membantu manusia memasuki peradaban dunia baru yang dikenal 
dengan istilah globalisasi.  Era globalisasi adalah era persaingan mutu atau kualitas 
yang dalam dunia pendidikan harus berbasis pada mutu dengan meningkatkan 
kualitas sumber daya manusia sebagai persiapan untuk memasuki dunia pendidikan 
pada jenjang berikutnya ataupun persiapan memasuki dunia kerja.  Upaya 
peningkatan sumber daya manusia di Indonesia mendapat perhatian dari pemerintah 
seperti terlihat pada GBHN 1999/2004 yang menyebutkan bahwa negara berusaha 
mengembangkan kualitas sumber daya manusia dengan meningkatkan taraf hidup, 
kecerdasan dan kesejahteraan yang semakin adil dan merata.  Oleh karena itu tidak 
dapat dipungkiri bahwa era globalisasi menuntut adanya sumber daya manusia yang 
berkualitas serta mampu bersaing. 
Salah satu cara untuk memperoleh sumber daya manusia yang berkualitas serta 
mampu bersaing adalah melalui dunia pendidikan.  Dunia pendidikan adalah sarana 
bagi generasi penerus bangsa untuk dapat mengembangkan potensi yang dimilikinya.  
Melalui dunia pendidikan diharapkan dapat mencetak generasi-generasi yang 
berkualitas dan siap bersaing di era globalisasi.  Begitu pula halnya dalam dunia  
kedokteran.  Kedokteran adalah salah satu fakultas yang paling banyak diminati oleh 
para calon mahasiswa, terlihat dari banyaknya peminat yang memilih fakultas 
kedokteran bila dibandingkan fakultas lainnya (Buku Panduan Sistem Penerimaan 
Mahasiswa Baru, 2005).  Belajar di dunia kedokteran tidaklah mudah, hal ini terlihat 
dari banyaknya mata kuliah yang menuntut konsentrasi yang tinggi dan menuntut 
kemampuan untuk mengingat dengan tepat semua materi perkuliahan.  Selain itu, di 
fakultas kedokteran juga banyak terdapat mata kuliah praktikum yang harus 
dimengerti dengan teliti, sebab hal tersebut menyangkut keselamatan jiwa manusia 
(Pedoman Penyelenggaraan Pendidikan Universitas Padjajaran, 2001). 
Setelah lulus dari fakultas kedokteran dan menjadi sarjana, mahasiswa fakultas 
kedokteran tersebut harus mengikuti praktek di rumah sakit dan disebut sebagai 
dokter muda (co-ass).  Di rumah sakit para dokter muda mulai belajar untuk 
menjalani profesi sebagai dokter yang sesungguhnya yaitu berhadapan langsung 
dengan para pasien, mulai dari mendiagnosa penyakit, belajar untuk menentukan 
tindakan preventif serta menentukan jenis obat yang dibutuhkan oleh pasien. Oleh 
karena itu dunia kedokteran menuntut orang-orang yang cerdas serta pintar secara 
akademis atau lebih dikenal dengan orang-orang yang memiliki kecerdasan 
intelektual (IQ) tinggi. 
Namun ternyata dewasa ini, IQ yang tinggi bukanlah jaminan kesuksesan 
seseorang, begitu pula halnya bagi seorang dokter muda yang sedang menjalankan 
praktek di rumah sakit.  Berdasarkan wawancara dengan salah seorang dokter didapat  
bahwa  ada keluhan atas pelayanan para dokter muda selama menjalankan 
praktek,misalnya para dokter muda yang kurang mampu berkomunikasi dengan para 
pasien dan keluarga pasien, memunculkan kesan kurang peduli sehingga membuat 
para pasien merasa kurang nyaman.  Seorang dokter muda tidak hanya diharapkan 
pintar serta mampu mendiagnosa penyakit dengan tepat, tetapi juga diharapkan untuk 
bersikap lebih bersahabat pada orang lain, khususnya pada para pasien.  Seorang 
dokter muda hendaknya dapat berinteraksi dengan baik dan dapat menunjukkan 
ekspresi wajah yang bersahabat.  Hal ini perlu agar orang lain khususnya para pasien 
dapat merasa nyaman saat sedang ditangani oleh mereka. 
Selain masalah pelayanan, masalah lain yang sering dihadapi oleh para dokter 
muda adalah adanya suatu masa di mana mereka mengalami rasa malas dan enggan 
untuk belajar serta praktek di rumah sakit.  Ada juga beberapa dokter muda yang 
merasa jenuh dan lelah karena harus selalu siap sedia dalam dua puluh empat jam 
untuk praktek di rumah sakit apabila dibutuhkan.  Misalnya, jadwal mereka untuk 
satu hari telah selesai, namun di saat mereka pulang ke rumah, mereka kembali 
dipanggil untuk membantu dokter yang bertugas karena ada pasien yang memang 
membutuhkan pertolongan.  Sementara jumlah dokter muda ataupun dokter dan 
suster yang bertugas tidak mencukupi.  Masalah lain adalah di saat mereka mendapat 
giliran jaga malam dan tidak memungkinkan mereka untuk tidur dengan cukup, 
keesokan paginya harus sudah berada di rumah sakit kembali di pagi hari.  Masalah-
masalah ini tentunya akan berpengaruh terhadap kualitas pelayanan dan keterampilan 
mereka saat praktek di rumah sakit.  Oleh karena itu, untuk dapat memberikan 
pelayanan secara optimal dibutuhkan kemampuan untuk dapat memotivasi diri, 
mengontrol emosi dan mengatur suasana hati agar dapat bekerja secara maksimal 
serta kemampuan untuk berempati.  Kemampuan ini dapat diperoleh jika para dokter 
muda memiliki kecerdasan emosional yang cukup tinggi. 
Kecerdasan emosional adalah kemampuan individu untuk memotivasi diri 
sendiri, bertahan terhadap frustrasi, mengendalikan dorongan hati, menjaga agar 
beban stres tidak melumpuhkan kemampuan berpikir, empati dan berdoa dan 
menggunakan emosi secara efisien untuk mencapai tujuan, membangun hubungan 
yang produktif dan mencapai keberhasilan (Goleman, 1997).  Adapun lima aspek dari 
kecerdasan emosional tersebut adalah knowing one s emotions (mengenali emosi-
emosi diri) yaitu dengan mengenali emosi pada saat emosi tersebut muncul, 
managing emotions (mengelola emosi-emosi diri) yaitu dengan mengelola emosi agar 
dapat terungkap dengan tepat, motivating one self (memotivasi diri sendiri) yaitu 
dengan cara memimpin emosi yang muncul agar tujuan dapat tercapai, recognizing 
emotions in others (mengenali emosi orang lain) yaitu merupakan kemampuan 
menempatkan diri dan merasakan perasaan orang lain atau empati serta handling 
relationship (membina hubungan dengan orang lain) yaitu kemampuan membina 
hubungan dengan orang lain. 
Konsep kecerdasan emosional dalam dunia pendidikan berhubungan dengan 
faktor-faktor non inteligensi seperti sikap, motivasi, minat dan faktor perasaan dalam 
menghadapi tugas belajar.  Para mahasiswa diharapkan memiliki disiplin dalam 
belajar, yang menuntut kemampuan memotivasi dan mengelola emosi agar dapat 
mencapai hasil studi yang memuaskan.  Untuk mengelola emosi dibutuhkan 
kemampuan dalam mengendalikan emosi tersebut, terutama saat mengalami suasana 
hati yang buruk (bad mood).  Sedangkan untuk menumbuhkan hubungan yang baik 
dengan orang lain dibutuhkan kemampuan untuk membina hubungan interpersonal 
dan pengenalan emosi orang/empati (dalam Goleman, 1997: 57).  Oleh karena itu 
tidaklah heran jika saat ini masalah kecerdasan emosional sangat diperhitungkan 
khususnya dalam dunia pendidikan kedokteran. 
Kecerdasan emosional dalam dunia kedokteran memiliki pengaruh bagi 
keberhasilan para mahasiswa juga para dokter muda.  Dengan kecerdasan emosional 
yang tinggi para dokter muda diharapkan dapat memotivasi diri sendiri untuk terus 
belajar sehingga mencapai nilai akademik yang memuaskan serta dapat selalu siap 
sedia untuk melakukan praktek di rumah sakit kapanpun mereka dibutuhkan.  Para 
dokter muda juga diharapkan untuk dapat bertahan terhadap frustrasi dan mampu 
mengatasi berbagai masalah tanpa harus mengorbankan tugas-tugas mereka di rumah 
sakit.  Selain itu para dokter muda juga diharapkan dapat berempati serta menjalin 
hubungan interpersonal, mampu menjaga beban stres agar tidak menggangu 
konsentrasi dalam belajar serta pada saat praktek di rumah sakit (Mu tadin dalam 
www.e-psikologi.com). 
Berdasarkan wawancara awal dengan 20 orang dokter muda, 25% dari mereka 
mengatakan bahwa mereka terkadang tidak sadar saat suatu perasaan sedang melanda 
mereka.  Saat mereka merasakan sesuatu hal yang  tidak sesuai dengan apa yang 
mereka harapkan, mereka cenderung tidak tahu bahwa mereka sedang marah.  Di saat 
mereka sedang marah atau murung, mereka tidak dapat cepat pulih dari perasaannya 
tersebut.  Hal ini menyebabkan mereka tidak dapat mengambil keputusan dengan 
tepat dalam setiap permasalahan yang mereka hadapi.  Mereka juga cenderung untuk 
menjadi cepat tersinggung dan sulit untuk menerima perbedaan pendapat.  Hanya ada 
10% dokter muda yang dapat menggunakan emosinya dengan baik sehingga 
mengetahui dengan tepat kapan ia harus menggunakan emosinya.  Mereka dapat 
mengetahui dengan pasti kapan saatnya harus marah sehingga hal tersebut tidak 
mengganggu aktivitasnya. 
Sebanyak 35% para dokter muda mengatakan bahwa mereka tidak dapat 
memotivasi diri mereka saat sedang mengalami masalah dalam materi perkuliahan.    
Mereka juga cenderung menjadi kurang produktif dalam studi dan praktek mereka di 
rumah sakit sehingga menimbulkan kesalahan dalam penanganan pasien dan menjadi 
kurang efektif dalam mengatasi permasalahan sehari-hari.  Apabila ditegur oleh 
dokter pembimbing mereka akan merasa malu dan sedih serta susah untuk 
membangkitkan kembali rasa percaya diri mereka.  Sementara itu terdapat 10% 
dokter muda yang memiliki motivasi diri yang tinggi.  Jika sudah mulai malas untuk 
belajar dan praktek, mereka tidak mau berdiam diri saja tetapi berusaha untuk terus 
bangkit dan melawan perasaan malas tersebut.  Mereka juga menganggap bahwa 
teguran dari para dokter pembimbing adalah untuk meningkatkan kinerja mereka di 
rumah sakit.  Oleh karena itu mereka tidak akan merasa malu atau sedih tetapi akan 
terus berusaha untuk bangkit dan maju dalam pelajaran dan dalam praktek di rumah 
sakit. 
Sebanyak 15% dokter muda mengatakan bahwa mereka kurang peduli terhadap 
apa yang dirasakan orang lain dan kurang dapat menempatkan diri dalam bergaul. 
Mereka juga kurang dapat membina hubungan yang hangat dan akrab dengan orang 
lain.  Mereka memiliki prioritas untuk menomorsatukan pelajaran dan nilai akademis 
saja, tanpa perlu menemukan cara bagaimana membuat para pasien merasa nyaman 
dan senang untuk berobat pada mereka nantinya.  Sementara itu terdapat 5% para 
dokter muda yang mengatakan bahwa selain pintar mereka juga harus dapat bersikap 
ramah dan dekat terhadap para pasien.  Hal ini bertujuan selain dapat membangkitkan 
rasa percaya diri para pasien untuk segera sembuh dari penyakitnya, juga dapat 
membuat pasen merasa nyaman. Apabila kedua hal tersebut telah terpenuhi, 
kemungkinan besar pasien tersebut akan kembali pada mereka apabila terserang 
penyakit di masa yang akan datang. 
Berdasarkan fenomena yang terjadi diatas, diketahui bahwa gambaran 
kecerdasan emosional para dokter muda berbeda-beda.  Oleh karena itu peneliti 
merasa tertarik untuk mengetahui gambaran mengenai kecerdasan emosional secara 
lebih mendalam pada dokter muda di Universitas  X  Bandung. 
1.2.Identifikasi Penelitian 
Bagaimanakah gambaran mengenai tingkat kecerdasan emosional pada para 
dokter muda di Universitas  X  Bandung? 
Untuk memperoleh gambaran mengenai tingkat kecerdasan emosional pada 
para dokter muda di Universitas  X  Bandung. 
Untuk mengetahui dan menggambarkan secara komprehensif mengenai 
tingkat kecerdasan emosional dan aspek-aspeknya serta faktor-faktor yang 
mempengaruhi kecerdasan emosional pada dokter muda di Universitas  X  Bandung. 
1. Penelitian ini diharapkan dapat menambah informasi dan wawasan di 
bidang Psikologi Pendidikan mengenai kecerdasan emosional dalam 
dunia pendidikan khususnya dunia kedokteran. 
2. Memberi pengetahuan dan gambaran bagi peneliti lain yang berminat 
untuk meneliti mengenai kecerdasan emosional dalam dunia 
pendidikan. 
1. Sebagai masukan bagi pihak pengajar/dosen mengenai kecerdasan 
emosional dalam pengembangan sumber daya manusia. 
2. Sebagai masukan bagi para dokter muda mengenai gambaran 
kecerdasan emosional dalam pengembangan diri demi tercapainya 
pelayanan secara optimal bagi masyarakat. 
3. Sebagai masukan bagi Fakultas Kedokteran Universitas  X  Bandung 
mengenai gambaran kecerdasan emosional para dokter muda dalam 
rangka meningkatkan mutu dan pelayanan di rumah sakit. 
Sebelum seorang mahasiswa siap menjadi seorang dokter dan mengabdi pada 
masyarakat, mereka terlebih dahulu harus melewati masa praktek di rumah sakit 
tertentu selama kurang lebih dua tahun.  

