Zaman era globalisasi seperti saat ini, pendidikan menjadi sangatlah 
penting, baik untuk mengembangkan potensi dalam diri maupun untuk mencapai 
impian masa depan. Biasanya masyarakat di Indonesia mengikuti pendidikan 
dasar yang berupa Taman Kanak-Kanak hingga Sekolah Menengah Atas 
dilaksanakan di lembaga atau sekolah yang lokasinya dekat dengan rumah atau 
dikota masing-masing. Masyarakat Indonesia masih menganggap bahwa anak 
umur sekolah belum bisa mandiri dan mengatur kehidupan mereka sendiri. 
Berbeda  dengan anak yang menempuh pendidikan di Perguruan Tinggi, orang tua 
cenderung membolehkan dan mendukung anak mereka untuk menjalani 
pendidikan, baik di daerah mereka masing-masing maupun diluar daerah, karena 
sebagai orang tua menginginkan anak-anak mereka mendapatkan kualitas 
pendidikan terbaik, yang mungkin tidak selalu mereka dapatkan di daerah mereka 
sendiri walaupun dengan biaya yang tidak murah. 
Perkembangan dunia pendidikan di Pulau Jawa lebih pesat, ditandai 
dengan tersedianya lembaga pendidikan yang lebih maju dan berkualitas, seperti 
adanya universitas-universitas yang terkemuka, baik negeri maupun swasta, yang 
dinilai berkualitas baik dan memiliki banyak pilihan fakultas. Bandung 
merupakan salah satu sasaran yang dituju dan Bandung juga dijuluki sebagai kota 
pelajar.  
Memasuki dunia universitas membuat mahasiswa lebih tumbuh 
berkembang dan lebih mandiri. Mahasiswa memiliki kesempatan untuk membina 
hubungan sosial yang baik dengan teman maupun dosen, mempunyai waktu yang 
lebih banyak dengan teman sebaya, memiliki kesempatan untuk menggali gaya 
hidup dan nilai-nilai yang berbeda, dan menikmati kebebasan dari orang tua. 
Dalam menghadapi perubahan-perubahan kondisi diatas dibutuhkan penyesuaian 
diri dari mahasiswa yang bersangkutan dengan lingkungan belajar yang baru, 
tempat tinggal yang baru, pergaulan yang berbeda dimana terdapat perbedaan 
kebiasaan dari budaya baru dengan budaya dari tempat asalnya. 
Dari sekian banyak universitas swasta dikota Bandung, Universitas  X  
merupakan salah satu universitas yang banyak dipilih untuk menimba ilmu. Sejak 
berdirinya pada tahun 1965 Universitas "X" telah menghasilkan banyak sarjana 
yang berkualitas, yang telah mengabdikan ilmunya bagi bangsa dan negara. 
Universitas "X" berkembang seiring dengan berjalannya waktu. Di Universitas 
 X  terdapat tujuh fakultas dengan dua puluh dua program studi, serta program 
pascasarjana dengan tiga program studi. Banyaknya pilihan program studi pada 
Universitas  X  menjadikan Universitas  X  sebagai pilihan yang tepat bagi 
pelajar untuk belajar (www. X .edu) dan dengan didukungnya berbagai fasilitas 
terbaik yang diberikan oleh Universitas  X .  
Berdasarkan data yang diperoleh dari Badan Administrasi dan Akademis 
Universitas "X", mahasiswa yang menuntut ilmu di Universitas  X  bertambah 
dari waktu ke waktu. Universitas  X  Bandung terdiri dari mahasiswa-mahasiswa 
yang berasal dari berbagai kota, suku dan dengan latar belakang budaya yang 
beragam. Badan Administrasi Akademis Universitas  X  Bandung mencatat pada 
tahun 2012 mahasiswa yang berasal dari kota Medan merupakan jumlah 
terbanyak dari tahun-tahun sebelumnya. 
Mahasiswa yang berasal dari Medan mempunyai kesempatan untuk 
mempelajari dan menyesuaikan diri dengan budaya yang baru di kota Bandung. 
Saat seseorang harus meninggalkan lingkungan yang familiar dan kemudian 
tinggal di lingkungan yang baru tidak menutup kemungkinan untuk mengalami 
tekanan-tekanan tertentu yang dapat menimbulkan suatu perasaan tidak nyaman 
ketika mempelajari budaya yang baru, begitu juga yang dialami oleh mahasiswa 
yang berasal dari Medan yang berpindah dari kota Medan ke kota Bandung. 
Keadaan tersebut dikenal dengan culture shock, istilah culture shock pertama kali 
diperkenalkan oleh Kalervo Oberg pada tahun 1958 mengatakan bahwa setiap 
manusia yang bepergian dan hidup disuatu negara atau daerah dengan kebiasaan 
masyarakat yang berbeda dengan kebiasaan masyarakat ditempat tinggal asalnya, 
akan mengalami culture shock.  
Menurut Kalervo Oberg (dalam Ward, Bochner, & Furnham; 2001) 
culture shock menggambarkan suatu keadaan negatif yang berhubungan dengan 
aksi yang diderita oleh individu yang secara tiba-tiba harus berpindah ke suatu 
lingkungan baru yang berbeda dengan lingkungan asalnya. Culture shock adalah 
suatu keadaan yang diderita karena secara tiba-tiba berpindah atau dipindahkan ke 
lingkungan yang baru. Culture shock terjadi karena perasaan cemas yang 
ditimbulkan oleh adanya kehilangan tanda-tanda dan lambang-lambang dalam 
pergaulan sosial, culture shock termasuk juga gaya hidup, cara berpakaian, tempat 
tinggal, makanan termasuk cara memasak, menyajikannya hingga menikmati 
hidangan, kendala komunikasi (bahasa) karena akan sulit untuk memulai 
berinteraksi di lingkungannya. Individu akan kehilangan pegangan lalu 
mengalami frustrasi dan kecemasan. Sebelum individu yang pindah ke lingkungan 
baru dapat menyesuaikan diri ada empat tahapan culture shock. Tahapan-tahapan 
culture shock adalah tahapan honeymoon, crisis, recovery dan adjustment. Culture 
shock berada pada tahapan crisis dimana perbedaan mulai memunculkan 
kebingungan pada individu dan membuat individu merasa terisolasi dan tidak 
nyaman. Culture shock mengacu pada saat individu menetap kurang lebih 18 
bulan kedatangannya. Mahasiswa semester dua masuk dalam tahapan crisis 
(culture shock) dimana mahasiswa Medan menetap di kota Bandung pada 
semester dua yaitu pada 12 bulan dari kedatangannya. 
Berdasarkan hasil wawancara yang dilakukan oleh peneliti pada 10 orang 
mahasiswa semester dua yang berasal dari kota Medan di Universitas  X . Dari 
hasil wawancara diperoleh bahwa sebanyak 100% mengatakan mereka memilih 
kuliah di Bandung karena terdapat universitas-universitas unggulan dan memiliki 
banyak pilihan fakultas yang disukai. Sebanyak 60% memilih kuliah di Bandung 
karena ingin mandiri dan sebanyak 40% karena mengikuti teman yang juga 
melanjutkan kuliah di Bandung.  
Sebanyak 100% mengatakan ketika pindah kuliah ke Bandung mereka 
merasa makanan di Bandung manis-manis dan sangat berbeda dengan makanan di 
tempat asalnya yang makanannya lebih pedas dan juga makanan disini ada yang 
setengah matang dan ada lalapan. Mereka merasa malas makan namun mereka 
harus makan karena apabila tidak makan mereka takut sakit. Mereka sering 
memilih-milih makanan karena ada makanan yang tidak cocok yang dapat 
menyebabkan sakit perut. 90% merasa rindu masakan rumah dan apabila makan 
bersama dengan teman yang berasal dari Bandung mereka lebih memilih tempat 
makan yang menjual makanan yang halal, karena kebanyakan mahasiswa yang 
berasal dari Medan adalah agama Kristen.  
Sebanyak 90% mengalami masalah dengan perbedaan cara bicara 
masyarakat Sunda yang berbeda dengan budaya mereka dan 10% tidak mngalami 
masalah dengan perbedaan cara bicara masyarakat Sunda. Menurut mereka cara 
berbicara masyarakat Sunda lembut dan volume suaranya pelan berbeda dengan 
budaya mereka yang tegas dan keras sehingga mereka merasa sedikit kesulitan 
karena harus mengatur cara berbicara ketika berbicara dengan masyarakat ataupun 
teman-teman yang berasal dari Sunda.  
Sebanyak 90% mengalami kesulitan dengan bahasa, mahasiswa Medan 
tidak mengerti bahasa Sunda sehingga kesulitan untuk mengerti dan juga sering 
menjadi masalah komunikasi dan 10% tidak mengalami kesulitan dengan bahasa 
Sunda. Beberapa kali Mahasiswa yang mengalami kesulitan dengan bahasa Sunda 
mengalami salah pengertian, misalnya merasa tersindir, merasa teman-temannya 
yang berasal dari Bandung membicarakan atau menjelek-jelekan mereka. 
Mahasiswa Medan ketika berbicara suaranya keras dan lantang. Ketika teman-
temannya yang berasal dari Bandung berbicara dan juga beberapa kali 
mengatakan kata  anjing  mereka merasa tersinggung. Mahasiswa asal Medan 
lebih cepat tersinggung dan mudah marah karena perbedaan karakteristik dan 
kebiasaan berbicara misalnya juga seperti intonasi bicara masyarakat Bandung. 
Mereka juga ada yang merasa hilang percaya diri, mereka merasa seperti orang 
asing dan tidak memahami bahasa ditempat yang baru (bahasa Sunda) sehingga 
mereka merasa kurang percaya diri untuk bergaul dengan lingkungan sekitarnya 
yang baru. Di daerah tempat asal di Medan menyebutkan  motor  dengan 
 kereta  sedangkan di Bandung dan ditempat lain menyebutkan kereta adalah 
kereta api. Begitu juga ketika di Bandung mahasiswa asal Medan merasa aneh 
dengan kata  punten  dan dibalas dengan kata mangga  , di Bandung  mangga  
diartikan dengan silahkan sedangkan untuk didaerah lain mangga artinya buah 
mangga. 
Sebanyak 60% mengatakan mereka ingin berinteraksi dengan masyarakat 
dilingkungan sekitar dan 40% mengatakan tidak ingin berinteraksi dengan 
masyarakat sekitar. Mereka ingin mencari teman-teman baru di Bandung dan 
ingin berinteraksi dengan orang lain dilingkungan sekitar mereka.  Sebanyak 70% 
mengatakan budaya Sunda itu ramah, dan juga mempunyai kebiasaan yang baik 
misalnya mengatakan  punten  setiap kali lewat. Sebanyak 90% mengatakan 
ingin mempelajari Bahasa Sunda dan mulai belajar dan meminta teman-temannya 
mengajarkan mereka bahasa Sunda dan juga mereka mulai untuk menyesuaikan 
dengan makanan dan budaya disini dan 10% mengatakan belajar bahasa Sunda 
perlahan-lahan. Namun semua tetap bangga dengan budaya asalnya dan masih 
mengikuti perkumpulan gabungan mahasiswa Sumatera Utara. 
Sebanyak 50% mengatakan merasa tidak bersemangat melakukan aktifitas, 
karena mereka belum mempunyai banyak teman sehingga mereka juga belum 
mempunyai banyak kegiatan bersama teman-temannya sedangkan 50% semangat 
melakukan aktifitas. Mereka sering merasa kesepian dan lebih sering merindukan 
teman-teman dari daerah tempat asalnya sehingga mereka merasa ingin pulang 
ketempat asalnya. 100% mengatakan transportasinya tertib akan tetapi lalu lintas 
di Bandung macet berbeda dengan tempat asalnya. Ada juga yang mengatakan 
pergaulan disini individual berbeda dengan orang tempat asal yang berkelompok 
dan setia kawan. 
Sebanyak 90% tidak mengalami sulit tidur dan 10% mengatakan pada 
malam hari mereka sulit tidur dan kadang-kadang menjelang pagi hari barulah 
bisa tidur sehingga sering terlambat bangun pagi padahal ada kuliah pagi dan 
merasa badan malas untuk pergi ke kampus. Mahasiswa dari Medan merasa sulit 
tidur pada malam hari karena pada siang hari mereka tidur, mereka sebenarnya 
ingin melakukan kegiatan namun karena belum memiliki teman yang cocok 
karena cara bergaul yang berbeda sehingga mereka menghabiskan waktu di 
tempat kos dan tidur. 
Pada awalnya mahasiswa yang berasal dari kota Medan mengalami 
hambatan karena perbedaan budaya tempat asal yaitu kota Medan dan budaya 
ditempat yang baru yang adalah budaya Sunda. Perbedaan diataranya dalam hal 
makanan, bahasa, kondisi lalu lintas, dan lain sebagainya. Perbedaan karaktersitik 
dan adat istiadat budaya yang menjadikan peneliti tertarik untuk meneliti culture 
shock pada mahasiswa semester dua yang berasal dari kota Medan di Universitas 
 X  Bandung. Berdasarkan fenomena di atas peneliti ingin melihat derajat culture 
shock pada mahasiswa yang berasal dari kota Medan.  

