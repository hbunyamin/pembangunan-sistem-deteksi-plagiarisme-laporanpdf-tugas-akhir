Manusia adalah makhluk ciptaan Tuhan yang tidak mungkin dapat hidup 
sendiri. Di sepanjang rentang kehidupan, setiap manusia membutuhkan manusia 
lainnya untuk mempertahankan kelangsungan hidup. Oleh sebab itu manusia 
dikatakan sebagai makhluk sosial. Kebutuhan dalam diri seseorang untuk 
bergantung pada orang lain disekitarnya telah muncul semenjak manusia berada 
pada fase bayi. Saat masih bayi, seorang anak bergantung pada orang lain yang 
dirasakan dapat memberikan rasa aman dan nyaman baginya. Di usia yang sangat 
dini, kenyamanan yang dirasakan oleh seorang bayi berasal dari figur orangtua 
ataupun figur pengasuh. Umumnya, figur pengasuh signifikan lebih besar adalah 
Ibu, namun tidak menutup kemungkinan ada figur lain selain ibu. Ikatan antara 
bayi dengan figur tersebut ini adalah keterikatan yang bersifat primer. 
Keterikatan ini menjadi fasilitator aktivitas psikologis yang baru dimulai, 
yaitu seperti kemampuan bicara, mengatur indera-indera, tindakan fisik, berfikir 
dengan simbol, meniru dan belajar dari orang lain (Santrock, 2007).  
Responsivitas figur pengasuh yang signifikan dalam memenuhi kebutuhan bayi 
tersebut menanamkan pemahaman pada bayi bahwa figur tersebut adalah figur 
yang dapat dipercaya untuk memperhatikan kebutuhannya dan dapat diandalkan 
bantuannya. Hal tersebut juga membuat bayi merasakan bahwa betapa aman 
dirinya dan betapa orang-orang disekitarnya dapat diandalkan ketika ia 
menghadapi situasi yang menekan.  Berbagai pengalaman mengenai keterikatan 
seseorang pada masa bayi dengan figur signifikan akan terus berlanjut sampai 
masa remaja, dewasa, dan bahkan sampai tua (Hazan & Shaver, 1987), serta 
menjadi pola yang dimiliki individu ketika menjalin ikatan attachment dengan 
orang lain selain figur attachmentnya. 
Semakin bertambahnya usia seseorang, maka ia akan memasuki 
lingkungan sosial yang lebih luas dari pada lingkungan keluarga. Untuk dapat 
bertahan berada dalam suatu lingkungan sosial, seseorang harus mampu menjalin 
relasi dengan orang-orang yang berada dalam lingkungan tersebut. Salah satu 
lingkungan yang menuntut adanya relasi sosial adalah situasi pendidikan. Setiap 
jenjang pendidikan memiliki tuntutan yang berbeda-beda dan tingkat kesulitannya 
akan terus meningkat sesuai dengan tujuan yang ingin dicapai dari jenjang 
tersebut. Tuntutan yang sangat berbeda terasa pada peralihan antara sekolah 
menengah atas dan perguruan tinggi. Tuntutan yang lebih tinggi dan kesulitan 
yang dialami berada pada relasi yang harus terbangun dengan teman, dosen, dan 
warga kampus lainnya.  
Dalam situasi perkuliahan mahasiswa dituntut untuk menambah luas 
relasi yang dimiliki dan menambah jumlah teman. Relasi sosial yang dimiliki 
secara langsung  akan membawa banyak manfaat dalam menjalani proses belajar 
sebagai mahasiswa. Dalam mengerjakan tugas yang bersifat kelompok, maka 
akan lebih mudah diselesaikan apabila mahasiswa memiliki kemauan untuk 
menjalin relasi yang saling percaya dengan orang lain terutama dengan teman 
dalam kelompok belajarnya. Sebaliknya, jika mahasiswa kurang mampu menjalin 
relasi sosial maka akan menghambat proses penyelesaian tugas. Disamping harus 
memenuhi tugas perkembangan dalam aspek sosial, mahasiswa baru juga harus 
beradaptasi dengan situasi belajar yang berbeda.  Situasi perkuliahan menuntut 
mahasiswa untuk lebih aktif dan dosen hanya berperan sebagai fasilitator dalam 
proses belajar mengajar.  
Tuntutan yang semakin kompleks untuk diselesaikan itu menjadi 
permasalahan tersendiri bagi mahasiswa. Masing-masing mahasiswa memiliki 
penghayatan berbeda mengenai kesulitan-kesulitan tersebut dan memunculkan 
respon yang berbeda-beda pula. Namun, apapun kondisi yang dialaminya dalam 
perkuliahan, mahasiswa dituntut untuk mampu mengatur emosinya agar tetap 
dapat menjalani proses belajar di perkuliahan dengan baik. Oleh karena itu, untuk 
dapat menjalaninya setiap mahasiswa harus memiliki kecerdasan emosional yang 
tinggi. 
 Menurut Goleman (2007) kecerdasan emosional adalah kemampuan untuk 
mengatur kehidupan emosinya dengan intelegensia; menjaga keselarasan emosi 
dan pengungkapannya melalui keterampilan personal atau personal competence 
dan keterampilan sosial atau social competence. Personal competence terdiri dari 
kemampuan (1) mengenali emosi diri; kemampuan untuk mengenali perasaan 
dalam diri sewaktu perasaan itu terjadi; (2) mengelola emosi, yaitu upaya yang 
dilakukan seseorang untuk menyeimbangkan keadaan emosi yang dirasakannya; 
(3) memotivasi diri sendiri; kemampuan menata emosi sebagai alat untuk 
mencapai tujuan dan memiliki kemampuan dalam memiliki pandangan yang 
positif dalam menilai segala sesuatu yang terjadi dalam dirinya. Pada 
keterampilan sosial atau social competence terdiri dari kemampuan  (1) mengenali 
emosi orang lain, yaitu kemampuan untuk berempati, mampu menangkap sinyal-
sinyal sosial yang tersembunyi yang mengisyaratkan apa yang dibutuhkan oleh 
orang lain sehingga lebih mampu menerima sudut pandang orang lain; (2) 
membina hubungan, yaitu meliputi keterampilan sosial yang menunjang 
popularitas, kepemimpinan dan keberhasilan antarpribadi. 
Menurut Goleman (2007), kecerdasan emosional ini dipengaruhi oleh 
beberapa faktor, salah satunya adalah faktor keluarga. Kehidupan keluarga 
merupakan sekolah pertama dalam mempelajari emosi. Kecerdasan emosional 
dapat diajarkan pada saat masih bayi melalui ekspresi. Peristiwa emosional yang 
terjadi pada masa anak-anak akan melekat dan menetap secara permanen hingga 
dewasa. Kehidupan emosional yang dipupuk dalam keluarga sangat berguna bagi 
anak kelak dikemudian hari. Proses mempelajari emosi berawal dari figur-figur 
terdekat dengan anak saat awal kehidupan seorang anak misalnya ibu dan ayah 
sebagai orangtua, ataupun figur pengasuh yang memberikan rasa nyaman dan 
aman bagi anak. 
Sejalan dengan faktor keluarga yang dipaparkan oleh Goleman, fokus pada 
penelitian ini adalah pada attachment style merupakan suatu relasi antara seorang 
bayi dengan figur pengasuh signifikan baginya. Beberapa penelitian telah 
menemukan bahwa seorang yang memiliki secure attachment lebih mampu 
menanggulangi emosi negatif dalam interaksi sosial dibandingkan dengan orang 
dengan insecure attachment (Kobak, & Sceery, 1988), memiliki lebih banyak 
emosi positif dalam berinteraksi secara sosial (Simpson, 1990), dan memiliki 
kemampuan regulasi emosi yang positif (Cooper et. Al., 1998). Faktor lain yang 
memengaruhi kecerdasan emosional  seseorang adalah faktor non-keluarga 
(Goleman, 1997). Faktor non-keluarga adalah lingkungan dimana individu 
tumbuh, umumnya adalah lingkungan bermain dan sekolah. Lingkungan ini turut 
memengaruhi kemampuan individu dalam berempati terhadap orang lain. 
Untuk mengetahui gambaran mengenai fenomena yang terjadi pada 
mahasiswa baru, peneliti telah melakukan survey awal terhadap mahasiswa baru 
Fakultas Psikologi Universitas Kristen Maranatha Bandung. Berdasarkan hasil 
survey awal terhadap 30 responden, 40% (12 orang) diantaranya menghayati 
memiliki secure attachment style. Pada dasarnya mereka memiliki model of self 
yang positif. Mereka merasa nyaman pada dirinya sendiri saat ini dan merasa 
bahwa dirinya layak untuk diterima oleh teman-temannya kuliahnya. Selain itu, 
mereka memiliki model of other yang positif. Mereka merasa teman-teman 
kuliahnya ada untuk dirinya ketika dibutuhkan dan juga memberikan dukungan 
bagi dirinya. Ia menghayati bahwa mudah bagi dirinya untuk memiliki kedekatan 
secara emosional dengan orang lain. selain itu, mereka merasa nyaman 
bergantung pada orang lain di sekitarnya dan sebaliknya. Mereka tidak khawatir 
apabila harus sendiri dalam menjalani aktivitas sehari-hari dan juga tidak merasa 
khawatir bahwa orang lain akan menolak kehadirannya. Beberapa responden 
merasa cukup mampu mengenali emosi diri, mengelola emosi, memotivasi diri, 
mengenali emosi orang lain dan membina hubungan. Responden lainnya  
menghayati bahwa dirinya merasa kurang mampu mengenali emosi orang lain, 
dimana hal tersebut dirasakannya ketika belajar dalam kelompok. Ia tidak 
menyadari kapan teman kelompok belajarnya itu merasa sedih, senang, bosan, 
jenuh dan lainnya. Sebagian lainnya menghayati bahwa dirinya kurang mampu 
mengelola emosi. Hal tersebut dirasakannya ketika mengerjakan tugas kelompok, 
ia merasa kurang mampu mengolah rasa jenuh sehingga saat rasa jenuh datang, ia 
tidak bisa fokus pada penyelesaian tugas kelompok. 
30% (9 orang) diantaranya menghayati mereka memiliki preoccupied 
attachment style. Pada dasarnya, mereka memiliki model of self yang negatif. 
Mereka merasa tidak nyaman pada dirinya sendiri saat ini dan merasa dirinya 
kurang layak untuk diterima oleh teman-teman kuliahnya karena merasa  memiliki 
kemampuan yang kurang dalam hal akademik. Selain itu, mereka memiliki model 
of other yang positif. Mereka merasa khawatir bahwa teman-teman kuliahnya 
tidak ada untuk dirinya ketika dibutuhkan dan juga memberikan dukungan bagi 
dirinya. Sebagian responden merasa dirinya kurang mampu mengenali emosi diri, 
sehingga saat suasan hati tidak menentu, dapat menghambatnya dalam belajar. 
Hal tersebut juga berkaitan dengan kemampuan memotivasi diri. Responden  
merasa bahwa ia hanya mampu mengerjakan tugas dengan baik ketika suasana 
hati sedang baik. Responden lainnya merasa kurang mampu mengelola emosi diri. 
Ia merasakan hilangnya minat untuk mengerjakan tugas kelompok ketika ia 
merasa kesal terhadap salah satu teman dalam kelompok belajarnya. Ia juga 
merasa kurang mampu memotivasi diri untuk menyelesaikan tugas secepatnya. 
Sedangkan sejumlah responden lainnya merasa bahwa mereka kurang mampu 
mengenali emosi diri. Mereka seringkali tidak memahami emosi yang mereka 
rasakan, sehingga berdampak pada suasana hati yang tidak menentu. Ketika 
dituntut untuk tetap menyelesaikan tugas, mereka merasa tidak bersemangat. 
Sebanyak 20% (6 orang) memiliki dismissing attachment style. Pada 
dasarnya mereka memiliki model of self yang positif. Mereka memandang dirinya 
merasa nyaman walaupun tidak memiliki relasi yang dekat dengan orang lain.  Ia 
juga merasa bahwa dirinya mampu mandiri. Selain itu, mereka memiliki model of 
other yang negatif. Ia merasa mampu memenuhi kebutuhannya sendiri sehingga  
memilih untuk tidak bergantung kepada orang lain dan tidak membiarkan orang 
lain bergantung padanya. Sebagian responden merasa kesulitan dalam membina 
hubungan karena ia merasa gugup ketika harus berkenalan terlebih dahulu dengan 
orang lain. Hal tersebut mempengaruhi kinerjanya dalam kelompok. Ia merasa 
kurang mampu menyampaikan pendapat-pendapatnya karena merasa pendapatnya 
tidak akan didengarkan oleh teman dalam kelompok belajar. Sejumlah responden 
lainnya merasa bahwa dirinya kurang mampu mengenali penyebab emosi yang 
dirasakannya, sehingga seringkali dikuasai oleh perasaannya, misalnya ketika 
suasana hatinya sedang gembira maka ia akan semangat dalam mengerjakan tugas 
dan beraktivitas. Sebaliknya, ketika suasana hatinya sedang tidak menentu tanpa 
sebab yang jelas, ia merasa malas dalam beraktivitas dan tidak bersemangat. 
Sebanyak 10% (3 orang) memiliki fearful attachment style. Mereka 
memiliki model of self dan model of other yang negatif. Tidak merasa nyaman 
dengan dirinya apa adanya karena alasan bahwa dirinya memiliki sifat pemalu dan 
sulit menjalin hubungan dengan orang lain. Sebagian responden merasa minder 
karena menurutnya dia tidak pintar dan tidak ada yang bisa diunggulkan. 
Akibatnya mereka hanya berbicara ketika ditanya dan tidak berani untuk bertanya 
lebih dulu. Selain itu, ia juga memandang bahwa orang lain tidak mampu 
memenuhi kebutuhannya akan pertemanan. Ia merasa orang lain enggan untuk 
mengajaknya berbicara. Responden seringkali tidak mengetahui emosi yang 
muncul dalam dirinya. Suasana hatinya seringkali tidak menentu tanpa penyebab 
yang jelas. Hal tersebut berdampak pada aktivitas kuliahnya sehari-hari, dimana 
mereka larut dalam suasana emosi tersebut dan tidak bersemangat dalam 
menjalani kuliah. Dalam kelompok belajar, ia merasa pasif dan tidak peduli 
dengan teman anggota kelompok yang lain. Mereka  merasa bahwa sulit baginya 
untuk membina hubungan pertemanan yang akrab. Mereka merasa lebih cocok 
dengan proses belajar yang tidak menuntut kerjasama dalam tim. 
Adult attachment style dan kecerdasan emosional masing-masing memiliki 
dua komponen yang mengarah pada diri individu yang bersangkutan dan 
mengarah pada orang lain atau lingkungan sosial. Peneliti memiliki asumsi bahwa 
model of self pada adult attachment style akan sejalan dengan personal 
competence pada kecerdasan emosional. Apabila individu memiliki model of self 
yang positif maka ia juga akan memiliki kemampuan yang tinggi dalam 
komponen personal competence dalam kecerdasan emosional. Apabila individu 
memiliki model of others yang positif maka ia akan memiliki kemampuan yang 
tinggi dalam komponen social competence. 
Oleh karena itu, berdasarkan fenomena diatas ingin diketahui apakah 
terdapat hubungan antara adult attachment style dengan kecerdasan emosional 
pada mahasiswa baru di fakultas psikologi universitas  X  di kota Bandung. 
Penelitian ini ingin mengetahui apakah terdapat hubungan antara adult 
attachment style dengan derajat kecerdasan emosional pada mahasiswa baru 
angkatan 2014 di Fakultas Psikologi Universitas Kristen Maranatha di Kota 
Bandung. 
1. Untuk memeroleh gambaran mengenai attachment style pada mahasiswa 
baru angkatan 2014 Fakultas Psikologi Universitas  X  di Kota Bandung 
2. Untuk memeroleh gambaran mengenai kecerdasan emosional pada 
mahasiswa baru angkatan 2014 Fakultas Psikologi Universitas  X  di Kota 
Untuk mengetahui apakah terdapat hubungan antara adult attachment style 
terhadap kecerdasan emosional pada mahasiswa baru angkatan 2014 Fakultas 
Psikologi Universitas  X  di kota Bandung. 
1. Memberikan informasi bagaimana keterkaitan antara variabel adult 
attachment style dan kecerdasan emosional dapat memengaruhi proses 
belajar yang responden jalani dalam perkuliahan 
2. Menjadi bahan evaluasi untuk fakultas psikologi dalam memfasilitasi 
masing-masing mahasiswa agar lebih mampu mengenai dirinya sendiri 
1. Memberikan sumbangsih informasi dan ilmu pengetahuan terhadap 
pengembangan bidang kadian psikologi pendidikan dan psikologi 
perkembangan. 
Individu yang menjadi seorang mahasiswa baru akan megalami banyak 
perubahan dan pengalaman baru. Pada masa ini, individu tersebut mengalami 
perubahan dalam aktivitas yang mereka alami saat kegiatan belajar di kampus dan 
juga dari relasi sosial yang dijalin. Tuntutan yang diberikan kepada mereka lebih 
berat jika dibandingkan dengan ketika mereka berada di sekolah menengah atas. 
Mereka harus beradaptasi kembali dengan teman-teman baru dan model 
pembelajaran yang baru. Kurikulum Berbasis Kompetensi (KBK) yang diterapkan 
kepada mahasiswa di fakultas psikologi universitas  X  di kota Bandung menjadi 
tuntutan tersendiri. Dalam kurikulum KBK, mereka harus menjalankan sejumlah 
model pembelajaran yang dihayati cukup berat oleh mereka karena mahasiswa 
memiliki peran yang lebih aktif dibandingkan dengan dosen. Proses belajar seperti 
itu merupakan Student Centered Learning (SCL), dimana iklim pembelajaran 
yang dikembangkan adalah kolaboratif, suportif dan kooperatif.  
Tidak semua mahasiswa merasa mampu belajar secara efektif bila berada 
dalam kelompok belajar. Dalam kelompok belajar akan melibatkan interaksi 
setiap anggota kelompok untuk menyelesaikan permasalahan dan kesulitan yang 
dialami dalam proses belajar. Dalam proses belajar, mahasiswa mengalami 
perbedaan pendapat antara anggota-anggota dalam kelompoknya sehingga 
memicu adanya konflik. Dalam menghadapi situasi menekan tersebut dibutuhkan 
adanya kecerdasan emosional agar emosi yang dirasakan oleh masing-masing 
anggota kelompok tidak menggangu relasi yang terjalin dalam anggota kelompok 
dan  iklim bekerja sama dalam kelompok tetap terbentuk. Kecerdasan emosional 
adalah derajat kemampuan seseorang dalam mengenali emosi diri, mengelola 
emosi, memotivasi diri sendiri, mengenali emosi orang lain (empati), dan 
kemampuan untuk membina hubungan (kerjasama) dengan orang lain (Goleman, 
2007). Goleman (1995) mengelompokan kelima aspek tersebut menjadi dua 
komponen besar besar yaitu komponen personal competence yang terdiri dari 
kemampuan mengenali emosi diri, mengelola emosi dan memotivasi diri dan 
komponen social competence yang terdiri dari kemampuan empati dan 
kemampuan membina hubungan. 

