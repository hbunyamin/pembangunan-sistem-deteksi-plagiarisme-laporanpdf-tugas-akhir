Pacaran merupakan sebuah konsep "membina" hubungan dengan orang lain dengan 
saling mengasihi, saling mengenal, dan juga merupakan sebuah aktifitas sosial dimana dua 
orang yang berbeda jenis kelaminnya untuk terikat dalam interaksi sosial. Melalui pacaran, 
individu berharap dapat lebih mengetahui sifat dan sikap dari pasangannya untuk menentukan 
hubungan kedepan. Dalam menjalin hubungan pacaran, terdapat fungsi dan pengharapan yang 
ingin diperoleh oleh individu yang menjalaninya. Fungsi utama dari pacaran adalah untuk 
mengembangkan hubungan interpersonal individu pada hubungan heteroseksual sampai 
dengan pernikahan. Melalui hubungan pacaran, individu juga memiliki pengharapan tersendiri 
akan pemuasan kebutuhannya. Kebutuhan tersebut dapat berupa kebutuhan afeksi (Papalia, 
2001).  
 Pada masa dewasa awal, berpacaran sudah lebih terfokus dalam upaya mencari 
pasangan hidup, karena pada masa ini individu mulai berpikir untuk mencari pasangan hidup 
dengan membina hubungan saling mendukung untuk membangun kehidupan yang akan 
datang, sehingga proses pacaran bukan semata untuk kesenangan. Menurut Erikson, individu 
pada usia 18 - 40 tahun masuk pada fase intimacy vs isolation. Pada fase ini seseorang 
diharapkan sudah memiliki komitmen untuk menjalin suatu hubungan dengan individu lain. 
Individu sudah mulai selektif untuk membina hubungan yang akrab dengan individu lainnya. 
Tujuan dari menjalin kedekatan (pacaran) dengan individu lain adalah untuk menemukan dan 
mencari pasangan yang benar-benar tepat untuk dirinya dan kelak akan menjadi pasangan 
hidupnya (Dusek, 1996). Individu yang telah mapan baik dalam segi finansial dan psikologis 
akan memiliki tujuan hubungan yang mengarah pada pernikahan.  
Pernikahan merupakan tujuan bagi setiap pasangan yang memasuki usia dewasa awal. 
Hubungan yang dijalani menjadi lebih serius dengan mendekatkan keluarga satu sama lain. 
Kedekatan yang dijalani oleh keduanya bukan hanya sekedar hubungan sebagai pasangan 
yang 'pacaran', namun pasangan tersebut akan mulai menata masa depan yang akan dijalani 
bersama. Setiap individu yang menjalani relasi interpersonal yang relatif lama umumnya 
memiliki keinginan untuk maju ke tahap yang lebih serius. Masing-masing pasangan akan 
berusaha mengenalkan dan mendekatkan diri dengan keluarga pasangan. Pasangan juga akan 
lebih terbuka dalam mengungkapkan perasaan dan masalah yang dihadapi. Pasangan laki-laki 
akan lebih memikirkan dan merencanakan keuangannya untuk masa depan keluarganya kelak, 
begitu juga dengan perempuan yang harus memikirkan tanggung jawab dalam mengurus 
rumah tangga. 
Keinginan untuk menjaga hubungan pasangan yang sesuai dan sejalan agar hubungan 
intim tetap terjaga dan komitmen yang dijalani tidak akan lepas dari adanya sebuah konflik.  
Masalah-masalah besar maupun kecil akan muncul karena perbedaan pendapat, adanya 
sensitifitas tinggi diantara pasangan sehingga muncul kesalahpahaman yang berdampak pada 
pertengkaran, muncul keragu-raguan untuk menikah dengan calon yang telah ditetapkan, 
masalah keuangan untuk biaya menikah, mengurus persyaratan nikah, perbedaan - perbedaan 
kebudayaan, perbedaan agama dan interaksi antar individunya. Masalah-masalah tersebut 
dapat saja terjadi pada pasangan secara umum.  
Berkaitan dengan perbedaan kebudayaan dan perbedaan agama yang menjadi bagian 
dari kemungkinan munculnya konflik dalam hubungan, Negara Kesatuan Republik Indonesia 
(NKRI) merupakan negara kepulauan yang cukup luas wilayahnya. Jumlah penduduknya 
tahun 2014 sebesar 252.124.458 jiwa terdiri dari beraneka ragam suku, bahasa  dan agama. 
Terdapat 6 agama yang diakui di Indonesia saat ini, yakni Agama Islam, Kristen Protestan, 
Katolik, Hindu, Buddha dan Kong Hu Cu. Perbedaan agama di Indonesia akan memberikan 
pengaruh baik secara langsung maupun tidak langsung pada interaksi yang terjadi antara satu 
individu dengan individu yang lain dalam menjalin sebuah relasi interpersonal.  
Masalah umum yang sebelumnya telah dipaparkan mungkin saja dialami oleh 
pasangan satu agama, dan pada kenyataannya masalah pada pasangan beda agama dipandang 
lebih complicated. Dapat diketahui bahwa masalah yang dialami oleh pasangan beda agama 
lebih beragam serta memiliki lebih banyak kemungkinan-kemungkinan penyebab  keruhnya  
hubungan pacaran pasangan tersebut. Pebedaan kepercayaan bukanlah hal yang diutamakan 
dalam relasi individu yang memutuskan untuk berpacaran, sejauh keduanya menyadari bahwa 
hubungan percintaan mereka bukanlah sesuatu yang serius. Namun, berbeda halnya dengan 
pasangan yang berfikir untuk meneruskan hubungan ke pernikahan. Perbedaan agama 
mungkin menjadi sorotan dan juga penyebab dibalik masalah-masalah yang muncul.  
Adanya perbedaan dalam prinsip hidup yang mendasar memberikan  percikan api  
pada hubungan beda agama. Perbedaan agama tersebut menciptakan individu dengan ajaran 
dan norma-norma yang berbeda. Ajaran dan norma yang berbeda mengakibatkan kesulitan 
dalam pemecahan masalah yang muncul dalam hubungan individu karena perbedaan pola 
pikir dan prinsip hidup. Situasi tersebut muncul ketika status pacaran tersebut akan 
ditingkatkan ke tahap yang lebih serius, mulai dari pihak mana yang harus  mengalah , 
biasanya baik pria maupun wanita saling bersikukuh dengan keyakinannya dan saling 
mengajak pasangannya untuk  ikut  keyakinannya, kalaupun ada yang  mengalah  dan 
bersedia mengikuti pacarnya, biasanya orang tua yang menolak bahkan mungkin memisahkan 
mereka ketika pasangan ingin melanjutkan hubungan pacaran ke tingkat yang lebih serius. 
Kesulitan akan dirasakan pada saat individu memiliki keinginan untuk mengenal lebih dekat 
keluarga dari masing-masing individu pasangan beda agama tersebut. Merasa asing dan 
terkucil mungkin terjadi ketika yang bersangkutan berada di tengah keluarga besar pasangan 
dengan kebiasaan dan adat yang berbeda. 
Hubungan yang dijalani akan rentan putus pada pasangan beda agama karena 
tantangan yang akan dihadapi umumnya lebih besar daripada hubungan satu agama. 
Umumnya hubungan seperti ini akan jalan di tempat hingga bertahun-tahun. Rasa bersalah 
yang muncul dalam benak individu ketika menjalin hubungan dengan pasangan yang berbeda 
keyakinan, biasanya ada rasa bersalah yang menghantui, meskipun sedikit dan berhasil 
ditekan. Berdasarkan kutipan dari merdeka.com oleh  Tantri Setyorini menyatakan bahwa, 
pasalnya sejak kecil dalam benak individu pada umumnya sudah ditanamkan bahwa 
keyakinan adalah benar, dan diharapkan untuk mencari pasangan dengan keyakinan yang 
sama.  
Orientasi masa depan dalam bidang pernikahan penting bagi seseorang terutama pada 
individu yang sudah masuk ke tahap perkembangan usia dewasa awal karena menyangkut 
cara seseorang untuk menentukan dan menghadapi masa depannya dalam hal pernikahan. 
Individu yang sudah memasuki tahap perkembangan dewasa awal memiliki tuntutan tahap 
perkembangan berkaitan dengan orientasi masa depan yang mengarah pada pernikahan. Beda 
halnya dengan individu yang masih pada tahap remaja. Menurut Erikson, proses mencarian 
jati diri baru mulai berlangsung dalam tahap perkembangan remaja (identity vs confusion) dan 
belum memiliki tuntutan khusus untuk menentukan masa depan pernikahannya. Adanya 
orientasi masa depan berarti seseorang telah melakukan antisipasi terhadap kejadian-kejadian 
yang mungkin timbul di masa depan. Hal ini berkaitan dengan bagaimana seseorang 
menentukan pasangan yang tepat untuk masa depannya. 
Orientasi masa depan yang sudah mulai direncanakan oleh pasangan dewasa muda 
akan menimbulkan banyak pertimbangan ketika adanya latar belakang berbedanya 
kepercayaan. Seperti yang terjadi di Univertsitas "X" di Bandung ini yang merupakan kampus 
besar dengan keanekaragaman asal daerah, budaya, dan agama didalamnya. Meskipun 
mayoritas mahasiswa Universitas  X  di Bandung memeluk agama Kristen, masing-masing 
mahasiswa memiliki toleransi, keterbukaan, dan kebebasan dalam bersosialisasi dengan 
agama lain menjadi awal bagi mahasiswa mengenal individu lain. Dalam proses interaksi 
tersebut menimbulkan kemungkinan tumbuhnya rasa kenyamanan dengan lawan jenis tanpa 
memerhatikan kepercayaan yang berbeda. Dengan atas dasar rasa nyaman dan rasa sayang 
yang tumbuh, relasi tersebut menjadi semakin dekat dan intim. Seiring berjalannya waktu, 
mahasiswa yang menjalani hubungan berbeda agama ini menemukan permasalahan yang 
telah disebutkan sebelumya. Masalah-masalah tersebut muncul dan selalu berakhir dengan 
pertanyaan mengenai tujuan hubungan yang sedang dijalani saat ini dengan pasangan dapat 
mengarah ke pernikahan atau tidak. Semakin lama hubungan tersebut dijalani, mahasiswa 
yang menjalani hubungan ini akan berkembang pemikirannya untuk menentukan orientasi 
masa depan mengenai hubungan tersebut dengan latar belakang agama yang berbeda.  
Berdasarkan hasil survey yang telah dilakukan peneliti kepada 10 mahasiswa yang 
menjalani hubungan berbeda agama, dapat diketahui 50% mahasiswa menyatakan bahwa 
mereka memiliki minat untuk membawa hubungan pacaran ini ke jenjang yang lebih serius, 
meskipun beberapa dari mereka menyatakan adanya sedikit rasa tidak yakin dengan masalah 
yang akan muncul sebagai pengahalang untuk mencapai tujuan, namun sejauh ini mereka 
mampu menyelesaikan permasalahan yang muncul baik dari lingkungan teman maupun 
lingkungan keluarga. Akan tetapi 50% diantaranya mengakui bahwa mereka belum memiliki 
minat untuk membawa hubungan yang dijalani ke jenjang yang lebih serius. Mereka 
memberikan sedikit penjabaran bahwa ada kebingungan untuk memulai, rasa takut terhadap 
penolakan orang tua dari pasangan, dan mereka mempertahankan karena sudah sangat sayang 
tetapi belum berani untuk membuat langkah yang lebih jauh. 
Hal lain, 40% mahasiswa menyatakan bahwa mereka memiliki rencana-rencana yang 
berkaitan dengan masa depan hubungan mereka. Rencana yang diutamakan adalah mendekati 
dan meyakini orang tua pasangan mengenai keseriusan hubungan yang dijalani dengan 
pasangan, meskipun beberapa dari mereka mendapat respon baik dari keluarga akan tetapi 
mereka ada mendapatkan penolakan. Penolakan tersebut berupa kurang diikut sertakan dalam 
acara keluarga besar, orang tua tidak menatap saat berbicara, dan juga ada orang tua yang 
sama sekali tidak ingin bertemu. Penolakan itu tidak membuat mereka berhenti untuk 
mendekati keluarga, mereka dan pasangan membuat rencana lain untuk medekatkan diri 
seperti mendekati adiknya terlebih dahulu atau kakak. Berbeda dengan 60% mahasiswa lain, 
beberapa tidak memiliki rencana-rencana terkait tujuan hubungan mereka ke arah pernikahan. 
Beberapa dari mereka menjelaskan bahwa tujuan ada tetapi untuk mencapai hal tersebut 
mereka belum berbuat apapun. Mereka yang mendapat penolakan keras dari keluarga 
membuat mereka enggan memikirkan langkah perencanaan ke arah pernikahan. Adanya rasa 
keinginan yang kuat untuk menikah karena umur yang sudah matang tetapi mereka lebih 
memilih untuk mempertahankan hubungan yang  jalan ditempat . 
Seiring dengan adanya tujuan yang telah ditetapkan, 50% pasangan suka berbincang 
mengenai hal-hal yang kurang dan harus diperbaiki dalam hubungan. Satu sama lain 
mengungkapkan perasaan dan gagasan untuk menentukan sebuah solusi permasalahan yang 
dihadapi. Masalah itu biasanya timbul dari adanya rasa jenuh karena hubungan yang beda 
agama yang dirasakan mereka sulit untuk mengarah ke pernikahan, kesalahan yang pasangan 
lakukan karena berbeda dengan kebiasaan keluarga, dan mengevaluasi perilaku yang baik 
pada saat pendekatan dengan keluarga besar. Akan tetapi beberapa dari mereka menyatakan 
bahwa terkadang mereka tidak menemukan jalan keluar karena alasan beda agama, 
permasalahan tersebut berakhir tanpa solusi dan mencoba melupakan masalah tersebut. 
Kemudian, 50% diantaranya berbincang hanya saat ada masalah yang sangat besar saja. 

