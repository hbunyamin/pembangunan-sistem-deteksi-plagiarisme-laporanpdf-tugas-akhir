Beberapa tahun terakhir ini, negara Indonesia sedang mengalami 
kemunduran hampir disegala bidang kehidupan, bermula dari krisis ekonomi yang 
berdampak pada bidang lainnya. Krisis ekonomi yang melanda Indonesia 
berdampak negatif terhadap kesejahteraan masyarakat, diantaranya meningkatnya 
jumlah pengangguran, meningkatnya kemiskinan dan menurunnya taraf hidup 
masyarakat. Kesulitan yang harus dihadapi, menimbulkan tantangan-tantangan 
yang harus dilalui guna memperoleh kehidupan yang lebih baik.  
Berkaitan dengan krisis ekonomi di atas, hampir semua bidang usaha 
mengalami dampaknya, khususnya dalam mempertahankan kelangsungan hidup 
perusahaannya.  Industri tekstil adalah salah satunya. Beribu-ribu karyawan 
terpaksa harus di-PHK atau dirumahkan karena industri-industri tekstil tersebut 
gulung tikar. Ini berarti, tingkat pengangguran semakin meningkat dan dampak-
dampak sosial yang mungkin ditimbulkannyapun semakin beragam. Industri 
tekstil bersaing semakin ketat, tidak hanya menghadapi persaingan dengan 
industri tekstil dalam negeri melainkan dengan industri tekstil dari luar negeri 
seperti China, Hongkong, India. 
Dalam menghadapi persaingan, ada perusahaan yang berhasil dan 
karenanya dapat mempertahankan kelangsungan hidupnya tetapi tidak kurang 
pula yang tidak berhasil. Menurut data yang diperoleh awal tahun 2003, dari 
2.738 industri tekstil di Indonesia, sebanyak 40% telah mati dalam arti sama 
sekali menghentikan produksinya. Sebagian besar (60%) diantaranya berlokasi di 
Jawa Barat (Kompas, 16 Maret 2003). Industri-industri tekstil tersebut mati 
karena tidak dapat bertahan hidup dalam kondisi krisis ekonomi. Sejak Indonesia 
mengalami krisis ekonomi, secara otomatis industri tekstil Indonesia dihadapkan 
pada berbagai masalah yang berakibat pada peningkatan biaya produksi secara 
drastis dan berdampak pada kelangsungan hidup industri tekstil tersebut. Adapun 
beberapa hal yang berdampak kepada peningkatan biaya produksi ini adalah 
kenaikan tarif dasar listrik, kenaikan BBM, besarnya pajak bahan baku, dan 
meningkatnya biaya tenaga kerja. 
Industri tekstil Indonesia kalah bersaing dengan industri tekstil dari  
negara luar di pasar dalam negeri. Pasar lokal memilih barang-barang dari luar 
negeri dengan alasan harga lebih murah, desain atau corak kain lebih variatif dan 
kualitasnya terjamin, sedangkan barang lokal lebih mahal, coraknya kurang 
variatif dan kualitasnya kurang terjamin (Kompas, 29 Desember 2004). Tekstil 
Indonesia saat ini harus bersaing dengan produk dari negara-negara lain seperti 
China dan India yang merupakan dua negara dengan industri tekstil terkuat. China 
menguasai 51% dan India 13% pasar tekstil, disamping Thailand, Vietnam, Turki, 
Bangladesh, dan Pakistan. Tekstil Indonesia hanya menguasai dua persen pasar 
tekstil dunia (Tempo Interaktif, 17 Mei 2005). Diberlakukannya AFTA 
(ASEAN Free Trade Area) dan penghapusan kuota sejak 1 Januari 2005, 
memperburuk kondisi industri tekstil Indonesia dan mendorongnya masuk dalam 
persaingan yang sangat ketat dengan industri tekstil dari luar Indonesia. 
Untuk dapat bertahan dalam keadaan yang sulit ini, industri tekstil 
Indonesia harus mampu bangkit dari keterpurukannya, mencoba membenahi diri, 
misalnya dengan meningkatkan kualitas SDM (Sumber Daya Manusia) agar dapat 
mengolah dengan benar SDA (Sumber Daya Alam) Indonesia yang potensial, 
meningkatkan kualitas hasil produksi, meningkatkan kreativitas dalam membuat 
desain. Di Jawa Barat, tepatnya di daerah Nanjung, terdapat industri tekstil yang 
berhasil bertahan dalam masa sulit ini. Menurut HRD PT.  X  ini, perusahaan 
mengalami penurunan permintaan konsumen yang berarti, tetapi bagaimanapun 
juga perusahaan harus berusaha mempertahankan kelangsungan hidupnya, maka 
diambillah beberapa kebijakan untuk dapat menyelamatkan perusahaan.  
Melihat persaingan yang sangat ketat khususnya dengan produk tekstil 
dari luar negeri, maka perusahaan meningkatkan daya saingnya dalam beberapa 
hal, berikut ini: 
? menerima semua permintaan konsumen yang masuk minimal 100 
? membuat desain-desain warna dan corak baru yang sedang trend 
maupun yang belum ada di pasaran 
? meningkatkan kualitas kain yang dihasilkan menjadi grade A 
Usaha-usaha yang sudah dilakukan memang berhasil mempertahankan 
kelangsungan hidup perusahaan, akan tetapi  kebijakan-kebijakan yang ditempuh 
masih belum mampu mendongkrak permintaan. 
Penurunan permintaan atas produk tekstil perusahaan ini berdampak 
kepada banyaknya karyawan yang tidak ada pekerjaan, sehingga diambil 
keputusan untuk  merumahkan  karyawan dan memotong hari kerja (dari enam 
menjadi lima hari) sebagai upaya menekan pengeluaran. PT  X  telah mengalami 
empat periode dalam merumahkan karyawannya sepanjang tahun 1997 - 2000, 
periode pertama 60 orang, kedua 50 orang, ketiga 30 orang, dan keempat 15 
orang. Perampingan yang dilakukan perusahaan cukup signifikan mengingat 
PT. X  pada dasarnya termasuk industri kecil. Segala  upaya yang dilakukan 
PT. X  belum mampu memulihkan keadaan perusahaan dikarenakan kebijakan-
kebijakan pemerintah yang semakin menekan industri tekstil. 
Kebijakan-kebijakan pemerintah tersebut sering menuai aksi demo yang 
secara umum berdampak pada penurunan produktivitas perusahaan. Absensi 
meningkat, kualitas kerja menurun, dan target produksi yang ditetapkan tidak 
tercapai, sehingga keuntungan perusahaan berkurang banyak. Kondisi-kondisi 
demikian berusaha di atasi dengan mengedepankan sisi kebutuhan-kebutuhan 
karyawan sebagai manusia. Karyawan menuntut kenaikan upah, yang tentu saja 
tidak dapat dipenuhi perusahaan dalam kondisi seperti sekarang ini, maka hal 
yang dilakukan perusahaan adalah menjanjikan bonus bagi setiap karyawan yang 
persentase ketidakhadirannya rendah, rajin bekerja, disiplin, dan bertanggung 
jawab. Sebaliknya  perusahaan menetapkan sangsi potong gaji bagi karyawan 
yang ketidakhadirannya tinggi, malas-malasan, kurang disiplin, dan kurang 
bertanggung jawab. Hal ini dilakukan perusahaan sebagai langkah untuk 
memotivasi setiap karyawannya agar menampilkan kinerja yang lebih baik. 
Kebijaksanaan lain yang diambil adalah meningkatkan suasana 
kekeluargaan bagi semua karyawan tanpa memandang jabatan. Dalam hal ini 
semua karyawan diharuskan memakai seragam perusahaan, istirahat makan siang 
bersama di kantin perusahaan, olah raga bersama, dan piknik bersama. 
Kebijaksanaan yang diambil diharapkan dapat memotivasi setiap karyawan untuk 
memperlihatkan kinerja terbaiknya. Dampak positif lainnya adalah melatih 
objektivitas para pemimpinnya. Contohnya, Manajer menilai Kepala Bagian, 
Kepala Bagian menilai Kepala Seksi, Kepala Seksi menilai Kepala Regu, dan 
Kepala Regu menilai Karyawan biasa. 
Setiap pemimpin walaupun berbeda bidang, tetapi memiliki tugas yang 
sama yaitu menilai kinerja bawahan, mendelegasikan tugas kepada bawahan, dan 
memotivasi bawahan. Peneliti tertarik untuk mencermati lebih lanjut pada Kepala 
Regu, mengingat jumlah orang yang dipimpin setiap Kepala Regu berkisar         
15   20 orang, dan Kepala Regu terjun langsung di lapangan untuk 
mendelegasikan tugas maupun mengawasi kinerja karyawannya. Hal ini berbeda 
dengan pemimpin di level lain, yang hanya mendelegasikan tugas tanpa terjun 
langsung ke lapangan.  
Karu atau yang biasa disebut juga dengan supervisor merupakan pekerja 
lini depan atau non managerial (John R Schermerhon Jr, 1996) bertanggung 
jawab terhadap kelompok kerja tunggal untuk menghadapi tujuan kinerja jangka 
pendek sesuai dengan rencana manager tingkat atas. Tugas-tugas Karu 
diantaranya adalah menjelaskan tugas, menilai kinerja dan membina bawahan, 
memotivasi bawahan guna mempertahankan antusiasme yang tingi untuk bekerja, 
memberitahu bawahan tentang tujuan dan harapan organisasi, memberitahu 
kepada pihak yang lebih tinggi tentang tuntutan bawahan, dan merencanakan serta 
menjadwalkan pekerjaan setiap hari, minggu, dan bulan. 
Kepala Regu atau disebut Karu tentunya menjadi unsur penting bagi 
berjalannya proses produksi. Wewenang dan tugas yang disandang Karu 
menimbulkan kesulitan yang cukup besar mengingat banyaknya orang yang 
dipimpin dengan karakter yang berbeda-beda, disamping itu juga kewajiban dan 
tanggung jawab terhadap atasannya. Kondisi yang terjadi di lapangan juga 
menimbulkan kesulitan tersendiri, contohnya pernah terjadi mesin tiba-tiba rusak, 
yang menuntut Karu untuk cepat tanggap membaca situasi dan mengendalikan 
keadaan. Produktivitas Karu dipantau terus oleh Kasie, sehingga sepanjang tahun 
2003 terjadi penggantian tiga orang Karu karena dinilai kurang produktif. 
Hasil wawancara terhadap lima orang Karu menyatakan bahwa kesulitan 
yang mereka hadapi sebagai Karu relatif sama, yaitu merasa kewalahan 
mengawasi sekaligus memimpin karyawan yang jumlahnya cukup banyak dengan 
karakter yang beragam, dan kasus terbanyak adalah ketidakdisiplinan dalam 
menggunakan alat pengaman saat bekerja (masker, sarung tangan, tutup kepala). 
Empat orang mengatakan kondisi itu di atasi dengan menegur bawahan. Satu 
orang lainnya mengatakan dibiarkan saja sebab pernah ditegur namun tetap 
membandel. Hal ini menyatakan bahwa ada Karu yang merasa ketidakdisiplinan 
tidak akan berlangsung lama jika diberi teguran, Karu yang lain merasa walau 
ditegur tidak akan ada perubahan akan tetap membandel.  
Kesulitan lain yang dirasakan adalah berkonsentrasi pada pekerjaan dalam 
kondisi ketika banyak masalah keluarga yang muncul sebagai akibat krisis 
ekonomi. Dua orang mengatakan dirinya harus professional dan berkonsentrasi 
penuh dalam bekerja, masalah pekerjaan tidak boleh dicampuradukkan dengan 
masalah keluarga. Tiga orang mengatakan kadang-kadang kurang berkonsentrasi 
dalam bekerja karena lebih memikirkan masalah keluarga dibandingkan masalah 
pekerjaan itu sendiri. Pengakuan mereka menggambarkan ada Karu yang dapat 
membatasi dan memilah masalah yang dihadapi, dan ada Karu yang kurang 
mampu membatasi dan memilah masalah yang dihadapi. 
Ketika dimintai pendapat mengenai aksi protes yang dilakukan karyawan, 
kelima Karu tersebut berpendapat bahwa aksi protes tersebut tidak akan menolong 
bahkan akan semakin memperburuk keadaan ekonomi perusahaan. Mereka 
mengatakan bahwa akan lebih membantu jika setiap karyawan bekerja dengan 
giat agar produktivitas meningkat dan kondisi perusahaan stabil sehingga tidak 
ada yang harus kehilangan pekerjaan lagi. Mereka percaya jika setiap karyawan 
menghadapi kondisi ini dengan kepala dingin, maka keadaan akan membaik dan 
tidak perlu lagi melancarkan aksi demo. Pendapat kelima Karu tersebut 
menunjukkan tanggung jawab dan perasaan memiliki yang tinggi, serta keyakinan 
bahwa situasi sulit akan berlalu. 
Ketika disinggung tentang krisis ekonomi, Karu menyatakan bahwa 
kondisi ekonomi seperti sekarang ini sangat meresahkan dan mengganggu 
konsentrasi mereka dalam bekerja. Kelima orang Karu tersebut memiliki pendapat 
yang sama bahwa krisis ekonomi menjadi pemicu timbulnya kesulitan di 
perusahaan tempat mereka bekerja, dan kesejahteraan hidup karena mereka semua 
telah berkeluarga. Tiga orang mengatakan bekerja sampingan untuk menambah 
penghasilan, dan dua orang lainnya h arus sangat berhemat agar gaji yang didapat 
cukup untuk memenuhi kebutuhan sehari-hari. Jawaban yang berbeda 
menandakan bahwa Karu melihat masalah yang sama dengan sudut pandang yang 
berbeda. Ada Karu yang merasa mampu mengatasi kesulitan ekonomi yang 
dialami dengan bekerja sampingan, dan ada Karu yang merasa pasrah dengan 
harus hidup hemat. 
Kesulitan lainnya adalah bagaimana Karu memotivasi diri sendiri agar 
tetap produktif menghadapi beban dan tanggung jawab yang berat serta rutinitas 
pekerjaan yang menimbulkan kejenuhan. Karu mengatakan untuk memotivasi diri 
sendiri mereka biasanya mengingatkan diri sendiri bahwa tujuan mereka bekerja 
adalah mendapat penghasilan guna memenuhi kebutuhan hidup keluarga.  
Untuk mampu bertahan dalam keadaan sulit ini Karu harus memiliki 
kemampuan untuk mengatasi kesulitan yang disebut AQ. Setiap Karu memiliki 
respon yang berbeda terhadap kesulitan-kesulitan yang mereka hadapi, namun 
tujuannya tetap sama, yaitu mengatasi kasulitan. Hal ini sejalan dengan teori Paul 
G. Stoltz (2000) yang mengatakan bahwa kesulitan merupakan bagian dari hidup 
manusia, dan setiap manusia memiliki kemampuan untuk mengatasi kesulitan 
yang menghadangnya. Stoltz (dalam AQ @ Work, 2003)  menggunakan istilah 
Adversity Quotient yang berarti pola tanggapan atau respon individu terhadap 
semua bentuk dan intensitas dari kesulitan, mulai dari masalah yang besar sampai 
gangguan yang kecil, dan pada akhirnya memunculkan perilaku tertentu. 
Menurut Stoltz (dalam Adversity Quotient, 2000), ada tiga tingkat 
kesulitan dalam hidup, yaitu kesulitan dimasyarakat, tempat kerja dan individual 
yang harus dihadapi setiap manusia dalam perjalanan hidupnya. Kunci dari 
mengatasi kesulitan adalah diri sendiri atau individu sendiri.
Diawali dengan 
perasaan mampu mengendalikan kesulitan (Control), perasaan bertanggung jawab 
untuk memperbaiki situasi tanpa mempedulikan penyebab kesulitan (Ownership), 
keyakinan bahwa akibat dari kesulitan yang dihadapi tidak akan mempengaruhi 
seluruh aspek kehidupan lainnya dengan kata lain kemampuan untuk membatasi 
jangkauan masalah yang dihadapi (Reach), dan keyakinan bahwa akibat dari 
situasi sesulit apapun akan berlalu dan dapat diatasi (Endurance). 

