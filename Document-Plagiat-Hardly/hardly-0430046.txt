Kesehatan adalah komponen dalam hidup yang sangat penting, tanpa 
kesehatan yang baik maka tidak tersedia modal untuk melangkah ke depan 
ataupun untuk melakukan aktivitas sehari - hari. Tubuh yang tidak sehat dapat 
disebabkan oleh beberapa hal terutama pola hidup dewasa ini, seperti 
mengkonsumsi makanan siap saji dan makanan yang mengandung bahan 
pengawet. Contohnya seperti penggunaan boraks dan formalin sebagai pengawet 
makanan dan minuman. Pola hidup yang ada di masyarakat sekarang ini, 
menyebabkan tubuh rentan terhadap penyakit. Selain itu, penyakit juga bisa 
muncul karena pengaruh lingkungan, faktor keturunan (generatif), kurangnya atau 
kelebihan asupan gizi, gaya hidup yang tidak aktif (kurangnya berolahraga) dan 
stress.  
Salah satu penyakit yang dapat ditimbulkan dari faktor-faktor lingkungan, 
faktor keturunan (generatif), kurangnya atau kelebihan asupan gizi, gaya hidup 
yang tidak aktif (kurang berolahraga) dan stress adalah bersumber pada 
penyempitan pembuluh darah (kardiovaskuler). Penyempitan pembuluh darah ini 
timbul karena berbagai faktor risiko yang salah satunya adalah hipertensi. 
Penyakit hipertensi ini, mungkin belum diketahui oleh banyak kalangan sebagai 
penyakit berbahaya. Masyarakat awam lebih paham jika disebut penyakit darah 
tinggi. Selain itu, belum banyak pula yang paham, bahwa hipertensi tergolong 
penyakit pembunuh diam-diam (silent killer). Tergolong demikian karena 
penderita hipertensi merasa tubuhnya sehat dan tidak memiliki keluhan yang 
berarti, sehingga penderita hipertensi cenderung menganggap enteng penyakit 
hipertensi tersebut. Hipertensi pada dasarnya mengurangi harapan hidup para 
penderitanya, penyakit ini menjadi muara beragam penyakit degeneratif yang 
dapat mengakibatkan kematian (www.suarakarya-online.com).   
Hasil penelitian menunjukkan jumlah penderita hipertensi semakin 
meningkat, berdasarkan data WHO dari 50% penderita hipertensi yang diketahui 
hanya 25% yang mendapat pengobatan, dan hanya 12,5% yang diobati dengan 
baik, Berdasarkan Survei Kesehatan Rumah Tangga (SKRT) tahun 2001, 
kematian akibat penyakit jantung dan pembuluh darah di Indonesia sebesar 
26,3%. Sedangkan data Indonesian Society of Hypertention (InaSH), secara 
umum prevalensi hipertensi pada orang dewasa antara 15% (www.suarakarya-
online.com).  Di Indonesia banyaknya penderita hipertensi diperkirakan 15 juta 
orang tetapi hanya 4% yang merupakan hipertensi terkontrol. Penelitian 
menunjukkan, hanya 50% penderita hipertensi yang dapat terdeteksi, dari jumlah 
tersebut hanya 50% yang berobat secara teratur dan hanya setengahnya yang 
terkontrol dengan baik. Artinya, dari seluruh penderita hipertensi di Indonesia 
yang terkontrol dengan baik jumlahnya di bawah 10% (Kompas, 8 September 
2003). Penyakit ini telah menjadi masalah utama dalam kesehatan masyarakat 
yang ada di Indonesia maupun di beberapa negara yang ada di dunia. 
Diperkirakan sekitar 80% kenaikan kasus hipertensi terutama di negara 
berkembang tahun 2025 dari sejumlah 639 juta kasus di tahun 2000, diperkirakan 
menjadi 1,15 milyar kasus di tahun 2025. Prediksi ini didasarkan pada angka 
penderita hipertensi saat ini dan pertambahan penduduk saat ini. 
(ridwanamiruddin.wordpress.com) 
Penyakit hipertensi ini berpotensi untuk menimbulkan stroke, gagal 
jantung dan serangan jantung. Berdasarkan hasil wawancara dengan 5 penderita 
hipertensi, terdapat pula akibat fisik dan psikis yang ditimbulkan oleh hipertensi 
seperti tubuh terasa mudah lelah, sulit untuk berkonsentrasi, kurang dapat 
mengendalikan emosi dan timbul rasa cemas dalam diri.  
Penyakit hipertensi ini semakin rentan terjadi pada dewasa madya. Masa 
dewasa madya dikarakteristikan dengan penurunan umum kebugaran fisik dan 
penurunan kesehatan. Masalah  kesehatan utama pada usia dewasa madya adalah 
penyakit kardiovaskuler. Jantung di usia 40 tahun memompa hanya 23 liter darah 
per menit, sehingga terjadi penyempitan pembuluh koroner pada saat jantung 
memompa darah. Hal ini mengakibatkan darah pada setiap denyut jantung dipaksa 
untuk melalui pembuluh yang lebih sempit daripada biasanya dan menyebabkan 
naiknya tekanan darah (dalam Santrock 2004:141). 
Dengan semakin banyak orang yang mencapai dewasa madya, maka beban 
komplikasi semakin meningkat, demikian pula biaya yang harus dikeluarkan oleh 
masyarakat akan meningkat karena biaya di dalam penanganan komplikasi-
komplikasi hipertensi mencapai beberapa kali lebih besar dibandingkan hipertensi 
tanpa komplikasi (www.balipost.com). Selain itu usia dewasa madya merupakan 
usia produktif, jika penyakit hipertensi ini dibiarkan, maka akan berakibat pada 
menurunnya tingkat produktivitas.  Maka dari itu, hipertensi membutuhkan 
perhatian, penanganan serta tindakan komprehensif dan perlu dicegah juga 
diobati. Salah satu caranya adalah dengan mengubah pola makan menjadi pola 
makan sehat dengan berpedoman pada aneka ragam makanan yang memenuhi gizi 
Penderita hipertensi dewasa madya disarankan untuk melaksanakan diet 
rendah garam, mengkonsumsi obat-obatan secara teratur dan berolahraga secara 
rutin. Penderita hipertensi dewasa madya diupayakan untuk melaksanakan hal 
tersebut secara rutin untuk menghindari perubahan kondisi tubuh yang semakin 
memburuk, seperti serangan jantung, stroke atau gagal ginjal dan untuk 
mengurangi mortalitas kardiovaskuler. Selain itu, hipertensi adalah penyakit 
seumur hidup maka dari itu penanganan penyakit ini harus dilaksanakan secara 
teratur dan disiplin oleh penderita.  
Berdasarkan wawancara dengan delapan orang penderita hipertensi 
dewasa madya, terdapat berbagai kesulitan dalam menanggulangi hipertensi ini, 
terkadang penderita hipertensi dewasa madya tergoda oleh makanan-makanan 
enak dan malas untuk melakukan olahraga secara rutin, sehingga lupa akan 
kewajibannya untuk menanggulangi hipertensi. Penanggulangan hipertensi yang 
diupayakan penderita hipertensi dewasa madya ini berkaitan dengan bagaimana 
penderita mengatur dirinya untuk menanggulangi hipertensi secara rutin. Untuk 
menanggulangi hipertensi, penderita hipertensi dewasa madya perlu untuk 
menentukan perencanaan, melaksanakan dan mengevaluasi kesehatannya. 
Di dalam Self-Regulation terdapat juga fase yang sama dengan proses 
penanggulangan hipertensi di atas. Fase pertama adalah forethought, yaitu goal 
yang dipilih oleh penderita hipertensi dewasa madya untuk diet rendah garam 
dengan tidak mengonsumsi asupan garam berlebih dan bagaimana penderita 
hipertensi dewasa madya tersebut merencanakan strategi-strategi untuk dapat 
mencapai tubuh yang sehat, misalnya penderita hipertensi dewasa madya 
merencanakan untuk membuat suatu catatan yang dapat mengingatkannya agar 
tidak mengkonsumsi garam berlebih. Selanjutnya, fase yang kedua adalah  
performance atau volitional control yaitu tindakan-tindakan yang dilakukan 
penderita hipertensi dewasa madya sesuai dengan apa yang telah direncanakannya 
pada tahap pertama, misalnya tidak mengonsumsi makanan yang mengandung 
garam berlebih. Fase selanjutnya adalah self-reflection (refleksi diri), penderita 
hipertensi dewasa madya mengevaluasi apakah setelah  melakukan serangkaian 
tindakan, seperti memeriksakan diri ke laboratorium, goal penderita hipertensi 
dewasa madya tersebut  tercapai atau tidak, apakah penderita hipertensi dewasa 
madya merasa puas dengan apa yang telah dicapainya atau tidak. Apabila 
penderita hipertensi dewasa madya tidak puas dengan hasil evaluasinya, maka hal 
ini akan mempengaruhi fase forethought di masa yang akan datang, apakah 
penderita  hipertensi dewasa madya ini akan mengubah goal berikutnya atau 
mengubah strategi dalam pencapaian goal-nya tersebut.    
Setiap penderita hipertensi dewasa madya memiliki kemampuan 
meregulasikan diri yang berbeda-beda. Berdasarkan wawancara terhadap lima 
orang yang memiliki kandungan hipertensi dalam tubuh, 80% tidak memiliki 
perencanaan untuk tidak mengonsumsi makanan yang mengandung garam 
berlebih, mengkonsumsi obat-obatan secara rutin dan untuk berolahraga secara 
rutin, namun penderita hipertensi dewasa madya tersebut melakukan pemeriksaan 
rutin, 20% memiliki perencanaan untuk tidak mengkonsumsi makanan yang 
mengandung garam berlebih serta merencanakan untuk berolahraga secara rutin, 
mengonsumsi obat-obatan secara rutin dan melakukan pemeriksaan rutin. 
Peneliti tertarik untuk membahas masalah ini karena dalam melaksanakan 
diet hipertensi ini diperlukan regulasi diri yang baik, mengingat terdapat beberapa 
pantangan yang harus dilaksanakan oleh penderita hipertensi dewasa madya. 
Penderita hipertensi dewasa madya ini harus melakukan perencanaan dalam 
melaksanakan dietnya dan pada akhirnya mengevaluasi apa yang telah 
dilaksanakannya. Regulasi diri dalam melaksanakan diet hipertensi ini memiliki 
tujuan  untuk menunjang kesehatan dan untuk dapat terhindar dari berbagai 
macam penyakit dan juga risiko kematian yang semakin meningkat.  
Melalui penelitian ini peneliti ingin mengetahui bagaimana gambaran self-
regulation pada penderita hipertensi dewasa madya di Rumah Sakit X  Bandung. 
Penelitian ini dilakukan untuk memperoleh suatu gambaran mengenai 
derajat self-regulation pada penderita hipertensi dewasa madya di Rumah Sakit 
 X  Bandung. 
Penelitian ini bertujuan untuk mengetahui derajat Self-Regulation dan 
aspek-aspek dari Self-Regulation pada penderita hipertensi dewasa madya yang di 
Rumah Sakit  X  Bandung. 
  Memberikan informasi bagi ilmu Psikologi Kesehatan mengenai self- 
regulation hipertensi dewasa madya di Rumah Sakit  X  Bandung. 
  Memberikan informasi bagi yang ingin meneliti lebih lanjut mengenai 
self-regulation pada penderita hipertensi dewasa madya. 
  Memberikan informasi kepada para penderita hipertensi dewasa madya 
mengenai self-regulation, agar dapat menaggulangi hipertensi dengan 
memperhatikan self-regulation. 
  Memberikan informasi kepada keluarga penderita hipertensi dewasa 
madya mengenai derajat self- regulation dalam menaggulangi 
hipertensi, agar dapat mengoptimalkan penaggulangan tersebut dengan 
memperhatikan self-regulation.  
  Memberikan informasi kepada dokter di Rumah Sakit  X , Bandung 
mengenai derajat self-regulation dalam menanggulangi hipertensi pada 
dewasa madya. 
Hipertensi adalah kondisi medis dengan terjadinya peningkatan tekanan 
darah secara kronis (dalam jangka waktu lama). Penderita yang mempunyai 
sekurang-kurangnya tiga bacaan tekanan darah yang melebihi 140/90 mmHg saat 
istirahat diperkirakan mempunyai keadaan darah tinggi. Tekanan darah yang 
selalu tinggi adalah salah satu faktor risiko untuk stroke, serangan jantung, gagal 
jantung dan merupakan penyebab utama gagal jantung kronis (id.wikipedia.org). 
Selain itu, berdasarkan data wawancara penderita hipertensi dewasa madya tidak 
dapat beraktivitas secara optimal, konsentrasi mudah terganggu, emosi menjadi 
sulit terkontrol, mudah cemas, bahkan sampai tidak dapat beraktivitas sama sekali.  
Penyakit hipertensi ini semakin rentan terjadi pada dewasa madya. Masa 
dewasa madya ini dikarakteristikkan dengan penurunan umum kebugaran fisik 
dan penurunan kesehatan. Jantung di usia 40 tahun memompa hanya 23 liter darah 
per menit, sehingga terjadi penyempitan pembuluh koroner pada saat jantung 
memompa darah. Hal ini mengakibatkan darah pada setiap denyut jantung dipaksa 
untuk melalui pembuluh yang lebih sempit daripada biasanya dan menyebabkan 
naiknya tekanan darah (dalam Santrock 2004:141). 
Oleh karena itu, selain pemberian obat-obatan anti hipertensi, penderita 
hipertensi dewasa madya perlu mengubah gaya hidup, yang salah satu prosesnya 
adalah melaksanakan diet. Tujuan dari penatalaksanaan diet adalah untuk 
membantu menurunkan tekanan darah dan mempertahankan tekanan darah 
menuju normal. Prinsip diet pada penderita hipertensi dewasa madya adalah 
makanan beraneka ragam dan gizi seimbang, jenis dan komposisi makanan 
disesuaikan dengan kondisi penderita, jumlah garam dibatasi sesuai dengan 
kesehatan penderita dan berolahraga (id.wikipedia.org). 
Dalam menanggulangi hipertensi penderita hipertensi dewasa madya perlu 
mengatur diri untuk mencapai tujuannya, yang disebut dengan Self-Regulation. 
Self-Regulation mengacu pada pemikiran diri yang terus berkembang, tindakan 
dan perasaan yang berasal dari dalam diri yang telah dirancang serta secara 
berulang diadaptasi untuk mencapai personal goal, yaitu untuk menanggulangi 
hipertensi. Self-Regulation dideskripsikan sebagai sebuah siklus, karena feedback 
dari tingkah laku sebelumnya digunakan untuk membuat penyesuaian tindakan 
yang dilakukan pada saat ini. (dalam Boekarts, 2000 : 14) 
Self-regulation memiliki tiga fase dalam pelaksanaannya. Fase pertama 
meliputi  forethought, yaitu fase perencanaan yang di dalamnya meliputi task 
analysis dan self-motivation beliefs. Task analysis adalah tahap penderita 
hipertensi dewasa madya dalam merencanakan dan menganalisa penanggulangan 
hipertensi yang harus dilakukannya. Self-motivation beliefs adalah motivasi yang 
ada dalam diri penderita hipertensi dewasa madya yang harus dimunculkan, jika 
ingin memperoleh hasil yang tinggi. 

