SMA (Sekolah Menengah Atas) merupakan lanjutan dari SMP (Sekolah Menengah 
Pertama) dimana siswa memperoleh ilmu pengetahuan secara umum. SMAK di Indonesia 
mengadakan program pemilihan jurusan bagi kelas X, untuk menganalisa jurusan yang tepat 
bagi siswanya di kelas XI kemudian diteruskan pada kelas XII nanti. Pemilihan jurusan 
biasanya didahului dengan tes minat dan tes psikologi. Hal tersebut bertujuan untuk 
memberikan gambaran yang lebih jelas mengenai potensi yang dimiliki siswa, agar dapat di 
maksimalkan oleh siswa yang bersangkutan. Biasanya ada tiga pilihan dalam pemilihan minat 
jurusan, yaitu IPA, IPS, dan Bahasa.Implementasi Kurikulum 2013 terus dievaluasi. 
Diantaranya adalah sistem peminatan untuk siswa SMAK. Kepala Unit Implementasi 
Kurikulum (UIK) Kementerian Pendidikan dan Kebudayaan (Kemendikbud) Tjipto Sumadi 
menerima laporan, 90 persen siswa SMAK meminati jurusan IPA. (diunduh dari  
http://www.jpnn.com/read/2014/03/15/222110/90-Persen-Siswa-Meminati-IPA-) 
Seiring waktu berjalan, masyarakat dan pelajar mulai mengkotak-kotakkan jurusan-
jurusan tersebut. Masyarakat dan pelajar mulai menganalogikan kalau jurusan IPA merupakan 
jurusan yang memiliki peluang untuk bekerja tinggi sedangkan IPS tidak. Lalu ada anggapan 
kalau jurusan IPS cuma jurusan untuk  buangan  siswa-siswa yang tidak diterima di jurusan 
IPA. Dengan munculnya sugesti seperti itu, masyarakat awam mulai terpengaruh, sehingga 
jurusan IPS semakin dihindari dan dianggap tidak prospektif. Sedangkan jurusan IPA dijadikan 
suatu terget  wajib  bagi siswa SMAK. 
Diunduh dari http://justice-for-education.blogspot.co.id/2011/03/argumentasi-dilema-jurusan-
ipa-dan-ips.html) 
Siswa kelas XII SMAKK berusia antara 16-18 tahun dan menurut Santrock (2007) 
memasuki tahap perkembangan remaja late adolescence. Santrock (2007)  mengungkapkan 
bahwa eksplorasi minat dalam memilih jurusan dan karir pada remaja akan lebih nyata pada 
tahap ini. Piaget (dalam Santrock, 2007) juga mengungkapkan bahwa masa remaja memasuki 
tahap perkembangan kognitif formal operational dengan ciri-ciri mampu berpikir fantasy fight 
untuk melihat kemungkinan ke masa depan. 
Melihat kemungkinan ke masa depan berarti siswa memiliki orientasi masa depan. 
Dengan adanya orientasi masa depan berarti siswa telah melakukan antisipasi terhadap 
kejadian-kejadian yang mungkin timbul di masa depan (Nurmi, 1989). Kegiatan setelah lulus 
SMAK yang dapat dilakukan oleh siswa kelas XII salah satunya adalah menentukan apakah 
dirinya akan masuk ke perguruan tinggi setelah lulus, perguruan tinggi apa yang akan dipilih, 
jurusan apa yang akan dijalani. Hal ini disebut dengan orientasi masa depan bidang pendidikan. 
Orientasi masa depan adalah cara pandang seseorang terhadap masa depannya. Jelas atau tidak 
jelasnya individu memandang masa depannya, akan tergambar melalui harapan-harapan, 
tujuan standar, perencanaan dan strategi (Nurmi, 1989). Orientasi masa depandalam 
pendidikan merupakan suatu proses yang akan mencakup tiga tahapan, yaitu motivasi, 
perencanaan, dan evaluasi. 
Motivasi meliputi motif, minat, dan harapan siswa yang berkaitan dengan masa depannya 
dalam bidang pendidikan. Minat yang dimiliki siswa akan mengarahkan dirinya dalam 
menentukan tujuan pendidikan yang ingin dicapai pada masa yang akan datang. Perencanaan 
adalah proses yang terdiri dari penentuan sub tujuan, penyususan rencana dan perwujudan 
rencana sesuai dengan tujuan pendidikan yang telah ditentukan. Pada tahap evaluasi, siswa 
menilai sejauh mana tujuan pendidikan yang telah ditetapkan dan rencana yang telah disusun 
dapat direalisasikan. 
Siswa kelas XII SMAK perlu memiliki orientasi masa depan dalam bidang pendidikan 
yang jelas karena siswa akan lebih termotivasi untuk belajar agar memperoleh nilai yang baik 
dan berusaha mewujudkan tujuan-tujuan yang realistik di masa depan. Setelah lulus, siswa 
dapat langsung mendaftarkan diri pada perguruan tinggi tertentu sesuai dengan jurusan yang 
diinginkan. Siswa diharapkan dapat mencapai tujuan mereka dan sukses di masa depan serta 
dapat bertahan menjalani kulian di jurusan yang diinginkan ketika menghadapi kesulitan-
kesulitan selama kuliah. Sedangkan, bagi siswa yang memiliki orientasi masa depan dalam 
bidang pendidikan yang tidak jelas menjadi kurang termotivasi untuk belajar dan mewujudkan 
tujuan di masa depan. Siswa juga akan mengalami kebingungan untuk menentukan apa yang 
akan mereka lakukan setelah lulus. Siswa akan menyerah dalam menghadapi kesulitan di 
perguruan tinggi sehingga membuat siswa tidak bertahan lama dalam menjalani kuliah di 
jurusan yang dipilih. 
SMAK  X  merupakan salah satu SMAK  swasta yang ada di Bandung dan berakreditasi 
A. Dari hasil wawancara dengan guru Bimbingan Konseling, beliau mengungkapkan bahwa 
sebagian besar siswa kelas XII IPS SMAK  X  Bandung belum menentukan perguruan tinggi 
dan jurusan yang diinginkan. Berdasarkan hal tersebut, SMAK  X  bandung berusaha untuk 
membantu siswa yang ingin melanjutkan pendidikan dengan terbuka perguruan-perguruan 
tinggi datang ke sekolah dan mempromosikan diri kepada siswa dengan cara melakukan 
kegiatan presentasi di kelas-kelas atau membagikan brosur perguruan tinggi. 
Berdasarkan hasil wawancara yang dilakukan dari 10 siswa dari 73 siswa kelas XII IPS 
tersebut, terdapat 3 orang siswa (30%) yang telah mencari informasi mengenai jurusan 
perkuliahan, persyaratan dan informasi yang berkaitan dengan Perguruan Tinggi yang 
diinginkan melalui orangtua, teman, guru ataupun mencari informasi melalui media cetak dan 
internet. Siswa tersebut juga telah menyusun langkah-langkah yang akan mereka tempuh agar 
dapat diterima di jurusan perkuliahan dan Perguruan Tinggi yang mereka inginkan, misalnya 
dengan menentukan jurusan perkuliahan lain untuk dijadikan cadangan. Sedangkan 7 orang 
siswa (70%)yang masih belum menentukan jurusan perguruan tinggi yang diinginkannya, 5 
siswa  diantaranya belum memiliki rencana terarah pada tujuannya masuk ke perguruan tinggi, 
mereka ingin mendaftarkan dirinya ke beberapa jurusan di Perguruan Tinggi negeri maupun 
swasta di Bandung, Jakarta, Yogyakarta, dsb. Sedangkan 2 orang siswa lebih menyerahkan 
urusan perkuliahannya kepada orangtua sehingga siswa tersebut menjadi pasif dalam dalam hal 
eksplorasi. Sebanyak 2 dari 10 siswa (20%) telah melakukan penilaian terhadap kemampuan 
diri mereka melalui prestasinya di sekolah. Sedangkan 8 dari 10 siswa (80%) merasa tidak 
yakin akan minat, bakat, dan kemampuan dirinya, mereka memiliki harapan yang rendah dalam 
mencapai Perguruan Tinggi yang diinginkannya. 
Dari data yang diperoleh bahwa siswa kelas XII IPS belum memiliki gambaran yang 
jelas mengenai orientasi masa depan bidang pendidikan. Siswa yang memiliki orientasi masa 
depan di bidang pendidikan yang tidak jelas masih mengalami kebingungan dalam 
memutuskan untuk melanjutkan pendidikan di perguruan tinggi dan dalam menentukan jurusan 
di perguruan tinggi yang akan dijalaninya. Siswa memiliki perencanaan dan strategi yang tidak 
terarah untuk mencapai tujuannya serta tidak akurat dalam mengevaluasi kemungkinan 
pencapaian tujuan dan rencana-rencana yang telah dibuatnya. Siswa yang memiliki orientasi 
masa depan di bidang pendidikan yang jelas akan memutuskan untuk melanjutkan pendidikan 
ke jenjang yang lebih tinggi dan menentukan fakultas atau jurusan perguruan tinggi yang sesuai 
dengan minatnya. Siswa juga memiliki perencanaan dan strategi yang terarah untuk mencapai 
tujuannya tersebut serta dapat mengevaluasi secara akurat tujuan dan rencana-rencana yang 
telah dibuat dengan melihat faktor-faktor yang menghambat dan menunjang pencapaian tujuan. 
Siswa yang memiliki orientasi masa depan yang jelas akan gigih dalam  usaha dan 
konsisten dalam kepentingan cenderung lebih mengevaluasi kinerja akademik jangka pendek 
dalam kaitannya dengan pencapaian jangka panjang (Barber et al, 2009). Dengan orientasi 
yang jelas maka siswa akan lebih tekun dan semangat dalam mencapai tujuannya. Ketekunan 
dibutuhkan agar siswa dapat menghadapi hambatan dan rintangan yang dapat menghalangi 
siswa kepada tujuannya. Beberapa hambatan yang dihadapi adalah  tuntutan-tuntutan yang 
ditujukan kepada siswa dimana siswa dituntut untuk dapat menyerap materi lebih cepat, lebih 
aktif mencari materi, aktif bertanya dan berdiskusi. Selain ketekunan, siswa juga diharapkan 
untuk dapat tetep konsisten dan fokus pada tujuan dan pilihan mereka yaitu lulus dari sekolah 
menengah atas serta bersemangat dalam menjalani apapun kesulitan yang mereka hadapi serta 
dapat membuahkan hasil yang terbaik yang dapat terlihat dari hasil kelulusan.  Ketekunan dan 
konsistensi dalam minat mereka, diistilahkan oleh Duckworth sebagai Grit. 
Grit adalah ketekunan (perseverance) dan semangat (passion) untuk tujuan jangka 
panjang. Grit melibatkan bekerja dengan keras menghadapi tantangan, mempertahankan usaha 
dan minat bertahun-tahun meskipun ada kegagalan, kesulitan, dan keadaan tanpa kemajuan 
(plateaus) dalam proses pencapaian tujuan jangka panjang tersebut (Duckworth, 2007). 
Grit termasuk ke dalam kelompok trait personality. Grit menurut Angela Lee Duckworth 
(2007) adalah kecenderungan untuk mempertahankan ketekunan dan semangat untuk tujuan 
jangka panjang yang menantang, dimana orang-orang bertahan dengan hal-hal yang menjadi 
tujuan mereka dalam jangka waktu yang sangat panjang sampai mereka menguasai hal-hal 
tersebut. Didalam Grit terdapat dua hal penting, yakni konsistensi minat dan ketekunan usaha. 
Konsistensi minat diartikan sebagai seberapa konsisten usaha seseorang untuk menuju suatu 
arah, dan ketekunan usaha adalah seberapa keras seseorang berusaha untuk mencapai tujuan. 
Di dalam ketekunan terdapat energi yang menggerakkan seseorang. 
Dalam menjalani proses belajar, siswa kelas XII IPS memiliki tujuan agar dapat lulus dari 
jenjang pendidikan menengah atas. Sebelum siswa kelas XII IPS memutuskan untuk masuk 
jurusan IPS mereka memiliki minat yang berbeda-beda. Namun ketika mereka memutuskan 
untuk jurusan IPS, minat mereka terfokus pada bidang/jurusan perkuliahan yang berkaitan 
dengan IPS. Grit pada penelitian ini menyoroti apakah terjadi perubahan minat pada siswa 
kelas XII IPS setelah menjalani proses belajar dan bagaimana usaha yang dikerahkan dalam 
menjalaninya. Salah satu wujud dari Grit yang dapat terlihat pada siswa kelas XII IPS adalah 
rasa ingin tahu yang tak kunjung habis. Hal ini sejalan dengen metode belajar yang menuntut 
mahasiswa untuk aktif dalam mencari materi (Student Centered Learning). 
Hasil penelitian menunjukkan bahwa individu yang berorientasi masa depan lebih 
Grittier daripada rekan-rekan mereka yang tidak berorientasi masa depan. Siswa yang gigih 
dalam usaha dan konsisten dalam kepentingan cenderung lebih baik mengevaluasi kinerja 
akademik jangka pendek dalam kaitannya dengan pencapaian jangka panjang (Barber et al., 
2009). Siswa kelas XII IPS yang mempunyai orientasi masa depan diharapkan mempunyai Grit 
yang tinggi daripada siswa yang tidak mempunyai orientasi masa depan. 
Dari data yang didapat dari guru BP pada tahun ajaran 2014-2015 sebanyak 90% siswa 
kelas XII IPS langsung melanjutkan ke jenjang perguruan tinggi, 15% siswa mengambil cuti 
1tahun untuk mengambil program bahasa atau menyalurkan minatnya yang lain seperti 
mengambil kursus modelling, memasak, tatarias, dan lainnya. Sebanyak 5% siswa masih 
belum mengetahui minatnya sehingga memerlukan waktu untuk mencari tahu minat mereka. 
Selain itu, dari hasil wawancara dengan 10 orang siswa alasan mereka memilih 
penjurusan IPS pada sekolah menengah, sbengah 6 orang siswa mengatakan karena IPS 6 orang 
siswa (60%) mengatakan bahwa IPS lebih mudah daripada IPA, 2 orang siswa (20%) 
mengatakan memang menyukai pelajaran IPS, serta 2 orang siswa (20%) mengatakan karena 
tidak bisa masuk IPA.  
Dari hasil survey awal kepada 10 orang siswa kelas XII IPS juga didapati 5 orang siswa 
(50%) yang akan melanjutkan jurusan di bidang IPS seperti akuntansi, manajemen, bisnis, dan 
lainnya tetapi mereka belum menentukan perguruan tinggi yang akan diambil. 5 orang siswa 
ini berusaha mencari tahu dan membandingkan perguruan tinggi dengan jurusan yang mereka 
inginkan, serta mereka lebih mendalami pelajaran IPS dan berusaha untuk menaikkan nilai-
nilai mereka. Sementara 3 orang siswa kelas XII IPS mengaku tidak begitu berminat dengan 
jurusan IPS tetapi mereka mengikuti perintah orangtua untuk mengembil jurusan IPS sehingga 
mereka kurang berusaha untuk mencari tahu dan belajar lebih giat serta tidak berusaha untuk 
menaikkan nilainya. 2 orang siswa kelas XII IPS masih belum memutuskan. 
Berdasarkan uraian yang telah dijelaskan, hal tersebut menarik perhatian peneliti untuk 
melakukan penelitian mengenai hubungan antara orientasi masa depan dan Grit bidang 
pendidikan pada siswa kelas XII SMAK  X  Bandung. 
Dari penelitian ini ingin diketahui bagaimana hubungan antara Orientasi Masa Depan dan 
Grit dalam bidang pendidikan pada siswa kelas XII IPS SMAK  X  Bandung. 
Maksud dari penelitian  ini adalah untuk memperoleh gambarantentang hubungan 
mengenai Orientasi Masa Depan bidang pendidikan dan Grit pada siswa kelas XII IPS SMAK 
 X  Bandung. 
Tujuan penelitian ini adalah untuk mengetahuibagaimana hubungan antara Orientasi 
Masa Depan dan Grit dalam bidang pendidikan pada siswa kelas XII IPS SMAK  X  Bandung. 
1. Memberikan informasi mengenai hubungan orientasi masa depan bidang 
pendidikan dan Grit ke dalam ilmu psikologi, khususnya Psikologi 
2. Sebagai masukan bagi penelitian lain yang ingin meneliti orientasi masa depan, 
terutama orientasi masa depan dalam bidang pendidikan 
3. Memberi masukan bagi peneliti yang berminat melakukan penelitian lanjutan 
1. Memberikan informasi kepada para siswa kelas XII IPS SMAK  X  Bandung 
mengenai hubungan orientasi masa depan dan Grit dalam bidang pendidikan 
sehingga membantu siswa dalam menentukan arah terkait masa depan dalam 
bidang pendidikan di Perguruan Tinggi. 
2. Memberikan informasi kepada guru BP atau kepala sekolah mengenai orientasi 
masa depan dan Grit dalam bidang pendidikan para siswa sehingga dapat menjadi 
bahan pertimbangan dalam membimbing para siswa untukmenentukan masa 
depan dalam bidang pendidikan di perguruan tinggi dan untuk mengembangkan 
dan meningkatkan Grit. 
3. Memberikan informasi kepada orangtua siswa mengenai orientasi masa depan 
dalam bidang pendidikan siswa sehingga dapat menjadi pertimbangan dalam 
berdiskusi dan membimbing siswa dalam merencanakan masa depan, terutama 
dalam bidang pendidikan di perguruan tinggi. 
Siswa kelas XII IPS SMAK  X  Bandung adalah siswa yang berusia 16 sampai 17 
tahun. Berdasarkan usia tersebut,menurut Santrock (2007) siswa telah memasuki masa 
perkembangan remaja akhir. Pada masa remaja akhir minat terhadap pendidikan dan eksplorasi 
identitas lebih nyata dibandingkan pada masa remaja awal. Sejalan dengan perkembangannya, 
berkembang pula kematangan kognitifnya, pada tahap ini remaja telah memasuki tahap 
berpikir formal operational (piaget, 1971 dalam Mussen, 1984). Pada tahap ini remaja dapat 
menggunakan variasi yang lebih luas untuk strategi pemecahan masalah, fleksibilitas dalam 
berpikir dan bernalar serata dapat melihat segala sesuatu dari sejumlah sudut pandang. Selain 
itu, pada tahap ini memungkinkan remaja untuk melakukan antisipasi terhadap kejadian atau 
peristiwa di masa depan dan untuk berpikir tentang konsekuensi di masa mendatang. Tahap ini 
pula membuat remaja memiliki orientasi masa depan. Artinya, remaja telagh mampu membuat 
skema kognitif guna mengarahkannya dalam konteks aktifitas masa depan serta hasil-hasil 
yang akan datang (Nurmi, 1989) 
Berdasarkan di atas, maka dapat dikatakan bahwa remaja yang sejak awal telah mampu 
menetapkan tujuan dan membuat persiapan dan perencanaan dalam bidang pendidikan, 
menunjukkan bahwa mereka cenderung memiliki orientasi masa depan yang jelas. 

