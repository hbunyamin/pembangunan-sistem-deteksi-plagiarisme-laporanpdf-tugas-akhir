Pendidikan adalah usaha sadar dan terencana untuk mewujudkan suasana belajar dan 
proses pembelajaran peserta didik yang dapat mengembangkan potensi dirinya untuk memiliki 
kekuatan spiritual, pengendalian diri, kepribadian, kecerdasan, akhlak mulia, serta keterampilan 
yang diperlukan dirinya, masyarakat, bangsa dan negara (UU RI No. 20 Tahun 2003 tentang 
DirJend Pendidikan Nasional Pasal 1 Ayat 1). Pendidikan formal sudah dimulai dari usia 6 
tahun di Sekolah Dasar hingga usia dewasa yaitu Perguruan Tinggi. Tujuan pendidikan secara 
umum menyediakan lingkungan yang memungkinkan peserta didik agar dapat 
mengembangkan bakat dan kemampuan yang dimiliki secara optimal sehingga dapat menjadi 
dirinya dan berfungsi seutuhnya sesuai dengan kebutuhan pribadi dan kebutuhan masyarakat. 
Pendidikan di perguruan tinggi merupakan puncaknya pendidikan formal. Pendidikan 
adalah salah satu dari Tri Dharma Perguruan Tinggi yang dilaksanakan dengan menjadi 
mahasiswa. Kini, mahasiswa harus menghadapi semakin banyaknya tuntutan. Mulai dari 
adanya Peraturan Menteri Pendidikan Kebudayaan No. 49 Tahun 2014 tentang Standar 
Nasional Pendidikan Tinggi yang berisi pembatasan masa studi pada jenjang pendidikan tinggi 
tingkat diploma 4 (D-4) dan sarjana dari 7 tahun menjadi 4-5 tahun (Pikiran Rakyat, 11 
September 2014). Setelah lulus, mahasiswa harus memikirkan akan meneruskan pendidikan 
atau bekerja dimana. Berdasarkan data Dinas Tenaga Kerja Kota Bandung, hingga akhir Juli 
2014 ada 42.093 pencari kerja yang belum tersalurkan. Seorang pelaksana Disnaker kota 
Bandung, Muslihat (2014) mengatakan bahwa kebanyakan pencari kerja itu berpendidikan 
sarjana.  
Berbagai macam tuntutan tersebut dirasakan oleh sebagian besar mahasiswa di 
Indonesia. Salah satunya bagi mahasiswa Fakultas Psikologi di salah satu universitas. Mulai 
tahun 2013, telah diterapkan Kurikulum Berbasis Kompetensi (KKNI), yang menekankan pada 
pendekatan student centered dimana mahasiswa dituntut untuk lebih mandiri, aktif dalam 
proses belajar mengajar. Kurikulum ini  menerapkan kolaborasi, suport dan kerjasama yang 
tidak hanya mengukur hard skill mahasiswa tetapi juga soft skill mahasiswa. Mahasiswa 
diharapkan tidak hanya memiliki kemampuan menjelaskan (misalnya: pengantar 
perkembangan remaja, perubahan-perubahan fundamental pada masa remaja, konteks remaja 
dan perkembangan psikososial pada masa remaja) secara tertulis maupun lisan, tetapi juga 
memiliki kemampuan akhir dapat berkomunikasi efektif baik secara lisan maupun tertulis, 
dapat bekerjasama dan bekerja keras. Hal ini diberikan dalam bentuk pembelajaran seperti 
cooperative learning, contextual instruction, menuliskan tugas/karya tulis/slide, 
mempresentasikan, berpendapat, menyiapkan tugas kelompok, berdiskusi dan antusiasme baik 
mencari sumber  sumber bacaan tugas maupun menjadi penyaji yang komunikatif dan apik. 
Maka dari itu, mahasiswa tidak hanya berelasi dengan dosen maupun asisten dosen, namun juga 
lebih banyak berelasi dengan teman sebayanya. Berdasarkan penuturan salah satu mahasiswa 
angkatan 2013, waktu perkuliahan yang lebih panjang, dimulai dari pukul 9.00 hingga pukul 
15.00. Selain itu setiap harinya, mahasiswa mendapat banyak tugas kelompok maupun individu 
yang tidak jarang harus diselesaikan pada hari yang sama. Setiap mahasiswa diharapkan 
mencapai nilai minimal B pada akhir modul untuk dinyatakan lulus dalam satu Mata Kuliah.  
Hal ini dapat menjadi kesulitan tersendiri bagi mahasiswa. Cara menghadapi berbagai 
tuntutan dan situasi ini bergantung pada penilaian diri mahasiswa terhadap situasi tersebut. 
Mahasiswa dapat menilai apakah dirinya mampu atau tidak, berharga atau tidak dan dapat 
diterima atau tidak. Penilaian mahasiswa terhadap diri sendiri ini oleh Coopersmith (1967) 
dinamakan self-esteem. Self-esteem adalah evaluasi atau penilaian diri individu yang tampak 
dalam sikap penerimaan atau penolakan seseorang mengenai sejauh mana menganggap dirinya 
mampu, berarti, berhasil dan berharga. 
Self-Esteem merupakan salah satu aspek psikologis yang penting dan harus 
dikembangkan melalui pendidikan untuk keberhasilan masa depan para peserta didik. 
Mahasiswa yang memiliki self-esteem tinggi akan relatif merasa optimis, memiliki 
penyelesaian masalah yang tepat, dapat menyesuaikan diri baik dalam perkuliahan maupun 
relasi sosial. Contohnya pada Raeni, mahasiswi Universitas Negeri Semarang yang menjadi 
wisudawati terbaik dengan IPK 3,96. Ia menunjukkan tekadnya untuk menikmati masa depan 
yang lebih baik meskipun ayahnya hanya seorang tukang becak (Yulianingsih, 2014). 
Sebaliknya, mahasiswa yang memiliki self-esteem rendah akan relatif merasa cemas, tertekan, 
pesimis tentang diri dan masa depan, mudah merasa gagal, kurang dapat menyelesaikan 
masalah, kurang dapat menyesuaikan diri dalam perkuliahan maupun relasi dengan sebaya dan 
kurang kompeten dalam menjalankan tugas dan perannya sebagai mahasiswa. Contohnya yang 
terjadi pada Indrawan, yang merasa tertekan tidak juga menyelesaikan skripsi dan meraih gelar 
sarjana, melakukan aksi nekat bunuh diri lompat dari gedung lantai 13(Amirullah, 2008),  
begitu juga yang terjadi pada LA mahasiswi semester akhir di Batu Raja, yang mengeluh 
tentang sulitnya untuk sampai ujian akhir hingga akhirnya bunuh diri (Sumardi, 2014).  
Self-esteem memiliki keterkaitan dengan evaluasi dan self concept yang terbentuk 
dimulai dari pengalaman pada masa kanak-kanak. Interaksi pada masa bayi dengan figur 
signifikan membentuk internal working model tentang diri yang baik dan berharga atau diri 
yang buruk dan tidak berharga. Interaksi ini membentuk suatu ikatan yang disebut attachment. 
Rasa aman yang tercipta dalam diri sejak bayi akan menimbulkan rasa percaya diri untuk 
menggali dan mengenali lingkungan sekitar mereka sehingga pada akhirnya dapat menciptakan 
self-esteem, inisiatif dan kemandirian pada dewasa (Bowlby, 1969).  
Memasuki masa remaja hingga dewasa, teman menjadi figur yang lebih signifikan 
dibandingkan orangtua. Kelekatan antara individu dengan teman ini dinamakan peer 
attachment. Peer attachment memiliki hubungan yang positif dengan prestasi akademik. Selain 
itu, individu juga akan memiliki emosi yang positif dalam interaksi sosial dan kompetensi 
sosial. Armsden & Greenberg (1987) mengatakan, kualitas attachment terhadap orangtua dan 
teman sebaya pada remaja akhir memiliki hubungan yang tinggi dengan well-being, terutama 
dalam hal self-esteem dan kepuasan hidup. Bowlby (dalam Cassidy dan Shaver, 2008) 
mengatakan bahwa secure attachment (kualitas attachment yang tinggi) mempengaruhi 
pandangan dan penilaian seseorang terhadap dirinya.  
Penelitian menunjukan bahwa individu yang memiliki secure attachment melihat 
hubungan dengan teman sebaya menjadi sumber yang memberi dukungan sosial dan dukungan 
emosi yang positif (Laible, 2007). Begitu juga pada penelitian yang dilakukan oleh Laumi 
(2012) di Yogyakarta yang menemukan bahwa peer attachment menjadi prediktor yang lebih 
kuat dibanding dengan attachment pada ibu terhadap self-esteem remaja.  
Individu yang menghayati adanya penerimaan, kasih sayang orang lain (Secure 
attachment), akan memberikan pengalaman yang positif untuk meningkatkan self-esteem 
(Mikulincer, 2007). Contohnya yang terjadi pada Raeni yang berhasil menjadi lulusan terbaik 
yang tidak hanya mendapat dukungan dari ayahnya namun juga teman-teman serta ibu 
kosnya(Liputan 6, 2014). Sementara, ketika individu menghayati adanya penolakan (insecure 
attachment), akan memberikan pengalaman yang menyakitkan yang menurunkan self-esteem 
(Mikulincer, 2007). Contohnya yang terjadi pada mahasiswa yang merasa tertekan serta tidak 
berguna karena proposalnya ditolak oleh dosen (Insan, 2014).  
Berdasarkan teori Bowlby, Armsden & Greenberg (1987) memandang peer attachment 
dari 3 dimensi, yaitu trust, communication dan alienation. Dimensi trust yang ditandai dengan 
adanya hubungan timbal balik antara dihargai dan percaya, dimensi communication yaitu 
persepsi individu tentang komunikasi dengan teman sebaya, dan dimensi alienation yaitu 
perasaan individu tentang pengasingan, kesendirian. Individu yang memiliki trust dan 
communication yang positif maka cenderung tidak akan terlalu merasakan hal yang negatif 
ketika dirinya sendirian, sehingga cenderung menghayati  secure attachment. Sedangkan jika 
individu sulit memiliki kepercayaan pada orang lain pada umumnya akan menilai negatif pada 
interaksi yang dialaminya, cenderung merasakan kesepian jika orang yang dipercayainya tidak 
ada  sehingga ia cenderung memiliki insecure attachment. 
Berdasarkan survey awal melalui wawancara kepada 10 orang mahasiswa Fakultas 
Psikologi angkatan 2013, diperoleh gambaran sebagai berikut: 6 orang diantaranya, pada 
dimensi pertama yaitu trust, tiga orang diantaranya memiliki kecenderungan sangat percaya 
pada teman dekatnya dan menganggap teman dekatnya sudah mengerti tentang kepribadian 
mereka. Dua orang lainnya memercayai bahwa figur terdekat mereka sangat pengertian dan 
memahami mereka. Seorang lainnya percaya pada teman dekatnya hanya dalam hal-hal terkait 
perkuliahan saja. Dalam hal komunikasi, empat orang diantaranya merasa teman-teman 
sebayanya membawa pengaruh yang baik dan dapat memberikan solusi ketika diajak bercerita. 
Dua orang diantaranya merasa teman dekatnya peduli terhadap mereka. Sedangkan pada 
dimensi alienation, baik tiga orang diantaranya merasakan kecewa jika teman dekatnya tidak 
ada. Dua orang lainnya merasa ada yang kurang jika tidak ada teman dekatnya. Seorang lainnya 
jika teman dekatnya tidak ada merasa tidak kesepian. Dari penjelasan diatas, diperoleh 
sebanyak 6 dari 10 orang mahasiswa (60%) memiliki kecenderungan secure attachment. 
Individu yang menghayati secure peer attachment akan meningkatkan self-esteem-nya kearah 
yang lebih tinggi.  Misalnya yang dialami oleh 5 orang dari 10 responden (50%) memiliki 
karakteristik individu yang self-esteem tinggi. Tiga diantaranya ketika menerima tugas yang 
banyak dalam perkuliahan mereka berusaha untuk mencapai nilai yang maksimal dengan 
menyusun rencana pengerjaan tugas, tidak terlambat mengumpulkan tugas hingga 
menyelesaikan dengan cepat dan sebaik mungkin. Seorang responden selain memiliki 
keinginan IPK 3,5, ia juga memiliki keinginan acara kepanitiaan yang diikutinya dapat sukses. 
Untuk mencapai hal tersebut, ia lebih serius dalam mengerjakan tugas-tugas yang diberikan 
baik dalam perkuliahan kepanitiaan. Sedangkan seorang lainnya memiliki keinginan untuk 
memperoleh beasiswa di perkuliahan, karenanya ia sering browsing tentang program beasiswa 
dan berusaha untuk memperoleh IPK yang bagus. 
Pada 4 dari 10 orang mahasiswa (40%) diperoleh keterangan sebagai berikut; pada 
dimensi trust, dua orang memiliki kecenderungan sulit untuk percaya pada orang lain. Terkait 
hal komunikasi merasa teman-teman sebayanya mengerti keadaan dan menghargai pendapat 
mereka. Pada dimensi ketiga, yaitu alienation, keduanya memiliki kecenderungan untuk selalu 
berada bersama dengan teman dekatnya. Sedangkan dua orang lain menilai bahwa teman dekat 
bagi mereka adalah orang yang bisa dipercaya dan dapat memberikan saran/solusi ketika sedang 
ada permasalahan. Ketika figur yang terdekat(teman) tidak ada, merasa kesal, merasa sedih dan 
bingung kemana akan bercerita. Dari keterangan diatas menunjukkan 4 orang tersebut 
kecenderungan menghayati insecure attachment. Individu yang menghayati insecure peer 
attachment akan menurunkan self-esteem-nya ke arah yang lebih rendah.  Misalnya yang 
dialami oleh 2 orang dari 10 responden (20%) memiliki karakteristik individu yang self-esteem 
rendah. Seorang diantaranya yang ketika menemui tugas yang banyak, cenderung panik takut 
tugasnya tidak selesai. Sedangkan seorang lainnya ketika menemui tugas yang banyak, akan 
mengatakan semoga dirinya menjadi cepat kurus dikarenakan banyaknya tugas. 
Berdasarkan survey awal, terlihat adanya perbedaan peer attachment dan derajat self-
esteem pada diri mahasiswa Fakultas Psikologi angkatan 2013 di Universitas  X  kota 
Bandung. Oleh karena itu, peneliti tertarik untuk meneliti lebih lanjut mengenai pengaruh dari 
dimensi peer attachment terhadap self-esteem mahasiswa Fakultas Psikologi angkatan 2013 di 
Universitas  X  kota Bandung. 
1. 2 Identifikasi Masalah 
Dari penelitian ini ingin diketahui apakah terdapat pengaruh dimensi peer attachment 
terhadap self-esteem pada mahasiswa Fakultas Psikologi angkatan 2013 di Universitas  X  kota 
Bandung. 
1. Untuk memeroleh gambaran mengenai dimensi peer attachment pada 
mahasiswa Fakultas Psikologi angkatan 2013 di Universitas  X  kota 
2. Untuk memeroleh gambaran mengenai self-esteem pada mahasiswa Fakultas 
Psikologi angkatan 2013 di Universitas  X  kota Bandung 
Untuk mengetahui apakah terdapat pengaruh dimensi peer attachment terhadap 
self-esteem pada mahasiswa Fakultas Psikologi angkatan 2013 di Universitas 
1. Memberikan informasi apakah dimensi peer attachment pada mahasiswa 
Fakultas Psikologi angkatan 2013 di Universitas  X  Kota Bandung 
memengaruhi self-esteem. 
2. Memberikan masukan kepada peneliti lain yang memiliki ketertarikan untuk 
melakukan penelitian lanjutan mengenai dimensi peer attachment dan self-
esteem.  
1. Memberikan informasi bagi mahasiswa Fakultas Psikologi angkatan 2013 di 
Universitas  X  Bandung mengenai dimensi peer-attachment dan self-esteem 
yang diharapkan menjadi bahan evaluasi diri bagi mahasiswa yang berkaitan 
dengan teman sebaya dan penilaian terhadap diri mahasiswa. 

