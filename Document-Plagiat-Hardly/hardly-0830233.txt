Bapak  X  menambahkan bahwa banyak distributor yang merasa sulit 
menjalankan bisnis Tianshi karena tidak mengerti cara kerja di bisnis Tianshi. 
Di bisnis Tianshi, distributor sendiri yang menentukan jadwal kerja mereka. 
Oleh karena itu diperlukan impian yang ingin diwujudkan melalui bisnis 
Tianshi ini. Namun pada kenyataannya banyak distributor yang belum 
memiliki impian yang jelas sehingga mempengaruhi cara kerja mereka dan 
mengganggap bisnisnya tidak berkembang. Oleh karena itu, banyak distributor 
yang berhenti ketika menghadapi masalah (seperti tidak mencapai target, 
mengalami penolakan dan menghadapi downline yang mengundurkan diri) 
dan tidak mampu untuk mengatasinya, terlebih lagi jika distributor tersebut 
tidak mau mengembangkan diri dan kemampuan. 
Selain itu, tantangan lainnya yang dihadapi distributor ialah ketika 
mengejar target. Bapak  X  kembali menyampaikan bahwa dalam masa 
pengejaran target dibutuhkan kerja keras dari para distributor yang 
menjalankannya. Selain harus memenuhi jumlah omset tertentu, mereka juga 
harus menambah jumlah anggota aktif dalam jaringan bisnis mereka. 
Keterbatasan waktu serta persyaratan yang harus dipenuhi untuk mengejar 
target, dapat membuat para distributor merasa tertekan. Distributor harus 
meluangkan waktu dan tenaga lebih banyak agar target mereka tercapai. Jika 
distributor berhasil memenuhi target, perusahaan akan memberikan reward 
pada distributor tersebut. Jika distributor tidak berhasil memenuhi target 
tersebut, distributor memiliki kesempatan untuk mengikuti challange lainnya 
pada periode selanjutnya. Namun hal  ini tentunya menambah waktu, tenaga, 
serta biaya yang harus dikeluarkan distributor untuk memenuhi target 
selanjutnya.    
Peneliti juga melakukan wawancara terhadap 10  orang distributor Tianshi 
tahap pengembangan (peringkat *4, *5, *6 dan *7). Dari hasil wawancara 
tersebut diperoleh informasi bahwa 60% (6 dari 10 orang) distributor merasa 
memiliki tantangan dari dalam diri mereka saat menjalankan bisnis Tianshi, 
seperti usaha untuk keluar dari zona nyaman mereka (mengatasi rasa malas 
dalam memenuhi target, disiplin dalam melakukan pekerjaan dan 
pembelajaran) serta menjaga mental pribadi agar tetap memiliki pikiran dan 
sikap positif terhadap kendala-kendala yang mereka hadapi.  Hal ini dirasakan 
sulit untuk mereka lakukan. Mereka memiliki target namun kurang diimbangi 
dengan kerja keras untuk mencapai target tersebut, sehingga menjelang batas 
waktu penutupan target mereka sering merasa stress karena target belum 
tercapai. Saat merasa stress atau tertekan, distributor harus menjaga sikap dan 
pikirannya tetap positif agar mereka bisa bangkit lagi. Namun untuk dapat 
bangkit lagi dari kegagalan dan tetap memiliki sikap positif bukanlah hal 
mudah. Tidak jarang distributor merasa putus asa dan tidak berdaya saat 
mengalami kegagalan. 
Selanjutnya 30% (3 dari 10 orang) distributor merasa memiliki tantangan 
dari luar ketika menjalankan bisnis Tianshi, seperti penolakan dari orang-
orang di sekitar dan banyaknya downline yang mengundurkan diri dari 
jaringan bisnis Tianshi mereka. Penolakan-penolakan yang terjadi dapat 
menurunkan semangat kerja dan membuat para distributor ini merasa putus 
asa dalam merekrut downline. Selain itu, para distributor ini juga merasa stress 
ketika downline-downline mereka mengundurkan diri karena dengan begitu 
para distributor ini harus berusaha lebih keras lagi untuk membentuk tim atau 
jaringan yang baru. Dalam kondisi tersebut, terkadang para distributor ini 
merasa bahwa usaha yang dilakukannya dalam membangun bisnis Tianshinya 
berakhir dengan sia-sia. 
Kemudian 10% (1 dari 10 orang) distributor merasa memiliki tantangan 
dari luar dan dari dalam diri saat menjalankan bisnisnya. Tantangan dari luar 
tersebut seperti adanya komunikasi yang kurang lancar antara upline dan 
downline, misalnya upline yang terlalu sibuk mengurusi downline-nya yang 
lain sehingga downline lainnya yang jarang dibantu merasa terabaikan. Hal ini 
dapat menimbulkan kesalahpahaman dan dapat membuat hubungan antara 
upline dan downline menjadi kurang baik. Kondisi seperti ini dapat 
menghambat pencapaian target distributor tersebut. Tantangan dari dalam diri 
sendiri berupa usaha untuk keluar dari zona nyaman, distributor merasa sudah 
nyaman dengan prestasi kerjanya saat ini sehingga kurang termotivasi 
melakukan pekerjaan di bisnis ini. Akibatnya, prestasi kerjanya menjadi 
menurun. 
Selain itu, dalam menghadapi tantangan-tantangan tersebut para distributor 
juga merasa stress. Penolakan yang pernah dialami distributor membuat 
distributor merasa cemas ketika menghadapi prospek. Target yang diberikan 
pada distributor dengan waktu yang terbatas juga membuat pikiran dan 
perasaan distributor menjadi tidak tenang. Hal ini juga membuat para 
distributor merasa cemas dan sulit tidur, distributor merasa takut dirinya tidak 
dapat mencapai target. Meskipun demikian, para distributor ini akan berusaha 
untuk tetap bertahan dalam bisnis Tianshi dan menghadapi situasi stress yang 
mereka alami. Saat merasa stress dan jenuh dengan pekerjaan di bisnis 
Tianshi, beberapa cara yang dilakukan oleh para distributor untuk tetap 
bertahan yaitu dengan menghadiri pertemun yang diadakan support system 
One Vision, membaca buku-buku yang dianjurkan oleh support system One 
Vision, berkonsultasi dengan upline, dan terkadang selama beberapa hari 
distributor ini tidak melakukan pekerjaan di bisnis Tianshi terlebih dahulu 
untuk mengurangi rasa jenuh. 
Dari hasil wawancara tersebut dapat dilihat bahwa setiap distributor 
memiliki tantangan yang berbeda-beda dalam menjalankan bisnis di Tianshi. 
Tantangan yang dihadapi para distributor dalam pekerjaan mereka bisa 
menimbulkan keadaan stressful. Dalam menghadapi keadaan stressful 
tersebut, ada distributor yang tetap bertahan dalam pekerjaan mereka, namun 
tidak sedikit pula distributor yang mengundurkan diri dari pekerjaan mereka. 
Hal ini juga dipengaruhi oleh daya tahan (resilience at work) masing-masing 
distributor dalam bekerja. 
Resilience at Work merupakan kemampuan seseorang untuk dapat 
mengolah sikap dan kemampuannya menolong dirinya sendiri untuk bangkit 
kembali dari keadaan stress, memecahkan masalah, belajar dari pengalaman 
sebelumnya, menjadi lebih sukses dan mencapai kepuasan di dalam suatu 
proses (Maddi & Khoshaba, 2005). Resilience at work ini dapat terlihat jika 
distributor sedang dalam kesulitan dan tekanan dalam lingkungan kerjanya. Ia 
akan tetap berjuang dan mencari solusi positif untuk mengatasi kesulitan dan 
tekanan kerja yang dihadapinya. 
Resilience at Work memiliki tiga aspek yaitu commitment, control dan 
challenge. Commitment yaitu sejauh mana keterikatan dan keterlibatan 
individu dengan pekerjaannya meskipun berada dalam situasi yang stressful. 
Control merupakan sejauh mana individu akan berusaha  mengarahkan 
tindakannya untuk mencari solusi ketika menghadapi situasi yang stressful. 
Challenge yaitu sejauh mana sikap individu dalam memandang perubahan 
atau situasi yang stressful sebagai sarana untuk mengembangkan dirinya 
(Maddi & Khoshaba, 2005).  
Individu yang memiliki resilience at work tinggi akan mengubah kesulitan 
menjadi kesempatan mereka untuk mengembangkan dirinya dan membuat 
dirinya merasa antusias dan mampu menyelesaikan pekerjaannya. Individu 
akan lebih mampu untuk menanggulangi kesulitannya dengan mencari solusi-
solusinya dan saling mendukung dengan orang-orang yang ada di sekitarnya. 
Individu yang memiliki resilience at work  yang rendah, akan menganggap 
kesulitan menjadi sesuatu yang membebani dirinya dan membuat individu 
merasa pesimis, mudah menyerah dalam menghadapi situasi yang sulit dan 
menarik diri dari orang-orang yang ada di sekitarnya (Maddi & Khoshaba, 
2005). 
Disributor network marketing Tianshi khususnya yang berada di tahap 
pengembangan dalam menjalankan pekerjaannya menghadapi banyak 
tantangan yang bisa menyebabkan situasi stressful bagi mereka. Pada 
kenyataannya, dalam menghadapi situasi stressful tersebut ada distributor 
yang dapat bertahan dan ada pula distributor yang mengundurkan diri. 
Resilience at work di perusahaan network marketing Tianshi ini dibutuhkan 
agar para distributornya mampu bertahan dalam menghadapi setiap tantangan 
yang mereka hadapi dan tetap bertahan meskipun dalam situasi stressful. 
Berdasarkan hal tersebut mendorong peneliti untuk melakukan penelitian 
mengenai  resilience at work pada distributor network marketing Tianshi 
tahapan pengembangan di kota Bandung.  
 Ingin mengetahui bagaimana gambaran resilience at work pada distributor 
network marketing Tianshi tahapan pengembangan di kota Bandung. 
Memperoleh gambaran mengenai resilience at work pada distributor 
network marketing Tianshi tahapan pengembangan di kota Bandung. 
Memperoleh informasi mengenai resilience at work pada distributor 
network marketing Tianshi tahapan pengembangan di kota Bandung 
berdasarkan aspek commitment, control, dan challange. 
organisasi mengenai resilience at work. 
untuk meneliti topik yang serupa dan mendorong dikembangkannya 
penelitian yang berhubungan dengan hal tersebut. 
bahan pertimbangan untuk memberikan suatu program dalam hal 
mengembangkan resilience at work pada distributor network 
marketing Tianshi di kota Bandung. 
pentingnya sebuah resillience at work dalam usaha memajukan 
bisnis Tianshi para distributor. 
 Setiap orang yang berada pada usia 18   40 tahun berada pada tahap 
perkembangan masa dewasa awal, dimana salah satu tugas perkembangan 
mereka adalah untuk mendapatkan pekerjaan (Hurlock, 1980). Selama 
pemilihan pekerjaan, orang dewasa awal dengan sendirinya perlu 
menyesuaikan diri dengan sifat dan macam pekerjaan tersebut yang meliputi 
jenis pekerjaan, penyesuaian terhadap rekan kerja dan pimpinan, penyesuaian 
dengan lingkungan tempat ia bekerja, dan penyesuaian dengan peraturan serta 
batasan yang berlaku selama waktu kerja.  
 Salah satu jenis pekerjaan yang dipilih oleh orang-orang yang berada pada 
tahap masa dewasa awal tersebut sebagai upaya untuk memenuhi tugas 
perkembangannya ialah dengan bekerja sebagai distributor network marketing 
perusahaan Tianshi yang berada pada tahapan pengembangan di kota 
Bandung. Distributor network marketing Tianshi tahapan pengembangan di 
kota Bandung adalah pengusaha mandiri di kota Bandung yang 
mendistribusikan produk perusahaan langsung kepada konsumen. Para 
distributor network marketing Tianshi tahapan pengembangan di kota 
Bandung ini  kemudian mensponsori orang-orang lagi untuk membantu 
mendistribusikan produk dari perusahaan. 
Ketika melaksanakan pekerjaannya sebagai distributor network marketing 
Tianshi, tantangan yang sering dihadapi para distributor network marketing 
Tianshi tahapan pengembangan di kota Bandung yaitu berupa adanya 
penolakan dari orang-orang (prospek) yang mereka tawari untuk bergabung 
dengan bisnis Tianshi. Tidak jarang para distributor network marketing 
Tianshi tahapan pengembangan di kota Bandung mendapat hinaan dan celaan 
dari prospek mereka. Tidak sedikit pula para distributor network marketing 
Tianshi tahapan pengembangan di kota Bandung yang mendapat larangan dari 
keluarganya untuk menjalankan bisnis Tianshi.  
Selain itu, ketika para distributor network marketing Tianshi tahapan 
pengembangan di kota Bandung telah mendapatkan downline, distributor 
network marketing Tianshi tahapan pengembangan di kota Bandung dituntut 
untuk bisa mengajari, membimbing, mengarahkan, dan membantu downline 
mereka menjalankan bisnis Tianshi sesuai dengan sistem One Vision. Dalam 
hal ini, diperlukan kemampuan dari distributor network marketing Tianshi 
tahapan pengembangan di kota Bandung untuk melakukan pendekatan yang 
berbeda terhadap masing-masing downline-nya. Kemudian ketika para 
distributor network marketing Tianshi tahapan pengembangan di kota 
Bandung telah berhasil mengumpulkan downline, tidak sedikit downline-
downline para distributor yang pada akhirnya mengundurkan diri. Distributor 
network marketing Tianshi tahapan pengembangan di kota Bandung pun harus 
berusaha lebih keras lagi untuk mencari dan menemukan downline yang lebih 
serius dan berkomitmen.  
Selain itu, untuk distributor network marketing Tianshi tahapan 
pengembangan di kota Bandung yang ingin naik peringkat ataupun ingin 
mendapat reward dari perusahaan, distributor network marketing Tianshi 
tahapan pengembangan di kota Bandung harus memenuhi target dan 
persyaratan tertentu dengan batas waktu yang telah ditentukan oleh 
perusahaan Tianshi.  Keterbatasan waktu yang dimiliki serta persyaratan yang 
harus dipenuhi untuk mencapai target, membuat para distributor network 
marketing Tianshi tahapan pengembangan di kota Bandung beserta jaringan 
grupnya harus bekerja keras agar target terpenuhi.  
 Adapun saat menjalankan tugas-tugasnya tersebut, tidak jarang para 
distributor network marketing Tianshi tahapan pengembangan di kota 
Bandung mengalami tekanan dan stress akibat tuntutan tugas serta tantangan 
yang harus dihadapi. Oleh karena itu, para distributor network marketing 
Tianshi tahapan pengembangan di kota Bandung tersebut diharapkan memiliki 
kemampuan untuk bertahan dalam menghadapi stress yang menimpanya. Para 
distributor network marketing Tianshi tahapan pengembangan di kota 
Bandung tersebut diharapkan memiliki kemampuan resilience at work yang 
berguna sebagai kekuatan untuk tetap bertahan dalam situasi apapun.  
 Resilience at work merupakan kemampuan seseorang untuk dapat 
mengolah sikap dan kemampuannya menolong dirinya sendiri untuk bertahan 
dalam keadaan stress, memecahkan masalah, belajar dari pengalaman 
sebelumnya, menjadi lebih sukses dan mencapai kepuasan di dalam suatu 
proses (Maddi & Khoshaba, 2005). Individu yang resilient akan mengubah 
kesulitan menjadi kesempatan mereka untuk mengembangkan dirinya dan 
membuat dirinya merasa antusias dan mampu menyelesaikan pekerjaannya. 
Individu akan lebih mampu untuk menanggulangi kesulitannya dengan 
mencari solusi-solusinya dan saling mendukung dengan orang-orang yang ada 
di sekitarnya. 
 Di dalam resilience at work terdapat ketahanan sikap untuk berkomitmen 
(commitment), mengontrol (control) dan tantangan (challenge). Ketiga hal 
tersebut merupakan aspek dari resilience at work. Ketiganya memberikan 
keberanian bagi distributor network marketing Tianshi tahapan pengembangan 
di kota Bandung dan mendorong mereka untuk dapat menghadapi hambatan 
dalam lingkungan kerja mereka.  
 Aspek pertama dari resilience at work ini yaitu commitment. Commitment 
merupakan sejauh mana keterikatan dan keterlibatan  distributor network 
marketing Tianshi tahapan pengembangan di kota Bandung dengan 
pekerjaannya meskipun saat berada di dalam kondisi yang stressful. Pada saat 
distributor network marketing Tianshi tahapan pengembangan di kota 
Bandung sedang mengalami masalah dalam bisnis Tianshinya seperti 
mengalami penolakan dari prospek atau keluarga, adanya anggota jaringan 
yang mengundurkan diri dan saat ada kesulitan dalam mencapai target, 
distributor tersebut tetap menjalankan tujuh langkah sukses sebagai 
pekerjaannya di bisnis Tianshi.  Distributor network marketing Tianshi 
tahapan pengembangan di kota Bandung yang berkomitmen akan memiliki 
kekuatan di dalam dirinya untuk tetap bertahan di dalam keadaan stress, 
distributor network marketing Tianshi tahapan pengembangan di kota 
Bandung akan menunjukkan betapa pentingnya pekerjaannya dan menuntut 
dirinya untuk memberikan perhatian penuh pada usaha yang dijalankannya. 
 Aspek kedua dari resilience at work yaitu control 

