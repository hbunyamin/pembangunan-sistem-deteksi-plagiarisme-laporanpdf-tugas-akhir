Compassion dalam melaksanakan tugas mereka sebagai guru SLB sehingga mereka dapat 
meningkatkan self-compassion mereka supaya dapat menangani anak berkebutuhan khusus 
dengan lebih baik. 
yang dimiliki oleh guru SLBN B Bandung. Sehingga dapat digunakan sebagai informasi untuk 
dapat meningkatkan kinerja para guru SLBN B dan kualitas pengajaran melalui seminar atau 
konseling. 
Guru SLBN B Bandung bertugas untuk mengajar, mendampingi murid tuna rungu. Sebagai 
pengajar dan pendamping guru bertugas untuk memberikan bimbingan sosial, mental, fisik dan juga 
psikis. Para guru membantu murid untuk dapat meakukan tugas mandiri dan mengembangkan 
keterampilan mereka sehingga setelah lulus dari sekolah mereka dapat mandiri, bekerja dan ikut berperan 
di dalam masyarakat. Guru SLBN B Bandung dalam melaksanakan tugasnya, mereka tidak hanya 
mentransfer ilmu dan keterampilan saja pada murid, tetapi juga mentransfer keterampilan mereka, agar 
pada saat mereka sendiri setidaknya mereka masih dapat melakukan kegiatan sederhana dengan mandiri. 
  Dalam pengertian Self-Compassion terdapat 3 komponen utama yang membentuknya, yaitu Self-
kindness, common humanity, dan mindfulness. Ketiga komponen tersebut saling berkaitan yang akan 
menciptakan Self-Compassion. Self-kindness adalah kemampuan Guru SLBN B Bandung untuk dapat 
bersikap hangat dan penuh pengertian terhadap diri ketika berada dalam masa-masa sulit dan menyadari 
bahwa melakukan suatu kesalahan merupakan hal yang wajar dan apabila Guru mengembangkan self-
kindness, mereka juga  akan tetap mampu untuk menghargai dirinya sendiri sehingga mereka akan 
berfikir lebih positif  dan tidak akan melakukan penilaian negative terhadap dirinya sendiri apabila 
mereka melakukan suatu kesalahan atau gagal dalam mengajar dan mendampingi murid. Mereka 
cenderung lembut terhadap diri sendiri dan mengerti kelemahan dan kegagalan yang dimiliki bukan 
untuk dikritik secara berlebihan. Namun sebaliknya, apabila para guru mengembangkan self-judgement 
maka mereka akan cenderung memberikan penilaian yang negatif terhadap dirinya dan merasa gagal, 
tidak berguna,marah,stress, dan frustasi ketika menghadapi kegagalan. Guru SLB yang self-judgement 
cenderung mengkritik dan menghakimi diri secara berlebihan terhadap kekurangan diri ketika mengalami 
kesulitan atau kegagalan dalam mengajar dikelas. 
Common humanity  merupakan kemampuan guru untuk menyadari bahwa kesulitan hidup dan 
kegagalan merupakan bagian dari kehidupan yang dialami oleh semua manusia, bukan hanya dialami 
oleh dirinya sendiri. Dengan common humanity , guru tidak melihat kesulitan dan kegagalan yang di 
alaminya ketika mengajar murid sebagai sesuatu yang hanya dialami oleh dirinya sendiri, tetapi 
merupakan sesuatu yang dialami oleh semua guru. Namun apabila seorang guru merasa bahwa hanya 
dirinya yang mengalami kesulitan dan kegagalan, berarti guru tersebut mengalami isolation. Guru yang 
mengalami isolation membuat mereka sulit untuk menerima kenyataan dan merasa bahwa orang lain 
lebih mudah untuk berhasil dalam melakukan tugasnya sebagai guru SLB dibandingkan dirinya. 
Mindfulness merupakan kemampuan guru untuk menerima dan menyadari perasaan dan 
pikirannya sendiri saat mengalami kegagalan apa adanya, tanpa disangkal atau ditekan dan tidak 
menyangkal aspek-aspek yang tidak disukai baik di dalam dirinya maupun di dalam kehidupannya. Guru 
yang mengembangkan mindfulness mampu mengakui dan menerima kegagalan atau kesalahannya. Ia 
tetap berusaha untuk berpikiran positif dan secara tenang berusaha untuk mengatasi kegagalan tersebut 
sehingga ia dapat mengambil langkah yang tepat untuk mengatasi kegagalannya tersebut dan kelak tidak 
melakukan kesalahan yang sama. Apabila seorang guru merasa kurang mampu dan kurang 
berpengalaman dalam menangani murid, ia dapat menerima hal itu dan berusaha untuk mencari jalan 
keluar yang tepat misalnya dengan meminta bantuan guru lain yang lebih berpengalaman sehingga murid 
bisa mendapat pengajaran dan penampingan yang lebih baik. Namun sebaliknya jika guru 
mengembangkan over-identification maka ia cenderung tidak mengakui kegagalannya atau 
mengeluarkan emosi negatif yang berlebihan ketika mengalami kegagalan, seperti merenungi dan merasa 
menyesal setiap kali melakukan kegagalan atau tidak mampu menangani murid tertentu. 
Ketiga komponen dalam self compassion menurut Neff (2003) saling mempengaruhi dan saling 
berhubungan satu dengan yang lain. Mindfulness memiliki kontibusi terhadap dua komponen lainnya 
yaitu Self-kindness dan common humanity. Dengan kemampuan untuk menyadari perasaan dan 
pikirannya apa adanya, tanpa melebih-lebihkan, dapat mengurangi self-critism dan meningkatkan self-
uderstanding, sehingga meningkatkan self-kindness. Selain itu juga dengan menyadari perasaan dan 
pikiran secara apa adanya, dapat mengurangi egocentism yanfg dapat menyebabkan perasaan terisolasi 
dan terpisah dari orang lain, sehingga meningkatkan perasaan interconnectedness. Self-kindness dan 
perasaan connectedness dapat meningkatkan mindfulness. Dengan berhenti menghakimi diri sendiri akan 
mempengaruhi dan meningkatkan derajat self-acceptance, dampak negative dari pengalaman emosional 
pada individu akan berkurang, sehingga akan mempermudah individu dalam menjaga keseimbangan 
pikiran dan emosinya tidak menghindari atau terbawa oleh perasaannya. Demikian pula, dengan 
menyadari bahwa penderitaan dan kegagalan merupakan sesuatu yang dialami oleh setiap orang 
membantu individu untuk dapat melihat sesuatu sesuai dengan apa adanya, dan meningkatkan 
kemampuan untuk menyadari pikiran dan perasaannya dan tidak melebih-lebihkannya. 
Self-kindness dan common humanity juga saling mempengaruhi satu sama lain. Ketika individu 
menghakimi dirinya secara berlebihan, dapat meningkatkan kesadaran diri dan perasaan terisolasi. 
Namun dengan berbaik hati terhadap diri sendiri dapat mengurangi kesadaran diri, dan meningkatkan 
perasaan terhubung dengan orang lain. Self-compassion memerlukan ketiga komponen tersebut, satu 
komponen berhubungan dengan komponen-komponen lainnya dan saling memperngaruhi. Self-
compassion dari seorang guru dapat dikatakan tinggi apabila ketiga komponen  tersebut tinggi. 
Sebaliknya, apabila salah satu atau dua ataupun ketiga komponen tersebut rendah, maka self-compassion 
dari seorang guru dikategorikan rendah. Selain ketiga komponen dari self-compassion di atas, derajat 
self-compassion pada Guru SLBN B Bandung juga dipengaruhi oleh beberapa faktor yang terdiri dari 
faktor internal, yaitu personality dan jenis kelamin, dan faktor eksternal, yaitu The Role of Parents 
(Attachement, Maternal Criticism, Modeling of parents). Personality dapat memengaruhi derajat self 
compassion pada  Guru SLBN B Bandung. Penelitian menunjukkan self compassion memililki hubungan 
dengan The Big Five Personality. The Big Five Personality adalah suatu pendekatan yang digunakan 
untuk melihat kepribadian manusia melalui trait yang tersusun dalam lima buah domain kepribadian 
yang telah dibentuk dengan menggunakan analisis factor. Trait kepribadian tersebut adalah extraversion, 
Agreeableness, Conscientiousness, Neuroticism, dan openness to experiences. Dari kelima trait tersebut, 
setiap individu memiliki satu trait yang dominan di dalam diriya. Berdasarkan pengukuran yang 
dilakukan oleh NEO-FFI, ditemukan bahwa self-compassion memiliki hubungan dengan dimensi 
neuroticism, agreeableness, extroversion, dan conscientiousness dari the big five personality. Namun, 
self-compassion tidak memiliki hubungan dengan openness to experience, sebab trait ini mengukur 
karakteristik individu yang memiliki imajinasi yang aktif, kepekaan secara aesthetic, sehingga dimensi 
openness to experience ini tidak sesuai dengan self-compassion (Neff, 2007).  
Self-compassion memiliki hubungan yang paling kuat dengan neuroticism, semakin tinggi derajat 
Self-compassion maka akan semakin rendah derajat neuroticism. Menurut Costa & McCrae (1997) 
neuroticism menggambarkan seseorang yang memiliki masalah dengan emosi yang negative seperti rasa 
khawatir dan rasa tidak aman, mudah mengalami kecemasan, rasa marah, dan depresi. Hubungan ini 
bukanlah suatu hal yang mengejutkan, karena mengkritik diri dan perasaan terasing yang menyebabkan 
rendahnya self-compassion memiliki kesamaan dengan neuroticism. Misalnya ketika ada murid yang 
lamban atau sulit memahami suatu materi tertentu yang disampaikan di dalam kelas, hal tersebut 
membuat guru yang mengajar di kelas tersebut merasa bersalah, ia merasa bahwa dirinya tidak memiliki 
kompetensi yang cukup dalam mengajar sehingga murid tidak dapat memahami materi dengan benar. 
Self-Compassion menunjukkan hubungan yang positif dengan agreeableness dan extraversion. 
Agreeableness dapat disebut juga social adaptability yang mengindikasikan individu ramah, memiliki 
kepribadian yang selalu mengalah,menghindari konflik, dan memiliki kecenderungan untuk mengikuti 
orang lain dan memiliki emosional yang seimbang. Agreeableness tinggi ditandai dengan sikap yang 
baik, penuh simpati,kehangatan dan penuh pertimbangan terhadap perasaan orang lain. Guru SLB yang 
terbuka terhadap pendapat orang lain, seperti pada saat guru mendapat masukkan dari guru lain karena 
melakukan kesalahan pada saat melakukan tugasnya, maka guru SLB dapat melihat teguran itu 
meupakan masukan dan kritikan sebagai hal yang positif. Sehingga tidak ada rasa marah,mengritik atau 
menyalahkan diri secara berlebihan akibat teguran yang diberikan oleh kepala sekolah. Hal tersebut dapat 
membuat Guru SLB memiliki derajat Self-Compassion yang tinggi. 
Extraversion dicirikan dengan afek positif seperti antusiasme yang tinggi, senang bergaul, 
memiliki emosi positif, energik, tertarik dengan banyak hal, ambisius, workaholic juga ramah terhadap 
orang lain. Extraversion  memiliki tingkat motivasi yang tinggi dalam bergaul, menjalin hubungan 
dengan sesama dan juga dominan dalam lingkungannya. Extraversion menggambarkan individu yang 
senang bergaul dan memiliki emosi positif cenderung lebih banyak terbuka terhadap orang lain. Guru 
SLBN  X  di kota Bandung yang terbuka terhadap orang lain, seperti saling sharing mengenai saat 
mereka mengalami situasi sulit dalam melaksanakan tugasnya akan memiliki kesadaran bahwa kegagalan 
merupakan hal yang juga dapat dialami oleh semua guru, sehingga tidak membuat guru merasa terisolasi 
atas kegagalan yang dialami. Hal ini dapat membuat guru SLBN  X  Bandung memiliki self-compassion 
yang tinggi. Dengan agreeableness dan extraversion guru SLBN  X  , individu cenderung jarang 
mengalami masalah dalam hal emosi negatif, seperti depresi, kecemasan, rasa marah,sehingga guru lebih 
dapat bersikap baik dan ramah terhadap diri sendiri (self  kindness) dan melihat pengalaman yang negatif 
sebagai pengalaman yang dialami semua manusia (common humanity) (Neff,2003).  
Kemampuan untuk dapat berbaik hati, perasaan terhubung dengan orang lain dan keseimbangan 
secara emosional pada individu yang self-compassionate mempengaruhi kemampuan individy untuk 
dapat menjalin hubungan yang baik dengan orang lain. Guru yang memiliki derajat yang tinggi dalam 
agreeableness dan extraversion berorientasi pada sifat sosial sehingga hal itu dapat membantu mereka 
untuk bersikap baik kepada orang lain dan melihat pengalaman yang negatif sebagai pengalaman yang 
dialami semua manusia. 
Guru yang cenderung extraversion akan memiliki self-compassion tinggi, karena mereka tidak 
terlalu khawatir dengan pandangan orang lain tentang mereka, yang dapat mengarah pada rasa malu dan 
perilaku menyendiri. Hubungan yang signifikan juga ditemukan antara self-compassion dan 
conscientiousness. Menurut Costa & McCrae (1997), conscientiousnes mendeskripsikan control 
terhadap lingkungan sosial, berpikir  sebelum bertindak, menunda kepuasan, mengikuti peraturan dan 
norma, terencana, terorganisir, dan memprioritaskan tugas. Hal ini dapat membantu guru untuk lebih 
memperhatikan kebutuhan mereka dan untuk merespon situasi itu tanpa memberikan kritik yang 
berlebihan. Keseimbangan emosional pada guru yang self-compassionate dapat menghasilkan perilaku 
yang lebih bertanggung jawab. Conscientiousness menggambarkan individu yang mengejar sedikit 
tujuan dalam satu cara yang terarah dan cenderung bertanggung jawab. Guru SLBN  X  yang 
menunjukkan kestabilan emosi dapat membantu guru agar dapat mengelola emosi negatifnya dengan 
baik dan menimbulkan perilaku yang bertanggung jawab untuk terus belajar daripada terpaku pada semua 
kesalahan dirinya akibat kegagalan. Hal ini dapat membuat guru SLBN  X  mempunyai derajat self 
compassion yang tinggi. 
Faktor lainnya yang mempengaruhi adalah faktor eksternal yaitu The role of parents yang terdiri 
dari maternal support, modeling parents dan attachment dapat memengaruhi derajat self-compassion( 
Neff, 2011). 

