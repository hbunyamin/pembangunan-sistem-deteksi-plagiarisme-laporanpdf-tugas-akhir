Masyarakat Indonesia merupakan suatu masyarakat majemuk yang 
memiliki keanekaragaman di dalam berbagai aspek kehidupan. Bukti nyata 
adanya kemajemukan di dalam masyarakat kita terlihat dalam beragamnya 
kebudayaan di Indonesia. Tidak dapat kita pungkiri bahwa kebudayaan 
merupakan hasil cipta, rasa, karsa manusia yang menjadi sumber kekayaan bagi 
bangsa Indonesia. Tidak ada satu masyarakat pun yang tidak memiliki 
kebudayaan. Begitu pula sebaliknya tidak akan ada kebudayaan tanpa adanya 
masyarakat. Ini berarti begitu besar kaitan antara kebudayaan dengan masyarakat. 
Melihat realita bahwa bangsa Indonesia adalah bangsa yang plural maka akan 
terlihat pula adanya berbagai suku bangsa di Indonesia. Tiap suku bangsa inilah 
yang kemudian mempunyai ciri khas kebudayaan yang berbeda-beda. (http://de-
kill.blogspot.com/2009).  
Salah satu suku yang ada di indonesia adalah suku toraja. Suku Toraja 
adalah suku yang menetap di pegunungan bagian utara Sulawesi Selatan, 
Indonesia. Mereka juga menetap di sebagian dataran Luwu dan Sulawesi Barat. 
Nama Toraja mulanya diberikan oleh suku Bugis Sidenreng dan dari Luwu. Orang 
Sidenreng menamakan penduduk daerah ini dengan sebutan To Riaja yang 
mengandung arti "Orang yang berdiam di negeri atas atau pegunungan". Ada juga 
versi lain bahwa kata Toraya asal To = Tau (orang), Raya = dari kata Maraya 
(besar), artinya orang orang besar, bangsawan. Lama-kelamaan penyebutan 
tersebut menjadi Toraja, dan kata Tana berarti negeri, sehingga sekarang lebih 
dikenal dengan Tana Toraja. 
(http://petanitangguh.blogspot.com/2010/03/kebudayaan-toraja.html) 
Suku Toraja memiliki sapaan khas yaitu, manasumora ka (apakah nasi 
sudah masak?). Sapaan ini digunakan pada saat seseorang lewat di depan rumah. 
Balasan dari sapaan itu adalah manasumo, ta lendu opa (iya sudah masak, mari 
singgah dulu). Balasan ini tetap diberikan walaupun sbenarnya belum waktu 
makan atau bahkan api belum menyala. Makanan atau makan bersama adalah 
wadah untuk menyatakan persekutuan. Dari sapaan di atas dapat ditarik 
kesimpulan bahwa bagi orang Toraja adalah suatu nilai yang tinggi untuk 
memberi makan kepada tamu. Latar belakang dari nilai penghargaan terhadap 
tamu adalah harapan bahwa yang bersangkutan akan mendapatkan berkat dari 
pada dewa. (Manusia Toraja, Kobong, 1983)  
Suku Toraja masih terikat oleh adat istiadat dan kepercayaan nenek 
moyang. Kepercayaan asli masyarakat Tana Toraja yang disebut Aluk Todolo, 
kesadaran bahwa manusia hidup di Bumi ini hanya untuk sementara, begitu kuat. 
Prinsipnya, selama tidak ada orang yang bisa menahan Matahari terbenam di ufuk 
barat, kematian pun tak mungkin bisa ditunda. Sesuai mitos yang hidup di 
kalangan pemeluk kepercayaan Aluk Todolo, seseorang yang telah meninggal 
dunia pada akhirnya akan menuju ke suatu tempat yang disebut puyo; dunia 
arwah, tempat berkumpulnya semua roh. Hanya saja tidak setiap arwah atau roh 
orang yang meninggal itu dengan sendirinya bisa langsung masuk ke puyo. Untuk 
sampai ke sana perlu didahului upacara penguburan sesuai status sosial semasa ia 
hidup. Jika tidak diupacarakan atau upacara yang dilangsungkan tidak sempurna 
sesuai aluk, yang bersangkutan tidak dapat mencapai puyo dan Jiwanya akan 
tersesat. Oleh karena itu, upacara kematian menjadi penting dan semua aluk yang 
berkaitan dengan kematian sedapat mungkin harus dijalankan sesuai ketentuan. 
Sebelum menetapkan kapan dan di mana jenazah dimakamkan, pihak keluarga 
harus berkumpul semua, hewan korban pun harus disiapkan sesuai ketentuan. 
Pelaksanaannya pun harus dilangsungkan sebaik mungkin agar kegiatan tersebut 
dapat diterima sebagai upacara persembahan bagi tomebali puang mereka agar 
bisa mencapai puyo alias surga. Ritual pemakaman Toraja merupakan peristiwa 
sosial yang penting, biasanya dihadiri oleh ratusan orang dan berlangsung selama 
beberapa hari. (http://petanitangguh.blogspot.com/2010/03/kebudayaan-
toraja.html) 
Prosesi penguburan jenazah (rambu solo ) bisa lebih semarak 
dibandingkan upacara pernikahan anggota masyarakat adat mereka. Biaya pesta 
penguburan bagi seseorang yang berkhasta tinggi bisa mencapai  1 milyar rupiah 
bahkan lebih. Dana sebesar itu dipakai untuk membeli babi, kerbau, membangun 
pemondokan pesta, dan pernak-pernik upacara lainnya. Jika dana tak kunjung 
memadai untuk semua itu, jenazah bisa disimpan selama satu sampai dua tahun 
sebelum dikubur.Kuburan kuno dalam dinding dingin cadas juga diyakini sebagai 
representasi surga. Semakin banyak benda yang dibawa sang mayat maka 
semakin bahagia hidupnya di alam baka. Keluarga-keluarga kaya biasanya 
menyertakan emas dan perhiasan dalam kubur leluhur mereka.Sebuah kubur batu 
biasanya disiapkan hingga berbulan-bulan.Setelah siap, di beranda kubur itu 
diletakkan  Tau-tau sebagai sarana mengingat jenazah. 
(http://nulisonline.wordpress.com /2009/09/30/mumi-toraja/) 
Satu hal yang memegang peranan yang cukup penting dalam adat istiadat 
masyarakat suku Toraja yakni Kerbau. Bagi etnis Toraja, kerbau adalah binatang 
yang paling penting dalam kehidupan sosial mereka. Kerbau atau dalam bahasa 
setempat tedong tidak dapat dipisahkan dari kehidupan sehari-hari masyarakat. 
Selain sebagai hewan untuk memenuhi kebutuhan hidup sosial, ritual maupun 
kepercayaan tradisional, kerbau juga menjadi alat takaran status sosial, dan alat 
transaksi. Dari sisi sosial, kerbau merupakan harta yang bernilai tinggi bagi 
pemiliknya. Tidak mengherankan bila orang Toraja sangat menjaga kerbau 
mereka. Hal ini dapat dilihat dari percakapan sehari-hari, pada saat hendak 
bertransaksi, mengadakan pesta, dalam menjalankan ibadah keagamaan. 
(http://magazindo.info/tag/suku-toraja/)  
Selain pemakaman ada juga tongkonan(rumah adat Toraja).Tongkonan, 
yang berbentuk rumah panggung dan beratap melengkung, terdiri dari tiga bagian, 
yakni atas, tengah, dan bawah.Bagian tengah berfungsi sebagai tempat tinggal 
yang di dalamnya terdapat teras, ruang tamu, ruang tidur, dan dapur.Bagian atas 
biasanya digunakan sebagai tempat menyimpan jenazah sebelum 
dimakamkan.Bagian kolong biasanya untuk tempat warga bercengkerama.Bagi 
penganut Aluk Todolo, bagian atas, tengah, dan bawah tongkonan bermakna 
langit, bumi, dan bawah bumi.Langit dipercaya tempat Puang Matua (pencipta) 
yang berwujud laki-laki.Bumi digambarkan sebagai Datu Baine, saudara 
perempuan Puang Matua.Inilah yang memunculkan idiomatik tongkonan berjenis 
kelamin perempuan.Tongkonan tidak pernah berdiri sendiri. Di depan tongkonan 
selalu terdapat alang, tongkonan berukuran lebih kecil. Alang berfungsi sebagai 
tempat penyimpanan padi.Penganut Aluk Todolo umumnya menyebut alang 
Londong Nabanua (ayam jantan).Itulah mengapa alang diibaratkan berjenis 
kelamin laki-laki. Jika atap tongkonan dan alang disatukan akan membentuk 
bulatan, simbol keseimbangan makrokosmos dan mikrokosmos dalam hidup suku 
Toraja.(Stanislaus) 
Dari nilai harga diri dalam hubungan dengan proses mengumpulkan 
kekayaan sudah dapat ditarik kesimpulan bahwa nilai pekerjaan dan kerajinan 
besar artinya bagi orang Toraja. Dalam masyarakat tradisional seseorang dapat 
mengembangkan miliknya dari kecil sampai ke hal yang besar. Selanjutnya dari 
peribahasa berikut dapat dilihat bahwa kerajinan dan pekerjaan sungguh suatu 
kebajikan :   Labiran mamma -mamma  na iatu leppeng. Mandu melo opa iatu 
sumalong-malong na iatu madokko -dokko . Apa la bi melo iatu mengkarang na 
iatu sumalong-malong . (  Lebih baik tidur-tiduran daripada tidur nyenyak. Lebih 
baik duduk-duduk daripada tidur-tiduran. Jauh lebih baik jalan-jalan daripada 
duduk-duduk. Tetapi lebih baik lagi bekerja daripada jalan-jalan ). Jadi jelas 
bahwa nilai bekerja dan pekerjaan sangat tinggi. Hal ini dilambangkan di dalam 
Pa  barre allo motif ayam dan matahari terbit dalam ukiran Toraja. Ayam 
membangunkan manusia pada waktu fajar merekah, supaya jangan ketiduran.  
(Manusia Toraja, Kobong, 1983)  
Value adalah konsep atau kepercayaan, mengarahkan pada keadaan akhir 
atau tingkah laku yang diinginkan, hakikat dari sesuatu yang spesifik, pedoman 
untuk menyelesaikan tingkah laku dan kejadian-kejadian yang disusun 
berdasarkan kepentingan yang relatif (Schwartzs dan bilsky (2001). Adapun value 
itu adalah Self direction values, Stimulation value, Hedonism value, Achievement 
value, Power value, Security value, Conformity value, Tradition value, 
Benevolence value, dan Universalism value. 
Apabila mengacu pada value diatas, maka value yang terpenting dalam 
suku Toraja adalah tradition value. Hal ini terlihat dari banyaknya upacara-
upacara adat yang masih dilakukan sampai sekarang, ukiran-ukiran yang masih 
dipeertahankan sampai sekarang, dan pembangunan tongkonan yang masih 
dipertahankan sampai sekarang.selain itu dalam upacara rambu solo terkandung 
jugatradition, security, dan power value.Tradition value dimana hal ini sudah 
merupakan tradisi dari masa lampau yaitu kepercayaan aluk todolo. Security value 
dimana semua yang dilakukan itu untuk menjamin agar orang yang meninggal 
dapat tiba dengan selamat di surga. Power value dimana upacara ini juga 
dilakukan untuk menunjukkan kekuasaan, dimana orang yang meninggal biasanya 
seorang penguasa atau orang kaya. 
Selain dari ke tiga value itu, pada suku Toraja juga terdapat benevolence, 
achievement, conformity, dan hedonism value. Benevolence value dilihat dari 
sapaan manasumora ka (apakah nasi sudah masak?), yang bertujuan untuk 
mensejahterakan orang yang berinteraksi dengannya sehari-hari. Achievement 
value dilihat dari peribahasa orang Toraja yang intinya lebih suka bekerja 
daripada hanya jalan-jalan. Conformity value dilihat dari orang suku Toraja yang 
walaupun sudah tidak melakukan Rambu Solo, tetap hadir di tempat upacara. 
Hedonism value dilihat dari Tedong (kerbau) yang juga mencerminkan 
kesenangan atau hobi. 
Pada dasarnya orang Toraja tidak agresif-expansif.Orang Toraja justru 
menjaga kedamaian, hidup rukun dengan tetangga dan dengan siapa saja.Nilai-
nilai lain bisa dikorbankan demi karapasan (kedamaian).Kebenaran dan keadilan 
bisa dikorbankan demi kedamaian dan kerukunan, bukan saja antara pihak-pihak 
yang bersengketa, tetapi justru untuk persekutuan yang lebih besar.Hal ini 
termasuk ke dalam universalism value. 
 Budaya yang di pelajari oleh remaja masa kini jelas berbeda dengan yang 
dipelajari oleh remaja zaman dulu. Dari budaya, hal baik yang dapat dilihat adalah 
remaja dapat berkembang dengan cara selalu mengeluarkan kreatifitas mereka, 
namun apabila mereka dibiarkan begitu saja, maka mereka akan kehilangan rasa 
sopan santun yang seharusnya ada dalam diri mereka masing-masing. Mungkin di 
mata generasi tua bahwa remaja tidak mementingkan nilai-nilai tradisional yang 
dimiliki, tetapi sebenarnya remaja tersebut hanya tidak tahu dan tidak familiar 
dengan nilai-nilai tesebut karena yang hal yang mereka pelajari sudah berbeda 
dari yang dipelajari oleh generasi-generasi tua. Di mata remaja, menurut mereka 
bahwa hal tersebut tidak familiar dan aneh apabila mereka ikuti, jadi mereka 
hanya menganggap bahwa hal tersebut tidak penting bagi mereka.Maka sebaiknya 
nilai-nilai tradisional yang ada dan nilai-nilai masa kini lebih baik kombinasi 
menjadi satu untuk membentuk karakter yang lebih  baik untuk para remaja. 
Dengan mengambil nilai-nilai baik dari kedua pihak dan meninggalkan yang 
buruk merupakan salah satu solusi yang baik.  
(http://nicsaypandanganremajatuknilaitradi.blogspot.com/) 
Salah satu kecamatan di Sulawesi selatan yang juga terdapat suku Toraja 
adalah Kecamatan Towuti. Kecamatan Towuti merupakan salah satu kecamatan di 
Kabupaten Luwu Timur, luas wilayahnya 1.820,48 km2, terdiri dari luas daratan 
1.219 km2 dan luas danau sebesar 601,48 km2. Kecamatan Towuti di huni 
beberapa suku diantaranya suku Toraja, Bugis, Jawa, Padohe, Ambon dan lainnya 
(www.luwutimurkab.go.id). Keberagaman suku yang ada dapat menyebabkan 
terjadinya percampuran dalam hal tradisi dan adat-adat para penganutnya. 
Di ibu kota kecamatan Towuti terletak hanya satu SMA, yaitu SMAN I 
Towuti. Di SMA ini siswanya terdiri dari beragam suku, dan termasuk juga di 
dalamnya suku toraja, sehingga pada saat siswa masuk ke sekolah ini maka 
akanberusaha untuk menyesuaikan diri. Karena banyaknya suku yang ada di 
SMAN I Towuti, maka terjadi multikultural. 
Value pada siswa/i SMA Towuti dipengaruhi oleh berbagai faktor, yaitu 
faktor internal dan eksternal. Faktor internal meliputi usia, jenis kelamin, agama, 
pendidikan, sedangkan faktor eksternal meliputi proses transmisi. Proses transmisi 
adalah proses yang bertujuan untuk mengenalkan perilaku yang sesuai kepada para 
anggotanya dari suatu budaya tertentu. Transmisi budaya terbagi menjadi tiga 
berdasarkan sumbernya, yaitu: vertical transmission (orang tua), oblique transmission 
(orang dewasa lain, guru, paman, bibi, kakek, nenek atau lembaga lain) dan 
horizontal transmission (teman sebaya) (Cavali-Sforza dan Feldman dalam Berry, 
1999). Proses transmisi budaya tersebut dapat berasal dari budaya sendiri maupun 
dari budaya lain, yang akan diikuti oleh proses enkulturasi, akulturasi serta sosialisasi. 
Dari survei awal yang dilakukan oleh peneliti terhadap 10 orang siswa, 10 
%siswa tidak bisa sama sekali berbahasa Toraja, 50 %siswa hanya bisa sedikit 
berbahasa Toraja, dan 40 %siswa fasih berbahasa Toraja. Penguasaan dalam hal 
bahasa kemungkinan dipengaruhi oleh pengunaan bahasa sehari-hari yang tidak 
menggunakan bahasa Toraja, namun menggunakan bahasa daerah atau bahasa 
Indonesia.Dalam hal penghayatan menjadi orang Toraja, 20 %siswa merasa biasa-
biasa saja karena menurut mereka budaya Toraja sama saja dengan yang lain, jadi 
tidak ada hal yang perlu dibanggakan, sedangkan 80 %siswa lain merasa bangga 
karenapersatuan dari orang Toraja yang sangat kuat, budaya dan tradisi yang unik, 
dan keramahan orang Toraja. Dari hal pengetahuan mengenai upacara-upacara 
yang ada di suku Toraja, kebanyakan dari siswa hanya menyebutkan upacara-
upacara yang sudah terkenal, seperti rambu solo, rambu tuka dan mangrara 
banua. Dari upacara-upacara tersebut, 20 %siswa mampu menyebutkan ketiga 
upacara itu, 30 %siswa hanya mampu menyebutkan rambu solo , sedangkan 50 
%siswa yang lain sama sekali tidak mampu menyebutkannya.Pada upacara-
upacara yang menggunakan adat Toraja, kesepuluh siswa masih mau untuk 
menggunakan pakaian adat.Selain itu mereka juga masih mengetahui sapaan khas 
dari suku Toraja seperti manasumora ka .Dari hasil survei awal kepada 10 orang 
siswa yang bersuku Toraja di SMA Towuti, maka dapat disimpulkan bahwa 
mereka sebagian masih kurang dalam hal pengetahuan mengenai Toraja. 
Hasil wawancara yang dilakukan peneliti dengan salah satu guru yang juga 
orang dari suku Toraja mengatakan bahwa secara keseluruhan para siswa suku 
Toraja masih memegang tradisi sebagai orang Toraja.Salah satu bukti adalah para 
siswa masih mau turut ambil bagian dalam upacara-upacara yang diadakan 
dengan menjadi pa gellu(penari) dan memakai pakaian adat. Selain itu mereka 
juga mengikuti sanggar tari yang diselenggarakan oleh masyarakat Toraja. 

