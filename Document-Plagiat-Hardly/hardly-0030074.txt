Penelitian ini meneliti optimisme penderita kanker leher rahim stadium 
invasif dini di kota Bandung. Acuan teori yang digunakan adalah teori optimisme 
dari Martin E.P. Seligman serta Petterson - Bossio, teori ginekologi khususnya 
mengenai kanker leher rahim, dan teori perkembangan masa pertengahan dewasa 
dari Santrock.   
 Penelitian ini dilakukan secara kualitatif yaitu menggali setiap kasus 
secara mendalam tanpa menghilangkan keunikan tiap-tiap kasus.
