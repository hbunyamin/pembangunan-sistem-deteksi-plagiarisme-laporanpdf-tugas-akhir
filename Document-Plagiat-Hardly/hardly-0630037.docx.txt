Faktor-faktor yang mempengaruhi istri korban KDRT dalam Explanatory Style yaitu: 
Explanatory Style dari figur-figur yang signifikan, seperti: ibu, ibu mertua, dan teman-teman 
terdekatnya yang memberikan perhatian terkait perilaku kekerasan yang dilakukan oleh 
suaminya. Dan faktor anak yang menjadi salah satu faktor terbesar. Istri memikirkan kondisi 
psikis dan masa depan anak ketika melihat kenyataan yang dialami oleh kedua orang tuanya.  
Faktor Explanatory Style  ini menjelaskan bahwa Explanatory Style tidak diturunkan, 
melainkan dipelajari dari lingkungan, melalui berkomunikasi dengan orang tuanya, suaminya, 
teman yang lebih tua atau lebih muda atau teman sebayanya, memperhatikan setiap isi 
perkataan, dan lain sebagainya yang akan membentuk Explanatory Style. 
Faktor yang kedua adalah kritik, komentar, saran, pujian juga nasihat dari figur-figur 
yang signifikan. Kritikan, saran, pujian dan nasihat ini didengarkan dan digunakan istri 
sebagai proses kognisi hingga akhirnya memandang kasus rumah tangga yang menimpa 
hingga menentukkan sikap yang akan diambil untuk menyelesaikan kasus kekerasan rumah  
tangga yang menimpanya.  
Kemudian masa krisis istri yang mengalami kekerasan dalam rumah tangga dimana 
pengalaman yang mempengaruhi terbentuknya cara pikir istri dalam melihat sebab dari 
peristiwa-peristiwa kekerasan selama ini yang telah di lakukan oleh suaminya. Jika terdapat 
trauma dan trauma tersebut tidak segera ditangani, maka istri tidak dapat menerima 
kenyataan yang ada dalam waktu lama, sehingga istri merasa tidak memiliki harapan 
(hopelessness) terhadap masalah dalam rumah tangganya.  
Explanatory style pada tiap-tiap istri berbeda-beda. Ada istri yang memiliki 
Optimistic Explanatory Style dan ada yang Pesimistic Explanatory Style. Menurut Seligman 
(1990) ada tiga dimensi yang digunakan dalam berpikir tentang suatu kejadian. Permanency 
yang membicarakan mengenai waktu berlangsungnya suatu keadaan, menetap (permanance) 
atau sementara (temporary). Pervasiveness, membicarakan mengenai ruang lingkup dari 
suatu peristiwa menyeluruh (universal) atau khusus (specific). Personalization, yaitu 
membicarakan mengenai siapa yang menjadi penyebab suatu keadaan, diri sendiri (internal) 
atau lingkungan (external).  
Karakteristik dari istri yang optimistis, yakin bahwa kondisi tubuhnya yang sehat, 
dukungan dari orang-orang terdekat, pemikiran bahwa keberadaan anak-anak yang menjadi 
sumber kekuatan, pekerjaan yang di miliki saat ini, perilaku suami yang berubah menjadi 
bertanggung  jawab terhadap istri dan anak-anaknya, orientasi mengenai masa depan terhadap 
rumah tangganya hingga dirinya bisa menjalin kehidupan yang lebih sejahtera dan meyusun 
kehidupan yang lebih baik akan berlangsung menetap dengan suami dan anak-anaknya. 
(PmG-permanence) dan kondisi tubuhnya yang kurang sehat, tidak adanya dukungan dari 
orang-orang terdekat, pemikiran bahwa keberadaan anak-anak yang menjadi beban, tidak 
adanya pekerjaan yang di miliki saat ini, perilaku suami yang tidak bertanggung  jawab 
terhadap istri dan anak-anaknya, orientasi mengenai masa depan , dan orientasi mengenai 
masa depan yang suram terhadap kehidupannya bila tanpa suami yang berlangsung  
sementara, serta pemikiran akan berlangsung sementara mengenai ketidak yakinan untuk 
dapat survive bersama anak-anaknya tanpa seorang suami di sisi (istri yang memutuskan 
untuk bercerai) (PmB-temporary), dukungan orang-orang terdekat dan anak sebagai kekuatan 
dapat mempengaruhi seluruh aspek kehidupannya (PvG-universal); dan penilaian, adjusment 
orang lain terhadap kondisi yang terjadi  dalam rumah tangga di pandang istri hanya 
mempengaruhi sebagian kecil aspek kehidupannya (PvB-spesific), dan faktor dalam diri 
sendiri yang berperan besar dalam keberhasilan istri memperbaiki perilaku suami dan 
memperbaiki rumah tangganya jauh lebih baik. (untuk istri yang memutuskan bercerai)  
faktor dalam diri sendiri yang berperan besar dalam keberhasilan dia untuk dapt survive dan 
membesarkan anak walaupun  tanpa suami (PsG-internal) dan Perilaku kasar suami dan 
penelantaran ekonomi tidak di sebabkan karena kesalahan istri namun terdapat faktor lain 
yang mempengaruhinya seperti: krisis ekonomi (PsB-external). Sehingga istri akan menjadi 
Istri yang  pesimis, yakin bahwa dan kondisi tubuhnya yang kurang sehat, kurangnya 
dukungan dari orang terdekat, pemikiran bahwa anak yang menjadi beban, tidak adanya 
pekerjaan untuk memenuhi kebutuhan ekonomi, pemikiran bahwa perilaku suami yang tidak 
bertanggung jawab, dan orientasi mengenai masa depan yang suram terhadap kehidupannya 
bila tanpa suami yang berlangsung  menetap, serta pemikiran akan berlangsung menetap 
mengenai ketidak yakinan untuk dapat survive bersama anak-anaknya tanpa seorang suami di 
sisi (istri yang memutuskan untuk bercerai) ( (PmB-permanence) dan kondisi tubuhnya yang 
sehat, dukungan dari orang-orang terdekat, pemikiran bahwa keberadaan anak-anak yang 
menjadi sumber kekuatan, pekerjaan yang di miliki saat ini, perilaku suami yang berubah 
menjadi bertanggung  jawab terhadap istri dan anak-anaknya, orientasi mengenai masa depan 
terhadap rumah tangganya hingga dirinya bisa menjalin kehidupan yang lebih sejahtera dan 
meyusun kehidupan yang lebih baik akan berlangsung hanya sementara dengan suami dan 
anak-anaknya, dan kemampuan sementara  untuk survive mendidik anak-anaknya walaupun 
tanpa suami (untuk istri yang memutuskan untuk bercerai) (PmG-temporary), penilaian atau 
adjusment orang lain terhadap kondisi yang terjadi  dalam rumah tangga di pandang istri 
hanya mempengaruhi seluruh aspek kehidupannya (PvB-universal) dukungan orang-orang 
terdekat dan anak sebagai kekuatan hanya mempengaruhi aspek kecil dalam kehidupannya 
(PvG-spesific) , Keberhasilan istri dalam memperbaiki perilaku suami dan memperbaiki 
rumah tangganya merupakan faktor dari lingkungan (PsG-external); dan apabila ada 
kegagalan yang terjadi dalam rumah tangganya merupakan kesalahan terbesar berada pada 
dirinya sendiri (PsB-internal). keberhasilan istri memperbaiki perilaku suami dan 
memperbaiki rumah tangganya jauh lebih baik. (untuk istri yang memutuskan bercerai)  
faktor dalam diri sendiri yang berperan besar dalam keberhasilan dia untuk dapt survive dan 
membesarkan anak walaupun  tanpa suami karena pengaruh lingkunagn yang berperan besar 
(PsG-external).  Sehingga istri akan menjadi Pesimistic Explanatory style. 
Faktor-faktor yang mempengaruhi terhadap 
optimis : 
1. Explanatory style ibu/significant 
2. Kritik dari orang dewasa/orang tua 
3. Masa krisis anak-anak 
Dimensi Explanatory Style : 
1. Permanence 
2. Pervasiveness 
3. Personalization 
Berdasarkan uraian diatas, maka dapat ditarik asumsi sebagai berikut : 
1. Istri yang memiliki belief positif, diyakini akan memiliki Optimistic Explanatory Style 
dalam menghadapi peristiwa kekerasan ekonomi yang dilakukan oleh suaminya. 
2. Istri yang memiliki belief negatif, diyakini akan memiliki Pesimistic Explanatory 
Style dalam menghadapi peristiwa kekerasan ekonomi yang dilakukan oleh suaminya. 
3. Istri yang mengalami kekerasan dalam rumah tangga  memiliki Explanatory Style yg 
berbeda-beda. 
4. Istri yang mengalami kekerasan dalam rumah tangga  yang memiliki Optimistic 
Explanatory akan mampu menjalani masalah rumah tangganya sesuai dengan yang di 
harapkan dan mampu menjalani kehidupanya dengan lebih baik. 
5. Istri mengalami kekerasan dalam rumah tangga  yang memiliki Pesimistic 
Explanatory akan mampu menjalani masalah rumah tangganya tidak sesuai dengan 
yang di harapkan. 
2. 1 Explanatory Style 
Explanatory Style adalah pandangan individu dalam menghadapi suatu keadaan, baik 
itu keadaan baik (good situation)  maupun keadaan buruk (bad situation). Keadaan yang 
dimaksud adalah peristiwa-peristtiwa baik maupun buruk yang terjadi dalam kehidupannya. 
Definisi dari kerakteristik individu yang optimis adalah individu yang percaya bahwa 
kekalahan yang dialaminya hanya sementara, terjadi pada peristiwa tertentu saja dan keadaan 
diluar dirinya (lingkungan) yang merupakan penyebab dari terjadinya kekalahan tersebut. 
Individu yang optimis menganggap bahwa situasi buruk yang terjadi merupakan suatu 
tantangan dan individu tersebut akan berusaha keras untuk menghadapi tantangan tersebut. 
Dasar optimisme adalah bagaimana individu berpikir tentang sebab dari suatu keadaan. 
Setiap individu memiliki kebiasaan (habit) dalam berpikir mengenai penyebab dari keadaan 
yang dialaminya. Ini disebut sebagai explanatory style. Explanatory style ini berkembang 
pada masa kanak-kanak dan remaja. Kebiasaan ini akan menetap seumur hidup, meskipun hal 
itu tidak dapat di jelaskan secara eksplisit. 
Ada 3 dimensi yang digunakan dalam berpikir tentang suatu kejadian: 
Dalam dimensi ini di bicarakan mengenai waktu berlangsungnya suatu keadaan. Bila 
seseorang berpikir mengenai dimensi permanence pada keadaan buruk disebut 
permanence bad, sebaliknya bila dalam situasi baik disebut permanence good.  Orang 
yang optimistik bila diperhadapkan dengan situasi buruk akan berpikir bahwa suatu 
kejadian berlangsung sementara (PmB-Temporer) dan pada situasi baik akan 
berlangsung menetap (PmG-Permanence). Orang dengan pesimistik akan menganggap 
bahwa kejadian buruk akan berlangsung lama atau menetap (PmB-permanence) sedang 
kejadian baik berlangsung hanya sementara (PmG-temporer). 
Dimensi kedua ini membicarakan mengenai ruang lingkupnya, dibedakan antara 
universal dan spesifik. Pada keadaan baik, seseorang berpikir tentang dimensi 
pervasivenes good dan sebaliknya pada keadaan buruk individu berpikir mengenai 
pervasiveness disebut pervasiveness bad. Orang yang optimistik akan berpikir bahwa 
keadaan baik akan terjadi secara universal (PvG-Universal) dan keadaan buruk akan 
terjadi secara spesifik (PvB-spesifik). Sedangkan orang yang pesimistik akan berpikir 
bahwa keadaan baik akan terjadi secara spesifik (PvG-spesifik) dan keadaan buruk akan 
terjadi secara universal (PvB-Universal). 
Dimensi ini membicarakan mengenai siapa yang menjadi penyebab suatu keadaan yaitu 
internal (dirinya sendiri) atau eksternal (lingkungan). Jika individu berpikir tentang 
dimensi ini (siapa yang menjadi penyebab) dari keadaan buruk disebut personal bad 
dan jika berpikir mengenai dimensi personalization dari keadaan baik disebut personal 
good. Pada dimensi ini orang dengan optimistik akan berpikir bahwa keadaan baik itu 
disebabkan oleh dirinya sendiri (PsG-Internal) dan keadaan buruk disebabkan oleh hal 
diluar dirinya (PsB-eksternal), sedangkan individu yang pesimistik akan berpikir bahwa 
keadaan buruk disebabkan oleh dirinya sendiri (PsB-internal). 
Dari ketiga dimensi ini diketahui bagaimana harapan seseorang terhadap suatu 
peristiwa, baik dalam keadaan baik maupun dalam keadaan buruk. Harapan tergantung pada 
dimensi permanence dan pervasiveness. Seseorang dikatakan memiliki harapan pada suatu 
keadaan buruk jika individu menjelaskan waktu berlangsungnya keadaan tersebut menetap 
dan terjadi pada seluruh kejadian (universal). Individu seperti ini akan mudah jatuh pada 
kesulitan, karena ia berpikir bahwa keadaan buruk berlangsung dalam waktu yang lama dan 
bersifat mendasar pada seluruh kegiatan. Individu berpikir tidak ada gunanya melakukan 
sesuatu tindakan karena kegiatan buruk menetap, bagaimanapun usahanya dan berpikir 
bahwa usahanya sia-sia. Sebaliknya individu memiliki harapan pada keadaan buruk jika 
berpikir keadaan tersebut terjadi hanya sementara dan pada kegiatan tertentu saja. Harapan 
pada keadaan baik, jika berlangsung menetap dan universal, individu dikatakan kurang 
memiliki harapan pada situasi baik jika individu berpikir bahwa keadaan baik hanya 
sementara dan spesifik Sehingga jelas bagaimana seseorang menjelaskan sebab situasi 
mencerminkan harapan seseorang atau seberapa besar energi yang dimiliki individu tersebut 
untuk menghadapi berbagai situasi. 

