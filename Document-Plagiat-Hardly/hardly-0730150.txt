Penelitian ini dilakukan untuk memperoleh gambaran mengenai derajat 
culture shock pada mahasiswa Buton tingkat I angkatan 2012 di Politeknik  X  
Bandung. Metode penelitian yang digunakan dalam penelitian ini adalah metode 
penelitian deskriptif. Sasaran penelitian ini adalah 36 orang mahasiswa Buton 
tingkat I angkatan 2012 di Politeknik  X  Bandung dan telah menetap di Bandung 
selama maksimal satu setengah tahun. Pengambilan data dilakukan dengan alat 
ukur berupa kuesioner culture shock yang terdiri dari 76 item valid, dengan uji 
validitas berkisar antara 0.326 sampai 0.835 dan reliabilitas 0.969.  
 Berdasarkan hasil penelitian, dari 36 orang mahasiswa Buton tingkat I 
angkatan 2012 di Politeknik  X  Bandung, 47.2% mahasiswa Buton mengalami 
culture shock dengan derajat yang rendah, dimana mereka telah mencapai tahap 
adjustment. Mahasiswa telah mampu beradaptasi dan menerima perbedaan-
perbedaan yang ada. Sebanyak 25% mahasiswa Buton mengalami culture shock 
dengan derajat yang sedang yaitu berada pada tahap recovery dan sebagian 
lainnya yaitu 27.8% mahasiswa Buton mengalami culture shock dengan derajat 
yang tinggi, dimana mereka berada pada tahap crisis. Mereka masih sering 
merasa tidak nyaman karena perbedaan-perbedaan yang ada dan mereka merasa 
tidak mampu menyesuaikan diri dengan lingkungan barunya. 
Dari penelitian ini peneliti mengajukan beberapa saran yaitu bagi Bagian 
Kemahasiswaan Politeknik  X  Bandung, diharapkan hasil penelitian ini dapat 
digunakan sebagai informasi untuk menyusun program bimbingan bagi 
mahasiswa yang berasal dari luar daerah untuk dapat beradaptasi di Bandung. 
Bagi peneliti lain, disarankan meneliti dampak culture shock yang dialami oleh 
mahasiswa baru, atau mengembangkan penelitian dengan menghubungkan 
derajat culture shock dan faktor yang mempengaruhi culture shock. 
 The study was constructed to know the culture shock degree in first year 
Buton students of 2012 in Politeknik  X  Bandung. The method used in this study 
is descriptive research method. The target of this study is 36 Buton student in first 
year of 2012 in Politeknik  X  Bandung and have settled in Bandung maximum 
one a half years. The data was collected with measuring instrument in form of 
questionnaire culture shock, consisting of 76 valid items, with validity between 
0.326 until 0.835 and reliability 0.969. 
 Based on the result of this study, from 36 Buton students in first year of 
2012 in Politeknik  X  Bandung, 47.2% experienced culture shock with low 
degree, who they have been on adjustment stage. They have been able to adjust 
and accept the differences. 25% students experienced culture shock with 
moderate degree, who have been on recovery stage and some other are 27.8% 
students experienced culture shock with high degree, who they have been on crisis 
stage. Buton students often feel unconfortable because the differences and they 
feel not able to adjust in their new environment. 
 From this study the researcher propose some suggestions. For Student 
Center of Politeknik  X  Bandung can use this result as information to arrange a 
program  for students from other Bandung to adjust in Bandung. For the other 
researchers, suggested researching the impact of the culture shock experienced 
by new students, or developing new research by connecting degree of culture 
shock and factors that influence the culture shock. 
KATA PENGANTAR                     .. i 
DAFTAR LAMPIRAN                     . x 
BAB I PENDAHULUAN                    . 1 
BAB II TINJAUAN PUSTAKA                 .. 22 
BAB III METODOLOGI PENELITIAN              . 46 
DAFTAR RUJUKAN                     .. 75 
tidak mampu menyesuaikan diri dengan lingkungan Bandung   .. 
harapan terhadap peran tersebut, nilai yang dianut, perasaan, dan identitas 
perbedaan bahasa, kebiasaan, nilai/norma, dan sopan santun       . 
 Pendidikan merupakan salah satu bidang penting bagi kehidupan manusia. 
Indonesia merupakan salah satu negara yang mengutamakan pentingnya 
pendidikan, dengan ditetapkannya wajib belajar sembilan tahun yang dicanangkan 
sejak tahun 1994 dengan Instruksi Presiden (Inpres) No. 1 tanggal 2 Mei 1994. 
Untuk memperoleh pekerjaan yang diinginkan, pendidikan sampai Sekolah 
Menengah Pertama (SMP) tidaklah cukup. Pendidikan harus diteruskan ke jenjang 
Sekolah Menengah Atas (SMA) dan Perguruan Tinggi (PT).  
Perguruan Tinggi merupakan kelanjutan pendidikan menengah yang 
diselenggarakan untuk menyiapkan peserta didik menjadi anggota masyarakat 
yang memiliki kemampuan akademik yang memadai. Perguruan Tinggi dapat 
berbentuk akademi, sekolah tinggi, institut, universitas, maupun politeknik. 
Setelah individu menyelesaikan pendidikannya di Perguruan Tinggi, diharapkan 
individu tersebut dapat mempraktekkan ilmu yang telah diperoleh ke dalam 
pekerjaan. Melalui pendidikan yang berkualitas akan menghasilkan sumber daya 
manusia yang lebih berkualitas, yang dapat bersaing di era kompetitif seperti 
sekarang ini.  
Pulau Jawa dinilai memiliki perkembangan pendidikan yang lebih pesat 
dibandingkan dengan pulau lainnya sehingga banyak calon mahasiswa tertarik 
melanjutkan pendidikan tingginya di Pulau Jawa. Bandung merupakan salah satu
kota yang dituju mahasiswa dari luar Pulau Jawa, karena Bandung memiliki 
banyak perguruan tinggi, baik universitas maupun politeknik unggul untuk 
dijadikan tempat menimba ilmu.  
 Dari sekian banyak perguruan tinggi, Politeknik  X  menjadi salah satu 
perguruan tinggi yang diminati oleh calon mahasiswa dari luar kota Bandung, 
seperti daerah Sumatera, Sulawesi, Maluku, Papua dan lain-lain. Politeknik ini 
merupakan institusi pendidikan yang menyelenggarakan pendidikan dan pelatihan 
D-I, D-III, dan D-IV yang menghasilkan tenaga ahli madya Sarjana Sains Terapan 
(SST) professional dimana memiliki kompetensi sesuai dengan tuntutan industri 
dan dunia kerja. Politeknik  X  bergerak dalam bidang pendidikan Teknik dan 
Kesehatan, dimana terdapat jurusan Teknik Elektro, Teknik Komputer dan 
Informatika, Teknik Mesin, Teknik Kimia, Teknik Sipil, dan Jurusan Ilmu 
Kesehatan. Masih sedikitnya pendidikan tinggi yang bergerak dalam bidang 
Teknik dan Kesehatan menjadikan Politeknik  X  diminati mahasiswa terutama 
mahasiswa dari luar kota Bandung (www.poltek x .ac.id). Politeknik ini 
bekerjasama dengan P4TK BMTI (Pusat Pengembangan dan Pemberdayaan 
Pendidik dan Tenaga Kependidikan Bidang Mesin dan Teknik Industri) dimana di 
P4TK BMTI secara berkala dilakukan pelatihan bagi guru SMA dan SMK seluruh 
Indonesia dan ketika pelatihan selesai guru-guru tersebut memberikan informasi 
kepada siswa-siswanya mengenai perguruan tinggi di Bandung yaitu Politeknik 
 X  Bandung. Hal ini menjadikan Politeknik  X  Bandung dikenal dan dijadikan 
pilihan sebagai tempat menuntut ilmu bagi mahasiswa luar kota Bandung. 
Berdasarkan data dari Bagian Akademis Politeknik  X , mahasiswa yang 
mendaftar di Politeknik ini bertambah setiap tahunnya. Pada tahun 2012 
mahasiswa baru mengalami peningkatan sebesar 43%, dengan mahasiswa yang 
berasal dari Bandung adalah 124 orang (39%) dan 194 orang (61%) berasal dari 
luar Bandung. Dari 61% mahasiswa yang berasal dari luar Bandung terdapat 19% 
mahasiswa yang berasal dari Buton. 
Menempuh pendidikan di daerah lain merupakan kesempatan yang 
menyenangkan karena memungkinkan seseorang untuk mempelajari berbagai 
macam budaya, namun selain memunculkan perasaan dan situasi yang 
menyenangkan, hidup dalam lingkungan budaya yang baru juga dapat 
memunculkan stress (Ward, Bochner, Furnham, 2001). Seseorang harus 
meninggalkan lingkungan yang familiar untuk kemudian tinggal di lingkungan 
baru yang masih terbilang asing, tidak menutup kemungkinan, orang tersebut akan 
mengalami tekanan yang dapat menimbulkan perasaan yang tidak nyaman. 
Mahasiswa dari daerah lain dituntut untuk menyesuaikan diri secara kultural 
dengan kondisi budaya setempat.  
 Ketika menghadapi budaya baru dalam rangka interaksi antar budaya, 
mahasiswa akan mengalami proses adaptasi untuk melewati masa transisi budaya. 
Proses menyesuaikan diri, adaptasi ketika seseorang masuk dalam lingkungan 
yang baru, lebih dikenal dengan akulturasi. Proses adaptasi ini berjalan secara 
alamiah dan tidak dapat dihindari dimana mahasiswa berusaha untuk mengetahui 
dan memahami tentang budaya dan lingkungan yang baru, namun, proses ini tidak 
dapat sepenuhnya berjalan dengan mulus. Budaya yang baru biasanya dapat 
menimbulkan tekanan karena memahami dan menerima nilai-nilai budaya lain 
adalah sesuatu yang sulit, terlebih jika nilai-nilai budaya tersebut berbeda dengan 
nilai budaya yang mahasiswa tersebut miliki. Mahasiswa akan melalui beberapa 
tahapan sampai ia bisa bertahan dan menerima budaya dan lingkungannya yang 
baru. Tahapan yang pertama adalah honeymoon dimana tahap ini seseorang baru 
saja masuk dalam sebuah lingkungan dan budaya yang baru. Ia akan merasa 
semua hal yang dialaminya sangat indah, menganggap banyak hal baru yang unik 
dan menyenangkan. Perbedaan budaya yang ada dianggap sebagai sesuatu yang 
menarik. Tahap selanjutnya adalah ketika seseorang merasa bahwa ternyata apa 
yang dialaminya pada lingkungan yang baru tidak sesuai dengan apa yang 
dibayangkan sebelumnya. Individu merasa apa yang terjadi saat itu sangat tidak 
sesuai dengan dirinya, pada tahap inilah terjadi culture shock. 
Culture shock menggambarkan sesuatu yang negatif dan menimbulkan 
aksi yang dialami oleh individu ketika berpindah ke lingkungan yang baru dan 
berbeda dengan lingkungan asalnya (Oberg, 1960). Culture shock pada umumnya 
dialami oleh pendatang 6 bulan sampai 1 tahun 6 bulan dari kedatangannya 
(Ward, Bochner, Furnham, 2001). Culture shock terjadi karena kecemasan yang 
ditimbulkan oleh adanya kehilangan petunjuk yaitu tanda dan lambang-lambang 
dalam pergaulan sosial. Petunjuk tersebut dapat berbentuk kata-kata, ekspresi 
wajah, kebiasaan seperti cara-cara yang dilakukan dalam sehari-hari, kapan 
berjabat tangan dan apa yang harus dikatakan bila bertemu dengan orang lain. 
Bila seseorang memasuki suatu budaya asing, semua atau hampir semua 
kebiasaan dari tempat asalnya hilang karena perbedaan kebiasaan. Individu akan 
kehilangan pegangan lalu mengalami frustrasi dan kecemasan. Awalnya individu 
akan menolak lingkungan yang menyebabkan ketidaknyamanan (Mulyana dan 
Rakhmat, 2001).  
Mahasiswa Buton yang berasal dari Pulau Buton, Sulawesi Tenggara 
memiliki bahasa daerah Buton yang biasa mereka gunakan sehari-hari. Mereka 
berbicara dengan cepat, tidak suka basa-basi, namun mereka sangat memegang 
tradisi dan agama. Karakter budaya dan pola pikir dari masyarakatnya cerdas, 
inovatif, dan mampu bertahan (Ahmadi, 2009). Di Bandung masyarakatnya 
terbiasa menggunakan bahasa Sunda dan Sunda dikenal dengan budaya yang 
menjunjung tinggi sopan santun, masyarakatnya dikenal sebagai masyarakat yang 
lembut. Kecenderungan ini tampak sebagaimana dalam pameo silih asih, silih 
asah, silih asuh; saling mengasihi (mengutamakan sifat welas asih), saling 
menyempurnakan atau memperbaiki diri (melalui pendidikan dan berbagi ilmu), 
dan saling melindungi (saling menjaga keselamatan). Perbedaan yang dialami 
mahasiswa Buton tingkat I angkatan 2012  di Politeknik  X  Bandung yang dapat 
menyebabkan terjadinya culture shock diantaranya adanya perbedaan dalam hal 
makanan, tipe perilaku, bahasa, kesempatan untuk melakukan kontak sosial, dan 
cara berbicara  (J.P.Spradley and M. Phillips (1972) dalam Ward, Bochner, 
Furnham, 2001,p.74). 
Maria Wisnu Kanita (2012) dalam jurnalnya yang berjudul Coping 
Mechanism Concerned with Culture Differences of Outer Region Students in The 
First Year (http://ejournal-s1.undip.ac.id/index.php/jnursing/article/view/122/129) 
mengungkapkan perbedaan bahasa menjadi kendala yang dialami mahasiswa dari 
luar Pulau Jawa, seperti yang diungkapkan beberapa mahasiswa yang berasal dari 
luar Pulau Jawa di Semarang, mereka merasa sulit berkomunikasi dengan teman 
dan warga yang berasal dari Semarang, karena mereka berbicara dengan 
menggunakan bahasa daerah setempat. Hal ini membuat beberapa dari mereka 
memilih diam, sehingga kurang dapat bersosialisasi dengan penduduk sekitar. Hal 
serupa juga diungkapkan oleh Oberg (dalam Sodjakusumah, 1996) yang 
menyatakan bahwa dampak negatif dari culture shock yang dialami oleh 
mahasiswa baru di New Zealand adalah masalah akademis (termasuk didalamnya 
perbedaan bahasa dan sistem pembelajaran), masalah sosial (tidak bisa 
berinteraksi dengan lingkungan sekitar), dan masalah pribadi (karena merasa 
sendiri dan rindu rumah atau daerah asalnya). 
Culture shock merupakan fenomena yang akan dialami oleh setiap orang 
yang melintasi dari suatu budaya ke budaya lain sebagai reaksi ketika berpindah 
dan hidup dengan orang-orang yang berbeda pakaian, rasa, nilai, bahkan bahasa 
dengan yang dipunyai oleh orang tersebut (Littlejohn, 2004). Chapdelaine (2004) 
menemukan bahwa hal yang mendasari munculnya culture shock  adalah adanya 
kesulitan-kesulitan sosial antara individu tersebut dengan penduduk asli dari 
negara yang didatanginya. Penelitan Chapdelaine pada siswa pria di Universitas 
Internasional Canada memberikan hasil bahwa culture shock yang dialami oleh 
individu berhubungan negatif dengan tingkat interaksi individu dengan penduduk 
asli. Semakin sering interaksi dengan penduduk asli maka semakin rendah culture 
shock yang dialami individu, sebaliknya, semakin jarang atau kurang interaksi 
dengan penduduk asli, culture shock yang dialami semakin tinggi. 
 Berdasarkan wawancara yang dilakukan kepada 5 orang mahasiswa Buton 
di Politeknik  X  Bandung, diperoleh data bahwa ketika kelima mahasiswa 
tersebut ke Bandung, mereka harus menyesuaikan diri dengan bahasa, cara 
berbicara, cuaca, makanan, teman baru, dan situasi sekitar tempat tinggal mereka. 
Berbagai perbedaan antara budaya Buton dan Sunda menjadi salah satu pemicu 
ketidaknyamanan yang umumnya dirasakan oleh mahasiswa Buton. Lima orang 
(100%) mahasiswa mengalami kesulitan dengan bahasa di Bandung. Mahasiswa 
Buton tidak mengerti bahasa Sunda dan sering mengakibatkan terjadinya masalah 
komunikasi. Tak jarang dosen yang mengajar berbicara dengan bahasa Sunda dan 
mahasiswa Buton menjadi tidak mengerti. Mereka mencoba mengerti bahasa 
Sunda dengan menerka-nerka arti pembicaraan dari mimik wajah. Kesulitan 
dalam memahami bahasa setempat membuat mahasiswa Buton merasa kurang 
percaya diri untuk memulai pembicaraan dengan orang-orang sekitar di 
lingkungan kampus maupun di tempat kos (aspek 5). 
Dari cara berbicara mahasiswa Buton merasa masyarakat Sunda berbicara 
dengan suara yang pelan, sedangkan mereka memiliki suara yang keras dengan 
tempo yang cepat. Mereka merasa cukup kesulitan karena harus mengatur cara 
bicara agar lebih lambat saat berbicara dengan masyarakat Sunda. Tiga orang 
(60%) mahasiswa pernah merasa kesal karena lawan bicara meminta mereka 
untuk mengulang-ulang apa yang mereka bicarakan. Hal ini membuat mereka 
cepat marah (aspek 1) dan memilih untuk menghindari interaksi dengan 
masyarakat Sunda (aspek 4). 
Mahasiswa Buton juga merasa kurang cocok dengan cuaca di Kota 
Bandung. Dari kelima mahasiswa yang diwawancarai, empat orang (80%) 
mahasiswa Buton merasa cuaca di Bandung lebih sejuk dibandingkan di Buton. 
Untuk beberapa hari mereka sakit flu dan hal tersebut cukup mengganggu 
aktivitas perkuliahan mereka. 

