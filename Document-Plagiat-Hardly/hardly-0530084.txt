. Intention sendiri 
diasumsikan sebagai penghubung langsung dari perilaku dan mengarahkan 
perilaku yang dikontrol dan disengaja. Semakin kuat intention untuk 
menampilkan suatu perilaku, semakin mungkin perilaku tersebut dilakukan. 
Intention dari suatu perilaku hanya dapat muncul jika individu dapat memutuskan 
keinginannya untuk melakukan atau tidak melakukan suatu perilaku (volitional 
control). Jika seandainya ada actual control dalam derajat yang cukup terhadap 
perilaku, individu diharapkan dapat melaksanakan intention-nya pada saat 
kesempatan muncul. Secara garis besar, semakin favourable attitude toward the 
behavior dan semakin positif subjective norms serta perceived behavioral control 
terhadap perilaku dalam memprediksi intention, diharapkan intention dapat 
berubah sesuai dengan perilaku dan situasinya.     
 Attitudes Towards the Behavior adalah sikap favorable/ unfavorable 
terhadap evaluasi positif atau negatif individu terhadap menampilkan suatu 
perilaku. Evaluasi seseorang terhadap suatu objek dapat terbentuk dari beliefs 
yang dipegang oleh orang itu mengenai objek tersebut. Ide inilah yang diterapkan 
pada pembentukan dari attitudes toward the behavior. Dalam membicarakan 
attitudes toward the behavior, setiap belief mengaitkan perilaku dengan 
konsekuensi tertentu atau dengan beberapa atribut lain seperti keuntungan atau 
kerugian yang berhubungan dengan pelaksanaan perilaku. Karena atribut yang 
menyertai perilaku sudah dinilai positif atau negatif, individu secara otomatis 
akan memiliki penilaian tersendiri terhadap suatu perilaku. Dengan cara itu 
individu belajar untuk menyukai perilaku yang dipercaya mempunyai konsekuensi 
yang ia inginkan dan individu tersebut membentuk sikap negatif terhadap perilaku 
yang ia asosiasikan dengan konsekuensi yang tidak ia inginkan. Menurut teori 
planned behavior, attitudes toward the behavior ditentukan oleh sejumlah 
keyakinan mengenai konsekuensi dari menampilkan suatu perilaku. Keyakinan ini 
disebut behavioral beliefs. Setiap behavioral beliefs mengkaitkan suatu perilaku 
pada outcome tertentu. Attitudes toward the behavior ditentukan oleh evaluasi 
seseorang mengenai outcomes yang diasosiasikan dengan perilaku dan kekuatan 
asosiasi tersebut. 
 Evaluasi dari setiap outcomes yang diasosiasikan dengan suatu perilaku 
berkontribusi pada attitude dalam proporsi pada kemungkinan subjektif seseorang 
bahwa suatu perilaku akan menghasilkan outcome tertentu. Jika behavioral belief 
strength dikalikan dengan outcome evaluation maka akan diperoleh perkiraan dari 
attitudes toward the behavior, sebuah perkiraan yang berdasarkan pada sejumlah 
beliefs mengenai suatu perilaku. Hal ini dapat digambarkan melalui simbol dalam 
persamaan berikut ini : 
AB ? ? biei 
 AB adalah attitudes toward the behavior B; bi adalah behavioral belief 
bahwa menampilkan perilaku B akan mengarahkan pada outcome i; ei adalah 
evaluasi dari outcome i (outcome evaluation). Dapat terlihat, secara umum 
seseorang yang yakin bahwa menampilkan suatu perilaku akan mengarahkan pada 
outcomes yang positif juga akan memiliki attitudes toward the behavior yang 
favourable terhadap perilaku tersebut. Begitu juga sebaliknya, seseorang yang 
yakin bahwa menampilkan suatu perilaku akan mengarahkan pada outcomes yang 
negatif juga akan memiliki attitude yang unfavourable terhadap perilaku tersebut. 
 Determinan kedua dari intention adalah subjective norms. Subjective 
norms adalah persepsi individu mengenai tuntutan dari orang-orang yang 
signifikan baginya (important others) untuk menampilkan atau tidak 
menampilkan suatu perilaku dan kesediaan individu untuk mematuhi tuntutan 
tersebut. Subjective norms terbentuk dari hasil perkalian antara normative beliefs 
dengan motivation to comply. Normative beliefs adalah keyakinan individu bahwa 
important others menuntut atau tidak menuntut individu untuk menampilkan 
suatu perilaku. Pada beberapa penelitian, yang termasuk important others adalah 
orang tua, pasangan hidup, teman dekat dan rekan kerja. Selain itu yang termasuk 
dalam important others juga tergantung pada perilaku yang sedang diteliti. Secara 
umum orang yang yakin bahwa kebanyakan important others yang memotivasi 
mereka untuk patuh berpikir bahwa orang tersebut harus menampilkan suatu 
perilaku, maka orang itu akan mempersepsi adanya tuntutan untuk menampilkan 
perilaku tersebut dan begitu juga sebaliknya. 
 Hubungan antara normative beliefs dan subjective norms digambarkan 
secara simbolis dalam persamaan berikut : 
SN ? ? nimi 
 SN adalah subjective norms; ni adalah normative belief yang 
mempertimbangkan important others i; mi adalah motivasi untuk mematuhi 
important others i (motivation to comply). Secara umum seseorang yang yakin 
bahwa important others menuntut ia untuk melakukan suatu perilaku dan ia 
memiliki motivasi untuk mengikuti tuntutan tersebut, maka individu akan 
memiliki subjective norms yang positif. Demikian juga sebaliknya, seseorang 
yang yakin bahwa important others tidak menuntut dia untuk melakukan suatu 
perilaku dan ia bermotivasi untuk mengikuti tuntutan tersebut, maka individu 
akan memiliki subjective norms yang negatif terhadap perilaku tersebut.  
 Determinan ketiga dari intention adalah perceived behavioral control, 
yaitu persepsi individu mengenai kemampuan mereka untuk menampilkan suatu 
perilaku. Perceived Behavioral Control juga diasumsikan merupakan fungsi dari 
sejumlah beliefs yaitu beliefs mengenai ada atau tidaknya faktor-faktor yang dapat 
mendukung atau menghambat dalam menampilkan suatu perilaku tertentu. Beliefs 
ini dapat didasari sebagian oleh pengalaman masa lalu, tetapi juga biasanya 
dipengaruhi juga oleh informasi tidak langsung mengenai suatu perilaku, dengan 
cara mengobservasi pengalaman teman. Selain itu juga dipengaruhi oleh faktor 
lain yang meningkatkan atau menurunkan persepsi mengenai kesulitan untuk 
menampilkan suatu perilaku tertentu. Semakin tersedia sumber daya dan 
kesempatan dan semakin sedikit hambatan yang mereka antisipasi, semakin besar 
persepsi mereka bahwa mereka dapat mengontrol atau melakukan perilaku 
tersebut. Beliefs mengenai hal ini disebut control beliefs. 
 Secara keseluruhan control beliefs mengarah pada persepsi bahwa 
seseorang memiliki atau tidak memiliki kemampuan untuk menampilkan suatu 
perilaku. Persamaan di bawah ini menggambarkan hubungan antara control 
beliefs dan perceived behavioral control. 
PBC ? ? pici 
 PBC adalah perceived behavioral control; ci adalah control beliefs bahwa 
faktor i akan muncul; pi adalah kekuatan faktor i untuk memfasilitasi atau 
menghambat munculnya suatu perilaku (power of control factor). Secara umum, 
seseorang yang yakin bahwa terdapat faktor-faktor yang mendukung dalam 
menampilkan suatu perilaku dan faktor-faktor memfasilitasi munculnya perilaku 
tersebut, maka individu akan memiliki perceived behavioral control yang positif. 
Demikian pula sebaliknya seseorang yang yakin bahwa faktor-faktor tidak 
memfasilitasi dia untuk melakukan suatu perilaku maka individu akan memiliki 
perceived behavioral control yang negatif terhadap perilaku tersebut. 
Attitude toward the behavior, subjective norms dan perceived behavioral control 
memberikan pengaruh pada terbentuknya intention untuk melakukan suatu 
perilaku. Attitude toward the behavior, subjective norms dan perceived behavioral 
control yang positif cenderung mengarah pada intentions yang kuat. Sebaliknya, 
Attitude toward the behavior, subjective norms dan perceived behavioral control 
yang negatif cenderung mengarah pada intention yang lemah. Attitude toward the 
behavior, subjective norms dan perceived behavioral control dalam diri seseorang 
dapat berbeda-beda kekuatannya. Sebagai contoh, seseorang dapat memiliki 
attitude toward the behavior yang positif terhadap suatu perilaku namun memiliki 
subjective norms dan perceived behavioral control yang negatif. Demikian pula, 
seseorang dapat memiliki attitude toward the behavior yang negatif namun 
memiliki subjective norms dan perceived behavioral control yang positif.  
Pengaruh ketiga determinan ini dapat berbeda-beda kekuatannya, 
tergantung determinan apa yang dianggap paling penting. Jika seseorang 
menganggap bahwa subjective norms adalah determinan yang paling penting 
dalam mempengaruhi intention, maka subjective norms akan memberikan 
pengaruh paling besar terhadap intention dibandingkan dengan pengaruh 
determinan lain. Artinya apabila subjective norms memberikan pengaruh paling 
kuat terhadap intention, maka hal ini sudah dapat memprediksi gambaran 
intention. Sebagai contoh, apabila subjective norms yang dimiliki indivdiu 
terhadap suatu perilaku adalah negatif, walaupun attitude toward the behavior dan 
perceived behavioral control-nya positif, intention untuk melakukan perilaku 
tersebut menjadi lemah karena subjective norms merupakan determinan yang 
memberikan pengaruh paling kuat terhadap intention.    
 Attitude toward the behavior, subjective norms dan perceived behavioral 
control saling berhubungan satu sama lain. Attitude toward the behavior, 
subjective norms dan perceived behavioral control dapat memiliki hubungan yang 
positif atau negatif. Sebagai contoh, jika dalam diri seseorang memiliki korelasi 
yang positif antara subjective norms dan attitude toward the behavior, artinya 
apabila seseorang memiliki persepsi mengenai tuntutan dari orang-orang yang 
penting baginya dan memiliki kesediaan untuk mematuhi tuntutan tersebut maka 
ia akan memiliki sikap favourable terhadap evaluasi positif pada perilaku tersebut, 
demikian juga sebaliknya. Jika seseorang memiliki persepsi bahwa orang-orang 
yang penting baginya tidak menuntut dan dia memiliki kesediaan untuk mematuhi 
orang-orang tersebut, maka ia akan memiliki sikap yang unfavourable terhadap 
perilaku tersebut. Jika seseorang memiliki korelasi yang negatif antara subjective 
norms dan attitude toward the behavior, artinya seseorang memiliki persepsi 
bahwa orang-orang yang penting baginya tidak menuntut untuk menampilkan 
suatu perilaku dan ia memiliki kesediaan untuk mematuhi tuntutan tersebut maka 
sikapnya akan unfavourable terhadap evaluasi pada perilaku tersebut, begitu pula 
sebaliknya. Jika seseorang memiliki persepsi bahwa orang-orang yang penting 
baginya kurang menuntut dan ia memiliki kesediaan untuk mematuhi tuntutan 
tersebut maka ia akan memiliki sikap yang favourable terhadap evaluasi perilaku. 
 Menurut teori planned behavior, tiga determinan dari intention dibentuk 
oleh behavioral beliefs, normative belief dan control belief. Ada banyak variabel 
yang dapat berhubungan atau mempengaruhi beliefs yang dipegang oleh 
seseorang, misalnya umur, gender, suku, status sosial ekonomi, pendidikan, 
kebangsaan, agama, kepribadian, emosi, sikap secara keseluruhan dan nilai-nilai, 
kecerdasan, keanggotaan dalam suatu kelompok, masa lalu, informasi, dukungan 
sosial, kemampuan mengatasi masalah dan lain-lain. Hal inilah yang disebut 
dengan background factors. Tentunya orang yang tumbuh di lingkungan sosial 
yang berbeda dapat memperoleh informasi yang berbeda mengenai masalah-
masalah yang berbeda. Informasi-informasi tersebut dapat menjadi dasar dari 
beliefs mengenai konsekuensi dari perilaku (behavioral belief), tuntutan sosial 
dari important others (normative belief) dan mengenai rintangan-rintangan  yang 
dapat mencegah mereka untuk menampilkan suatu perilaku (control belief). 
Semua faktor-faktor tersebut, dapat mempengaruhi behavioral, normative dan 
control beliefs dan sebagai akibatnya mempengaruhi intention dan perilaku.      
   Background factors yang telah disebutkan diatas dibagi menjadi tiga 
kategori yaitu personal, sosial dan informasi. Walaupun background factors dapat 
memberikan pengaruh secara nyata terhadap behavioral, normative atau control 
beliefs tidak terdapat hubungan yang terlalu erat antara background factors dan 
beliefs. Banyaknya hal-hal yang termasuk dalam background factors 
menjadikannya sulit untuk menentukan mana yang akan dipergunakan dalam 
suatu penelitian. Jika terdapat teori yang dapat mengarahkan dalam pemilihan 
background factors suatu perilaku, hal tersebut akan menjadikannya lebih mudah 
untuk menentukan background factors mana yang akan digunakan.  

