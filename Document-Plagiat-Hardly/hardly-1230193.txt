Sumber Daya Manusia (SDM) menjadi sorotan bagi organisasi untuk tetap dapat 
bertahan di era globalisasi. SDM mempunyai peran utama dalam setiap kegiatan perusahaan. 
Walaupun didukung dengan sarana dan prasarana serta sumber dana yang memadai, tetapi 
tanpa dukungan SDM kegiatan perusahaan tidak akan terselesaikan dengan baik. Hal ini 
menunjukkan bahwa SDM merupakan faktor terpenting dalam usaha pencapaian 
keberhasilan dalam persaingan dunia bisnis di era globalisasi ini. Para pelaku bisnis atau 
perusahaan harus dapat mengerahkan SDM yang ada untuk terus berusaha sekuat tenaga dan 
semaksimal mungkin agar dapat meraih posisi yang baik dalam persaingan ekonomi. SDM 
bukan semata-mata obyek dalam pencapaian tujuan, tapi lebih dari itu SDM adalah obyek 
pelaku. Tanpa SDM, perusahaan dan organisasi tidak dapat mewujudkan semua rencana yang 
telah dibuatnya. 
Dalam mencapai tujuan dari setiap perusahaan, selain SDM yang menjadi salah satu 
faktor penentu dalam persaingan bisnis sekarang ini namun juga perlu diperhatikan teknologi 
yang semakin canggih. Para pelaku bisnis harus dapat mengikuti perkembangan teknologi 
tersebut, dan semua itu kembali lagi pada kualitas SDM yang terdapat di dalam sebuah 
perusahaan. Hal yang dikehendaki pemilik perusahaan atau pelaku bisnis adalah memiliki 
karyawan yang tidak hanya dituntut untuk melakukan pekerjaan sesuai dengan job 
description yang diberikan secara efektif. Efektif disini berarti para karyawan sudah 
menjalankan job description sesuai dengan apa yang tertulis dan menjalankan sesuai aturan 
yang ada, namun karyawan juga diharapkan untuk memiliki perilaku yang dapat membantu 
rekan kerja lainnya yang mengalami kesulitan dalam bekerja serta saling bekerja sama. 
Kerjasama dan koordinasi yang baik merupakan bagian terpenting dalam tugas utama 
karyawan, sehingga dapat mencapai kinerja yang optimal dan tugas-tugas pun dapat 
terselesaikan dengan baik dan tepat waktu dalam usaha mencapai target dan harapan yang 
ditetapkan perusahaan. 
Salah satu perusahaan yang mengikuti perkembangan bisnis era globalisasi dan terus 
berusaha untuk meningkatkan kualitas dan kuantitas SDM-nya adalah PT  X . PT  X  
merupakan salah satu pelaku ekonomi yang bergerak di bidang industri manufaktur tas dan 
dompet di kota Bandung. Mayoritas tas dan dompet hasil produksi PT  X  berbahan dasar 
kain polyester, sehingga sangat cocok untuk digunakan sebagai tas sekolah, tas travel, tas 
keperluan bayi, tas olahraga, dan lain-lain. PT  X  memproduksi berbagai macam jenis tas 
yang selalu mengikuti trend terkini. Berawal dari sebuah home industri yang berdiri pada 
tahun 1985 dalam memproduksi bermacam-macam tas. Pada tahun 2012 PT  X  berkembang 
menjadi Perseroan Terbatas (PT), yang terus meningkatkan kualitas produksinya. PT  X  
dapat memproduksi tas dan dompet sesuai dengan desain dan spesifikasi yang diinginkan 
oleh konsumen. 
Sebagian besar konsumen merupakan korporasi pemegang lisensi berbagai brand 
ternama di Indonesia, diantaranya PT Mamagreen Pasific, PT Sophie Paris Indonesia, PT 
Hini Daiki Indonesia, PT Joma Indonesia Perkasa, PT Mitra Adiperkasa, TBK., PT Creativity 
Makmur Sejahtera, PT Hace Sunrise, PT Chung Sun Indonesia, PT Mitra Garindo Perkasa, 
PT Polyfilatex, CV. Penta, PT MAP Aktif Adiperkasa, dan lain-lain. Kapasitas produksi rata-
rata PT  X  adalah sekitar 15.000 tas per bulan, dan terus bertambah seiring dengan 
berkembangnya perusahaan. Dalam menunjang proses produksi selain didukung oleh 
teknologi yang terkini, PT  X  juga memiliki jaringan material supplier yang handal. Selain 
itu PT  X  juga membuka permintaan material sesuai yang diinginkan oleh konsumen. 
Visi dan misi PT  X  adalah menjadi perusahaan manufaktur terkemuka di Indonesia 
dengan memberikan kualitas tinggi, layanan yang baik, dan kepuasan pelanggan. PT  X  
berkomitmen untuk memastikan kepuasan pelanggan dengan mengembangkan produk dan 
layanan untuk memenuhi kebutuhan dan harapan pelanggan. PT  X  merupakan perusahaan 
keluarga yang memiliki karyawan yang berjumlah 348 orang. Pembagian tugas di PT  X  ini 
dibagi menjadi beberapa bagian yaitu HRD, Accounting & Finance, Purchasing, Bagian 
Produksi dan Warehouse. Di dalam bagian tersebut terdapat warehouse yang fungsinya 
paling berpengaruh pada proses produksi. Bagian warehouse terdiri dari posisi receiving, 
head warehouse, admin warehouse, warehouse accessories, dan warehouse bahan.  
Karyawan warehouse di PT  X  terdiri dari 30 orang dan sudah berstatus sebagai 
karyawan tetap. Tugas karyawan receiving adalah menerimaan dan mencek material serta 
tanda terima barang masuk dari supplier. Tugas head warehouse adalah bertanggung jawab 
atas seluruh bahan baku dan material, melakukan pembelian bahan baku kepada supplier, 
melakukan pencatatan bahan baku serta material jadi yang masuk dan keluar, dan menuliskan 
kebutuhan produksi pada papan proyek (schedule board). Tugas karyawan admin warehouse 
adalah mencatat jumlah material yang diterima dan yang keluar dan melaporkan hasil 
pemasukan dan pengeluaran material pada bagian accounting untuk disesuaikan. Tugas 
karyawan warehouse accessories dan warehouse bahan antara lain mencatat penerimaan dan 
pengeluaran material, menyimpan material di gudang, mencatat distribusi material untuk 
produksi, dan menerbitkan laporan jumlah persediaan untuk keperluan operasional (stock 
opname). 
Karyawan warehouse memiliki tanggung jawab yang besar akan ketersediaan barang 
untuk proses produksi. Karyawan warehouse perlu bekerja dengan teliti dan bekerja tanpa 
menggunakan alat atau masih dikatakan manual untuk menghitung setiap material yang ada. 
Proses kerja yang manual menyebabkan cukup banyak kesalahan yang dilakukan oleh 
karyawan sendiri (human error) seperti kesalahan dalam menghitung jumlah material yang 
keluar   masuk serta ketidakcocokan jumlah fisik material yang ada dengan catatan yang 
dimiliki oleh admin warehouse. Adanya human error pada karyawan warehouse menjadi 
masalah besar dalam pemenuhan target produksi sehingga terjadinya keterlambatan 
pengiriman barang ke tangan konsumen. Hal ini berdampak pada waktu bekerja karyawan 
warehouse yang pada umumnya memulai bekerja pada pukul 07.30 hingga 16.00, karena 
terjadinya keterlambatan pengiriman barang ke tangan konsumen maka karyawan warehouse 
sering melakukan lembur atau bekerja diluar jam kerja. Selain itu, karyawan warehouse 
selalu diminta untuk datang ke perusahaan pada musim libur panjang yang telah ditentukan 
oleh perusahaan untuk menyelesaikan tugas yang belum selesai, seperti mengitung stock 
opname atau menghitung seluruh stock material yang tersedia di gudang serta mencari selisih 
dari material.  
Prosedur kerja karyawan warehouse berawal dari karyawan receiving yang menerima 
dan mencek material serta tanda terima barang masuk dari supplier, kemudian material 
accessories diambil oleh karyawan warehouse accessories, sedangkan material bahan 
diambil oleh karyawan warehouse bahan untuk dihitung jumlah material yang diterima sesuai 
dengan pesanan, lalu menyerahkan tanda terima barang masuk sebagai arsip dan tanda terima 
asli kepada admin warehouse. Namun pada kenyataannya karyawan warehouse accessories 
sering tidak menghitung ulang dan mencatat mengenai jumlah material yang keluar masuk 
sehingga menyebabkan selisih pada pencatatan stock material. Hal tersebut disebabkan oleh 
banyaknya permintaan material accessories untuk proses produksi yang tidak teratur. Dengan 
begitu membuat karyawan warehouse accessories tidak mencatat secara rinci pengeluaran 
material dari gudang berdasarkan bon permintaan material untuk produksi. Ketika bagian 
produksi meminta material kepada head warehouse untuk keperluan produksi, disinilah 
terjadi misscommunication dimana data jumlah material yang dimiliki admin warehouse 
berbeda dengan jumlah fisik material yang tersedia untuk proses produksi. Begitu pula pada 
head warehouse yang tidak pernah menuliskan kebutuhan produksi pada papan proyek 
(schedule board). 
Kurangnya jumlah fisik material menyebabkan bagian purchasing untuk kembali 
memesan material yang kurang tersebut. Namun karena pekerjaan yang overload dan 
membutuhkan material segera untuk proses produksi maka menyebabkan head warehouse 
harus turun tangan dalam memesan material yang dibutuhkan kepada supplier. Kekurangan 
jumlah fisik material dipengaruhi juga oleh kelalaian dari head warehouse yang sangat jarang 
mencek stock material accessories serta tidak memperhatikan dan menjaga jumlah stock 
material accessories yang digunakan sebagai stock cadangan untuk disimpan dan dapat 
digunakan jika dalam keadaan terdesak. Hal ini sering kali menjadi sumber terhambatnya 
proses produksi yang akan dilaksanakan. Bagian produksi menjadi mengulur waktu dalam 
memproduksi pesanan dan akhirnya terjadi keterlambatan dalam pengiriman pesanan ke 
tangan konsumen. Keterlambatan dalam pengiriman barang ke tangan konsumen membuat 
PT  X  mendapatkan sanksi berupa denda yang perlu dibayar kepada konsumen.  
Pimpinan PT  X  sering kali bertindak dan memberi tanggapan dalam mengatasi 
keterlambatan produksi pesanan. Tak jarang pimpinan langsung memberikan teguran keras 
pada head warehouse dan warehouse accessories dengan mengumpulkan seluruh karyawan 
untuk mengadakan meeting dengan tujuan untuk membahas permasalahan yang terjadi. 
Pimpinan PT  X  menyadari pentingnya fungsi dari karyawan warehouse yang merupakan 
salah satu  ujung tombak  dari keberhasilan produksi. Dengan begitu pimpinan pun tidak 
dengan mudah dapat memberhentikan karyawan warehouse accessories yang lalai karena 
akan memungkinkan muncul masalah baru di gudang seperti semakin banyak pekerjaan yang 
terbengkalai dan dampaknya akan kembali menghambat proses produksi.  
Dengan kondisi yang terjadi, karyawan receiving dan karyawan warehouse bahan 
sering membantu tugas dari karyawan warehouse accessories. Dengan pertimbangan 
karyawan warehouse bahan tidak memiliki kesulitan dalam menyelesaikan tugasnya, dimana 
karyawan warehouse bahan hanya memeriksa dan mencatat material tertentu sesuai pesanan 
produksi yang dimana jumlahnya  100 jenis dan berupa roll-an kain sehingga akan lebih 
mudah untuk dihitung. Sedangkan karyawan receiving hanya menerima barang dari supplier, 
sehingga karyawan warehouse bahan dan receiving memiliki waktu luang lebih banyak. 
Alasan karyawan warehouse accessories sering menghambat jalannya produksi adalah 
karena karyawan warehouse accessories mengalami kesulitan dalam melakukan perhitungan 
material yang berjumlah  300 dan terdiri dari beberapa item dengan berbagai macam tipe 
sehingga lebih kompleks dan memerlukan ketelitian. Dengan begitu karyawan warehouse 
accessories mengalami kesulitan dalam melakukan pencatatan, yang disebabkan karena 
jumlah item dari accessories yang banyak dan beragam serta banyaknya permintaan material 
untuk proses produksi yang tidak teratur.  
Melihat kondisi seperti ini, pimpinan PT  X  tidak hanya mengharapkan karyawan 
dapat melaksanakan pekerjaan sesuai dengan yang tercantum pada job description, namun 
karyawan juga diharapkan menunjukkan perilaku melebihi tugas atau peranannya 
berdasarkan inisiatif dan diharapkan memberikan kontribusi lebih bagi perusahaan. 
Memberikan kontribusi lebih bagi perusahaan bukan karena digerakkan oleh hal-hal yang 
menguntungkan bagi karyawan, namun karyawan memiliki perasaan yang puas apabila 
melakukan hal tersebut. Maka kondisi tersebut bisa disebut sebagai perilaku 
kewarganegaraan organisasi atau juga disebut organizational citizenship behavior (OCB).  
OCB menurut (Organ, 2006) merupakan perilaku membantu pada individu yang 
dilakukan atas kemauannya sendiri, meskipun tidak tercantum dalam job description, dan 
tidak berkaitan secara langsung atau eksplisit dengan sistem reward, yang pada dasarnya 
dapat meningkatkan berfungsinya organisasi secara efektif dan efisien. Menurut Podssakoff, 
MacKenzie, Moorman & Fetter,1990 (dalam Organ, 2006) terdapat lima dimensi perilaku 
yaitu: altruism (perilaku menolong yang dilakukan oleh karyawan kepada rekan kerja, untuk 
tugas-tugas yang berkaitan dengan organisasional), conscientiousness (melakukan hal-hal 
yang menguntungkan organisasi melebihi dari apa yang dipersyaratkan dalam kehadiran, 
kepatuhan pada peraturan, menfaatkan waktu luang, berisi tentang kinerja yang 
mempersyaratkan peran yang melebihi standard minimum), sportsmanship (kesediaan 
individu untuk toleransi pada iklim kerja tanpa disertai dengan keluhan), courtesy (perilaku 
individu yang mencegah penyebab masalah dalam pekerjaan yang berkaitan dengan 
pekerjaan orang lain), civic virtue (tingkah laku individu yang menggambarkan keterlibatan 
dan kepedulian individu terhadap kelangsungan hidup perusahaan). Dengan begitu karyawan 
warehouse membutuhkan perilaku organizational citizenship behavior (OCB) untuk 
mengatasi keterlambatan pesanan ke tangan konsumen.  
Berdasarkan hasil survei awal yang dilakukan kepada 10 orang terhadap karyawan 
warehouse. Maka diperoleh data tujuh orang (70%) karyawan mengatakan bersedia untuk 
membantu rekan kerja yang sedang mengalami overload dalam pekerjaan secara sukarela dan 
tiga orang (30%) lainnya mengatakan bersedia untuk membantu rekan kerja jika dimintai 
bantuan atau diperintahkan. Selanjutnya enam orang (60%) karyawan mengaku sering 
terlambat datang karena berbagai alasan eksternal dan empat orang (40%) lainnya berusaha 
untuk datang tepat waktu. Selanjutnya enam orang (60%) sering mengeluhkan pekerjaannya 
terutama ketika mendapatkan teguran dari pimpinan dan empat orang (40%) lainnya 
menerima ketika mendapatkan teguran dari pimpinan dan berusaha memperbaiki kesalahan. 
Selanjutnya lima orang (50%) mengakui tidak pernah terlibat permasalahan dengan rekan 
kerja maupun pimpinan dan lima orang (50%) lainnya mengaku pernah mengalami 
permasalahan ringan dengan sesama rekan kerja maupun pimpinan mengenai 
kesalahpahaman dan perbedaan cara pikir. Selanjutnya enam orang (60%) berusaha mencari 
informasi mengenai perkembangan perusahaan dan empat orang (40%) lainnya hanya 
menunggu pimpinan memberikan informasi. 
Kinerja karyawan warehouse di PT  X  yang tinggi sangatlah diharapkan oleh 
perusahaan. Semakin banyak karyawan yang mempunyai kinerja tinggi maka produktivitas 
perusahaan secara keseluruhan akan meningkat sehingga perusahaan akan dapat bertahan 
dalam persaingan global. Serta sebagai perusahaan yang baru berkembang menjadi Perseroan 
Terbatas (PT) dan ingin terus memantapkan posisinya di bidang industri manufaktur 
khususnya tas dan dompet, sangat memerlukan karyawan yang mampu mendukung kemajuan 
perusahaan untuk mencapaian keberhasilan dalam persaingan dunia bisnis di era globalisasi. 
Hal tersebut sejalan dengan penelitian yang telah dilakukan oleh Gunawan (2010) dimana 
melalui perilaku OCB yang ditampilkan oleh karyawan akan membuat aktivitas organisasi 
suatu perusahaan berjalan dengan lebih lancar dan membawa pada tingkat efektivitas yang 
lebih tinggi serta mampu mempertahankan kelangsungan organisasi.  
Berdasarkan uraian diatas, maka peneliti tertarik untuk meneliti lebih lanjut mengenai 
gambaran Organizational Citizenship Behavior pada karyawan warehouse PT  X  di Kota 
Bandung. 
Berdasarkan uraian pada latar belakang masalah yang dikemukakan di atas, maka 
ingin diketahui bagaimana tingkat Organizational Citizenship Behavior pada karyawan 
warehouse PT  X  di Kota Bandung. 
 Memperoleh gambaran mengenai tingkat Organizational Citizenship Behavior pada 
karyawan warehouse PT  X  di Kota Bandung. 
 Memberikan paparan yang lebih rinci mengenai tingkat Organizational Citizenship 
Behavior yang dimunculkan pada karyawan warehouse PT  X  di Kota Bandung. 
dapat memperkaya penelitian dan pemahaman kajian di bidang psikologi seperti 
dalam psikologi industri dan organisasi. 
meneliti lebih lanjut mengenai Organizational Citizenship Behavior yang dapat 
berguna bagi penelitinya. 
Organizational Citizenship Behavior (OCB) pada karyawan warehouse dan 
manfaatnya untuk dapat diterapkan dalam perilaku kerja. 
karyawan warehouse, pengelola PT  X  dapat memanfaatkan informasi ini untuk 
merencanakan langkah selanjutnya yang dapat ditindaklanjuti untuk ditingkatkan 
apabila diperlukan. 

