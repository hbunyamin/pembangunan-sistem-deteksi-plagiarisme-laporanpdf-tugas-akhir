Rumah merupakan salah satu kebutuhan primer bagi setiap manusia yang 
hidup di dunia. Rumah menjadi penting karena menjadi tempat berteduh, 
berkembang dan berlindungnya sebuah keluarga. Saat ini tidak setiap keluarga 
memiliki rumah karena berbagai faktor, diantaranya rendahnya pendapatan yang 
diterima suami dan atau istri, maupun kehilangan rumah karena terkena bencana 
baik bencana alam maupun bencana sosial (Perang, krisis ekonomi dan lain-lain). 
Hal tersebut menggugah Millard dan Linda Fuller untuk membantu, sehingga 
pada tahun 1976 membentuk sebuah organisasi yang bergerak untuk membantu 
orang-orang yang membutuhkan rumah dari kalangan ekonomi menengah ke 
bawah yang bernama Habitat for Humanity.  
Habitat for Humanity memiliki visi setiap orang memiliki tempat tinggal 
yang layak di seluruh dunia. Untuk menjalankan visi tersebut Habitat for 
Humanity memiliki misi dengan bekerja bersama Tuhan dan setiap orang 
dimanapun, dari semua orang yang hidup, untuk membangun komunitas orang-
orang yang membutuhkan dan merenovasi rumah sehingga disana dapat 
membangun komunitas dimana semua orang-orang dapat hidup dan berkembang 
sesuai harapan Tuhan. Tujuan yang ingin dicapai Habitat for Humanity adalah 
mengurangi rumah yang tidak layak dan yang tidak memiliki tempat tinggal dari 
muka bumi dengan membangun rumah yang standar dan layak dihuni. 
Habitat for Humanity terus berkembang sehingga membuka jaringan 
(affiliate) di Indonesia sejak 1997. Indonesia saat ini telah membuka affiliate 
dibeberapa kota antara lain Jakarta, Bandung, Yogyakarta, Surabaya, Manado dan 
Batam. Habitat for Humanity Indonesia berharap mampu membuat perubahan 
tempat tinggal orang-orang di Indonesia yang berada di bawah garis kemiskinan 
dan tidak membedakan kepercayaan, jenis kelamin, agama, dan ras. Pada Tahun 
2010 Habitat for Humanity Indonesia memiliki target membangun 2000 rumah di 
Indonesia.  
Affiliate menjadi tumpuan tercapainya visi, misi dan tujuan dari Habitat 
for Humanity Indonesia. Dalam hal ini, setiap affiliate dituntut mampu untuk 
membangun sesuai apa yang ditargetkan Habitat for Humanity Indonesia. Proses 
pembangunan rumah yang dilakukan affiliate terdapat dua jenis program yaitu 
pembangunan regular dan disaster. Pembangunan regular merupakan 
pembangunan yang diberikan kepada orang-orang dari kalangan ekonomi 
menengah kebawah, namun yang memiliki kemampuan untuk membayar cicilan 
dari pembangunan rumah selama waktu tertentu sesuai dengan perjanjian sebelum 
pembangunan. Pembangunan disaster merupakan pembangunan rumah yang 
diberikan kepada orang-orang yang memiliki kemampuan ekonomi di bawah garis 
kemiskinan tanpa pungutan biaya apapun bagi penerima rumah. 
Peneliti memfokuskan penelitian pada affiliate Bandung atau Habitat for 
Humanity Indonesia Affiliate Bandung. Habitat for Humanity Indonesia Affiliate 
Bandung, dipimpin oleh seorang Affiliate Program Manager Bandung yang 
memiliki tugas menentukan target pembangunan yang akan dilakukan, menyusun 
strategi pembangunan yang dapat dilaksanakan, dan bertanggung jawab atas 
laporan pembangunan yang telah dilakukan kepada National Office Habitat for 
Humanity Indonesia setiap bulan. Untuk menjalankan tugasnya Affiliate Program 
Manager Bandung dibantu oleh affiliate construction supervisor, community 
organizer dan affiliate account assistant.  
Affiliate construction supervisor memiliki tugas mengawasi semua 
program pembangunan yang dilakukan Habitat for Humanity Indonesia Affiliate 
Bandung, memastikan keselamatan pekerja serta pekerja sukarela, dan memberi 
laporan kepada affiliate program manager setiap bulan. Dalam bekerja affiliate 
construction supervisor dibantu oleh logistic yang bertugas melakukan pencatatan 
dan pembelian barang-barang yang diperlukan untuk pembangunan rumah serta 
melaporkan hasil kerja setiap hari.  
Community organizer memiliki tugas untuk memperkenalkan organisasi 
pada setiap daerah di Jawa Barat, melakukan survey terhadap daerah binaan yaitu 
wilayah yang dibantu organisasi. Kemudian  community organizer memberikan 
informasi mengenai calon warga daerah binaan yang akan dibantu, yang disebut 
home partner, hingga tanggung jawab untuk menerima kembali repay home 
partner pada program regular atas rumah yang telah dibangun, repay adalah 
pembayaran yang dibayarkan home partner sesuai perjanjian yang telah 
disepakati. Selain itu, community organizer mengawasi saving group yang 
merupakan seorang home partner sebagai pengumpul repay daerah binaan dan 
memberi informasi dari home partner akan kebutuhan warga lain yang 
membutuhkan bantuan organisasi. Community organizer memberikan laporan 
kepada affiliate program manager, affiliate account assistant, dan affiliate 
construction supervisor setiap hari.  
Affiliate account assistant bertugas untuk melakukan pencatatan, 
pengeluaran dan pelaporan dana untuk operasional affiliate kepada affiliate 
program manager setiap hari dan National Office Habitat for Humanity Indonesia 
setiap bulan. Dalam bekerja Affiliate account assistant dibantu oleh finance 
officer yang bertugas untuk melakukan pencatatan keuangan yang masuk dan 
keluar setiap hari dan dilaporkan setiap hari ke affiliate account assistant dan juga 
mencatat jam kerja staf untuk menilai gaji yang akan diberikan. 
Proses pembangunan yang dilakukan oleh Habitat for Humanity Indonesia 
Affiliate Bandung yang ditargetkan sebanyak 1000 rumah di Jawa Barat dimulai 
dari penunjukkan daerah binaan yang ditentukan oleh Affiliate Program Manager 
Bandung dan National Office Habitat for Humanity Indonesia. Dua orang 
community organizer melakukan survey terhadap daerah binaan yang ditunjuk. 
Community organizer melakukan pencatatan informasi mengenai calon home 
partner yang dilaporkan kepada Affiliate Program Manager Bandung. Setelah 
Affiliate Program Manager Bandung menyetujui home partner yang diajukan, 
community organizer mengajukan tipe rumah kepada affiliate construction 
supervisor dan mengajukan biaya yang diperlukan untuk membangun rumah 
home partner kepada seorang affiliate account assistant, dan juga membuat 
perjanjian dengan home partner mengenai rumah yang akan dibangun, biaya, dan 
waktu kewajiban membayar cicilan (repay). Affiliate account assistant 
mengajukan dana yang dibutuhkan kepada National Office Habitat for Humanity 
Indonesia untuk membangun rumah bagi home partner. Bila disetujui National 
Office Habitat for Humanity Indonesia, Affiliate account assistant meminta 
community organizer melengkapi data home partner serta membentuk saving 
group di daerah tersebut, dan Affiliate Program Manager Bandung mengajukan 
perijinan kepada pemerintah daerah. Setelah mendapatkan ijin dari pemerintah 
daerah affiliate construction supervisor melakukan pembangunan rumah bagi 
home partner. 
Setelah pembangunan selesai community organizer menerima repay dari 
home partner melalui saving group yang telah dibentuk sebelumnya. Setiap hasil 
repay yang diterima community organizer akan dilaporkan kembali ke affiliate 
account assistant melalui finance officer. Affiliate account assistant membuat 
laporan akhir setiap bulan mengenai kemajuan pembangunan, pengeluaran untuk 
proses pembangunan dan repay dari seluruh saving group di Affiliate Office 
Bandung. Laporan akhir akan diserahkan kepada Affiliate Program Manager 
Bandung dan national accountant. Seluruh Affiliate Program Manager akan 
mempertanggungjawabkan hasil kerja kepada affiliate executive board untuk 
kebijakan yang diberlakukan, affiliate development officer untuk operasional 
terjadi, dan national accountant untuk penggunaan dana dari setiap affiliate yang 
dipimpinnya di national office Habitat for Humanity Indonesia. Seluruh data yang 
diterima Habitat for Humanity Indonesia akan dilaporkan ke kantor pusat Habitat 
for Humanity dan kepada donatur sebagai pertanggungjawaban atas dana yang 
telah diberikan. 
Pada kenyataannya masih terdapat kendala yang dihadapi Habitat for 
Humanity Indonesia Affiliate Bandung, sehingga berdampak pada siklus 
operasional organisasi. Kendala yang muncul diantaranya terdapat beberapa orang 
yang bekerja part time di Affiliate Bandung. Menurut Affiliate Program Manager 
Bandung hasil kerja staf part time cukup baik, Namun, waktu yang diberikan staf 
part time terkadang tidak cukup dan sangat berpengaruh pada kelancaran 
informasi yang diperlukan. Misalnya saat Affiliate Program Manager 
memerlukan informasi, staf part time tidak masuk kantor karena kuliah dan 
laporan yang ada tidak dipersiapkan terlebih dahulu. Hal tersebut mengganggu 
kinerja staf bagian lain yang harus mencari data yang dibutuhkan affiliate 
program manager. 
Affiliate account assistant juga merasakan masalah mengenai laporan yang 
diberikan oleh community organizer maupun affiliate construction supervisor 
dimana seringkali mereka membuat laporan yang tidak tersusun rapih, misalnya 
hanya memasukkan seluruh kuitansi pengeluaran dan laporan mengenai home 
partner ke dalam satu map sesuai dengan job description-nya saja. Ini 
mengganggu kinerja affiliate account assistant maupun finance officer yang harus 
merapikan dahulu setiap kuitansi maupun data dari mereka sebelum membuat 
laporan selanjutnya. Kendala tersebut juga pernah mengakibatkan affiliate 
account assistant menunda laporan yang akan dilaporkan kepada national office, 
sebagai pertimbangan penilaian kerja terhadap Habitat for Humanity Indonesia 
Affiliate Bandung. 
Kantor Affiliate Bandung sementara ini sedang dipindahkan ke 
Pangalengan, untuk memperlancar kerja Habitat for Humanity Indonesia Affiliate 
Bandung membangun rumah bagi keluarga yang terkena bencana gempa 
Pangalengan. Kantor di Pangalengan terorganisir dengan memisahkan posisi 
ruang kerja setiap bagian agar menjadi lebih mudah pengurusan administrasi. 
Namun, pada malam hari beberapa ruang kerja menjadi kamar tidur untuk staf, 
karena kantor tersebut juga menjadi rumah tinggal semua staf. Berdasarkan 
wawancara dengan empat orang staf, tiga staf merasa kurang nyaman dengan 
keadaan tersebut. Walaupun terasa kurang ideal sebagai tempat kerja terdapat satu 
staf memahami keadaan tersebut. Mereka merasa bahwa organisasi ini lebih 
mengutamakan bantuan kepada warga yang kurang mampu, sehingga mereka 
tetap semangat bekerja selama kebersihan kantor terjaga. 
Pindahnya kantor juga membuat dua orang community organizer 
merasakan kelelahan karena harus mengadakan perjalanan yang lebih jauh setiap 
hari kerja untuk memperkenalkan organisasi kepada daerah binaan dan 
mengawasi saving group daerah binaan sebelumnya agar repay tepat waktu. Dana 
repay sangat diperlukan untuk kelancaran operasional affiliate. Sebagian besar 
dana repay digunakan affiliate untuk membayar gaji staf maupun pekerja 
bangunan, sisanya disimpan sebagai dana tidak terduga. Kerja operasional akan 
terganggu bila terjadi keterlambatan pembayaran gaji, terlebih gaji pekerja 
bangunan yang harus menyelesaikan pembangunan rumah yang ditargetkan 
Affiliate Program Manager Bandung. Keterlambatan penyelesaian pembangunan 
akan berdampak pada meningkatnya biaya operasional dan melemahkan hasil 
kerja di laporan akhir affiliate Bandung. Kerja yang seperti ini sangat 
mengganggu karena hasil kerja affiliate dinilai dari laporan yang dibuat untuk 
dipertanggungjawabkan kepada pemberi dana. Dampak terburuk dari menurunnya 
hasil kerja affiliate adalah penghentian dana kepada Affiliate Bandung dan 
akhirnya Habitat for Humanity Indonesia Affiliate Bandung akan ditutupnya. Oleh 
karena itu, staf harus cepat dan tepat dalam menyampaikan informasi setiap 
harinya sehingga pekerjaan rekan kerja yang membutuhkan informasi dapat lebih 
efektif dalam mencapai tujuan organisasi. 
Dengan munculnya kendala seperti itu, Affiliate Program Manager 
Bandung berharap agar setiap staf dapat menjalankan tugasnya dengan optimal 
serta memahami kemungkinan munculnya kesulitan bagi rekan kerja bagian lain 
atas pekerjaannya. Staf diharapkan mampu melaksanakan tugas dan job 
description secara efektif. Selain itu, staf juga diharapkan memiliki tanggung 
jawab dan perilaku saling mendukung antar bagian. Dalam hal ini, staf dituntut 
mampu bekerja sebagai tim. Kerja sama dan koordinasi yang baik dapat 
memudahkan proses operasional pembangunan rumah home partner. Kerja sama 
tim tidak hanya yang tersirat dalam job description masing-masing bagian saja 
tapi juga kemauan menolong staf lain secara sukarela tanpa menunggu permintaan 
dari atasan untuk memberikan bantuan kepada rekan kerja yang mengalami 
pekerjaan yang overload maupun membantu mempermudah pekerjaan rekan kerja 
yang berkaitan. Perilaku untuk saling membantu tidak hanya dalam bidangnya 
masing masing, melainkan antar sesama staf dalam bagian staf Habitat for 
Humanity Indonesia Affiliate Bandung. Perilaku membantu yang dilakukan secara 
sukarela tersebut menurut Organ (2006) adalah Organizational Citizenship 
Behavior.  
Organizational Citizenship Behavior (OCB) merupakan perilaku yang 
dilakukan atas kemauannya sendiri, meskipun tidak secara langsung atau secara 
 eksplisit  memiliki nilai imbalan, dan apabila dilakukan secara bersamaan akan 
berdampak meningkatnya fungsi organisasi secara efektif dan efisien (Organ, 
1988 : 3, dalam Organ 2006 :3). 
Dasar dari 
perilaku OCB adalah perbuatan menolong secara spontan (tanpa ada arahan atau 
permintaan). Menurut Podsakoff, MacKenzie, Moorman, dan Fetter (1990 dalam 
Organ 2006 : 251) OCB memiliki lima dimensi yaitu Altruism, Conscientiousness, 
Sportsmanship, Courtesy, dan Civic virtue. 
Staf Habitat for Humanity Indonesia Affiliate Bandung yang melakukan 
OCB akan menjalankan tugasnya dengan tanggung jawab serta menampilkan 
perilaku saling tolong-menolong dan mendukung antar staf. Dalam hal ini, 
diperlukan kerjasama untuk menyelesaikan target organisasi, tidak hanya bekerja 
sesuai job description-nya, namun juga ada kemauan menolong staf lain secara 
sukarela (meski tidak tercantum dalam job description). Bantuan dilakukan secara 
spontan, tanpa mengharapkan imbalan ataupun pujian dari rekan kerja dapat 
menghemat energi sumber daya anggota dan memelihara fungsi kelompok. 
Keuntungan dari perilaku menolong adalah meningkatkan semangat, moral, dan 
keeratan kelompok, sehingga anggota kelompok tidak perlu menghabiskan energi 
dan waktu untuk pemeliharaan fungsi kelompok. Hal tersebut akan membuat staf 
Habitat for Humanity Indonesia Affiliate Bandung bekerja lebih efektif dan 
optimal. 
Perilaku saling membantu sangat dibutuhkan staf Habitat for Humanity 
Indonesia Affiliate Bandung agar informasi yang dibutuhkan setiap bagian dapat 
berjalan lancar, sehingga tidak menghambat kerja dan berdampak pada bagian lain 
dalam rutinitas kerjanya. Berdasarkan fakta di atas peneliti ingin melakukan 
penelitian mengenai gambaran Orgnizational Citizenship Behavior (OCB) pada 
staf Habitat for Humanity Indonesia Affiliate Bandung. 


