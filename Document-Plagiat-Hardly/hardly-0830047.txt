Pendidikan formal di Indonesia merupakan rangkaian jenjang pendidikan 
yang wajib dilakukan oleh seluruh warga Negara Indonesia, di mulai dari Sekolah 
Dasar (SD), Sekolah Menengah Pertama (SMP), dan Sekolah Menengah Atas 
(SMA).Sekolah Menengah Pertama (SMP) adalah jenjang pendidikan dasar 
formal di Indonesia setelah menyelesaikan sekolah dasar (SD), yang dilaksanakan 
dalam kurun waktu 3 tahun, mulai dari kelas 7 sampai kelas 9. Sekolah Menengah 
Pertama (SMP) termasuk dalam lingkup program wajib belajar bagi setiap warga 
negara Indonesia yang berusia 7-15 tahun yaitu pendidikan sekolah dasar (atau 
sederajat) selama 6 tahun dan sekolah menengah pertama (atau sederajat) selama 
3 tahun, dan pelajar SMP biasanya berusia 12-15 tahun (Kementerian Pendidikan 
Nasional). 
Sekolah merupakan tempat untuk melakukan kegiatan pembelajaran, 
memberi dan menerima pelajaran berdasarkan tingkatannya. Menurut Syamsu 
Yusuf (2011) sekolah adalah lembaga pendidikan yang merupakan lingkungan 
sosial bagi siswa untuk dapat berinteraksi dengan teman sebaya atau dengan siswa 
lainnya. Sekolah berkewajiban untuk membentuk lingkungan sosial yang 
konstruktif bagi siswa, sehingga mampu menghilangkan gangguan-gangguan 
sosial-psikologis, seperti kecemasan yang berlebihan, putus asa, egois, stres dan 
gangguan psikologis lainnya. 
Sekolah Menengah Pertama (SMP) merupakan transisi dari sekolah dasar 
(SD), transisi ini merupakan sebuah pengalaman normatif yang dialami oleh 
semua anak. Transisi tersebut dapat menimbulkan stres karena terjadi secara 
simultan dengan terjadinya sejumlah perubahan lain dalam diri individu, keluarga 
dan sekolah. Secara intelektual, siswa lebih tertantang oleh tugas-tugas akademik. 
Para siswa juga akan memiliki lebih banyak kesempatan untuk meluangkan waktu 
bersama dengan teman-teman, dan memilih teman yang cocok serta menikmati 
kemandirian dari pengawasan orangtua secara langsung (Santrock, 2012). 
 Menurut Kementerian Pendidikan Nasional banyak siswa yang sangat 
terbiasa dengan cara guru  X  mengajar, maka dengan cara tersebut ia dapat 
optimal menyerap materi pelajaran. Kenyataannya, saat naik kelas atau transisi 
dari SD ke SMP dan seterusnya siswa mendapatkan cara guru yang berbeda cara 
mengajarnya dengan guru yang sebelumnya, hal ini tentu memengaruhi sikap 
siswa. Fenomena seperti ini berlaku pada perbedaan aturan sekolah, guru, teman-
teman, lingkungan, maka siswa yang memasuki kondisi sekolah yang baru, maka 
anak dituntut untuk melakukan penyesuaian. Penyesuaian merupakan kemampuan 
seseorang untuk hidup dan bergaul secara wajar terhadap lingkungannya, 
sehingga siswa merasa puas terhadap diri sendiri dan lingkungan. Penyesuaian 
merupakan hal yang sangat penting untuk dapat memenuhi kebutuhan siswa 
dengan segala macam kemungkinan yang ada di lingkungan.  
Penyesuaian sosial adalah suatu kemampuan seseorang dalam bereaksi 
secara efektif, sehat terhadap keadaan-keadaan yang dihadapinya agar survive. 
Schneiders mengatakan bahwa penyesuaian sosial timbul apabila terdapat 
kebutuhan, dorongan, tuntutan dan keinginan yang harus dipenuhi oleh individu 
termasuk juga saat menghadapi suatu masalah di sekolah yang harus diselesaikan 
(Schneiders, 1964). 
Dalam kerangka menyesuaikan diri di sekolah, siswa harus memerhatikan  
pelbagai tuntutan, seperti harus menerima dan menghargai otoritas yaitu 
memosisikan guru-guru sebagai figur yang dihormati dan dihargai, menerima 
peraturan sekolah; bersedia berpartisipasi dalam aktivitas sekolah; membina relasi 
yang sehat dengan teman sebaya, guru dan staf di sekolah; menerima batasan dan 
tanggung jawab; serta membantu merealisasikan tujuan sekolah. Kehidupan 
sekolah hanyalah sebuah bagian dari realitas, dan faktor-faktor seperti kurangnya 
minat terhadap sekolah, bolos, hubungan emosional yang tidak sehat dengan guru, 
memberontak, dan menantang otoritas, merupakan hambatan bagi penyesuaian 
sosial di sekolah (Schneiders, 1964). 
Menurut Scheneiders (1964) proses penyesuaian sosial di sekolah dapat 
menimbulkan berbagai masalah terutama masalah-masalah di sekolah yang terjadi 
pada diri sendiri, siswa yang berhasil memenuhi berbagai tuntutan tanpa 
gangguan, maka siswa tersebut dikatakan dapat melakukan penyesuaian sosial di 
sekolah, namun siswa yang tidak berhasil memenuhi tuntutannya dikatakan 
maladjusted. Siswa yang dapat melakukan penyesuaian sosial dengan ciri-ciri 
seperti memiliki rasa tanggung jawab, adaptabilitas, memiliki tujuan yang jelas, 
dapat mengendalikan diri, adanya penerimaan sosial. 
Berdasarkan hasil survey awal yang dilakukan penulis di SMP Negeri  X  
Bandung, ditemukan sebesar 50% dari 366 siswa kelas VII yang melakukan 
pelanggaran terhadap beberapa aturan sekolah yaitu 5% siswa terlambat datang ke 
sekolah, 5% siswa yang membolos, 15% siswa tidak mengerjakan PR, 25% siswa 
merasa malu membina hubungan dengan teman sebaya dan guru. Siswa yang 
melakukan pelanggaran terhadap aturan-aturan yang telah ditetapkan oleh 
sekolah, akan diberikan sanksi-sanksi yang berlaku. 
Menurut Syamsu Yusuf (2011) ketidakmampuan dalam menyesuaikan diri 
terhadap lingkungan sekolah terlihat dari ketidakpuasan terhadap diri sendiri dan 
lingkungan sekolah, memiliki sikap-sikap yang menolak terhadap realitas dan 
lingkungan sekolah. Siswa yang mengalami perasaan ini akan merasa terasing dari 
lingkungan, akibatnya siswa tidak mengalami kebahagiaan dalam berinteraksi 
dengan guru, teman sebaya ataupun keluarganya. 
Respon terhadap untutan dan realitas kehidupan sosial di sekolah akan 
diekspresikan secara berbeda-beda oleh masing-masing siswa, tergantung pada 
kemampuan penyesuaian sosial yang dimilikinya. Siswa dalam melakukan 
tuntutan-tuntutan penyesuaian sosial di sekolah membutuhkan keterampilan 
emosional yang tercermin melalui kemampuan mengelola emosi diri sendiri dan 
empati terhadap orang lain. Keterampilan emosional di atas merujuk pada 
kecerdasan emosional dari Goleman (2002). 
Kecerdasan emosional merupakan salah satu faktor yang memengaruhi 
penyesuaian sosial di sekolah, sehingga siswa yang memiliki kecerdasan 
emosional rendah akan mengalami kesulitan dalam menyesuaikan diri dengan 
lingkungan sekolah. Menurut Goleman (2002) kecerdasan emosional adalah 
kemampuan memotivasi diri sendiri dan bertahan menghadapi frustrasi, 
mengendalikan dorongan hati dan tidak melebih-lebihkan kesenangan, mengatur 
suasana hati dan menjaga agar beban stres, berempati dan berdoa. Kecerdasan 
emosional pada siswa akan terlihat melalui kecerdasan interpersonal, yaitu 
mampu untuk menyadari emosi diri sendiri, mampu untuk mengelola emosi diri 
sendiri, dan memotivasi diri sendiri, serta diimbangi oleh kecerdasan 
antarpersonal yang meliputi mampu berempati, bagaimana kemampuan siswa 
dalam memberikan kesan yang baik tentang dirinya, mampu mengekspresikan 
emosi dengan cara-cara yang matang, melibatkan diri dengan permasalahan yang 
memiliki pandangan moral dan tanggung jawab, bersikap tegas, mudah bergaul, 
ramah, serta mampu menyesuaikan diri dengan beban stres. 
Berdasarkan uraian fenomena di atas, maka penulis bermaksud untuk 
melakukan penelitian tentang Hubungan antara Kecerdasan Emosional dan 
Penyesuaian Sosial di Sekolah pada Siswa Kelas VII di SMP Negeri  X  
Bandung. 
Seberapa kuat hubungan antara kecerdasan emosional dan penyesuaian 
sosial di sekolah pada siswa kelas VII di SMP Negeri  X  Bandung. 
Melihat hubungan antara dua variabel yaitu kecerdasan emosional dan 
penyesuaian sosial di sekolah. 
Memperoleh pemahaman mengenai hubungan antara kecerdasan 
emosional dan penyesuaian sosial di sekolah pada siswa kelas VII di SMP Negeri 
 X  Bandung. 
Penelitian ini diharapkan dapat memberikan sumbangan yang berarti bagi 
perkembangan ilmu Psikologi Pendidikan dan psikologi sosial, khususnya 
mengenai kecerdasan emosional dalam kaitannya dengan penyesuaian sosial di 
sekolah pada siswa. 
1) Memberikan masukan bagi siswa agar dapat menyesuaikan diri dengan 
diri sendiri, lingkungan sekolah, maupun lingkungan sosialnya. 
2) Memberikan masukan bagi pihak sekolah yang dapat 
dijadikan pertimbangan dalam memberikan tuntutan bagi siswa. 
Siswa yang sedang menempuh pendidikan di jenjang Sekolah Menengah 
Pertama (SMP) menemui berbagai macam tuntutan dari lingkungan tersebut, 
antara lain tuntutan akademis, tuntutan emosi dan tuntutan sosial. Dalam usaha 
untuk memenuhi tuntutan lingkungan, siswa harus mampu untuk menyesuaikan 
diri dengan tuntutan-tuntutan tersebut. 
 

