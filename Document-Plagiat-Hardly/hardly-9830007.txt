Penelitian ini dilakukan dengan tujuan untuk mengetahui gambaran dan 
Stres merupakan kata yang sering muncul dalam pembicaraan masyarakat 
umum akhir-akhir ini. Stres dapat diartikan sebagai perasaan tidak dapat 
mengatasi masalah atau masalah yang potensial dalam hidup seseorang. Stres 
dapat dialami oleh semua orang dari berbagai tingkatan usia, mulai dari anak-anak 
hingga orang dewasa. Hal-hal yang menimbulkan stres bagi tiap-tiap orang tidak 
sama, karena tiap-tiap orang memiliki daya tahan yang berbeda dalam 
menghadapi  masalah  dan karena tiap-tiap orang memiliki toleransi yang 
berbeda-beda dalam menghadapi tuntutan atau tekanan. Masalah atau tuntutan 
yang menimbulkan stres pada seseorang, belum tentu menimbulkan stres pada 
orang lain. Dalam situasi lain, bahkan suatu kondisi yang sama dapat 
menimbulkan stres dengan derajat yang berbeda-beda pada beberapa orang 
sekaligus, salah satunya adalah mengerjakan skripsi. 
 Skripsi merupakan suatu syarat yang diajukan sebagai tugas akhir pada 
Fakultas Sastra Universitas  X , sebelum seorang mahasiswa memperoleh 
kelulusannya. Persyaratan yang harus dipenuhi sebelum mengontrak skripsi pada 
mahasiswa Fakultas Sastra Jurusan Bahasa Inggris adalah mengambil semua mata 
kuliah, dan tidak boleh ada yang belum lulus, semua mata kuliah yang diambil 
harus bernilai minimal C; sedangkan pada mahasiswa Fakultas Sastra Jurusan 
Bahasa Jepang adalah sudah mengambil mata kuliah DFMP (= Dasar Filsafat 
Metodologi Penelitian) dan SPM (= Sejarah Pemikiran Modern). Pada Oktober 
2005, jumlah mahasiswa Fakultas Sastra Jurusan Bahasa Inggris Universitas  X , 
yang sedang aktif mengerjakan skripsi ada 48 mahasiswa yang terdiri atas 
angkatan 1997 sampai 2001. Pada angkatan 1997, yang masih skripsi ada 1 
mahasiswa, sedangkan yang sudah lulus 59 mahasiswa; angkatan 1998, yang 
masih skripsi ada 2 mahasiswa, sedangkan yang sudah lulus 54 mahasiswa; 
angkatan 1999, yang masih skripsi ada 4 mahasiswa, sedangkan yang sudah lulus 
78 mahasiswa; angkatan 2000, yang masih skripsi ada 17 mahasiswa, sedangkan 
yang sudah lulus 57 mahasiswa; angkatan 2001, yang masih skripsi ada 24 
mahasiswa, sedangkan yang sudah lulus 25 mahasiswa. Sedangkan jumlah 
mahasiswa Fakultas Sastra Jurusan Bahasa Jepang Universitas  X , yang sedang 
aktif mengerjakan skripsi ada 32 mahasiswa yang terdiri atas angkatan 1998 
sampai 2001. Pada angkatan 1998, yang masih skripsi ada 2 mahasiswa, 
sedangkan yang sudah lulus 18 mahasiswa; angkatan 1999, yang masih skripsi 
ada 7 mahasiswa, sedangkan yang sudah lulus 29 mahasiswa; angkatan 2000, 
yang masih skripsi ada 15 mahasiswa, sedangkan yang sudah lulus 20 mahasiswa; 
angkatan 2001, yang masih skripsi ada 8 mahasiswa, sedangkan yang sudah lulus 
4 mahasiswa. Dari data yang ada, dapat dilihat bahwa mahasiswa Fakultas Sastra 
Jurusan Bahasa Inggris dan Jepang Universitas  X  relatif cepat dalam 
mengerjakan dan menyelesaikan skripsi. 
Penyusunan skripsi ini sebenarnya menuntut banyak hal dari tiap 
mahasiswa, seperti ketekunan dan kegigihan, karena dalam perjalanan membuat 
skripsi mahasiswa akan menghadapi berbagai kesulitan atau tantangan yang bisa 
menjadi sumber stres bagi dirinya. Selain itu mahasiswa juga dituntut untuk 
mampu mengembangkan daya pikir dan penalarannya untuk menyusun naskah 
skripsi, dan juga mampu bekerjasama dengan orang-orang yang bersangkutan 
dalam penyelesaian penulisan skripsi, seperti dosen pembimbingnya. 
 Tugas-tugas yang harus dihadapi mahasiswa Fakultas Sastra Jurusan 
Bahasa Inggris dan Jepang Universitas  X  Bandung dalam mengerjakan skripsi 
secara garis besar sama, yaitu : Mendapatkan dosen pembimbing yang sesuai atau 
memiliki kompetensi untuk membimbing mahasiswa dalam meneliti bidang 
permasalahan yang dipilih mahasiswa. Penentuan dosen pembimbing ini 
ditentukan oleh pihak fakultas, dan mahasiswa harus dapat bekerjasama dengan 
dosen pembimbing yang telah ditetapkan. Mahasiswa diharuskan mencari suatu 
topik atau permasalahan yang menurut mahasiswa menarik, berguna dan cukup up 
to  date  untuk  diteliti.  Selain  itu  mahasiswa harus membaca dan mencari 
bahan-bahan untuk referensi penelitian. Selanjutnya memasuki proses bimbingan 
personal dengan dosen pembimbing, dan harus pula mengerjakan penulisan 
skripsi. Tugas terakhir mahasiswa adalah menghadapi sidang sebagai ujian akhir 
mahasiswa Fakultas Sastra Universitas  X . 
Sedangkan perbedaannya : Pada mahasiswa Fakultas Sastra Jurusan 
Bahasa Inggris, penulisan skripsi harus dalam bahasa Inggris dan isi skripsi adalah 
pembahasan novel atau drama, dan dibagi lagi dalam 2 kategori tugas berdasarkan 
IPK. Mahasiswa dengan IPK ? 3, harus membahas 2 novel atau 2 drama. 
Sebaliknya mahasiswa dengan IPK di bawah 3 hanya mengerjakan 1 novel atau 1 
drama. Kemudian mahasiswa diberi batas waktu 16 minggu untuk menyusun 
laporan dibimbing dosen pembimbing, setelah batas waktu mahasiswa harus 
menyerahkan laporan pada evaluator. Evaluator inilah yang akan menilai apakah 
laporan perlu diperbaiki atau tidak, bila laporan perlu diperbaiki maka laporan 
akan dikembalikan pada mahasiswa, dan harus diperbaiki dalam waktu 2 minggu, 
setelah itu diserahkan kembali kepada evaluator. Setelah perbaikan dianggap 
sempurna, mahasiswa hanya perlu menunggu panggilan untuk sidang skripsi. 
Pada mahasiswa Fakultas Sastra Jurusan Bahasa Jepang, penulisan laporan harus 
dalam bahasa Jepang, dan topik bahasan tidak hanya terbatas pada 1 novel atau 1 
drama, tetapi juga dapat membahas animasi, yaitu komik atau kartun. Mahasiswa 
hanya diberi waktu 2 semester, bila belum selesai, mahasiswa kemungkinan besar 
akan memperoleh penggantian dosen. Bila dalam 7 tahun mahasiswa belum 
selesai juga, mahasiswa akan di-DO. 
Selain tugas-tugas dari skripsi itu sendiri, terdapat berbagai kondisi yang 
kemungkinan besar dapat menimbulkan stres pada saat membuat skripsi, misalnya 
tugas yang mengharuskan dan menuntut mahasiwa membuat suatu karya tulis 
yang bersifat ilmiah, yang penulisannya tidak dapat dilakukan secara asal-asalan. 
Mahasiswa juga harus mencari suatu judul skripsi, dan mencari bahan materi yang 
mendukung penulisan tersebut, terkadang materi yang tersedia cukup banyak, 
akan tetapi terkadang materi yang dibutuhkan ternyata sangat terbatas, bahkan ada 
beberapa materi yang harus diterjemahkan dulu oleh mahasiswa. Banyak situasi 
lain yang menempatkan mahasiswa dalam kondisi yang cukup menekan, karena 
mahasiswa diperhadapkan kepada beberapa situasi baru, misalnya, masuk dan 
mengalami bimbingan secara intensif dan berhadapan muka dengan dosen-dosen 
pembimbing, padahal sebelumnya mahasiswa pada saat mengikuti kuliah di kelas 
hanya duduk mendengarkan dosen dan dalam kelompok besar. Pada saat 
bimbingan sudah berjalan ternyata kemudian mengalami kesulitan untuk menemui 
dosen yang cukup sibuk dengan jadwalnya yang ketat; belum lagi bila terjadi 
perbedaan pendapat dan pemahaman, serta kesulitan dalam berkomunikasi dengan 
dosen. Selain itu, diharuskannya mahasiswa membaca banyak materi untuk 
penulisan skripsi, padahal sebelumnya mahasiswa hanya membaca bahan-bahan 
spesifik yang diberikan dosen pada saat kuliah. Ditambah kurangnya sarana 
mahasiswa, seperti tidak tersedianya komputer untuk mengetik skripsi. Kesulitan 
dan tekanan yang lain dapat muncul juga dari pihak keluarga, misalnya orang tua 
yang menuntut mahasiswa untuk cepat lulus supaya tidak membuat malu keluarga 
dan membuang-buang uang, atau muncul kesulitan biaya justru pada saat 
mahasiswa sedang dalam pengerjaan skripsi. 
 Dari hasil wawancara terhadap 9 mahasiswa Fakultas Sastra Jurusan 
Bahasa Inggris dan 8 mahasiswa Fakultas Sastra Jurusan Bahasa Jepang 
Universitas  X  Bandung, diketahui bahwa terdapat kesulitan-kesulitan yang pada 
akhirnya menjadi sumber stres. Sumber stres yang paling banyak (88% atau 15 
orang) dialami mahasiswa Fakultas Sastra Universitas  X  adalah ancaman, yang 
muncul karena kesulitan dalam penggunaan dan penerapan bahasa asing untuk 
membuat laporan; dan, 47% (8 mahasiswa) mengaku sulit sekali menuangkan 
pemikiran-pemikiran mereka ke dalam bentuk tulisan atau laporan. Sumber stres 
lain adalah frustrasi, yang dialami 77% (13 mahasiswa), mereka mengakui 
mengalami   kesulitan   bekerjasama   dengan  dosen  pembimbing  karena  
alasan-alasan tertentu. Sumber stres lainnya, yang agak kurang dikeluhkan ialah 
konflik, yang dialami 18% (3 mahasiswa) mengakui kesulitan muncul karena 
dirinya sendiri yang malas. Selanjutnya tekanan (Pressure), yang dialami 12% (2 
mahasiswa) merasa sulit mengerjakan skripsi akibat ketegangan yang muncul 
karena adanya deadline penyerahan laporan; dan, 12% (2 mahasiswa) merasa sulit 
mengerjakan skripsi justru diakibatkan karena adanya tekanan dari orang tua yang 
menuntut terus-menerus supaya cepat selesai. 
Dari kesulitan-kesulitan yang ada, 17 mahasiswa Fakultas Sastra 
Universitas  X  tersebut juga mengakui bahwa mereka mengalami stres, yang 
muncul dalam simptom yang berbeda-beda, simptom yang paling banyak muncul, 
sebagai akibat kegelisahan, sulit tidur, dan merasa tertekan (71% atau 12 
mahasiswa) menjadi menunda-nunda dan malas mengerjakan skripsi. Selanjutnya 
59% (10 mahasiswa) menyebutkan mengalami ketidaknyamanan pada saat tidur, 
sehingga pada saat akan mengerjakan skripsi, mereka tidak mampu berkonsentrasi 
dan merasa letih; dan 47% (8 mahasiswa) memiliki kecenderungan untuk makan 
secara berlebihan dengan sering mengemil; serta selalu merasa gelisah sehingga 
melakukan apapun merasa tidak nyaman. Simptom yang lebih jarang muncul 
adalah, 18% (3 mahasiswa) merasa tidak menyukai atau membenci kampus, juga 
merasa tertekan; dan 12% (2 mahasiswa) sengaja menyibukkan diri dengan 
bekerja daripada mengerjakan skripsi. Sekalipun demikian, kesemua mahasiswa 
Fakultas Sastra universitas  X  yang diwawancara tersebut mengakui bahwa 
mereka tetap mengerjakan skripsi, walaupun mengerjakannya dengan terpaksa 
sehingga pengerjaan skripsi menjadi lambat dan tersendat-sendat, akibat stres 
yang muncul. Sebagian besar dari mahasiswa ini atau 77% (13 mahasiswa) 
mengembangkan cara penanggulangan sebagai berikut, yang paling banyak 
digunakan (3 mahasiswa) untuk meredakan stres adalah dengan sibuk main game 
sampai subuh. Kemudian ada 2 mahasiswa yang bertahan mengerjakan skripsi 
dengan mengandalkan Tuhan sebagai pemberi tugas dan penolong. Sedangkan 
yang lainnya, ada yang sibuk memfokuskan diri pada olahraga karena berat badan 
yang bertambah karena banyak makan, ada yang menyibukkan diri dengan 
membaca buku-buku yang tidak berkaitan dengan keperluan membuat skripsi, 
memfokuskan diri pada pekerjaannya, dan pergi main dengan teman-teman, akan 
tetapi skripsi untuk sementara tidak dikerjakan. Selain itu, ada yang meminta 
 pacar  untuk memarahi bila skripsi tidak dikerjakan, ada yang karena diomeli 
orang tuanya terus-menerus, akhirnya skripsi dikerjakan, ada yang mengerjakan 
karena teman-teman dan orang tua mengingatkan terus, dan ada yang 
mengerjakan skripsi dengan marah-marah, sehingga dianggap galak; akan tetapi 
mereka semua tetap berkomitmen untuk terus berjuang dan mengerjakan skripsi 
sampai selesai. 
Dalam rangka menyelesaikan skripsi, beberapa mahasiswa Fakultas Sastra 
universitas  X  menyadari bahwa dirinya mengalami stres, akan tetapi mereka 
kemudian mengembangkan kemampuan mental dan cara-cara untuk 
menanggulangi stres tersebut secara efektif dan terus mengerjakan skripsi sampai 
selesai. Sedangkan sebagian mahasiswa lain tidak atau kurang menyadari, 
sekalipun menyadari bahwa dirinya sedang mengalami stres, mereka tidak 
mengembangkan cara untuk menanggulangi stres tersebut. Mereka tidak berbuat 
apa-apa dan pasrah, sehingga pada akhirnya muncul gangguan-gangguan yang 
malah menambah kesulitan dalam penyelesaian skripsi tersebut. 
Dari hal-hal yang disampaikan di atas, dapat terlihat bahwa penyusunan 
skripsi pada mahasiswa Fakultas Sastra Jurusan Bahasa Inggris dan Jepang 
Universitas  X  Bandung menimbulkan stres dengan derajat yang berbeda-beda. 
Dari penghayatan stres ini kemudian muncul banyak masalah. Dalam menghadapi 
stres, tampak mahasiswa Fakultas Sastra Universitas  X  mengembangkan 
berbagai cara tertentu untuk menanggulangi stres, baik cara tersebut berhasil 
maupun kurang berhasil. 
Mahasiswa yang mampu mengembangkan cara untuk menanggulangi stres 
yang dialami dan dapat menghadapi gangguan yang muncul, akan mampu 
memfokuskan pikiran dan energinya untuk mengerjakan skripsi dengan baik dan 
lancar, serta menyelesaikan skripsi dengan cepat. Akan tetapi mahasiswa yang 
kurang mampu mengembangkan cara untuk menanggulangi stres yang muncul, 
terseret dalam lingkaran stres dan tidak dapat menghadapi gangguan yang ada, 
mengakibatkan mahasiswa menjadi berlambat-lambat mengerjakan skripsi, 
walaupun akhirnya mampu menyelesaikan skripsi tersebut. Bahkan ada 
kemungkinan mahasiswa yang akhirnya mundur karena tidak tahan dengan 
tekanan pada saat penyusunan skripsi. Hal tersebut dapat mendatangkan kerugian 
pada pihak mahasiswa dan juga keluarga mahasiswa yang bersangkutan. 
Dari hasil wawancara awal terhadap mahasiswa Fakultas Sastra Jurusan 
Bahasa Inggris dan Jepang Universitas  X  diketahui bahwa mereka mengalami 
stres, akan tetapi mahasiswa Fakultas Sastra Jurusan Bahasa Inggris dan Jepang 
Universitas  X  berhasil mengerjakan dan menyelesaikan skripsi dalam waktu 
yang relatif cepat.  
Sehubungan dengan itu peneliti ingin meneliti tentang :  Strategi 
Penanggulangan Stres pada Mahasiswa Fakultas Sastra Jurusan Bahasa Inggris 
dan Jepang Universitas  X  Bandung Yang Sedang Membuat Skripsi.  
I. 2. Identifikasi masalah 
Penelitian ini dilakukan untuk mengetahui : 
  Strategi penanggulangan stres manakah yang paling banyak dipakai oleh 
mahasiswa Fakultas Sastra Jurusan Bahasa Inggris dan Jepang Universitas  X  
dalam menghadapi stres yang muncul pada saat membuat skripsi. 
I. 3. Maksud dan tujuan Penelitian 
 Maksud penelitian ini adalah untuk mengetahui gambaran strategi 
penanggulangan stres yang dipakai oleh mahasiswa Fakultas Sastra Jurusan 
Bahasa Inggris dan Jepang Universitas  X , dalam menghadapi stres yang muncul 
sebagai akibat pembuatan skripsi. 
 Sedangkan tujuan penelitian ini adalah memperoleh pemahaman mengenai 
strategi penanggulangan stres yang digunakan mahasiswa Fakultas Sastra Jurusan 
Bahasa Inggris dan Jepang Universitas  X  Bandung dalam menanggulangi stres 
yang muncul sebagai akibat pembuatan skripsi. 
I. 4. Kegunaan Penelitian 
I. 4. 1. Kegunaan Ilmiah 
Kegunaan ilmiah penelitian ini adalah mengembangkan wawasan dalam 
ilmu Psikologi Pendidikan mengenai strategi penanggulangan stres yang 
digunakan oleh mahasiswa yang sedang membuat skripsi. 
Diharapkan juga penelitian ini dapat dijadikan acuan untuk peneliti 
berikutnya yang berminat dan ingin melakukan penelitian yang serupa, beserta 
pengembangannya. 
I. 4. 2. Kegunaan Praktis 
  Memberi informasi kepada mahasiswa Fakultas Sastra Jurusan Bahasa Inggris 
dan Jepang Universitas  X  Bandung mengenai strategi yang dipakai untuk 
menanggulangi stres yang muncul pada saat membuat skripsi, yang dapat 
dimanfaatkan dalam rangka membantu menanggulangi stres yang muncul 
pada saat mengerjakan skripsi dan membantu mahasiswa mengerjakan skripsi 
dengan efektif dan seoptimal mungkin. 
  Memberi informasi kepada pihak universitas dan fakultas yang bersangkutan, 
khususnya dosen pembimbing, strategi penanggulangan stres yang mahasiswa 
Fakultas Sastra Jurusan Bahasa Inggris dan Jepang Universitas  X  Bandung 
gunakan, yang nantinya dapat dipakai sebagai bahan pertimbangan dalam 
membantu dan membimbing mahasiswa yang sedang membuat skripsi. 
I. 5. Kerangka Pemikiran 
 Mahasiswa Fakultas Sastra Jurusan Bahasa Inggris dan Jepang Universitas 
 X  umumnya mulai memasuki dunia perkuliahan pada usia 18 tahun dan dapat 
berlangsung bahkan sampai usia 26 tahun, rentang usia demikian tergolong pada 
tahap masa dewasa awal. Pada usia demikian mahasiswa diharapkan sudah 
mandiri dan terlibat secara sosial (John W. Santrock, 2002). Mahasiswa Fakultas 
Sastra Jurusan Bahasa Inggris dan Jepang Universitas  X  harus belajar 
mengambil keputusan dalam menentukan masa depan perkuliahannya. Dari awal 
mahasiswa belajar menentukan mengambil mata kuliah apa, dan mengalami 
berbagai kesulitan yang mungkin dihadapi pada saat menjalaninya, baik dalam 
belajar, menyelesaikan tugas-tugas, maupun ujian. Pada akhirnya mahasiswa 
dihadapkan pada tugas akhir, yaitu skripsi, suatu tugas besar yang menuntut 
mahasiswa lebih dari sekadar belajar, lalu ujian. Mahasiswa melalui skripsi 
dituntut untuk mampu mengambil keputusan mulai dari menentukan judul, sampai 
memotivasi diri sendiri untuk mengerjakan skripsinya.  

