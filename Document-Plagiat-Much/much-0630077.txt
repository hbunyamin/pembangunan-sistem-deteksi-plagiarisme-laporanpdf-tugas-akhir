Penelitian ini berjudul  Studi Deskriptif tentang Kepuasan Kerja 
Karyawan pada Divisi Sekretariat Perusahaan dan SDM di PT. X  Bandung.  
Tujuan dari penelitian ini adalah memperoleh gambaran yang rinci mengenai 
kepuasan kerja pada karyawan divisi sekertariat perusahaan dan SDM PT. X  
Bandung melalui ketujuh aspek kepuasan kerja. 
Penelitian ini menggunakan teori kepuasan kerja, menurut John M. 
Ivancevich kepuasan kerja adalah sikap orang terhadap pekerjaan mereka, yang 
merupakan hasil dari persepsi mereka tentang pekerjaan mereka dan tingkat 
kesesuaian antara individu dengan organisasi.Aspek kepuasan kerja menurut 
John M.Ivanchevich yaitu sistem pembauaran, pekerjaan itu sendiri, kesempatan 
promosi, supervisi, rekan kerja, kondisi pekerjaan dan keamanan dalam bekerja. 
Rancangan penelitian yang digunakan adalah metode deskriptif. Populasi 
sampel adalah seluruh karyawan divisi sekertariat perusahaan dan SDM PT. X  
Bamdung. Alat ukur yang digunakan untuk menjaring informasi kepuasan kerja 
disusun oleh peneliti berdasarkan teori Ivancevich & Mateson tahun 2002. Data 
yang diperoleh diolah menggunakan distribusi frekuensi dan tabulasi silang. 
Berdasarkan hasil uji validitas kuesioner kepuasan kerja berkisar antara 0,404 
hingga 0,844, sedangkan reliabilitas kuesioner kepuasan kerja adalah 0,956. 
Berdasarkan hasil penelitian sebagian besar karyawan divisi sekertariat 
dan SDM PT. X  Bandung memiliki kepuasan kerja yang rendah. Faktor yang 
paling berkaitan dengan kepuasan kerja adalah faktor nilai, saran teoritis untuk 
penelitian ini adalah peneliti dapat mengkaitkan variabel yang dapat berkaitan 
dengan kepuawan kerja,meneliti subjek penelitian yang lebih bervariatif saran 
praktis untuk penelitian ini adalah karyawan disarankan untuk dapat 
memperhatikan kesesuaian tingkat pendidikan dengan pekerjaan dan sistem 
imbalan, juga karyawan disarankan untuk lebih memahami jenis pekerjaa yang 
diberikan, untuk meningkatkan kepuasan kerja pada aspek pekerjaan itu sendiri. 
BAB II  TINJAUAN PUSTAKA      20 
BAB III METODOLOGI PENELITIAN     30 
Table 3.4. Kisi-kisi Alat Ukur kepuasan Kerja            33 
Table 3.4.3. Penilaian Item               30 
I.I.Latar Belakang Masalah 
Era globalisasi mempunyai dampak dalam dunia usaha. Globalisasi 
menimbulkan persaingan yang ketat diantara perusahaan-perusahaan untuk 
mendapatkan pangsa pasar yang dibidiknya. Dengan adanya globalisasi maka dunia 
usaha didorong untuk mencapai suatu organisasi perusahaan yang efektif dan efisien. 
Keefektifan dan keefesienan dalam suatu perusahaan sangat diperlukan agar 
perusahaan dapat memiliki daya saing maupun keunggulan lebih dari para pesaing, 
sehingga perusahaan dapat bertahan dalam dunia persaingan yang ketat. 
Pegawai sebagai tenaga kerja berperan dalam perusahaan, sehingga dibutuhkan 
tenaga kerja yang terdidik dan siap pakai untuk mendukung pengembangan 
perusahaan. PT X  mengalami masa sulit ketika krisis ekonomi pada tahun 1998.  PT 
 X  merupakan salah satu perusahaan BUMN yang bergerak di bidang jasa 
telekomunikasi terutama dalam penyediaan sarana dan prasarana jaringan 
Telekomunikasi  (htp : //id Wikipedia.org / wiki/ Telekomunikasi(online) diknas 24 
Februari 2012). Sudah selayaknya bila perusahaan ini lebih memperhatikan kepuasan 
kerja karyawan karena masalah kepuasan kerja sangat berperan dalam kemajuan 
perusahaan. 
PT.  X  merupakan Badan Usaha Milik Negara (BUMN) yang berada di 
bawah Badan Pengelola Industri Telekomunikasi Strategis (BPIS) yang bergerak 
dalam bidang telekomunikasi. PT. X  saat ini memiliki 780 karyawan didalamnya 
yang terbagi kedalam beberapa divisi.  
 Dengan terus meningkatnya kebutuhan akan peralatan komunikasi, maka 
diperlukan suatu industri nasional yang mampu memproduksi alat-alat 
telekomunikasi sendiri. Berdasarkan hal tersebut, dikeluarkan surat Keputusan 
Menteri Keuangan Republik Indonesia No. Kep 117/MK/IV/12/1974 tanggal 28 
Desember 1974, disusul dengan Akte Notaris Abdul Latief Jakarta No. 332 mengenai 
peresmian suatu Industri telekomunikasi Nasional pada tanggal 30 desember 1974 
yaitu PT  X . 
Struktur organisasi PT X  terdiri atas 8 divisi yaitu. Internal Audit yang 
bertugas untuk memeriksa laba rugi perusahaan dan menghubungkan antara 
perusahaan dengan konsumen, Pusat Pengembangan Bisnis dan Produk bertugas 
untuk mengontrol produk dengan menjaga kualitas dan melakukan inovasi apabila 
dibutuhkan, Divisi Sekretariat Perusahaan dan SDM bertugas untuk menempatkan 
karyawan pada bidangnya dan mengurus administrasi perusahaan, mengelola 
kegiatan perusahaan, Divisi Keuangan mengatur dan bertanggung jawab untuk 
masalah administrasi perusahaan. 
PT. X  harus bekerja seoptimal mungkin agar mampu meningkatkan omset 
penjualan, yang pada akhirnnya akan meningkatkan laba perusahaan sehingga 
kelangsungan hidup perusahaan dapat dipertahankan. Salah satu aktivitas yang 
mempengaruhi kelangsungan hidup PT. X  adalah aktivitas penjualan. Aktivitas 
penjualan merupakan tulang punggung dalam perusahaan karena hasil dari penjualan 
menentukan besarnya laba atau rugi perusahaan. Laba maksimal dicapai dengan 
memaksimalkan aktivitas penjualan. Sehingga, pengawasan dan penempatan 
karyawan sesuai dengan bidangnya sangat diperlukan dan hal ini bisa ditunjang 
apabila kegiatan pendistribusian pegawai berjalan dengan efektif dan efisien. 
Penjualan merupakan perkiraan utama yang mempengaruhi laba atau rugi 
perusahaan. Keahlian karyawan dapat berpengaruh pada kesalahan dalam pelaporan 
yang akan mengakibatkan kesalahan dalam menghitung laba atau rugi perusahaan. 
Laba merupakan faktor yang sangat menentukan dalam mempertahankan 
kelangsungan hidup suatu perusahaan. 
Tujuan divisi sekertariat perusahaan dan SDM adalah mengetahui informasi 
tentang kepegawaian, seperti perkembangan karir, keahlian karyawan, latar belakang 
pendidikan karyawan, promosi pengangkatan status karyawan,  menempatkan 
pegawai dibidang yang sesuai dengan kompetensi yang dimiliki, menyediakan 
pegawai yang dibutuhkan dengan jumlah yang sesuai dengan  kebutuhan, dengan cara 
menyediakan training, membantu menilai kinerja pegawai dalam melaksanakan 
tanggung jawab mereka dengan cara menyajikan analisis, penilaian, rekomendasi, 
dan komentar-komentar penting mengenai kegiatan pegawai.  
Divisi sekretariat perusahaan dan SDM berhubungan dengan semua tahap 
kegiatan perusahaan,  sehingga harus memahami permasalahan-permasalahan dan 
kebijakan manajemen. Pihak manajemen sebagai pihak pengambil keputusan 
memerlukan bantuan sekretariat perusahaan dan SDM dalam menentukan kebijakan 
yang tepat sesuai dengan kondisi yang ada dalam perusahaan berdasarkan analisis, 
penilaian serta saran-saran yang objektif agar tujuan perusahaan dapat tercapai. Untuk 
mencapai tujuan perusahaan tersebut manajemen seharusnya memperhatikan segala 
aspek dalam perusahaan.  
Menurut wawancara awal dengan menejer SDM diketahui bahwa pada 
kenyataannya kegiatan pendistribusian pegawai tersebut belum dapat dilakukan 
sesuai dengan struktur yang ada, hal tersebut terjadi karena karyawan mengalami 
beberapa kendala yaitu terlalu  banyaknya tugas yang diberikan, belum siapnya 
sumberdaya manusia untuk ditempatkan di suatu jabatan yang baru, karena kurang 
tersedianya training untuk karyawan  yang sudah bekerja di PT X  itu sendiri, 
kurangnya kerja sama antar sesama karyawan, kurangnya objektifitas penilaian 
kinerja karyawan, berkurangnya tunjangan untuk karyawan sehingga mengakibatkan 
kurangya motivasi karyawan untuk bekerja lebih giat. Fenomena tersebut 
menjelaskan bahwa pelaksanaan pengembangan karir di divisi sekertariat dan SDM 
PT. X  belum dapat dilaksanakan dengan terstruktur sehingga berakibat pada 
kurangnya informasi yang tepat mengenai kepegawaian, tidak sesuainya penempatan 
karyawan dengan kompetensi karyawan, kurangnya persediaan jumlah karyawan 
yang dibutuhkan, menurunnya produktivitas karyawan, meningkatnya absenteeism 
yang berakibat pada tidak selesainya pekerjaan pada waktunya atau dikerjakan oleh 
karyawan lain sehingga hasilnya tidak maksimal. Diketahui dari hasil wawancara, 
pada bulan Juni 2011 hingga September 2011 dilihat dari keseluruhan karyawan yaitu 
sebanyak 30 karyawan, telah didapatkan jumlah absen meningkat dibandingkan 
dengan bulan-bulan sebelumnya sehingga banyak pekerjaan yang tidak dapat 
diselesaikan tepat waktu dan berpengaruh pada bulan berikutnya. 
Ivancevich & Matteson  (2002) menyatakan bahwa kepuasan kerja adalah 
sikap seseorang terhadap pekerjaan mereka. Hal tersebut dihasilkan dari persepsi 
mereka mengenai pekerjaan mereka dan tingkat kesesuaian antara individu dan 
organisasi.Terdapat aspek-aspek kepuasan kerja yaitu sistem pembayaran, pekerjaan 
itu sendiri, kesempatan promosi, supervisi, rekan kerja, kondisi pekerjaan dan 
keamanan dalam bekerja. Terdapat efek ketidak puasan kerja menurut ivancevich & 
Matteson (2002) dapat dilihat dari turn over, absenteeism dan kinerja karyawan.  
Dilakukan survei awal mengenai efek kepuasan kerja yang terjadi pada 
karyawan divisi sekertariat perusahaan dan SDM PT. X , akibat dari aspek 
ketidakpuasan kerja, Aspek yang pertama adalah sistem imbalan, yaitu jumlah 
pembayaran yang diterima dan tingkat kesesuaian anatara pembayaran tersebut 
dengan pekerjaan yang dilakukan. Dilakukan survei awal kepada 10 karyawan, 
terdapat 10% karyawan menghayati cukup dengan gaji dan tunjangan yang di 
peroleh, terdapat 90% karyawan lainnya menghayati gaji yang didapat dirasa kurang 
untuk memenuhi kebutuhan hidup sehari-harinya, sehingga karyawan mencari 
penghasilan lain, dengan membuka usaha, meminjam uang dari koperasi, dan 
menggunakan kartu kredit. Menurut wawancara dengan kepala subbagian 
kepegawaian PT. X , PT. X  memberikan kebijakan berupa gaji pada karyawannya 
sesuai dengan standar gaji pada perusahaan BUMN, namun tunjangan yang semakin 
lama semakin berkurang karena perusahaan mengirit biaya untuk menambah biaya 
produksi, sehingga karyawan mencari penghasilan tambahan, pinjaman koprasi atau 
kartu kredit. Karyawan merasa kebutuhannya semakin bertambah sepetri biaya untuk 
sekolah anak ke jenjang yang lebih tinggi, kendaraan yang lebih bagus dari 
sebelumnya, biaya menikahkan anak, biaya pembanguan rumah dan biaya lainnya. 
Aspek yang kedua adalah aspek pekerjaan itu sendiri, sejauh mana tugas 
pekerjaan yang dianggap menarik dan memberikan kesempatan untuk belajar dan 
menerima tanggung jawab. Pada pengambilan data awal yang dilakukan pada 10 
karyawan divisi sekertariat perusahaan dan SDM, terdapat 80% karyawan 
menghayati mendapatkan pekerjaan itu sulit, sehingga karyawan bertahan bekerja 
diperusaan ini meskipun pekerjaannya dianggap tidak menarik karena tidak sesuai 
dengan latar belakang pendidikannya, terdapat 20% karyawan lainnya menghayati 
bahwa pekerjaannya memiliki kesempatan untuk belajar dengan mengadakan training 
bagi karyawan baru maupun karyawan lama. Menurut hasil wawancara kebijakan 
perusahaan mengenai pekerjaan itu sendiri yaitu PT. X  menyediakan pelatihan 
namun tidak pada semua karyawan hanya pada karyawan baru dan sebagian 
karyawan lama yang dipercaya untuk mengerjakan suatu proyek tertentu. 
Aspek ketiga adalah kesempatan promosi yaitu ketersediaan kesempatan 
untuk maju. Dari hasil pengambian data awal pada 10 karyawan terdapat 90% 
karyawan menyatakan bahwa dirinya memiliki peluang untuk naik jabatan, karena 
kenaikan jabatan akan diadakan 5 tahun sekali dan semua akan mendapatkan giliran, 
hal tersebut sesuai dengan hasil wawancara awal diketahui kebijakan PT. X  
mengenai kesempatan promosi yaitu memberikan peluang untuk kenaikan jabatan 
karyawan setiap 5 tahun sekali, yang dipilih secara acak dan semua karyawan akan 
mendapatkan giliran, menurut hasil wawancara hal tersebut tidak memotivasi 
karyawan untuk kerja lebih giat karena karyawan akan mendapat giliran yang sama 
bagaimanapun cara kerjanya. Terdapat 10% karyawan menghayati bahwa ada 
kesempatan promosi apabila ada jabatan yang kosong salahsatunya karena pensiun. 
Aspek yang keempat adalah supervisi  yaitu kompetensi teknis dan skils 
interpersonal dari atasan langsung. Terdapat 70% karyawan menghayati bahwa atasan 
tidak menilai proses ketika karyawan bekerja, hal tersebut dirasakan karyawan bahwa 
pekerjaannya tidak dilihat apabila mengerjakan pekerjaan dengan baik, dan terdapat 
30% karyawan lainnya menghayati adanya penilaian langsung atas proses kerja dan 
hasil kerja namun hasil penilaiannya rata-rata baik meskipun pada kenyataannya tidak 
demikian, hal tersebut dirasakan karyawan bahwa karyawan yang bekerja dengan 
sedikit usahapun akan dinilai baik. Diketahui kebijakan PT. X , mengenai  supervisi 
yang didapat dari hasil wawancara awal bahwa ada dewan jabatan yang akan menilai 
proses kerja dan hasil kerja namun hasilnya rata-rata baik, dan dikemukakan oleh 
kepala subbagian kepagawaian PT. X  bahwa kenyataannya tidak selalu sama 
dengan hasil penilaian. 
Aspek yang kelima adalah rekan kerja, sejauh mana rekan kerja bersahabat, 
kompeten, dan memberikan dukungan. Terdapat 60% karyawan menghayati rekan 
kerja kurang kompeten dalam melakukan pekerjaannya dan terdapat 40% karyawan 
lainnya menghayati rekan kerja membantu pekerjaannya. Dari wawancara awal 
diketahui kebijakan PT. X" mengadakan kegiatan diluar kantor namun waktunya 
tidak tentu hal tersebut salah satu tujuannya untuk memberikan kesempatan pada 
karyawan agar lebih mengenal karyawan lainnya sehingga diharapkan karyawan 
memiliki rasa solidaritas terhadap karyawan lainnya. 
Aspek yang keenam adalah kondisi pekerjaan yaitu sejauh mana lingkungan 
kerja fisik nyaman dan mendukung produktivitas. Terdapat 30% karyawan yang 
menghayati tempat kerjanya nyaman dan terdapat 70% karyawan lainnya menghayati 
bahwa tempat kerjanya bising karna berdekatan dengan meja kerja karyawan lainnya 
dan sempit, sehingga kurang adanya privasi hal ini didapat dari penghayatan 
karyawan. Berdasarkan hasil observasi di PT. X  ruangan dibagi menjadi tiga bagian 
dan dalam satu bagian terdapat tiga sampai lima meja kerja sehingga terkesan sempit. 
Aspek yang ketujuh adalah keamanan dalam pekerjaan yaitu keyakinan bahwa 
posisi yang relatif aman dan berpeluang untuk tetap bekerja dalam organisasi. 
Terdapat 100% karyawan merasa tidak akan kehilangan pekerjaannya karena sudah 
menjadi pegawai tetap. Menurut wawancara awal diketahui bahwa kebijakan PT. X  
mengenai PHK tidak pernah dilakukan kepada karyawan, kecuali karyawan yang 
bermasalah, secara normatif PHK telah diatur dalam UU 13/2003 yakni ada 15 pintu 
PHK demikian juga dengan Uang Pesangon. 
Berdasarkan data yang didapat dari PT. X  terdapat jumlah turn over karyawan 
divisi sekertariat perusahaan dan SDM  pada bulan September 2010 sampai dengan 
bulan April 2011, jumlah karyawan yang masuk sebanyak 25 karyawan dan yang 
keluar sebanyak 11 karyawan, dan telah didapat data absenteeism 6-7% dalam 
sebulan. Sedangkan ideal yang ditentukan perusahaan  0,1-0,75% dari 41 karyawan. 
Berdasarkan latar belakang fenomena diatas mengenai sikap karyawan 
terhadap pekerjaannya, membuat peneliti merasa kepuasan kerja penting untuk diteliti 
karena dapat mempengaruhi job performance, absenteeism dan  turnover, pada 
karyawan yang dapat mempengaruhi kelangsungan hidup perusahaan. Oleh karena itu 
peneliti  tertarik untuk melakukan penelitian mengenai  Studi Deskriptif tentang 
Kepuasan Kerja Karyawan pada Divisi Sekretariat Perusahaan dan SDM di PT. X  
Bandung.  
1.2.Identifikasi Masalah 
Identifikasi masalah dari penelitian ini adalah untuk mengetahui kepuasan 
kerja karyawan pada divisi sekertariat perusahaan dan SDM di PT. X  Bandung. 
1.3.Maksud dan Tujuan Penelitian 
Maksud dari penelitian ini adalah memperoleh gambaran mengenai kepuasan 
kerja karyawan pada divisi sekertariat perusahaan dan SDM di PT. X  Bandung. 
Tujuan dari dilakukannya penelitian ini adalah memperoleh gambaran yang 
rinci mengenai kepuasan kerja pada karyawan divisi sekertariat perusahaan dan SDM 
di PT. X  Bandung, serta ketujuh aspeknya.  
1.4.Kegunaan Penelitian 
Kegunaan ilmiah dari penelitian ini adalah : 
1. Memberikan informasi pada bidang ilmu psikologi, khususnya psikologi Industri 
Organisasi, mengenai gambaran kepuasan kerja karyawan pada divisi sekertariat 
perusahaan dan SDM di PT. X  Bandung.  
2. Memberikan sumbangan informasi mengenai gambaran kepuasan kerja karyawan 
kepada peneliti-peneliti lainnya yang tertarik untuk meneliti lebih lanjut. 
Kegunaan praktis dari penelitian ini adalah : 
1. Memberikan masukan kepada perusahaan mengenai aspek kepuasan kerja mana 
yang rendah pada karyawan divisi sekertariat perusahaan dan SDM di PT.  X , 
sehingga dapat meningkatkan aspek kepuasan kerja yang rendah dan 
mempertahankan aspek kepuasan kerja yang tinggi, dengan cara memberikan 
hasil penelitian kepada PT X  dan mempresentasikannya, untuk memperkecil 
tingkat kemangkiran karyawan.  
2. Memberi tambahan informasi kepada karyawan sebagai bahan feedback sehingga 
dapat meningkatkan produktivitas karyawan. 
1.5.Kerangka Pemikiran 
Dalam suatu organisasi terdapat sumber daya manusia yang merupakan 
karyawan, maju mundurnya suatu organisasi tergantung dari kinerja karyawannya. 
Menurut ivancevich (2002), agar tujuan suatu organisasi dapat tercapai secara efektif, 
organisasi dituntut untuk dapat memberikan kepuasan kerja kepada karyawannya 
sehingga dapat meningkatkan produktivitas karyawan. 
Karyawan bekerja disatu perusahaan didorong oleh adanya kebutuhan, nilai 
dan harapan. Kebutuhan, nilai dan harapan yang kemudian mempengaruhi persepsi 
terhadap kepuasan kerja yang dimiliki karyawan. Oleh karena itu organisasi dan juga 
karyawan diharapkan bekerja sama untuk saling menguntungkan dan mencapai tujuan 
bersama (Allport&rekannya dalam Robbins 1996). 
Seperti halnya PT.  X  yang berdiri pada 30 desember 1974 merupakan 
perusahaan Badan Usaha Milik Negara (BUMN) yang berada di bawah Badan 
Pengelola Industri Telekomunikasi Strategis (BPIS) yang bergerak dalam bidang 
telekomunikasi, oleh sebab itu perusahaan membutuhkan karyawan untuk dapat 
mencapai tujuan perusahaan. Karyawan divisi secretariat perusahaan dan SDM di 
PT. X  harus bekerja dengan baik dalam penempatan karyawan-karyawan, pencarian 
informasi mengenai karyawan, mengetahui kemampuan, keahlian dan minat 
karyawan, hal tersebut sangat penting untuk kemajuan perusahaan, karena enempatan 
karyawan pada posisi yang tepat akan mempengaruhi performance kerja yang baik, 
sehingga mendukung harapan perusahaan. Namun karna kebutuhan karyawan 
berbeda-beda maka tidak selalu sesuai dengan  harapan. Begitu juga karyawan yang 
bekerja di PT. X  khususnya divisi sekertariat perusahaan dan SDM didorong oleh 
adanya kebutuhan, nilai dan harapan yang berbeda-beda, 
Menurut Abraham Maslow (Robbins, 1996) mengatakan bahwa kebutuhan 
suatu individu dilihat dalam  hirarki. Hirarki yang pertama merupakan kebutuhan 
fisiologis, yang kedua keamanan, yang ketiga sosial, yang keempat penghargaan, dan 
yang kelima aktualisasi diri. Nilai merupakan keyakinan-keyakinan dasar bahwa 
suatu perilaku akan lebih dapat diterima daripada suatu perilaku atau keadaan akhir 
dari eksistensi yang khas lebih dapat disukai secara pribadi atau sosiall daripada suatu 
perilaku atau keadaan akhir eksistensi yang berlawanan atau kebalikannya menurut 
Alport & rekannya(Robbins, 1996). Harapan adalah suatu tindakan akan diikuti oleh 
suatu keluhan tertentu dan pada daya tarik dari keluhan tertentu bagi individu tersebut 
menurut Alport & rekannya (Robbins,1996). 
Kebutuhan, nilai dan harapan karyawan divisi sekertariat perusahaan dan 
SDM PT. X  kemudian mempengaruhi persepsi kepuasan kerja karyawan. Menurut 
Ivancevich (2002), kepuasan kerja adalah sikap orang terhadap pekerjaan mereka 
yang merupakan hasil dari persepsi seseorang terhadap pekerjaannya dan tingkat 
kesesuaian antara individu dengan organisasi. kepuasan kerja tersebut diukur 
berdasarkan tujuh aspek. Aspek yang pertama yaitu  sistem imbalan yaitu besarnya 
pembayaran yang diterima, derajat sejauh mana pembayaran memenuhi harapan-
harapan karyawan, dan bagaimana pembayaran diberikan. Dalam PT. X  ini berupa 
gaji yang diterima, uang lembur, uang makan, bonus / insentif, uang transportasi, 
tunjangan kesehatan berupa asusansi kesehatan, dan tunjangan jabatan. 
Aspek yang kedua adalah pekerjaan Itu sendiri, yaitu tingkat dimana sebuah 
pekerjaan menyediakan tugas yang menyenangkan, kesempatan belajar dan 
kesempatan untuk mendapatkan tanggung jawab. Dalam PT X  berupa pekerjaan 
yang menarik, jumlah tugas, kesulitan pekerjaan, tanggung jawab yang diberikan dan 
pengembangan diri. 
Aspek yang ketiga adalah kesempatan promosi yaitu kesempatan untuk 
mengembangkan diri dan memperluas pengalaman kerja, atau naik jabatan. Dalam 
PT X  ini berupa sistem kenaikan jabatan, sosialisasi, waktu kenaikan jabatan, 
kriteria dan juga promosi jabatan. 
Aspek yang keempat adalah supervisi yaitu persepsi karyawan mengenai 
kemampuan supervisor untuk menyediakan bantuan teknis dan perilaku dukungan, 
hubungan fungsional dan hubungan keseluruhan yang positif memberikan tingkat 
kepuasan kerja yang paling besar dengan atasan. Dalam PT X   aspek supervisi ini 
berupa pengarahan oleh atasan, pengawasan dari atasan feedback dari atasan. 
Aspek yang kelima adalah rekan kerja yaitu rekan kerja yang ramah, 
kompeten dan mendukung keberhasilan seseorang dalam melaksanakan tugas-
tugasnya sangat dipengaruhi oleh interaksi antara orang-orang yang terdapat dalam 
satu satuan kinerja tertentu. Dalam perusahaan ini berupa dukungan rekan kerja, 
kompetensi rekan kerja dan keramahan rekan kerja. 
Aspek yang keenam adalah kondisi pekerjaan  yaitu sejauh mana lingkungan 
kerja fisik yang nyaman dan mendukung produktivitas seperti kenyamanan tempat 
kerja , ventilasi yang cukup, penerangan, kebersihan, keamanan dan lokasi tempat 
kerja. Dalam perusahaan ini berupa fasilitas komputer, ruangan kerja, fasilitas kamar 
mandi, kantin, mesin fotokopi, alat tulis, tempat parkir dan lift. 
Aspek yang ketujuh adalah keamanan dalam pekerjaan yaitu keyakinan 
individu bahwa posisi atau jabatannya cukup aman, tidak ada rasa khawatir dan 
adanya harapan bahwa tidak aka nada pemutusan hubungan kerja secara sepihak atau 
secara tiba-tiba. Dalam perusahaan ini berupa sistem pensiun dan tunjangan pensiun. 
Menurut Lily M. Berry (1998) terdapat tiga faktor yang mempengaruhi 
kepuasan kerja karyawan terhadap pekerjaannya yaitu usia, pendidikan dan jenis 
kelamin. Faktor yang pertama yang mempengaruhi kualitas kepuasan kerja karyawan 
divisi sekertariat perusahaan dan SDM adalah usia. Karyawan yang berusia lebih tua 
akan semakin puas terhadap pekerjaannya, karyawan tersebut telah mengalami 
perkembangan selama mereka bekerja. Faktor yang kedua yang mempengaruhi 
kepuasan kerja pada karyawan divisi sekertariat perusahaan dan SDM PT. X  sesuai 
dengan pendapat Lily M. Berry yaitu pendidikan depat menyebabkan beberapa nilai 
dalam bekerja, dengan mengabaikan tingkat pekerjaan mereka secara organisasional, 
karyawan yang memiliki pendidikan yang lebih tinggi diberikan tugas yang lebih 
berarti dan lebih sering dilibatkan dalam tugas-tugas tersebut dibandingkan dengan 
karyawan yang memiliki pemdidikam yang lebih rendah. Faktor yang ketiga yang 
mempengaruhi kepuasan kerja pada karyawan divisi sekertariat perusahaan dan SDM 
PT. X  adalah jenis kelamin fakta membuktikan bahwa wanita memperoleh gaji yang 
lebih sedikit daripada pria dan juga wanita lebih sedikit mendapat kesempatan bekerja 
daripada pria. Inilah alasan mengapa wanita akan merasa kurang puas dengan 
pekerjaannya. Jika wanita tidak berharap banyak, maka kepuasan mereka akan 
menjadi tinggi (Murny & Akitson 1981 dalam Lily M. Berry). 
Karyawan divisi sekertariat perusahaandan SDM, akan lebih puas dengan 
pekerjaannya yang saat ini dimilikinya apabila sebagian besar aspek-aspek kepuasan 
kerja  mereka telah terpenuhi. Dengan adanya kepuasan terhadap pekerjaan yang 
mereka lakukan, karyawan divisi sekertariat perusahaan dan SDM akan lebih 
berpartisipasi dalam pekerjaan mereka. Kondisi kepuasan kerja yang tinggi yaitu 
performance harus dirasakan sebagai alat dalam memperoleh reward, dan  reward 
harus dirasakan adil, setelah dilakukan penelitian, didapat hasil bahwa korelasi antara 
tingkat performance dan kepuasan menjadi positif ketika subjek diberi reward secara 
tepat, dan menjadi negative ketika diberi reward secara tidak tepat (Lawer & Potter 
1967 dalam Lily M. Berry).  
Jika sebagian besar aspek kepuasan kerja dirasakan kurang memuaskan bagi 
karyawan divisi sekertariat perusahaan dan SDM, maka dapat disimpulkan bahwa 
karyawan tersebut akan merasa tidak puas terhadap pekerjaannya, ketidakpuasan ini 
kemudian akan menimbulkan perilaku penarikan diri karyawan divisi sekertariat 
perusahaan dan SDM dari pekerjaan yang ia lakukan. Sejalan dengan teori Ivancevich 
(2002), jika karyawan mempersepsikan adanya ketidak adilan dalam pekkerjaan yang 
serupa yang membutuhkan usaha yang lama karyawan akan mengalami ketidak 
puasan dan mencari cara untuk mengembalikan ketidak adilan baik dengan mencari 
pekerjaan yang lebih baik atau dengan mengurangi usaha. Karyawan divisi sekertariat 
perusahaan dan SDM yang tidak puas  akan lebih memilih untuk tidak terlibat dalam 
pekerjaan atau organisasi yang mempekerjakan mereka. 
Karyawan yang merasa tidak puas dengan pekerjaannya akan cenderung 
memilih absen atau berhenti dari pekerjaannya dibandingkan karyawan yang puas. 
Menurut Lily M. Berry, karyawan yang tidak puas dalam pekerjaannya akan lebih 
sering absen dan karyawan yang tidak puas secara keseluruhan akan memilih untuk 
berhenti dari pekerjaannya. 
 Semakin terpenuhinya harapan karyawan akan menyebabkan karyawan 
semakin bersikap positif terhadap aspek kepuasan kerja,begitu juga semakin tidak 
terpenuhinya harapan karyawan akan semakin bersikap negatif terhadap aspek 
kepuasan kerja. Menurut Ivancevich, kepuasan kerja yang tinggi yaitu sikap positif 
karyawan terhadap pekerjaannya jika yang diharapkan berdasarkan aspek kepuasan 
kerja terpenuhi, dan kepuasan kerja yang rendah adalah sikap negatif karyawan 
terhadap pekerjaannyajika yang diharapkan berdasarkan aspek kepuasan kerja tidak 
terpenuhi (Ivancevich 2002). 
PT. X  
Aspek-aspek  
a. Sistem Pembayaran 
b.Pekerjaan itu Sendiri 
c. Kesempatan promosi 
d.Supervisi 
e. Rekan kerja 
f. Kondisi pekerjaan 
g.Keamanan dalam Pekerjaan 
Dari uraian diatas, maka dapat diambil asumsi sebagai berikut: 
1. Karyawan divisi sekertariat dan SDM PT. X  mempunyai derajat 
kepuasan kerja yang berbeda-beda. 
2. Setiap karyawan divisi sekertariat perusahaan dan SDM PT. X  
memiliki kebutuhan, nilai dan harapan yang berbeda-beda. 
3. Setiap karyawan divisi sekertariat perusahaan dan SDM PT. X  
memiliki persepsi yang berbeda terhadap pekerjaannya berdasarkan 
kesenjangan antara kebutuhan, nilai dan harapan dengan aspek 
kepuasan kerja yang menentukan tinggi rendahnya kepuasan kerja 
karyawan. 
4. Setiap karyawan divisi sekretariat perusahaan dan SDM PT. X  
memiliki tingkat kepuasan kerja yang berbeda-beda terhadap sistem 
pembayaran, pekerjaan itu sendiri, kesempatan promosi, supervisi, 
rekan kerja, kpndisi pekerjaan dan keamanan dalam bekerja.  
Kepuasan kerja dan moral adalah istilah yang sama mengacu pada sejauh 
mana organisasi memenuhi kebutuhan karyawan. Kepuasan kerja adalah sikap orang 
terhadap pekerjaan mereka, yang merupakan hasil dari persepsi mereka tentang 
pekerjaan mereka dan tingkat kesesuaian antara individu dengan organisasi. 
(Ivancavich, 2002) 
Menurut Ivancevich (2002) terdapat aspek-aspek yang terkait dengan kepuasan kerja 
yaitu: 
a. Sistem imbalan (Pay) 
Besarnya pembayaran yang diterima, derajat sejauh mana pembayaran 
memenuhi harapan-harapan karyawan, dan bagaimana pembayaran 
b. Pekerjaan Itu sendiri (Work itself) 
Tingkat dimana sebuah pekerjaan menyediakan tugas yang 
menyenangkan, kesempatan belajar dan kesempatan untuk mendapatkan 
tanggung jawab. 
c. Kesempatan promosi (Promotion opportunities) 
Kesempatan untuk mengembangkan diri dan memperluas pengalaman 
kerja, atau naik jabatan. 
d. Supervisi (Supervision) 
Kompetensi teknis dan perilaku dukungan, hubungan fungsional dan 
hubungan keseluruhan yang positif memberikan tingkat kepuasan kerja 
yang paling besar dengan atasan. 
e. Rekan kerja (Coworker) 
Sejauhmana rekan kerja yang ramah, kompeten dan mendukung 
keberhasilan seseorang dalam melaksanakan tugas-tugasnya sangat 
dipengaruhi oleh interaksi antara orang-orang yang terdapat dalam satu 
satuan kinerja tertentu. 
f. Kondisi pekerjaan (Working condition) 
Sejauh mana lingkungan kerja fisik yang nyaman dan mendukung 
produktivitas seperti kenyamanan tempat kerja , ventilasi yang cukup, 
penerangan, kebersihan, keamanan dan lokasi tempat kerja. 
g. Keamanan dalam pekerjaan yaitu (Job security) 
Keyakinan individu bahwa posisi atau jabatannya cukup aman, tidak ada 
rasa khawatir dan adanya harapan bahwa tidak akan ada pemutusan 
hubungan kerja secara sepihak atau secara tiba-tiba. 
Menurut Maslow kebutuhan suatu individu dilihat dalam sebuah 
hirarki. Hirarki yang pertama merupakan kebutuhan fisiologis yaitu 
kebutuhan akan udara, makanan, minuman dan sebagainya, kebutuhan ini 
dinamakan juga kebutuhan dasar (basic needs) jika kebutuhan dasar ini relatif 
sudah tercukupi, muncullah kebutuhan yang lebih tinggi yaitu kebutuhan akan 
rasa aman (safety needs).  Jenis kebutuhan yang kedua ini berhubungan 
dengan jaminan keamanan, stabilitas, perlindungan, struktur, keteraturan, 
situasi yang bisa diperkirakan, bebas dari rasa takut dan cemas dan 
sebagainya. Setelah kebutuhan dasar dan rasa aman relatif dipenuhi, maka 
timbul kebutuhan untuk dimiliki dan dicintai (belongingness and love needs). 
Setiap orang ingin mempunyai hubungan yang hangat dan akrab, bahkan 
mesra dengan orang lain, jika kebutuhan tingkat ketiga relatif sudah terpenuhi, 
maka timbul kebutuhan akan harga diri (esteem needs). Yang kelima adalah 
kebutuhan aktualisasi diri jika kebutuhan tidak terpenuhi maka akan terjadi 
seperti apatisme, kebosanan, putus asa, tidak punya rasa humor lagi, 
keterasingan, mementingkan diri sendiri, kehilangan selera dan sebagainya. 
 Nilai merupakan keyakinan-keyakinan dasar bahwa suatu perilaku 
akan lebih dapat diterima daripada suatu perilaku yang berlawanan atau 
kebalikannya. Menurut Allport & rekannya (Robbins, 1996) mengidentifikasi 
nilai kedalam enam tipe yaitu yang pertama nilai teoritis menganggap sangat 
penting penemuan kebenaran melalui satu pendekatan kritis dan rasional, 
yang kedua nilai ekonomis merupakan nilai yang menekankan pada kegunaan 
praktis, yang ketiga adalah nilai estetis yaitu nilai yang menaruh nilai tertinggi 
pada bantuk dan keselarasan, yang keempat adalah nilai sosial memberikan 
nilai tertinggi pada kecintaan pada orang-orang, yang kelima adalah nilai 
politis yaitu nilai yang menaruh tekanan pada diperolehnya kekuasaan dan 
pengaruh. Nilai yang keenam adalah nilai religious merupakan peduli akan 
kesatuan pengalaman dan pemahaman sebagai keseluruhan. Harapan adalah 
suatu tindakan akan diikuti oleh suatu keluhan tertentu dan pada daya tarik 
dari keluhan tertentu bagi individu tersebut. 
Terdapat tiga faktor yang dapat mempengaruhi kepuasan kerja 
seseorang terkadap pekerjaannya yaitu (Lily M.Berry, 1991) : 
Terdapat hubungan yang positif antara usia dan kepuasan kerja. 
Pekerja yang berusia lanjut akan lebih puas terhadap pekerjaannya daripada 
pekerja yang berusia lebih muda (Rodesm 1983 dalam Lily M. Berry). Bukti 
ini menunjukan bahwa orang yang merasa dirinya memiliki lebih sedikit 
alternatif pekerjaan lain akan lebih puas terhadap pekerjaannya. Salah satu 
alasannya bahwa nilai-nilai dari pekerjaan yang berusia lanjut telah berubah 
selama mereka bekerja dan berpengaruh untuk bekerja ditempat lain tidak 
sekuat dibandingkan dengan pekerja yang berusia lebih muda. 
Pekerja yang berusia lebih lanjut juga akan semakin puas terhadap 
tingkat pekerjaannya karena telah terjadi perkembangan selama mereka 
bekerja, sebagian besar perkembangan pekerjaan terdiri dari tiga tingkat : 
tingkat awal dimana pekerjaan baru berkembang, tingkat pertengahan dimana 
pekerjaan mulai maju, dan tingkat akhir dimana pekerjaan dipertahankan. 
Terdapat asumsi bahwa kebutuhan nilai dan harapan akan berubah ketika 
individu bergerak melalui ketiga tingkat pekerjaan tersebut. Pekerjaan itu 
sendiri akan lebih memuaskan secara instrinsik pada tingkat akhir dan hal ini 
menjadi alasan mengapa individu menjadi lebih puas dengan pekerjaan 
mereka pada saat mereka bertambah tua. 
   Secara umum dipercaya bahwa pendidikan merupakan peranan 
penting untuk mendapatkan pekerjaan yang lebih baik, namun dalam 
kenyataannya pendidikan diperlukan untuk mendapatkan tingkat pekerjaan 
dan gaji yang lebih tinggi, pada saat ini lebih banyak orang yang 
berpendidikan tinggi yang bekerja sebagai buruh, hali ini disebabkan karena 
kurang tersedianya pekerjaan yang sesungguhnya tidak memerlukan 
pendidikan yang tinggi, tetapi dilakukan oleh orang-orang lulusan perguruan 
tinggi. 
Penelitian menunjukan bahwa karyawan yang memiliki kedudukan 
yang lebih tnggi, diberi tugas yang lebih berarti dan lebih sering dilibatkan 
dalam tugas-tugas tersebut dibandingkan dengan karyawan yang memiliki 
pendidikan yang lebih rendah. Ketidak puasan juga lebih mungkin dirasakan 
jika nilai-nilai instrinsik dalam bekerja banyak yang tidak tercapai seperti 
tingkat pekerjaan yang rendah. 
 -  Jenis Kelamin 
Fakta menunjukan bahwa wanita memperoleh gaji yang lebih sedikit 
daripada pria dan juga wanita lebih sedikit mendapatkan kesempatan bekerja 
pada wanita lebih terbatas daripada pria. Inilah alasan mengapa wanita akan 
merasa kurang puas terhadap pekerjaannya. Jika wanita tidak berharap 
banyak, maka kepuasan mereka akan menjadi tinggi (Murny & Atkinson 1981 
dalam Lily M. Berry). 
 Berbagai dampak yang muncul dari kepuasan kerja adalah : 
1) Kepuasan dan produktivitas 
Pada kondisi tertentu kita dapat beranggapan bahwa kepuasan dan 
produktivitas berhubungan secara positif, tetapi hal ini tidak berguna untuk 
semua situasi. Lawer & Potter (1967 dalam Lily M. Berry) mengemukakan 
dua kondisi kepuasan tinggi yaitu performance harus dirasakan sebagai alat 
dalam memperoleh reward, dan reward harus dirasakan adil, setelah 
dilakukan penelitian , didapat hasil bahwa korelasi antara tingkat performance 
dan kepuasan menjadi positif ketika subjek diberi reward secara tepat, dan 
menjadi negatif ketika diberi reward secara tidak tepat.  
Pada level individual dikatakan bahwa terdapat hubungan antara 
performance dan kepuasan, yang menarik adalah bahwa pada tingkat 
organisasi ditemukan bukti baru mengenai kepuasan kerja, jika kepuasan kerja 
dan level produktivitas diambil dari perusahaan secara keseluruhan didapat 
hasil bahwa perusahaan dengan karyawan yang lebih puas cenderung lebih 
efektif daripada perusahaan yang kurang puas (Robbins, 2003)  
2) Kepuasan dan perilaku penarikan diri 
Manajer perusahaan sangat khawatir dengan penarikan diri yang 
dilakukan oleh karyawan, karena membutuhkan biaya yang tinggi ketika 
karyawan absen dari pekerjaannya karyawan tidak melakukan proses produksi 
dan perusahaan tetap harus membayar karyawan untuk waktu lembur mereka, 
serta ketika karyawan berhenti dari pekerjaannya, perusahaan harus mengerti 
dengan karyawan yang baru atau rekruitment, dan melakukan training terlebih 
dahulu. 
Terdapat asumsi bahwa akibat dari ketidak puasan kerja adalah penarikan 
diri dari pekerjaannya karyawan yang tidak puas lebih sering absen atau berhenti 
daripada karyawan yang puas dengan pekerjaannya, namun Porter 7 Steers (1973 
dalam Lily M. Berry) mengatakan bahwa absenteeism adalah tingkah laku yang 
spontan dan mungkin tidak berhubungan dengan kepuasan kerja. Disisi lain, 
keluarnya karyawan hal yang serius dan bisa merefleksikan kepuasan kerja. 
Dari penelitian yang telah dilakukan dikatakan bahwa hubungan antara 
kepuasan kerja dengan absenteeism tidak terlalu kuat. (Scott & Tylor 
dalam Lily M. Berry) menyatakan bahwa hubungan yang kuat dapat 
dilihat jika frekuensi dan durasi karyawan , absen dievaluasi dari segi 
kepuasan kerja. Mereka menemukan bahwa seringnya absen akan lebih 
kuat hubungannya pada kepuasan kerja secara keseluruhan, kepusan 
dengan rekan kerja dan kepuasan dengan pekerjaan itu sendiri. pada 
pandangan lain, (Hacket 1989, dalam Lily M. Berry) mengatakan bahwa 
karyawan yang tidak puas dengan pekerjaan itu sendiri akan lebih sering 
absen, dan karyawan yang tidak puas dengan pekerjaannya secara 
keseluruhan akan absen lebih lama. 
Turn over merupakan orang meninggalkan satu orang untuk 
alternative tempat lain, menurut Ivancevich dan Matteson, jika manajer 
bisa mengembangkan sistem reward yang mempertahankan karyawan 
terbaik, dan yang mengeluarkan karyawan yang berkinerja buruk dapat 
menyebabkan efektivitas organisasi secara keseluruhan akan membaik.  
 Robbins (2003) mendeskripsikan persepsi dalam kaitannya dengan 
lingkungan, yaitu sebagai proses dimana individu-individu mengorganisasikan dan 
menafsirkan kesan indera mereka agar member makna kepada lingkungan mereka. 
 Robbins (2003) menjelaskan bahwa meskipun individu-individu memandang 
pada satu benda yang sama mereka dapat mempersepsikannya berbeda-beda, ada 
sejumblah factor yang bekerja untuk membentuk dan terkadang memutar balikan 
persepsi. Factor-faktor ini dari ; perilaku persepsi (Percepvier), objek atau yang 
dipersepsikan, dan konteks dari situasi dimana persepsi itu dilakukan. 
Penelitian  ini  menggunakan  metode  deskriptif dengan teknik survei, 
dimana pengambilan data yang akan digunakan oleh peneliti dengan 
menggunakan kuesioner. Metode survei adalah penyelidikann yang diadakan 
untuk memperoleh fakta dari gejala-gejala yang ada dan mencari keterangan-
keterangan secara faktual. 

