# RINGKASAN #

Saat ini permasalahan mengenai penelitian deteksi plagiarisme (DP) untuk bahasa Indonesia adalah 
penekanan aspek teoritis yang menyulitkan aspek praktis, tidak adanya standar corpus untuk menguji teknik DP yang dikembangkan
oleh para peneliti, tidak ada aplikasi DP khusus bahasa Indonesia yang open source sehingga kalangan awam sulit 
untuk mempelajari DP apalagi untuk melakukan instalasi aplikasi atau menggunakannya. 

Penelitian ini bertujuan untuk membangun sistem DP bagi bahasa Indonesia. 
Dokumen sumber bagi sistem DP adalah laporan pdf tugas akhir mahasiswa Fakultas Psikologi Universitas Kristen Maranatha (UKM) 
angkatan 2000 sampai dengan 2012. 
Adapun hasil pertama penelitian yang ingin dicapai adalah penciptaan corpus standar yang dapat digunakan sebagai 
standar pengujian teknik dan metode DPE untuk bahasa Indonesia. 
Hasil kedua adalah sistem DP laporan pdf tugas akhir mahasiswa di perpustakaan Universitas Kristen Maranatha. 
Hasil ketiga penelitian adalah dokumentasi lengkap bagaimana menginstalasi sebuah sistem deteksi plagiarisme laporan pdf tugas
akhir mahasiswa bagi instansi atau perorangan yang memerlukan. 
Harapannya, hasil pertama penelitian dapat menjadi titik awal terbentuknya komunitas penelitian DP untuk Indonesia.

Kata kunci: external plagiarism detection, standard corpus plagiarism detection for indonesian text, laporan pdf tugas akhir mahasiswa

### Untuk apakah repository ini? ###

Repository ini berisi 5 (lima) folder, yaitu  
1. Document-Plagiat-Hardly berisi koleksi dokumen hasil plagiat dengan kadar plagiat 20%.
2. Document-Plagiat-Medium berisi koleksi dokumen hasil plagiat dengan kadar plagiat 50%.
3. Document-Plagiat-Much berisi koleksi dokumen hasil plagiat dengan kadar plagiat 80%.
4. Document-Plagiat-Entirely berisi koleksi dokumen hasil plagiat dengan kadar plagiat 100%.

Setiap file di dalam folder memiliki pasangan xml file sbb:  
`<document reference="<NAMA_FILE_PLAGIAT>">`  
`<feature feature_name="detected-plagiarism"`  
`source_length="<BANYAK_KARAKTER_DI_SOURCE>"`  
`source_offset="<POSISI_AWAL_DI_SOURCE_DOC>"`  
`source_reference="<SOURCE_SUMBER_PLAGIAT>"`  
`this_length="<BANYAK_CHARACTER_DI_DOC_INI>"`  
`this_offset="<POSISI_DI_DOC_PLAGIAT_INI>" />`  
`</document>`

dengan  
- `<NAMA_FILE_PLAGIAT>` : nama file dokumen plagiat  
- `<BANYAK_KARAKTER_DI_SOURCE>` : jumlah karakter di dokumen sumber  
- `<POSISI_AWAL_DI_SOURCE_DOC>` : posisi karakter plagiat pertama di dokumen sumber  
- `<SOURCE_SUMBER_PLAGIAT>` : nama file sumber dokumen yang diplagiat  
- `<BANYAK_CHARACTER_DI_DOC_INI>` : jumlah karakter di dokumen plagiat ini  
- `<POSISI_DI_DOC_PLAGIAT_INI>` : posisi karakter awal di dokumen plagiat ini. 

### Penanggung jawab ###

* Hendra Bunyamin (email: hendra.bunyamin@it.maranatha.edu)