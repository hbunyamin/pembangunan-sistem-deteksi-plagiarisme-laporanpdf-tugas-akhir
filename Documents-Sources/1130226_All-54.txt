P-kom (pembimbing KOMIT) dalam suatu gereja sebenarnya diharapkan 
memiliki religiusitas yang tinggi, agar dapat dijadikan role model bagi anggotanya, 
namun masih ditemukan P-kom yang tidak dapat dijadikan panutan dalam hal 
religiusitas seperti masih memiliki keraguan terhadap apa yang tidak dilihat 
langsung oleh mata, jarangnya melakukan saat teduh, kurang bersyukur atas apa 
yang dimilikinya, serta masih belum dapat menerapkan ajaran agamanya dalam 
kehidupan sehari-hari. Penelitian ini menggunakan teori Religiusitas (Glock & Stark, 
1965) dengan tujuan ingin mengetahui bagaimana derajat dimensi-dimensi 
religiusitas P-kom di Gereja  X  kota Bandung. 
Sebanyak 56 P-kom dikumpulkan menggunakan teknik purposive sampling , 
mengisi inform consent dan diukur religiusitasnya menggunakan (Glock & Stark, 
1965) yang dibuat oleh peneliti. Dimensi I terdiri dari 24 item, dimensi II 10 item, 
dimensi III 20 item, dimensi IV 14 item, dan dimensi V 18 item.Skor yang masuk 
median atas dikategorikan tinggi sedangkan skor yang masuk median bawah 
dikategorikan rendah. Berdasarkan pengolahan data secara statistik menggunakan 
distribusi frekuensi didapatkan hasil sebagian besar dari P-kom gereja  X kota 
Bandung memiliki derajat dimensi religiusitas yang tinggi. Data lain yang ditemukan 
significant berupa jenis kelamin dimana perempuan memiliki derajat yang lebih 
tinggi daripada laki-laki, dan SSE dimana P-kom yang menghayati status sosial 
ekonominya menengah kebawah cenderung memiliki derajat dimensi yang tinggi. 
Diharapkan dengan gambaran ini, gereja  X dapat mengusahakan strategi yang 
lebih baik untuk memertahankan religiusitas para P-kom nya.  
P-Kom (Preceptor of KOMIT) in a church should have a high sense of 
religiosity, so that other members could look up to them. However, there are still 
several P-Kom that do not fit to be role models in terms of religiosity such as having 
doubts toward unseen things, seldom doing reflection moment, not really being 
grateful about thing that they owned, and still cannot apply the Christianteaching in 
daily lives. For the purpose of analyzing religiosity dimensions degree of P-Kom at 
Church  X  Bandung-we use The Theory of Religiosity (Glock&Stark, 1965). 
Purposive sampling was used to gather 56 consented P-Kom and we measure 
each of their Religiosity Dimensions by using ourtests which based on Glock and 
Stark (1965).The test consist of 24 items for the first dimension, 10 items for the 
second dimension, 20 items for the third, 14 items for the fourth,  and 18 items for the 
fifth dimension. All that scores above the median are categorized as high, while those 
below it categorized as low. According to the statistical analysis using Frequency 
Distribution, it was found that most of the P-Kom at Church  X  havehigh sense of 
religiosity. Other significant findings including that the majority of female have 
higher sense of religiosity than male, P-Kom that percept themselves as having 
middle-to-low socioeconomic status tend to have a high sense if religiosity. From this 
research, we hope that Church  X   can develop more effective strategy to main the 
sense of religiosity among their P-Kom. 
Gambaran Subyek Penelitian berdasarkan penghayatannya mengenai status sosial 
Manusia memiliki kebebasan untuk memeluk dan menjalankan agama menurut 
kepercayaannya. Glock & Stark, (1965) mendefinisikan agama sebagai sistem simbol, sistem 
keyakinan, sistem nilai dan sistem perilaku yang terlembagakan, yang kesemuanya berpusat 
pada persoalan   persoalan yang dihayati sebagai paling maknawi. Pengakuan terhadap 
agama di Indonesia telah dituangkan dalam dasar negara yaitu Pancasila, sila pertama 
 Ketuhanan Yang Maha Esa . Menurut hasil revisi dari UUD Administrasi Kependudukan 
(Adminduk) nomor 23 tahun 2006, Indonesia memiliki enam agama yang diakui oleh 
pemerintahnya yaitu Islam, Katholik, Kristen, Hindu, Budha dan Konghuchu.  
Salah satu agama yang diakui Indonesia adalah agama Kristen dengan persentase 
sebesar 6,96 % (Badan Statistik Daerah, 2010). Terkait pentingnya mengaktualisasikan agama 
dalam diri manusia, terdapat beberapa upaya yang dilakukan umat Kristiani untuk lebih 
mengenal Tuhan melalui agamanya. Agama Kristen menyediakan fasilitas untuk individu 
dapat mengenal Tuhan, salah satunya melalui tempat ibadah yaitu Gereja. Kata Gereja berasal 
dari kata Portugis: igreja dan bahasa Yunani: ekklesia yang berarti suatu perkumpulan atau 
lembaga dari agama Kristen.  
  Salah satu Gereja yang mencoba menjadi fasilitator agar umat Kristiani dapat 
mendalami agama adalah Gereja  X  di kota Bandung. Berdasarkan wawancara yang 
dilakukan pada salah seorang pengurus Gereja  X  di Kota Bandung, diperoleh informasi 
bahwa di Gereja ini terdapat upaya untuk membuat manusia tidak hanya sekedar mengetahui 
agamanya, namun bertindak sesuai dengan apa yang diajarkan oleh agama tersebut yaitu 
dengan menerapkan konsep pemuridan. Konsep pemuridan di Gereja  X  kota Bandung 
menerapkan salah satu amanat agung dari Tuhan yaitu  Karena itu pergilah, jadikanlah 
bangsa murid-Ku dan baptislah mereka dalam Bapa dan Anak dan Roh Kudus  (Matius 
28:19). Konsep pemuridan diwujudkan melalui salah satu bentuk komunitas di Gereja  X  
Kota Bandung yang dikenal dengan KOMIT (Komunitas Otentik Murid Tuhan). KOMIT 
adalah suatu komunitas pelayanan di Gereja  X  Kota Bandung dengan metode pertemuan 
yang dilakukan setiap dua minggu sekali dan menerapkan proses mentoring time, proses 
KOMIT dilakukan dengan cara didampingi oleh seorang pembimbing yang membagikan 
bahan-bahan Alkitab untuk sama-sama direnungkan serta mendiskusikan mengenai 
permasalahan-permasalahan yang sedang digumulkan oleh anggota KOMIT. 
Menurut salah seorang pengurus Gereja  X  di Kota Bandung, KOMIT tidak dapat 
berjalan dengan baik apabila tidak ada seorang pembimbing untuk anggotanya. Pembimbing 
dalam KOMIT di Gereja  X  ini disebut P-kom. P-kom adalah orang yang membimbing 
anggotanya dengan menerapkan konsep pemuridan sesuai dengan amanat agung Tuhan 
kepada pemeluk agama Kristen. P-kom adalah pelayanan seumur hidup, dan tidak 
mendapatkan insentive. Jumlah P-kom di gereja  X  saat ini adalah 60 orang (wanita 29 orang 
dan pria 31 orang). Sebelum menjadi P-kom, maka seseorang akan mendapatkan pelatihan 
khusus berupa kelas persiapan yang dilakukan selama 10 kali pertemuan. Materi yang 
diberikan selama pelatihan yaitu pengenalan pelayanan P-kom di KOMIT, panduan untuk 
memulai dan menjalankan KOMIT, hal-hal yang harus dimiliki seorang P-kom, bagaimana 
cara menghadapi anggota ketika sedang mengalami permasalahan, dan dilakukan sharing 
mengenai pengalaman dari P-kom sebelumnya.  
Berdasarkan wawancara lebih lanjut dengan salah seorang pengurus Gereja  X  di Kota 
Bandung beberapa hal yang diharapkan dimiliki oleh P-kom adalah dapat datang ke Gereja 
setiap minggu dengan tepat waktu, membaca alkitab, melakukan  saat teduh / renungan 
pribadi, berdoa, memunyai integritas dalam keseharian, rendah hati, menyatakan kasih 
terhadap sesama, menjadi pendengar yang baik, dapat sama-sama bertumbuh secara rohani, 
memiliki komunitas yang benar, yakin bahwa Tuhan Yesus adalah juru selamat, dan 
membawa pengaruh yang positif bagi anggotanya. Harapan tersebut sejalan dengan konsep 
dari dimensi-dimensi religiusitas. 
Menurut Glock & Stark (1965) religiusitas dapat tergambar melalui 5 dimensi, pertama 
adalah seberapa yakin individu terhadap kebenaran agamanya (ideological dimension/ 
dimensi ideologis). Dimensi kedua adalah seberapa sering individu menjalankan ritual-ritual 
keagamaan yang telah ditetapkan oleh agamanya (ritualistic dimension/ dimensi ritualistik). 
Dimensi ketiga, berkaitan dengan seberapa besar pengalaman dan pengharapan individu pada 
ajaran agamanya ( experiential dimension/ dimensi eksperiensial). Dimensi keempat, seberapa 
besar pengetahuan atau informasi individu mengenai agama yang dianutnya (intellectual 
dimension/ dimensi intelektual). Dimensi terakhir yaitu dimensi yang dapat dibedakan dari 
perilaku umum yang ditunjukan dalam kehidupan sehari-hari apakah sesuai dengan ajaran 
agama yang telah dianutnya atau tidak (Consequential dimension/ dimensi konsekuensial).  
Berdasarkan hasil wawancara dengan P-kom Gereja  X  di Kota Bandung diperoleh hasil 
bahwa beberapa P-kom merasa bersyukur, bersukacita dan bisa mendapatkan banyak 
pelajaran tentang banyak hal dari anggota KOMIT nya, selain itu P-kom merasa diingatkan 
kembali untuk tidak melakukan hal yang dianggap kurang baik karena P-kom merasa bahwa 
dirinya harus menjadi teladan untuk anggota KOMIT nya, sesekali P-kom merasa sulit 
menangani anggota KOMIT yang dianggap menyebalkan dan sulit diatur. Ditemukan pula 
kesulitan yang dihadapi P-kom yaitu terdapatnya anggota KOMIT yang sulit terbuka 
mengenai permasalahannya sehingga perlu waktu khusus tersendiri dengan anggota tersebut.  
Beberapa P-kom merasa sulit untuk bisa menjadi role model bagi anggotanya karena apa 
yang dilakukan P-kom akan mencerminkan anggota KOMIT nya, namun selama ini P-kom 
merasa sudah berusaha untuk dapat menyelaraskan apa yang dikatakan dengan perilaku yang 
ditampilkan dalam kehidupan sehari-hari dan tidak di buat-buat. Menurut P-kom menjadi role 
model cukup penting karena terdapat pula P-kom yang tidak dapat dijadikan sebagai panutan 
oleh anggotanya dan akhirnya memutuskan untuk berhenti pelayanan atau munculnya konflik 
yang terjadi antara P-kom dan anggotanya. Menurut P-kom apabila tidak memiliki potensi 
untuk menjadi role model dalam hal ini menjadi panutan sebagai orang yang religius, maka P-
kom akan mengalami kesulitan dalam membangun anggota KOMITnya agar dapat 
mengetahui dan memahami hal-hal yang bersifat penting dalam agama serta apa yang 
dikatakan kurang memiliki power untuk dapat membantu anggotanya menjadi seorang murid 
yang identik dengan Tuhan. 
Menjadi seorang P-kom diharapkan memiliki derajat dimensi-dimensi religiusitas yang 
tergolong tinggi untuk dapat dijadikan role model bagi anggotanya agar tidak memunculkan 
rasa kurang percaya anggota terhadap apa yang disampaikan P-kom pada saat memulai 
KOMIT serta agar mampu sama-sama menjadi seorang murid yang identik dengan Tuhan. 
Tidak hanya menghayati dan mengerti doktrin-doktrin agama Kristen, namun memiliki 
pengetahuan yang luas mengenai kekeristenan, serta menerapkannya dalam kehidupan sehari-
hari. P-kom yang memiliki religiusitas yang tinggi akan mendapatkan manfaat untuk diri 
sendiri seperti memiliki hubungan yang erat dengan Tuhan, memiliki pemahaman yang utuh 
mengenai agama Kristen, dan menerapkan nilai-nilai Kristiani untuk mendasari tingkah laku 
yang akan ditampilkan dalam kehidupan sehari-hari.P-kom yang memiliki dimensi-dimensi 
religiusitas dengan derajat yang rendah berarti akan kurang menghayati perannya sebagai 
pembimbing yang seharusnya dapat dijadikan role model bagi anggotanya. Selain itu ketika 
P-kom memiliki religiusitas yang rendah, P-kom akan merasa tidak bersukacita dalam 
menjalani proses KOMIT sehingga dapat berakibat adanya rasa terpaksa ketika melayani, 
selain itu akan kurang memahami bahan yang akan disampaikan kepada anggotanya. 
Dengan mengetahui derajat dari dimensi-dimensi religiusitas ini, diharapkan P-kom dapat 
mengembangkan kembali dimensi-dimensi yang masih berada dalam derajat rendah atau 
dapat memertahankan dimensi-dimensi yang tergolong tinggi yang nantinya akan berguna 
untuk Gereja, anggota P-kom, serta P-kom sendiri. Oleh karena itu, peneliti tertarik melihat 
gambaran mengenai derajat dimensi-dimensi religiusitas P-kom Gereja  X  di kota Bandung.   
Dari penelitian ini ingin diketahui bagaimana derajat dimensi-dimensi religiusitas P-kom 
di Gereja  X  kota Bandung. 
Gereja  X  kota Bandung. 
Bandung. 
a. Secara teoretis penelitian ini diharapkan dapat menjadi referensi di bidang 
psikologi, khususnya psikologi agama Kristen. 
b. Penelitian ini diharapkan dapat memberi masukan dan tambahan informasi kepada 
peneliti lain yang tertarik untuk meneliti topik yang serupa sehingga penelitian 
dalam bidang religiusitas dapat berkembang.   
a. Memberikan informasi kepada Gereja  X  di kota Bandung mengenai derajat 
dimensi-dimensi religiusitas dari P-kom. Informasi ini juga dapat digunakan untuk 
memertimbangkan keefektifan dari pembekalan yang sudah diberikan kepada P-
kom. 
b. Memberikan saran kepada Gereja  X  kota Bandung untuk meningkatkan derajat 
dimensi-dimensi religiusitas pada P-kom yang berada pada kategori rendah, 
sehingga dapat melakukan pelayanannya lebih baik lagi. 
P-kom memikul peran untuk dijadikan role model bagi anggotanya, oleh karena 
itu seorang P-kom diharapkan  memiliki religiusitas yang tinggi yang dapat tercermin 
melalui tingkah laku sehari-hari. Religiusitas adalah tingkat konsepsi dan tingkat 
komitmen individu terhadap ajaran agamanya (Glock dan Stark, 1966). Tingkat 
konseptualisasi adalah tingkat pengetahuan seseorang terhadap agamanya dan tingkat 
komitmen adalah suatu hal yang perlu dipahami secara menyeluruh dalam 
menjalankan ajaran agamanya.  
Religiusitas pada P-kom di gereja  X  kota Bandung adalah bagaimana tingkat 
konseptualisasi dan tingkat komitmen P-kom terhadap ajaran agama Kristen. Menurut 
Glock dan Stark, (1965), religiusitas dapat dimanifestasikan melalui lima dimensi 
yaitu ideological dimension (dimensi ideologis), ritualistic dimension (dimensi 
ritualistik), experiential dimension (dimensi eksperensial), intellectual dimension 
(dimensi intelektual) dan consequential dimension (dimensi konsekuensial).  
Dimensi pertama adalah ideologis yang berisi pengharapan   pengharapan di mana 
orang religius berpegang teguh pada pandangan teologis tertentu dan mengakui 
kebenaran doktrin   doktrin tersebut. Dimensi ini menyangkut seberapa yakin P-kom 
di gereja  X  kota Bandung terhadap kebenaran ajaran-ajaran agama Kristen yang 
bersifat fundamental dan dogmatis, serta memahami keyakinan mengenai doktrin 
Allah, surga dan neraka, kisah para nabi dan mukjizatnya, dosa, dan keselamatan. P-
kom di Gereja  X  Kota Bandung yang mempunyai tingkat ideologis yang tinggi akan 
percaya sepenuhnya terhadap semua ajaran dan kepercayaan agama Kristen tanpa 
keraguan sedikitpun.  
P-kom ini telah mencapai tahap meyakini kebenaran ajaran Kristen secara 
menyeluruh sebagai suatu kebenaran yang mereka yakini dan ketika menjalankan 
KOMIT, P-kom akan memiliki keyakinan terhadap ajaran agama Kristen. Sebaliknya, 
P-kom di Gereja  X  Kota Bandung dengan ideologis yang rendah cenderung masih 
memunyai keraguan atas sebagian atau keseluruhan ajaran agama Kristen. P-kom di 
Gereja  X  Kota Bandung yang memunyai dimensi yang rendah masih 
memertanyakan tentang kebenaran-kebenaran ajaran agama Kristen khususnya pada 
hal-hal yang masih belum terlihat langsung melalui pancaindera serta ketika 
menjalankan KOMIT, P-kom akan memiliki keraguan terhadap ajaran agama Kristen 
seperti sering memertanyakan kebenaran dari ajaran agama Kristen. 
Dimensi kedua adalah ritualistik yang mencakup perilaku pemujaan, ketaatan, dan 
hal   hal yang dilakukan orang untuk menunjukkan komitmen terhadap agama yang 
dianutnya. Ritualistik pada P-kom Gereja  X  di Kota Bandung mencangkup seberapa 
sering P-Kom di Gereja  X  kota Bandung melakukan kegiatan-kegiatan ritual 
sebagaimana yang dianjurkan oleh agamanya, seperti melakukan  saat teduh , berdoa 
syafaat, membaca Alkitab, mengikuti setiap aktivitas kegiatan rohani didalam 
pelayanannya, mengikuti perjamuan kudus, dan menghadiri kebaktian ibadah minggu. 
P-kom yang memiliki derajat dimensi ritualistik yang tinggi akan rutin melakukan 
perjamuan kudus, membaca Alkitab,  saat teduh  dan juga doa   doa harian ketika 
menjalani kehidupan beragama dan tidak pernah lalai untuk melakukan ritual tersebut, 
P-kom akan menjadi model bagi anggotanya untuk menjalankan ritual agama Kristen, 
sedangkan P-kom yang memiliki derajat dimensi ritualistik yang rendah akan jarang 
serta lalai untuk melakukan perjamuan kudus, membaca Alkitab secara rutin,  saat 
teduh  dan juga doa   doa harian serta P-kom kurang menjadi model bagi anggotanya 
untuk menjalankan ritual keagamaan Kristen. 
Dimensi yang ketiga adalah experiential dimension (dimensi eksperensial) 
melibatkan proses afektif. Dimensi ini berkaitan dengan perasaan-perasaan serta 
pengalaman-pengalaman keagamaan yang dialami oleh P-kom Gereja  X  di Kota 
Bandung. Dimensi ini berkaitan dengan seberapa positif penghayatan P-Kom di 
Gereja  X  Bandung untuk menghayati ajaran agama Kristen dan dalam berelasi 
dengan Tuhan, seperti merasakan ketenangan dan sukacita didalam setiap pelayanan 
yang dilakukan sebagai pembimbing KOMIT, perasaan doa-doa yang sering terkabul, 
perasaan berserah dan bersyukur kepada Tuhan. P-kom Gereja  X  di Kota Bandung 
yang mempunyai dimensi eksperensial tinggi akan mempunyai perasaan dan 
pengalaman hidup yang semakin menambah keyakinannya terhadap agama Kristen. 
Pada P-kom yang mempunyai dimensi eksperensial yang tinggi pengalaman hidup 
yang dialaminya telah mengkonfirmasi kebenaran ajaran-ajaran Kristen yang selama 
ini diyakininya. Sebaliknya, P-kom Gereja  X  di Kota Bandung yang mempunyai 
dimensi eksperensial yang rendah, pengalaman hidup yang dijalaninya belum atau 
tidak mengkonfirmasi kebenaran yang diajarkan oleh agama Kristen. Sehingga 
berdasarkan pengalaman tersebut P-kom Gereja  X  di Kota Bandung akan cenderung 
memiliki perasaan yang kurang kuat terhadap kepercayaan-kepercayaan agama 
Kristen.  
Dimensi keempat adalah intelektual, mengacu kepada harapan bahwa orang 
beragama paling tidak memiliki pengetahuan mengenai dasar - dasar keyakinan, ritus - 
ritus, kitab suci dan tradisi - tradisi.  Dimensi intelektual mengacu pada harapan bahwa 
P-kom sekurang-kurangnya memiliki pengetahuan mengenai dasar-dasar keyakinan, 
ritus-ritus, alkitab dan tradisi-tradisi seperti isi Alkitab baik Perjanjian Baru dan 
Perjanjian Lama yang disampaikan pada waktu melayani sebagai P-kom. P-kom di 
Gereja  X  Kota Bandung yang memiliki derajat dimensi intelektual yang tinggi 
mengetahui dan memahami mengenai ajaran pokok agama Kristen secara keseluruhan, 
seperti isi Alkitab dan 10 perintah Allah, sedangkan P-kom di Gereja  X  Kota 
Bandung yang memiliki derajat dimensi intelektual yang rendah cenderung kurang 
mengetahui dan tidak memahami mengenai ajaran pokok agama secara keseluruhan, 
seperti seperti isi Alkitab dan 10 perintah Allah.  
Terakhir adalah dimensi konsekuensial, berkaitan dengan perilaku seseorang yang 
dimotivasi oleh ajaran Kristen atau bagaimana seseorang mengamalkan ajaran 
agamanya dalam kehidupan sehari-hari baik dalam menjalani perannya sebagai 
pembimbing maupun kehidupan sehari-hari yang dijalani oleh P-kom. Dimensi ini 
berkaitan dengan perilaku P-kom yang dimotivasi oleh ajaran agamanya atau 
bagaimana P-kom mengamalkan ajaran agamanya dalam kehidupan sehari-hari. P-
kom yang memiliki derajat dimensi konsekuensial yang tinggi dapat mengaplikasikan 
ajaran agamanya dalam kehidupan sehari-hari seperti seperti membantu sesama tanpa 
memandang agama sekalipun, saling mengasihi, jujur, menegakkan keadilan dan 
kebenaran, melawat anggota komsel yang sedang mengalami kesulitan atau jarang 
pergi kegereja dan menjaga lingkungan sekitarnya. 
Sementara P-kom yang memiliki dimensi konsekuensial rendah kurang dapat 
mengaplikasikan ajaran agamanya dalam kehidupan sehari-hari baik dalam pelayanan 
maupun di lingkungan sosialnya, melakukan kebohongan, membenci sesama termasuk 
anggota KOMIT, tidak melakukan pelayanan agama dan tidak mampu memberikan 
pengampunan terhadap orang yang bersalah termasuk anggota KOMITnya. 
Glock & Stark 1965; Stark 1972 menyatakan faktor yang memengaruhi 
religiusitas adalah status sosial ekonomi dari seseorang. Individu yang memiliki status 
sosial ekonomi yang kurang, dianggap lebih mampu membangun hubungannya 
dengan Tuhan sebagai kompensasi dari kekurangan mereka dan memeroleh reward 
yang biasanya tidak mampu mereka dapatkan.  
Individu dengan status sosio ekonomi atas maupun bawah memohon kepada 
Tuhan karena mengetahui bahwa terdapat hubungan antara memohon/berdoa pada 
Tuhan dengan posisi atau kelas sosio ekonomi pribadi mereka. P-kom yang memiliki 
status sosio ekonomi yang tergolong kurang akan lebih mampu membangun 
hubungannya dengan Tuhan sebagai kompensasi dari kekurangan mereka dan 
memeroleh reward yang biasanya tidak mampu mereka dapatkan, sedangkan P-kom 
yang memiliki status sosio ekonomi yang tergolong tinggi akan kurang mampu untuk 
membangun hubungannya dengan Tuhan.  
Guna memerjelas uraian diatas, maka kerangka pemikiran dalam penelitian ini 
digambarkan sebagai berikut:  
P-kom di 
Dimensi Religiusitas : 
1. Religiusitas dari P-kom diukur melalui masing-masing dimensinya, yaitu dimensi 
ideologis, ritualistik, eksperiensial, intelektual, dan konsekuensial. 
2. Dimensi-dimensi religiusitas pada P-kom di Gereja  X  Kota Bandung mempunyai 
derajat yang bervariasi. 
3. Tinggi rendahnya tingkat religiusitas pada P-kom di Gereja  X  Kota Bandung 
berkaitan dengan faktor yang memengaruhi yaitu status sosioekonomi. 
Religiusitas adalah tingkat konsepsi dan tingkat komitmen individu terhadap 
ajaran agamanya (Glock dan Stark, 1966). Tingkat konseptualisasi adalah tingkat 
pengetahuan seseorang terhadap agamanya, sedangkan yang dimaksud dengan tingkat 
komitmen adalah sesuatu hal yang perlu dipahami secara menyeluruh dalam 
menjalankan ajaran agamanya.  
Menurut C.Y Glock & R. Stark terdapat lima dimensi religiusitas, yaitu: 
1. Dimensi Ideologis 
Berisi pengharapan   pengharapan dimana orang religius berpegang teguh pada 
pandangan teologis tertentu dan mengakui kebenaran doktrin   doktrin 
tersebut.Terdapat tiga kategori kepercayaan. Pertama, kepercayaan yang menjadi dasar 
esensial suatu agama. Kedua, kepercayaan yang berkaitan dengan tujuan Ilahi dalam 
pencipaan manusia. Ketiga, kepercayaan yang berkaitan dengan cara terbaik untuk 
melaksanakan tujuan Ilahi yang diatas. 
2. Dimensi Ritualistik 
Dimensi ini mencakup perilaku pemujaan, ketaatan, dan hal   hal yang 
dilakukan orang untuk menunjukkan komitmen terhadap agama yang dianutnya. 
Praktik   praktik keagamaan ini terdiri atas dua kelas penting yaitu: 
a. Ritual, mengacu kepada seperangkat ritus, tindakan keagamaan formal dan 
praktik   praktik suci yang semua mengharapkan para pemeluk agama 
melaksanakan.  
b. Ketaatan dan ritual bagaikan ikan dengan air, meski ada perbedaan penting.  
Aspek ritual dari komitmen sangat formal, ketaatan dapat diungkapkan melalui 
renungan, membaca injil, dan pujian   pujian. 
Yang dimaksud dengan perilaku disini bukanlah perilaku umum yang 
dipengaruhi keimanan seseorang, melainkan mengacu kepada perilaku-perilaku 
khusus yang diterapkan oleh agama. Ritus-ritus ini berkembang bersama dengan 
perkembangan agama itu. Semakin terorganisasi sebuah agama, semakin banyak 
aturan yang dikenakan kepada pengikutnya. Aturan ini berkaitan dengan tata cara 
beribadah hingga jenis pakaian. 
3. Dimensi Eksperiensial 
Dimensi ini berkaitan dengan pemahaman keagamaan, perasaan perasaan, 
persepsi - persepsi dan sensasi - sensasi yang dialami seseorang atau didefinisikan 
oleh suatu kelompok keagamaan (atau suatu masyarakat) yang melihat komunikasi 
walaupun kecil, dalam suatu esensi ketuhanan, yaitu dengan Tuhan, kenyataan 
terakhir, dengan otoritas transedental. 
4. Dimensi Intelektual 
Dimensi ini mengacu kepada harapan bahwa orang beragama paling tidak 
memiliki pengetahuan mengenai dasar - dasar keyakinan, ritus - ritus, kitab suci dan 
tradisi - tradisi. Dimensi pengetahuan dan keyakinan jelas berkaitan satu sama lain, 
karena pengetahuan mengenai suatu keyakinan adalah syarat bagi pener maannya. 
Dimensi ini menunjuk pada tingkat pengetahuan dan pemahaman seseorang terhadap 
ajaran pokok agamanya. Sikap orang dalam menerima atau menilai ajaran agamanya 
berkaitan erat dengan pengetahuan agamanya berkaitan erat dengan pengetahuan 
agamanya itu. Orang yang sangat dogmatis tidak mau mendengarkan pengetahuan dari 
kelompok manapun yang bertentangan dengan keyakinan agamanya. 
5. Dimensi Konsekuensial 
Dimensi ini berkaitan dengan perilaku seseorang yang dimotivasi oleh ajaran 
agamanya atau bagaimana seseorang mengamalkan ajaran agamanya dalam kehidupan 
sehari   hari. Dimensi ini menunjukkan akibat ajaran agama dalam perilaku umum, 
yang tidak secara langsung dan khusus ditetapkan agama (seperti dalam dimensi 
ritualistik). Inilah efek ajaran agama pada perilaku individu dalam kehidupan sehari-
hari.  
Glock and Stark 1965; Stark 1972 menyatakan faktor yang memengaruhi religiusitas 
adalah status sosial ekonomi dari seseorang. Individu yang memiliki status sosial ekonomi 
yang kurang, dianggap lebih mampu membangun hubungannya dengan Tuhan sebagai 
kompensasi dari kekurangan mereka dan memeroleh reward yang biasanya tidak mampu 
mereka dapatkan.  
Individu dengan status sosio ekonomi atas maupun bawah memohon kepada Tuhan 
karena mengetahui bahwa terdapat hubungan antara memohon/berdoa pada Tuhan dengan 
posisi atau kelas sosio ekonomi pribadi mereka. Individu yang memiliki status sosio 
ekonomi yang tergolong kurang akan lebih mampu membangun hubungannya dengan 
Tuhan sebagai kompensasi dari kekurangan mereka dan memeroleh reward yang biasanya 
tidak mampu mereka dapatkan, sedangkan Individu yang memiliki status sosio ekonomi 
yang tergolong tinggi akan kurang mampu untuk membangun hubungannya dengan 
Tuhan.  
Selain itu Levin (1994) menyatakan bahwa religiusitas ditemukan lebih tinggi pada 
orang yang berjenis kelamin perempuan. Hal tersebut berhubungan dengan perbedaan 
lingkup sosial, ekspektasi peran, dan mechanism copyng dibandingkan laki-laki. 
Penelitian ini menggunakan metode deskriptif dengan teknik survey yang bertujuan untuk 
memeroleh gambaran tentang derajat dari dimensi-dimensi religiusitas pada P-kom gereja 
 X  Kota Bandung.  
Variabel dalam penelitian ini adalah derajat dimensi-dimensi religiusitas. 
Religiusitas adalah tingkat konsepsi seseorang terhadap agama dan tingkat komitmen 
seseorang terhadap agamanya. Tingkat konseptualisasi adalah tingkat pengetahuan seseorang 
terhadap agamanya, sedangkan yang dimaksud dengan tingkat komitmen adalah suatu hal 
yang perlu dipahami dan dilaksanakan secara menyeluruh dalam menjalankan ajaran 
P-kom di Gereja  X   
Dimensi-
agamanya (Glock & Stark, 1966).  Religiusitas diungkapkan dalam lima dimensi yaitu 
dimensi ideologis, dimensi ritualistik, dimensi eksperiensial, dimensi intelektual, dan dimensi 
konsekuensial.  
Menurut Glock dan Stark terdapat lima dimensi religiusitas, yaitu: 
1. Dimensi Ideologis 
Berisi pengharapan   pengharapan dimana orang religius berpegang teguh pada 
pandangan teologis tertentu dan mengakui kebenaran doktrin   doktrin tersebut. 
Terdapat tiga kategori kepercayaan. Pertama, kepercayaan yang menjadi dasar esensial 
suatu agama. Kedua, kepercayaan yang berkaitan dengan tujuan Ilahi dalam pencipaan 
manusia. Ketiga, kepercayaan yang berkaitan dengan cara terbaik untuk melaksanakan 
tujuan Ilahi yang diatas. 
2. Dimensi Ritualistik 
Dimensi ini mencakup perilaku pemujaan, ketaatan, dan hal   hal yang 
dilakukan orang untuk menunjukkan komitmen terhadap agama yang dianutnya. 
Praktik   praktik keagamaan ini terdiri atas dua kelas penting yaitu: 
a. Ritual, mengacu kepada seperangkat ritus, tindakan keagamaan formal dan 
praktik   praktik suci yang semua mengharapkan para pemeluk agama 
melaksanakan.  
b. Ketaatan. Ketaatan dan ritual bagaikan ikan dengan air, meski ada perbedaan 
penting.  
Aspek ritual dari komitmen sangat formal, ketaatan dapat diungkapkan melalui 
renungan pribadi, membaca injil, dan pujian   pujian. 
3. Dimensi Eksperiensial 
Dimensi ini berkaitan dengan pemahaman keagamaan, perasaan perasaan, 
persepsi - persepsi dan sensasi - sensasi yang dialami seseorang yang melihat 
komunikasi walaupun kecil, dalam suatu esensi ketuhanan, yaitu dengan Tuhan, 
kenyataan terakhir, dengan otoritas transedental. 
4. Dimensi Intelektual 
Dimensi ini mengacu kepada harapan bahwa individu beragama paling tidak 
memiliki pengetahuan mengenai dasar - dasar keyakinan, ritus - ritus, kitab suci dan  
tradisi. Dimensi pengetahuan dan keyakinan jelas berkaitan satu sama lain, karena 
pengetahuan mengenai suatu keyakinan adalah syarat bagi pener maannya. Dimensi 
ini menunjuk pada tingkat pengetahuan dan pemahaman seseorang terhadap ajaran 
pokok agamanya. 
5. Dimensi Konsekuensial 
Dimensi ini berkaitan dengan perilaku seseorang yang dimotivasi oleh ajaran 
agamanya atau bagaimana seseorang mengamalkan ajaran agamanya dalam kehidupan 
sehari   hari. 
Religiusitas merujuk pada tingkat konsepsi dan tingkat komitmen P-kom terhadap 
agama Kristen.  
Derajat dimensi-dimensi religius yang dimiliki oleh P-kom perlu dilihat dari dimensi-
dimensi yang menjadi bagian dari religiusitas sendiri. Menurut Glock dan Stark, (1965) 
religiusitas terwujud melalui lima dimensi yang dapat dilihat melalui aktivitas-aktivitas atau 
perilaku P-kom yaitu:  
a. Dimensi Ideologis (the ideological dimensions / religious belief), yaitu seberapa 
yakin P-kom di gereja  X  kota Bandung terhadap kebenaran ajaran-ajaran agama Kristen 
yang bersifat fundamental dan dogmatis, serta memahami keyakinan mengenai doktrin Allah, 
surga dan neraka, kisah para nabi dan mukjizatnya , dosa, kasih dan keselamatan. 
b. Dimensi Praktik Agama (the ritualistic dimensions / religious practice), yaitu 
seberapa sering P-com di Gereja  X  kota Bandung melakukan kegiatan-kegiatan ritual 
sebagaimana yang dianjurkan oleh agama Kristen, seperti melakukan saat teduh, berdoa 
syafaat pribadi, membaca alkitab, mengikuti setiap aktivitas kegiatan rohani didalam 
pelayanannya, melawat anggota KOMIT yang sedang mengalami kesulitan atau jarang pergi 
kegereja lagi dan menghadiri kebaktian ibadah minggu. 
c. Dimensi Pengalaman dan Penghayatan (the experiential dimensions / religious 
feeling), yaitu seberapa positif penghayatan P-kom di Gereja  X  Bandung untuk menghayati 
ajaran agama Kristen dan dalam berelasi dengan Tuhan, seperti merasakan ketenangan dan 
sukacita sebagai pembimbing KOMIT. 
d. Dimensi Pengetahuan Agama (the intellectual dimensions / religious knowledge), 
yaitu seberapa banyak dan seberapa komprehensif (lengkap) pengetahuan yang dimilik P-kom 
di Gereja  X  Bandung mengenai doktrin-doktrin dalam artian isi Alkitab baik Perjanjian 
Baru dan Perjanjian Lama yang disampaikan pada waktu melayani sebagai P-kom. 
e. Dimensi Pengamalan / konsekuensi (the consequential dimensions / religuos effect), 
yaitu seberapa sering pengamalan ajaran agama Kristen yang ditampilkan P-kom di Gereja 
 X  Kota Bandung yang ditampilkan didalam kehidupan sehari-hari, seperti membantu 
sesama termasuk anggota KOMIT tanpa memandang agama sekalipun, saling mengasihi 
dengan anggota KOMIT, jujur, menegakkan keadilan dan kebenaran, serta menjaga 
lingkungan sekitarnya. 
Penelitian ini menggunakan kuesioner yang disusun berdasarkan teori religiusitas dari 
Glock dan Stark (1966), dan telah mencangkup lima dimensi yaitu dimensi ideologis, dimensi 
ritualistik, dimensi eksperensial, dimensi intelektual, dan dimensi konsekuensial. Masing-
masing dimensi akan di ukur menggunakan kuesioner yang berbeda. Alat ukur dibuat oleh 
peneliti berdasarkan kondisi dari subjek yang akan diteliti.  
Dimensi Indikator Item + Item - 
Dimensi Ideologis Keyakinan kepada Tuhan 
1, 14 12, 20 
22, 2 21, 24 
16, 3, 11 10, 19, 4 
5, 15, 23 13, 8, 6 
18,7 9,17 
Pilihan jawaban Item positif Item negatif 
Sangat Yakin (SY) 4 1 
Yakin (Y) 3 2 
Tidak Yakin (TY) 2 3 
Sangat Tidak Yakin (STY) 1 4 
1, 6 
bersama P-kom lainnya 
2, 3, 5, 8 
4, 7 
9, 10 
Sangat sering (S) 4 
Sering (S) 3 
Jarang (J) 2 
Tidak Pernah (TP) 1 
Dimensi Indikator Item + Item - 
Eksperiensial Merasa suka cita saat 
(berdoa, membaca alkitab, 
renungan pribadi, dan 
1, 15  2, 20 
Perasaan doa-doa yang sering 
4, 16 3, 7  
6, 8, 6 , 17 5, 11 
14, 19, 10 9, 18, 12 
Pilihan jawaban Item positif Item negatif 
Sangat Sesuai (SS) 4 1 
Sesuai (S) 3 2 
Tidak Sesuai (TS) 2 3 
Sangat Tidak Sesuai (STS) 1 4 
Pengetahuan 10 Perintah Tuhan 1, 2, 8, 10, 14 
Isi Alkitab (Perjanjian lama) 3, 4, 6,  9, 11 
Isi Alkitab (Perjanjian baru) 5, 7, 12, 13 
Dimensi Indikator Item + Item - 
1 ,3, 17 6, 9 ,18 
7,16 10,13  
8,4 2, 15  
KOMIT nya. 
5 ,14 11, 12 
Pilihan jawaban Item positif Item negatif 
Sangat Setuju (SS) 4 1 
Setuju (S) 3 2 
Tidak Setuju (TS) 2 3 
Sangat Tidak Setuju (STS) 1 4 
Setelah responden mengisi kuesioner, nilai responden dihitung berdasarkan 
penjumlahan skor masing-masing dimensi religiusitas. Tingkat dimensi religiusitas pada 
masing-masing responden dilihat dari jumlah seluruh skor yang diperoleh responden dalam 
menjawab item-item tiap dimensi. Skor yang telah diperoleh diubah ke dalam bentuk 
persentase sehingga terdapat skor maksimum dan skor minimum. Skor yang telah diperoleh 
disusun dari mulai nilai terkecil hingga tertinggi kemudian dibagi menjadi 2 bagian yang 
sama banyak sehingga diperoleh nilai tengah (median). Skor yang masuk median atas 
dikategorikan tinggi sedangkan skor yang masuk median bawah dikategorikan rendah.  
Data pribadi dan data penunjang yang diberikan kepada responden digunakan untuk 
melengkapi dan menunjang hasil yang diperoleh dalam penelitian ini yang mencakup :  
1. Data demografis seperti usia dan jenis kelamin. 
2. Pertanyaan untuk menjaring mengenai faktor-faktor yang berkaitan dengan dimensi-
dimensi religiusitas P-kom gereja  X  Kota Bandung berupa pertanyaan untuk 
menjaring status sosial ekonomi seperti pendidikan terakhir, pendidikan terakhir ayah, 
pendidikan terakhir ibu, pendapatan/uang saku perbulan, dan penghayatan mengenai 
status sosial ekonomi. 
Uji validitas menunjukan ketepatan suatu alat ukur dalam mengukur apa yang hendak 
diukur. Uji validitas yang digunakan untuk kuesioner dimensi I, II, III, dan V adalah construct 
validity : corelate bivariate dimana untuk memeroleh item yang benar-benar sesuai dengan 
apa yang ingin diukur yaitu dengan melihat besarnya koefisien korelasi antara skor item yang 
mengukur aspek tertentu yang sesuai dengan konsep teoretik, dengan jumlah skor totalnya, 
sedangkan untuk kuesioner IV uji validitas yang digunakan adalah chi-square, ini berarti 
bahwa suatu alat ukur mampu mengungkap isi suatu konsep atau variabel yang hendak 
diukur. 
Pengujian validitas alat ukur dilakukan dengan menghitung korelasi antara skor item 
dengan skor keseluruhan dengan menggunakan korelasi Rank spearman. 
Rumus Rank Spearman (Sudjana, 1983) 
 Keterangan :   rs = koefisien korelasi Spearman  
   x = skor item 
   y = skor item keseluruhan 
   d = selisih skor x dan y      
Tingkat validitas alat ukur ini diperoleh dengan membandingkan koefisien korelasi 
rank Spearman dengan nilai kriteria dari Friedenberg (1995), yaitu : 
<0.3 : item dinyatakan tidak valid, sehingga item dibuang. 
?0.3  : item dinyatakan valid, sehingga item dapat dipakai. 
Pada penelitian ini alat ukur dimensi I memiliki tingkat validitas berkisar dari 0.338-
0.75, sedangkan alat ukur dimensi II berkisar dari 0.319-0.719, alat ukur dimensi III berkisar 
dari 0.318-0.641, dimensi IV berkisar dari 0.326-0.544, sedangkan dimensi V berkisar dari 
0.322-0.611. 
Uji reliabilitas adalah indeks yang menunjukkan sejauh mana alat ukur yang digunakan 
dapat dipercaya dan diandalkan (Notoatmodjo, 2002). Pengujian reliabilitas alat ukur 1, 2, 3, 
dan 5 dilakukan dengan menggunakan Alpha Cronbach, sedangkan alat ukur dimensi 4 akan 
dilakukan dengan menggunakan metode Split Half Reliability yang berarti teknik pengujian 
reliabilitas instrumen dengan cara membaginya menjadi dua bagian. Indeks reliabilitas 
dicerminkan dari korelasi antara dua bagian instrumen.  
Groth dan Marnat (2008) menyebutkan bahwa konsistensi internal melalui reliabilitas 
belah dua dan koefisien alpha merupakan teknik-teknik terbaik untuk menentukan reliabilitas 
sebuah ciri-sifat dengan derajat fluktuasi yang tinggi.  
 Keterangan: 
rii  =  koefisien reliabilitas 
n  =  jumlah item 
SDi2 =  varians skor item 
? SDi2 =  jumlah varians skor item 
Untuk menghitung nilai koefisien split half dengan rumus : 
r11= 2 rhh1+ rhh  
Keterangan : 
 = koefisien realibilitas 
 = koefisien korelasi product moment antara skor belahan satu dengan skor belahan lain 
Tingkat relibialitas alat ukur ini diperoleh dengan membandingkan indeks reliabilitas 
yang diperoleh  dengan kriteria Kaplan (2009):  
<0.7   :  Item Tidak Realiabel 
? 0.7   : Item Realiabel 
Pada penelitian ini alat ukur dimensi I memiliki tingkat reliabilitas berdasarkan alpha 
cronbach adalah 0.881, sedangkan alat ukur dimensi II adalah 0.791, alat ukur dimensi III 
adalah 0.815, alat ukur dimensi IV berdasarkan split-half yaitu 0.758, dan alat ukur dimensi V 
adalah 0.822. 
Karakteristik populasi dalam penelitian ini adalah P-kom yang melayani di Gereja  X  
Kota Bandung dan bersedia mengisi kuesioner yang diberikan. 
 Teknik analisis yang digunakan dalam penelitian ini adalah deskriptif yaitu dengan 
menghitung distribusi frekuensi dan tabulasi silang. Skor total dikategorikan menggunakan 
norma kelompok yang ditentukan dari nilai terendah dan tertinggi yang diperoleh oleh subjek 
penelitian. Cara menghitung distribusi frekuensi yang dilakukan dengan membagi frekuensi 
setiap jawaban pada responden dengan jumlah keseluruhan dikalikan dengan 100%.  
Presentase :   x 100% 
Keterangan :  
f: Frekuensi dari jumlah responden yang menjawab  
N: Jumlah keseluruhan responden  
 Selain itu dilakukan tabulasi silang untuk melihat signifikansi serta keterkaitan dengan 
faktor-faktor yang memengaruhi religiusitas sehingga akan didapat gambaran yang lebih 
detail mengenai keterkaitan antara dimensi-dimensi. Religiusitas dan faktor yang berkaitan 
dengan tingkat dimensi-dimensi religiusitas pada P-kom gereja  X  Kota Bandung. 
