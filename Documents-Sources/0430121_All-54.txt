Hardian Subhari 0430121. Penelitian ini dilakukan untuk mengetahui 
gambaran sikap terhadap perilaku fanatik pada suporter kesebelasan Persib 
Bandung di kota Bandung. Maksud penelitian ini adalah untuk menjaring aspek 
kognitif, afektif dan konatif terhadap perilaku fanatik pada suporter kesebelasan 
Persib Bandung. Tujuan penelitian ini adalah untuk memperoleh gambaran sikap 
positif atau negatif terhadap perilaku fanatik pada suporter kesebelasan Persib 
Bandung melalui pengukuran aspek kognitif, afektif dan konatif. 
 Pada penelitian menggunakan teori sikap. Teori ini dilengkapi oleh aspek 
kognitif, aspek afektif dan aspek konatif. Teori lain yang digunakan adalah teori 
fanatisme dan teori perilaku fanatic. 
Metode penelitian ini menggunakan metode penelitian deskriptif untuk 
menggambarkan variabel penelitian yaitu sikap terhadap perilaku fanatik. Alat ukur 
yang digunakan pada penelitian ini adalah alat ukur yang dirancang sendiri oleh 
peneliti berdasarkan teori sikap yang dikemukakan oleh Krech, Crutchfield, & 
Ballachey terhadap perilaku fanatik pada suporter kesebelasan Persib Bandung. 
Validitas menggunakan internal consistency dengan uji Rank Spearman,dan 
memperoleh 28 item dengan validitas berkisar 0,303-0,547. Untuk uji reliabitas 
menggunakan Alpha Cronbach sebesar 0,744 (reliabilitas tinggi). Populasi sasaran 
dalam penelitian ini adalah suporter kesebelasan Persib Bandung dengan teknik 
accidental sampling. Teknik analisis yang digunakan adalag distribusi sampling dan 
tabulasi silang. 
Berdasarkan hasil penelitian maka dapat disimpulkan bahwa seluruh suporter 
kesebelasan Persib Bandung memiliki sikap positif terhadap perilaku fanatik. 
Dengan pembagian aspek sikap positif pada aspek afektif sebesar 100% , aspek 
kognitif positif sebesar 96,3%, dan aspek konatif positif sebesar 26,9%, dan sikap 
negatif pada aspek konatif sebesar 73,1%, aspek kognitif negatif sebesar 3,7%, dan 
aspek afektif negatif sebesar 0%. 
Berdasarkan hasil penelitian ini, maka peneliti mengajukan saran kepada 
suporter kesebelasan Persib Bandung, dapat memberikan untuk dijadikan bahan 
evaluasi dan meningkatkan kepercayaan diri suporter untuk membangun perilaku 
fanatik yang positif. Peneliti pun mengajukan agar dilakukan penelitian serupa untuk 
menggali lebih jauh faktor-faktor lain yang mempengaruhi sikap terhadap perilaku-
perilaku suporter. Saran untuk kesebelasan Persib Bandung dapat memberikan 
tempat untuk masukan kesebelasan Persib Bandung dalam memahami apa yang 
menjadi cara pandang, kebutuhan, dan harapan dari suporter kesebelasan Persib 
Bandung. 
Puji dan syukur kepada Allah SWT yang telah memberikan berkat dan 
rahmat-Nya sehingga peneliti dapat menyelesaikan skripsi ini di Fakultas Psikologi 
Universitas Kristen Maranatha. 
 Peneliti mengetahui dan menyadari bahwa penyusunan skripsi yang berjudul 
 Studi Deskriptif Mengenai Sikap Terhadap Perilaku Fanatik Pada Suporter 
Kesebelasan Persib Bandung  , tidak terlepas dari kekurangan dan masih jauh dari 
kesempurnaan karena keterbatasan pengetahuan, pengalaman, serta kemampuan 
peneliti.  
 Pada kesempatan ini perkenankanlah saya mengucapkan terimakasih kepada 
semua pihak yang telah banyak membantu dalam menyelesaikan skripsi ini, terutama 
kepada : 
1. Ibu Jacqueline M.TJ., M.SI., Psikolog sebagai dosen pembimbing yang telah 
memberikan dukungan, kritik, dan masukan yang berarti bagi peneliti dalam 
menyelesaikan skripsi ini. 
2. Ibu Amanda M. M.Psi. selaku dosen pembimbing pendamping yang telah 
menyediakan waktu, membantu mengarahkan serta selalu memberikan 
semangat, saran-saran dan dukungan yang baik. 
3. Ibu Sianiwati H., M.Si., Psikolog sebagai dosen koordinator usulan penelitian 
dan skripsi. 
4. Seluruh staf Tata Usaha dan perpustakaan yang telah membantu peneliti 
dalam mencari dan melengkapi referensi yang diperlukan. 
5. Terima kasih untuk kedua orangtua peneliti, dan Yarda Subhari selaku kakak 
serta Arum Susilowati Subhari selaku adik yang telah memberikan dukungan 
baik moril maupun materil. 
6. Terima kasih buat teman-teman peneliti Anky Rachmansyah, Gerry 
Argiansyah, Mohammad Haqqi, Nur Anugerah, La Ode Arif Akbar, dan Utari 
Indra Putri yang telah memberikan dukungan dan semangat bagi peneliti 
untuk dapat menyelesaikan skripsi ini. 
7. Terima kasih juga untuk Ifrisa, Raditya Eka Nugraha, Galih Udeep, Wiraldi 
Kurniadi, Ayu Muharnintyas, Hana Talitha Rahma, dan Syafril Zulmi, yang 
telah memberikan kritikan yang membangun agar skripsi ini selesai. 
8. Terima kasih untuk rekan-rekan lainnya di Psikologi  06,  05, dan  04 yang 
tidak dapat disebutkan satu per satu, terima kasih telah menjadi teman sharing 
dan membantu serta memberikan semangat selama penyusunan skripsi ini. 
9. Terima kasih untuk semua pihak yang tidak dapat disebutkan satu per satu 
yang telah membantu peneliti dalam penyusunan skripsi ini. 
Akhir kata, peneliti berharap agar skripsi ini dapat berguna bagi semua pihak 
yang membutuhkan. 
Bandung, Desember 2010 
BAB I : Pendahuluan 
BAB II : Tinjauan Pustaka 
BAB III : Metodologi Penelitian 
Bandung                   ..    43 
Aspek-Aspek Sikap               .  45 
dengan Aspek Kognitif              . 45 
dengan Aspek Konatif              .. 47 
dengan Usia                   .  48 
dengan Lama menjadi Suporter Persib Bandung    ..  49 
dengan Aspek Kognitif              .. 45 
     dengan Aspek Konatif              .  47 
     dengan Lama menjadi Suporter kesebelasan Persib Bandung 49 
