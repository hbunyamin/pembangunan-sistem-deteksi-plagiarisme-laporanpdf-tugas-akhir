Dalam suatu perguruan tinggi terdapat proses belajar dan mengajar, proses ini 
lebih spesifik dibanding tingkat SMA. Disiplin ilmu yang disediakan merupakan 
hardskill, sedangkan kegiatan-kegiatan di luar studi merupakan softskill. Hardskill 
dan softskill inilah yang melingkupi seorang mahasiswa saat berada di perguruan 
tinggi. 
Mahasiswa ini merupakan subjek utama dalam proses belajar di perguruan 
tinggi. Selain mengikuti kegiatan perkuliahan, seorang mahasiswa dapat melibatkan 
diri ke dalam aktifitas lain misalnya menjadi anggota di suatu unit kegiatan di 
kampusnya. Mengikuti perkuliahan adalah tugas utama seorang mahasiswa, 
melibatkan diri ke dalam suatu unit kegiatan mahasiswa dapat dikatakan sebagai 
kegiatan tambahan atau ajang untuk menyalurkan bakat dan kemampuan-kemampuan 
di luar kegiatan akademik.  
Hampir semua unit kegiatan mahasiswa yang ada di suatu kampus menawarkan 
kegiatan-kegiatan yang berhubungan dengan hobi seperti hobi menyanyi di Unit 
Kegiatan Paduan Suara Mahasiswa, hobi bulu tangkis di Unit Kegiatan Bulu Tangkis 
Club. Setiap mahasiswa diberi kebebasan untuk memilih kegiatan-kegiatan yang 
ditawarkan oleh perguruan tinggi berdasarkan minatnya.   
Unit kegiatan Paduan Suara Mahasiswa (PSM) adalah salah satu unit kegiatan 
mahasiswa yang berada di Universitas  X  Bandung. PSM ini telah berdiri sejak 
tahun 1980 dengan visi Recognize as a world class choir. Unit kegiatan PSM  telah 
menghasilkan banyak prestasi skala regional, nasional dan internasional. Prestasi 
internasional yang terakhir diperoleh adalah Silver medal kategori symphony of 
voices 6th internationaler chorwettbewerb 2006 miltenberg Jerman. Prestasi skala 
internasional ini bukan hanya sekali diraih, tetapi pada tahun 2004 PSM juga pernah 
mendapat prestasi ketika mengikuti konser di Athena Yunani. Prestasi skala 
internasional ini menjadikan unit kegiatan PSM sebagai satu-satunya unit kegiatan di 
universitas  X  Bandung yang memiliki beberapa prestasi internasional.  
Atas dasar visi dan prestasi yang telah diraihnya selama ini, maka PSM perlu 
mempertahankan prestasinya dengan memiliki jadwal latihan rutin dan non-rutin 
yang harus dilakukan oleh anggotanya. Berdasarkan hasil wawancara dengan ketua 
PSM, didapatkan gambaran bahwa jadwal latihan rutin dilakukan seminggu tiga kali 
dari jam 5 sore sampai 7 malam bahkan sering lebih dari jam yang telah ditentukan, 
sedangkan jadwal latihan non-rutin pada hari libur atau hari minggu. Jika mereka 
tidak mengikuti latihan sebanyak 3 kali tanpa alasan yang jelas, maka mereka tidak 
diperbolehkan mengikuti konser yang akan diikuti.  
Pada periode 2009/2010 PSM memiliki anggota aktif sebanyak 32 orang. 
Sebagian besar mahasiswa anggota aktif PSM ini adalah mahasiswa semester 3 
hingga semester 7, saat itu umumnya beban perkuliahan mahasiswa masih terbilang 
padat. Oleh karena itu mahasiswa yang menjadi anggota aktif PSM memiliki tugas 
dan tanggungjawab baik sebagai mahasiswa ataupun sebagai anggota aktif PSM. 
Tugas utama sebagai mahasiswa yaitu bertanggungjawab untuk melaksanakan belajar 
mandiri (mencari materi dari sumber-sumber lain di luar jam kuliah) sesuai dengan 
jumlah SKS yang diambil, memenuhi kehadiran kuliah teori 75 % dan pratikum 100 
% dan mengerjakan tugas-tugas kuliah. Sedangkan sebagai anggota aktif PSM, 
mereka bertanggungjawab untuk mengikuti latihan secara rutin, dan mengikuti job 
atau mengisi suatu acara di dalam maupun di luar kampus.  
Peran sebagai mahasiswa dan anggota aktif PSM memiliki konsekuensi yang 
hanya dihadapi oleh mahasiswa bersangkutan. Konsekuensi tersebut ialah mahasiswa 
anggota aktif PSM harus membagi waktu agar dapat menjalankan tugas dan 
tanggungjawabnya dengan baik. Hasil wawancara dengan sepuluh mahasiswa yang 
menjadi anggota aktif PSM di Universitas  X  Bandung, didapatkan gambaran 
bahwa sebanyak 50 % dari mereka mengalami kelelahan fisik dan emosi seperti sakit 
karena pulang malam dan merasa kesal serta ingin memarahi teman-teman pada saat 
kuliah dan saat latihan. Hal ini mempengaruhi mereka dalam melakukan tugas dan 
tanggungjawab sebagai mahasiswa dan anggota aktif PSM, seperti tidak maksimal 
dalam mengikuti perkuliahan, IP (Indeks Prestasi) menurun setelah bergabung di 
PSM karena kurangnya waktu untuk belajar sendiri di luar jam kuliah, dan dalam 
latihan paduan suara menjadi tidak maksimal karena secara bersamaan menghadapi 
perkuliahan yang padat. Saat jadwal konser bertepatan dengan jadwal kuliah 
sebanyak 70 % memilih untuk mengikuti kuliah. Hal ini menimbulkan kekecewaan 
karena keinginan dari beberapa anggota PSM untuk mengikuti konser-konser baik di 
skala regional, nasional maupun internasional tidak terpenuhi. 
Berdasarkan hasil wawancara dengan pelatih PSM di universitas  X  
Bandung, diketahui bahwa alasan mahasiswa anggota aktif PSM tidak lagi aktif 
karena memiliki beberapa kendala-kendala seperti dilarang oleh orang tuanya untuk 
mengikuti kegiatan PSM karena menurunnya IP  atau karena sakit sehingga tidak bisa 
mengikuti semua kegiatan PSM. 
Konsekuensi-konsekuensi dalam menjalankan peran sebagai mahasiswa dan 
anggota aktif PSM ini akan berdampak buruk terhadap mahasiswa jika mereka tidak 
mengantisipasinya dengan tepat. Mengantisipasi konsekuensi-konsekuensi yang ada 
dapat dilakukan dengan membuat suatu perencanaan terhadap perkuliahan dan 
kegiatan PSM, melaksanakan dengan konsisten perencanaan yang semula dan 
melakukan evaluasi terhadap perencanaan dan proses pelaksanaan perencanaan 
tersebut, sangatlah diperlukan oleh mahasiswa anggota aktif PSM agar dapat 
memenuhi tugas dan tanggungjawabnya baik sebagai mahasiswa maupun sebagai 
anggota aktif PSM. Kemampuan mahasiswa anggota aktif PSM untuk mengatur 
dirinya agar dapat melakukan tugas dan tanggungjawabnya sebagai mahasiswa dan 
anggota aktif PSM terkait dengan konsep self-regulation. 
Hasil wawancara terhadap sepuluh orang mahasiswa anggota aktif PSM di 
Universitas  X  menunjukkan bahwa: sebanyak 70 % kurang mampu dalam 
perencanaan (fase forethought) terhadap tugas dan tanggung jawabnya, seperti 
mereka tidak memiliki jadwal tertulis sebagai bukti dari rencana, mereka hanya 
merencanakan di dalam pikiran sebatas perencanaan satu hari bukan merencanakan 
yang berkaitan dengan tujuan (goal setting) yang ingin diraih yaitu lulus dengan tepat 
waktu dengan IPK di atas 2,75, tidak menentukan target belajar untuk mendapatkan 
IP di atas 2,75. Dari 70 % yang kurang mampu pada fase forethought, 10 % dari 
mereka kurang mampu di aspek task analysis seperti mereka melewati atau 
menjalankan kuliah dan latihan tanpa perencanaan tertulis maupun lisan. 60 % dari 
mereka kurang mampu di aspek self-motivation beliefs seperti tidak yakin akan dapat 
memenuhi tugas dan tanggungjawab di perkuliahan maupun di PSM. Dalam 
melaksanakan tugas dan tanggungjawab sesuai rencana di fase foretouhgt, 80 % 
kurang mampu menetapkan dan melaksanakan tugas dan tanggungjawabnya itu 
secara konsisten, seperti mereka beberapa kali tidak mengikuti kuliah dan pertemuan 
kelompok untuk mengerjakan tugas kelompok kuliah (fase perforemance or 
volitional control), 70 % kurang mampu di aspek self-control seperti tidak 
sepenuhnya fokus pada tanggungjawab di perkuliahan, terkadang terganggu dengan 
latihan dan kegiatan PSM lainnya. 30 % dari mereka  kurang mampu dalam aspek 
self-observation seperti menghadapi jadwal yang bentrok antara kuliah dengan latihan 
atau konser dan harus mengorbankan kuliah demi konser yang akan diikuti. Dalam 
melakukan evaluasi terhadap perencanaan dan pelaksanaan tugas dan 
tanggungjawabnya (fase self-reflection), 40 % dari mereka kurang mampu 
melakukannya. Diantaranya 40 % kurang mampu pada aspek self-judgment seperti 
mereka tidak melakukan perbandingkan antara hasil ujian seperti kuis, UTS/UAS 
dengan target sebelum ujian tersebut dilaksanakan. 60 % dari mereka kurang mampu 
di aspek self-reaction seperti tidak lagi aktif PSM karena banyak kegiatan di PSM 
seperti latihan rutin yang mengakibatkan IP menurun.  
Dapat dilihat dari hasil wawancara di atas mahasiswa anggota aktif PSM 
memiliki perbedaan dalam proses melakukan self-regulation, berdasarkan masalah 
yang ada peneliti menjadi tertarik ingin meneliti mengenai self-regulation pada 
mahasiswa anggota aktif PSM di Universitas  X  kota Bandung. 
Bagaimana gambaran kemampuan self-regulation pada mahasiswa anggota aktif 
PSM di Universitas  X  kota Bandung. 
Maksud dari penelitian ini adalah untuk memperoleh gambaran mengenai self-
regulation pada mahasiswa anggota aktif PSM di Universitas  X  kota Bandung. 
Penelitian ini bertujuan untuk memperoleh gambaran terhadap derajat 
kemampuan self-regulation (berserta dengan fase forethought, fase performance 
dan fase self-reflection) serta faktor-faktor yang mempengaruhi self-regulation 
pada mahasiswa anggota aktif PSM di Universitas  X  kota Bandung.  
1. Memberikan informasi sebagai bahan rujukan bagi penelitian lebih lanjut 
mengenai self-regulation.  
1. Memberikan informasi mengenai kemampuan self-regulation dan faktor-
faktor lingkungan sosial yang mendukung kepada mahasiswa anggota 
aktif PSM di Universitas  X  kota Bandung, agar informasi ini dapat 
berguna bagi mahasiswa anggota aktif PSM di Universitas  X  kota 
Bandung dengan memperhatikan self-regulation. 
2. Memberikan informasi mengenai kemampuan self-regulation mahasiswa 
anggota aktif PSM, sehingga unit kegiatan PSM di universitas  X  dapat 
memberikan bimbingan kepada mahasiswa anggota aktif PSM untuk lebih 
dapat melakukan tugas dan tanggungjawabnya dengan memperhatikan 
self-regulation. 
Mahasiswa anggota aktif PSM memiliki tugas dan tanggungjawab sebagai 
mahasiswa dan anggota aktif. Tugas dan tanggungjawab sebagai mahasiswa, yaitu 
melaksanakan belajar mandiri di luar jam kuliah sesuai dengan jumlah SKS yang 
diambil;  memenuhi kehadiran kuliah sesuai dengan jadwal dan aturan yang telah 
ditetapan jurusan atau fakultas, yaitu minimal 75 % untuk mata kuliah teori dan 100 
% untuk mata kuliah pratikum; dan mengerjakan tugas-tugas kuliah (sumber: buku 
akademik dari berbagai fakultas /jurusan di Universitas  X  Bandung). Sedangkan 
tugas dan tanggungjawab sebagai aggota aktif PSM, yaitu mengikuti latihan secara 
rutin, dan mengikuti job atau mengisi suatu acara di dalam maupun di luar kampus.  
 Berdasarkan tugas dan tanggungjawabnya yang terbagi atas berbagai tugas, 
mahasiswa anggota aktif PSM membutuhkan kemampuan untuk mengatur dirinya 
atau yang disebut dengan self-regulation. Self-regulation mengacu pada kemampuan 
untuk berpikir, merasakan, dan bertindak, yang direncanakan dan berulang-ulang 
diadaptasikan terhadap pencapaian tujuan (Zimmerman, 2000).  
  Kemampuan self-regulation pada mahasiswa anggota aktif PSM ini terdiri 
atas tiga fase yang triadic (tiga komponen yang saling berhubungan), yaitu fase 
forethought, fase performance or volitional control, dan fase self-reflection (D. H. 
Schunk & B. J. Zimmerman, 1998 dalam Boekaerts, 2002). Fase yang pertama dalam 
siklus self-regulation adalah fase forethought atau perencanaan. Kognitif yang sudah 
berkembang dengan matang, membuat mahasiswa anggota aktif PSM bisa 
merencanakan tugas sesuai dengan tugas dan tanggungjawabnya. Menurut Boekaerts 
(2002), fase forethought mengacu pada proses yang berpengaruh dalam usaha 
melaksanakan tugas dan menentukan tahap-tahap untuk mencapai tugas dan 
tanggungjawabnya.   
 Mahasiswa anggota aktif PSM yang mampu melakukan self-regulation pada 
fase forethought akan mampu merencanakan secara pribadi tugas yang akan 
dilakukan, seperti memiliki jadwal kuliah yang tidak bertepatan dengan kegiatan 
PSM, sedangkan yang kurang mampu mereka tidak merencanakan kegiatan kuliah 
maupun kegiatan PSM. Pada fase performance or volitional control dikatakan 
mampu jika mereka mampu melaksanakan tugas dan tanggungjawabnya seperti 
mereka dapat melaksanakan kegiatan kuliah tanpa mengganggu kegiatan PSM yang 
telah dijadwalkan sebelumnya, sedangkan yang kurang mampu tidak dapat 
melaksanakan kegiatan yang sudah dijadwalkan secara pribadi maupun yang sudah 
ditetapkan oleh fakultas dan Unit Kegiatan PSM. Pada fase evaluation mereka 
dikatakan mampu jika mereka mampu mengevaluasi tugas yang sudah dilakukan 
seperti mengevaluasi hasil pengerjaan tugas-tugas kuliah, hasil kuis dan UTS/UAS, 
dan kualitas bernyanyi setelah konser sedangkan yang kurang mampu kesulitan 
mengevaluasi tugas yang sudah dilakukan seperti mereka tidak mengevaluasi 
pelaksanaan dalam mengerjakan tugas-tugas kuliah, maupun tidak mengevaluasi 
kualitas bernyanyi setelah konser. 
Pada fase forethought dalam task analysis mahasiswa anggota aktif PSM 
mampu menetapkan goal atau tujuan yang sesuai dengan tugas dan 
tanggungjawabnya sebagai mahasiswa anggota aktif PSM seperti mereka menetapkan 
lulus kuliah dengan tepat waktu dengan IPK di atas 2,75 (goal setting). Selanjutnya 
mahasiswa anggota aktif PSM akan menentukan langkah-langkah yang tepat agar 
tugas yang dilakukan sesuai dengan tugas dan tanggungjawabnya seperti mereka 
membuat jadwal pribadi secara tertulis maupun tidak tertulis mengenai kegiatan yang 
akan dilaksanakan jauh sebelum waktu pelaksanaan tugas dan tanggungjawab 
tersebut (strategic planning). 
 Pada aspek self-motivation beliefs, mahasiswa anggota aktif PSM dapat 
memotivasi dirinya sendiri untuk dapat menjalankan tugas seperti mengungkapkan 
 semangat  yang ditujukan pada diri sendiri dalam menjalankan tugas yang ada. Pada 
tahap self-efficacy, mahasiswa anggota aktif PSM memiliki keyakinan atas 
kemampuan yang dimilikinya untuk menjalankan tugas dan tanggungjawabnya 
seperti mereka merasa yakin untuk menjalankan kegiatan kuliah demi mencapai 
target lulus tepat waktu dengan IPK di atas 2,75. Lalu pada tahap outcome 
expectation, mahasiswa anggota aktif PSM berharap bahwa melakukan tugas yang 
dilakukannya akan bermanfaat bagi dirinya, seperti mendapatkan prestasi di kuliah 
maupun di paduan suara. 
 Mahasiswa anggota aktif PSM memiliki minat yang tinggi menjalankan tugas 
dan tanggungjawabnya (intrinsic interest or value) seperti keinginan untuk 
mendapatkan IPK di atas 2,75 dengan tetap mengikuti konser di luar negeri, dan 
mahasiswa anggota aktif PSM merancang usaha yang akan dilakukan untuk 
mempertahankan dan meningkatkan kualitas melakukan tugas yang sedang 
dilakukannya seperti mempertahankan kemampuan membuat target dalam waktu 
yang efektif untuk mengerjakan tugas-tugas kuliah (goal orientation). 
 Pada aspek self-control dari fase performance or volitional control, 
mahasiswa anggota aktif PSM akan mengontrol dirinya agar fokus terhadap tujuan 
yang ingin dicapainya seperti menjalankan kegiatan-kegiatannya sesuai dengan 
jadwal yang telah dibuat sebelumnya. Mahasiswa anggota aktif PSM akan 
mengarahkan dirinya untuk melakukan tugas dan tanggungjawabnya sebagai 
mahasiswa anggota aktif PSM seperti tetap mengarahkan dirinya untuk tetap 
mengerjakan tugas kuliah setelah selesai latihan PSM (self-instruction). 
Membayangkan keberhasilan dalam menjalankan tugas dan 
tanggungjawabnya seperti IP (Indeks Prestasi) yang selalu naik akan mempengaruhi 
IPK (Indeks Prestasi Kumulatif) mereka (imagery), memusatkan perhatian dan 
menyaring proses yang lain atau kejadian yang eksternal yang tidak berkaitan agar 
dirinya tetap dapat menjalankan tugasnya dengan benar seperti mengatur diri untuk 
konsentrasi pada saat kuliah, mengerjakan tugas maupun pada saat latihan menyanyi 
(attention focusing), serta melakukan kegiatan untuk menyelesaikan tugas dan 
tanggungjawabnya sebagai mahasiswa dan anggota aktif PSM seperti membuat 
langkah-langkah belajar mata kuliah pada saat jeda waktu latihan menyanyi di PSM 
(task strategis). 
 Selanjutnya (Self-Observation) mahasiswa anggota aktif PSM akan 
mengamati dan mengingat hal yang telah dialaminya selama melakukan tugas dan 
tanggungjawabnya seperti mengingat hal-hal yang menghambat dan mendukung 
dalam mengerjakan tugas-tugas kuliah maupun latihan menyanyi (self-recording), 
dan mencoba hal-hal baru yang belum pernah dilakukan sebelumnya (self-
experimentation). 
 Setelah melakukan tugas dan tanggungjawabnya, mahasiswa anggota aktif 
PSM akan melakukan evaluasi yaitu fase self-reflection terhadap hasil melakukan 
tugas yang sudah dilakukannya. 
 Pada aspek self-judgment, mahasiswa anggota aktif PSM mampu menilai 
seberapa besar usaha yang dilakukannya selama menjalankan tugas, lalu akan 
membandingkan dengan hasil evaluasi apakah mahasiswa anggota aktif PSM banyak 
kesalahan atau kegagalan dalam melakukan tugas atau sebaliknya seperti 
membandingkan hasil setiap ujian yang dihadapi dengan hasil ujian sebelumnya (self-
evaluation). Mahasiswa anggota aktif PSM akan menemukan keterkaitan hubungan 
antara usaha yang dilakukan dengan hasil kerja yang diperoleh seperti dapat 
menentukan hal apa saja yang mempengaruhi penurunan dan kenaikan IP (Indeks 
Prestasi) (causal attribution).  
 Pada aspek self-reaction, mahasiswa anggota aktif PSM menentukan apakah 
dirinya merasa puas atau tidak puas atas hasil evaluasi seperti dapat merasakan 
kepuasan dari IP (Indeks Prestasi) dan saat bersamaan merasakan puas saat mendapat 
penghargaan di konser (self-satisfaction). Kemudian dari penilaian tersebut maka 
mahasiswa anggota aktif PSM akan memunculkan reaksi terhadap hasil kerja yang 
diperoleh; jika adaptif inferences maka mahasiswa akan mencari kemampuan baru, 
strategi baru seperti mengerjakan tugas dengan waktu lebih efektif dan jika defensif 
inferences maka akan mengembangkan suatu reaksi yang menghindari ketidakpuasan 
yang lebih besar yang akan datang seperti tidak mengikuti kegiatan PSM karena nilai 
ujian akademik turun (adaptive or defensive inferences). 
Bagi mahasiswa anggota aktif PSM yang kurang mampu Pada fase 
forethought dalam task analysis akan menetapkan goal yang kurang sesuai dengan 
tugas dan tanggungjawabnya sebagai mahasiswa anggota aktif PSM seperti mereka 
tidak menetapkan lulus tepat waktu dengan IPK di atas 2,75 dan tidak dapat 
mengikuti konser-konser sebagai tujuan mereka di PSM (goal setting). Selanjutnya 
mahasiswa anggota aktif PSM akan mengalami kesulitan dalam membuat strategi 
yang tepat seperti mereka membuat rencana hanya dalam waktu sehari sebelum tugas 
dan tanggungjawabnya dilaksanakan (strategic planning). 
 Pada aspek self-motivation beliefs, mahasiswa anggota aktif PSM kurang 
mampu memotivasi dirinya sendiri seperti bersikap malas-malasan dalam mengikuti 
kuliah. Pada tahap self-efficacy, mahasiswa anggota aktif PSM kurang yakin terhadap 
kemampuan dan kapasitas dirinya untuk melakukan tugas dan tanggungjawabnya 
sebagai mahasiswa anggota aktif PSM seperti tidak hadir latihan Paduan Suara saat 
ada kuis dikarenakan takut tidak bisa belajar dengan baik. Lalu pada tahap outcome 
expectation, mahasiswa anggota aktif PSM tidak terlalu berharap adanya manfaat 
melakukan tugas dan tanggungjawabnya dengan benar seperti tidak mempersiapkan 
diri dengan baik saat menghadapi kuis karena mengikuti latihan. 
 Mahasiswa anggota aktif PSM yang kurang mampu tidak memiliki minat 
yang akan melatarbelakangi mereka dalam melakukan tugas dan tanggungjawabnya 
seperti mereka mengikuti kegiatan Paduan Suara dan kuliah hanya sebagai kewajiban 
bukan minat yang timbul dari diri sendiri, dan mahasiswa anggota aktif PSM yang 
kurang mampu tidak merancangkan usaha yang akan dilakukan untuk 
mempertahankan dan meningkatkan kualitas kemampuan melakukan tugas dan 
tanggungjawabnya sebagai mahasiswa anggota aktif PSM seperti tidak berusaha 
meningkatkan kualitas belajar mandiri  (goal orientation). 
 Pada aspek self-control dari fase performance or volitional control, 
mahasiswa anggota aktif PSM yang kurang mampu akan mengalami kesulitan 
mengarahkan dirinya agar fokus terhadap tujuan yang ingin dicapainya seperti 
melakukan atau mengikuti kegiatan lain di luar jadwal yang telah ditentukan 
sebelumnya. Mahasiswa anggota aktif PSM yang kurang mampu akan mengalami 
kesulitan dalam mengarahkan dirinya untuk melakukan tugas dan tanggungjawabnya 
seperti tidak hadir dalam mengerjakan tugas-tugas kelompok kuliah atau pribadi yang 
telah dijadwalkan sebelumnya (self-instruction). 
Mahasiswa anggota aktif PSM yang kurang mampu akan mengalami kesulitan 
dalam membayangkan keberhasilan untuk melakukan tugas dan tanggungjawabnya 
dengan benar seperti mereka tidak membayangkan bahwa setiap nilai-nilai ujian di 
kuliah mempengaruhi IP (Indeks Prestasi) dan IPK (Indeks Prestasi Kumulatif) 
mereka (imagery), mereka kesulitan dalam memusatkan perhatian dan menyaring 
proses yang lain agar dirinya tetap dapat melakukan tugas dan tanggungjawabnya 
dengan benar seperti terus merasa terganggu dengan masalah-masalah lain saat 
kuliah, mengerjakan tugas, dan pada saat latihan menyanyi  (attention focusing), serta 
mengalami kesulitan dalam melakukan kegiatan untuk menyelesaikan tugas dan 
tanggungjawabnya sebagai mahasiswa anggota aktif PSM seperti tidak bisa belajar 
mata kuliah selama waktu yang telah dijadwalkan atau dialokasikan (task strategis). 
 Selanjutnya (Self-Observation) mahasiswa anggota aktif PSM akan 
mengalami kesulitan dalam mengamati dan mengingat hal-hal yang dialaminya 
seperti tidak dapat mengingat kembali hal-hal yang menghambat dan mendukung 
dalam mengerjakan tugas-tugas kuliah maupun latihan menyanyi (self-recording), 
dan sulit untuk melakukan tugas dan tanggungjawab yang belum pernah 
dilakukannya seperti mengisi waktu luang dengan membaca buku kuliah (self-
experimentation). 
 Selanjutnya mahasiswa anggota aktif PSM akan mengalami kesulitan pada 
fase self-reflection. Pada aspek self-judgment, mahasiswa anggota aktif PSM yang 
kurang mampu sulit menilai seberapa besar usaha yang dilakukannya, lalu mahasiswa 
anggota aktif PSM tersebut sulit membandingkan hasil evaluasi yang didapat seperti 
tidak mengerti kenapa nilai ujiannya turun (self-evaluation). Mahasiswa anggota aktif 
PSM akan mengalami kesulitan untuk menentukan keberhasilan yang telah 
dicapainya itu disebabkan oleh keterbatasan kemampuannya atau kurangnya berusaha 
seperti tidak dapat mengetahui hal apa saja yang mempengaruhi penurunan dan 
kenaikan IP (Indeks Prestasi) (causal attribution).  
 Pada aspek self-reaction, mahasiswa anggota aktif PSM yang kurang mampu 
akan kesulitan menentukan apakah dirinya merasa puas atau tidak puas atas hasil 
evaluasi yang didapat seperti tidak dapat merasakan kepuasan dari IP (Indeks 
Prestasi) (self-satisfaction). Kemudian mahasiswa anggota aktif PSM akan 
memunculkan reaksi terhadap hasil kerja yang diperoleh; jika adaptif inferences 
maka mahasiswa akan mencari kemampuan baru, strategi baru dan jika defensif 
inferences maka akan mengembangkan suatu reaksi yang menghindari ketidakpuasan 
yang lebih besar yang akan datang (adaptive or defensive inferences). 
 Menurut Mach (1988) (dalam Boekaerts 2002), terdapat dua faktor yang 
mempengaruhi self-regulation mahasiswa anggota aktif PSM, yaitu lingkungan sosial 
dan lingkungan fisik. Lingkungan sosial mahasiswa anggota aktif PSM yang dapat 
mempengaruhi mereka yaitu teman kuliah dan sesama anggota aktif PSM, dosen wali 
dan keluarga. Teman kuliah dan sesama anggota aktif PSM dapat memberikan 
dukungan, seperti ketika menghadapi ujian tengah semester dan ujian akhir semester 
dan saat menghadapi konser mereka saling memberikan semangat untuk 
melakukannya sehingga dapat meningkatkan keyakinan diri untuk dapat menjalankan 
tugas dan tanggungjawab yang akhirnya dapat meningkatkan kemampuan self-
regulation, begitu pula dengan dukungan dosen wali seperti sikap saling menghargai 
membuat diri mereka dihargai dan dukungan keluarga seperti nasehat untuk mengatur 
waktu dengan baik membuat diri mereka termotivasi sehingga walaupun tugasnya 
banyak mereka tidak menganggap sebagai suatu kesulitan. Selain itu lingkungan fisik 
seperti jadwal kuliah dan jadwal latihan dapat menjadi acuan bagi mereka dalam 
menjalankan tugasnya. 
 Lingkungan sosial dan fisik dipandang oleh para peneliti kognitif sosial 
sebagai suatu sumber untuk meningkatkan forethought, performance or volitional 
control dan self-reflection. Lingkungan sosial dan fisik dapat mempengaruhi proses-
proses self-reflection dengan cara yang hampir serupa dengan proses-proses 
forethought dan fase performance (Zimmerman, 2000). 
 Di samping itu menurut Bandura & Kupers (dalam Boekaerts,2000) 
mahasiswa anggota aktif PSM yang memberikan penghargaan terhadap pencapaian 
prestasi akan lebih berhasil dari pada mahasiswa anggota aktif PSM yang melakukan 
aktivitas yang sama tanpa pendorong pada self-administered incentived (dorongan 
yang timbul dari diri sendiri). Misalnya bila mahasiswa anggota aktif PSM 
melakukan tugas sesuai dengan tugas dan tanggungjawabnya akan mendapatkan 
prestasi di kuliah dan di PSM, bila dibandingkan dengan mereka yang tidak 
berkeinginan untuk melakukan sesuai dengan tugas dan tanggungjawabnya, mereka 
akan mengerjakan tugas dengan kurang efektif. Hal ini sesuai dengan diungkapkan 
oleh Zimmerman, (2002). 
Skema 1.1. Skema kerangka pemikiran 
Fase Self-reflection 
Kegiatan PSM : 
1. Lingkungan sosial: 
2. Lingkungan fisik: 
1. Mahasiswa anggota aktif PSM memiliki kesulitan yang berbeda ketika melakukan 
tugas dan tanggungjawab sebagai mahasiswa anggota aktif PSM. 
2. Mahasiswa anggota aktif PSM memerlukan self-regulation untuk dapat melakukan 
tugas dan tanggungjawabnya sesuai dengan perannya sebagai mahasiswa dan 
anggota aktif PSM. 
3. Mahasiswa anggota aktif PSM memiliki derajat kemampuan self-regulation yang 
berbeda - beda (Mampu dan Kurang Mampu). 
 Secara khusus, perspektif sosial kognitif memandang self-regulation sebagai 
suatu interaksi antara proses personal, behavior, dan environment yang bersifat 
triadic (saling berhubungan) (Bandura, 1986). Lebih spesifik lagi, hal ini tidak hanya 
menyangkut kemampuan kemungkinan-kemungkinan self-managing dalam 
lingkungan, tapi juga pengetahuan dan penghayatan mengenai komponen personal 
untuk menerapkan keterampilan behavioral tersebut secara relevan. Self-regulation 
diartikan pada pikiran, perasaan dan tindakan yang digerakkan dari dalam diri yang 
telah direncanakan, dan secara berulang diadaptasi dengan pencapaian personal goal. 
Definisi self-regulation ditinjau dari tindakan dan proses yang covert yang 
keberadaan dan kualitasnya tergantung dari kualitas kemampuan dan motif, 
dibedakan dari definisi yang menekankan pada trait (sifat), ability, dan stage-stage 
kompeten yang tunggal. Definisi proses dapat menjelaskan mengapa seseorang dapat 
mengatur dirinya pada satu tipe perilaku tampilan tetapi tidak pada tipe yang lain. 
Perumusan personal agency ini juga dibedakan dari pandangan metacognitif yang 
ditegaskan hanya menurut kondisi pengetahuan dan penalaran deduktif, sebagai 
contoh, pemilihan strategi cognitif. Meskipun metakognisi memegang peranan 
penting, namun self-regulation juga tergantung pada self-belief dan reaksi afektif 
seperti keraguan, ketakutan mengenai konteks tingkah laku yang spesifik 
(Zimmerman, 1995b). Dapat dicobakan pada pemain catur yang bercita-cita tinggi, 
mungkin ingin mencoba strategi defense yang baik tetapi hal ini seringkali di 
tinggalkan oleh mereka yang mulai merasa ragu pada saat bertemu dengan lawan 
yang di anggap lebih berat. Self-process yang berkaitan secara kontekstual seperti 
mempersepsi efficacy, telah terbukti sangat cocok untuk menjelaskan berbagai variasi 
dalam personal motivation untuk self-regulation seseorang. (Bandura, 1997; Pajares 
& Miller, 1994; Zimmerman, 1995a). Self-efficacy berarti keyakinan mengenai 
kemampuan seseorang untuk mengorganisasikan dan melaksanakan tindakan yang 
diperlukan untuk pencapaian perwujudan skill yang sudah ditentukan untuk 
melaksanakan tugas-tugas yang  spesifik. 
 Self-regulation dideskripsikan sebagai sebuah siklus karena feedback dari 
tingkah laku sebelumnya digunakan untuk membuat penyesuaian dalam usaha atau 
tindakan saat ini. Adjustments seperti itu penting, karena faktor-faktor personal, 
behavior, dan environmental secara konstan selalu berubah selama proses belajar dan 
bertingkah laku, dan harus di observasi atau di monitor dengan menggunakan ke tiga 
rangkaian self-oriented feedback loops. Behavioral self-regulation terdiri dari self-
observing, dan secara strategis menyesuaikan proses perilakunya, misalnya metode 
belajar dimana environmental self-regulation diartikan pada penyesuaian kondisi 
lingkungan dan hasil yang dicapai. Self-regulation covert melibatkan monitoring dan 
adjusting dari kognitif dan afektif seperti halnya dalam mengingat atau relaksasi. 
Keakuratan dan kekonstanan seorang learner self-monitoring atau orang yang sedang 
belajar pada sumber triadic ini secara langsung berpengaruh terhadap efektivitas 
terhadap penyesuaian strategi self-belief. Berbeda dari rangkaian close loops, yang 
bersifat reactive membatasi sebuah standart untuk tidak berubah, melainkan 
meningkatkan performance untuk bergerak sedekat mungkin pada standart yang 
sudah ditentukan, (Locke, 1991), open loops secara proactive yang meningkatkan 
ketidak sesuaian tingkah laku dengan pencapaian tujuan dan melihat lebih banyak 
tantangan pekerjaan. Sebagai contoh, ketika pemain catur memutuskan untuk 
menaikkan level permainannya, mereka lebih sulit untuk menjadi sukses berprestasi, 
tetapi menggunakan ketidaksesuaian antara standart dan prestasi yang telah dicapai 
Jadi self-regulation melibatkan proses triadic yang secara proaktif dan juga reaktif 
untuk pencapaian personal goal. 
Covert self -
Skema 2.1. Bentuk Triadic Self-Regulation 
Self-regulation 
Telah dibicarakan bahwa setiap orang berusaha untuk meregulasi fungsi 
dirinya dengan berbagai cara untuk mencapai goal atau tujuan dalam hidup dan 
merupakan hal yang kurang tepat untuk membicarakan tentang individu yang tidak 
dapat meregulasi diri atau bahkan tidak tepat untuk individu yang memiliki self-
regulation (Winne, 1997). Dari prespektif ini, apa yang membedakan bentuk self-
regulation yang efektif dan tidak efektif selain dari kualitas dan kuantitas dari proses 
self-regulatory. Proses yang paling efektif telah diidentifikasikan melalui berbagai 
sumber empirik yang berbeda, termasuk wawancara dengan para ahli yang dikenal 
sukses dan self-discipline mereka (e.g., Ericsson & Lehman, 1996; Zimmerman & 
Martinez-Pons, 1986, 1988), studi klinis yang mengalami disfungsi self-regulation  
individu-individu (Watson & Tharp, 1993), dan penelitian eksperimental mengenai 
metode personal selama ada tuntutan dari pelaksanaan tugas-tugas (Kanfer & 
Ackerman, 1989; Kuhl, 1985). Isu yang terpenting adalah untuk mengerti bagaimana 
proses ini berhubungan secara structural dan berulang terus menerus.  
Berdasarkan perspektif social cognitive, proses self-regulatory dan dengan 
keyakinan yang mengikutinya digambarkan dalam tiga fase perputaran: Fase 
forethought (perencanaan), performance or volitional control (kehendak, dan self-
reflection (proses refleksi diri). Forethought berkaitan dengan proses-proses yang 
berpengaruh yang mendahului usaha untuk bertindak dan menentukan tahap-tahap 
untuk mencapai usaha tersebut. Performance or Volitional Control meliputi proses-
proses yang terjadi selama usaha motorik itu berlangsung dan berdampak pada 
perhatian dan tindakan yang dilakukan. Self-reflection meliputi proses yang terjadi 
setelah suatu usaha dilakukan dan pengaruh dari respon individu terhadap 
pengalamannya tsb. Pada self-reflection ini, termasuk pula pengaruh forethought 
terhadap usaha-usaha motorik berikutnya jadi melengkapi siklus Self-regulatory. 
Skema 2.2 Putaran Fase Self-Regulation 
A. Forethought Phase 
Terdapat dua kategori yang berbeda saling berhubungan erat dengan 
forethought. 
1. Task analysis 
2. Self-Motivation beliefs 
Self-reflection 
Struktur fase dan Sub-subproses pada self-regulation 
Fase putaran Self-Regulation 
Forethought Performance/Volitional 
Self-Reflection 
Self-control 
 Self-instruction 
Self-Judgement 
 Self-evaluation 
Self-Motivation Beliefs 
 Self-Efficacy 
interest/value 
Self-observation 
 Self-recording 
 Self-experimentation 
Self-reaction 
 Self-satisfaction/affect 
 Adaptive-Defensive 
Inti dari task analysis meliputi penentuan goal ( the setting of goal). Goal 
setting ini berkenaan dengan pengambilan keputusan dalam hal hasil belajar atau 
perilaku yang spesifik seperti mengatasi masalah dalam persoalan matematika selama 
proses belajar berlangsung  (Locke dan Lathan,1990, dalam Boekaerts, 2000). Goal 
sistem dari individu yang memiliki self-regulationnya tinggi tersusun secara hierarki, 
dan proses-proses goal tersebut akan dijalankan sebagai regulators (pengatur) untuk 
mendapatkan tujuan atau hasil yang sama dengan hasil yang pernah dicapai. Proses 
subgoal tidak hanya sekedar mengecek angka pada jalur untuk memperoleh hasil 
yang tinggi; di samping itu mereka menanamkan dengan arti personal karena mereka 
membawa bukti dari perkembangan. Misalnya, murid belajar tentang cara servis 
dalam tennis akan merasa meningkat efikasinya tentang keterampilannya sebagai 
komponen yang telah didapatkannya, seperti cara memegang raket, memukul bola, 
dan membalikkan bola. Bandura dan Schunk (1981) melaporkan fakta-fakta tentang 
murid-murid yang mengejar dan mendapatkan tujuan-tujuan dalam matematik, 
mereka mengembangkan self-efficacy yang sangat pesat dan minat intrinsic dalam 
topic ini.  
Bentuk kedua dari task analysis adalah strategic planning (Weinstein & 
Mayer, 1986). Agar suatu skill dapat dikuasai dan dilaksanakan secara optimal, 
learner membutuhkan metode-metode yang tepat untuk tugas dan situasi atau kondisi. 
Strategi self-regulative merupakan proses dan tindakan personal yang bertujuan dan 
diarahkan untuk memperoleh dan menunjukkan suatu skill. (Zimmerman, 1989). 
Strategi yang dipilih secara tepat dapat meningkatkan performance dengan 
mengembangkan kognisi, mengontrol affect, dan mangarahkan kegiatan motorik. 
(Pressley & Wolloshyn, 1995). Sebagai contoh, strategi kata kunci atau integrative 
image diketahui untuk meningkatkan recall dan menggunakan informasi selama 
performance atau kegiatan motorik (Schneider & Pressley, 1997). Strategi 
perencanaan dan seleksi membutuhkan adjustment yang berulang (cyclical 
adjustment) karena fluktuasi atau perubahan dalam covert personal, behavioral, dan 
komponen-komponen environment. Tidak ada self-regulation yang bekerja sama 
baiknya untuk semua orang, dan beberapa, bila ada, strategi akan bekerja optimal 
untuk seseorang pada semua tugas dan situasi. Sejalan dengan perkembangan suatu 
skill, keefekifan perolehan suatu strategi seringkali menurun di saat strategi lain 
menjadi penting, misalnya bagi seorang pemain golf pemula, dari strategi 
mengayunkan ke strategi mengarahkan bola. Jadi, sebagai akibat dari perbedaan dan 
perubahan intrapersonal, interpersonal, dan kondisi-kondisi sesuai konteks, individu 
yang memiliki self-regulation harus terus menerus menyesuaikan antara tujuan 
dengan pemilihan strategi. 
 Keterampilan self-regulation (self-regulation skills) menjadi kecil nilainya 
jika seseorang tidak dapat memotivasi diri sendiri untuk menggunakannya. Proses-
proses foretought yang dasar dari goal setting dan strategic planning adalah self-
motivational beliefs yang penting, yaitu self-efficacy, autcome expectations, intrinsic 
interest or valuing, dan goal orientation. Sebagaimana telah tercantum sebelumnya, 
self-efficacy merujuk pada personal beliefs mengenai arti belajar dan bertindak secara 
efektif, sementara outcome expectations merujuk pada keyakinan tentang pencapaian 
hasil akhir dari suatu performance (Bandura, 1997). Sebagai contoh, self-efficacy 
mengacu pada keyakinan bahwa seseorang dapat memperoleh nilai A dari suatu 
kursus, dan outcomes mengacu pada harapan mengenai konsekuensi dari nilai ini 
yang akan diperoleh setelah lulus, misalnya diperlukan untuk mencari pekerjaan yang 
diinginkan. 

