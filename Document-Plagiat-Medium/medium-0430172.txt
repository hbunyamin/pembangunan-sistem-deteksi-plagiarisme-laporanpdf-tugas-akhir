Penelitian ini untuk mengetahui profil komitmen organisasi guru di SDK 2 
 X  Program MATIUS Bandung. Rancangan yang digunakan menggunakan 
metode deskriptif dengan teknik survei. Sampel 23 guru tetap di SDK 2  X  
Program MATIUS Bandung.  
Penelitian menggunakan teori Meyer & Allen (1997) menyatakan bahwa 
komitmen organisasi adalah suatu keadaan psikologis yang merupakan 
karakteristik hubungan antara anggota dengan organisasinya, dan mempunyai 
implikasinya berupa keputusan untuk berhenti atau terus menjadi anggota 
organisasi tersebut. Komitmen organisasi didasari oleh tiga aspek yaitu affective 
commitment, continuance commitment, dan normative commitment. Dilihat dari 
tinggi rendahnya tiga aspek tersebut akan membentuk 8 tipe profil komitmen 
organisasi. 
Alat ukur yang digunakan adalah modifikasi alat ukur yang dibuat oleh 
Meyer & Allen yaitu Organizational Commitment Questioner (OCQ). 
Berdasarkan hasil uji vaiditas diperoleh nilai 0,303 sampai 0,887 dan reliabilitas 
alat ukur untuk affective commitment 0,85; continuance commitment 0,841; dan 
normative commitment 0,764 (reliabilitas tinggi). 
Berdasarkan pengolahan data diperoleh hasil 39,1% guru di SDK 2  X  
Program MATIUS Bandung memiliki profil komitmen organisasi affective tinggi, 
continuance rendah, normative tinggi. Dengan demikian sebagian besar guru 
bertahan di SDK 2  X  Program MATIUS Bandung dikarenakan kecintaan guru 
terhadap organisasi dan rasa tanggung jawab terhadap organisasi, dan bukan 
karena kerugian yang diperoleh apabila meninggalkan organisasi.  
Peneliti mengajukan saran agar Pimpinan SDK 2  X  Program MATIUS 
Bandung dapat terus meningkatkan rasa kekeluargaan dalam organisasi, dan 
membantu guru lebih mengerti tugas dan tanggung jawab mereka di SDK 2  X  
Program MATIUS Bandung. Dapat juga dilakukan penelitian mengenai profil 
yang cocok untuk profesi guru. 
Pendidikan mempunyai peranan penting dalam keseluruhan aspek 
kehidupan manusia dan akan berpengaruh langsung terhadap pembentukan 
kepribadian manusia. Di samping itu pendidikan adalah pengembangan 
kemampuan dan jati diri peserta didik sebagai wujud kepribadian yang utuh, 
melalui program pengajaran yang diarahkan melalui kurikulum program studi. 
(Keputusan Mentri Negara Koordinator Bidang Pengawasan Pembangunan dan 
Pendayagunaan Aparatur Negara, Nomor : 38/KEP/MK.WASPAN/9/1999). 
Pendidikan di Indonesia mempunyai dua jalur, yaitu jalur pendidikan 
sekolah dan jalur pendidikan luar sekolah. Jalur pendidikan sekolah merupakan 
pendidikan di sekolah melalui kegiatan belajar mengajar secara berjenjang dan 
berkesinambungan. Jenjang pendidikan yang termasuk dalam jalur pendidikan 
sekolah adalah pendidikan dasar, menengah, dan pendidikan tinggi, sedangkan 
jalur pendidikan luar sekolah merupakan kegiatan belajar mengajar yang tidak 
harus berjenjang dan berkesinambungan, misalnya kursus penyegaran, penataran, 
seminar, lokakarya dan konferensi ilmiah (Fuad Hasan,1995). 
Pada Jalur pendidikan sekolah terdapat jenjang Sekolah Dasar (SD). Di 
Jenjang SD ini siswa sebagai calon Sumber Daya Manusia masa depan harus 
dipersiapkan sebaik mungkin dan itulah yang menjadi tujuan dari YPK  X  maka 
pada tanggal 22 Januari 1973 dengan izin Departemen P&K no.22/93/1.1.2/1972, 
didirikan SDK  X  Bandung. Sejalan dengan waktu SDK  X  Bandung 
dimekarkan menjadi SDK 1  X  dan SDK 2  X , dan untuk menghadapi era 
globalisasi dan segala tantangannya, SDK 2  X  membuka program MATIUS. 
Dengan Visi yaitu  Menjadi lembaga penyedia layanan pendidikan yang 
memperhatikan perkembangan berbagai aspek kepribadian peserta didik secara 
seimbang, baik fisikal, intelektual, emosional, social, maupun spiritual . Dan 
Misinya adalah  Me-MATIUS-kan peserta didik .  
Program MATIUS adalah program sekolah yang menekankan pada 
pembinaan karakter setiap siswanya. Kepanjangan dari MATIUS adalah karakter-
karakter yang ingin dikembangkan pada anak didik yaitu Mandiri, Aktif, Taat, 
Inovatif, Ulet, dan Sopan. Di SDK 2  X  Program MATIUS Bandung, program 
sekolahnya adalah program full day school, sekolah mulai pukul 07.30 sampai 
dengan 15.30. Selain itu SDK 2  X  Program MATIUS Bandung ini menekankan 
penggunaan Bahasa Inggris dan Mandarin dalam penyampaian materi setiap hari 
di dalam kelas, tetapi kurikulum yang dipakai tetap kurikulum nasional.  
Penekanan karakter MATIUS ditunjang dengan fasilitas dan gaya 
pembelajaran yang sesuai untuk mengasah karakter MATIUS setiap siswa. Di 
sekolah setiap siswa mempunyai loker masing-masing yang harus selalu dijaga 
kerapian, kelengkapan, dan kebersihannya, disini siswa diajarkan untuk mandiri. 
Karakter taat ditekankan pada saat siswa harus mengikuti schedule dan mematuhi 
peraturan yang ditetapkan oleh sekolah. Penekanan karakter aktif dilakukan pada 
jam pelajaran, siswa berpartisipasi di setiap kegiatan belajar dan kegiatan tersebut 
dapat memancing siswa untuk lebih aktif. Pembentukan karakter inovatif 
didorong melalui sistem pembelajaran yang lebih menekankan pada penemuan-
penemuan baru yang bisa dilakukan oleh siswa.  
Karakter ulet dapat dibentuk pada saat siswa belajar di kelas mereka 
diharapkan mau mencoba hal-hal baru dan berusaha menyelesaikan semua tugas 
yang mereka anggap sulit tanpa mengeluh. Terakhir mendukung pembentukan 
karakter sopan maka setiap siswa diberikan  Phrase of the day  kalimat-kaliamat 
dalam Bahasa Inggris atau Mandarin untuk digunakan saat mereka memberi 
salam, meminta ijin, meminta tolong, dan berkomunikasi dengan guru dan murid 
lainnya. Pemantauan perkembangan karakter siswa setiap hari diadakan jam BP 
(Bimbingan Prilaku), yaitu jam pelajaran yang berupa pengarahan dan pembinaan 
karakter dari wali kelasnya. Perkembangan karakter di rumah dipantau dengan 
buku karakter yang harus diisi oleh orang tua, kemudian dilaporkan kepada guru 
kelas setiap minggunya. 
Jumlah guru di SDK 2  X  Program MATIUS Bandung yaitu 23 guru 
tetap yang mempunyai jabatan tertentu seperti ketua kelas (menjadi ketua guru 
untuk satu angkatan), wali kelas (menjadi wali di satu kelas di satu angkatan), dan 
guru bidang studi (mengajar bidang studi tertentu saja), sedangkan guru honorer 
kurang lebih 15 guru memiliki jabatan sebagai guru bidang studi. Setiap guru 
tetap memiliki tugas dasar mengajar dan membimbing siswanya memahami 
materi dan juga memotivasi siswa dalam belajar. Kewajiban guru tetap adalah 
membuat program pengajaran di awal tahun, membuat silabus, membuat 
persiapan bahan pengajaran, dan mempersiapkan alat peraga, bekerja sama 
dengan sesama guru untuk menunjang kelancaran belajar mengajar.  
Guru MATIUS mempunyai tugas khusus yang berbeda dari guru sekolah 
lainnya yaitu menonjolkan karakter MATIUS secara langsung, dengan cara 
memberikan contoh bagaimana berlaku mandiri, aktif, taat, inovatif, ulet, dan 
sopan dalam kehidupan sehari-hari. Jadi, guru tetap di SDK 2  X  Program 
MATIUS Bandung dituntut lebih memperlihatkan karakter-karakter yang ada 
dalam program MATIUS dalam kehidupannya sehari-harinya. Setiap guru tetap 
menangani 20 sampai 25 siswa  di tiap kelasnya, tetapi para guru tetap diharapkan 
memantau perkembangan karakter semua anak di SDK 2  X  Program MATIUS 
Bandung. Setiap guru tetap juga dituntut untuk memiliki keahlian yang baik 
dalam mengajar, walaupun sebagian besar guru tetap berasal dari latar belakang 
pendidikan bukan guru. Setiap guru tetap juga dituntut untuk dapat menguasai 
Bahasa Inggris untuk menunjang proses pembelajaran. 
Tuntutan bagi guru tetap di Program MATIUS berbeda dari sekolah lain. 
Oleh karena itu, guru tetap menjadi salah satu sumbar daya yang paling 
berpengaruh dalam proses belajar mengajar dan kelancaran jalannya program 
MATIUS. Setiap guru tetap di SDK 2  X  Program MATIUS Bandung 
membutuhkan suatu profil komitmen tertentu untuk dapat bertahan dan 
melakukan semua kewajibannya dengan baik sebagai guru tetap di SDK 2  X  
Program MATIUS Bandung.  
Menurut teori dari Meyer & Allen komitmen organisasi terdiri dari tiga 
komponen, yaitu affective commitment, continuance commitment, dan normative 
commitment. Aspek ini lebih merupakan suatu komponen daripada suatu aspek, 
jadi motif yang muncul dari gabungan ke tiga komponen tadi disebut profil 
komitmen organisasi. Setiap komponen dilihat tinggi rendahnya dan itu yang 
membentuk profil komitmen setiap guru tetap di SDK 2  X  Program MATIUS 
Bandung. Misalnya, guru dengan affective commitment dan normative 
commitment yang tinggi, tetapi continuance commitment rendah akan melakukan 
pekerjaannya sebaik mungkin, karena mereka merasa adanya keterikatan 
emosional dengan anak didik dan juga merasakan tanggung jawab yang besar 
untuk menjaga reputasi SDK 2  X  Program MATIUS Bandung. Berbeda dengan 
guru dengan yang memiliki affective commitment dan continuance commitment 
yang tinggi, tetapi normative commitment rendah guru tersebut akan merasa 
senang bekerja di SDK 2  X  Program MATIUS Bandung karena ia merasa 
nyaman bekerja di lingkungan SDK 2  X  Program MATIUS Bandung Bandung 
dan juga mendapatkan keuntungan secara finansial, atau mendapatkan fasilitas 
yang membuatnya bertahan di SDK 2  X  Program MATIUS Bandung.  
Guru dengan continuance commitment dan normative commitment yang 
tinggi, tetapi affective commitment rendah akan merasa mendapatkan keuntungan 
lebih apabila dia bekerja di SDK 2  X  Program MATIUS Bandung, dan ia juga 
merasakan adanya tanggung jawab untuk membantu SDK 2  X  Program 
MATIUS Bandung agar lebih maju. Perbedaan-perbedaan profil komitmen itulah 
yang nantinya dapat menyebabnya perbedaan motif yang akan tampak dalam 
prilaku setiap guru tetap di SDK 2  X  Program MATIUS Bandung. Profil 
komitmen organisasi juga memiliki indikator yang dapat dinilai, salah satu 
indikatornya yaitu performance kerja guru tetap di SDK 2  X  Program MATIUS 
Bandung, dan juga indikator lain seperti tingkat absensi, turn over, dan masa kerja 
(Meyer dan Allen, 1997). 
Berdasarkan wawancara dengan kepala sekolah SDK2  X  Program 
MATIUS Bandung diketahui bahwa performance yang diharapkan dari setiap 
guru di SDK 2  X  Program MATIUS Bandung ini adalah guru-guru diharapkan 
memiliki hati yang mau melayani untuk mengubahkan anak-anak dari yang tidak 
MATIUS menjadi MATIUS. Setiap guru diharapkan dapat berbahasa Inggris dan 
atau Mandarin karena bahasa asing adalah salah satu keunggulan dari SDK 2  X  
Program MATIUS Bandung, dan beragama Kristen, karena dengan iman Kristen 
karakter MATIUS juga bisa diterapkan.  
Menurut kepala sekolah belum semua guru tetap memiliki performance 
yang diharapkan. Untuk mengatasi hal tersebut sekolah mengadakan pembinaan 
seperti conversation class, pembinaan karakter untuk guru, retreat guru, dan 
acara-acara kebersamaan lainnya. Kepala sekolah mengharapkan semua guru 
merasa menjadi bagian keluarga besar di sekolah tersebut. Sedangkan untuk 
beberapa alasan guru yang tidak bertahan di sekolah tersebut dikarenakan ada 
beberapa guru yang memutuskan untuk bersekolah kembali, melahirkan, menikah, 
tetapi ada juga yang dikarenakan ada konflik intern dengan sesama guru maupun 
dengan kepala sekolah.  
Fenomena yang terjadi di SDK 2  X  Program MATIUS Bandung, dilihat 
dari turn over yang terjadi pada setiap tahunnya yaitu kurang lebih sebanyak 8,3% 
pada awal tahun ajaran. Berdasarkan wawancara dengan 5 orang mantan guru 
tetap SDK 2  X  Program MATIUS Bandung, alasan turn over ini dikarenakan 
permintaan dari guru-guru yang bersangkutan untuk berhenti dari sekolah, bukan 
diberhentikan oleh sekolah, dengan alasan yang beragam antara lain, pindah kerja, 
menikah, meneruskan sekolah, mempunyai anak, merasa kurang berkembang, 
masalah keuangan, dan merasa tidak betah dengan lingkungan kerja. Sekolah 
dalam hal ini juga berusaha mempertahankan guru-guru tersebut dengan cara 
membujuk secara langsung guru yang bersangkutan. 
Fenomena lainnya didapat dari hasil observasi dan wawancara dengan 10 
orang guru tetap. Hasil observasi mengenai tingkat keterlambatan dan absensi 
yang cukup bermasalah karena seringkali dibahas pada rapat yang diadakan setiap 
minggunya. Ada kebijakan sekolah tentang gaji ke-13, yang menyatakan setiap 
guru tidak berhak mendapatkan gaji ke-13 apabila jumlah keterlambatan, izin, dan 
cuti melebihi 30 hari kerja dalam satu tahun ajaran. Sedangkan absensi di SDK 2 
 X  Program MATIUS Bandung ini masih memakai sistem absen tertulis yang 
sangat mengandalkan kejujuran dari guru-guru. Hal ini merupakan indikator dari 
affective commitment yang rendah, karena terdapat korelasi positif antara 
kehadiran kerja dengan affective commitmen seseorang (Mathieu dan Zajac, dalam 
Meyer & Allen, 1997). 
Mengenai masa kerja, guru tetap di SDK 2  X  Bandung ini terbagi 
menjadi 2 kelompok, guru senior yang memiliki masa kerja lebih dari 10 tahun, 
dan guru junior yang memiliki masa kerja kurang dari 10 tahun. Berdasarkan 
wawancara, dengan 9 orang guru tetap, mereka mengatakan suasana kerja di SDK 
2  X  Program MATIUS Bandung cukup nyaman dikarenakan kebersamaan yang 
terlihat cukup erat, tetapi mereka mengatakan bahwa jurang antara senior dan 
junior tetap terasa, dan menyebabkan perasaan canggung diantara kedua 
kelompok tersebut. Kejadian yang menyebabkan terjadinya perasaan tersebut, 
salah satunya adalah menegur dengan cara yang kurang bisa diterima antara guru 
senior dan guru junior. Perbedaan akses ke fasilitas sekolah, seperti akses ke 
perlengkapan mengajar, guru senior dapat lebih mudah meminta perlengkapan 
mengajar dibandingkan guru junior. Semua ini dapat menyebabkan iklim kerja 
terasa kurang nyaman. Mereka pun mengeluhkan pembagian kerja untuk setiap 
kegiatan dibebankan secara tidak merata dan tidak jelas.  
Berdasarkan hasil survey awal pada 10 orang guru tetap di SDK 2  X  
Program MATIUS Bandung untuk mengetahui profil komitmen para guru tetap 
terhadap SDK 2  X  Program MATIUS Bandung, didapatkan informasi bahwa 
10% guru tetap memiliki profile Afe (T) Con (T) Nor (R), karena ia merasakan 
adanya ikatan emisional dengan anak didik dan dengan SDK 2  X  Program 
MATIUS Bandung. Ia merasa bahwa di SDK 2  X  Program MATIUS Bandung, 
ia mendapat keuntungan secara finansial dibandingkan di tempat kerjanya dahulu. 
Tetapi ia tidak merasa harus bertanggung jawab dengan kemajuan SDK 2  X  
Program MATIUS Bandung. 
10% guru tetap merasa sudah cocok dengan lingkungan di SDK 2  X  
Program MATIUS Bandung dan merasakan ikatan emosional dengan anak didik 
dan rekan kerja di SDK 2  X  Program MATIUS Bandung. Mengenai 
penghasilan ia merasa cukup, karena Tuhan pasti mencukupkan, dan ia juga tidak 
pernah merasa bertanggung jawab dengan apa yang terjadi dengan SDK 2  X  
Program MATIUS Bandung. Hal itu menggambarkan profil Afe (T) Con (R) Nor 
(R) pada guru tetap di SDK 2  X  Program MATIUS Bandung. 
20% guru tetap mengatakan menyukai lingkungan dan relasi dengan rekan 
kerja di SDK 2  X  Program MATIUS Bandung yang terasa kekeluargaan 
walaupun mereka kadang merasa ada ketidak cocokkan dengan atasan. Mereka 
juga menganggap pendapatan dan fasilitas yang diberikan oleh sekolah sudah 
cukup baik, walaupun mereka tetap mengharapkan ditambahnya fasilitas yang 
tanpa syarat bagi guru-guru tetap di SDK 2  X  Program MATIUS Bandung. 
Tetapi mengenai perkembangan SDK 2  X  Program MATIUS Bandung mereka 
merasa itu bukan tanggung jawab mereka. Hal itu menggambarkan profil Afe (T) 
Con (T) Nor (R) pada guru tetap di SDK 2  X  Program MATIUS Bandung. 
20% guru tetap merasa nyaman bekerja di SDK 2  X  Program MATIUS 
Bandung karena suasana kerja yang nyaman dan hubungan dengan rekan kerja 
yang baik. Mereka mengatakan pendapatan yang mereka dapatkan masih kurang 
sesuai karena job desk mereka kurang jelas dan membuat pekerjaan mereka 
menjadi terlalu banyak, sehingga harus mencari pekerjaan tambahan di luar 
sekolah. Hal itu menggambarkan profile Afe (T) Con (T) Nor (T) pada guru tetap 
di SDK 2  X  Program MATIUS Bandung. 
40% guru tetap mengatakan merasa senang bekerja di  SDK 2  X  
Program MATIUS Bandung karena hubungan atara rekan kerja yang baik, dan 
lingkungan kerja yang nyaman, meskipun ada yang mengeluhkan tentang 
hubungan dengan atasan mereka yang kurang baik. Mereka mengatakan 
pendapatan mereka biasa saja, mereka merasa sudah cukup nyaman dengan 
pekerjaan mereka jadi pendapatan tidak menjadi masalah. Mereka juga 
mengatakan bahwa mereka mencari cara atau metode mengajar yang lebih baik 
lagi agar kualitas SDK 2  X  Program MATIUS Bandung terus bertambah. Hal 
itu menggambarkan profile Afe (T) Con (R) Nor (T) pada guru tetap di SDK 2 
 X  Program MATIUS Bandung. 
Ingin mengetahui bagaimana profil komitmen organisasi pada guru tetap 
di SDK 2  X  Program MATIUS Bandung. 
Maksud penelitian ini adalah untuk memperoleh gambaran mengenai 
profil komitmen organisasi yang terdapat pada guru tetap di SDK 2  X  Program 
MATIUS Bandung. 
Tujuan penelitian ini adalah untuk memperoleh gambaran yang lebih 
rinci mengenai profil komitmen organisasi pada guru tetap di SDK 2  X  Program 
MATIUS Bandung serta faktor-faktor yang mempengaruhinya. 
1. Memberikan informasi tambahan kepada bidang Psikologi Industri dan 
Organisasi mengenai profil komitmen organisasi pada guru. 
2. Memberikan informasi tambahan pada peneliti lain yang tertarik untuk 
meneliti profil komitmen organisasi dan mendorong dikembangkannya 
penelitian yang berhubungan dengan hal tersebut. 
1. Memberikan informasi kepada Kepala Sekolah SDK 2  X  Program 
MATIUS Bandung mengenai sejauh mana profil komitmen organisasi 
yang dimiliki oleh para guru tetep di SDK 2  X  Program MATIUS 
Bandung sehingga hasil penelitian ini dapat menjadi bahan masukan 
untuk dapat lebih memahami mengenai profil komitmen organisasi dan 
faktor-faktor yang mempengaruhinya. 
2. Menjadi acuan bagi pengurus yayasan di SDK 2  X  Program 
MATIUS Bandung sebagai informasi mengenai gambaran profil 
komitmen organisasi yang dimiliki guru tetap di SDK 2  X  Program 
MATIUS Bandung sehingga dapat memberikan kebijakan-kebijakan 
yang dapat meningkatkan kinerjanya di dalam organisasi. 
Sekolah Dasar Swasta merupakan salah satu lembaga pendidikan yang 
merupakan bagian dari tujuan pendidikan nasional pada umumnya. Untuk 
mencapai keberhasilan dan tujuan pendidikan banyak sekali faktor yang turut 
menentukan terutama instrumen input (tenaga pengarar, fasilitas,dan lain-lain), 
organisasi, dan pengelolaannya. Kualitas sekolah dasar pun harus ditingkatkan, 
yaitu dengan mengetahui profil komitmen yang dimiliki oleh setiap guru agar 
setiap guru dapat lebih bertanggungjawabnya terhadap tugas dan kewajibannya. 
Komitmen organisasi merupakan suatu keadaan psikologis tertentu yang 
merupakan karakteristik hubungan antara anggota dengan organisasinya, dan 
mempunyai implikasi berupa keputusan untuk berhenti atau terus menjadi anggota 
organisasi tersebut (Meyer&Allen,1991). Meyer & Allen (1997) melakukan 
penggabungan konsep pembentuk tiga aspek komitmen, yaitu Affective 
Commitment, Continuance Commitment, dan Normative Commitment. Affective 
Commitment dari Meyer  & Allen (1991) mengarah pada keterikatan emosional 
guru, identifikasi, dan keterlibatan guru pada organisasinya. Ketiga aspek ini yang 
nantinya akan membentuk delapan profil komitmen yang akan diteliti. 
Guru yang memiliki affective commitment akan tetap berada di organisasi 
karena mereka menginginkan hal itu (want to). Guru yang memiliki affective 
commitment yang tinggi akan memiliki keinginan yang kuat untuk menetap dalam 
organisasinya, merasa bangga menjadi anggota dari organisasi tersebut mereka 
memiliki motivasi dan keinginan untuk berkontribusi secara berarti terhadap 
organisasinya, misalnya dengan mengikuti rapat-rapat mingguan, mengikuti 
acara-acara untuk bertahan dalam organisasinya karena mereka merasa bahwa 
keanggotaannya dalam organisasi adalah suatu hal yang penting.  
Guru yang memiliki continuance commitment akan bertahan dalam 
organisasinya apabila mereka mendapatkan imbalan yang sesuai dengan tugas dan 
kewajiban yang mereka berikan. Dalam hal ini, guru-guru di SDK 2  X  Program 
MATIUS Bandung yang menunjukan continuance commitment yang tinggi akan 
bertahan dalam organisasi apabila mendapatkan gaji yang sesuai, diberikan 
penghargaan secara materi dan fasilitas yang dapat menunjang kehidupan guru 
tersebut. 
Guru dengan normative commitment akan merasa memiliki kewajiban 
untuk terlibat dalam aktivitas organisasinya dan mengembangkan dirinya sebagai 
bentuk rasa tanggung jawab atau rasa moral yang dimilikinya, karena mereka 
merasa sudah mendapatkan banyak hal dari organisasi tersebut yang mewajibkan 
mereka untuk membalas budi dengan tetap bertahan dan terus meningkatkan diri 
guna kepentingan organisasi. Dalam hal ini, guru-guru di SDK 2  X  Program 
MATIUS Bandung yang menunjukan normative commitment yang tinggi akan 
bertanggungjawab sebagai seorang guru dan juga aktif dalam kegiatan organisasi 
yang dilakukan di SDK 2  X  Program MATIUS Bandung, karena mereka 
merasa memiliki tanggung jawab dan menunjukan loyalitas mereka kepada 
organisasi tempat mereka bekerja. 
Meyer & Allen (1997) menambahkan, bahwa setiap individu memiliki 
derajat aspek komitmen yang bervariasi. Setiap aspek komitmen yang dimiliki 
seseorang, berkembang sebagai hasil dari pengalaman-pengalaman yang berbeda 
serta memiliki  implikasi berbeda pula pada tingkah laku dalam bekerja. Sebagai 
contoh, ada individu yang memiliki kelekatan perasaan terhadap organisasi 
(affective), juga memiliki kewajiban untuk bertahan dalam organisasi (normative). 
Di samping itu pula, individu lain mungkin kurang senang pada pekerjaannya 
dalam organisasi (affective), namun menyadari bahwa jika meninggalkan 
organisasi akan emberikan kerugian finansial dan kerugian lain (continuance). 
Individu lainnya memiliki kemauan, kebutuhan, dan kewajiban untuk 
bertahan dalam organisasi, namun memiliki derajat berbeda-beda, dengan adanya 
derajat aspek komitmen yang bervariasi ini, maka dapat diketahui profil komitmen 
organisasi yang dimiliki oleh individu terhadap organisasinya, melihat dari derajat 
yang didapat setiap individu untuk setiap aspek komitmennya. Setiap guru akan 
menampilkan sikap dan perilaku yang berbeda-beda sesuai dengan profil 
komitmen yang mereka miliki terhadap organisasi. Terdapat delapan profil 
komitmen berdasarkan perpaduan derajat setiap aspek dalam komitmen 
organisasi. Guru dengan affective commitment, continuance commitment, dan 
normative commitment yang tinggi akan dapat bertahan di organisasi karena 
merasakan ikatan emosional dengan organisasi, merasa mendapatkan keuntungan 
dari organisasi, dan juga merasa bertanggung jawab atas kemajuan organisasi. 
Guru dengan affective commitment dan continuance commitment yang tinggi, 
tetapi normative commitment rendah akan bertahan di organisasi apabila ada 
ikatan emosional dengan organisasi dan ia mendapatkan timbal balik yang sesuai 
dari organisasinya, walaupun ia tidak merasa bertanggung jawab dengan 
kemajuan organisasi. Guru dengan affective commitment dan normative 
commitment yang tinggi, tetapi continuance commitment rendah akan bertahan di 
organisasi apabila ada ikatan emosional dengan organisasi dan juga merasa 
bertanggung jawab atas kemajuan organisasi, walaupun ia tidak mendapatkan 
keuntungan secara finansial dari organisasi. Guru dengan continuance 
commitment dan normative commitment yang tinggi, tetapi affective commitment 
rendah akan bertahan di organisasi apabila ia mendapatkan timbal balik yang 
sesuai dari organisasi dan ia juga bertahan karena merasakan tanggung jawab atas 
kemajuan organisasi, walaupun ia merasa tidak nyaman dengan lingkungan kerja 
dan relasi dengan rekan kerja kurang baik. Guru dengan affective commitment 
yang tinggi, tetapi continuance commitment dan normative commitment rendah 
akan bertahan di organisasi karena merasakan ikatan emosional yang erat dengan 
lingkungan kerjanya, walaupun ia tidak banyak ikut bertanggung jawab dengan 
kemajuan perusahaan, dan ia juga tidak mendapatkan keuntungan secara finansial. 
Guru dengan continuance commitment yang tinggi, tetapi affective commitment 
dan normative commitment rendah akan bertahan di organisasi apabila guru 
tersebut merasakan mendapatkan keuntungan lebih dibandingkan dengan di 
tempat lain, walaupun ia merasa tidak nyaman dengan pekerjaannya dan merasa 
tidak memiliki tanggung jawab terhadap kemajuan dari organisasinya. Guru 
dengan normative commitment yang tinggi, tetapi continuance commitment dan 
affective commitment rendah akan bertahan di organisasi apabila guru tersebut 
merasakan adanya tanggung jawab untuk bertahan dan memajukan sekolah tempat 
ia bekerja, tanpa melihat keuntungan finansial yang dia dapat dan tidak 
mementingkan ikatan emosional antara rekan kerja di dalam organisasinya. Guru 
dengan affective commitment, continuance commitment, dan normative 
commitment yang rendah akan sulit bertahan di suatu organisasi, dan akan sering 
berpindah-pindah kerja. 
Terdapat faktor-faktor yang dapat mempengaruhi derajat dari setiap aspek 
dalam komitmen organisasi (Meyer&Allen,1997) diantaranya adalah karakteristik 
individu (usia, lama kerja, jenis kelamin, tingkat pendidikan, dan status 
perkawinan), karakteristik pekerjaan (job design, variasi, tantangan tugas), dan 
pengalaman kerja (fasilitas, imbalan). Karakteristik individu adalah usia, masa 
kerja, tingkat pendidikan, jenis kelamin, status perkawinan. Usia menunjukan 
catatan biografis lamanya masa hidup seseorang.  
Guru tetap di SDK 2  X  Program MATIUS Bandung terbagi kedalam 
dua tahap perkembangan yaitu, tahap perkembangan dewasa awal dan tahap 
perkembangan dewasa madya. Semakin dewasa usia seorang guru maka 
pertimbangan secara kognitif dalam memutuskan untuk bekerja di suatu 
organisasi akan berbeda dibandingkan guru yang usianya lebih muda. Guru yang 
berada pada tahap perkembangan dewasa madya akan memutuskan bekerja di 
bidang yang mereka kuasai karena masa pengembangan diri telah mereka lalui. 
Sedangkan guru yang berada pada tahap perkembangan dewasa awal lebih 
menginginkan tantangan dalam pekerjaannya. Karena usia dan affective 
commitment memiliki hubungan positif Makin tua usia seorang guru mereka 
semakin banyak mendapatkan pengalaman positif di tempat kerjanya sehingga 
membuat hal ini mempengaruhi affective commitment menjadi lebih tinggi 
dibandingkan guru-guru dengan usia lebih muda (Mathieu dan Zajac, dalam 
Meyer & Allen, 1997).  
 Lama kerja merupakan lamanya seseorang bekerja atau menjabat suatu 
posisi di dalam organisasi. Di SDK 2  X  Program MATIUS Bandung Guru tetap 
terbagi dalam dua masa kerja, yaitu masa kerja diatas 10 tahun dan masa kerja 
dibawah 10 tahun. Umumnya orang-orang berusia lebih tua dan telah lama 
bekerja memiliki affective commitment yang tinggi dibandingkan dengan mereka 
yang berusia muda, hal ini dipengaruhi oleh pandangan bahwa masa hidup mereka 
baik kehidupan biologis maupun usia kerja di organisasi hanya tinggal sesaat 
(Meyer & Allen, 1997). March & Simon (1958) mengatakan bahwa dengan 
meningkatnya usia dan masa kerja, kesempatan individu untuk mendapatkan 
pekerjaan lain menjadi lebih terbatas. Kesempatan kerja yang semakin sedikit ini 
juga memiliki hubungan positif dengan affective commitment seseorang. Maka 
dapat dikatakan terdapat hubungan positif antara usia, lama kerja, status 
perkawinan, dengan affective commitment (Mathieu dan Zajac, dalam Meyer & 
Allen, 1997), sedangkan dengan faktor yang lain yaitu pengalaman kerja, 
berdasarkan penelitian Mathieu dan Zajac (Meyer & Allen, 1997) ditemukan juga 
bahwa terdapat hubungan positif antara pengalaman kerja dengan affective 
commitment, karena semakin banyak pengalaman positif seseorang di pekerjaan 
dapat meningkatkan affective commitment seseorang.  
Berkaitan dengan jenis kelamin, wanita lebih banyak bekerja sebagai 
karyawan level rendah dengan status dan gaji yang rendah dibandingkan laki-laki, 
sehingga wanita cenderung menunjukan continuance commitment lebih lemah 
(Aven, Parker, & McEvoy; Meyer & Allen, 1997). Status perkawinan berkaitan 
dengan tanggung jawab untuk mencukupi kebutuhan hidup pasangan dan anak-
anak, sehingga guru yang telah menikah menunjukan continuance commitment 
yang lebih tinggi. Tingkat pendidikan yang tinggi memberikan peluang yang lebih 
besar untuk mencari pekerjaan yang lebih baik, sehingga guru yang memiliki 
tingkat pendidikan yang tinggi cenderung menunjukan continuance commitment 
yang rendah terhadap organisasi (Meyer & Allen, 1997). Tingkat pendidikan (Lee, 
dalam Meyer & Allen, 1997), usia dan lama kerja (Ferris & Aranya, dalam Meyer 
& Allen, 1997) berpengaruh terhadap continuance commitment. Semakin tinggi 
pendidikan maka akan semakin tinggi continuance commitment, dan semakin tua 
usia dan lama kerja seorang guru, maka continuance commitment semakin rendah 
karena kesempatan seorang guru untuk berpindah organisasi makin kecil. 
Meyer & Allen (1997), juga menemukan bahwa kepuasan kerja 
berhubungan negatif dengan continuance commitment, semakin tinggi kepuasan 
kerja, maka continuance commitment akan semakin randah. Karena kepuasan 
kerja didapat dari kesenangan seseorang terhadap pekerjaannya, walaupun 
keuntungan secara finansial yang didapat kurang memuaskan. Pengalaman kerja 
yang menyenangkan dan kepuasan kerja memiliki korelasi positif dengan 
normative commitment. Semakin tinggi kepuasan kerja seorang guru  dalam 
pekerjaannya maka akan semakin tinggi pula normative commitment orang 
tersebut.  
Karakteristik pekerjaan adalah bagaimana tantangan yang ada dalam 
pekerjaan tersebut yaitu sejauh mana pekerjaannya membutuhkan kreatifitas, 
membutuhkan tanggung jawab (Dorstein & Matalon, dalam Meyer  Allen, 1997). 
Individu yang lebih tertantang dan menganggap pekerjaannya menarik akan 
memiliki normative commitment yang lebih tinggi. Sama halnya dengan semakin 
seorang guru merasa dihargai atau dibutuhkan oleh sekolah maka normative 
commitment seseorang akan semakin tinggi Ketidak jelasan peran atau kurangnya 
pengertian akan hak dan kewajiban juga dapat mengurangi normative commitment 
(Meyer & Allen, 1997). Selain itu, ada konflik peran bagi guru yang telah 
menikah, antara peran sebagai guru dan peran sebagai orang tua dari anak-anak 
mereka. Perbedaan antara tuntutan pekerjaan dengan tuntutan fisik, bagi guru 
yang sudah senior dengan segala keterbatasan fisik. Perbedaan harapan terhadap 
sekolah dan nilai-nilai pribadi juga dapat mengurangi normative commitment 
seseorang pada organisasinya (Meyer & Allen, 1997). 
Tentunya guru tetap di SDK 2  X  Program MATIUS Bandung memiliki 
beberapa macam karakteristik seperti usia, lama kerja, tingkat pendidikan, 
persepsi mengenai tugas dan pekerjaan, tingkat otonomi, tantangan tugas, 
kejelasan peran, dan hubungan dengan atasan maupun rekan kerja yang berbeda. 
Hal itulah yang nantinya dapat mempengaruhi komitmen organisasi guru tetap di 
SDK 2  X  Program MATIUS Bandung. 
Afe (R) Con (T) Nor (T) 
Afe (T) Con (T) Nor (R) 
Afe (T) Con (T) Nor (T) 
Afe (R) Con (T) Nor (R) 
Guru-guru tetap di 
Afe (R) Con (R) Nor (T) 
Afe (R) Con (R) Nor (R) 
Afe (T) Con (R) Nor (T) 
Afe (T) Con (R) Nor (R) 
Faktor-faktor yang berpengaruh pada komitman organisasi : 
1. Karakteristik individu (usia, lama bekerja, jenis 
kelamin,tingkat pendidikan, status perkawinan) 
2. Karakteristik pekerjaan (job design, variasi, tantangan 
kerja). 
3. Pengamalaman kerja (fasilitas, imbalan) 
1. Profil komitmen guru tetap di SDK 2  X  Program MATIUS Bandung 
terhadap organisasi merupakan keterikatan guru tetap di SDK 2  X  
Program MATIUS Bandung terhadap organisasi . 

