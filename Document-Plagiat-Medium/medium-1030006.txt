Gereja berasal dari  bahasa Yunani yaitu ekklesia. Ek artinya keluar, 
sedangkan klesia berasal dari kata kaleo yang berarti memanggil. Diterjemahkan 
sebagai kumpulan orang yang dipanggil keluar dari dunia. Definisi ini 
mengandung beberapa arti,  yang pertama ialah umat atau lebih tepat persekutuan 
orang Kristen. Pengertian ini merupakan pengertian yang diterima orang Kristen 
mula-mula sebagai arti gereja. Berdasarkan pemaparan di atas dapat disimpulkan 
gereja pada awalnya  bukanlah sebuah gedung. 
Arti kedua adalah sebuah perhimpunan atau 
pertemuan ibadah umat Kristen. Bisa bertempat di rumah kediaman, lapangan, 
ruangan di hotel, maupun tempat rekreasi. Arti ketiga ialah mazhab (aliran) 
atau denominasi dalam agama Kristen. Gereja Katolik, Gereja Protestan, dll. Arti 
keempat ialah lembaga (administratif) daripada sebuah mazhab Kristen. Contoh 
kalimat  Gereja menentang perang Irak . Arti terakhir mengacu pada sebuah 
rumah ibadah umat Kristen secara umum, di mana umat bisa berdoa dan 
menyembah Tuhan. (http://bible-truth.org/Ekklesia.html) 
 Gereja  X  merupakan salah satu gereja injili di Kota Bandung. Gereja  X  
merupakan gereja yang berlatar belakang Tionghoa yang berada dalam klasis 
Priangan Sinode Jawa Barat. Youth gereja  X   merupakan komunitas anak muda 
Gereja  X  yang berasal dari jenjang sma, kuliah, dan awal kerja. Youth gereja  X  
memiliki visi memfasilitasi anak muda di Kota Bandung untuk menyembah Allah 
dan membangun komunitas orang percaya yang bertumbuh maksimal dalam 
segala bidang, serta giat bersaksi dan menjangkau generasinya bagi Allah. Pada 
hari minggu jemaat yang beribadah kurang lebih sebanyak 350 orang.  
 Setiap jemaat memiliki komunitas sel (komsel) sebagai tempat untuk 
bertumbuh, membahas Firman Tuhan dan sharing mengenai kehidupan mereka. 
Komsel ini dilakukan setiap dua minggu sekali setelah kebaktian. Pada umumnya 
setiap komsel beranggotakan 5-7 orang dan memiliki seorang pemimpin komsel 
(PKS) yang bertugas membawakan bahan dan melihat bagaimana kondisi 
kerohanian setiap anak-anak yang dibimbingnya. Namun peran ini belum 
dijalankan oleh PKS dengan maksimal. Komsel yang berjalan baru di kisaran 50% 
dari semua komsel yang ada. Penyebabnya beraneka ragam, ada yang disebabkan 
oleh karena PKSnya dan ada juga yang disebabkan oleh anggota komselnya. 
Masing-masing dari PKS tersebut juga akan menjadi anggota komsel yang 
dibawakan oleh seorang mentor. Komsel ini diadakan secara rutin seminggu 
sekali pada hari Senin. Mentor pada umumnya berada di usia 30 tahun  ke atas. 
Mentor berfungsi untuk membawakan bahan, membimbing, dan juga mendidik 
setiap anggota komselnya agar dapat bertumbuh di dalam Tuhan. Dengan ini 
setiap PKS juga memiliki seorang mentor di mana dia dapat menceritakan 
mengenai kesulitannya saat membawakan komsel, pergumulan, dan juga 
kehidupan pribadinya kepada seorang mentor. Ketika PKSnya memiliki suatu 
permasalahan, mentor akan memberikan konseling sederhana, membimbing dan 
juga mendoakan PKSnya tersebut.  
Selain bertugas membawakan bahan komsel bagi PKS untuk PKS teruskan 
bagi anak-anak komselnya di minggu, mentor juga ditempatkan di setiap bidang 
untuk mengarahkan pengurus youth gereja  X  di bidang tersebut. Mentor yang 
ditempatkan dalam divisi tersebut akan mengarahkan, membimbing dan juga 
berdiskusi dengan pengurus untuk perencanaan program bidang selanjutnya. 
Selain itu ada program perencanaan gereja untuk lima tahun mendatang di mana 
akan ada perwakilan dari mentor utama untuk membuat perencaan gereja secara 
umum. Kemudian semua mentor beserta pengurus akan bertemu untuk 
membicarakan dan menyusun tujuan spesifik yang ingin dicapai youth gereja  X  
secara keseluruhan untuk setahun mendatang. Pada akhir tahun akan diadakan 
evaluasi mengenai program yang diberikan oleh mentor dan juga pengurus. Lewat 
evaluasi tersebut diharapkan mendapatkan suatu pembelajaran baru dan juga 
untuk membantu dalam membuat perencanaan selanjutnya. 
Ketika membawakan komsel, tidak jarang seorang mentor mengalami 
situasi dilema. Pada umumnya setiap komsel pernah mengalami konflik antar 
anggotanya. Ada konflik yang dapat dibereskan sendiri oleh antar anggotanya, 
tetapi ada juga konflik yang semakin parah. Perselisihan ini pernah membuat 
salah seorang anggota komselnya meninggalkan gereja. Mentor sebagai pemimpin 
tersebut seharusnya dapat menyelesaikannya.  
Mentor perlu mengambil keputusan untuk mempertemukan kedua belah 
pihak yang berselisih sehingga dapat diselesaikan dengan baik, tetapi pada 
prosesnya hal ini tidak berjalan dengan lancar. Pada akhirnya salah satu dari pihak 
yang berselisih memutuskan untuk meninggalkan gereja. Mentor perlu 
melaksanakan tugas dan tanggung jawabnya untuk dapat menyelesaikannya, 
namun, hal ini tidak mampu dilakukan oleh seorang mentor. Oleh karena itu 
dibutuhkan kebijaksanaan (wisdom) dalam menyelesaikanyya.  
Kebijaksanaan (wisdom) merupakan keahlian dalam sistem pengetahuan 
yang mumpuni dalam kehidupan mendasar, seperti perencanaan hidup dan 
meninjau kembali kehidupan. Hal tersebut membutuhkan pengetahuan umum 
yang kaya mengenai persoalan kehidupan, pengetahuan praktis yang kaya 
mengenai masalah dalam kehidupan, pemahaman mengenai perbedaan antara 
konteks kehidupan dan nilai atau prioritas, dan pengetahuan mengenai hal-hal 
yang tak terduga mengenai hidup ini. (Baltes dan Smith 1994).  Konsep dari 
pragmatik mendasar diartikan sebagai pengetahuan dan penilaian mengenai esensi 
manusia dalam membuat perencanaan, pengelolaan, serta memiliki pemahaman 
mengenai kehidupan (P. Baltes & Smith, 1990; Baltes & Staudinger, 1993). 
Perencanaan merupakan salah satu hal yang mendasar untuk dapat melihat 
wisdom yang dimiliki seseorang. 
Wisdom peduli dengan keahlian (mastery) pada dialek mendasar eksistensi 
manusia. Seperti dialektik antara yang baik dengan yang buruk, positif dan negatif, 
kendali dan kurangnya kendali, dependent dan independent, kekuatan dan 
kelemahan, egois dan altruisme. Keahlian (mastery) bukan berarti membuat 
keputusan hanya dari satu sisi melainkan mempertimbangkan keduanya dalam 
mengambil keputusan. Dengan memiliki wisdom individu dapat mengetahui pada 
keadaan seperti apa seseorang perlu fokus pada salah satu kutub yang berlawanan 
(Staudinger 1999b dalam Baltes2000). Berdasarkan tujuan di atas, dibuatlah 
langkah-langkah perencanaan dengan memperhatikan dialektik yang ada di 
lapangan agar tujuan awal dapat terlaksana. 
Perencanaan yang baik tidak hanya memakai logika tapi lebih dari itu 
dibutuhkan wisdom untuk membuat rencana dan mengambil keputusan. Sehingga 
seorang mentor juga memerlukan wisdom dalam membuat perencanaan untuk 
program satu tahun ke depan. Wisdom juga diperlukan ketika seorang mentor 
membawakan komsel PKS. Komsel pada umumnya dibuka dengan doa kemudian 
membagikan bahan. Setelah bahan dibagikan akan ada sharing baik dari 
kehidupan mentor berkaitan dengan bahan tersebut, kemudian menanyakan 
bagaimana kondisi PKS berkenaan dengan bahan tersebut. Pada bagian akhir PKS 
dapat bercerita mengenai masalah pribadi yang mereka alami. Sehingga ketika 
PKS bercerita mengenai permasalahan mereka pada umumnya mentor perlu untuk 
memberikan masukan kepada PKS tersebut. Wisdom sering termanifestasi dalam 
situasi sosial seperti ketika memberikan saran dan juga pengarahan (guidance) 
(Montgemery et al 2002 dalam Sternberg 2005). Berdasarkan pernyataan tersebut 
artinya wisdom yang dimiliki mentor dapat termanifestasi ketika memberikan 
saran dan pengarahan terhadap masalah yang dialami oleh anggotanya. 
Tidak jarang bahkan masalah-masalah dilemma yang diperhadapkan 
sehingga wisdom merupakan hal yang esensial dalam menghadapinya. Dilemma 
merupakan situasi di mana pilihan yang sulit harus diambil di antara dua atau 
lebih alternatif, terutama yang sama-sama tidak diinginkan (Oxford English 
Dictionary). Sebagai contoh salah seorang anggotanya bercerita mengenai 
keadaan keluarganya di mana ayahnya kurang bertanggung jawab terhadap 
keluarga  dan mau meninggalkan rumah. Anggota tersebut telah bekerja sehingga 
tidak terlalu masalah baginya karena dia dapat menghidupi ibu dan adiknya. 
Namun ia kebingungan dengan situasi tersebut dan tidak tahu harus berbuat apa. 
Di satu sisi ia sangat kesal terhadap ayahnya, di sisi lain ia ingin agar ayahnya 
dapat tetap bersama keluarganya dan ia juga tahu bahwa ia harus mengampuni 
ayahnya. Situasi ini menjadi konflik baginya.  
Pada situasi seperti ini mentor perlu melihat situasinya secara mendalam, 
dapat berempati dan juga mengerti cara pandang orang tersebut, kemudian 
barulah memberikan saran yang komprehensif dengan penyampaian yang tepat. 
Beberapa mentor juga pada umumnya menghadapi situasi di mana salah seorang 
PKSnya mulai undur dari gereja dan mulai tidak rutin membawakan komsel ke 
area jemaat. Seorang mentor memerlukan wisdom untuk dapat berkata-kata secara 
tepat kepada PKSnya tersebut, menggali permasalahan apa yang dihadapinya, 
tindakan apa yang harus diambil berdasarkan masalahnya PKS tersebut. Apakah 
PKS tersebut dapat diberikan pengertian sehingga dapat kembali aktif, atau ada 
situasi yang berada di luar dirinya yang tidak memungkinkan PKS tersebut 
membawakan komsel kembali. Kemudian keputusan apa yang harus dibuat, 
apakah PKS tersebut harus dipertahankan atau diganti. Bagaimana dengan 
hubungan yang selama ini telah dibangun oleh PKS dan anggotanya, apakah ada 
pengganti yang cocok yang dapat menggantikan. Sehingga banyak hal yang 
menjadi pertimbangan dan situasi ini juga dapat menjadi dilemma bagi seorang 
mentor. 
Bahkan di area jemaat jika ada permasalahan berat atau dilemma yang 
tidak dapat di tangani oleh PKSnya maka mentor akan turun tangan untuk 
membantu menyelesaikannya dan juga memberikan konseling sederhana.  
Berdasarkan pemaparan di atas, wisdom merupakan hal mendasar dan esensial 
yang dibutuhkan oleh seorang mentor. Dikarenakan tugas dan tanggung jawabnya 
selalu bersentuhan dengan situasi yang menuntut wisdom 
Peneliti melakukan survey awal terhadap seorang mentor yang menjadi 
ketua bagi youth gereja  X  yang berusia 53 tahun. Subjek (B) selama menjadi 
mentor cukup banyak mengalami permasalahan dan tanggung jawab yang harus 
dijalankan. Masalah yang paling umum yang selalu ada adalah perbedaan 
pendapat menurut B. Perbedaan pendapat adalah hal yang wajar. Namun ketika 
berbeda pendapat, B selalu melihat apakah pendapatnya itu suatu kebenaran atau 
sebenarnya hanya pendapat yang jika diterima untuk memuaskan diri B saja. 
Dalam membuat perencanaan program youth gereja  X  ke depannya , bukan hal 
yang jarang B juga tidak setuju dengan pendapat rekannya. Saat itu terjadi, B akan 
berusaha untuk mengidentifikasi ketidaksetujuannya dan bertanya kepada diri 
sendiri mengapa sebenarnya tidak setuju. Setelah mengetahui apa yang 
melatarbelakanginya terkadang ada waktu di mana B juga harus rela melepaskan 
pendapatnya dan dengan rendah hati juga menerima pendapat yang lain. 
Salah satu hal yang paling berat yang pernah dialami B adalah masa 
transisi di mana terbentuknya youth gereja  X  dengan tempat ibadah baru dan 
fasilitas yang cukup besar yang diterimanya. Sehingga banyak dari berbagai pihak 
yang memberikan pendapat dan kritik terhadap youth gereja  X . B merasa kritik 
bukan sesuatu hal yang mengenakan, namun hal ini adalah hal yang diperlukan 
untuk kemajuan youth gereja  X  dan juga diri B ke depannya. Dalam merespon 
terhadap kritik, ada kritik yang B dapat terima, namun ada juga kritik yang B sulit 
untuk terima. Untuk kritik yang B sulit untuk terima, B selalu berusaha untuk 
merefleksikannya. Apakah B tidak dapat menerima karena ini merupakan kritik 
yang salah atau karena identifikasi B yang kuat pada youth gereja  X . Hal ini 
berakibat ketika youth gereja  X  dikritik, B juga merasa dirinya dikritik. Namun, 
di masa sulit itulah B belajar rendah hati menerima semua kritikan yang ada dan 
ternyata kritik itu merupakan hal yang diperlukan untuk youth gereja  X  ke 
depannya. 
 B menginginkan untuk ke depannya youth gereja  X  tetap dilayani secara 
multigenerasi. Multigenerasi yang dimaksudkan adalah berjenjang. Menurut B 
tidak mungkin youth dipimpin oleh sesama youth, melainkan tetap harus ada 
peran seorang yang lebih tua mau mementoring dan membimbing youth sendiri. 
Sehingga B menerapkan dan meminta beberapa belas orang mentor yang terbeban 
untuk melayani youth. Dengan diterapkannya pola ini, mentoring dan kaderisasi di 
dalam youth dapat tetap berjalan. 
 B juga memiliki keinginan untuk mengembangkan jemaat awam. Di masa 
awal remaja, seorang remaja masih belum memiliki identitas yang jelas dan belum 
mengetahui siapa dirinya sendiri. Pada saat itu remaja tersebut dibina dengan 
keteladanan secara berjenjang ( multigenerasi) sehingga dapat menjadi seorang 
yang mengenal dirinya sendiri, dapat menerima dirinya, dan juga pada akhirnya 
dapat menjadi seorang pemimpin. Untuk sampai pada tahapan tersebut, B 
menekankan tetap paling penting adalah mengenal Tuhan. Setelah mengenal 
Tuhan, barulah seorang remaja dapat mengenal dirinya sendiri, membangun nilai-
nilai dan prinsip hidup yang benar di hadapan Tuhan. 
 Pada awalnya B menemukan konsep multigenerasi pada saat tahun 2000 
masuk ke dalam komisi remaja ( belum berganti nama menjadi YOUTH GEREJA 
 X ). B menemukan kesulitan ketika seorang anak SMA memimpin anak SMA 
lainnya. Ketika diberikan tugas banyak hal yang tidak mengerti. Sehingga B 
berpikir hal ini tidak bisa dijalankan. Kemudian B berpikir yang menjadi pengurus 
butuh seorang mahasiswa. Sehingga B mulai membangun hubungan dengan para 
mahasiswa, pergi bersama, berkomunitas bersama, dan saling terbuka satu dengan 
lainnya.  
Setelah itu ditanamkanlah nilai-nilai dan tujuan kepada mahasiswa 
tersebut, kemudian pada akhirnya mengajak menjadi seorang pengurus. Ketika 
menjadi seorang pengurus, yang terpenting bagi B adalah pendampingan. 
Sehingga B mendampingi para pengurus, dan ketika mendampingi bukan hanya 
peduli terhadap pelayanan yang mereka lakukan, namun juga peduli secara 
keseluruhan terhadap studi mereka, keluarga mereka, dan juga iman mereka. 
Setiap tahunnya B selalu update dan tetap melihat kondisi youth sedang 
ada di mana, trendnya sedang bagaimana, dan apa tantangan terbesar mereka saat 
ini. B menjaga tetap update biasanya dengan cara membaca buku dan juga melihat 
hasil-hasil konfrensi di dunia mengenai youth. B mencontohkan youth di era 
postmodern dan era digital memiliki kebutuhan yang berbeda. Hal ini ikut serta 
mempengaruhi nilai-nilai yang dimiliki oleh youth. Sehingga dengan mengetahui 
nilai-nilai yang dimilikinya, apa yang B sampaikan dapat relevan dengan 
B dapat melakukan identifikasi terhadap dirinya mengenai persoalan yang 
terjadi di dalam hidupnya. B juga memiliki kemauan untuk belajar dan membaca 
buku secara rutin mengenai perkembangan youth. B juga memiliki pengalaman 
yang cukup dalam berhadapan dengan anak di usia youth karena telah memulai 
pelayanannya selama 15 tahun, namun tetap ada kasus perselisihan di antara 
jemaat yang B tidak dapat selesaikan dan berakhir dengan salah satu anggotanya 
meninggalkan gereja.  
Oleh karena itu peneliti tertarik untuk mengetahui gambaran wisdom pada 
mentor di youth gereja  X  .  
Penelitian ini dilakukan untuk mengetahui bagaimana gambaran Wisdom 
pada mentor di youth gereja  X  . 
Mengetahui Wisdom pada mentor di youth gereja  X  . 
Mengetahui wisdom pada mentor di youth gereja  X   yang dikaitkan 
dengan kriteria-kriteria wisdom. 
berhubungan dengan wisdom. 
penelitian selanjutnya mengenai wisdom  pada young adult dan older adult 
mereka mengetahui secara umum mengenai gambaran wisdom yang 
mereka miliki dan dapat menjadi masukan serta bahan evaluasi bagi 
mereka untuk lebih meningkatkan wisdom yang mereka miliki. 
responden dengan memperhatikan masukan mengenai kriteria-kriteria 
pada wisdom  yang perlu ditingkatkan. 
Youth gereja  X   adalah suatu komunitas gereja yang khusus melayani 
anak muda di kota Bandung. Youth gereja  X  memiliki visi  memfasilitasi anak 
muda di Kota Bandung untuk menyembah Allah dan membangun komunitas 
orang percaya yang bertumbuh maksimal dalam segala bidang, serta giat bersaksi 
dan menjangkau generasinya bagi Allah. Youth gereja  X  sendiri memiliki 
mentor-mentor dalam membimbing, mendidik, dan juga mengarahkan jemaatnya 
yang kebanyakan berada dalam tahap perkembangan adolescent. Mentor yang 
berada di Youth gereja  X  sendiri bertugas untuk menentukan tujuan apa yang 
ingin dicapai oleh Youth gereja  X  untuk ke depannya. Mereka juga aktif dalam 
mengajar jemaat melalui kelas pembinaan yang ada di gereja dan juga secara rutin 
membimbing pemimpin komsel (PKS) di mana PKS tersebut yang akan 
membimbing jemaat langsung secara kelompok, dan juga memberikan konseling 
langsung terhadap individu yang membutuhkan.  
 Ketika konseling tersebut dibutuhkan rich factual knowledge untuk 
menilai secara komprehensif dan juga holistik mengenai keadaan individu dan 
juga permasalahan itu sendiri. Mentor juga membutuhkan life-span contextualism 
dan value relativism untuk dapat melihat gambaran yang lebih luas mengenai 
konteks individu dan permasalahannya tanpa melibatkan nilai-nilai yang dimiliki 
oleh mentor tersebut. Terakhir dibutuhkan rich procedural knowledge dan 
management uncertainty untuk mengambil keputusan yang tepat dengan 
memperhitungkan berbagai kemungkinan yang dapat terjadi di masa depan  
Dalam hal ini wisdom dibutuhkan oleh seorang mentor dalam 
menjalankan peranan mereka seperti yang telah dipaparkan di atas. Paul Baltes 
mendefinisikan  Wisdom sebagai keahlian (expertise) dalam mengatasi 
permasalahan mendasar yang berkaitan dengan perilaku dan makna hidup. 
Individu yang memiliki wisdom dapat dilihat dari 5 kriteria yaitu pengetahuan 
umum yang kaya ( rich factual knowledge), pengetahuan praktis yang kaya ( rich 
procedural knowledge), memahami konteks rentang kehidupan manusia (lifespan 
contextualism), relativisme dari nilai dan prioritas kehidupan (relativism of values 
and life priorities), mengetahui dan mengelola ketidakpastian  (the recognition 
and management uncertainty) ( e.g., Baltes & Staudinger 2000). 
Rich factual knowledge mencakup pengetahuan umum mengenai kondisi 
manusia dan juga pengetahuan spesifik mengenai suatu kejadian yang spesifik 
(kelahiran, menikah, kematian) Mentor yang memiliki Rich factual knowledge 
akan memiliki pengetahuan umum yang kaya,  termasuk di dalamnya 
pengetahuan yang berhubungan dengan  kehidupan manusia seperti human nature, 
norma sosial, perkembangan sepanjang rentang kehidupan, dan juga relasi 
interpersonal. Di samping itu mentor memiliki suatu kedalaman, mampu 
menghubungkan pengetahuan detil yang sangat kaya sehingga membentuk pola 
informasi kompleks mengenai manusia dan kehidupan. 
Rich Procedural Knowledge adalah pengetahuan mengenai heuristic dan 
strategi untuk meninterpretasikan masalah kehidupan dengan  melihat masa lalu, 
masa kini, dan juga masa depan. Rich Procedural Knowledge merupakan strategi 
mentor dalam memimpin / melakukan (conduct) kehidupan dan juga memiliki 
makna hidup . Mentor yang memiliki Rich Procedural Knowledge akan mampu 
menganalisis dan belajar dari pengalaman masa lalu, serta memiliki pengetahuan 
untuk mengaplikasikannya dalam kehidupan. Mentor tersebut juga mempunyai 
pengetahuan yang mendalam mengenai kapan waktu yang tepat untuk 
memberikan saran dan bagaimana cara mengungkapkannya ketika seseorang 
berada situasi kehidupan yang sulit.  
Lifespan Contextualism merupakan kemampuan untuk melihat individu 
dan kejadian tidak terisolasi melainkan secara terelaborasi dengan konteks. 
Lifespan Contextualism merupakan bagaimana seorang mentor dalam melihat 
suatu permasalahan yang terkait dengan bidang-bidang kehidupan yang selama ini 
telah dijalani. Mentor dalam melihat suatu masalah memperhatikan konteks usia, 
biografi, dan budaya. Hal ini dilihat mentor secara menyeluruh dan terintegrasi, 
bukan secara terpisah satu dengan lainnya.  
Value Relativism/ tolerance adalah implikasi pengetahuan bahwa di dalam 
lingkungan ada nilai dan tujuan yang berbeda antara satu dengan yang lainnya. 
Value Relativism/ tolerance merupakan bagaimana mentor dapat melihat bahwa 
setiap individu itu berbeda antara yang satu dengan yang lainnya. Mentor dapat 
mengakui dan mentoleransi bahwa setiap individu memiliki nilai yang berbeda 
satu dengan lainnya. Tidak berhenti di sana namun, mentor juga mengarahkan 
individu untuk mencapai suatu optimalisasi dan keseimbangan untuk kebaikan 
Awareness/management of uncertainty merupakan pengetahuan bahwa 
kehidupan tidak dapat selalu diprediksi serta keputusan hidup, interpretasi 
kehidupan, dan juga perencanaan hidup tidak pernah lepas dari ketidakpastian. 
Awareness/management of uncertainty adalah seorang mentor menyadari bahwa 
dirinya terbatas dan tidak mengetahui semuanya. Dia mengetahui bahwa dia tidak 
dapat selalu menentukan pilihan yang terbaik untuk masa sekarang, memprediksi 
masa depan dengan sempurna, atau dengan yakin 100% mengapa suatu kejadian 
di masa lalu terjadi seperti yang dilakukan ketika itu. Tidak berhenti sampai di 
sana, namun mentor juga setelah mengetahui adanya ketidakpastian dalam 
kehidupan ini dapat berhasil mengelola ketidakpastian yang terjadi dalam hidup 
ini. 
Ada 3 jenis faktor yang berpengaruh dalam perkembangan wisdom-related 
knowledge yaitu Context-Related factor di mana hal ini berisi mengenai  usia, 
interaksi sosial, pendidikan, dan budaya / agama yang dianut oleh individu. 
Kebanyakan orang percaya bahwa orang yang bijaksana biasanya berusia lanjut 
(Clayton & Birren 1980, Orwoll & Perlmutter 1990 dalam Sternberg 2005). 
Faktanya kebanyakan orang yang dinominasi orang yang bijaksana setidaknya 
memiliki usia 60 tahun (Baltes et al. 1995, Denney et al.1995, Jason et al. 2001, 
Maercker et al. 1998,Orwoll & Perlmutter 1990 dalam Staudinger 2010). Di sisi 
lain kebanyakan orang sadar, tidak semua orang mengembangkan wisdom pada 
usia tua mereka, dan orang muda juga dapat cukup bijaksana. Asosiasi antara 
wisdom dan usia di bawa oleh ide bahwa orang tersebut telah memiliki 
pengalaman naik dan turun dalam kehidupan manusia. (e.g., Clayton& Birren 
1980, Gl uck & Bluck 2010, Holliday & Chandler 1986, Sternberg 1985 dalam 
Sternberg 2005). Mentor yang lebih tua dipercaya telah lebih banyak mengalami 
banyak hal dalam kehidupan mereka sehingga dapat dianggap mentor yang lebih 
Mentor yang aktif dalam berinteraksi sosial lebih bijaksana. Hal ini 
dikarenakan  wisdom merupakan konstruk yang berkembang secara sosial 
sehingga perkembangan kebijaksanaan dalam kehidupan individu berlangsung 
lebih lancar ketika sering berdiskusi dengan orang lain (Sternberg & Jordan, 
2005). Jika mentor sering berdiskusi dengan orang lain, mereka dapat memperluas 
perspektifnya dalam memandang suatu masalah sehingga dapat memiliki 
pemahaman yang lebih komprehensif dan mampu membuat suatu keputusan 
dengan lebih bijaksana. 
Selain dari usia, pendidikan juga dipandang memainkan peranan yang 
penting. Seorang mentor yang memiliki pendidikan yang tinggi dipercaya lebih 
cerdas dan juga bijaksana. Budaya dan agama juga berpengaruh dalam 
pembentukan wisdom seseorang. Seseorang yang diasosiasikan memiliki wisdom 
memiliki nilai dan idealisme pada budayanya. Sebagai contoh orang Budha 
percaya tingkat yang lebih tinggi dari wisdom dapat dicapai melalui usaha sadar, 
di mana kebanyakan orang Kristen tidak (Rappersberger 20007). Takashi dan 
Overton membedakan wisdom berdasarkan budaya barat dan budaya timur. Pada 
budaya barat orang yang memiliki wisdom adalah orang yang memiliki 
pengetahuan, cognitive complexity, dan dapat melakukan sinthesis sedangkan 
budaya timur fokus pada integrasi antara kognitif dan afek (Sternberg 2005). 
Berikutnya adalah Expertise-Related factor di mana faktor ini mengenai 
mentor/ role model,  terus berlatih, pengalaman hidup, dan juga pelatihan 
profesional. Mentor dalam hal ini berpengaruh jika seorang memiliki mentor yang 
bijak akan memperngaruhi wisdom seseorang, sebaliknya jika individu tersebut 
terus berlatih dan melakukan mentoring terhadap individu lainnya akan 
mempengaruhi dalam perkembangan wisdom yang dimilikinya. 
Pengalaman hidup memainkan peranan yang penting dalam 
memperngaruhi wisdom. Pengalaman hidup pada situasi kritis selain membentuk 
seseorang dalam situasi tersebut juga dapat dipandang sebagai waktu untuk 
seseorang berlatih dan mengaplikasikan pengetahuan yang dimilikinya secara 
mendalam. Mentor yang pernah memiliki pengalaman hidup dalam situasi kritis 
akan lebih bijaksana dibandingkan yang tidak.   
Terakhir adalah Person-Related factor di mana berisi mengenai inteligensi 
trait kepribadian, emotional competence serta motivasi. Inteligensi terkait dengan 
wisdom dibedakan menjadi dua yaitu crystallized intelligent dan fluid ability. 
Crystallized intelligent diukur melalui kemampuan verbal sebagai indikatornya 
Mentor yang memiliki tingkat inteligensi yang tinggi akan mendukung 
perkembangan wisdom yang dimilikinya.  
Trait kepribadian dari Big Five Personality (Extraversion, Agreeableness, 
Conscientiousness, Neuroticism dan Openness to Experience) memiliki hubungan 
dengan wisdom individu. Faktor kepribadian adalah suatu predisposisi bawaan 
yang melekat pada diri individu sehingga akan berpengaruh pada bagaimana 
individu bereaksi dan menanggapi lingkungan serta pengalamannya. Mentor yang 
extraversion yang cenderung didominasi oleh perasaan positif, energik dan 
memiliki dorongan untuk menjalin relasi dengan orang-orang di sekitarnya. 
Openness to experience (sejalan dengan extraversion) muncul sebagai prediktor 
yang kuat dalam pengaruhnya terhadap wisdom. 
Emotional Competence adalah kapasitas untuk memiliki self efficacy 
dalam emosi ,diperoleh dari transaksi sosial (Saarni 1990, 1999). Termasuk ke 
dalamnya sadar akan emosi orang lain, dapat mengekspresikan emosi, empati, 
regulasi emosi, dan beradaptasi serta mengkomunikasikan emosi ketika berrelasi 
dengan orang lain. Emotional competence mencirikan orang yang matang dan 
juga stabil dalam berrelasi dengan orang lain. Seseorang yang memiliki emotional 
competence akan mempengaruhi terhadap wisdom dan membantunya dalam 
mendengar permasalahan orang lain, memberikan saran dan juga menyampaikan 
pendapatnya dengan baik. 
Ketiga tipe faktor ini berpengaruh dalam perkembangan wisdom-related 
knowledge dikarenakan ketiga faktor ini menentukan cara seseorang mengalami 
dunianya, membuat perencanaan, mengelola, memikirkan ulang kehidupan 
mereka (  context of developmental regulation ) 
Budaya/ agama 
1. Rich Factual knowledge 
2. Rich Procedural knowledge 
3. Lifespan contextualism 
4. Value  Realtivism/tolerance 
5. Awareness/management of 
Mentor / role model 
lima kriteria, yaitu rich factual knowledge, rich procedural 
knowledge,  lifespan contextualism , value relativism/ tolerance, dan 
awereness/ management of uncertainty 
dilatarbelakangi oleh life planning dan life review 
 Wisdom merupakan sistem pengetahuan yang mumpuni dalam bidang 
pragmatik kehidupan mendasar , seperti perencanaan hidup dan meninjau kembali 
kehidupan. Hal tersebut membutuhkan pengetahuan umum yang kaya mengenai 
persoalan kehidupan, pengetahuan praktis yang kaya mengenai masalah dalam 
kehidupan, pemahaman mengenai perbedaan antara konteks kehidupan dan nilai 
atau prioritas, dan pengetahuan mengenai hal-hal yang tak terduga mengenai 
hidup ini. (Baltes dan Smith 2000)  
Kelima kriteria ini 
bertujuan untuk memenuhi dua komponen dasar kebijaksanaan, yaitu intelek dan 
karakter (Sternberg & Jordan, 2005). Kelima kriteria ini dapat dibedakan menjadi 
2 kriteria dasar dan 3 lainnya adalah metakriteria 

