Penelitian ini adalah suatu penelitian deskriptif yang bertujuan untuk 
mengetahui perilaku adaptif siswa TK  X  Bandung. Responden dalam penelitian 
ini adalah siswa TK  X  Bandung. Pemilihan sampel menggunakan metode 
accidental sampling, dan sampel penelitian ini berjumlah 40 siswa. 
 Alat ukur yang digunakan adalah Skala Perilaku Adaptif Vineland edisi 
kelas (Vineland Adaptive Behavior: Classroom Edition (VCE)) dari Sparrow, S. 
S., Balla, D. A.,& Cicchetti, D.V (1984). yang terdiri atas 244 item. Data yang 
diperoleh diolah dengan menggunakan uji korelasi T-Pearson dengan program 
SPSS 17.0.Dari uji statistik diperoleh bahwa reliabilitas alat ukur sebesar 0,980 
serta validitas yang berkisar antara 0,3-0,8. 
 Berdasarkan pengolahan data secara statistik diketahui bahwa 80% siswa 
TK  X  memiliki perilaku adaptif tinggi serta 20% siswa TK  X  memiliki 
perilaku adaptif rendah.  
 Kesimpulan yang dapat diperoleh adalah sebagian besar siswa TK  X  
memiliki perilaku adaptif tinggi. Siswa yang perilaku adaptif tinggi, memiliki 
hasil yang tinggi pada aspek dan subaspek. Hanya sebagian kecil saja yang 
perilaku adaptif rendah, memiliki aspek dan subaspek yang tinggi, yaitu aspek 
motorik dan komunikasi (ekspresif dan ekspresif).  
 Peneliti mengajukan saran untuk meneliti lebih lanjut mengenai peran 
serta orangtua terhadap perilaku adaptif serta faktor-faktor yang mempengaruhi 
perilaku adaptif.  
Setiap anak mengalami proses pertumbuhan dan perkembangan dalam 
hidupnya. Perkembangan anak terjadi melalui beberapa tahapan dan setiap 
tahapan mempunyai ciri dan tuntutan tersendiri sehingga pengasuhan anak perlu 
disesuaikan dengan tahapan perkembangan tersebut. Pertumbuhan dan 
perkembangan yang paling signifikan terjadi ketika anak memasuki usia sekolah 
(prasekolah), karena pada masa ini anak akan mengembangkan keterampilan dan 
kemampuan yang telah dimilikinya dalam hal motorik, komunikasi, sosialisasi, 
keterampilan hidup.  
Namun pada jaman sekarang ini banyak taman kanak-kanak yang 
menerapkan calistung (baca, tulis, hitung) yang semestinya mereka terima pada 
saat sekolah dasar. Penerapan calistung pada saat taman kanak-kanak dikarenakan 
banyak sekolah dasar yang menuntut bahwa anak harus dapat membaca, menulis 
dan berhitung ketika masuk sekolah dasar. Sehingga banyak anak yang 
mengalami stress karena banyak tugas yang diberikan sekolah dan anak berada di 
sekolah dalam jangka waktu yang lama. Padahal menurut UU Sistem Pendidikan 
Nasional No. 20 tahun 2003, taman kanak-kanak termasuk dalam pendidikan anak 
usia dini yang menitikberatkan pembelajaran pada  nilai-nilai moral, nilai-nilai 
agama, kemandirian, emosional. Prasekolah atau Taman Kanak-Kanak 
merupakan suatu pendidikan yang bertujuan untuk mengembangkan keterampilan 
yang dimiliki anak-anak seperti kemampuan dalam komunikasi, motorik, 
keterampilan hidup sehari-hari, serta sosialisasi. Keterampilan yang telah 
dikembangkan sejak taman kanak-kanak diharapkan dapat bermanfaat saat anak 
menempuh jenjang pendidikan yang lebih tinggi dan dengan tuntutan yang lebih 
tinggi pula. Namun ada pula sekolah yang lebih mengembangkan keterampilan 
serta kemampuan anak daripada calistung, salah satunya adalah TK  X .  
TK  X  merupakan salah satu sekolah interaktif, yaitu sekolah yang 
memfokuskan pelajaran dan pengajaran pada siswa-siswinya sehingga sekolah 
cukup sering mengadakan kegiatan yang melibatkan siswa-siswinya seperti outing 
bersama dengan keluarga dan guru yang diadakan setiap jenjang waktu tertentu, 
mengadakan pentas seni atau perlombaan pada hari besar tertentu, mengadakan 
pargelaran drama di hari besar tertentu, mengikuti perlombaan-perlombaan, dan 
lain sebagainya. Diharapkan dengan banyaknya program kegiatan tersebut, TK 
 X  ini dapat mengembangkan keterampilan-keterampilan yang ada pada diri 
siswa. Misalnya ketika siswa melakukan kegiatan bersama dengan guru dan 
keluarga, siswa dapat mengembangkan kemampuan sosialisasi, komunikasi, 
motoriknya baik dengan orang dewasa ataupun dengan teman sebaya. Ketika 
siswa melakukan drama, maka akan diajarkan bagaimana cara berkomunikasi dan 
bersosialisasi dengan teman-teman yang lainnya. Selain itu melatih kemampuan 
motorik siswa karena dalam drama terdapat tarian-tarian atau gerakan-gerakan 
yang harus dilakukan, siswa juga diajarkan untuk dapat bertanggung jawab 
terhadap tugasnya.  
Dengan berkembangnya keterampilan tersebut maka berkembang pula 
keterampilan untuk melakukan aktivitas sehari-hari di rumah dan di kelas. 
Penampilan perilaku yang diperlukan untuk melakukan aktivitas sehari-hari di 
rumah dan di kelas disebut sebagai perilaku adaptif (Sparrow dkk, 1984). Perilaku 
adaptif siswa meliputi empat aspek, yaitu aspek komunikasi, sosialisasi, motorik, 
serta keterampilan hidup sehari-hari. Ketika siswa menulis dan membaca dengan 
tata cara yang benar, siswa mampu berbicara dengan orang lain dengan 
menggunakan kosakata yang benar maka dapat dikatakan bahwa siswa memiliki 
perilaku adaptif dalam komunikasi. Keterampilan hidup sehari-hari ditampilkan 
siswa dengan cara mampu mengenakan baju dan sepatu sendiri, mencuci tangan 
sendiri, sikat gigi dan cuci muka sendiri, menyisir rambut sendiri. siswa mau 
bermain dengan teman sebaya, berbagi mainan dengan temannya, mampu 
berempati pada orang lain, maka perilaku ini termasuk aspek sosialisasi. 
Sedangkan ketika siswa mewarnai dan menggambar, menggunting, bermain bola, 
itu termasuk dalam aspek motorik.  
Keempat aspek perilaku adaptif dapat terlihat dengan jelas ketika siswa 
berada di sekolah, karena perilaku siswa akan dibandingkan dengan perilaku 
siswa lain seusianya. Diharapkan siswa TK  X  telah memiliki keterampilan 
dalam keempat aspek perilaku adaptif tersebut. Namun berdasarkan hasil 
wawancara dengan kepala sekolah dan guru di TK  X , permasalahan yang 
banyak dialami adalah mengenai perilaku adaptif, seperti siswa tidak menjawab 
pertanyaan guru ketika sedang ditanya, tidak dapat mengikuti apa yang 
diperintahkan oleh guru, tidak mau berbagi mainan dengan temannya, tidak 
mengerjakan tugas sesuai dengan yang diperintahkan, mengaduk-aduk makanan, 
kurang dapat membereskan mainan.  
Berdasarkan hasil observasi terhadap 10 orang anak, diketahui bahwa 
terdapat enam orang siswa yang kurang dapat menulis dengan menggunakan 
huruf yang tepat, misalnya dalam menulis suatu kata terdapat huruf yang hilang, 
menulis huruf dengan bentuk huruf yang tidak tepat. Dua orang siswa tidak 
menjawab pertanyaan atau memberikan respon ketika guru bertanya, misalnya 
ketika siswa diberikan suatu pertanyaan, siswa tersebut tidak menjawabnya 
meskipun ia mengetahui jawaban pertanyaannya. Empat orang siswa kurang mau 
bersosialisasi dengan temannya dan kurang mau mengikuti kegiatan yang 
diadakan di sekolah atau kelas, misalnya ketika siswa sedang bermain maka ada 
siswa yang jarang ikut bermain dengan temannya, siswa tersebut lebih memilih 
bersama dengan guru atau hanya memperhatikan teman yang lain dan 
sekelilingnya. Contoh lainnya adalah ketika kelas sedang ada kegiatan bernyanyi 
atau menari bersama maka ada beberapa siswa yang kurang mengikuti kegiatan 
tersebut, kemudian terdapat beberapa siswa yang tidak meletakkan barang sesuai 
pada tempatnya ketika sudah menggunakannya. 
Dari hasil wawancara terhadap kepala sekolah, guru dan hasil observasi, 
diketahui bahwa ada beberapa anak yang perilaku adaptifnya belum berkembang 
dengan baik. Namun terdapat juga anak yang mampu menampilkan perilaku 
adaptif. Oleh karena itu peneliti ingin mengetahui perilaku adaptif TK  X  
Bandung. 
 Berdasarkan fakta yang telah diuraikan di atas maka peneliti tertarik untuk 
mengetahui gambaran perilaku adaptif  siswa TK  X  Bandung. 
Maksud penelitian ini adalah memperoleh gambaran mengenai perilaku 
adaptif siswa TK  X  Bandung. 
Tujuan penelitian adalah mengetahui gambaran mengenai aspek dan 
subaspek perilaku adaptif siswa TK  X  Bandung. 
perilaku adaptif siswa TK. 
 -  Sebagai bahan referensi bagi peneliti lain yang ingin mengetahui 
mengenai perilaku adaptif siswa TK. 
sehingga sekolah dapat mengembangkan kemampuan siswa agar lebih 
optimal. 
sehingga dapat bekerja sama dengan pihak sekolah untuk dapat 
meningkatkannya.  
Anak usia 4-6 tahun merupakan usia prasekolah, dan pada masa ini anak 
memasuki Taman Kanak-Kanak. Taman kanak-kanak merupakan jenjang 
pendidikan anak usia dini dalam bentuk pendidikan formal. Kurikulum TK, dalam 
hal ini TK  X  pun ditekankan pada pemberian rangsangan pendidikan untuk 
membantu pertumbuhan dan perkembangan jasmani dan rohani agar siswa 
memiliki kesiapan dalam memasuki jenjang pendidikan lebih lanjut.  
Semakin usia siswa bertambah maka diharapkan siswa TK  X  dapat 
mengembangkan keterampilan-keterampilan yang dimilikinya dalam aspek 
komunikasi, keterampilan hidup sehari-hari, sosialisasi, serta motorik. Dengan 
berkembangnya keterampilan keterampilan tersebut berarti berkembang pula 
perilaku mereka yang diperlukan untuk dapat memenuhi kebutuhannya sendiri 
dan tanggung jawabnya kepada masyarakat. Menurut Sparrow dkk (1984), 
tampilan perilaku anak dalam aktivitas sehari-hari yang diperlukan untuk bekal 
kemandirian pribadi dan tanggung jawab sosialnya disebut perilaku adaptif.  
Sparrow (1984) mengemukakan terdapat tiga prinsip dalam perilaku 
adaptif, pertama perilaku adaptif  TK  X  berkaitan dengan usia. Semakin usianya 
bertambah maka perilaku adaptifnya semakin meningkat dan kompleks. Kedua, 
perilaku adaptif dipengaruhi oleh harapan  lingkungan (guru dan orangtua). 
Perilaku adaptif yang ditampilkan oleh siswa akan dinilai oleh lingkungannya  
dan orang sekitarnya (dalam hal ini guru). Ketiga, perilaku adaptif merupakan 
suatu performance bukan ability. Perilaku adaptif tidak adekuat ketika siswa tidak 
menampilkan keterampilannya ke dalam kehidupan sehari-hari meskipun siswa 
mampu melakukannya.   
Sparrow (1984), mengemukakan bahwa perilaku adaptif memiliki empat  
aspek, yaitu komunikasi, keterampilan hidup sehari-hari, sosialisasi dan motorik. 
Demikian pula, diharapkan perilaku yang ditampilkan siswa TK  X  memiliki 
keempat aspek tersebut. Aspek pertama adalah komunikasi, aspek ini terbagi atas 
tiga subaspek yaitu reseptif, ekspresif, dan tulisan. Reseptif yakni bagaimana 
perilaku siswa TK  X  dalam memahami materi atau informasi yang disampaikan 
oleh guru. Ekspresif yaitu bagaimana perilaku siswa TK  X  dalam menyatakan 
pendapat atau pemikiran. Ketiga adalah tulisan, yaitu bagaimana siswa TK  X  
menampilkan kemampuannya dalam membaca bacaan dan menulis huruf. 
 Aspek kedua adalah keterampilan hidup sehari-hari yang terbagi atas tiga 
subaspek yaitu pribadi, kerumahtanggaan, kemasyarakatan. Subaspek pribadi 
adalah bagaimana perilaku siswa TK  X  ketika makan, berpakaian dan 
membersihkan diri. Kerumahtanggaan yakni bagaimana perilaku siswa TK  X  
ketika dihadapkan pada tugas-tugas rumah tangga seperti menyimpan dan 
membereskan mainan, menyapu dan mengelap. Kemasyarakatan yaitu bagaimana 
perilaku siswa TK  X  ketika menggunakan waktu, telepon dan ketrampilan 
akademis di kelas.  
Aspek ketiga adalah sosialisasi yang terbagi atas tiga subaspek yaitu 
hubungan antar manusia, bermain dan waktu luang, mengatasi situasi. Hubungan 
antar manusia yakni bagaimana siswa  TK  X  berinteraksi dengan orang lain. 
Bermain dan waktu luang yaitu bagaimana siswa TK  X  bermain dan 
menggunakan waktu. Mengatasi situasi yakni bagaimana siswa TK  X  
menunjukkan tanggung jawab dan kepekaan terhadap orang lain.  
Aspek yang keempat adalah ketrampilan motorik yang terbagi atas dua 
subaspek yaitu keterampilan motorik kasar yaitu bagaimana siswa TK  X  
menggunakan lengan dan kakinya untuk bergerak dan berkoordinasi, dan 
keterampilan motorik halus yaitu bagaimana siswa TK  X  menggunakan tangan 
dan jarinya untuk melakukan sesuatu.  
 Keempat aspek perilaku adaptif dimiliki oleh setiap siswa TK  X  namun 
masing-masing aspek perilaku adaptif tersebut belum tentu sama satu sama lain. 
Hal ini dikarenakan ada faktor yang mempengaruhi perilaku adaptif. Menurut 
Tudor (1981), ada beberapa faktor yang dapat mempengaruhi perilaku adaptif 
siswa TK  X , yaitu faktor internal dan eksternal. Faktor eksternal terdiri dari 
stimulation, deprivation (kekurangan), media massa, kekacauan atau bencana 
sosial, sekolah dan orang tua. Faktor internal terdiri dari budaya, kelas sosial, ras, 
status kesehatan dan penyakit.  
Stimulation yang efektif bagi perkembangan siswa TK  X  dapat 
dilakukan dengan berbagai cara misalnya menggunakan fasilitas bermain untuk 
melatih perkembangan motorik sehingga siswa dapat berlatih melompat dengan 
satu atau dua kaki, menangkap bola. Fasilitas yang disediakan oleh TK  X  dalam 
melatih perkembangan motorik adalah adanya tempat bermain yang dinamakan 
 kamar busa  yang berisi trampolin, balok-balok yang dapat digunakan untuk 
melatih keseimbangan, panjat tebing untuk anak-anak. Kemudian sekolah dapat 
melakukan kegiatan outing atau drama sehingga dengan acara seperti ini siswa 
dapat meningkatkan kemampuan dalam bersosialisasi seperti bagaimana cara 
berbagi dengan teman dan berbicara dengan teman atau dengan orang dewasa dan 
lain sebagainya. Stimulasi tidak hanya dapat dilakukan di luar kelas tetapi di 
dalam kelas seperti makan bersama sehingga dapat mengembangkan keterampilan 
hidup sehari-hari.  Dengan adanya pembiasaan terhadap kegiatan-kegiatan 
tersebut, diharapkan anak akan semakin dapat mengembangkan perilaku 
adaptifnya dan tidak mengalami deprivation (kekurangan) yang menghambat 
dalam perkembangan perilaku adaptif mereka.  
Faktor eksternal lain adalah media massa seperti seperti buku, televisi, 
film. Media ini memiliki dampak yang positif maupun negatif. Positifnya, melalui 
media, siswa dapat memperluas wawasan dan pengetahuan serta menambah 
pembendaharaan kata yang akan mempengaruhi keterampilan komunikasi. 
Namun bila siswa dibiarkan menonton tanpa pengawasan orang tua dapat 
memberi dampak negatif seperti berbicara kasar (misalnya kata  bego , 
 pencuri ). Faktor lainnya adalah kekacauan atau bencana sosial. Menurut 
Newman (1976), bencana tidak memiliki dampak langsung pada anak. Bila anak 
di bawah usia dua belas tahun mengalami bencana sosial atau alam maka akan 
menimbulkan dampak negatif terhadap bencana misalnya pengertian mereka akan 
realitas dan kemungkinan terhadap peningkatan stress di masa yang akan datang 
(ketika anak telah remaja atau dewasa).  
Dampak sekolah terhadap perkembangan perilaku seringkali diasumsikan 
sebagai hal yang positif karena disekolah siswa dapat mengembangkan 
keterampilan-keterampilan yang dimilikinya. Faktor eksternal yang terakhir 
adalah pengaruh dari orangtua. Orangtua memiliki peran penting terhadap 
perkembangan anak. Orangtua berperan sebagai media perantara antara 
karakteristik perkembangan anak mereka dengan lingkungan di mana mereka 
tinggal. Tingkat pendidikan orang memiliki pengaruh langsung maupun tidak 
langsung terhadap perkembangan perilaku adaptif anak mereka. Hal ini terlihat 
dalam luasnya wawasan dan pengetahuan orangtua. Bekal wawasan dan 
pengetahuan orangtua yang luas dapat bermanfaat ketika membimbing, mendidik 
perilaku adaptif anak mereka. Jenis pekerjaan orangtua juga dapat mempengaruhi 
perkembangan perilaku adaptif anak. Jenis pekerjaan orangtua dapat menjadi hal 
yang bermanfaat bagi anak mereka bila adanya waktu ataupun tersedianya 
fasilitas yang berguna untuk mengembangkan perilaku adaptif anak. Namun akan 
kurang bermanfaat bila orangtua menjadi lebih sibuk sehingga kurang 
menyediakan waktu bagi anak mereka. 
Faktor yang kedua yaitu faktor internal yang terdiri atas status kesehatan 
dan penyakit. Penyakit merupakan hal yang menghalangi perilaku seiring dengan 
perkembangan fisik. Anak yang memiliki fisik yang sehat merupakan prasyarat 
untuk perkembangan perilaku yang sehat yang dinyatakan melalui relasi dari 
kemampuan fisik terhadap tugas-tugas perkembangan bagi anak. Faktor internal 
yang lain adalah budaya yang didefinisikan secara umum sebagai sekumpulan 
nilai, keyakinan, informasi dan aturan yang diwariskan oleh sekelompok individu 
dari generasi ke generasi. Pengaruh budaya bagi perilaku siswa-siswi dapat positif 
maupun negatif. Positifnya, sebagian pengalaman dapat mengembangkan 
beberapa perilaku seperti inisiatif dan adaptasi sosial pada siswa-siswi dan 
negatifnya, dapat menimbulkan kebingungan bagi siswa-siswi dalam berperilaku. 
Misalnya ada budaya yang dalam berkomunikasi harus menggunakan suara yang 
 besar/lantang  dan ada budaya yang mengharuskan berkomunikasi dengan suara 
yang  halus . Ketika anak yang berkomunikasi dengan suara yang  besar  masuk 
dalam lingkungan yang diharuskan berkomunikasi dengan suara yang  halus  
maka akan terjadi kebingungan dalam diri anak serta memerlukan adaptasi dalam 
diri anak.   
  Faktor lainnya adalah kelas sosial, yang pengaruhnya akan lebih nyata 
pada kelompok tingkat ekonomi bawah yaitu anak harus belajar menghadapi 
kenyataan mengenai kemiskinan. Ketika anak berada dalam garis kemiskinan, 
maka anak akan mengalami keterbatasan dalam mengembangkan perilaku 
adaptifnya, seperti anak akan kurang mendapatkan sarana dan prasarana. Faktor 
internal yang terakhir adalah ras, yang sering dikaitkan dengan perbedaan kelas 
sosial. Anggota dari ras mayoritas memiliki gambaran diri yang positif 
dibandingkan dengan anggota dari ras minoritas. Hal tersebut dapat berpengaruh 
pada perilaku, misalnya siswa yang berasal dari suku minoritas ( Batak ), siswa 
membutuhkan waktu untuk beradaptasi dalam permainan yang biasa dimainkan 
oleh suku mayoritas ( Sunda ). Pengalaman yang dimiliki anak akan berpengaruh 
pada perkembangan anak selanjutnya.  
 Faktor-faktor yang mempengaruhi perilaku tersebut dapat dihayati 
berbeda-beda oleh anak TK  X  yang satu dengan yang lain sehingga perilaku 
adaptifnya dapat berbeda-beda untuk setiap kategori.  Perilaku adaptif yang 
berada pada kategori tinggi bila anak TK  X  dapat menampilkan perilaku yang 
dimaksud dalam aktivitas sehari-hari yang diperlukan untuk bekal kemandirian 
dan tanggung jawab sosialnya. Perilaku adaptif dalam kategori rendah bila anak 
TK  X  tidak atau kurang dapat menampilkan perilaku yang dimaksud dalam 
aktivitas sehari-hari yang diperlukan untuk bekal kemandirian dan tanggung 
jawab sosialnya.  
Untuk lebih jelasnya dapat dilihat pada skema berikut ini 
Siswa-siswi  
Aspek : 
1. Komunikasi 
2. Ketrampilan 
hidup sehari-hari 
3. Sosialisasi 
4. Keterampilan 
Faktor-faktor yang mempengaruhi perilaku: 
1. Eksternal 
  stimulation (Perangsangan) dan deprivation (Kekurangan) 
2. Internal 
  Status kesehatan & penyakit 
keterampilan hidup sehari-hari. 
Anak prasekolah, dari usia tiga sampai lima tahun merupakan anak yang 
kompeten, terampil, berbicara spontan dan mementingkan dirinya sendiri 
(egosentris). Secara fisik, anak baik dalam mengendalikan dirinya sendiri dan 
pertumbuhannya sudah mantap tak ada yang dapat menganggu keseimbangan 
fisiologis. Sedangkan anak usia lima hingga tujuh tahun berada pada masa 
peralihan dari usia prasekolah menuju usia sekolah. 
Anak-anak  pada periode ini memasuki tahap inisiative versus guilt. 
Tahap ini dibangun secara otonomi dan menambah kemampuan untuk 
mengerjakan, dan melengkapi sebuah tugas. Dorongan untuk mengerjakan 
secara kooperatif dan berbagi kewajiban, anak sangat siap untuk belajar. Anak 
prasekolah sangat bangga dalam penyelesaian tugas. 
Berjalan, berlari dan memanjat sekarang hanya memerlukan sedikit usaha 
dalam melakukannya. Bila diberikan harapan yang jelas serta ditawarkan 
pertolongan, anak prasekolah akan menunjukkan kebanggaan dengan duduk 
tenang dan tetap mendengarkan. 
Bab II  Tinjauan Pustaka                                                                                      16 
  Perkembangan meningkat sejak usia tiga tahun, hal tersebut merupakan 
hasil dari peningkatan lebih lagi dari keterampilan pada masa batita. Pemenuhan 
yang utama meliputi kemampuan untuk berjalan lurus, mundur, berjalan dengan 
berjingkat, melempar bola dengan posisi tubuh berdiri tanpa kehilangan 
keseimbangan, menangkap bola dengan kedua lengan yang lurus, menendang 
bola, melompat dari ketinggian beberapa sentimeter, mengendarai sepeda roda 
tiga dan mengelilingi sudut yang luas. Anak usia empat tahun telah mampu untuk 
berlari dengan posisi berjingkat dengan pengontrolan yang baik ketika mulai, 
berhenti dan berjalan, menangkap bola dengan kedua lengan yang sudah mampu 
dilenturkan, menendang bola dengan lebih akurat, melompat dari tempat yang 
lebih tinggi, mengendarai sepeda roda tiga dengan lebih cepat dan mengelilingi 
sudut yang lebih tajam, meloncat dengan kaki yang disukainya, menaiki tangga, 
pohon, dan sarana taman bermain. 
  Sepanjang masa prasekolah, anak melanjutkan pemenuhan keterampilan 
dasarnya dan meningkatkannya lebih baik lagi seperti mengubah perilakunya 
yang sebelumnya ceroboh dengan bekerja lebih tenang dan berupaya mencapai 
hasil. Perilaku adaptif dari motorik halus anak usia ini berkaitan dengan 
pembentukan hubungan dasar sebelumnya dan kemampuan perseptualnya 
terhadap dimensi, bentuk, kedalaman dan ingatan terhadap bagian-bagian. 
Permainan motorik halus memiliki nilai sosial yang baru dimana anak 
Bab II  Tinjauan Pustaka                                                                                      17 
menggambar, mengelem, dan membangun bersama-sama dengan teman 
sebayanya. Meniru satu dengan lainnya terjadi, meningkatkan keterampilan-
keterampilan yang dimilikinya.  
  Menyediakan anak dengan peralatan yang tepat akan mendukung mereka 
mengekspresikan perasaan dan konsepnya tidak hanya melalui pengekspresian 
secara verbal (Gesell, Halverson, Thompson, Ilg, Castner, Ames, and Amatruda 
dalam Tudor, 1980). Perilaku motorik halus anak usia tiga tahun lebih 
mengindikasikan kemampuan yang berbeda dimana anak lebih mengontrol ciri-
ciri yang lebih ditegaskan lagi, kebingungan yang lebih sedikit, dan pengulangan 
yang lebih sedikit pula. Anak usia tiga tahun dapat membangun menara dengan 
9 10 blok, terjadi peningkatan dibandingkan ketika berusia dua tahun yang 
hanya mampu membangun 6-7 blok menara, mereka juga dapat melipat kertas 
dengan posisi memanjang namun belum dapat melipat secara diagonal, dapat 
menyalin lingkaran dan masih butuh contoh dalam menggambar salib. 
Berpakaian dan melepaskan pakaian sungguh membutuhkan keterampilan 
motorik halus dari anak usia tiga tahun. Anak usia tiga setengah tahun 
menunjukkan sedikit kekacauan dalam koordinasi motorik halusnya; mereka 
juga mulai menggunakan tangan yang tidak biasa digunakan atau menggunakan 
secara bergantian. 
  Anak usia empat tahun menemukan kesenangan dengan koordinasi 
motorik halusnya. Mereka mulai mengancingkan pakaian mereka dan 
mengikatkan tali sepatunya. Dalam menggambar, mereka melukiskan obyek 
dengan sedikit detil, namun demikian mereka berupaya konsentrasi untuk 
Bab II  Tinjauan Pustaka                                                                                      18 
menampilkan suatu detil yang spesifik. Mereka menggambar suatu lingkaran 
menurut putaran jam, dan lingkarannya lebih baik dibandingkan ketika usia tiga 
tahun. Mereka dapat menyalin salib, tapi tidak dapat menyalin berlian dari 
contohnya. Gambar orang yang khas meliputi kepala, anggota tubuh dan 
mungkin dua mata. Seringkali mereka menggambar suatu lingkaran mengelilingi 
bagian-bagian guna menghasilkan kesatuan. Dengan mengikuti contoh, anak usia 
empat tahun dapat melipat selembar kertas sebanyak tiga kali, yang membedakan 
dibandingkan ketika usia tiga tahun adalah melipat secara diagonal. Anak 
prasekolah mulai membuat desain yang kasar dan tulisan-tulisan. Mereka senang 
melihat namanya dituliskan pada gambar yang dibuatnya dan mulai menyalin 
tulisan-tulisan tersebut.  Anak usia empat tahun juga sudah dapat menggunakan 
gunting dengan keberhasilan yang masih sedikit (Gesell et al., 1940). Mereka 
mengekspresikan perasaan dan konsepnya melalui keterampilan motorik halus 
dan perilaku dengan jalannya dimana belum mungkin diekspresikan melalui 
keterampilan verbal.   
 Tugas terpenting dari komunikasi bagi anak prasekolah adalah bagaimana 
menggunakan bahasa yang didapatkan dari interaksi terbaik dengan lingkungan. 
Bahasa dapat dipakai untuk dikomunikasikan dengan yang lain dalam artian yang 
lebih eksplisit dibanding dengan anak-anak yang mampu melakukannya sebelum 
usia tiga tahun. Lingkungan yang luas mengasah belajar tentang komunikasi pada 
Bab II  Tinjauan Pustaka                                                                                      19 
anak-anak prasekolah; bagaimanapun juga keluarga merupakan pengaruh yang 
penting dalam perkembangan keterampilan komunikasi. 
Anak pada usia lima tahun dan siap untuk sekolah, dia mengharapkan 
untuk dapat menanyakan pertanyaan, terlibat dalam suatu percakapan dengan 
orang dewasa dan teman sebaya, mengikuti petunjuk yang komplek dan berkata 
sendiri dalam sebuah kelompok. Anak-anak belajar bahasa dalam setting alami, 
meskipun baik anak maupun orang tua tampak fokus pada proses belajar bahasa. 
Allen (1976) dan Brown (1973) menyatakan bahwa anak-anak dan orang dewasa 
keduanya berpikir tentang arti dan motif-motif dan hanya tambahan dalam 
kalimat, peraturan dalam tata bahasa, atau fonologi sebagaimana mereka 
berbicara. Akan tampak bahwa anak belajar mengenai peraturan tentang 
komunikasi dan bagaimana menggunakan kode bahasa untuk menemukan 
permintaan dari situasi komunikasi. 
Sejak usia prasekolah, perkembangan anak seperti layaknya kesadaran 
tentang dirinya sendiri, relasi yang ia punyai dengan yang lain akan 
mempengaruhi bagaimana anak menggunakan bahasa.  Memberikan respon 
kepada orang lain untuk interaksi yang komunikatif akan menyediakan 
kesempatan anak untuk belajar. Perilaku nonverbal sama baiknya seperti tingkah 
verbal dari yang lain akan mempengaruhi usaha anak dalam berkomunikasi, 
perasaannya mengenai dirinya berarti dan usahanya dalam interaksi sosial. 
Wawancara dengan para ibu mengenai strategi anak waktu makan, tidur, 
dan bermain, ditemukan bahwa anak yang berusia antara tiga sampai empat tahun 
mengintegrasikan strategi verbal dan nonverbal dan menggunakan strategi 
Bab II  Tinjauan Pustaka                                                                                      20 
psikologi seperti mencari perhatian, mengekspresikan kesendiriannya, seruan 
akan ketakutan, menggunakan ayah untuk membujuk ibu (Allen, 1976; Brown, 
1973). 
Snow (1972) memberitahukan dalam studinya mengenai kemampuan 
berbicara ibu (Mothers  speech) merupakan produksi modifikasi ibu untuk anak 
yang lebih muda akan berharga setidaknya melalui dua cara. Cara yang pertama 
adalah menjadikan pembicaraan tersebut sederhana, menarik, dan dapat dipahami 
untuk anak yang usianya lebih muda. Cara yang kedua, bahwa pembicaraan yang 
sederhana dipolakan untuk anak dalam belajar bahasa. 
Penguasaan anak mengenai jumlah keterampilan yang cukup dalam 
bentuk, isi, dan penggunaan bahasa meliputi fungsi perkembangan yang 
mengikutinya yaitu : perkembangan fonetis (phonetic development) yaitu produksi 
suara (sound production), tata bahasa (Lexican): kemahiran dalam kata, semantik : 
menggunakan bahasa dan relasi dengan yang lainnya, kemampuan untuk mengerti 
bahasa (verbal) dan ekspresi bahasa (komunikasi verbal), pengertian jarak dari 
pengucapan, sintaksis (Syntax) yaitu peraturan dalam tata bahasa, perhatian dalam 
komunikasi yaitu interaksi sosial, mendapatkan informasi; memberikan informasi; 
mengekspresikan kebutuhan dan perasaan, interaksi orang tua dan anak, 
komunikasi dari norma dan aturan-aturan sebuah kebudayaan,komunikasi dari 
peraturan suatu kondisi atau situasi, intonasi dan pola penekanan dari percakapan. 
Fungsi-fungsi ini secara spesifik berelasi dengan kemampuan anak untuk 
berkomunikasi dalam arti eksplisit kepada yang mendengarkan sama baiknya 
dengan mengerti bahasa. 
Bab II  Tinjauan Pustaka                                                                                      21 
Semua huruf hidup  terdengar benar pada usia ini. Huruf mati seperti m, n, ng, 
f, p, l, w, b, t, dan h dapat diucapkan dipermulaan, pertengahan dan akhir dari 
kata-kata yang digunakan secara konsisten oleh anak. Selalu terdapat variabel 
yang luas di dalam pengertian dari percakapan anak berusia tiga hingga empat 
tahun; pada umumnya, bagaimana pun juga, penelitian mengindikasikan 
bahwa anak berusia tiga tahun mempunyai banyak huruf hidup dan diatas 
huruf mati. Lebih dari 90% dari percakapan anak yang berusia tiga tahun 
seharusnya sudah dapat dimengerti (Von Riper, 1978). 
Usia tiga tahun, anak mampu mengekspresikan pembendaharaan kata sekitar 
900 kata dan dapat menerima pembendaharaan kata sampai 1500 kata. 
  Makna jarak dari ungkapan/ucapan 
Makna jarak dari suatu ungkapan untuk anak usia tiga hingga empat tahun 
adalah 2,7 sampai 4 kata (Brown, 1973). 
  Sintaksis (SYNTAX) 
Struktur kalimat telah terbukti benar (well established). Frasa kata benda yang 
komplek + frasa kata kerja + frasa kata kerja tampak dalam:   Saya 
menginginkan banyak biskuit dan kemudian saya akan minum susu.  Anak 
mulai menggunakan kata ganti: mereka (them), kamu (you), kepunyaanmu 
(your), dia perempuan (she). Anak akan menggunakan  s untuk kata benda 
Bab II  Tinjauan Pustaka                                                                                      22 
jamak (plurality): books, toys, blocks. Pada usia ini, anak mulai menanyakan 
pertanyaan dengan memulai kata  apa  di awal kalimat.  
Anak mulai menghubungkan kejadian dalam waktu: relasi sementara dan 
urutan ide-ide atau kejadian. Anak merespon penjelasan dengan pertanyaan 
tambahan. Anak juga akan menentukan relasi dan mengklasifikasikan informasi:    
 saya membutuhkan sebuah Band-aid, karena jari tangan saya terluka   saya 
menginginkan sebuah biskuit karena saya menyukainya . Dia mulai 
menambahkan istilah warna, ukuran dan bentuk:  saya ingin bermain dengan 
anjing yang kecil   jerapah itu besar, lehernya panjang . 
 Pada ulang tahunnya yang keempat, anak dapat berdiskusi sejumlah topik 
yang lebih luas. Anak dapat menverbalisasikan apa yang ia lakukan atau kejadian 
yang lalu dengan deskripsi spesifik yang dapat dimengerti oleh pendengar:  saya 
melihat seekor beruang putih besar di kebun binatang kemarin   ayah bekerja di 
pesawat terbang besar yang terbang di angkasa. . 
 Mungkin banyak dari perubahan yang signifikan sejak tahun ketiga dan 
tahun keempat bahwa anak dapat menggunakan bahasa untuk menanyakan 
tentang kejadian atau memberikan informasi; dia akan mulai mempertahankan 
dialog dengan orang dewasa dan dapat mengekspresikan perasaannya dengan 
kata-kata. 
Bab II  Tinjauan Pustaka                                                                                      23 
Kebanyakan konsonan diartikulasikan secara benar dengan banyak 
perkecualian r, s, dan l. Dalam banyak konteks dari struktur kalimat, 
pengucapannya akan berubah atau disubstitusikan dan seringkali pembicaraan 
tidak berpengaruh dan terhenti. Bagaimanapun juga, anak pada usia ini selalu 
bepengaruh dan pembicaraan dapat dimengerti. 
Anak pada usia empat hingga lima tahun memiliki kurang lebih 1500 kata 
dalam mengekspresikan pembendaharaan katanya. Dia dapat mengerti sekitar 
1500 sampai 2500 kata (Lillywhite, 1958). 
  Sintaksis (SYNTAX) 
Anak yang berusia empat sampai lima tahun menggunakan struktur kalimat 
yang komplek. Kata ganti, preposisi, kata bantu (auxiliaries), dan kata 
penghubung (conjunction) telah terbukti benar (well established).  Artikel  a  
dan  the  juga telah terbukti benar (well established).   
 Anak yang berusia 4-5 tahun memiliki banyak cara untuk menanyakan 
pertanyaan :  apakah saya akan pergi ke pesta? ,  kapankah saya berulang 
tahun? ,  bolehkah saya pergi ke pesta ulang tahun? ,  bolehkah saya 
memiliki sebuah kue ulang tahun? ,  akankah kamu menghadiri pesta ulang 
tahunku? ,  dimanakah saya akan merayakan pesta ulang tahun saya?  
Bab II  Tinjauan Pustaka                                                                                      24 
Anak pada usia ini dapat memulai menceritakan sebuah cerita atau 
kejadian. Mengimajinasikan permainan dengan dialog. Dia juga dapat 
mempertahankan sebuah dialog dalam sebuah topik. Anak prasekolah sering 
menggunakan isyarat motorik dan respon untuk mendefinisikan kata-kata dan 
menggambarkan aktifitas. Anak dapat memberitahu pendengar mengenai sebuah 
kejadian, contohnya bagaimana ia membuat sebuah gambar atau membuat suatu 
benda, menggunakan percakapan dan isyarat tubuh  yang komplek. Seorang anak 
dapat menggambarkan perbedaan dan persamaan sebuah obyek. 
Bagaimanapun juga, dasar dari keseluruhan komunikasi dibangun pada usia lima 
tahun. Bahasa merupakan sebuah alat untuk bersosialisasi dan pengalaman 
bekerja, pendidikan, dan tentu saja untuk kehidupan sehari-hari. 
Periode prasekolah terbentuk berdasarkan pemenuhan bulan-bulan 
sebelumnya, anak prasekolah khususnya diharapkan untuk memenuhi kebutuhan 
dasar dirinya. Tugas yang diperlukan baik oleh keterampilan motorik yang 
berlainan dan berbeda-beda akan menjadi mungkin diakhir masa prasekolah. 
Meningkatnya kemampuan kognitif dan bahasa juga mendukung pembentukan 
keterampilan. 
 Anak prasekolah memiliki keinginan untuk menyenangkan orang-orang 
yang dianggapnya penting, sama senangnya seperti ketika upayanya berhasil, 
akan menjadi faktor yang memotivasi. Konsep Erickson (1950) mengenai 
Bab II  Tinjauan Pustaka                                                                                      25 

