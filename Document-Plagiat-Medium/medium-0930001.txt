Penelitian ini dilakukan untuk memperoleh gambaran mengenai jenis motivasi prososial 
yang dominan pada mahasiswa profesi dokter (koasisten) universitas  X  Bandung. Rancangan 
yang digunakan dalam penelitian ini adalah rancangan penelitian deskriptif.  Pemilihan sampel 
menggunakan metode accidental sampling dan sampel dalam penelitian ini berjumlah 67 orang 
mahasiswa profesi dokter. 
Alat ukur yang digunakan adalah skenario proyektif motivasi prososial yang bersifat 
semi proyeksi, yang dibuat oleh peneliti berdasarkan aspek-aspek motivasi prososial dari Januz 
Reykowsky dan telah divalidasi dengan menggunakan content validity. Alat ukur ini terdiri dari 
13 situasi. Pengolahan data disajikan dalam bentuk distribusi frekuensi dan tabulasi silang. 
Hasil penelitian menunjukkan bahwa 50,74% mahasiswa profesi dokter universitas  X  
Bandung memiliki intrinsic prosocial motivation, 35,82% memiliki endocentric motivation dan 
13,43% memiliki ipsocentric motivation. 
Kesimpulan dalam penelitian ini yaitu mahasiswa profesi dokter universitas  X  
Bandung pada umunya didominasi oleh intrinsic prosocial motivation dan terdapat 
kecenderungan keterkaitan antara motivasi prososial dengan jenis kelamin, feedback dan 
 Peneliti mengajukan saran agar dilakukan penelitian lebih lanjut mengenai hubungan 
antara motivasi prososial dengan jenis kelamin, feedback, dan petunjuk verbal. Bagi mahasiswa 
profesi dokter yang memiliki intrinsic prosocial motivation disarankan untuk memelihara 
motivasi tersebut dalam menolong pasien. Misalnya dengan cara membina komunikasi antara 
mahasiswa profesi dokter dengan pasien sehingga mahasiswa profesi dokter mengetahui apa 
yang menjadi kebutuhan pasien.  
Kata kunci : motivasi prososial, mahasiswa profesi dokter 
This research was conducted to gain an idea of the type of motivation on student 
prosocial dominant medical profession (koasisten) university "X" Bandung. This research used a 
descriptive methods. 

