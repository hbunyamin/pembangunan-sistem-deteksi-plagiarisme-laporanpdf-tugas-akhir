Kriminalitas adalah suatu tindakan yang melanggar undang-undang atau 
ketentuan yang berlaku dan diakui resmi di mata hukum. Secara kriminologi yang 
berbasis sosiologis kejahatan merupakan suatu pola tingkah laku yang merugikan 
masyarakat (dengan kata lain terdapat korban) dan suatu pola tingkah laku yang 
mendapatkan reaksi sosial dari masyarakat 
(http://id.wikipedia.org/wiki/Kriminalitas). 
Terkait dengan semua tindakan yang berhubungan dengan pelanggaran 
hukum, kepolisian merupakan institusi pertama yang harus berhadapan dengan 
masalah-masalah kriminalitas ini. Pendekatan hukum merupakan pilihan terakhir 
dari semua permasalahan tersebut. Tugas inilah yang diemban oleh polisi. Untuk 
menangani permasalahan-permasalahan tersebut polisi harus mengetahui benar 
bidang-bidang yang berkaitan dengan jalur hukum.  Setelah lepas dari TNI, 
POLRI dituntut lebih profesional dan mandiri dalam menjalankan tugas-tugasnya. 
Penegak hukum di Indonesia, POLRI, secara umum memiliki visi: mampu 
menjadi pelindung, pengayom, dan pelayan masyarakat yang selalu dekat dan 
bersama-sama masyarakat, serta sebagai penegak hukum profesional dan 
proporsional yang selalu menjunjung tinggi supermasi hukum dan hak asasi 
manusia, pemelihara keamanan dan ketertiban serta mewujudkan keamanan 
negeri dalam suatu kehidupan nasional yang demokratis dan masyarakat yang 
sejahtera (Undang-Undang Republik Indonesia No. 2 tahun 2002). Sesuai dengan 
Undang-Undang yang ada, POLRI berada di setiap provinsi Indonesia, termasuk 
Banten. 
Provinsi Banten adalah sebuah provinsi yang dulunya merupakan bagian 
dari provinsi Jawa Barat, namun dipisahkan sejak tahun 2000. Provinsi Banten 
terdiri atas empat kabupaten dan empat kota, yang salah satunya adalah kabupaten 
 X , kota  Y . Kabupaten  X  terdiri dari 28 kecamatan yang dibagi lagi atas 340 
desa dan 5 kelurahan dengan luas 304.472 Ha (www.wikipedia.com, diakses 12 
April 2011). Luasnya kabupaten  X  tidak sebanding dengan luasnya lapangan 
pekerjaan di daerah tersebut. Hal ini juga disebabkan oleh faktor sumber daya 
yang terdapat  di kabupaten  X  bukan merupakan sektor industri yang paling 
kuat di provinsi Banten. Hal ini menyebabkan pemuda-pemuda yang berada 
disana mencari pekerjaan ke kota lain bahkan sampai ke Jakarta. Namun ada juga 
yang memutuskan untuk tetap tinggal disana sehingga menambah tingkat 
pengangguran di kabupaten tersebut. Selain itu, tingkat kriminalitas di provinsi ini 
juga tergolong tinggi menurut laporan anggota Reserse Kriminal Polres  X  
Provinsi Banten.  
Berdasarkan data dari Survei Angkatan Kerja Nasional, Agustus 2011, 
Angka Pengangguran Terbuka (APT) di Provinsi Banten mencapai 697.083 
orang. Tingginya tingkat pengangguran itu, karena harapan masyarakat terhadap 
Provinsi Banten sebagai daerah ekonomi potensial sangat tinggi dan memicu 
kenaikan urbanisasi. Angka pengangguran di provinsi Banten tertinggi 
dibandingkan provinsi lain di Indonesia. Banyak dari para pengangguran ini tidak 
memiliki gelar pendidikan yang tinggi sehingga semakin sulit untuk mendapatkan 
kerja, namun terdapat pula mereka dengan gelar sarjana kesulitan mendapatkan 
kerja. Tentu saja, hal ini diiringi dengan potensi kriminalitas yang semakin tinggi 
(http://www.tempointeraktif.com/hg/nusa_lainnya/2011.html, diakses 11 Oktober 
2011) 
Berdasarkan data dari Badan Pusat Statistik berkaitan dengan jumlah 
tindak kriminalitas di Indonesia setiap tahunnya, terutama di Provinsi Banten, 
terlihat adanya pergerakan fluktuatif yang memengaruhi dinamika dan tuntutan 
kerja Kepolisian setempat. Tercatat pada tahun 2004, terjadi 802 tindak 
kriminalitas kemudian meningkat (lebih dari 100%) di tahun 2005 menjadi 1.946 
tindak kriminalitas. Pada tahun 2007, terjadi penurunan angka kriminalitas 
menjadi 1.771, kemudian semakin menurun di tahun 2008 menjadi 1.255, dan 
kembali melonjak di tahun 2009 menjadi 2.481 tindak kriminalitas 
(http://www.bps.go.id/tab_sub/view.php?tabel=1&daftar=1, diakses 11 Oktober 
2011). Gambaran tingkat pengangguran dan tingkat kriminalitas di Provinsi 
Banten tentunya juga menjadi kondisi umum di Kabupaten  X , yang menjadi 
wilayah hukum Satuan Reskrim Polres  X . 
Satuan Reserse Kriminal (Satreskrim) yang merupakan divisi di bawah 
Badan Reserse Kriminal bertugas melaksanakan penyelidikan, penyidikan, dan 
pengawasan penyidikan tindak pidana, termasuk fungsi identifikasi dan 
laboratorium forensik lapangan serta pembinaan, koordinasi dan pengawasan 
Penyidik Pegawai Negeri Sipil (PPNS). Dengan visi dan misi serta fungsi yang 
harus dijalankan, Anggota Polisi Satuan Reskrim Polres  X  diharapkan dapat 
menunjukkan kinerja yang optimal terutama dalam menjalankan fungsi-fungsi 
Reskrim di wilayah tersebut.  
Dalam menjalankan fungsinya, para anggota Satreskrim mengalami 
beberapa kendala seperti tingkat kriminalitas yang tergolong tinggi namun jumlah 
anggota yang ada kurang memadai. Hal ini menjadi suatu bentuk stressor bagi 
Reskrim Provinsi Banten. Terdapat beberapa hal lain dalam satuan reskrim Polres 
 X  yang dapat menjadi sumber stressor bagi anggota-anggotanya. Salah satu 
sumbernya adalah dalam menjalani fungsi-fungsi reskrim guna mencapai visi misi 
yang ada dapat seringkali menimbulkan stress berupa kurangnya konsentrasi 
dalam bekerja, emosi yang menjadi fluktuatif, hingga kondisi kesehatan yang 
memburuk karena beban mental. Stressor lain berupa terancamnya kesejahteraan 
hidup anggota karena dalam menunaikan tugasnya Satreskrim seringkali 
mempertaruhkan nyawa seperti pada saat penyergapan tersangka yang bersenjata 
api. Namun, secara aktual, dampak stress yang dirasakan oleh para anggota polisi 
tidak selalu dapat terlihat langsung. Dampak stress dari tuntutan pekerjaan bisa 
berupa gangguan-gangguan yang dirasakan ketika individu tidak sedang bekerja 
atau bertugas, misalnya mengalami kesulitan tidur, pola makan yang berubah, 
bahkan dengan adanya keinginan untuk mengundurkan diri dari kepolisian.  
Tuntutan-tuntutan pekerjaan anggota Satreskrim sebagaimana yang 
dijabarkan sebelumnya, dapat menjadi stressor bagi para anggota polisi. Stressor 
adalah tuntutan atau tekanan yang membutuhkan penanggulangan tingkah laku 
sebagai bagian dari individu atau kelompok (Coleman dalam Lazarus & Folkman, 
1984). Stressor dapat menyebabkan stress. Biasanya jika mengalam stress, 
manusia akan melakukan coping stress untuk mengembalikan keseimbangan 
dalam diri individu dan membantu individu beradaptasi dengan masalah yang 
dialaminya agar individu dapat bangkit dan kembali menjalankan aktivitas sehari-
hari. Berdasarkan studi empiris yang ada (Eisenberg, 1975; Stratton, 1978 dalam 
http://www.metro.polri.go.id/sterss), polisi peka terhadap variasi yang luas dari 
tekanan pekerjaan atau "penyebab stress." Penyebab stress ini dapat 
dikelompokkan dalam kategori berikut: (1) di luar departemen polisi, meliputi 
keputusan pengadilan yang tak menguntungkan, ketiadaan dukungan masyarakat, 
dan potensi kekerasan warga bahkan ketika berhadapan dengan penyelidikan lalu-
lintas rutin atau pertengkaran rumah tangga; (2) sumber internal, meliputi gaji 
rendah, kemajuan karir yang terbatas, pengembangan atau insentif profesional 
yang kecil, dan ketiadaan dukungan administratif; serta (3) penyebab stress yang 
berasal pada peran polisi itu sendiri, termasuk perputaran shift kerja, kerja 
administratif yang berlebihan, dan harapan publik bahwa polisi harus menjadi 
panutan dan figur yang melayani kebutuhan semua orang (Eisenberg, 1975; 
Stratton, 1978 dalam http://www.metro.polri.go.id/sterss). Penyebab stress 
potensial akan bertukar-tukar intensitas, tergantung pada situasi dan kepribadian 
petugas.  
Jika petugas memiliki kepribadian yang kurang mampu menampilkan 
kinerja optimal di bawah tekanan yang terus menerus; kurang mampu 
menyelesaikan permasalahan secara efektif dan efisien; merasa kurang yakin diri 
dapat memenuhi tuntutan pekerjaan yang ada, segala sumber stress potensial akan 
dihayati dengan intensitas yang lebih tinggi dibandingkan kondisi nyata. 
Sebaliknya, pada petugas dengan kepribadian yang cenderung mampu 
menampilkan kinerja optimal dibawah tekanan; memiliki kemampuan pemecahan 
yang baik; merasa yakin diri akan kemampuan dan usaha yang diberikan dalam 
memenuhi tuntutan pekerjaan, sumber stress potensial akan dihayati dengan 
intensitas sebagaimana adanya dan tidak akan menjadi beban atau tekanan 
berlebih dalam mencapai target kerja. Durasi dan besarnya stress tergantung pada 
umur panjang, jabatan, dan jenis kelamin petugas tersebut 
(http://www.metro.polri.go.id/stress,  diakses 28 September 2011). 
Individu yang mengalami stress untuk periode waktu yang panjang 
mempunyai konsekuensi negatif bagi fisik, psikologis, dan kesejahteraan 
emosional. Konsekuensi ini berpengaruh terhadap perilaku petugas, keterampilan 
menangani pekerjaan, dan mutu intervensi manajemen; bagaimana lingkungan 
kerja serta pihak yang terlibat secara langsung dengan anggota polisi dapat 
memberikan bantuan ataupun kritik konstruktif yang dapat diterima oleh individu 
dan secara efektif memengaruhi atau memerbaiki kinerja individu yang 
mengalami stres karena tuntutan pekerjaan yang ia dapat. Stress memengaruhi 
stabilitas hubungan keluarga secara merugikan. Tambahan konsekuensi lainnya 
terhadap stress polisi termasuk alkoholisme dan bunuh diri. Perkiraan 
mengindikasikan bahwa antara 20 dan 30 persen dari semua petugas polisi 
mempunyai masalah yang berkaitan dengan minuman keras, dan figur ini 
mungkin meningkat. Tipikal peminum adalah lajang, di atas usia 40 tahun, dengan 
pengalaman kerja menjadi polisi 15 sampai 20 tahun (Shook, 1978; Somodevilla, 
1978). Efek kombinasi dari mabuk dan stress menyebabkan darah tinggi, serangan 
jantung, dan gangguan kardiovaskuler lainnya. Dua dari tiga kematian polisi dapat 
dihubungkan dengan kekacauan kardiovaskuler (Kreitner et al., 1985 ;Violanti et 
al., 1983; Violanti et al., 1985). Tingkat bunuh diri petugas penegak hukum 
adalah dua berbanding enam kali lebih tinggi dibanding mereka yang memiliki 
pekerjaan lain (Blackmore, 1978; Somodevilla, 1978). Di tahun 1950, sebagai 
contoh, petugas polisi New York City melakukan bunuh diri paling tinggi kedua 
dari tiga puluh enam survei pekerjaan (Labovitz dan Hagedorn, 1971).  
Di Indonesia pun banyak kasus yang terjadi berkaitan dengan stress yang 
dialami polisi seperti kasus Oknum anggota Polresta Samarinda Brigadir Polisi 
Satu (Briptu) S (27 tahun) warga Samarinda yang beberapa waktu lalu mencoba 
bunuh diri karena stress akibat kasus yang dilakukannya yaitu meninggalkan 
tugas selama berbulan-bulan (desersi) dan terlibat kasus narkoba, yang akhirnya 
berkonsekuensi pemecatan (http://www.balikpapanpos.co.id , diakses 11 Oktober 
2011). Kasus lainnya seperti penganiayaan fisik yang dilakukan petugas polisi 
yang diduga mengalami stress, sebagaimana dipaparkan dalam artikel berikut :  
 Inspektur Satu TM yang menjadi tersangka penembakan tiga orang 
rekannya di Kepolisian Resor Jakarta Barat, Selasa (28/11) pagi, disinyalir 
sebelumnya sudah menunjukkan gejala stress. Demikian dikatakan petugas 
Divisi Provost Kepolisian daerah Metro Jaya, baru-baru ini. Sebelumnya, 
TM dan tiga rekannya Iptu JI, Iptu M, dan Iptu IPEI hanya ngobrol-ngobrol 
biasa diruang kerja M. Namun, mendadak pecah perang mulut antara TM 
dan J. Tragisnya, perang mulut itu ditutup TM dengan letusan senjata. JI 
dan IPEI langsung tewas setelah peluru bersarang di kepalanya. Sedangkan 
M masih dirawat di Rumah Sakit Pelni Jakarta. Sementara, TM hingga kini 
masih menjalani pemeriksaan di Polda Metro Jaya.  
(http://berita.liputan6.com/read/4434/tasman-diduga-sebagai-polisi-stres , 
diakses 11 Oktober 2011).  
Studi dan kasus yang telah dipaparkan di atas menunjukkan pentingnya 
strategi penanggulangan stress (coping stress) yang digunakan anggota kepolisian 
sehingga dapat mengatasi stress yang dialami oleh seseorang. Individu dalam 
menghadapi permasalahan, hambatan dan kesulitan berkaitan dengan pekerjaan 
tidak akan lepas dari kondisi yang membuat mereka stress sehingga individu perlu 
mengembangkan kemampuan atau strategi yang membuat stress yang ada tidak 
akan berdampak negatif pada kinerja sehari-hari. Coping stress menjadi penting 
dalam membantu individu mempertahankan pola pikir yang rasional, pengaturan 
mood, mendukung pemecahan masalah yang lebih efektif, dan fokus terhadap 
pekerjaan yang sedang dilakukan (Lazarus & Folkman, 1984). 
Coping stress adalah usaha untuk mengubah tingkah laku dan kognitif 
secara konstan untuk mengatur tuntutan-tuntutan internal dan/atau eksternal yang 
spesifik yang dinilai sebagai beban atau melampaui sumber daya yang dimiliki 
individu. Strategi coping stress menurut Lazarus ada dua, yaitu problem focused 
coping, individu menggunakan strategi kognitif dalam mengatasi stress pada saat 
menghadapi masalah dan mencoba untuk menyelesaikannya. Misalnya bagi 
anggota polisi yang bekerja dalam tekanan dari atasan yang setiap saat selalu 
mengawasi kinerja mereka bahkan tidak jarang mereka mendapat teguran untuk 
kesalahan yang dilakukan seperti menyalahi peraturan dalam menangani kasus 
tertentu. Tetapi bila individu tersebut mampu menunjukkan kinerjanya dengan 
baik, individu tersebut dapat memperoleh pujian atau bahkan tunjangan dari 
atasan.  
Strategi kedua adalah emotion focused coping, individu memberikan respon 
terhadap stress secara emosional, terutama menggunakan penilaian defensive 
(bertahan). Misalnya ketika kepolisian berusaha untuk mensosialisasikan program 
dan upaya mereka dalam meningkatkan pelayanan dan kesejahteraan masyarakat, 
hal ini tentu membawa efek yang berbeda bagi tiap orangnya terutama dengan 
banyaknya stereotip negatif masyarakat pada aparat kepolisian. Ada anggota 
kepolisian yang tetap bersemangat setelah mendapatkan judgement yang kurang 
menyenangkan dan hanya menganggap hal tersebut sebagai hal biasa dalam 
pekerjaannya untuk kemudian berusaha mengubah pandangan negatif yang ada 
pada masyarakat. Namun ada pula yang menunjukkan penurunan kinerja 
dikarenakan hal tersebut, mereka merasa kurang dihargai sebagai aparat 
kepolisian. Anggota polisi menjadi kurang attentive dalam menanggapi keluhan 
atau pengaduan yang disampaikan anggota masyarakat; tidak secara sigap 
bertindak dalam menyelesaikan kasus yang disampaikan anggota masyarakat, 
misalnya dengan menunda pencarian kendaraan bermotor yang dicuri sesuai 
dengan yang dilaporkan oleh anggota masyarakat. 
Peneliti melihat bahwa dalam kepolisian provinsi Banten, terdapat 
Satreskrim yang dinilai memiliki tuntutan pekerjaan yang tergolong stressful. 
Guna membuktikan penilaian ini, peneliti menggunakan alat ukur stress PSQ 
(Perceived Stress Questionnaire). PSQ adalah alat ukur yang dibuat oleh 
Levenstein, Prantera, Varvo, Scribano, Berto, Luzi, dan Andreoli pada tahun 
1993.  PSQ merupakan kuesioner yang mampu mengukur derajat stress yang 
dipersepsikan oleh individu. Pada tahun 2004, PSQ divalidasi ulang sedemikian 
rupa sehingga masih memiliki validitas dan reliabilitas yang terpercaya dalam 
mengukur derajat stress yang dipersepsikan oleh individu (Fliege et al., 2005). 
Dari perspektif konseptual, kuesioner ini sangat berguna pada saat mengukur 
derajat stress yang dipersepsikan individu secara langsung, tanpa memberikan 
praduga penilaian kontrol terhadap stress dan strategi penanggulangan stress. 
Selain itu, bilamana diperlukan, PSQ juga mampu menjaring informasi 
menyangkut stressor eksternal. Peneliti menyebarkan PSQ ke seluruh anggota 
Satreskrim Polres  X  Provinsi Banten. 
Peneliti mendapatkan gambaran akurat mengenai derajat stress yang 
dipersepsikan oleh anggota Satreskrim setelah mengolah data-data yang 
didapatkan melalui PSQ. Berdasarkan hasil dari pengolahan skor PSQ, peneliti 
menemukan bahwa 21 dari 51 anggota Satreskrim menghayati dirinya mengalami 
stress yang tergolong tinggi, dan 30 dari 51 anggota Satreskrim menghayati 
dirinya mengalami stress yang tergolong rendah. Dari 30 anggota Satreskrim 
dengan golongan stress rendah, terdapat 17 anggota Satreskrim golongan stress 
rendah (59% dari kelompok golongan stress rendah) berada di ambang batas 
golongan stress tinggi. Rasio persentase anggota Satreskrim yang menghayati 
dirinya mengalami stress dari golongan rendah, dan tinggi adalah 41% : 59%. 
Dari penjabaran data sebelumnya, dapat disebutkan bahwa anggota Satreskrim 
mengalami stress dalam menjalankan tugasnya sebagai Satreskrim. 
Peneliti kemudian melakukan survei awal lebih lanjut pada 10 anggota 
Polisi Satuan Reskrim Polres  X  Provinsi Banten untuk melihat respon yang 
muncul dari masing-masing individu ketika mereka menghadapi deadline target 
penurunan angka kriminalitas setiap bulannya. Tiga dari sepuluh anggota polisi 
tersebut merasa bahwa target yang ditetapkan memang menyebabkan stress dalam 
kehidupan sehari-hari mereka, namun stress tersebut juga menjadi dorongan untuk 
bekerja lebih baik lagi dan mendorong mereka untuk menentukan strategi yang 
lebih efektif dalam mencapai target yang ada. Salah satu cara coping yang mereka 
anggap efektif dalam mencapai target adalah dengan berdiskusi sesama anggota 
polisi dan bekerja sama dalam memberantas kriminalitas bidang tertentu secara 
satu per satu. Upaya yang dikeluarkan ini dihayati anggota lebih menyita waktu 
dan energi sehingga seringkali waktu tidur mereka kurang dan hal ini 
mempengaruhi pula mood swing sehari-hari, mereka merasa lebih sensitif dan 
mudah terganggu dengan hal kecil. Misalnya saja ada salah satu anggota yang 
sudah memiliki anak, dan suatu hari anak tersebut memaksanya untuk bermain 
bersama, karena polisi tersebut sedang fokus menyelesaikan laporan bulanan, ia 
pun tidak sengaja membentak anak tersebut hingga anaknya menangis. Sebanyak 
tujuh dari sepuluh responden awal menunjukkan respon terhadap stress yang lebih 
berfokus pada kondisi emosi mereka, dua dari tujuh responden tersebut 
menyatakan bahwa setiap kali deadline target mendekat, mereka akan mengalami 
gangguan tidur; sulit untuk tidur atau tidur tidak tenang dan bahkan mengalami 
mimpi buruk dimarahi oleh atasan karena target yang tidak terpenuhi. Dua 
responden lain menyatakan bahwa stress pekerjaan seringkali membuat mereka 
merasa mual dan pusing setiap pagi sebelum berangkat kerja, membuat mereka 
ingin izin untuk tidak masuk kerja dan menghindari tuntutan yang begitu 
membebani mereka. Sebanyak tiga responden lain seringkali merasa mood mereka 
begitu mudah terpengaruh kondisi lingkungan sekitar, mereka mudah marah, 
kecewa, dan sedih. Seringkali mereka merasa perlu menceritakan beban yang 
dirasakan ini pada anggota keluarga seperti istri atau orang tua. 
Hasil PSQ dan survei awal yang dilakukan menunjukkan bahwa anggota 
polisi mengalami stress. Kondisi tersebut dapat diakibatkan oleh adanya tuntutan 
pekerjaan anggota polisi sebagai Satreskrim yang memiliki tanggung jawab di 
berbagai aspek; tidak hanya berkaitan dengan tindak kriminalitas, namun juga 
bertanggung jawab dengan pembinaan, pengawasan, pendisiplinan lembaga 
masyarakat pada umumnya. Tugas dan tuntutan yang didapat akan dipandang 
sebagai stressor dan mendorong anggota polisi untuk mengembangkan pula 
strategi yang efektif dalam menanggulangi kondisi tersebut. Coping stress yang 
dikembangkan oleh individu dapat secara efektif membantu mereka dalam 
menjalankan tugas pekerjaan sehari-hari; mendukung adanya kinerja yang 
optimal, kondisi fisik dan psikis yang sehat dalam bekerja, serta pencapaian target 
kerja. Namun, jika coping stress tidak berjalan secara efektif, individu dapat 
mengalami penurunan kinerja; stress yang dialami menjadi hambatan yang 
membuat individu tidak dapat fokus dalam bekerja, misalnya tidak mampu 
memenuhi deadline pekerjaan yang diberikan atasan, kesulitan mencapai target 
kerja yang ada, mengembangkan pola interaksi sosial yang buruk dengan orang-
orang disekitarnya, dan adanya potensi pengunduran diri oleh individu yang 
merasa tidak mampu bertahan dengan kondisi kerja yang penuh tuntutan. 
Berdasarkan hal tersebut dapat dilihat bahwa coping stress yang dipilih 
individu dalam menghadapi tuntutan pekerjaan berbeda-beda, tergantung pada 
penghayatan individu pada masalah yang mereka hadapi. Oleh karena itu, peneliti 
tertarik untuk meneliti coping stress pada Anggota Polisi Satuan Reskrim Polres 
 X  Provisi Banten. 
Bagaimana coping stress yang dominan digunakan pada Anggota Polisi 
Satuan Reskrim Polres  X  Provinsi Banten. 
Maksud dari penelitian ini adalah untuk memperoleh gambaran bentuk 
coping stress yang digunakan pada Anggota Polisi Satuan Reskrim Polres 
 X  Provinsi Banten. 
Tujuan dari penelitian ini adalah untuk mengetahui mengenai gambaran 
bentuk coping stress yang digunakan Anggota Polisi Satuan Reskrim Polres 
 X  Provinsi Banten berdasarkan bentuk-bentuk spesifik coping stress 
problem focus dan coping stress emotional focus berikut faktor-faktor yang 
memengaruhi penggunaan jenis coping stress. 
Klinis dan Psikologi Sosial guna memperkaya pemahaman serta 
fenomena stress yang ada pada lingkungan kerja kepolisian. 
lanjut mengenai coping stress khususnya pada anggota kepolisian.  
digunakan Anggota Polisi Satuan Reskrim Polres  X  Provinsi Banten 
sebagai bahan pengembangan diri anggota dalam meningkatkan efisiensi 
kerja. 
 X  Provinsi Banten mengenai strategi penanggulangan stress yang 
digunakan pada Satreskrim sehingga dapat menjadi masukan apakah 
coping stress tersebut membantu mereka mengatasi stress yang dialami 
dalam melaksanakan fungsi-fungsi Satreskrim.  
Stress muncul jika seseorang dihadapkan pada berbagai tuntutan 
lingkungan yang mengganggu dan membebani serta melampaui batas kemampuan 
dirinya (Lazarus, 1984). Anggota Polisi Satuan Reskrim Polres  X  Provinsi 
Banten  akan melakukan suatu penilaian dalam menentukan ada tidaknya 
keseimbangan antara tuntutan dan kemampuan yang dimiliki sehingga Anggota 
Polisi Satuan Reskrim Polres  X  Provinsi Banten  dapat menentukan apakah 
suatu stressor dapat menimbulkan stress atau tidak. Walaupun stressor yang 
dihadapi sama tapi penilaian individual ini memberikan penghayatan yang 
berbeda-beda. 
Jika stressor dihayati sebagai stress maka Anggota Polisi Satuan Reskrim 
Polres  X  Provinsi Banten  akan berusaha melakukan penanggulangan dan 
tindakan yang diambil akan berbeda. Pada penilaian primer Anggota Polisi Satuan 
Reskrim Polres  X  Provinsi Banten  akan mengevaluasi apakah tuntutan dan 
target kerja yang diberikan organisasi/lembaga sesuai dengan kemampuan diri 
atau tidak dan apakah hal tersebut mengancam atau tidak, memberi tekanan atau 
tidak, membuat konflik atau tidak, membuat frustrasi atau tidak. 
Frustrasi yaitu stress yang muncul bila Anggota Polisi Satuan Reskrim 
Polres  X  Provinsi Banten  mengalami hambatan atau kegagalan dalam usahanya 
mencapai suatu tujuan, misalnya polisi yang ingin mencapai target penurunan 
angka kejahatan untuk mendapatkan reward tertentu namun persaingan dengan 
anggota polisi lain sangat ketat sehingga targetnya tidak berhasil dicapai. Konflik 
yaitu stress yang muncul bila salah satu dari dua atau lebih kebutuhan atau tujuan 
yang berlawanan dan terjadi pada saat yang bersamaan, misalnya anggota polisi 
yang harus menangani dua kasus kriminalitas penting di saat bersamaan dan 
merasa kurang mampu menyelesaikan keduanya sehingga memberikan salah satu 
kasus ke rekan kerjanya. 
 Contoh konflik lain yang dapat terjadi dalam pelaksanaan tugas dan 
tanggung jawab polisi adalah dalam membagi waktu antara pekerjaan dan 
keluarga. Pasangan, orang tua, maupun anggota keluarga lain yang mungkin tidak 
sepenuhnya memahami tuntutan pekerjaan yang dihadapi Anggota Polisi Satuan 
Reskrim Polres  X  Provinsi Banten  dapat memberikan komentar negatif 
mengenai Anggota Polisi Satuan Reskrim Polres  X  Provinsi Banten  yang 
kurang meluangkan waktu untuk menjalani aktivitas bersama dengan keluarga. 
Tekanan yaitu stress yang muncul apabila Anggota Polisi Satuan Reskrim Polres 
 X  Provinsi Banten  mendapat tekanan atau paksaan untuk mencapai hasil 
tertentu yang sumbernya dapat berasal dari dalam diri atau lingkungannya, 
misalnya ketika organisasi/lembaga memberikan peringatan pada anggota polisi 
yang sering gagal memenuhi target bulanan dan memberikan sanksi tertentu atau 
penurunan jabatan, bahkan pemecatan. Anggota polisi dapat memandang tekanan 
yang diberikan lingkungan kerja, misalnya atasan, sebagai suatu bentuk 
reinforcement untuk semakin mendorong diri mencapai hasil kerja yang 
diharapkan, namun tidak jarang tekanan justru membuat Anggota Polisi Satuan 
Reskrim Polres  X  Provinsi Banten  semakin merasa tidak mampu untuk 
menjalankan tugas dan tanggung jawabnya sesuai dengan ekspektasi dari dalam 
maupun luar dirinya.  
Ancaman yaitu stress yang muncul bila Anggota Polisi Satuan Reskrim 
Polres  X  Provinsi Banten  mengantisipasi hal-hal yang merugikan atau tidak 
menyenangkan bagi dirinya, misalnya anggota polisi berusaha lebih giat untuk 
menyelesaikan berbagai kasus dan melakukan tindakan pencegahan kriminalitas 
karena ia ingin mendapatkan kenaikan pangkat, namun jika ia gagal ia terancam 
pemecatan. Ancaman tersebut dapat lebih kuat pengaruhnya pada kinerja polisi 
dibandingkan dengan reinforcement positif yang ada (kenaikan pangkat), hal ini 
dapat menyebabkan polisi tidak dapat fokus sepenuhnya menjalankan tugasnya; 
terdistraksi dengan pemikiran negatif dan kecemasan akan sanksi yang akan ia 
terima jika ia gagal. Bila pada penilaian ini polisi menganggap situasi yang 
dihadapi mengancam atau melebihi kemampuannya maka akan mengalami stress 
yang ditandai dengan gejala psikis dan fisik, misalnya pusing, lelah, cemas, sulit 
konsentrasi, jenuh dengan pekerjaan.  
Pada penilaian sekunder Anggota Polisi Satuan Reskrim Polres  X  
Provinsi Banten  mengevaluasi seberapa besar kemampuannya, apakah cukup 
mampu untuk memenuhi target penurunan angka kriminalitas atau tidak. Pada 
penilaian ini mereka mencoba memahami potensi dalam diri. Setelah melakukan 
penilaian sekunder maka akan menentukan coping stress yang akan digunakan. 
Coping stress dapat diartikan sebagai usaha untuk mengubah tingkah laku dan 
kognitif secara konstan untuk mengatur tuntutan-tuntutan internal dan/atau 
eksternal yang spesifik yang dinilai sebagai beban atau melampaui sumber daya 
yang dimiliki Anggota Polisi Satuan Reskrim Polres  X  Provinsi Banten . 
Keberhasilan coping stress ini tergantung pada kesediaan dan energi yang 
merupakan sumber fisik, misalnya kondisi fisik yang dimiliki anggota polisi 
dalam menjalankan tugas setiap harinya, prima atau tidak; dorongan personal 
yang dimiliki anggota polisi untuk menampilkan kinerja yang terbaik karena karir 
sebagai seorang polisi adalah pilihan personal yang memang telah lama menjadi 
aspirasinya; keterampilan untuk memecahkan masalah, bagaimana polisi memiliki 
kemampuan untuk secara efektif dan efisien mencari jalan keluar dalam 
menghadapi segala tantangan dan hambatan yang ditemui dalam menjalankan 
tugas dan tanggung jawab; bagaimana polisi dapat tetap berpikir jernih dalam 
memecahkan suatu kasus meskipun berada dibawah tekanan masyarakat yang 
tidak tenang dengan situasi kehidupan yang diwarnai oleh tindak kriminalitas 
maupun dibawah tekanan atasan yang terus menuntut perbaikan kinerja anggota 
polisi.  
Faktor lainnya adalah keyakinan yang positif. Hal ini mencakup adanya 
belief (seperti nilai-nilai spiritual) maupun nilai moral atau etis lain yang dihayati 
oleh Anggota Polisi Satuan Reskrim Polres  X  Provinsi Banten  yang 
memberikan dorongan atau encouragement dalam memberikan usaha yang terbaik 
dan mempertahankan pandangan positif bahwa tuntutan kerja yang dimiliki dan 
semua hambatan yang menyebabkan stress dalam pelaksanaan tanggung jawab 
sehari-hari dapat diselesaikan dengan baik. Keterampilan sosial secara adekuat 
dan efektif menggambarkan bagaimana Anggota Polisi Satuan Reskrim Polres 
 X  Provinsi Banten  dapat membangun dan mempertahankan relasi sosial yang 
memberikan dampak positif pada kinerja Anggota Polisi Satuan Reskrim Polres 
 X  Provinsi Banten  seperti relasi professional yang terjalin dengan rekan kerja 
maupun atasan dan terutama dengan masyarakat luas; keterampilan sosial yang 
ditampilkan oleh masing-masing anggota polisi dapat menjadi cerminan 
kepolisian secara menyeluruh dan juga menjadi penentu image yang muncul 
dalam masyarakat mengenai polisi.  
Dukungan sosial juga termasuk dalam faktor yang mempengaruhi coping 
stress. Baik dari rekan kerja yang memberikan bantuan ketika Anggota Polisi 
Satuan Reskrim Polres  X  Provinsi Banten  tidak dapat menyelesaikan suatu 
kasus seorang diri; atasan yang memberikan kritik konstruktif dan dukungan 
positif pada anggota polisi yang merasa tidak mampu menyelesaikan tugas sesuai 
target; keluarga yang menerima apa adanya dan memahami tuntutan besar yang 
dimiliki oleh anggota polisi, memberikan dorongan-dorongan moral dan doa. 
Faktor lain yang mempengaruhi coping stress lainnya adalah sumber 
material. Faktor ini dapat mendukung terlaksananya penanggulangan secara 
efektif; fasilitas, sarana dan pra-sarana yang tersedia di lingkungan kerja yang 
dapat mendukung kelancaran dan keberhasilan kinerja anggota polisi, misalnya 
seperti kendaraan dinas, ruang kerja yang kondusif (bersih dan sejuk). Jika 
strategi coping dirasa kurang sesuai atau gagal maka akan dilakukan reappraisal. 
Reappraisal mengacu pada perubahan penilaian yang terjadi karena disadari oleh 
munculnya informasi baru, baik yang berasal dari lingkungan yang dapat 
menahan atau memperkuat tekanan bagi Anggota Polisi Satuan Reskrim Polres 
 X  Provinsi Banten , maupun informasi dari reaksi Anggota Polisi Satuan 
Reskrim Polres  X  Provinsi Banten  itu sendiri. 
Menurut Lazarus (1984) coping stress lebih ditujukan pada hal-hal yang 
dilakukan seseorang untuk mengatasi situasi stress atau tuntutan. Dinamika dan 
perubahan pada proses coping dikarakteristikkan melalui fungsi seseorang dengan 
lingkungannya. Perubahan tersebut dapat meliputi perubahan penilaian kognitif 
dan penilaian kembali cara penanggulangan dan proses emosional. 
Coping stress ada dua, yaitu yang berpusat pada emosi dan yang berpusat 
pada masalah. Coping stress yang berpusat pada emosi berfungsi untuk mengatur 
respon emosional terhadap stressor. Tingkah laku yang termasuk di dalamnya 
adalah seeking social support, yaitu apa yang dilakukan Anggota Polisi Satuan 
Reskrim Polres  X  Provinsi Banten  untuk mencari informasi dan nasihat dari 
seseorang untuk mendapatkan dukungan/sekedar simpati dari orangtua; self 
control yaitu Anggota Polisi Satuan Reskrim Polres  X  Provinsi Banten  
menguasai diri sendiri secara emosional; distancing yaitu Anggota Polisi Satuan 
Reskrim Polres  X  Provinsi Banten  menjaga jarak dari masalah agar dirinya 
tidak mengalami situasi yang muncul, Anggota Polisi Satuan Reskrim Polres  X  
Provinsi Banten  cenderung untuk menjauhinya; positive reappraisal yaitu 
Anggota Polisi Satuan Reskrim Polres  X  Provinsi Banten  cenderung mencari 
harapan-harapan yang positif dari keadaan yang menekannya; escape avoidance 
yaitu Anggota Polisi Satuan Reskrim Polres  X  Provinsi Banten  berusaha 
menghindar dari setiap masalah yang muncul dengan makan, minum, merokok, 
menggunakan obat-obatan atau tidur; accepting responsibility yaitu Anggota 
Polisi Satuan Reskrim Polres  X  Provinsi Banten  bertanggung jawab terhadap 
situasi yang tengah dihadapi. 
Coping stress yang berpusat pada masalah berfungsi untuk memecahkan 
masalah. Tingkah laku yang termasuk didalamnya adalah confrontative coping 
yaitu Anggota Polisi Satuan Reskrim Polres  X  Provinsi Banten  mencari cara 
untuk mengatasi keadaan yang menekan dirinya, misalnya polisi yang berusaha 
mencari strategi alternatif untuk dapat memenuhi target yang diembankan 
padanya; planful problem solving yaitu Anggota Polisi Satuan Reskrim Polres 
 X  Provinsi Banten  memikirkan tentang akibat dari keadaan yang menekan 
tersebut dan merencanakan sesuatu untuk mengatasi keadaan, misalnya polisi 
yang terlebih dahulu membuat atau menetapkan daftar daerah yang akan ia 
datangi dan dilakukan operasi penyisiran tindak kriminal. Jika Anggota Polisi 
Satuan Reskrim Polres  X  Provinsi Banten menggunakan coping stress yang 
berpusat pada masalah dan emosi secara bersamaan maka akan muncul 
keseimbangan derajat stress yang dihayati Anggota Polisi Satuan Reskrim Polres 
 X  Provinsi Banten . 
Penjelasan di atas mengenai coping stress dapat dirangkum dengan bagan 
sebagai berikut: 
Provinsi Banten. 
Sumber stress : 
Emotional Focused : 
Problem Focused : 
Faktor-faktor yang memengaruhi : 
 Keterampilan sosial secara adekuat dan 
 Sumber-sumber material 
Provinsi Banten dapat berbentuk frustrasi, konflik, tekanan atau 
ancaman. 
Kriminal Polres  X  Provinsi Banten terhadap stress; Penilaian 
sekunder menentukan coping stress yang akan digunakan. 
adalah kesediaan energi, keterampilan memecahkan masalah, 
keyakinan yang positif, keterampilan sosial secara adekuat dan efektif, 
dukungan sosial, dan sumber-sumber material. 
pada masalah (problem focused coping), berfokus pada kondisi 
emosional (emotional focused coping), atau seimbang 
Menurut Lazarus (1984), stress merupakan bentuk interaksi antara 
individu dengan lingkungannya, yang dinilai oleh individu sebagai sesuatu yang 
membebani atau melampaui kemampuan yang dimilikinya, serta mengancam 
kesejahteraan dirinya. Apabila individu merasakan adanya ketidak seimbangan 
antara tuntutan dengan kemampuan yang dimilikinya, maka stress akan muncul. 
Hal ini menunjukan bahwa stress tidak hanya tergantung pada kondisi eksternal 
saja melainkan juga pada kerawanan konstitusional atau konstitusi tubuh dari 
individu yang bersangkutan, juga pada mekanisme pengolahan kognitif terhadap 
kondisi tersebut. Keadaan yang dimaksud ini adalah segala elemen fisik maupun 
psikososial dari suatu situasi yang harus dihadapi melalui tindakan fisik maupun 
mental oleh individu sebagai upaya untuk menyesuaikan diri. 
Dengan kata lain, stress merupakan fenomena individual dan 
menunjukkan respon individu terhadap lingkungan. Secara terus menerus individu 
akan menilai keadaan dan hambatan yang terdapat di lingkungan serta menilai 
kemampuan dirinya untuk mengatasi tuntutan tersebut. 
Levenstein menilai bahwa berbagai definisi stress terfokus pada dua 
komponen utama yaitu akibat dari kondisi lingkungan dan reaksi individu 
terhadap stress itu sendiri (Fliege et al., 2005). Suatu studi empiris yang berdasar 
pada structural equation modeling techniques menunjukkan bahwa pengalaman 
stress didefinisikan dengan sangat baik bila menggunakan konstruk stress dua 
faktorial. Kedua faktor ini mengombinasikan faktor kondisi lingkungan dengan 
faktor stress appraisal serta respon emosional.  Semakin banyak peneliti yang 
berfokus pada persepsi subjektif individu mengenai stress itu sendiri 
dibandingkan menjelaskan dari faktor lingkungan. 
Tuntutan atau tekanan lingkungan yang mengganggu dan membebani serta 
melampaui batas kemampuan penyesuaian diri individu sehingga menyebabkan 
stress disebut sebagai stressor. 

