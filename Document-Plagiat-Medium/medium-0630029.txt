Alat transportasi menjadi instrumen penting dalam kehidupan manusia. 
Manusia menggunakan alat transportasi untuk berpindah dari satu tempat ke 
tempat lain yang menjadi tujuannya. Selain dapat mengangkut manusia, alat 
transportasi dapat juga dimanfaatkan untuk mengangkut barang terlebih barang 
yang memiliki bobot berat sehingga lebih mudah dipindahkan. Kelebihan lainnya, 
yaitu dapat menghemat tenaga karena dioperasikan dengan menggunakan 
teknologi mesin dan memiliki  efisiensi waktu karena dapat lebih cepat sampai ke 
tujuan. Selain kendaraan umum, mobil maupun motor menjadi alternatif pilihan 
sebagai alat transportasi bagi masyarakat di zaman modern yang memudahkan 
mobilitas mereka. 
 Kendaraan tersebut tak hanya digunakan oleh individu yang bekerja, tetapi 
mahasiswa/i dan siswa/i sekolah pun turut menggunakannya. Pada awalnya 
kebanyakan dari mereka masih menggunakan jasa kendaraan umum, akibat 
beberapa alasan seperti terus melonjaknya harga bahan bakar bensin yang 
berdampak pada semakin mahalnya tarif kendaraan umum, ditambah dengan 
banyaknya tindak kriminal yang terjadi di dalam kendaraan umum membuat 
masyarakat semakin enggan menggunakan kendaraan umum dan lebih memilih 
menggunakan kendaraan pribadi. Hal tersebut didukung dengan maraknya kredit 
murah yang ditawarkan oleh produsen-produsen kendaraan sehingga menarik 
minat masyarakat untuk memiliki kendaraan pribadi, khususnya motor. Motor 
mempunyai harga jual yang relatif lebih murah daripada mobil, ditambah dengan 
penggunaan bahan bakar yang lebih irit serta perawatan yang relatif lebih mudah 
semakin memantapkan masyarakat untuk memilih berkendara dengan 
menggunakan motor. Dengan menggunakan motor, waktu yang digunakan untuk 
mencapai tempat tujuan pun lebih singkat.  
 Seiring dengan maraknya penggunaan kendaraan pribadi, kemacetan di 
kota-kota besar seperti halnya di kota Bandung tidak dapat dihindari. Bandung 
sendiri kini masuk dalam daftar enam kota besar di Indonesia yang kemacetannya 
sangat parah dan berada pada posisi kedua setelah DKI Jakarta. Menurut sumber 
terbaru dari Yudhiana (2014) selaku kepala seksi Manajemen dan Rekayasa Lalu 
Lintas Dishub Kota Bandung menyatakan terdapat sekitar 895 ribuan unit sepeda 
motor atau sekitar 72% dan 282 ribuan unit mobil pribadi atau sekitar 23% dari 
total komposisi kendaraan bermotor yang ada di Kota Bandung. 
 Dari jumlah yang disebutkan di atas, tidak mengherankan jika kita 
menjumpai pada setiap hari di jam-jam sibuk seperti pagi hari, pemandangan jalan 
raya dipadati oleh orang yang berbondong-bondong menggunakan kendaraannya 
untuk berangkat kerja atau mengantar anaknya ke sekolah. Tak jarang hal tersebut 
menimbulkan kemacetan, sehingga pengendara jadi terhambat perjalanannya dan 
tidak dapat sampai ke tempat yang dituju dengan tepat waktu. Pada situasi macet, 
pengendara motor harus ekstra berhati-hati dan meningkatkan konsentrasinya 
demi menjaga kendaraannya agar tetap seimbang. Namun, di lapangan masih 
terlihat beberapa pengendara yang enggan bersabar dan memaksakan motornya 
agar tetap melaju. Tak jarang pula, beberapa pengendara melanggar ketentuan 
cara berkendara yang aman yang telah diatur oleh undang-undang seperti tidak 
mengindahkan rambu-rambu lalu lintas, menjalankan kendaraannya di atas 
trotoar, atau bahkan menyalip kendaraan lain secara sembrono yang sudah tentu 
dapat mengakibatkan kecelakaan dan mengancam keselamatan diri dan orang lain. 
 Kecelakaan lalu lintas menempati peringkat kedelapan penyebab kematian 
di dunia, dan penyebab utama kematian orang muda dengan usia 15 tahun hingga 
29 tahun. Setiap populasi 100.000 orang, terdapat 18 orang yang meninggal 
karena kecelakaan lalu lintas. Demikian laporan Global Status Report on Road 
Safety yang dilansir Badan Kesehatan Dunia (2013). 
Survei tersebut menyebutkan, rata-rata 1,24 juta nyawa melayang setiap tahun 
karena kecelakaan lalu lintas di seluruh dunia. Sementara sekitar 20 juta hingga 
50 juta orang mengalami cedera akibat tabrakan lalu lintas di jalan. 
Indonesia menempati urutan kelima dalam peringkat negara dengan korban tewas 
terbanyak akibat kecelakaan lalu lintas dan faktor kelalaian manusia menduduki 
posisi puncak pemicu kecelakaan. Menurut Kepala Korps Lalu Lintas Polri 
Inspektur Jenderal Pudji Hartanto (2014), telah terjadi 101.037 kecelakaan lalu 
lintas di tahun 2013. Ini berarti, setiap jam terjadi 12 kasus kecelakaan lalu lintas. 
Kecelakaan tersebut menimbulkan 25.157 korban meninggal dunia. Di Indonesia, 
rata-rata tiga orang meninggal setiap jam akibat kecelakaan lalu lintas. Tahun 
2013, kecelakaan lalu lintas menyebabkan 29.347 orang menderita luka berat dan 
113.131 orang luka ringan. Selain itu, kerugian material yang ditimbulkan 
kecelakaan lalu lintas pada 2013 mencapai Rp 254,6 miliar.  
 Mahasiswa/i, khususnya di Universitas  X  Bandung tak luput menjadi 
bagian dari masyarakat yang menggunakan sepeda motor. Universitas  X  sendiri 
terdiri dari 9 fakultas berbeda-beda dan pada setiap tahunnya jumlah mahasiswa/i 
yang terdaftar kian bertambah sehingga dapat dibayangkan pula begitu penuhnya 
motor yang berjajar di area parkir yang baru dapat menampung kurang lebih 2000 
motor. Selain itu, lokasi Universitas  X  Bandung yang berada di tengah kota, 
dekat dengan pintu tol serta ruas jalan yang cukup sempit dan dipadati kendaraan 
bermotor setiap harinya memungkinkan mahasiswa/i Universitas  X  Bandung 
mengalami kemacetan di jalan raya saat menuju ke kampus.  
 Hakikinya, mahasiswa/i Universitas  X  Bandung yang seharusnya sudah 
memiliki kematangan baik dari segi fisik, kemampuan berpikir, dan mengolah 
emosi sehingga mampu mengendalikan situasi dan dirinya ketika berada dalam 
kondisi macet (Santrock, 2002). Akan tetapi, masih dijumpai mahasiswa/i 
khususnya yang mengendarai motor kurang mampu menguasai situasi tersebut. 
Dengan ukuran motor yang lebih kecil dibandingkan mobil, membuat pengendara 
motor memiliki lebih banyak kesempatan melanggar ketertiban berlalu lintas, 
seperti menerobos lampu lalu lintas, menyalip mobil atau motor lain tanpa melihat 
kaca spion, tidak mau mengalah untuk memberikan jalan kepada pengendara lain 
atau kepada pejalan kaki, menggunakan trotoar sebagai perlintasan serta tidak 
memperhitungkan kecepatannya dalam mengendarai motor. Hal tersebut tentunya 
sangat berisiko untuk dilakukan karena dapat membahayakan diri sendiri maupun 
orang lain. 
 Tingkah laku individu saat mengendarai kendaraannya dan melaksanakan 
peraturan lalu-lintas itu ditentukan oleh locus of control. Locus of control sendiri 
menggambarkan posisi serta seberapa kuat kendali yang ada pada diri individu, 
baik yang bersumber dari dalam diri/ internal atau yang berasal dari luar diri/ 
eksternal (Rotter, 1972). Adapun locus of control terbagi menjadi dua berdasarkan 
reinforcement-nya, yaitu locus of control internal dan locus of control eksternal. 
Individu yang memiliki locus of control internal akan berusaha mengoptimalkan 
segala kemampuan yang dimiliki, pemahaman, serta pengalamannya yang 
kemudian diaplikasikan pada saat mengendarai motor di jalan raya supaya tertib, 
aman, dan sampai ke tujuan tepat waktu. Sedangkan individu yang memiliki locus 
of control eksternal bentuk perilakunya patuh terhadap tata tertib berlalu lintas 
hanya semata-mata untuk menghindari sanksi dari polisi dan memiliki anggapan 
bahwa segala sesuatu yang terjadi pada dirinya selama mengendarai motor bukan 
merupakan tanggung jawab dirinya sepenuhnya melainkan lebih ditentukan oleh 
nasib dan keberuntungan. 
 Dari survei awal yang peneliti lakukan terhadap 25 mahasiswa/i 
Universitas  X  Bandung yang mengendarai motor, diketahui bahwa semua 
sampel memiliki alasan mengendarai motor yang sama yaitu jarak tempuh dan 
efisiensi waktu. Semua sampel memberikan penilaian yang sama terhadap kondisi 
jalan dan pengendaranya saat ini yang dinilai kurang disiplin sehingga membuat 
kondisi jalan semrawut. Semua sampel mengakui bahwa mereka mengetahui 
rambu-rambu yang dianggap umum dan penting yang biasa ditemukan di jalan 
raya. Sebanyak 60% sampel menyatakan sering melanggar peraturan lalu lintas, 
terutama terjadi saat situsi jalan sedang sepi atau ketika situasi mendesak seperti 
menghindari hujan dan terlambat sampai ke tujuan. Sebesar 40% sampel 
menyatakan tidak pernah melanggar peraturan lalu lintas, karena dinilai dapat 
membahayakan diri sendiri dan juga orang lain. 
 Dari survei awal di atas, dapat disimpulkan bahwa terdapat perbedaan 
kecenderungan locus of control pada mahasiswa/i Universitas  X  Bandung yang 
mengendarai motor tentang peraturan lalu lintas. Atas dasar itu, peneliti tergugah 
untuk melakukan penelitian lebih lanjut mengenai locus of control pada 
mahasiswa/i Universitas  X  Bandung yang mengendarai motor tentang peraturan 
lalu lintas.  
 Berdasarkan latar belakang yang telah diuraikan di atas, maka dari 
penelitian ini ingin diketahui seperti apakah kecenderungan tipe locus of control 
pada mahasiswa/i Universitas  X  Bandung yang mengendarai motor tentang 
peraturan lalu lintas. 
 Maksud diadakannya penelitian ini adalah untuk mengetahui gambaran 
tipe locus of control pada mahasiswa/i Universitas  X  Bandung yang 
mengendarai motor tentang peraturan lalu lintas. 
 Tujuan diadakannya penelitian ini adalah untuk mengetahui 
kecenderungan tipe locus of control dan faktor-faktor yang memengaruhinya pada 
mahasiswa/i Universitas  X  Bandung yang mengendarai motor tentang peraturan 
lalu lintas. 
1) Memberikan sumbangan informasi bagi ilmu psikologi, khususnya 
psikologi perkembangan dan psikologi sosial yang berkenaan dengan locus 
of control. 
2) Menjadi referensi dan memberikan masukan bagi peneliti lain yang 
berminat melakukan penelitian lanjutan mengenai locus of control. 
1) Memberikan informasi kepada mahasiswa/i tentang peranan locus of 
control khususnya pada saat sedang mengendarai kendaraan, agar 
mahasiswa/i lebih mengenali pengendalian dirinya.  
2) Memberikan sumbangan bagi polisi agar dapat menciptakan sebuah aturan 
yang tegas dan memberikan efek jera sehingga dapat meningkatkan 
ketertiban berlalu lintas. 
 Mahasiswa/i berada pada tahap perkembangan dewasa awal. Pada tahapan 
ini mahasiswa/i mengalami puncak kematangan baik dari segi fisik, kognitif, 
maupun sosioemosional. Kondisi fisik yang baik sangat memungkinkan 
mahasiswa/i untuk mengendarai kendaraannya sendiri secara baik. Dari segi 
kognitif, kemampuan mahasiswa/i jauh lebih kompleks dan terlatih untuk 
menyelesaikan persoalan-persoalan sulit sehingga diharapkan mahasiswa/i 
mampu memecahkan setiap permasalahan yang dihadapi ketika sedang 
menjalankan kendaraannya di jalan raya, misalnya menemukan jalan alternatif 
untuk menghindari kemacetan atau memperbaiki kendaraan yang rusak. Dari segi 
sosioemosional, mahasiswa/i diharapkan mampu mengolah emosinya dan mampu 
mengkomunikasikannya secara baik kepada orang lain. Hal tersebut dapat 
diterapkan oleh mahasiswa/i ketika mengendarai kendaraannya supaya aman dan 
tertib, misalnya tidak cepat marah ketika bersinggungan dengan pengendara lain. 
 Tingkah laku yang ditampilkan oleh mahasiswa/i pada saat mengendarai 
kendaraannya di jalan raya itu ditentukan oleh locus of control. Locus of control 
menggambarkan posisi serta seberapa kuat kendali mahasiswa/i untuk mematuhi 
peraturan lalu lintas, baik bersumber dari dalam diri yang disebut locus of control 
internal maupun berasal dari luar diri yang disebut locus of control eksternal 
(Rotter, 1972). Pada saat memunculkan tingkah laku tertentu, biasanya 
mahasiswa/i mengharapkan adanya penguatan atau reinforcement yang mengikuti 
tingkah laku tersebut dan nantinya akan mengarahkan tingkah laku itu pada saat 
mengalami situasi yang sama. Misalnya, mahasiswa/i dengan locus of control 
internal mempunyai harapan bahwa dengan ia mematuhi peraturan lalu lintas, 
maka ia akan selamat sampai ke tujuan tanpa harus membahayakan diri sendiri 
maupun orang lain. Sedangkan mahasiswa/i dengan locus of control eksternal bisa 
saja berharap dengan ia mematuhi peraturan lalu lintas, maka ia tidak akan terkena 
tilang. Dalam hal ini, sebenarnya keduanya berada pada situasi yang sama yaitu 
mematuhi peraturan lalu lintas, namun berbeda reinforcement-nya. Pada 
mahasiswa/i dengan locus of control internal yang menjadi reinforcement-nya 
adalah keselamatan yang didapatkan apabila mematuhi peraturan lalu lintas, 
sedangkan pada mahasiswa/i dengan locus of control eksternal yang menjadi 
reinforcement-nya adalah menghindari tilang atau hukuman bila tidak mematuhi 
peraturan lalu lintas.  
 Adapun faktor-faktor yang memengaruhi locus of control yaitu usia, 
pengalaman dalam lembaga, latihan dan pengalaman, serta keberhasilan 
mengatasi peristiwa. Usia seseorang dapat menentukan kematangan berpikir 
termasuk cara dalam pengambilan keputusannya. Semakin usia bertambah, 
diharapkan semakin internal locus of control-nya. Hal itu seharusnya tercermin 
pada mahasiswa/i yang sudah dianggap dewasa oleh keluarga maupun negara, 
karena itu juga individu yang telah berusia 17 tahun ke atas diberikan kepercayaan 
untuk memiliki SIM (Surat Izin Mengemudi). Diharapkan setelahnya mahasiswa/i 
yang sarat akan pengalaman dan telah cukup berinteraksi dengan lingkungan di 
luar dirinya, mampu meningkatkan locus of control internalnya, sehingga perilaku 
yang ditampilkan saat sedang berkendara merupakan suatu sikap dari keputusan 
yang dipilih secara tepat, serta tidak merugikan atau membahayakan diri sendiri 
maupun orang lain.  
 Mahasiswa/i yang memiliki pengalaman pernah tinggal dalam suatu 
lembaga, seperti panti asuhan, penjara, rehabilitasi/pengobatan, asrama, dan lain-
lain dipercayai bahwa lembaga-lembaga tersebut telah menerapkan disiplin tinggi 
terhadap aturan bagi yang menghuninya, sehingga diharapkan mahasiswa/i yang 
 berangkat  dari lembaga tersebut membawa kedisiplinan itu dan mampu 
mengaplikasikannya pada kehidupan sehari-hari termasuk mengendarai 
kendaraannya secara tertib. Hal tersebut menjadikan locus of control-nya semakin 
eksternal. 
 Latihan dan pengalaman mempengaruhi locus of control, misalnya 
mahasiswa/i yang pernah mengalami tilang menyadari bahwa atas kejadian itu 
mahasiswa merasa dirugikan. Di antaranya ialah perjalanan mahasiswa menjadi 
tertunda sehingga tidak dapat sampai ke tempat yang dituju dengan tepat waktu, 
selain menguras tenaga juga pikiran ketika berhadapan dengan polisi, bahkan 
beberapa mahasiswa dengan terpaksa mengeluarkan sejumlah uang dengan 
maksud menempuh  jalan damai  untuk menghindari persidangan. Jadi 
mahasiswa belajar agar tidak mengalami pengalaman serupa dan enggan 
menanggung resiko karena banyak yang dikorbankan. Oleh karena itu, locus of 
control internalnya menjadi meningkat. 
 Keselamatan yang didapatkan oleh mahasiswa/i setiap kali mengendarai 
motor secara tertib dapat dijadikan  efek terapi  sebagai keberhasilan menghadapi 
peristiwa dan akan terus diulangi oleh mahasiswa/i tersebut karena perilaku itu 
memberikan semacam reward yang diharapkan seperti tiba ke tempat yang dituju 
tanpa mendapat masalah. Karenanya meningkatkan locus of control internalnya. 
Faktor-faktor yang memengaruhi locus of control tersebut sifatnya subjektif 
sehingga hasilnya akan berbeda pada setiap individu, tergantung pada 
penghayatannya masing-masing. 
 Mahasiswa/i dengan kecenderungan locus of control internal lebih 
didominasi oleh faktor-faktor yang ada di dalam dirinya, usaha yang dikerahkan 
untuk mematuhi peraturan lalu lintas menjadi lebih besar, mahasiswa/i bersedia 
mengandalkan kemampuannya untuk mematuhi peraturan lalu lintas, dan 
memandang peraturan itu sesuai dengan karakteristik dirinya, mahasiswa/i juga 
mau belajar dari pengalaman ketika melakukan kesalahan/pelanggaran lalu lintas 
karena pada dasarnya mahasiswa/i ini mempersepsikan aturan itu memiliki tujuan 
yang baik. Dengan demikian perilaku yang akan dimunculkan oleh mahasiswa/i 
dengan locus of control internal yaitu bertanggung jawab, mampu mengendalikan 
diri, dan sadar akan konsekuensi atas setiap tingkah lakunya di jalan raya saat 
sedang mengendarai motor. 
 Mahasiswa/i dengan locus of control eksternal lebih didominasi oleh 
faktor-faktor yang berasal dari luar dirinya, mahasiswa/i terlalu percaya bahwa 
kejadian apa saja yang dialaminya saat mengendarai kendaraannya di jalan raya 
seperti ditilang, kecelakaan, atau sampai ke tujuan tanpa adanya masalah semata-
mata merupakan nasib dan keberuntungan/ketidakberuntungan. Selain itu, 
perilaku patuh dan tidak patuh terhadap peraturan lalu lintas tergantung pada ada 
atau tidak adanya pengaruh orang lain atas dirinya, misalnya mahasiswa/i baru 
akan mengenakan helm saat diadakannya razia.. Sehingga perilaku yang mungkin 
muncul pada mahasiswa/i dengan locus of control eksternal yaitu perilaku 
patuh/tidak patuh tergantung pada otoritas seperti polisi, mahasiswa/i kesulitan 
untuk mengendalikan diri dalam mematuhi peraturan lali lintas karena 
ketidaksesuaian dengan karakter dirinya, dan mahasiswa/i ini tidak menyadari 
konsekuensi dari tingkah lakunya sehingga tampak sembrono dalam 
mengemudikan kendaraannya. 
dewasa awal dimana fisik/motorik, 
kognitif, dan sosioemosional 
Mahasiswa/i universitas  X  Bandung 
Faktor yang memengaruhi : 
  Latihan & Pengalaman 
Aspek Locus of Control :  
  Tingkah laku mahasiswa/i pada saat mengendarai motor di jalan raya 
dipengaruhi locus of control. 
  Mahasiswa Universitas  X  Bandung yang mengendarai motor tentang 
peraturan lalu lintas mempunyai 2 tipe locus of control yaitu locus of 
control internal dan locus of control eksternal. 
  Aspek yang membentuk locus of control internal mahasiswa Universitas 
 X  Bandung yang mengendarai motor tentang peraturan lalu lintas adalah 
usaha dan kemampuan diri. 
  Aspek yang membentuk locus of control eksternal mahasiswa Universitas 
 X  Bandung yang mengendarai motor tentang peraturan lalu lintas adalah 
nasib atau keberuntungan dan pengaruh orang lain atas dirinya. 
  Faktor yang memengaruhi locus of control mahasiswa Universitas  X  
Bandung yang mengendarai motor tentang peraturan lalu lintas adalah 
usia, pengalaman di lembaga, latihan dan pengalaman, dan efek terapi. 
 Locus of control menggambarkan posisi serta seberapa kuat kendali yang 
ada pada diri individu, baik yang bersumber dari dalam diri/internal atau yang 
berasal dari luar diri/eksternal (Rotter, 1972). Peran locus of control yaitu sebagai 
pusat kendali dan pusat pengarahan perilaku individu. Rotter lebih menekankan 
pada proses kognitif, khususnya persepsi yang mengarahkan dan mengendalikan 
tingkah lakunya. 
 Dalam kehidupan sehari-hari, biasanya kita mempunyai harapan subjektif 
saat memunculkan tingkah laku tertentu, artinya kita mengharapkan adanya 
reinforcement yang mengikuti tingkah laku tersebut. Suatu tingkah laku akan 
menghasilkan reinforcement tertentu pula yang nantinya akan mengarahkan 
tingkah laku kita saat mengalami situasi yang sama. Pada setiap individu derajat 
kepentingan untuk setiap reinforcemet berbeda-beda dan tidak akan bernilai sama 
untuk setiap individu. Menurut Rotter (1972), tingkah laku yang muncul 
merupakan fungsi dari harapan terhadap tujuan tertentu dan nilai reinforcement 
dari goal tersebut. Jadi munculnya tingkah laku tertentu menurutnya bergantung 
dari bagaimana derajat kepentingan yang individu tetapkan terhadap hasil perilaku 
tersebut, serta bagaimana keyakinannya bahwa perilaku tersebut akan 
memberikan hasil yang diinginkan. 
Variabel harapan di sini, menurut Rotter (1972) merupakan fungsi dari 
harapan (yang disadari oleh pengalaman sebelumnya dalam memperoleh 
reinforcement) terhadap respon tertentu yang diarahkan oleh rangsang situasi 
tertentu dengan harapan yang digeneralisasi (berdasarkan kaitan rangkaian 
tingkah lalu dengan reinforcement lain). Sehubungan dengan rumusannya 
mengenai harapan yang digeneralisasi dan harapan yang spesifik tersebut, Rotter 
melakukan penelitian mengenai efek dari tugas-tugas yang berkaitan dengan 
keberuntungan. Pada situasi yang berkaitan dengan nasib dan keberuntungan, 
reward yang diperoleh bersumber dari faktor-faktor eksternal. Oleh karena itu, 
kemungkinan untuk memperoleh reward tidak dipengaruhi oleh respon 
individunya.  
Di lain pihak, pada situasi yang berkaitan dengan skill, reinforcement yang 
diperoleh bergantung pada tingkah laku individunya. Dalam hal ini reward 
ditentukan oleh faktor-faktor internal. Dengan demikian perolehan reinforcement 
pada situasi yang dirasakan ditentukan oleh nasib akan menghasilkan perubahan 
dalam harapan individu, yang berbeda dengan perubahan yang terjadi pada situasi 
yang dirasakan ditentukan oleh skill. Menurut Rotter (1972), setiap individu 
mempunyai perbedaan dalam memandang suatu kejadian sebagai reward dan 
punishment, perbedaan ini juga terdapat dalam tindakannya. Salah satu penentu 
tindakan tersebut adalah derajat yang dirasakan individu bahwa reinforcement 
yang diperolehnya mengikuti atau bergantung pada atribut atau perilaku dirinya, 
serta derajat yang dirasakan bahwa reinforcement yang diperoleh dikendalikan 
oleh kekuatan di luar dirinya dan tidak bergantung pada perilakunya. 
 Pengaruh reinforcement mengikuti perilaku kehidupan manusia. Namun 
proses tersebut tidak sederhana, tergantung dari ada atau tidaknya persepsi 
individu mengenai hubungan tingkah laku dengan reinforcement yang 
diperolehnya. Jika reinforcement yang dirasakan mengikuti beberapa tindakan 
dirinya atau dinilai tidak sepenuhnya bergantung pada perilakunya, hal semacam 
itu khas disebut sebagai hasil dari keberuntungan dan nasib, di bawah kendali 
orang lain yang berpengaruh atau sebagai sesuatu yang tidak dapat diramalkan 
karena adanya kerumitan-kerumitan dari kekuatan-kekuatan di sekitarnya. Jika 
suatu kejadian dipersepsi oleh individu dengan cara demikian, Rotter 
menyebutnya sebagai keyakinan dalam kendali eksternal. Jika suatu kejadian 
dirasakan bergantung pada perilaku atau karakteristik permanen dalam dirinya, ia 
menyebutnya dalam kendali internal. Kendali internal-eksternal ini berkaitan 
dengan ada atau tidaknya individu merasakan bahwa dirinya memiliki penguasaan 
atau kehilangan pengusaan atas kejadian yang dialaminya.  
Selanjutnya, oleh Rotter (1972) hal itu dijadikan dasar bagi tipologinya 
yang membagi manusia ke dalam 2 tipe umum. Ditinjau dari sumber 
reinforcement-nya, Rotter mengemukakan adanya perbedaan mendasar dari 
penghayatan subyektif seseorang terhadap sumber pengolahan reinforcement 
tersebut. Ada yang menganggap sumber perolehan reinforcement-nya berasal dari 
dalam diri, Rotter menyebutnya sebagai individu dengan locus of control internal. 
Ada pula yang menganggap bahwa sumber perolehan reinforcement-nya berasal 
dari luar diri, Rotter menyebutnya sebagai individu dengan locus of control 
eksternal. Penghayatan tersebut dimiliki individu berdasarkan pengalaman-
pengalaman sebelumnya. 
 Individu dengan locus of control internal meyakini bahwa kehidupannya, 
hasil kerja, dan karirnya ditentukan oleh faktor-faktor internal seperti usaha dan 
kemampuan dirinya sendiri serta karakteristik-karakteristik lain di dalam dirinya. 
Sedangkan individu dengan locus of control eksternal merasa bahwa apa yang 
terjadi pada dirinya diyakini bersumber dari hal-hal di luar dirinya seperti nasib, 
keberuntungan atau karena tindakan-tindakan orang lain. 
 Pada dasarnya locus of control seseorang menggambarkan letak dan 
kekuatan kendali yang ada dalam diri seseorang, bersumber dari dalam atau dari 
luar diri yang menjadi dasar pembentukan serta penampilan tingkah lakunya. 
Dengan demikian, locus of control juga berperan sebagai pusat kendali dan pusat 
pengarahan diri dari setiap perilakunya. 
 Ketika seorang anak berinteraksi dengan lingkungan sekitarnya, ia belajar 
sesuatu tentang kedudukannya dalam suatu sistem dan motif kompetisinya 
menjadi terbentuk melalui agen sosialisasi (Orang tua). Kejadian ini menimbulkan 
hasil perkembangan yang berbeda-beda. Mula-mula anak belajar berjalan, 
kemudian ia mampu mengendalikan pembuangan air (Toilet training) dan 
kemudian dengan cepat menguasai bahasa. Kejadian di atas disertai dengan 
eksplorasi lingkungan dan manipulasi lingkungan. 

