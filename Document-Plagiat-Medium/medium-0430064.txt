Distribusi merupakan kegiatan yang langsung berhubungan dengan 
konsumen. Dalam distribusi, sangat diperlukan seorang salesman untuk 
memasarkan produk produk perusahaan dan mengirimkan barang kepada 
pelanggan. Perusahaan menyediakan barang, sementara salesman bertugas untuk 
memasarkan serta mengirimkan barang tersebut kepada pelanggan/konsumen. 
Salah satu perusahaan distribusi tersebut adalah PT  X . 
PT  X  adalah perusahaan yang bergerak dalam bidang distribusi produk-
produk farmasi, dimana perusahaan ini menawarkan produknya ke apotek dan 
toko obat. PT  X  memiliki kelengkapan dari segi produk, memberikan diskon 
yang tinggi untuk beberapa produk tertentu. Selain itu juga, PT  X  memiliki 
komitmen untuk menginformasikan kepada pelanggan berkaitan dengan 
ketersediaan stok barang serta adanya perubahan harga-harga barang. Pelanggan 
yang mencapai target pembelian tertentu akan mendapatkan bonus berupa barang, 
yang diberikan secara langsung oleh supervisor perusahaan. Dengan adanya 
fasilitas-fasilitas yang dimiliki oleh perusahaan tersebut, diharapkan dapat 
mendorong salesman PT  X  untuk bekerja sebaik mungkin, sehingga dapat 
memaksimalkan potensi kerja mereka. PT  X   memiliki 500 pelanggan di 
seluruh wilayah Jakarta serta 50 orang karyawan dimana 30 orang diantaranya 
adalah salesman. 
Visi dan misi dari PT  X  adalah  Panca Krida , visi dan misi ini 
mengandung pengertian kesetiaan, kedisiplinan, kejujuran/tanggungjawab, 
kemauan/tekad, dan keterampilan. Para salesman yang bekerja di PT  X  ini 
dituntut untuk dapat menerapkan visi dan misi tersebut, dalam mengutamakan 
kualitas pelayanan kepada pelanggan. Dalam melaksanakan tugasnya, seorang 
salesman harus berpegang pada standar uraian pekerjaan yang telah ditetapkan 
oleh perusahaan. Salesman PT  X  dituntut untuk melakukan pengiriman barang 
tepat waktu yang mencakup memeriksa secara rutin jadwal pengiriman barang, 
mengirimkan barang sesuai dengan janji yang telah ditentukan (ada dua shift 
dalam sehari yaitu dari jam 10-12 siang dan jam 2-5 sore), memeriksa barang 
sebelum dikirim sehingga tidak terjadi kesalahan pesanan barang pelanggan, 
menjaga barang agar tetap dalam keadaan baik. Untuk dapat melakukan 
pengiriman barang tepat waktu, maka salesman harus memiliki usaha dan 
dorongan sehingga memunculkan niat (intention) dalam melakukan kegiatan 
tersebut. Adapun teori yang membahas tentang intention adalah Theory of 
Planned Behavior (Icek Ajzen, 2005). 
Intention ini dicetuskan oleh  Icek ajzen (1991), dalam teori Planned 
Behavior, yang menyatakan setiap perilaku manusia ditentukan oleh seberapa kuat 
usaha salesman PT  X  Jakarta dalam melakukan pengiriman barang tepat waktu. 
Intention dipengaruhi oleh tiga determinan, yaitu sikap favourable dan 
unfavourable  yang dimiliki salesman dalam melakukan pengiriman barang tepat 
waktu (attitude toward the behavior), persepsi salesman mengenai atasan, 
supervisor, rekan kerja dan keluarga mendukung atau tidak mendukung mereka 
dalam mengirimkan barang tepat waktu untuk mematuhi orang-orang tersebut 
(subjective norms), persepsi salesman mengenai kemampuan mereka dalam 
melakukan pengiriman barang tepat waktu (perceived behavioral control). 
Terdapat beberapa faktor yang berpengaruh terhadap intention salesman dalam 
melakukan pengiriman barang tepat waktu, yakni faktor internal dan faktor 
eksternal. Faktor internal mencakup sifat dalam diri seseorang, stabilitas emosi, 
dan penghayatan seorang salesman dalam melakukan pengiriman barang secara 
tepat waktu. Faktor eksternal mencakup informasi yang dimiliki salesman 
mengenai pengiriman barang, dukungan sosial, gaji dan fasilitas dari perusahaan. 
Berdasarkan data tahun 2007 dari PT  X  terhadap 200 orang pelanggan, 
sebanyak 134 (67%) pelanggan mengeluhkan pengiriman barang yang dilakukan 
oleh salesman PT  X  yakni ada beberapa ketidaksesuaian waktu penerimaan 
barang dari yang telah dijanjikan atau barang yang terima pelanggan tepat waktu 
namun kondisi beberapa barang rusak atau tidak lengkap. Sementara itu, hanya 66 
(33%) pelanggan yang lain menganggap bahwa pengiriman barang yang 
dilakukan pada salesman di PT  X  tepat waktu dan mereka menilai para 
salesman tersebut berupaya memperhatikan kepuasan pelanggan.  
Berdasarkan wawancara terhadap 10 orang salesman di PT  X , 
ditemukan bahwa masing-masing dari mereka sebenarnya mampu melakukan 
pengiriman barang tepat waktu, namun seringkali terdapat hambatan-hambatan 
yang berasal dari faktor lingkungan diantaranya kondisi lalu lintas yang macet 
sehingga memperlambat pengiriman barang, kondisi kendaraan yang kurang 
bagus, kondisi cuaca yang kurang mendukung seperti hujan. Hal-hal tersebut, 
menjadikan salesman tidak dapat memaksimalkan kemampuannya untuk 
mengirimkan barang tepat waktu. 
Dari 10 orang salesman yang diwawancara, sebanyak 70% dari mereka 
memiliki sikap yang favourable dalam melakukan pengiriman barang tepat waktu. 
Mereka melihat bahwa dengan mengirimkan barang tepat waktu akan 
mendatangkan beberapa konsekuensi yang positif (attitude toward the behavior 
positif), yakni mendapatkan pujian dari atasan, memperoleh bonus sebagai bentuk 
penghargaan dari perusahaan sehingga dapat menambah penghasilan mereka. 
Dengan adanya konsekuensi-konsekuensi positif tersebut, sikap salesman semakin 
favourable (menyukai) dalam melakukan pengiriman barang tepat waktu, 
sehingga akan menguatkan intention salesman dalam melakukan kegiatan 
tersebut.  
Sebanyak 30% dari salesman memiliki sikap yang unfavourable dalam 
melakukan pengiriman barang tepat waktu karena menurut mereka untuk 
melakukan kegiatan tersebut membutuhkan waktu yang banyak yakni dibutuhkan 
waktu untuk memeriksa barang-barang yang akan dikirim, mempersiapkan 
terlebih dahulu barang pesanan pelanggan sebelum mengirimkannya, dimana 
kegiatan tersebut membuat waktu istirahat mereka menjadi berkurang. Hal ini 
menunjukkan sikap mereka yang negatif terhadap pengiriman barang tepat waktu 
(attitude toward the behavior negatif). Sebesar 10% dari mereka merasa enggan 
untuk mempersiapkan barang, dan  20% yang lainnya merasa enggan memeriksa 
kembali barang yang akan dikirim. Sikap salesman yang demikian akan 
melemahkan intention salesman dalam melakukan pengiriman barang tepat 
waktu. 
Sebanyak 30% dari salesman mempersepsi bahwa keluarga, atasan, 
supervisor dan rekan kerja mendukung mereka dalam melakukan pengiriman 
barang tepat waktu dan mereka bersedia untuk mematuhi orang-orang tersebut 
(subjective norms positif). Dari 30% salesman, sebanyak 20% salesman merasa 
bahwa keluarga, atasan, supervisor dan rekan kerja mendukung mereka dalam 
mengingatkan jadwal-jadwal pengiriman barang, 10% salesman lain merasa 
atasan, supervisor, rekan kerja dan keluarga, mendukung mereka dalam 
memberikan motivasi dan arahan ketika mereka mengalami masalah dalam 
melakukan pengiriman barang. Dukungan yang dipersepsi oleh salesman ini akan 
menguatkan intention mereka dalam melakukan pengiriman barang tepat waktu.  
Sebanyak 70% salesman mempersepsi bahwa keluarga, atasan, supervisor 
dan rekan kerja tidak mendukung mereka dalam melakukan pengiriman barang 
tepat waktu yaitu jarang mengingatkan mereka untuk memeriksa jadwal 
pengiriman barang ke pelanggan (subjective norms negatif). Hal ini menjadikan 
salesman mengirimkan barang tidak tepat waktu, sehingga intention salesman 
menjadi lemah dalam melakukan pengiriman barang tepat waktu. 
Sebanyak 80% dari salesman mempersepsi bahwa mereka merasa mampu  
dalam melakukan pengiriman barang tepat waktu, karena mereka merasa dirinya 
cukup cekatan dalam bekerja serta memiliki rasa tanggung jawab terhadap barang 
yang telah dipercayakan kepadanya. Berdasarkan teori planned behavior, hal ini 
menunjukkan adanya perceived behavioral control yang positif dari seorang 
salesman. Persepsi salesman akan kemampuan yang dimiliki untuk melakukan 
pengiriman barang tepat waktu, akan menguatkan intention salesman dalam 
melakukan kegiatan tersebut.   
Sebanyak 20% salesman yang lain mengatakan bahwa mereka merasa 
kurang mampu untuk melakukan pengiriman barang tepat waktu, karena kondisi 
fisiknya yang kurang menunjang seperti mudah lelah, kondisi tubuh yang lemah. 
Selain itu, beberapa salesman merasa kurang mampu karena pihak perusahaan 
kurang memberikan fasilitas berupa penyediaan kendaraan bermotor serta 
communicator. Salesman mempersepsi bahwa hal tersebut sulit untuk dilakukan 
(perceived behavioral control negatif). Hal ini melemahkan intention salesman  
dalam melakukan pengiriman barang tepat waktu. 
Sebanyak 30% mengatakan bahwa mereka mempersepsi dirinya mampu 
melakukan pengiriman barang tepat waktu dan mereka kurang mendapat 
dukungan dari orang-orang yang signifikan bagi mereka. Dimana orang-orang 
yang dianggap signifikan ini jarang mengingatkan mereka untuk memeriksa 
jadwal pengiriman barang. Dengan kemampuan dan kurangnya dukungan dari 
orang-orang yang dianggap penting, ternyata mereka tetap mengirimkan barang 
tersebut tepat waktu. Hal ini menunjukkan bahwa meskipun mereka tidak 
mendapatkan dukungan, mereka tetap memiliki intention yang kuat dalam 
melakukan pengiriman barang tepat waktu. 
Sebanyak 10% mengatakan bahwa mereka mempersepsi dirinya kurang 
mampu melakukan pengiriman barang tepat waktu, namun mereka merasa 
mendapat dukungan dari orang-orang yang signifikan baginya, walaupun 
demikian mereka mengatakan tetap berusaha melakukan kegiatan tersebut dengan 
sebaik-baiknya. Hal ini menunjukkan bahwa meskipun mereka merasa kurang 
mampu (perceived behavioral control negatif), mereka tetap memiliki intention 
yang kuat dalam melakukan pengiriman barang tepat waktu. 
Sementara itu, sebanyak 30% salesman mempersepsi dirinya mampu 
melakukan pengiriman barang tepat waktu dan mereka memiliki sikap 
unfavourable terhadap pengiriman barang secara tepat waktu karena mereka 
mempersepsi bahwa kegiatan tersebut menjadikan waktu istirahat mereka 
berkurang. Namun demikian mereka tetap mengirimkan barang hanya saja barang 
tersebut sampai ke pelanggan tidak sesuai dengan waktu yang dijanjikan. Hal ini 
menunjukkan bahwa salesman merasa mampu untuk melakukan pengiriman 
barang tepat waktu (perceived behavioral contol positif), namun sikap mereka 
yang unfavourable terhadap kegiatan pengiriman barang tepat waktu (attitude 
toward the behavior negatif), ternyata melemahkan intention salesman dalam 
melakukan pengiriman barang tepat waktu. 
Berdasarkan uraian di atas maka peneliti tertarik untuk mengetahui 
bagaimana kontribusi determinan   determinan dari Planned Behavior terhadap 
Intention dalam melakukan pengiriman barang tepat waktu pada Salesman PT 
 X  Jakarta. 
 Masalah dalam penelitian ini adalah bagaimana kontribusi determinan   
determinan dari Planned Behavior terhadap intention salesman PT  X  Jakarta 
dalam melakukan pengiriman barang tepat waktu. 
Penelitian ini dimaksudkan untuk memperoleh gambaran mengenai 
kontribusi determinan   determinan dari Planned Behavior terhadap intention 
dalam melakukan pengiriman barang tepat waktu pada salesman PT  X  Jakarta 
Untuk mengetahui derajat intention, kontribusi determinan-determinan 
terhadap intention, korelasi antara ketiga determinan intention dalam melakukan 
pengiriman barang tepat waktu, serta faktor-faktor yang mempengaruhinya 
ditinjau melalui teori Planned Behavior pada salesman PT  X  Jakarta. 
1. 4. 1 Kegunaan Teoretis 
  Menambah informasi mengenai gambaran kontribusi determinan   
determinan  terhadap intention dari teori planned behavior kepada peneliti 
lain, khususnya dalam bidang kajian Psikologi Industri dan Organisasi.  
  Memberikan informasi untuk penelitian lebih lanjut mengenai pengiriman  
barang tepat waktu ditinjau melalui teori  Planned Behavior . 
1. 4. 2 Kegunaan Praktis 
  Memberikan informasi kepada Supervisor PT  X  mengenai intention dan 
determinan-determinan yang dimiliki oleh salesman dalam melakukan 
pengiriman barang tepat waktu serta memberikan gambaran determinan 
yang paling berkontribusi terhadap intention. Informasi tersebut dapat 
digunakan oleh Supervisor, salah satunya adalah untuk menentukan cara 
memotivasi salesman agar memiliki intention yang kuat dalam melakukan 
pengiriman barang tepat waktu. 
  Memberikan informasi kepada salesman PT  X  mengenai kontribusi dan 
determinan terhadap intention dalam melakukan pengiriman barang tepat 
waktu dan faktor-faktor yang berpengaruh terhadap intention agar mereka 
dapat mempertahankan dan meningkatkan intention mereka dalam 
melakukan kegiatan tersebut. 
 PT  X  merupakan salah satu perusahaan yang bergerak dalam bidang 
farmasi atau obat-obatan. PT  X  menetapkan beberapa uraian tugas salesman 
dalam pengiriman barang yaitu memeriksa secara rutin jadwal pengiriman barang, 
mengirimkan barang sesuai dengan janji yang telah ditentukan (ada dua shift 
dalam sehari yaitu dari jam 10-12 siang dan jam 2-5 sore), memeriksa barang 
sebelum dikirim sehingga tidak terjadi kesalahan pesanan barang pelanggan, serta 
menjaga barang agar sampai ke pelanggan dalam keadaan baik. 
Menurut Icek Ajzen (2005), individu berperilaku berdasarkan akal sehat 
dan selalu mempertimbangkan dampak dari perilakunya. Hal ini yang membuat 
seseorang memiliki niat untuk melakukan atau tidak melakukan suatu tindakan. 
Dalam teori planned behavior, intention adalah suatu keputusan mengerahkan 
usaha untuk menampilkan perilaku. Terdapat tiga determinan yang mempengaruhi 
kemunculan suatu intention. Ketiga determinan tersebut adalah attitude toward 
the behavior, subjective norms dan perceived behavioral control. 
Determinan pertama yakni, attitude toward the behavior merupakan suatu 
sikap favourable atau unfavorable terhadap evaluasi positif atau negatif individu 
dalam menampilkan suatu perilaku. Attitude toward the behavior didasari oleh 
behavioral belief, yaitu keyakinan mengenai evaluasi dari konsekuensi 
menampilkan suatu perilaku. Jika salesman memiliki keyakinan mengenai adanya 
konsekuensi positif dalam melakukan pengiriman barang tepat waktu seperti 
mendapatkan penghargaan dari atasan berupa bonus, dipuji oleh atasan karena 
kinerja mereka yang memuaskan, maka salesman akan memiliki sikap favorable 
dalam melakukan pengiriman barang tepat waktu (attitude toward the behavior) 
sehingga akan menguatkan intention salesman dalam kegiatan tersebut. Jika 
salesman memiliki keyakinan bahwa dengan menampilkan perilaku dalam 
pengiriman barang tepat waktu memiliki konsekuensi negatif yaitu mereka harus 
mengirimkan barang tepat waktu meskipun kondisi cuaca yang kurang 
mendukung maka salesman akan memiliki sikap unfavorable dalam melakukan 
pengiriman barang tepat waktu (attitude toward the behavior) sehingga akan 
melemahkan intention salesman untuk melakukan kegiatan tersebut. 
 Banyaknya Informasi mengenai pengiriman barang tepat waktu yakni 
lokasi pengiriman barang, kondisi jalanan, keuntungan dari melakukan kegiatan 
pengiriman barang yang dimiliki oleh salesman PT  X  Jakarta mempengaruhi 
sikap salesman menjadi semakin favourable (menyukai) untuk melakukan 
pengiriman barang tepat waktu. Begitu juga sebaliknya, apabila informasi yang 
dimiliki oleh salesman PT  X  dalam melakukan pengiriman barang sedikit, 
maka sikap salesman menjadi unfavourable (tidak menyukai) untuk melakukan 
pengiriman barang tepat waktu (Icek Ajzen, 2005) 
Determinan kedua yakni, Subjective norms merupakan persepsi individu 
mengenai tuntutan dari orang-orang yang signifikan untuk menampilkan atau 
tidak menampilkan suatu perilaku dan kesediaan untuk mematuhi orang-orang 
tersebut. Subjective norms didasari oleh normative belief, yaitu keyakinan 
seseorang bahwa individu atau kelompok yang penting baginya akan menyetujui 
atau tidak menyetujui penampilan suatu perilaku dan kesediaan individu untuk 
mematuhi orang-orang yang signifikan tersebut. Tuntutan yang dipersepsi 
salesman ini dapat berasal dari teguran ataupun peringatan dari keluarga, 
atasan, supervisor dan rekan kerja mereka untuk bekerja secara jujur dan bekerja 
sesuai target yang telah ditentukan. Jika salesman mempersepsi bahwa keluarga, 
atasan, supervisor, dan rekan kerja mendukung salesman untuk menampilkan 
perilaku dalam melakukan pengiriman  barang tepat waktu dan bersedia mematuhi 
orang-orang tersebut (subjective norms), maka akan menguatkan intention 
salesman untuk menampilkan perilaku mengirimkan barang tepat waktu. Jika 
salesman mempersepsi bahwa keluarga, atasan, supervisor, dan rekan kerja tidak 
mendukung salesman untuk melakukan pengiriman barang tepat waktu yakni 
jarang mengingatkan mereka untuk memeriksa jadwal pengiriman barang dan 
mereka bersedia mematuhi tuntutan orang-orang tersebut, maka akan melemahkan 
intention salesman dalam melakukan kegiatan tersebut. 
Determinan ketiga yakni, Perceived behavioral control merupakan 
persepsi individu mengenai kemampuan mereka untuk menampilkan suatu 
perilaku. Perceived behavioral control didasari oleh control belief, yaitu 
keyakinan mengenai ada atau tidak adanya faktor-faktor yang mendukung atau 
menghambat dalam menampilkan suatu perilaku. Jika salesman mempersepsi 
dirinya mampu untuk melakukan pengiriman barang tepat waktu (perceived 
behavior control) dan ditunjang oleh faktor-faktor yang mendukung, seperti sifat 
dari dalam diri yakni bertanggungjawab dan jujur, serta fasilitas dari perusahaan 
seperti kendaraan bermotor, maka akan menguatkan intention salesman dalam 
melakukan  kegiatan tersebut. Jika salesman mempersepsi dirinya tidak mampu 
dalam melakukan pengiriman barang tepat waktu disertai adanya berbagai faktor-
faktor yang menghambat, seperti kurang mampu mengendalikan emosi, 
kurangnya fasilitas dari perusahaan maka akan melemahkan intention salesman 
dalam melakukan kegiatan tersebut. 
Interaksi antara attitude toward behavior, subjective norm, dan perceived 
behavioral control tersebut akan mempengaruhi kuat atau lemahnya intention 
seseorang dalam menampilkan suatu perilaku tertentu (Icek Ajzen, 2005). 
Apabila attitude toward the behavior, subjective norm, dan perceived behavioral 
control salesman dalam melakukan pengiriman barang tepat waktu seluruhnya 
positif, maka intention salesman dalam kegiatan tersebut akan semakin kuat. 
Sebaliknya bila attitude toward the behavior, subjective norm, dan perceived 
behavioral control seluruhnya negatif, maka intention salesman dalam 
memunculkan perilaku dalam melakukan pengiriman barang tepat waktu akan 
semakin lemah. 
Berbeda halnya bila terdapat variasi pada ketiga determinan tersebut 
(dimana tidak seluruhnya positif atau negatif). Berdasarkan teori Planned 
Behavior, walaupun dua dari ketiga determinan yang berpengaruh tersebut 
bernilai positif terhadap intention dalam melakukan pengiriman barang tepat 
waktu, belum tentu intention salesman dalam kegiatan tersebut akan semakin 
kuat. Hal ini disebabkan intention salesman ditentukan tidak berdasarkan jumlah 
determinan yang positif terhadap perilaku dalam melakukan pengiriman barang 
tepat waktu, melainkan seberapa besar pengaruh masing   masing determinan 
(baik yang positif maupun negatif) terhadap intention salesman dalam kegiatan 
tersebut (Icek Ajzen ,2005).  
Apabila salesman yang memiliki attitude toward the behavior yang positif 
dan determinan tersebut memiliki pengaruh paling kuat terhadap intention, maka 
intention salesman dalam melakukan pengiriman barang tepat waktu akan kuat 
walaupun dua determinan yang lainnya negatif. Begitu juga sebaliknya, apabila 
salesman memiliki attitude toward the behavior yang negatif dan kedua 
determinan yang lain positif, maka intention salesman dalam melakukan 
pengiriman barang tepat waktu menjadi lemah karena attitude toward the 
behavior memberikan pengaruh yang paling kuat terhadap intention.  
 Attitude toward the behavior, subjective norms dan perceived behavioral 
control juga saling berhubungan satu sama lain. Apabila hubungan antara attitude 
toward the behavior dan subjective norms erat, maka salesman yang memiliki 
sikap favourable dalam melakukan pengiriman barang tepat waktu seperti tertarik 
dalam melakukan pengiriman barang tepat waktu juga memiliki persepsi bahwa 
atasan, supervisor, rekan kerja dan keluarga mendukung mereka dalam melakukan 
pengiriman barang tepat waktu dan mereka bersedia mematuhi orang-orang 
tersebut. Salesman yang mempersepsi bahwa atasan, supervisor, rekan kerja dan 
keluarga mendukung mereka dalam melakukan pengiriman barang tepat waktu 
dan mereka bersedia mematuhi orang-orang tersebut, maka sikapnya akan 
semakin favourable dalam melakukan kegiatan tersebut. 
Apabila terdapat hubungan yang erat antar attitude toward the behavior 
dan perceived behavioral control, maka salesman yang memiliki sikap 
favourable, terhadap kegiatan melakukan pengiriman barang tepat waktu, juga 
akan memiliki persepsi bahwa dalam melakukan pengiriman barang tepat waktu 
mudah untuk dilakukan. Begitu pula sebaliknya, jika salesman memiliki persepsi 
bahwa dalam melakukan pengiriman barang tepat waktu mudah untuk dilakukan, 
maka salesman juga akan memiliki sikap favourable dalam melakukan kegiatan 
tersebut. Salesman yang memiliki sikap unfavourable terhadap kegiatan 
melakukan pengiriman barang tepat waktu maka salesman juga akan memiliki 
persepsi bahwa dalam melakukan pengiriman barang tepat waktu sulit untuk 
dilakukan. 
Apabila terdapat hubungan yang erat antara subjective norms dan 
perceived behavioral control, maka salesman yang memiliki persepsi bahwa 
atasan, supervisor, rekan kerja dan keluarga mendukung mereka dalam melakukan 
pengiriman barang tepat waktu dan mereka bersedia mematuhi orang-orang 
tersebut, juga akan memiliki persepsi bahwa mereka mampu melakukan 
pengiriman barang tepat waktu, sehingga dukungan dari orang-orang tersebut 
membuat salesman semakin mudah melakukan pengiriman barang tepat waktu.  
Kontribusi dan korelasi dari ketiga determinan akan mempengaruhi kuat 
lemahnya intention salesman dalam melakukan pengiriman barang tepat waktu. 
Dari uraian diatas dapat digambarkan dalam bagan sebagai berikut : 
Faktor-faktor yang mempengaruhi: 
penghayatan melakukan kegiatan. 
Dari kerangka pemikiran di atas, peneliti mempunyai asumsi, yaitu: 
  Salesman di PT  X  memiliki derajat intention yang berbeda-beda dalam 
melakukan pengiriman barang tepat waktu 
  Intention salesman dalam melakukan pengiriman barang tepat waktu 
dipengaruhi oleh attitude toward the behavior, subjective norm, dan 
perceived behavioral control yang berbeda   beda. 
  Determinan-determinan salesman PT  X  dalam melakukan pengiriman 
barang tepat waktu memiliki derajat yang berbeda-beda. 
  Attitude toward the behavior, subjective norms, dan perceived behavioral 
control saling berkorelasi dan memiliki kaitan satu sama lain. 
  Kekuatan dari ketiga determinan intention dipengaruhi oleh faktor internal 
maupun eksternal. 
1. H0 :  Tidak ada pengaruh yang signifikan antara attitude toward the 
behavior terhadap intention salesman dalam melakukan pengiriman 
barang  tepat waktu.  
H1 : Ada pengaruh yang signifikan antara attitude toward the behavior 
terhadap intention salesman dalam melakukan pengiriman barang 
tepat waktu. 
2. H0 :   Tidak ada pengaruh yang signifikan antara subjective norms terhadap 
 intention salesman dalam melakukan pengiriman barang tepat 
waktu. 
H1 : Ada pengaruh yang signifikan antara subjective norms terhadap 
intention salesman dalam melakukan pengiriman barang tepat 
waktu. 
3. H0 : Tidak ada pengaruh yang signifikan antara perceived behavioral 
control terhadap intention salesman dalam melakukan pengiriman 
barang tepat waktu. 
H1 : Ada pengaruh yang signifikan antara perceived behavioral control 
terhadap intention salesman dalam melakukan pengiriman barang 
tepat waktu. 
 Teori planned behavior dilandasi oleh asumsi bahwa manusia selalu 
berperilaku didasarkan dengan akal sehat; manusia memperhitungkan 
ketersediaan informasi, dan secara implisit atau eksplisit mempertimbangkan 
dampak dari perilaku mereka. Selaras dengan asumsi tersebut, teori planned 
behavior menetapkan bahwa intention seseorang untuk melakukan (atau tidak 
melakukan) sebuah perilaku merupakan penentu yang paling utama dan paling 
dekat dengan perilaku tersebut. 
 Menurut teori planned behavior, intention (dan behavior) adalah fungsi 
dari tiga determinan dasar, yang pertama adalah attitude toward the behavior. 
Attitude toward behavior adalah sikap favorable / unfavorable terhadap evaluasi 
positif atau negatif seseorang dalam menampilkan perilaku tertentu. Determinan 
kedua adalah persepsi seseorang mengenai tuntutan dari orang-orang yang penting 
baginya (important other) untuk menampilkan atau tidak menampilkan suatu 
perilaku dan kesediaan untuk mengikuti orang-orang tersebut, determinan ini 
disebut subjective norm. Determinan yang ketiga adalah persepsi seseorang 
mengenai kemampuan untuk menampilkan suatu perilaku yang disebut perceived 
behavioral control. Secara umum manusia cenderung menampilkan suatu perilaku 
ketika mereka mengevaluasi bahwa perilaku tersebut positif, merasakan adanya 
tekanan sosial untuk menampilkan perilaku tersebut, dan merasa yakin bahwa 
mereka memiliki sumber daya dan kesempatan untuk menampilkan perilaku 
tersebut. 
 Teori planned behavior memiliki asumsi bahwa tingkat kepentingan dari 
ketiga determinan dapat berbeda-beda tergantung dari intention yang diteliti. Pada 
beberapa penelitian tentang intention, attitude toward the behavior memiliki 
pengaruh yang lebih besar dibandingkan dengan subjective norms ataupun 
perceived behavioral control begitu juga sebaliknya. Pada penelitian lain 
memungkinkan subjective norms ataupun perceived behavioral control memiliki 
pengaruh yang lebih besar dibandingkan dengan attitude toward the behavior.  
 Intention adalah suatu keputusan (niat) mengerahkan usaha untuk 
melakukan suatu perilaku. Intention merupakan tanda dari seberapa keras 
seseorang berusaha, seberapa banyak usaha yang mereka rencanakan akan 
dilakukan, dalam tujuan untuk menampilkan seluruh perilaku. Intention sendiri 
diasumsikan sebagai antecedent langsung dari perilaku dan mengarahkan perilaku 
yang dikontrol dan disengaja. Semakin kuat intention untuk menampilkan suatu 
perilaku, semakin mungkin perilaku tersebut dilakukan. Intention dari suatu 
perilaku hanya dapat muncul jika individu dapat memutuskan keinginannya untuk 
melakukan atau tidak melakukan suatu perilaku (volitional control). Jika 
seandainya ada actual control dalam derajat yang cukup terhadap perilaku, 
individu diharapkan dapat melaksanakan intention-nya pada saat kesempatan 
muncul. 
 Attitudes Toward the Behavior adalah sikap favourable / unfavourable 
terhadap evaluasi positif atau negatif individu dalam menampilkan suatu perilaku. 
Evaluasi seseorang terhadap suatu objek dapat terbentuk dari beliefs yang 
dipegang oleh orang itu mengenai objek tersebut. Ide inilah yang diterapkan pada 
pembentukan dari attitudes toward the behavior. Dalam membicarakan attitude 
toward the behavior, setiap belief mengaitkan perilaku dengan konsekuensi 
tertentu atau dengan beberapa atribut lain seperti keuntungan atau kerugian yang 
berhubungan dengan pelaksanaan perilaku. Mengingat atribut yang menyertai 
perilaku sudah dinilai positif atau negatif, individu secara otomatis akan memiliki 
penilaian tersendiri terhadap suatu perilaku. Dengan cara itu individu belajar 
untuk menyukai perilaku yang dipercaya mempunyai konsekuensi yang ia 
inginkan dan individu tersebut membentuk sikap negatif terhadap perilaku yang ia 
asosiasikan dengan konsekuensi yang tidak ia inginkan. Menurut teori planned 
behavior, attitudes toward the behavior ditentukan oleh sejumlah keyakinan 
mengenai konsekuensi dari menampilkan suatu perilaku. Keyakinan ini disebut 
behavioral beliefs. Setiap behavioral beliefs mengaitkan suatu perilaku pada 
outcome tertentu. Attitude toward the behavior ditentukan oleh eveluasi seseorang 
mengenai outcomes yang diasosiasikan dengan perilaku dan kekuatan asosiasi 
tersebut.  
 Evaluasi dari setiap outcomes yang diasosiasikan dengan suatu perilaku 
berkontribusi pada attitude dalam proporsi pada kemungkinan subjektif seseorang 
bahwa suatu perilaku akan menghasilkan outcome tertentu. Jika behavioral belief 
strength dikalikan dengan outcome evaluation maka akan diperoleh perkiraan dari 
attitude toward the behavior, sebuah perkiraan yang berdasarkan pada sejumlah 
beliefs mengenai suatu perilaku. Hal ini dapat digambarkan melalui simbol dalam 
persamaan berikut ini : 
AB ? ? biei 
AB adalah attitude toward the behavior B; bi adalah behavioral belief 
bahwa menampilkan perilaku B akan mengarahkan pada outcome i; ei adalah 
evaluasi dari outcome i (outcome evaluation). Dapat terlihat, secara umum 
seseorang yang yakin bahwa menampilkan suatu perilaku akan mengarahkan pada 
outcomes yang positif juga akan memiliki attitude toward the behavior yang 
favourable terhadap perilaku tersebut. Begitu juga sebaliknya, seseorang yang 
yakin bahwa menampilkan suatu perilaku akan mengarahkan pada outcomes yang 
negatif juga akan memiliki attitude yang unfavourable terhadap perilaku tersebut. 
 Determinan kedua dari intention adalah subjective norms. Subjective 
norms adalah persepsi individu mengenai tuntutan dari orang-orang yang 
signifikan baginya (important others) untuk menampilkan atau tidak 
menampilkan suatu perilaku dan kesediaan individu untuk mematuhi tuntutan 
tersebut. Subjective norms terbentuk dari hasil perkalian antara normative beliefs 
dengan motivation to comply. Normative belief adalah keyakinan individu bahwa 
important others menuntut atau tidak menuntut individu untuk menampilkan 
suatu perilaku. Pada beberapa penelitian, yang termasuk important others adalah 
orang tua, pasangan hidup, teman dekat dan rekan kerja. Selain itu yang termasuk 
dalam important others juga tergantung pada perilaku yang sedang diteliti. Secara 
umum orang yang yakin bahwa kebanyakan important others yang memotivasi 
mereka untuk patuh berpikir bahwa orang tersebut harus menampilkan suatu 
perilaku, maka orang itu akan mempersepsi adanya tuntutan untuk menampilkan 
perilaku tersebut dan begitu juga sebaliknya. 
 Hubungan antara normative beliefs dan subjective norms digambarkan 
secara simbolis dalam persamaan berikut : 
SN adalah subjective norms; ni adalah normative belief yang mempertimbangkan 
important others i; mi adalah motivasi untuk mematuhi important others i 
(motivation to comply). Secara umum seseorang yang yakin bahwa important 
others menuntut ia untuk melakukan suatu perilaku dan ia memiliki motivasi 
untuk mengikuti tuntutan tersebut, maka individu akan memiliki subjective norms 
yang positif. Demikian juga sebaliknya, seseorang yang yakin bahwa important 
others tidak menuntut dia untuk melakukan suatu perilaku dan ia bermotivasi 
untuk mengikuti tuntutan tersebut, maka individu akan memiliki subjective norms 
yang negatif terhadap perilaku tersebut. 
 Determinan ketiga dari intention adalah perceived behavioral control, juga 
diasumsikan merupakan fungsi dari sejumlah beliefs yaitu beliefs mengenai ada 
atau tidaknya faktor-faktor yang dapat mendukung atau menghambat dalam 
menampilkan suatu perilaku tertentu. Beliefs ini dapat didasari sebagian oleh 
pengalaman masa lalu, tetapi juga biasanya dipengaruhi juga oleh informasi tidak 
langsung mengenai suatu perilaku, dengan cara mengobservasi pengalaman 
teman. Selain itu juga dipengaruhi oleh faktor lain yang meningkatkan atau 
menurunkan persepsi mengenai kesulitan untuk menampilkan suatu perilaku 
tertentu. 

