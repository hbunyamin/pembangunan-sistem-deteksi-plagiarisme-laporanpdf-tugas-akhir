Kesehatan merupakan kunci utama bagi kesejahteraan hidup. Definisi sehat 
menurut World Health Organization (WHO) adalah suatu keadaan sejahtera yang 
meliputi fisik, mental dan sosial yang tidak hanya bebas dari penyakit atau kecacatan 
(WHO, 1992). Menurut WHO, ada empat komponen penting dalam definisi sehat 
yaitu: Pertama, sehat jasmani merupakan komponen penting dalam arti sehat 
seutuhnya, dimana seluruh fungsi fisiologi tubuh berjalan normal. Kedua, sehat 
mental yaitu ketika seseorang selalu merasa puas dengan apa yang ada pada dirinya, 
pengertian dan toleransi terhadap kebutuhan emosi orang lain, dapat mengontrol diri 
dan dapat menyelesaikan masalah secara cerdik dan bijaksana. Ketiga, kesejahteraan 
sosial merupakan suasana kehidupan berupa perasaan aman damai dan sejahtera, 
cukup pangan, sandang dan papan. Keempat, sehat spiritual merupakan komponen 
tambahan pada definisi sehat oleh WHO dan memiliki arti penting dalam kehidupan 
sehari-hari masyarakat. 
Untuk memiliki kondisi tubuh yang sehat harus disertai dengan usaha untuk 
menjaga kesehatan. Usaha yang dilakukan seseorang untuk menjaga kesehatan dapat 
dilihat dari perilaku hidup sehat, yaitu perilaku yang bertujuan untuk 
mempertahankan atau meningkatkan kesehatan (Kasl&Cobb, 1966). Perilaku sehat 
yang buruk memiliki efek yang buruk terhadap kesehatan atau menimbulkan 
penyakit. Perilaku tersebut termasuk merokok, konsumsi alkohol yang berlebihan, 
dan konsumsi makanan berlemak tinggi. Sebaliknya meningkatkan perilaku sehat 
bermanfaat untuk kesehatan atau melindungi individu dari penyakit. Perilaku 
tersebut termasuk olahraga, konsumsi buah dan sayur. Banyak kondisi kesehatan 
yang buruk disebabkan oleh perilaku seperti minum alkohol, penggunaan narkoba, 
merokok, makan berlebihan (Renner&Schwarzer, 2003). 
Usaha mempertahankan dan meningkatkan kesehatan bukan hanya untuk 
individu pada usia lanjut namun juga bagi individu yang masih muda, seperti 
mahasiswa. Mereka harus mulai menjaga kesehatannya agar tidak berdampak 
panjang saat usia lanjut. Pada usia 18 tahun seorang remaja mulai memasuki dunia 
mahasiswa (Gunarsa & Gunarsa, 2004). Dalam statusnya sebagai mahasiswa dengan 
atribut sebagai agent of change, generasi muda memiliki peran penting dalam 
pembangunan bangsa (Wikagoe, 2003). Seiring dengan perkembangannya 
mahasiswa yang tergolong sebagai remaja lanjut masih mengalami banyak masalah 
dan kesukaran yang sering timbul diantaranya berkaitan dengan masalah pergaulan, 
konformitas, masalah dengan lawan jenis (percintaan), penyesuaian di bidang 
akademik yang oleh remaja terkadang dianggap berlebihan dan berat sehingga 
kemungkinan dapat mengakibatkan timbulnya kegoncangan bahkan menimbulkan 
suatu hambatan besar (Gunarsa & Gunarsa, 2004). Dunia mahasiswa yang padat 
dengan berbagai aktivitas baik dari dalam ataupun dari luar kampus namun masih 
tetap membutuhkan penyesuaian diri dengan lingkungan masyarakat. Selain itu 
adanya beban tanggung jawab terhadap diri sendiri ataupun tuntutan untuk mampu 
menghadapi berbagai masalah yang datang (Putro, 2006). Segala perubahan yang 
terjadi saat menjadi mahasiswa, seperti perbedaan beban akademik, padatnya 
aktifitas dapat membuat mahasiswa kurang dapat menjaga kesehatan sehingga 
muncul masalah pada kesehatan mahasiswa, seperti pola makan yang kurang teratur, 
tidak berolahraga, merokok dan mulai meminum alkohol. 
Kondisi tubuh yang kurang baik dapat mengganggu mahasiswa dalam 
menjalani aktivitas sehari-hari. Para peneliti telah menunjukkan secara global bahwa 
banyak mahasiswa terlibat dalam berbagai perilaku sehat beresiko meliputi merokok, 
penggunaan alkohol dan obat-obatan lainnya, unprotective sex, perilaku makan, 
aktifitas fisik, dan mengontrol berat badan (Von, Ah D., Ebert S., Ngamvitroj, A., 
Park, N., & Kang, D. H, 2004). Banyak mahasiswa yang telah mengetahui 
pentingnya kesehatan dan akibat dari perilaku sehat yang buruk tetapi kurang mampu 
mengaplikasikan pengetahuannya tersebut untuk meningkatkan kualitas 
kehidupannya. Untuk mendorong para mahasiswa agar dapat menyadari pentingnya 
menjaga kesehatan salah satu universitas di Bandung yaitu Universitas  X   
membuat slogan  X Sehat  yang terpampang di area kampus, menyediakan Rumah 
Sakit Gigi dan Mulut, melarang mahasiswa untuk merokok dan menyediakan 
lapangan untuk berolahraga. Dengan hal tersebut universitas mengharapkan para 
mahasiswa dapat menjaga kesehatan agar dapat menjalankan aktivitas belajar dengan 
lancar. Berdasarkan observasi yang dilakukan peneliti walaupun universitas telah 
melakukan berbagai upaya namun masih saja ada mahasiswa yang kurang dapat 
menyadari pentingnya menjaga kesehatan, terlihat dari mahasiswa yang masih 
merokok di lingkungan kampus, dan masih banyak mahasiswa yang belum 
memanfaatkan lapangan olahraga yang disediakan. Beberapa upaya yang dilakukan 
universitas untuk meningkatkan kesehatan mahasiswa ternyata tidak sejalan dengan 
perilaku yang ditampilkan oleh mahasiswa dimana mahasiswa memiliki perilaku 
hidup yang tidak sehat. 
Salah satunya perilaku sehat pada mahasiswa dapat dilihat dari pola makan. 
Pola makan yang sehat diasosiasikan dengan pengaturan jumlah dan jenis makanan 
dengan maksud tertentu, seperti mempertahankan kesehatan dan status nutrisi 
(Sebayang, 2012).  Nutrisi jelas penting untuk mencegah penyakit dan meningkatkan 
kesehatan. Pola makan yang sehat adalah makanan dengan menu seimbang dalam 
arti kualitas dan kuantitas cukup untuk memenuhi kebutuhan tubuh (Notoatmodjo, 
2003). Berdasarkan survei awal pada sepuluh mahasiswa. Dari perilaku makan 
didapatkan bahwa tujuh mahasiswa (70%) kadang-kadang memakan makanan yang 
sehat, tiga mahasiswa (30%) sering memakan makanan yang sehat. 
Perilaku sehat juga dapat dilihat dari aktivitas fisik. Aktifitas fisik yang 
dilakukan manusia bertujuan untuk meningkatkan kualitas fisik sumber daya 
manusia, terutama apabila dilakukan secara benar dan teratur. Menurut WHO (2008), 
aktivitas fisik adalah setiap gerakan tubuh yang dihasilkan oleh otot rangka yang 
memerlukan pengeluaran energi. Aktivitas fisik yang dapat dilakukan bermacam-
macam dari yang ringan sampai berat. Berdasarkan survei awal pada mahasiswa, 
untuk aktivitas fisik enam mahasiswa (60%) kadang-kadang melakukan aktivitas 
fisik, dan empat (40%) mahasiswa sering melakukan aktivitas fisik. 
Perilaku sehat yang lainnya dapat dilihat dari tidak merokok. Merokok bukan 
merupakan hal yang asing di kalangan mahasiswa, sering dijumpai mahasiswa 
merokok di sela-sela kegiatan di kampus. Padahal dalam rokok terdapat zat adiktif 
yang dapat membuat seseorang kecanduan. Maka, jika mahasiswa yang merokok 
tidak segera berhenti merokok, kebiasaan tersebut dapat berlanjut terus hingga 
mereka tua dan mereka harus siap menanggung penyakit-penyakit yang ditimbulkan 
oleh rokok. Berdasarkan survei awal pada mahasiswa, untuk perilaku merokok enam 
mahasiswa (60%) tidak pernah merokok, dua mahasiswa (20%) kadang-kadang 
merokok, dan dua mahasiswa (20%) sering merokok. 
Perilaku sehat selanjutnya dapat dilihat dari tidak mengkonsumsi alkohol. 
Konsumsi alkohol yang berlebihan dapat menghasilkan beberapa masalah kesehatan 
yang serius. Mengkonsumsi alkohol adalah mengkonsumsi minuman yang 
mengandung cairan tidak berwarna (bening), mudah menguap dan mudah terbakar, 
dapat menimbulkan adiksi yaitu ketagihan atau ketergantungan (Indrawan, 2007). 
Walaupun sering minum alkohol   sangat   lazim   pada usia  ini,  mahasiswa  
cenderung  lebih sering  minum  dan  lebih  berat  daripada  mereka   yang  tidak  
berkuliah (Papalia, dkk. 2009). Berdasarkan survei awal pada mahasiswa, untuk 
perilaku mengkonsumsi alkohol empat mahasiswa (40%) tidak pernah 
mengkonsumsi alkohol, empat mahasiswa (40%) kadang-kadang mengkonsumsi 
alkohol, dan dua mahasiswa (20%) sering mengkonsumsi alkohol. 
Ketika menjalankan perilaku hidup sehat ada beberapa hal yang menghambat 
mahasiswa seperti mahasiswa kurang dapat mengatur waktu, memiliki kegiatan yang 
padat, rasa malas, dan juga keyakinan mahasiswa untuk dapat melaksanakan perilaku 
hidup sehat atau yang disebut self-efficacy. Self-efficacy yaitu keyakinan diri 
seseorang bahwa orang tersebut memiliki kemampuan untuk melakukan suatu 
perilaku (Bandura, 1999). Orang dengan self-efficacy rendah memiliki aspirasi yang 
rendah dan komitmen yang lemah terhadap  tujuan-tujuan yang mereka tetapkan. 
Mereka juga cenderung menghindari tugas sulit yang dipandang sebagai ancaman 
terhadap diri mereka. Sebaliknya, mereka yang memiliki self-efficacy tinggi akan 
menentukan tujuan yang menantang dan berkomitmen terhadap tujuan tersebut. 
Menurut Sarafino dan Smith (2011) hal yang terpenting yang harus dimiliki 
oleh individu untuk dapat melaksanakan perilaku sehat adalah self-efficacy. Seorang 
individu memerlukan cukup self-efficacy untuk melaksanakan perubahan dalam 
hidupnya tanpa self-efficacy, motivasi mereka untuk berubah akan terhambat. Self-
efficacy mengatur motivasi dengan menentukan tujuan yang ditetapkan untuk diri 
mereka sendiri, kekuatan komitmen mereka dan hasil yang  diharapkan dari usaha 
yang telah mereka lakukan (Bandura,1998). Semakin kuat self-efficacy dirasakan dan 
ditanamkan, semakin besar orang-orang untuk mendapatkan dan mempertahankan 
upaya yang diperlukan untuk mengadopsi, mempertahankan dan meningkatkan 
perilalu kesehatan (Bandura,1998).  
Self-efficacy merupakan kesadaran yang menentukan apakah perubahan 
perilaku hidup sehat akan dimulai, seberapa banyak upaya yang akan dikeluarkan, 
dan seberapa lama hal tersebut akan dipertahankan dalam menghadapi tantangan dan 
kegagalan. Self-efficacy memengaruhi upaya pencetus untuk merubah perilaku 
beresiko dan ketekunan untuk terus berjuang meskipun terdapat hambatan yang 
dapat menurunkan motivasi. Self-efficacy memengaruhi tantangan yang diambil oleh 
individu serta seberapa tinggi mereka menetapkan tujuan mereka seperti  Saya 
bermaksud untuk mengurangi merokok , atau  Saya bermaksud untuk berhenti 
merokok . Individu dengan  self-efficacy yang tinggi akan memilih tujuan yang lebih 
menantang (De Vellis & De Vellis, 2000). Schwarzer mengklaim bahwa self-efficacy 
merupakan prediksi dari intention perilaku dan perubahan perilaku pada beberapa 
macam perilaku sehat seperti dental floss, frequency of flossing, effective use of 
contraception, breast self-examination, drug addicts, intentions to quit smoking and 
intentions to adhere to weight loss programmes and exercise.  
Berdasarkan survei awal pada sepuluh mahasiswa Universitas Kristen 
Maranatha mengenai self-efficacy dalam menjalankan perilaku hidup sehat. Untuk 
self-efficacy dalam menjalankan pola makan sehat didapatkan bahwa empat 
mahasiswa (40%) tidak yakin dirinya mampu mengatur diri untuk tetap makan 
makanan yang sehat, dan enam mahasiswa (60%) yakin bahwa mampu mengatur diri 
untuk tetap makan makanan yang sehat. Untuk self-efficacy dalam olahraga enam 
mahasiswa (60%) tidak yakin dirinya mampu mengatur untuk melaksanakan niat 
olahraganya, dan empat mahasiswa (40%) yakin dirinya mampu mengatur untuk 
melaksanakan niat olahraganya. Untuk self-efficacy untuk tidak merokok empat 
mahasiswa (40%) tidak yakin mampu mengendalikan dirinya untuk tidak merokok, 
dan enam mahasiswa (60%) yakin mampu mengendalikan dirinya untuk tidak 
merokok. Untuk self-efficacy mengendalikan diri untuk tidak mengkonsumsi alkohol 
dua mahasiswa (20%) tidak yakin mampu mengendalikan dirinya untuk tidak 
mengkomsumsi alkohol dan delapan mahasiswa (80%) yakin mampu mengendalikan 
dirinya untuk tidak mengkomsumsi alkohol. 
Berdasarkan hasil survei awal di atas didapatkan bahwa terdapat mahasiswa 
yang memiliki self-efficacy tinggi dan memiliki perilaku hidup sehat serta ada pula 
mahasiswa yang memiliki self-efficacy tinggi dan memiliki perilaku hidup tidak 
sehat. Oleh sebab itu peneliti tertarik untuk meneliti lebih lanjut mengenai hubungan 
antara self-efficacy dengan perilaku hidup sehat pada mahasiswa Universitas  X  
Bandung.  
Dari penelitian ini yang ingin diteliti adalah bagaimana hubungan antara self-
efficacy dengan perilaku hidup sehat pada mahasiswa Universitas  X  Bandung.    
Untuk memperoleh data dan gambaran mengenai self-efficacy dengan 
perilaku hidup sehat pada mahasiswa Universitas  X  Bandung.  
Untuk mengetahui derajat hubungan antara self-efficacy dengan perilaku 
hidup sehat pada mahasiswa Universitas  X  Bandung.  
1. Memberikan tambahan informasi bagi health psychology, mengenai hubungan 
antara self-efficacy dengan perilaku hidup sehat pada mahasiswa Universitas  X  
Bandung. 
2. Memberikan masukan bagi peneliti lain yang berminat melakukan penelitian 
serupa mengenai hubungan antara self-efficacy dengan perilaku  hidup sehat pada 
mahasiswa Universitas  X  Bandung. 
1. Dapat menjadi masukan bagi mahasiswa yang belum menjalankan perilaku hidup 
sehat dan memiliki self-efficacy rendah agar mulai menjalankan perilaku hidup 
sehat dan meningkatkan self-efficacy sehingga dapat menjalankan perilaku hidup 
sehat dengan lancar. 
2. Dapat menjadi masukan bagi mahasiswa yang memiliki self-efficacy tinggi dalam 
menjalankan perilaku hidup sehat agar dapat mempertahankan sehingga dapat 
meningkatkan kualitas hidupnya.   
Mahasiswa merupakan kaum terpelajar, mereka telah belajar di institusi 
formal setidaknya selama 12 tahun yang tentunya mengajarkan mana hal yang benar 
dan salah, dengan demikian mahasiswa mengetahui tentang manfaat hidup sehat. 
Akan tetapi masih banyak mahasiswa yang belum menjalankan perilaku hidup sehat. 
Mahasiswa tentunya harus mulai menjaga kesehatan dari sekarang agar tidak 
berakibat buruk nantinya di masa yang akan datang. Usaha yang dilakukan 
mahasiswa untuk menjaga kesehatan dapat dilihat dari perilaku hidup sehat, yaitu 
perilaku yang bertujuan untuk mempertahankan atau meningkatkan kesehatan 
(Kasl&Cobb, 1966). Perilaku hidup sehat pada mahasiswa merupakan upaya yang 
dilakukan mahasiswa untuk menjaga mengatur dan menstabilkan kesehatan mereka 
(Taylor, 2009). Perilaku sehat mahasiswa yang akan dianalisis adalah perilaku 
makan, aktifitas fisik, merokok dan konsumsi alkohol. 
Dalam menjalankan perilaku hidup sehat menurut HAPA terdapat dua tahap, 
tahap yang pertama motivational phase terdiri dari action self-efficacy, outcome 
expectancies, risk perception, dan behavioral intentions. Tahap kedua volitional 
phase terdiri dari action planning, coping planning, maintenance self-efficacy, dan 
recovery self-efficacy. Telah ditemukan bahwa self-efficacy berperan penting pada 
setiap tahap dari proses perubahan perilaku hidup sehat (Bandura, 1997). Self-
efficacy adalah keyakinan diri pada mahasiswa bahwa dirinya memiliki kemampuan 
untuk melakukan perilaku hidup sehat (Bandura, 1999). 
Menurut HAPA ketika akan melakukan perilaku hidup sehat mahasiswa 
melewati dua tahapan, tahap pertama motivational phase adalah proses dimana 
mahasiswa membentuk niat baik untuk mengubah perilaku beresiko dan mengarah 
pada intention untuk bertindak, pada tahap ini mahasiswa mengalami tiga proses. 
Pertama, action self-efficacy merupakan proses dimana ketika mahasiswa belum 
melakukan perilaku hidup sehat tetapi telah memiliki telah memiliki keyakinan dapat 
melakukan perilaku hidup sehat. Hal tersebut tampak dari mahasiswa yang yakin 
bahwa dirinya dapat makan dengan gizi seimbang, rajin berolahraga, tidak merokok, 
dan tidak mengkonsumsi alkohol, mahasiswa yakin bahwa dirinya dapat berhasil 
dalam menjalankan perilaku hidup sehat. Kedua outcome expectancies adalah proses 
dimana mahasiwa memikirkan mengenai apa yang akan didapatkan apabila 
melakukan perilaku hidup sehat, berkaitan dengan  penilaian positif dan negatif 
terhadap hasil yang akan didapatkan. Hal tersebut tampak dari mahasiswa yang 
memprediksi perubahan apa yang akan dialami atau dirasakan apabila makan dengan 
gizi seimbang, rajin berolahraga, tidak merokok, dan tidak mengkonsumsi alkohol. 
Seperti mahasiswa merasa lebih sehat apabila selalu makan dengan gizi seimbang, 
rajin berolahraga, tidak merokok, dan tidak mengkonsumsi alkohol. Ketiga risk 
perception adalah proses dimana individu memikirkan mengenai resiko yang 
mungkin akan dialami apabila tidak melakukan perilaku perilaku hidup sehat. Hal 
tersebut tampak dari mahasiswa yang memikirkan kemungkinan gangguan kesehatan 
apa saja yang akan dialami ketika tidak makan dengan gizi seimbang, tidak rajin 
melakukan aktivitas fisik, merokok, dan mengkonsumsi alkohol Seperti kolestrol 
meningkat. Dari ketiga proses yang dialami mahasiswa terbentuk intention untuk 
melakukan perilaku hidup sehat. 
Setelah muncul intention kemudian masuk ke tahap kedua yaitu volitional 
phase adalah proses dimana mahasiswa telah mengarah pada perilaku sehat 
sebenarnya. Proses yang pertama, action planning dimana mahasiswa menentukan 
secara rinci bagaimana dan dalam situasi apa tindakan situasional akan dilakukan. 
Hal tersebut tampak dari mahasiswa yang telah memiliki rencana kapan dan 
bagaimana akan memulai makan dengan gizi seimbang, rajin berolahraga, tidak 
merokok, dan tidak mengkonsumsi alkohol. Kedua, coping planning dimana 
mahasiswa merencanakan antisipasi akan hambatan yang mungkin muncul saat 
melakukan perilaku hidup sehat dan sejauh mana mahasiswa telah mengembangkan 
strategi yang tepat untuk mengatasi hambatan tersebut. Hal tersebut tampak dari 
mahasiswa yang telah mempersiapkan rencana yang detail bagaimana menghadapi 
rintangan saat melakukan perilaku hidup sehat. Seperti mahasiswa memiliki rencana 
yang detail bagaimana untuk merespon teman yang menawarkan rokok saat 
mahasiswa mulai akan berhenti merokok. 
Saat membuat action planning dan coping planning mahasiswa dipengaruhi 
oleh maintenance self-efficacy yang merupakan keyakinan yang optimis untuk 
menghadapi halangan yang terjadi saat menjalankan perilaku hidup sehat. Hal 
tersebut tampak dari mahasiswa Universitas  X  Bandung yang memiliki derajat 
self-efficacy yang tinggi merasa optimis saat menghadapi halangan ketika 
menjalankan perilaku hidup sehat, dan yakin bahwa dirinya dapat tetap menjalankan 
perilaku hidup sehat walaupun ada halangan. Sedangkan Mahasiswa Universitas  X  
Bandung yang memiliki derajat self-efficacy yang rendah akan pesimis bahwa 
dirinya dapat menghadapi halangan yang ada saat menjalankan perilaku hidup sehat 
dan tidak yakin bahwa dirinya akan tetap mampu menjalankan perilaku hidup sehat. 
Setelah mahasiswa melalui beberapa proses yaitu action self-efficacy, outcome 
expectancies, risk perception, kemudian memunculkan intention dalam melakukan 
perilaku hidup sehat, lalu mahasiswa melakukan planning sehingga memunculkan 
perilaku hidup sehat. 
Perilaku hidup sehat yang muncul dipengaruhi oleh recovery self-efficacy 
merupakan keyakinan untuk memperbaiki pengalaman akan kegagalan ketika 
melakukan perilaku hidup sehat. Mahasiswa Universitas  X  Bandung yang 
memiliki derajat self-efficacy yang tinggi yakin bahwa dirinya dapat memperbaiki 
kegagalan yang pernah dialami saat menjalankan perilaku hidup sehat, mahasiswa 
yakin bahwa dirinya akan tetap bisa menjalankan perilaku hidup sehat meskipun 
sebelumnya pernah mengalami kegagalan.  Sedangkan Mahasiswa Universitas  X  
Bandung yang memiliki derajat self-efficacy yang rendah tidak yakin bahwa dirinya 
dirinya akan tetap bisa menjalankan perilaku hidup sehat meskipun sebelumnya 
pernah mengalami kegagalan. 
Untuk dapat menjalankan perilaku hidup sehat perlu disertai dengan 
keyakinan mahasiswa bahwa dirinya mampu untuk melaksanakannya. Jika mahasiwa 
yakin bahwa dirinya dapat menjalankan perilaku hidup sehat maka mahasiswa akan 
mengerahkan usahanya, saat ada rintangan mahasiswa akan berusaha mengatasinya 
dan tetap menjalankan perilaku hidup sehat walaupun dihadapkan dengan berbagai 
hambatan, setelah mengalami kegagalan mahasiswa akan cepat mengembalikan 
keyakinan bahwa dirinya mampu untuk menjalankan perilaku hidup sehat serta 
mahasiswa akan meningkatkan usahanya untuk dapat berhasil. Sedangkan 
mahasiswa yang kurang yakin bahwa dirinya dapat menjalankan perilaku hidup sehat 
akan sulit untuk memotivasi dirinya, ketika mengalami hambatan mahasiwa akan 
mudah menyerah dan menghindari untuk menghadapi hambatan yang dialami, dan 
saat mengalami kegagalan merasa kecewa dan tidak mencoba kembali. Untuk lebih 
jelasnya dapat digambarkan dalam bagan kerangka pikir sebagai berikut: 
hidup sehat: 
Self-efficacy 
Aspek self-efficacy: 
-Action self-efficacy 
-Maintenance self-
-Recovery self-efficacy 
Dari pemaparan di atas maka peneliti merumuskan asumsi: 
1. Perilaku hidup sehat mahasiswa dapat digambarkan dari beberapa hal yaitu 
perilaku makan, aktivitas fisik, tidak merokok dan tidak mengkonsumsi alkohol.  
2. Mahasiswa Universitas  X  Bandung memiliki gambaran perilaku hidup sehat 
yang berbeda-beda. 
3. Self-efficacy berperan dalam semua tahap pembentukan perilaku hidup sehat.  
4. Self-efficacy dapat digambarkan melalui tiga aspek, yaitu: Action self-efficacy, 
Maintenance self-efficacy dan Recovery self-efficacy. 
5. Mahasiswa Universitas  X  Bandung memiliki derajat self-efficacy yang berbeda-
beda.  
Terdapat hubungan positif antara self-efficacy dengan perilaku hidup sehat 
pada Mahasiswa Universitas  X  Bandung. 
Menurut Bandura (2002), self-efficacy merupakan keyakinan terhadap 
kemampuan seseorang dalam mengorganisir dan melaksanakan arah-arah 
tindakannya yang dibutuhkan untuk mengatur situasi-situasi yang prospektif. Di 
halaman lain dalam bukunya, Bandura (2002) juga mengungkapkan bahwa self-
efficacy adalah suatu belief (keyakinan) mengenai kemampuan individu untuk 
melakukan sesuatu hal ketika berada dalam berbagai macam kondisi dengan apapun 
keterampilan yang dimilikinya saat ini. 
Individu yang memiliki self-efficacy tinggi atau belief yang kuat dalam 
kemampuan mereka, memandang persoalan sebagai tantangan untuk diatasi bukan 
ancaman yang harus dihindari. Orientasi tersebut memelihara minat dan ketertarikan 
untuk terlibat dalam aktivitas. Individu tersebut membuat tujuan yang menantang 
untuk dirinya dan mempertahankan komitmen yang kuat pada tujuan tersebut. 
Individu memberikan upaya yang tinggi pada apa yang dikerjakannya dan 
meningkatkan upayanya saat menghadapi kegagalan atau kemunduran. Individu tetap 
berfokus pada tugas dan memikirkan strategi untuk menghadapi kesulitan. Individu 
menganggap kegagalan sebagai akibat upaya yang kurang memadai, yang akan 
mendukung orientasi kesuksesan. Individu cepat memulihkan rasa efficacy-nya 
setelah mengalami kegagalan dan kemunduran. Individu memandang ancaman dan 
stressor potensial dengan percaya diri bahwa ia dapat melakukan kontrol terhadap 
hal tersebut. Cara pandang individu yang efficacy tersebut memperbesar 
kemungkinan penyelesaian tugas, mengurangi stres, dan mengurangi kerentanan 
untuk mengalami depresi. Individu yang meragukan kemampuannya dalam area 
kegiatan tertentu (self efficacy rendah) menarik diri dari tugas sulit yang ada di area 
ini. Individu tersebut merasa sulit untuk memotivasi dirinya sendiri, mengendurkan 
usahanya atau menjadi terlalu cepat menyerah ketika menghadapi rintangan. Individu 
memiliki aspirasi yang rendah dan komitmen yang lemah terhadap tujuan yang ingin 
dicapainya. Dalam situasi yang menekan, mereka menekankan kelemahan 
personalnya, sulitnya tugas, dan konsekuensi merugikan jika mengalami kegagalan. 
Pemikiran yang mengganggu tersebut selanjutnya merusak usaha dan kemampuan 
berpikir analitiknya dengan mengalihkan perhatian dari bagaimana cara terbaik untuk 
melakukan tindakan kepada pemikiran yang berlebihan mengenai kelemahan 
personal dan kemungkinan permasalahan. Individu lambat dalam memulihkan rasa 
efficacy setelah mengalami kegagalan dan kemunduran. Oleh karena individu yang 
tampilan kinerjanya kurang memadai dianggap sebagai individu yang memiliki 
bakat/kemampuan yang kurang memadai. Individu seperti ini kemudian mudah 
mengalami stres dan depresi. 
Self-efficacy telah ditemukan menjadi penting pada semua tahap proses 
perubahan perilaku kesehatan (Bandura, 1997), tetapi tidak selalu membentuk 
sesuatu yang sama. Maknanya tergantung pada situasi tertentu dan individu yang 
mungkin dapat lebih maju atau tidak dalam proses perubahannya.  
HAPA dikembangkan oleh Schwarzer, HAPA dikonsepkan sebagai model 
berbasis-tahapan, dalam artian HAPA menspesifikkan dua fase berbeda yang harus 
dilewati agar individu dapat mengadopsi, berinisiatif, dan mempertahankan 
perlindungan kesehatan atau perilaku pendukungnya. HAPA dikonseptualisasikan 
sebagai model tahapan dinamis yang membuat perbedaan antara motivational phase 
dan volitional phase dalam perubahan perilaku sehat. HAPA menjelaskan pengertian 
tentang komponen-komponen psikologi dari setiap fase ini dan bagaimana masing-
masing komponen berinteraksi dengan komponen lain untuk mempengaruhi intensi 
untuk bersikap atau bertindak. Terdapat dua fase dalam HAPA fase pertama adalah 
motivational phase mengarah pada intention untuk bertindak, fase ini memprediksi 
jika intensi untuk bertindak atau tujuan perilaku, diprediksi langsung dari action self-
efficacy, outcome expectancies, risk perception, dan behavioral intention. Action 
self-efficacy adalah tahapan pertama dari proses dimana individu belum bertindak 
tetapi telah memiliki keinginan untuk melakukannya, ini merupakan keyakinan 
optimis selama preactional phase. Individu yang memiliki derajat action self-efficacy 
tinggi akan membayangkan kesuksesan, mengantisipasi potensi hasil yang akan 
didapatkan dari strategi yang beragam, dan lebih mungkin untuk memulai perilaku 
baru. Mereka yang memiliki derajat action self-efficacy rendah membayangkan 
kegagalan, memiliki self-doubts, dan kecenderungan menunda-nunda. Outcome 
expectancies adalah proses dimana individu memikirkan mengenai apa yang akan 
didapatkan apabila melakukan perilaku tertentu. Risk perception adalah proses 
dimana individu merenung mengenai resiko yang mungkin akan dialami apabila 
melakukan perilaku tertentu. Intensi disini dianggap sebagai sebuah intensi tujuan---
berniat mengerjakan sesuatu untuk mencapai kondisi yang dibutuhkan, seperti 
melakukan latihan olahraga secara teratur untuk merasa lebih sehat. 
Fase kedua volitional phase mengarah pada perilaku sehat sebenarnya yang 
dilihat dari action planning, coping planning, maintenance self-efficacy dan recovery 
self-efficacy. Action planning dimana individu menentukan secara rinci bagaimana 
dan dalam situasi apa tindakan situasional akan dilakukan. Coping planning 
merupakan dimana individu merencanakan antisipasi akan hambatan yang mungkin 
muncul saat melakukan perilaku hidup sehat dan sejauh mana mahasiswa telah 
mengembangkan strategi yang tepat untuk mengatasi hambatan tersebut.  
Maintenance self-efficacy adalah menunjukkan keyakinan yang optimis untuk 
menghadapi halangan yang terjadi. Perilaku kesehatan yang baru mungkin berubah 
menjadi jauh lebih sulit diikuti dari yang diperkirakan, tetapi self-efficacious person 
akan merepson secara percaya diri dengan strategi yang lebih baik, lebih banyak 
usaha, dan ketekunan yang terus menerus untuk mengatasi rintangan tersebut. 
Setelah suatu tindakan telah dilakukan individu yang memiliki derajat maintenance 
self-efficacy yang tinggi akan menanamkan lebih banyak usaha dan bertahan lebih 
lama daripada mereka yang memiliki derajat maintenance self-efficacy yang rendah. 
Recovery self-efficacy adalah keyakinan untuk memperbaiki pengalaman akan 
kegagalan. Jika penyimpangan terjadi, individu dapat menjadi korban  efek 
melanggar pantangan  yaitu menghubungkan penyimpangan yang dialami ke 
penyebab internal, stabil, dan global, mendramatisir acara, dan menafsirkannya 
sebagai full-blown relapse (Marlatt et al., 1995). Individu yang memiliki derajat self-
efficacy yang tinggi, bagaimanapun akan menghindari efek ini dengan 
menghubungkan penyimpangan yang terjadi dengan mencari cara untuk 
mengendalikan kerusakan dan mengembalikan harapan. Recovery self-efficacy 
berkaitan dengan keyakinan seseorang untuk kembali ke jalurnya setelah 
 tergelincir . Individu yang percaya kemampuan dirinya untuk mendapatkan kembali 
kontrol setelah mengalami kemunduran atau kegagalan dan untuk mengurangi 
bahaya (Marlatt, 2002). Untuk lebih jelas lihat bagan berikut: 
Perbedaan antara action self-efficacy, maintenance self-efficacy, dan recovery 
self-efficacy telah dikemukanan oleh Marlatt, Baer, dan Quigley (1995)  dalam 
perilaku adiktif. Perbedaan rasional antara beberapa tahapan self-efficacy bahwa 
selama perubahan perilaku sehat, tugas yang berbeda harus dapat dikuasai, dan 
keyakinan self-efficacy yang berbeda dibutuhkan untuk berhasil menguasai tugas-
tugas ini. Sebagai contoh, seseorang mungkin menjadi percaya diri dalam 
kemampuannya untuk menjadi physically active pada umumnya (yaitu action  self-
efficacy), tapi mungkin tidak sangat yakin tentang melanjutkan aktivitas fisik setelah 
mengalami kemunduran (low recovery self-efficacy). Ada perbedaan fungsional 
antara konsep-konsep self-efficacy sedangkan urutannya sementara kurang penting. 
Fase tertentu dalam self-efficacy yang berbeda dapat berada pada titik yang sama 
dalam suatu waktu, asumsinya bahwa mereka bekerja dengan cara yang berbeda. 
Misalnya, keberhasilan recovery self-efficacy paling berfungsi ketika ada penundaan 
dan akan melanjutkan kembali (Luszczynska, Mazurkiewicz, Ziegelmann, & 
self-efficacy 
self-efficacy 
self-efficacy 
Schwarzer, 2007; Luszczynska & Sutton, 2006).  Perbedaan antara fase self-
efficacy telah terbukti berguna dalam berbagai domain perubahan perilaku (Marlatt et 
al., 1995). Action self-efficacy cenderung untuk memprediksi niat, sedangkan 
maintenance self-efficacy cenderung untuk memprediksi perilaku. Individu yang 
telah pulih kembali dari kemunduran diperlukan keyakinan diri yang berbeda 
daripada mereka yang telah mempertahankan tingkat aktivitas mereka (Scholz et al., 
2005). Beberapa penulis (Rodgers, Hall, Blanchard, McAuley, & Munroe, 2002; 
Rodgers & Sullivan, 2001) telah menemukan bukti untuk fase tertentu self-efficacy 
dalam perilaku berolahraga (yaitu action self-efficacy, maintenance self-efficacy, dan 
scheduling self-efficacy). Dalam penelitian yang menerapkan HAPA, tahap spesifik 
self-efficacy memiliki efek yang berlainan pada berbagai perilaku kesehatan 
preventif, seperti breast self-examination (Luszczynska & Schwarzer, 2003), 
perilaku diet (Schwarzer & Renner, 2000), dan olahraga (Scholz et al., 2005). 
HAPA telah berhasil diaplikasikan dalam berbagai peningkatan perilaku sehat 
seperti healthy nutrition, physical activity, dan breast self-examination. Sebuah 
penelitian oleh Renner&Schwarzer (2005) berkaitan dengan peran intentions, risk 
perception, outcome expectancies, dan self-efficacy mulai berperan ketika 
mengadopsi atau mempertahankan diet yang sehat. Penelitian dilakukan terhadap 
1782 pria dan wanita berumur antara 14 sampai 87 tahun. Didapatkan perbedaan 
antara non-intenders dan intenders, non-intenders memiliki tingkat nutrisi yang lebih 
rendah telihat dalam konsumsi lemak, serat, vitamin, buah-buahan
Dalam sebuah penelitian yang dilakukan oleh 
Lippke (2004) pada 560 rehabilitas terhadap aktivitas fisik mereka. Pasien yang 
sejauh ini tidak aktif tapi memiliki keinginan untuk berolahraga secara khusus 
diuntungkan dari perencanaan intervensi, dimana pasien yang tidak memiliki 
keinginan untuk berolahraga atau pasien yang sudah melakukan aktivitas fisik tidak 
diuntungkan sebanyak pasien yang tidak aktif tapi memiliki keinginan untuk 
berolahraga. 

