Jumlah pengangguran lulusan pendidikan tinggi di Indonesia semakin hari 
semakin besar. Di tahun 2009 angka pengangguran terdidik telah mencapai 626.600 
orang. Banyaknya sarjana lulusan perguruan tinggi yang menjadi pengangguran 
disebabkan karena jumlah sarjana tidak sebanding dengan jumlah lapangan kerja 
yang tersedia (Media Indonesia, 20-8-2009). 
Penguasaan kompetensi serta produktivitas tenaga kerja di Indonesia masih 
kurang, sehingga tidak banyak berpengaruh terhadap peningkatan pertumbuhan 
ekonomi. Semua hal ini menyebabkan tenaga kerja di Indonesia sulit bersaing bahkan 
tidak sedikit peluang pekerjaan yang ada di Indonesia diisi oleh tenaga pekerja asing. 
Untuk mengatasi tuntutan dan permasalahan tersebut maka perlu dilakukan upaya-
upaya pembangunan melalui pendidikan dengan peningkatan kualitas Sumber Daya 
Manusia (SDM) yang terdidik dan mampu mengikuti corak dan dinamika yang 
sedang berkembang secara cepat (www.mediaindonesia.com, diakses 12 Februari 
2010). 
Untuk menjawab tantangan di zaman persaingan ini, setiap individu harus 
memiliki pengetahuan dan keterampilan yang memadai. Salah satu usaha yang dapat 
ditempuh untuk memiliki pengetahuan dan keterampilan yang memadai tersebut 
adalah melalui jalur pendidikan formal, bermula dari jenjang Sekolah Dasar (SD), 
Sekolah Menengah Pertama (SMP), Sekolah Menengah Akhir (SMA) dan Perguruan 
Tinggi (PT). Bagi para pelajar, pendidikan formal di perguruan tinggi merupakan 
jalur yang penting untuk mempersiapkan diri menghadapi persaingan di dunia kerja 
kelak. Perguruan tinggi sebagai salah satu institusi pendidikan formal juga 
memainkan peran yang besar dalam pendidikan bagi generasi muda. Perguruan tinggi 
bertujuan untuk memberikan bekal pengetahuan dan keterampilan bagi generasi muda 
untuk dapat menjawab tantangan globalisasi saat memasuki masa dewasa. Perguruan 
tinggi hadir sebagai institusi pembangun hubungan antara dunia sekolah dan dunia 
kerja, sekaligus mempersiapkan lulusan sekolah menjadi personel yang siap pakai 
dan siap diberdayakan (www.kompas.com, diakses 12 Februari 2010). 
Universitas  X  sebagai salah satu perguruan tinggi terkemuka di kota 
Bandung, juga menjalankan fungsinya sebagai jembatan yang mempersiapkan para 
lulusannya agar mampu bersaing dalam dunia kerja kelak. Di Universitas  X  ini 
terdapat beberapa fakultas, salah satunya adalah Fakultas Kedokteran. Fakultas 
Kedokteran di Universitas  X  merupakan salah satu Fakultas Kedokteran swasta 
tertua di kota Bandung yang sudah berdiri sejak tahun 1965. Sejak tahun akademik 
2006/2007, Fakultas Kedokteran Universitas  X  menerapkan sistem Kurikulum 
Berbasis Kompetensi (KBK), yang sekaligus memperpendek masa studi mahasiswa 
menjadi lima tahun. Artinya dalam kurun waktu lima tahun, seorang mahasiswa bisa 
sekaligus menyelesaikan Program Sarjana Kedokteran dan Program Profesi Dokter. 
Program Sarjana Kedokteran ditetapkan berlangsung selama 3,5 tahun atau tujuh 
semester dan Program Profesi Dokter berlangsung selama 1,5 tahun atau tiga 
semester. Setelah menjalani kedua program tersebut mahasiswa akan lulus dan 
memperoleh gelar Dokter. Selain memperpendek masa studi, penggunaan sistem 
KBK ini juga  mengubah sistem pengontrakan beban studi dari yang awalnya berupa 
mata kuliah, kini menjadi sistem blok. Blok adalah suatu pengelompokkan bidang 
studi yang saling terintegrasi dan tidak dapat dipisahkan satu sama lain. 
Pembelajaran dalam sistem KBK ini mengarah pada problem based learning, 
sehingga mahasiswa dituntut untuk dapat belajar secara mandiri dan aktif untuk 
mencari penyelesaian dari tugas yang diberikan. Sebagai contoh, dosen memberi 
skenario kasus tentang seseorang yang mengalami kecelakaan pada saat mengendarai 
motor dan ia menderita luka memar. Dalam skenario kasus itu, mahasiswa didorong 
untuk mempelajari penyebab awal munculnya luka, bukan langsung pada 
penanganannya. Mahasiswa diberi keleluasaan untuk bertanya pada siapa pun, dan 
mencari literatur sebanyak-banyaknya mengenai luka memar, agar dapat memahami 
secara mendalam penyebab terjadinya luka dan dapat memberi penanganan secara 
efektif. Oleh karena itu, dalam sistem KBK ini dosen tidak perlu banyak 
menerangkan materi di dalam kelas, tetapi mahasiswa harus berusaha untuk mencari 
tahu mengenai materi yang sedang dibahas dan menjelaskannya kepada dosen. 
Menurut hasil wawancara peneliti dengan kepala bagian KBK di Fakultas 
Kedokteran Universitas  X , Program Sarjana Kedokteran terbagi ke dalam 28 blok, 
dan seluruh blok tersebut wajib dituntaskan oleh mahasiswa sebagai syarat kelulusan. 
Dari 28 blok yang harus ditempuh tersebut, blok Reproductive, Growth, and 
Development dapat dikategorikan blok yang memiliki tingkat kesulitan yang tinggi 
bagi mahasiswa, berdasarkan indikator persentase ketidaklulusan pada blok 
bersangkutan yakni mencapai 24% di tahun ajaran 2008/2009. Blok Reproductive, 
Growth, and Development merupakan blok yang mempelajari penyakit sistem 
reproduksi dan masalah tumbuh kembang dengan menitikberatkan pada penerapan 
ilmu kedokteran dasar untuk menjelaskan fenomena penyakit yang dijumpai dan 
cara-cara menegakkan diagnosis serta penatalaksanaannya.  
Pada blok tersebut di atas, mahasiswa harus mempelajari beberapa materi 
yakni anatomi, histologi, fisiologi, biokimia sistem reproduksi, patologi sistem 
reproduksi, cara menginterpretasikan hasil pemeriksaan laboratorium, dan pilihan 
pengobatan pada kelainan reproduksi dan tumbuh kembang. Untuk dapat 
mempelajari dan memahami ke tujuh materi tersebut sebagai suatu kesatuan yang 
terintegrasi merupakan suatu hal yang sulit bagi para mahasiswa, selain karena materi 
perkuliahan yang banyak, mahasiswa juga harus mengerjakan tugas dan melakukan 
praktikum laboratorium yang rumit. Oleh karena itu, dibutuhkan keterpaduan antara 
kemauan, keseriusan, kerja keras, serta keyakinan diri agar dapat mencapai hasil yang 
optimal dalam mempelajari blok ini.   
Secara spesifik, Albert Bandura (2002) menyatakan keyakinan yang tertanam 
dalam diri seseorang akan kemampuannya untuk menyelesaikan pelbagai tugas 
terangkum ke dalam istilah self-efficacy belief. Self-efficacy belief didefinisikan 
sebagai keyakinan seseorang mengenai kemampuan dirinya dalam mengatur dan 
melaksanakan serangkaian tindakan yang dibutuhkan untuk memperoleh hasil yang 
diinginkan.   
Self-efficacy belief selanjutnya akan mempengaruhi keyakinan seseorang akan 
pilihan yang dibuat, usaha yang dikerahkan, daya tahan, dan bagaimana perasaannya 
ketika berhadapan dengan situasi yang menekan. Oleh karena itu, mahasiswa yang 
self-efficacy belief-nya rendah memiliki kecenderungan untuk menghindari tugas-
tugas maupun situasi sulit yang dipandang sebagai ancaman bagi dirinya. Mahasiswa 
juga akan menetapkan aspirasi dan komitmen yang rendah terhadap tujuan-tujuan 
yang telah ditetapkannya sendiri. Ketika berhadapan dengan tugas-tugas yang sulit, 
mahasiswa ini terpaku pada hambatan dan kemungkinan akan memperoleh hasil tidak 
memuaskan ketimbang berusaha mencapai kesuksesan itu sendiri. Mahasiswa ini 
akan menurunkan usahanya dan cepat menyerah ketika menghadapi kesulitan, hanya 
dengan sedikit kegagalan saja dirinya dapat kehilangan keyakinan akan 
kemampuannya sendiri serta mudah terkena stress dan depresi ketika menghadapi 
hambatan dan kegagalan. 
Sebaliknya, mahasiswa yang memiliki self-efficacy belief yang tinggi 
mempunyai keyakinan kuat atas kemampuan dirinya. Mahasiswa tersebut 
menganggap tugas yang sulit sebagai tantangan yang harus dikuasai dan bukan 
sebagai ancaman atau sesuatu yang harus dihindari. Usaha yang penuh keyakinan 
tersebut memunculkan minat yang berasal dari dalam diri dan usaha itu menyerap 
perhatian yang mendalam terhadap aktivitas perkuliahan. Mahasiswa itu menentukan 
tujuan yang menantang dan berkomitmen terhadap tujuan tersebut, meningkatkan dan 
mempertahankan usaha pada waktu menghadapi kegagalan. Apabila menemui 
kegagalan maka kegagalan dipandang sebagai kurangnya usaha yang dilakukan  
selama ini atau kurangnya pengetahuan dan keterampilan yang dimiliki. Situasi-
situasi yang mengancam dihadapi dengan penuh keyakinan bahwa dirinya  dapat 
mengendalikan situasi tersebut. Usaha yang penuh keyakinan itu memungkinkan 
mahasiswa untuk meraih prestasi yang optimal, mengurangi stres dan menurunkan 
kerentanan terhadap depresi ketika mahasiswa tersebut menghadapi hambatan 
ataupun kegagalan.  
Self-efficacy belief dibentuk oleh empat sumber. Sumber pertama yaitu 
mastery experience (merujuk pada pengalaman keberhasilan dan kegagalan yang 
dialami dan dihayati oleh mahasiswa, selanjutnya berfungsi sebagai indikator dari 
kemampuan dirinya). Misalnya mahasiswa yang menghayati pengalaman dapat 
menyelesaikan tugas dengan baik diwaktu-waktu sebelumnya sebagai suatu 
keberhasilan, pada umumnya keadaan ini akan menumbuhkan keyakinan akan 
kemampuan dirinya. Sumber kedua yaitu vicarious experience, merujuk pada proses 
membandingkan antara diri sendiri dengan orang lain. Tatkala mahasiswa melihat 
orang lain yang memiliki karakteristik sama dengan dirinya   yaitu mahasiswa-
mahasiswa yang sebelumnya telah berhasil menyelesaikan blok reproduction, 
growth, and development   maka akan meningkatkan self-efficacy belief-nya. 
Demikian sebaliknya. Sumber ketiga yaitu verbal atau social persuasions. Verbal 
atau social persuasions berkaitan dengan upaya-upaya menyemangati atau 
memadamkan semangat. Persuasi positif akan meningkatkan self-efficacy belief, 
sedangkan persuasi negatif akan menurunkan self-efficacy belief. Sumber keempat 
adalah physiological and affective state, yang berkaitan dengan penilaian mahasiswa 
mengenai ketergugahan fisik dan emosional yang dialami sebagai indikator dari 
kemampuan diri. Penghayatan mahasiswa terhadap tanda-tanda distress seperti 
gugup, sakit, lelah, takut dan mual dapat mengubah penilaiannya terhadap self-
efficacy belief yang dimiliki.  
Peneliti melakukan wawancara terhadap 25 mahasiswa yang sedang 
mengontrak blok Reproductive, Growth, and Development mengenai keyakinan 
mereka untuk dapat lulus dari blok ini, dan didapatkan hasil 56% mahasiswa menilai 
tidak yakin dengan kemampuannya untuk dapat lulus dari blok ini. Mereka 
menghayati, materi perkuliahan yang diajarkan sangat banyak dan rumit, sehingga 
akan semakin mempersulit mereka untuk lulus dari blok tersebut. Jika mendapat nilai 
C saja rasanya sudah senang. Kelompok mahasiswa ini merasa kurang perlu mencari 
informasi tambahan dari materi yang diajarkan baik melalui senior maupun teman-
teman. Mahasiswa ini meyakini bahwa dirinya akan merasa malas ketika berhadapan 
dengan hambatan baik ketika belajar maupun mengerjakan tugas. Mahasiswa ini pun 
kurang yakin dapat mengendalikan kecemasan dan stress yang dirasakan selama 
mengontrak blok ini. 
Lebih lanjut peneliti berusaha mengetahui penghayatan kelompok mahasiswa 
yang tidak yakin dengan kemampuannya ini mengenai sumber-sumber self-efficacy 
belief-nya diperoleh hasil, sebesar 21% mahasiswa menilai pengalaman keberhasilan 
yang mereka alami pada blok-blok sebelumnya membuat mereka menjadi lebih yakin 
untuk dapat lulus dari blok ini, mereka beranggapan jika di blok-blok sebelumnya 
saja mereka bisa lulus maka di blok ini pun mereka pasti bisa melewatinya dengan 
baik. Sebesar 50% mahasiswa lainnya menilai pengalaman kegagalan yang mereka 
alami pada blok-blok sebelumnya membuat mereka menjadi kurang yakin untuk 
dapat lulus dari blok ini, mereka beranggapan jika di blok-blok sebelumnya saja 
mereka banyak mengalami ketidaklulusan maka di blok ini pun mereka akan 
mengalami kesulitan untuk dapat lulus. Sedangkan 29% mahasiswa lainnya, menilai 
pengalaman keberhasilan dan kegagalan pada blok-blok yang telah dilalui tidak 
berpengaruh terhadap keyakinan mereka untuk dapat lulus dari blok ini.  
Berkaitan dengan sumber vicarious experience, melihat keberhasilan senior 
yang memiliki karakteristik yang sama dengannya (memiliki nilai IPK yang tidak 
jauh berbeda dengan dirinya) dalam menyelesaikan blok ini maka 43% mahasiswa 
menilai mereka menjadi lebih yakin untuk dapat lulus dari blok ini, karena mereka 
menganggap jika seniornya saja bisa lulus mereka pun pasti bisa. Melihat kegagalan 
senior yang memiliki kesamaan karakteristik dengan dirinya dalam menyelesaikan 
blok ini maka 14% mahasiswa menilai mereka menjadi kurang yakin untuk dapat 
lulus dari blok ini, karena mereka menganggap jika seniornya saja tidak lulus mereka 
pun sepertinya akan mengalami hal yang serupa. Sedangkan 43% mahasiswa lainnya 
menilai kegagalan ataupun keberhasilan senior yang memiliki kesamaan karakteristik 
dengan dirinya tidak berpengaruh terhadap keyakinan dirinya untuk dapat lulus dari 
blok Reproductive, Growth, and Development.  
Sebesar 50% mahasiswa dari kelompok ini menilai verbal/social persuasion 
berupa dukungan verbal dan pujian yang mereka terima dari orang lain, membuat 
mereka semakin yakin dapat lulus dari blok ini. Sebesar 43% mahasiswa lainnya 
menilai umpan balik berupa kritik yang diberikan pada blok-blok sebelumnya 
membuat mereka menjadi kurang yakin untuk dapat lulus dari blok ini. Sedangkan 
7% lainnya menilai umpan balik (kritik ataupun pujian) yang diberikan pada blok-
blok sebelumnya tidak berpengaruh terhadap keyakinan mereka untuk dapat lulus 
dari blok Reproductive, Growth, and Development.  
Sebesar 21% mahasiswa dari kelompok ini menilai kecemasan yang 
mahasiswa rasakan ketika menjalani proses pembelajaran pada blok ini akan 
membuat mahasiswa merasa tertantang untuk belajar lebih baik lagi dan akan 
membuat mahasiswa lebih yakin dapat lulus dari blok Reproductive, Growth, and 
Development. Sedangkan 79% mahasiswa lainnya menilai kecemasan yang dirasakan 
ketika menjalani proses pembelajaran pada blok ini akan membuat mahasiswa merasa 
takut jika tidak lulus, sehingga membuat mahasiswa kurang yakin dapat lulus dari 
blok Reproductive, Growth, and Development.  
Dari 25 mahasiswa yang diwawancarai tersebut 44% mahasiswa lainnya 
merasa yakin dapat lulus dari blok Reproductive, Growth, and Development. Dengan 
kemampuan yang dimiliki, mahasiswa ini merasa yakin dapat mengatasi setiap 
masalah yang dihadapi ketika mempelajari blok ini. Mahasiswa ini akan berusaha 
sendiri terlebih dahulu dalam mengerjakan tugas-tugas dosen, tetapi jika tidak 
berhasil barulah mahasiswa akan mencari bantuan kepada teman-teman kuliahnya 
yang dianggap lebih mengerti materi tersebut atau bertanya kepada dosen. Di awal 
perkuliahan, mahasiswa ini sempat berbincang-bincang dengan mahasiswa senior 
yang sudah pernah mempelajari blok ini, dan senior merekapun memberikan 
beberapa saran agar dapat mempelajari blok ini secara efisien dan efektif. Diakui 
bahwa untuk dapat lulus dari blok Reproductive, Growth, and Development bukanlah 
hal yang mudah, namun menganggap itu sebagai suatu tantangan dan justru semakin 
memicu motivasinya untuk berusaha lebih optimal. Dalam belajar maupun 
mengerjakan tugas, mahasiswa yakin dapat berusaha untuk tetap fokus dan 
mengesampingkan rasa malas dan bosan yang mereka rasakan. Ketika mereka 
mengalami kegagalan dalam mengerjakan tugas, mereka juga yakin mampu 
mengendalikan rasa frustasi dan kekecewaan yang mereka alami. 
Dari kelompok mahasiswa yang yakin dapat lulus dari blok Reproductive, 
Growth, and Development, peneliti berusaha untuk mengetahui penilaian mahasiswa 
bersangkutan tentang sumber-sumber self-efficacy belief-nya. Berdasarkan 
penelusuran peneliti didapatkan hasil, sebesar 73% mahasiswa menilai pengalaman 
keberhasilan yang mereka alami pada blok-blok sebelumnya membuat mereka 
menjadi lebih yakin untuk dapat lulus dari blok ini, mereka beranggapan jika di blok-
blok sebelumnya saja mereka bisa lulus maka di blok ini pun mereka pasti bisa 
melewatinya dengan baik. Sedangkan 27% mahasiswa lainnya, menilai pengalaman 
keberhasilan dan kegagalan pada blok-blok yang telah dilalui tidak berpengaruh 
terhadap keyakinan mereka untuk dapat lulus dari blok ini.  
Berkaitan dengan sumber vicarious experience, melihat keberhasilan senior 
yang memiliki karakteristik yang sama dengannya (memiliki nilai IPK yang tidak 
jauh berbeda dengan dirinya) dalam menyelesaikan blok ini maka 36% mahasiswa 
menilai mereka menjadi lebih yakin untuk dapat lulus dari blok ini, karena mereka 
menganggap jika seniornya saja bisa lulus mereka pun pasti bisa. Melihat kegagalan 
senior yang memiliki kesamaan karakteristik dengan dirinya dalam menyelesaikan 
blok ini maka 9% mahasiswa menilai mereka menjadi kurang yakin untuk dapat lulus 
dari blok ini, karena mereka menganggap jika seniornya saja tidak lulus mereka pun 
sepertinya akan mengalami hal yang serupa. Sedangkan 55% mahasiswa lainnya 
menilai kegagalan ataupun keberhasilan senior yang memiliki kesamaan karakteristik 
dengan dirinya tidak berpengaruh terhadap keyakinan dirinya untuk dapat lulus dari 
blok Reproductive, Growth, and Development.  
Sebesar 36% mahasiswa dari kelompok ini menilai verbal/social persuasion 
berupa dukungan verbal dan pujian yang mereka terima dari orang lain, membuat 
mereka semakin yakin dapat lulus dari blok ini. Sebesar 18% mahasiswa lainnya 
menilai umpan balik berupa kritik yang diberikan pada blok-blok sebelumnya 
membuat mereka menjadi kurang yakin untuk dapat lulus dari blok ini. Sedangkan 
46% lainnya menilai umpan balik (kritik ataupun pujian) yang diberikan pada blok-
blok sebelumnya tidak berpengaruh terhadap keyakinan mereka untuk dapat lulus 
dari blok Reproductive, Growth, and Development.  
Sebesar 46% mahasiswa dari kelompok ini menilai kecemasan yang 
mahasiswa rasakan ketika menjalani proses pembelajaran pada blok ini akan 
membuat mahasiswa merasa tertantang untuk belajar lebih baik lagi dan akan 
membuat mahasiswa lebih yakin dapat lulus dari blok Reproductive, Growth, and 
Development. Sebesar 36% mahasiswa lainnya menilai kecemasan yang dirasakan 
ketika menjalani proses pembelajaran pada blok ini akan membuat mahasiswa merasa 
takut jika tidak lulus, sehingga membuat mahasiswa kurang yakin dapat lulus dari 
blok Reproductive, Growth, and Development. Sedangkan 18% mahasiswa lainnya 
menilai kecemasan yang mereka rasakan ketika menjalani proses pembelajaran pada 
blok Reproductive, Growth, and Development tidak mempengaruhi keyakinan 
mereka untuk dapat lulus dari blok ini. 
Berdasarkan hasil wawancara di atas, peneliti mendapat pemahaman bahwa 
kontribusi sumber-sumber self-efficacy belief terhadap self-efficacy belief mahasiswa 
Fakultas Kedokteran yang sedang mengontrak blok Reproductive, Growth, and 
Development bervariasi, sehingga mendorong peneliti untuk melakukan penelitian 
secara menyeluruh mengenai kontribusi sumber-sumber self-efficacy belief terhadap 
self-efficacy belief pada mahasiswa Fakultas Kedokteran yang sedang mengontrak 
blok Reproductive, Growth, and Development di Universitas  X  kota Bandung.  
Seberapa besar kontribusi sumber-sumber self-efficacy belief terhadap self-
efficacy belief pada mahasiswa Fakultas Kedokteran yang sedang mengontrak blok 
Reproductive, Growth, and Development di Universitas  X  kota Bandung. 
Maksud dari penelitian ini adalah untuk memperoleh gambaran empirik 
mengenai sumber-sumber self-efficacy belief dan self-efficacy belief pada mahasiswa 
Fakultas Kedokteran yang sedang mengontrak blok Reproductive, Growth, and 
Development di Universitas  X  kota Bandung. 
Tujuan dari penelitian ini adalah untuk mengetahui seberapa besar kontribusi 
sumber-sumber self-efficacy belief terhadap self-efficacy belief pada mahasiswa 
Fakultas Kedokteran yang sedang mengontrak blok Reproductive, Growth, and 
Development di Universitas  X  kota Bandung.  
melakukan penelitian-penelitian lanjutan mengenai kontribusi sumber-sumber 
self-efficacy belief terhadap self-efficacy belief pada mahasiswa Fakultas 
Kedokteran yang sedang mengontrak blok Reproductive, Growth, and 
Development di Universitas  X  kota Bandung. 
mengontrak blok Reproductive, Growth, and Development, mengenai 
kontribusi sumber-sumber self-efficacy belief terhadap self-efficacy belief 
mereka sehingga dapat menjadi bahan untuk pengenalan diri.  
Development mengenai kontribusi sumber-sumber self-efficacy belief terhadap 
self-efficacy belief mahasiswa didiknya, dalam rangka membimbing 
mahasiswa untuk dapat mempelajari blok ini secara optimal. 
Mahasiswa Fakultas Kedokteran yang sedang mengontrak blok Reproductive, 
Growth, and Development termasuk individu yang berada pada tahap perkembangan 
remaja akhir. Periode dari masa remaja akhir berkisar antara usia 18-22 tahun 
(Santrock, 2007). Mahasiswa dalam tahap ini sedang menjalani transisi dari masa 
remaja ke masa dewasa, mereka harus menghadapi dunia yang kompleks dan penuh 
dengan tantangan dari berbagai macam peran dan tugas yang harus dijalankan. 
Mahasiswa dapat mempersiapkan diri dengan memperoleh pendidikan di Perguruan 
Tinggi sebelum menjalani peran baru di masyarakat agar tidak kesulitan dalam 
menjalankan peran dan tugas tersebut. Saat mengontrak blok Reproductive, Growth, 
and Development, mahasiswa akan menghadapi banyak kesulitan dikarenakan materi 
yang harus mereka pelajari sangat banyak, praktikum laboratorium yang rumit, dan 
mereka juga mendapat tugas yang harus dikerjakan baik secara individual maupun 
kelompok. Untuk menghadapinya maka dibutuhkan tidak hanya sekedar kemauan, 
keseriusan, dan kerja keras melainkan juga keyakinan terhadap kemampuan diri pada 
masing-masing mahasiswa, yang disebut Bandura (2002) sebagai self-efficacy belief. 
Self-efficacy belief merupakan keyakinan seseorang mengenai kemampuan 
dirinya dalam mengatur dan melaksanakan serangkaian tindakan yang dibutuhkan 
untuk memperoleh hasil yang diinginkan (Bandura, 2002). Bila batasan di atas 
diadaptasikan pada mahasiswa Fakultas Kedokteran yang sedang mengontrak blok 
Reproductive, Growth, and Development maka self-efficacy belief mahasiswa 
bersangkutan akan terukur melalui keyakinan yang dimilikinya akan kemampuannya 
dalam mengatur dan melaksanakan serangkaian tindakan yang dibutuhkan untuk 
memperoleh hasil yang diinginkan. Self-efficacy belief mahasiswa Fakultas 
Kedokteran yang sedang mengontrak blok Reproductive, Growth, and Development 
tidak berkaitan dengan seberapa banyak kemampuan yang mereka miliki untuk dapat 
menjalankan tugas-tugasnya, tetapi lebih banyak berkaitan dengan keyakinannya 
bahwa dengan kemampuan yang dimiliki akan berhasil menyelesaikan tugas-
tugasnya saat dihadapkan pada pelbagai keadaan.  
Self-efficacy belief mahasiswa Fakultas Kedokteran yang sedang mengontrak 
blok Reproductive, Growth, and Development akan mempengaruhi pilihan kegiatan 
mahasiswa guna menunjang keberhasilannya dalam blok yang sedang dikontrak, 
usaha yang dikerahkan mahasiswa guna menyelesaikan blok ini, daya tahan 
mahasiswa dalam menghadapi pelbagai kendala yang mungkin muncul saat mereka 
berjuang menyelesaikan blok, dan perasaan mahasiswa ketika beradaptasi dengan 
situasi yang menekan khususnya dalam blok ini.  
Mahasiswa yang menunjukkan self-efficacy belief yang tinggi akan 
menentukan pilihan yang menantang dan berkomitmen terhadap pilihannya tersebut. 
Mahasiswa akan memilih untuk segera mengerjakan tugas yang diberikan oleh dosen 
daripada menghabiskan waktu untuk bermain. Mahasiswa juga akan meningkatkan 
dan mempertahankan usaha mereka pada waktu mengalami kegagalan. Mereka 
dengan cepat mengembalikan penghayatan terhadap efficacy setelah mereka 
mengalami kegagalan. ketika diperhadapkan dengan masalah mahasiswa akan 
bertahan menghadapi dan menyelesaikan masalah tersebut hingga selesai, dan 
menganggap kesulitan yang dihadapi sebagai motivator untuk mempelajari 
bagaimana mengubah suatu kegagalan menjadi keberhasilan. Mereka juga tidak akan 
mudah stres dan depresi saat menghadapi kesulitan serta dapat mengendalikan stres 
ketika mendapatkan kesulitan mengerjakan tugas dari dosen 
Sebaliknya mahasiswa yang memiliki derajat self-efficacy belief yang rendah 
akan menentukan pilihan yang kurang menantang dan komitmen yang lemah 
terhadap pilihan yang telah mereka tetapkan. Mahasiswa memilih pergi bermain 
bersama teman ketika mendapat banyak tugas dari dosen dan tidak mengerjakan tugas 
tersebut. Ketika berhadapan dengan tugas yang sulit, mahasiswa terpaku pada 
kelemahan-kelemahan mereka. Mereka menurunkan usahanya dan cepat menyerah 
dalam menghadapi kesulitan. Mereka lambat bangkit dari kegagalan karena melihat 
performa yang kurang sebagai kemampuan yang tidak mencukupi, hanya dengan 
sedikit kegagalan saja mereka bisa kehilangan keyakinan mengenai kemampuannya. 
Mahasiswa juga menjadikan kesulitan yang ditemui sebagai hambatan yang 
melemahkan, sehingga mereka tidak dapat bertahan lama saat menghadapi kesulitan 
tersebut. Mahasiswa juga akan mudah merasa stres dan depresi saat menghadapi 
kesulitan, karena mereka berpikir bahwa dirinya tidak mampu mengatasi kesulitan 
tersebut. 
Self-efficacy belief individu tidak terlepas dari pengaruh sumber-sumber self-
efficacy belief itu sendiri. Jadi, self-efficacy belief mahasiswa kedokteran yang sedang 
mengontrak blok Reproductive, Growth, and Development juga dibentuk dari empat 
sumber. Keempat sumber yang dimaksud adalah mastery experience, vicarious 
experience, verbal/social persuasion dan physiological and affective states.  
Mastery experience merujuk pada pengalaman mahasiswa Fakultas 
Kedokteran yang sedang mengontrak blok Reproductive, Growth, and Development 
yang mampu menguasai keterampilan tertentu sehingga dapat mencapai keberhasilan 
dalam pembelajarannya. Mastery experience dapat berupa pengalaman keberhasilan 
maupun kegagalan yang dialami. Pengalaman keberhasilan ataupun kegagalan 
tersebut dihayati mahasiswa sebagai tolok ukur akan kemampuannya yang kemudian 
akan berpengaruh dalam pembentukan self-efficacy belief. Keberhasilan dalam 
mengerjakan tugas yang diberikan diwaktu sebelumnya dapat membangun self-
efficacy belief-nya ketika mempelajari blok Reproductive, Growth, and Development. 
Selain itu, pengalaman kegagalan dapat menurunkan self-efficacy belief  mahasiswa. 
Vicarious experience merujuk pada pengamatan mahasiswa Fakultas 
Kedokteran yang sedang mengontrak blok Reproductive, Growth, and Development 
terhadap orang lain dan menemukan beberapa persamaan antara dirinya dengan 
model yang diamatinya tersebut. Melihat senior yang memiliki karakteristik yang 
serupa dengannya dapat melewati blok tersebut dengan nilai yang memuaskan setelah 
berusaha dengan sungguh-sungguh, akan dapat meningkatkan kepercayaan 
mahasiswa tersebut bahwa ia juga dapat menguasai kemampuan untuk melakukan 
aktivitas yang kurang lebih sama untuk mencapai sukses. Begitu juga sebaliknya, jika 
mahasiswa melihat senior yang mirip dengannya walaupun sudah berusaha keras 
namun tetap saja tidak lulus di blok tersebut, hal tersebut akan dapat menurunkan 
penilaian terhadap self-efficacy belief yang mereka miliki. Besarnya pengaruh 
pengalaman orang lain terhadap self-efficacy belief tergantung dari seberapa besar 
kemiripan yang dimiliki keduanya. Semakin besar asumsi bahwa dirinya menyerupai 
sang model, semakin besar pengaruh kegagalan dan keberhasilan dari model tersebut 
terhadap dirinya.  
Verbal/social persuasion merujuk pada  ungkapan verbal yang disampaikan 
oleh orang lain (teman, keluarga, dosen) berupa pujian ataupun kritik. Pemaknaan 
terhadap ungkapan verbal yang diterima mahasiswa berbeda-beda tergantung dari 
bentuk ungkapan yang diberikan (positif atau negatif) dan siapa yang memberikan 
persuasi verbal tersebut. Seorang mahasiswa yang dipersuasi secara verbal bahwa 
mereka memiliki atau tidak memiliki hal-hal yang dibutuhkan untuk berhasil dan 
lulus dalam mempelajari blok Reproductive, Growth, and Development, akan 
membentuk keyakinan diri mereka mengenai kemampuan mereka. Seorang 
mahasiswa yang dipersuasi bahwa dirinya memiliki kemampuan yang mencukupi dan 
dapat lulus dari blok Reproductive, Growth, and Development, maka mahasiswa 
tersebut akan memiliki keyakinan yang lebih kuat terhadap kemampuannya dan akan 
mengoptimalkan usahanya. Sebaliknya, seorang mahasiswa yang dipersuasi bahwa ia 
tidak memiliki kemampuan untuk lulus dari blok Reproductive, Growth, and 
Development, cenderung akan mudah menyerah dan meragukan kemampuannya 
Physiological and affective states merujuk pada penghayatan mahasiswa 
Fakultas Kedokteran yang sedang mengontrak blok Reproductive, Growth, and 
Development terhadap kondisi fisiologis dan emosional yang dirasakan mahasiswa 
sewaktu menghadapi tugas akademis. Keadaan fisik dan emosional saat menghadapi 
dan mengerjakan tugas akan dijadikan informasi mengenai kemampuannya dalam 
mengerjakan suatu tugas. Dalam melakukan aktivitas yang melibatkan kekuatan dan 
stamina, mahasiswa menilai kelelahan dan sakit yang mereka alami sebagai tanda 
dari pelemahan fisik. Keadaan emosional juga mempengaruhi penilaian mahasiswa 
tentang self-efficacy belief. Keadaan emosional yang positif meningkatkan self-
efficacy belief, sedangkan keadaan emosional yang negatif menurunkannya. Secara 
umum, meningkatkan kesejahteraan fisik dan emosional seseorang dan mengurangi 
keadaan emosional yang negatif dapat menguatkan self-efficacy belief.  
Setiap mahasiswa Fakultas Kedokteran yang sedang mengontrak blok 
Reproductive, Growth, and Development akan berhadapan dengan sumber-sumber 
self-efficacy belief secara serempak, namun dengan kekuatan pengaruh yang berbeda-
beda. Untuk dapat membentuk self-efficacy belief, keempat sumber informasi tersebut 
harus diproses secara kognitif. Dalam pemrosesan kognitif ini mahasiswa memilih, 
mempertimbangkan dan mengintegrasikan sumber-sumber informasi ke dalam 
penilaian self-efficacy belief. Berdasarkan pengaruh salah satu sumber ataupun 
kombinasi dari beberapa sumber self-efficacy belief, maka self-efficacy belief 
mahasiswa dapat terbentuk, yang selanjutnya akan mempengaruhi keyakinan 
mahasiswa dalam menentukan pilihan, pengerahan usaha, daya tahan dalam 
menghadapi kesulitan, serta perasaan mahasiswa ketika menghadapi situasi yang 
menekan.   
Seperti misalnya mahasiswa yang menghayati pengalaman keberhasilan yang 
dialaminya selama menempuh perkuliahan di blok-blok sebelumnya sebagai sumber 
self-efficacy belief yang paling berpengaruh bagi dirinya, sehingga membentuk self-
efficacy belief-nya menjadi tinggi. Hal tersebut akan berpengaruh pada penentuan 
aktifitasnya sehingga mahasiswa lebih memprioritaskan waktunya untuk belajar, dan 
mempelajari blok tersebut dengan sungguh-sungguh. Ketika menghadapi kesulitan 
dalam menjalani perkuliahan di blok Reproductive, Growth, and Development, 
mahasiswa tidak mudah putus asa dan merasa tertantang untuk menyelesaikan setiap 
kesulitan dan hambatan yang ia hadapi selama perkuliahan.  
Reproductive, 
Growth, and 
Skema kerangka pikir adalah sebagai berikut : 
Sumber self-efficacy belief :        
SELF-EFFICACY 
keyakinan akan :                 
1. Pilihan yang dibuat         
2. Usaha yang dikerahkan                   
3. Daya tahan dalam         
4. Penghayatan perasaan  
1. Keberhasilan atau kegagalan mahasiswa untuk menyelesaikan blok 
Reproductive, Growth, and Development berkaitan dengan sejauhmana 
mahasiswa meyakini kemampuannya dalam mempelajari blok tersebut. 
2. Mahasiswa yang meyakini kemampuannya, akan membuka peluangnya untuk 
berbuat optimal bagi keberhasilannya menuntaskan blok Reproductive, 
Growth, and Development. 
3. Mahasiswa yang tidak yakin akan kemampuannya, akan menurunkan 
peluangnya untuk berbuat optimal dalam menuntaskan blok Reproductive, 
Growth, and Development. 
1. Terdapat kontribusi sumber mastery experience terhadap self-efficacy belief 
mahasiswa Fakultas Kedokteran yang sedang mengontrak blok Reproductive, 
Growth, and Development  
2. Terdapat kontribusi sumber vicarious experience terhadap self-efficacy belief 
mahasiswa Fakultas Kedokteran yang sedang mengontrak blok Reproductive, 
Growth, and Development  
3. Terdapat kontribusi sumber verbal/social persuasion terhadap self-efficacy 
belief mahasiswa Fakultas Kedokteran yang sedang mengontrak blok 
Reproductive, Growth, and Development  
4. Terdapat kontribusi sumber physiological and affective states terhadap self-
efficacy belief mahasiswa Fakultas Kedokteran yang sedang mengontrak blok 
Reproductive, Growth, and Development  
Menurut Bandura (2002), self-efficacy belief merupakan keyakinan seseorang 
mengenai kemampuan dirinya dalam mengatur dan melaksanakan serangkaian 
tindakan yang dibutuhkan untuk memperoleh hasil yang diinginkan.  
Self-efficacy belief  dapat mempengaruhi pilihan yang dibuat oleh seseorang, 
usaha yang dikeluarkannya, berapa lama seseorang bertahan saat dihadapkan pada 
rintangan-rintangan (dan saat dihadapkan dengan kegagalan), serta bagaimana 
perasaannya beradaptasi dengan situasi yang menekan tersebut. Self-efficacy belief 
mempengaruhi pilihan yang dibuat dan arah dari berbagai macam tindakan yang 
mereka inginkan. Individu cenderung untuk memilih tugas dan aktivitas dimana 
mereka merasa yakin dan berkompeten dan mereka akan menghindari tugas di mana 
mereka merasa tidak yakin. Kecuali jika orang percaya bahwa tindakan mereka akan 
mempunyai konsekuensi yang diinginkan, mereka hanya mempunyai sedikit 
incentive untuk melibatkan diri dalam tindakan tersebut. Apapun juga faktor yang 
mempengaruhi perilaku, mereka berakar pada keyakinan inti bahwa seseorang 
mempunyai kemampuan untuk melakukan perilaku tersebut. Orang dengan self-
efficacy belief rendah memiliki aspirasi yang rendah dan komitmen yang lemah 
terhadap tujuan-tujuan yang telah mereka tetapkan. Orang yang meragukan 
kemampuan mereka juga menghindari tugas-tugas yang sulit yang dipandang sebagai 
ancaman terhadap diri mereka. Sebaliknya mereka yang mempunyai self-efficacy 
belief tinggi akan menentukan tujuan yang menantang dan berkomitmen terhadap 
tujuan tersebut. Orang dengan keyakinan yang tinggi terhadap kemampuan mereka 
juga menganggap tugas yang sulit sebagai tantangan yang harus dikuasai, dan bukan 
sebagai ancaman atau sesuatu yang harus dihindari.  
Self-efficacy belief juga menentukan besarnya usaha yang dikerahkan pada 
suatu aktivitas, berapa lama mereka akan bertahan ketika menghadapi rintangan dan 
ketika mereka dihadapkan pada situasi yang kurang baik. Semakin tinggi 
penghayatan efficacy belief, semakin besar usaha, ketekunan, dan daya tahannya. 
Orang dengan self-efficacy belief tinggi mempunyai minat yang berasal dari dalam 
diri dan perhatian yang mendalam pada aktivitas. Mereka meningkatkan dan 
mempertahankan usaha mereka pada waktu menghadapi kegagalan. Mereka dengan 
cepat mengembalikan penghayatan terhadap self-efficacy belief setelah mereka 
mengalami kegagalan atau hambatan. Mereka memandang kegagalan sebagai usaha 
yang tidak memadai atau kurangnya pengetahuan dan keterampilan, yang sebetulnya 
dapat diperoleh. Mereka mendekati situasi-situasi mengancam dengan penuh 
keyakinan bahwa mereka dapat mengendalikan situasi-situasi tersebut. Sebaliknya, 
orang dengan self-efficacy belief rendah ketika berhadapan dengan tugas yang sulit, 
mereka terpaku pada kelemahan-kelemahan mereka dan hambatan-hambatan yang 
akan mereka hadapi, dan kemungkinan hasil yang tidak menyenangkan daripada 
berkonsentrasi pada usaha untuk mencapai keberhasilan. Mereka menurunkan 
usahanya dan cepat menyerah dalam menghadapi kesulitan. Mereka lambat bangkit 
dari kegagalan karena mereka melihat performance yang kurang sebagai kemampuan 
yang tidak memadai. Hanya dengan mengalami sedikit kegagalan saja mereka bisa 
kehilangan keyakinan mengenai kemampuan mereka. 
Self-efficacy belief juga mempengaruhi pola pikir dan reaksi emosional. Self-
efficacy belief yang tinggi menciptakan ketenangan dalam mendekati aktivitas dan 
tugas yang sulit. Usaha penuh keyakinan yang dimiliki oleh orang dengan self-
efficacy belief tinggi itu menghasilkan prestasi pribadi, mengurangi stres dan 
menurunkan kerentanan terhadap depresi. Dan sebaliknya, orang dengan self-efficacy 
belief yang rendah yakin bahwa berbagai hal lebih berat daripada kenyataannya. 
Keyakinan tersebut menciptakan kecemasan, stres, depresi, dan mempersempit 
pikiran dalam mencari bagaimana cara terbaik untuk memecahkan suatu masalah. 
Oleh karena itu orang dengan penghayatan self-efficacy belief rendah akan mudah 
terkena stres dan depresi. 
Self-efficacy belief seseorang dapat dikembangkan secara kognitif melalui 
empat sumber pengaruh utama, yaitu: mastery experiences, vicarious experience, 
social/verbal persuasion dan physiological and affective states. Hal tersebut dapat 
menjadi instruktif hanya melalui proses kognitif dan pemikiran secara reflektif. Oleh 
karena itu, harus dapat dibedakan antara informasi yang diperoleh melalui kejadian 
yang dialami dengan informasi yang telah dipilih, dipertimbangkan dan 
diintegrasikan menjadi penilaian self-efficacy belief. 
Pemrosesan kognitif dari informasi efficacy belief meliputi dua fungsi. Fungsi 
yang pertama berkaitan dengan tipe informasi yang diperhatikan seseorang dan 
digunakan sebagai indikator dari self-efficacy belief. 

